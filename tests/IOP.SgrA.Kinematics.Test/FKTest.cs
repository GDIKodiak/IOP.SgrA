﻿using IOP.ISEA.URDF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Kinematics.Test
{
    public class FKTest
    {
        public const string URDFFILE = "E:\\Projects\\kr20_18urdf\\urdf\\kr1.urdf";

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void FKTest1()
        {
            try
            {
                var system = GeneratorSystem();
                var m1 = system.FKinSpace("joint1", new List<double>() { 0.5 });
            }
            catch (Exception e)
            {
                Assert.Fail(e.ToString());
            }
        }

        [Test]
        public void FKTest2()
        {
            try
            {
                var system = GeneratorSystem();
                var m1 = system.FKinSpace("joint2", new List<double>() { 0.5, 0.6 });
            }
            catch (Exception e)
            {
                Assert.Fail(e.ToString());
            }
        }

        [Test]
        public void FKTest3() 
        {
            try
            {
                var system = GeneratorSystem();
                var m1 = system.FKinSpace("joint3", new List<double>() { 0.5, 0.6, -0.4 });
            }
            catch (Exception e)
            {
                Assert.Fail(e.ToString());
            }
        }

        [Test]
        public void FKTest4()
        {
            try
            {
                var system = GeneratorSystem();
                var m1 = system.FKinSpace("joint4", new List<double>() { 0.5, 0.6, -0.4, 1.57073 });
            }
            catch (Exception e)
            {
                Assert.Fail(e.ToString());
            }
        }

        [Test]
        public void FKTest5()
        {
            try
            {
                var system = GeneratorSystem();
                var m1 = system.FKinSpace("joint5", new List<double>() { 0.5, 0.6, -0.4, 1.57073, 1.3305 });
            }
            catch (Exception e)
            {
                Assert.Fail(e.ToString());
            }
        }

        [Test]
        public void JointTrajectoryTest()
        {
            try
            {
                var system = GeneratorSystem();
                List<double> init = [0, 0, 0, 0, 0, 0];
                List<double> end = [0.1, 1.8, -1.6, -0.6];
                var r = system.JointTrajectory("joint6", init, end, TimeSpan.FromSeconds(10).TotalMilliseconds, 66, 
                    out ThetaPlanningData? data);
                Assert.That(data, Is.Not.Null);
                Assert.That(r, Is.EqualTo(PlanningResult.Success));
            }
            catch (Exception e)
            {
                Assert.Fail(e.ToString());
            }
        }

        private RobotSystem GeneratorSystem()
        {
            URDFDocument document = URDFDocument.ParseFromFile(URDFFILE);
            return document.GenerateSystem();
        }
    }
}
