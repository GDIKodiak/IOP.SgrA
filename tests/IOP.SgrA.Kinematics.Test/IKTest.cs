﻿using IOP.ISEA.URDF;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Complex;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Kinematics.Test
{
    public class IKTest
    {
        public const string URDFFILE = "E:\\Projects\\kr20_18urdf\\urdf\\kr1.urdf";
        public static MatrixBuilder<double> VM = Matrix<double>.Build;
        public const double Epsilon = 0.00001;

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void IKTest1()
        {
            try
            {
                var system = GeneratorSystem();
                Matrix<double> T = VM.DenseOfRowArrays([
                    [0.877583, -0.479426, 0, 0], 
                    [0.479426, 0.877583, 0, 0], 
                    [0, 0, 1, 0.2452], 
                    [0, 0, 0, 1]]);
                var r = system.IKinSpace("joint1", T, out List<double> thetaList, 20, Epsilon, Epsilon);
                Assert.That(r == IKResult.Success, Is.True);
                Assert.That(thetaList, Has.Count.EqualTo(1));
                Assert.That(Math.Abs(thetaList[0] - 0.5), Is.LessThanOrEqualTo(Epsilon));
            }
            catch (Exception e)
            {
                Assert.Fail(e.ToString());
            }
        }

        [Test]
        public void IKTest2()
        {
            try
            {
                var system = GeneratorSystem();
                Matrix<double> T = VM.DenseOfRowArrays([
                    [0.715476, -0.455518, 0.529714, -0.180886],
                    [0.419594, 0.886406, 0.195511, 0.027996],
                    [-0.5586, 0.0823813, 0.825336, 0.515],
                    [0, 0, 0, 1]]);
                var r = system.IKinSpace("joint2", T, out List<double> thetaList, 20, Epsilon, Epsilon);
                Assert.That(r == IKResult.Success, Is.True);
                Assert.That(thetaList, Has.Count.EqualTo(2));
                Assert.That(Math.Abs(thetaList[0] - 0.5), Is.LessThanOrEqualTo(Epsilon));
                Assert.That(Math.Abs(thetaList[1] - 0.6), Is.LessThanOrEqualTo(Epsilon));
            }
            catch (Exception e)
            {
                Assert.Fail(e.ToString());
            }
        }

        [Test]
        public void IKTest3()
        {
            try
            {
                var system = GeneratorSystem();
                Matrix<double> T = VM.DenseOfRowArrays([
                    [0.859082, -0.476697, 0.18638, -0.334438],
                    [0.472597, 0.87859, 0.0687905, -0.0191913],
                    [-0.196543, 0.0289858, 0.980067, 1.27833],
                    [0, 0, 0, 1]]);
                var r = system.IKinSpace("joint3", T, out List<double> thetaList, 40, Epsilon, Epsilon);
                Assert.That(r == IKResult.Success, Is.True);
                Assert.That(thetaList, Has.Count.EqualTo(3));
                Assert.That(Math.Abs(thetaList[0] - 0.5), Is.LessThanOrEqualTo(0.0001));
                Assert.That(Math.Abs(thetaList[1] - 0.6), Is.LessThanOrEqualTo(0.0001));
                Assert.That(Math.Abs(thetaList[2] + 0.4), Is.LessThanOrEqualTo(0.001));
            }
            catch (Exception e)
            {
                Assert.Fail(e.ToString());
            }
        }

        [Test]
        public void IKTest4()
        {
            try
            {
                var system = GeneratorSystem();
                Matrix<double> T = VM.DenseOfRowArrays([
                    [0.866729, -0.0894094, 0.490701, -0.576086],
                    [0.487347, -0.0576234, -0.871305, -0.212628],
                    [0.106179, 0.994327, -0.00637042, 1.44037],
                    [0, 0, 0, 1]]);
                var r = system.IKinSpace("joint4", T, out List<double> thetaList, 40, Epsilon, Epsilon);
                Assert.That(r == IKResult.Success, Is.True);
                Assert.That(thetaList, Has.Count.EqualTo(4));
                Assert.That(Math.Abs(thetaList[0] - 0.5), Is.LessThanOrEqualTo(0.0001));
                Assert.That(Math.Abs(thetaList[1] - 0.6), Is.LessThanOrEqualTo(0.0001));
                Assert.That(Math.Abs(thetaList[2] + 0.4), Is.LessThanOrEqualTo(0.0001));
                Assert.That(Math.Abs(thetaList[3] - 1.57073), Is.LessThanOrEqualTo(0.001));
            }
            catch (Exception e)
            {
                Assert.Fail(e.ToString());
            }
        }

        [Test]
        public void IKTest5()
        {
            try
            {
                var system = GeneratorSystem();
                Matrix<double> T = VM.DenseOfRowArrays([
                    [-0.238873, -0.299166, 0.923818, -1.12082],
                    [0.794088, -0.607744, 0.00851887, -0.361936],
                    [0.558896, 0.735627, 0.382737, 1.38463],
                    [0, 0, 0, 1]]);
                var r = system.IKinSpace("joint5", T, out List<double> thetaList, 66, Epsilon, Epsilon);
                Assert.That(r == IKResult.Success, Is.True);
                Assert.That(thetaList, Has.Count.EqualTo(5));
            }
            catch (Exception e)
            {
                Assert.Fail(e.ToString());
            }
        }

        private RobotSystem GeneratorSystem()
        {
            URDFDocument document = URDFDocument.ParseFromFile(URDFFILE);
            return document.GenerateSystem();
        }
    }
}
