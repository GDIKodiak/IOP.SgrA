﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SixLabors.ImageSharp.Drawing;
using System.Numerics;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using SixLabors.ImageSharp.Drawing.Processing;

namespace IOP.SgrA.Modeling.Test
{
    public class BSplineTest
    {
        [Test]
        public void CreateTest1()
        {
            try
            {
                int ii = 4;
                Vector3[] points = new Vector3[]
                {
                    new Vector3(200,400,0),
                    new Vector3(300,300,0),
                    new Vector3(500,500,0),
                    new Vector3(600,400,0)
                };
                Span<Vector3> vectors = BSpline.CreateBSpline(1024, 2, points, null, null);
                PointF[] pointFs = new PointF[vectors.Length];
                PointF[] controls = new PointF[]
                {
                    new PointF(200,400),
                    new PointF(300,300),
                    new PointF(500,500),
                    new PointF(600,400)
                };
                for(int i = 0; i < vectors.Length; i++)
                {
                    pointFs[i] = new PointF(vectors[i].X, vectors[i].Y);
                }
                using var image = new Image<Rgba32>(800, 800);
                var lineSeq = new LinearLineSegment(pointFs);
                var p = new Path(lineSeq);
                image.Mutate(x => x.BackgroundColor(Color.White).Draw(Color.Black, 2, p));
                for(int i = 0; i < controls.Length; i++)
                {
                    image.Mutate(x => x.Fill(Color.Red, new EllipsePolygon(controls[i], 5)));
                }
                image.Save($"test1{ii}.png");
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }
    }
}
