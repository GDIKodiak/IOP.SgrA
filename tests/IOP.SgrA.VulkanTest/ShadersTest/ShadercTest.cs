﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IOP.ISEA;
using IOP.SgrA.GLSL;
using IOP.SgrA.VulkanTest.TestShaders;
using Silk.NET.Shaderc;
using Silk.NET.Vulkan;

namespace IOP.SgrA.VulkanTest
{
    public class ShadercTest
    {
        [Test]
        public void NormalVertTest1()
        {
            Shaderc api = Shaderc.GetApi();
            TestVert vert = new TestVert();
            string code = vert.GetCode();
            ParseCode(vert, api);
        }
        [Test]
        public void NormalFragTest1()
        {
            Shaderc api = Shaderc.GetApi();
            TestFrag frag = new TestFrag();
            ParseCode(frag, api);
        }
        [Test]
        public void NormalVertTest2()
        {
            Shaderc api = Shaderc.GetApi();
            TestVert2 vert = new TestVert2();
            ParseCode(vert, api);
        }
        [Test]
        public void NormalVertTest3()
        {
            Shaderc api = Shaderc.GetApi();
            TestVert3 vert = new TestVert3();
            string code = vert.GetCode();
            ParseCode(vert, api);
        }
        [Test]
        public void NormalVertTest4()
        {
            Shaderc api = Shaderc.GetApi();
            TestVert4 vert = new TestVert4();
            string code = vert.GetCode();
            ParseCode(vert, api);
        }
        [Test]
        public void NormalVertTest5()
        {
            Shaderc api = Shaderc.GetApi();
            TestVert5 vert = new TestVert5();
            string code = vert.GetCode();
            ParseCode(vert, api);
        }
        [Test]
        public void NormalFragTest2()
        {
            Shaderc api = Shaderc.GetApi();
            TestFrag2 vert = new TestFrag2();
            ParseCode(vert, api);
        }
        [Test]
        public void NormalFragTest3()
        {
            Shaderc api = Shaderc.GetApi();
            TestFrag3 frag = new TestFrag3();
            string code = frag.GetCode();
            ParseCode(frag, api);
        }
        [Test]
        public void NormalFragTest4()
        {
            Shaderc api = Shaderc.GetApi();
            TestFrag4 frag = new TestFrag4();
            string code = frag.GetCode();
            ParseCode(frag, api);
        }
        [Test]
        public void NormalFragTest5()
        {
            Shaderc api = Shaderc.GetApi();
            TestFrag5 frag = new TestFrag5();
            string code = frag.GetCode();
            ParseCode(frag, api);
        }
        [Test]
        public void NormalFragTest6()
        {
            Shaderc api = Shaderc.GetApi();
            TestFrag6 frag = new TestFrag6();
            string code = frag.GetCode();
            ParseCode(frag, api);
        }
        [Test]
        public void NormalFragTest7()
        {
            Shaderc api = Shaderc.GetApi();
            TestFrag7 frag = new TestFrag7();
            string code = frag.GetCode();
            ParseCode(frag, api);
        }
        [Test]
        public void NormalFragTest8()
        {
            Shaderc api = Shaderc.GetApi();
            TestFrag8 frag = new TestFrag8();
            string code = frag.GetCode();
            ParseCode(frag, api);
        }
        [Test]
        public void NoramlFragTest9()
        {
            Shaderc api = Shaderc.GetApi();
            TestFrag9 frag = new TestFrag9();
            string code = frag.GetCode();
            ParseCode(frag, api);
        }

        [Test]
        public void NoramlGeomTest1()
        {
            Shaderc api = Shaderc.GetApi();
            TestGeom geom = new TestGeom();
            string code = geom.GetCode();
            ParseCode(geom, api);
        }

        private void ParseCode(GLSLVert vert, Shaderc api)
        {
            var code = vert.GetCode();
            if (string.IsNullOrEmpty(code)) throw new NullReferenceException("No Code");
            Span<byte> buffer = Encoding.UTF8.GetBytes(code);
            ParseCode(buffer, ShaderKind.VertexShader, "main.vert", api);
        }
        private void ParseCode(GLSLFrag frag, Shaderc api)
        {
            var code = frag.GetCode();
            if (string.IsNullOrEmpty(code)) throw new NullReferenceException("No Code");
            Span<byte> buffer = Encoding.UTF8.GetBytes(code);
            ParseCode(buffer, ShaderKind.FragmentShader, "main.frag", api);
        }
        private void ParseCode(GLSLGeom geom, Shaderc api)
        {
            var code = geom.GetCode();
            if (string.IsNullOrEmpty(code)) throw new NullReferenceException("No Code");
            Span<byte> buffer = Encoding.UTF8.GetBytes(code);
            ParseCode(buffer, ShaderKind.GeometryShader, "main.geom", api);
        }
        private unsafe void ParseCode(Span<byte> buffer, ShaderKind kind, string fileName, Shaderc api)
        {
            Compiler* compiler = api.CompilerInitialize();
            CompileOptions* option = api.CompileOptionsInitialize();
            api.CompileOptionsSetSourceLanguage(option, SourceLanguage.Glsl);
            api.CompileOptionsSetTargetEnv(option, TargetEnv.Vulkan, (1u << 22) | (2 << 12));
            try
            {
                CompilationResult* result = api.CompileIntoSpv(compiler, in buffer[0], (nuint)buffer.Length, kind, fileName, "main", null);
                CompilationStatus status = api.ResultGetCompilationStatus(result);
                if (status != CompilationStatus.Success)
                {
                    var message = api.ResultGetErrorMessageS(result);
                    api.ResultRelease(result);
                    throw new Exception(message);
                }
                api.ResultRelease(result);
            }
            finally
            {
                api.CompileOptionsRelease(option);
                api.CompilerRelease(compiler);
            }
        }
    }
}
