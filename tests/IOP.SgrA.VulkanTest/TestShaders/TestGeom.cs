﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.VulkanTest.TestShaders
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    [GeometryVertexInput(GeometryInputType.TrianglesAdjacency)]
    [GeometryVertexOutput(GeometryOutputType.TriangleStrip, 15)]
    public partial class TestGeom : GLSLGeom
    {
        [GeometryInput(0)]
        public vec3[] VNormal { get; set; }
        [GeometryInput(1)]
        public vec3[] VPosition { get; set; }

        [ShaderOutput(0)]
        public vec3 GNormal { get; set; }
        [ShaderOutput(1)]
        public vec3 GPosition { get; set; }
        [ShaderOutput(2)]
        public int GIsEdge { get; set; }

        public float EdgeWidth = 2.0f;
        public float PctExtend = 1.5f;

        /// <summary>
        /// 是否为正面
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        public bool IsFrontFacing(vec3 a, vec3 b, vec3 c)
        {
            return ((a.x * b.y - b.x * a.y) +
                (b.x * c.y - c.x * b.y)
                + (c.x * a.y - a.x * c.y)) > 0;
        }
        /// <summary>
        /// 插入描边线
        /// </summary>
        /// <param name="e0"></param>
        /// <param name="e1"></param>
        public void EmitEdgeQuad(vec3 e0, vec3 e1)
        {
            vec2 ext = PctExtend * (e1.xy - e0.xy);
            vec2 v = normalize(e1.xy - e0.xy);
            vec2 n = new vec2(-v.y, v.x) * EdgeWidth;
            // Emit the quad
            GIsEdge = 1;//This is part of the sil. edge
            gl_Position = new vec4(e0.xy - ext, e0.z, 1.0f);
            EmitVertex();
            gl_Position = new vec4(e0.xy - n - ext, e0.z, 1.0f);
            EmitVertex();
            gl_Position = new vec4(e1.xy + ext, e1.z, 1.0f);
            EmitVertex();
            gl_Position = new vec4(e1.xy - n + ext, e1.z, 1.0f);
            EmitVertex();
            EndPrimitive();
        }

        public override void main()
        {
            vec3 p0 = gl_in[0].gl_Position.xyz / gl_in[0].gl_Position.w;
            vec3 p1 = gl_in[1].gl_Position.xyz / gl_in[1].gl_Position.w;
            vec3 p2 = gl_in[2].gl_Position.xyz / gl_in[2].gl_Position.w;
            vec3 p3 = gl_in[3].gl_Position.xyz / gl_in[3].gl_Position.w;
            vec3 p4 = gl_in[4].gl_Position.xyz / gl_in[4].gl_Position.w;
            vec3 p5 = gl_in[5].gl_Position.xyz / gl_in[5].gl_Position.w;
            if (IsFrontFacing(p0, p2, p4))
            {
                if (!IsFrontFacing(p0, p1, p2))
                    EmitEdgeQuad(p0, p2);
                if (!IsFrontFacing(p2, p3, p4))
                    EmitEdgeQuad(p2, p4);
                if (!IsFrontFacing(p4, p5, p0))
                    EmitEdgeQuad(p4, p0);
            }
            // Output the original triangle
            GIsEdge = 0;
            // This triangle is not part of an edge.
            GNormal = VNormal[0];
            GPosition = VPosition[0];
            gl_Position = gl_in[0].gl_Position;
            EmitVertex();
            GNormal = VNormal[2];
            GPosition = VPosition[2];
            gl_Position = gl_in[2].gl_Position;
            EmitVertex();
            GNormal = VNormal[4];
            GPosition = VPosition[4];
            gl_Position = gl_in[4].gl_Position;
            EmitVertex();
            EndPrimitive();
        }
    }

    public struct Params
    {
        public float EdgeWidth;
        public float PctExtend;
    }
}
