﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.VulkanTest
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class TestVert3 : GLSLVert
    {
        [UniformBuffer(0, 0, DescriptorSetTarget.RenderObject)]
        public PreBufferVals vals { get; set; }
        [UniformBuffer(1, 0, DescriptorSetTarget.Pipeline)]
        public JitterVals jitter { get; set; }

        [VertInput(0, Format.R32G32B32Sfloat)]
        public vec3 pos { get; set; }
        [VertInput(1, Format.R32G32B32Sfloat)]
        public vec3 color { get; set; }
        [ShaderOutput(0)]
        public vec3 outColor { get; set; }
        [ShaderOutput(1)]
        public vec4 outPrePos { get; set; }
        [ShaderOutput(2)]
        public vec4 outCurrPos { get; set; }

        public override void main()
        {
            vec4 prePos = vals.preMvp * new vec4(pos, 1.0f);
            vec4 currPos = vals.mvp * new vec4(pos, 1.0f);
            outPrePos = prePos;
            outCurrPos = currPos;

            gl_Position = vals.mm * jitter.view * jitter.jitterProject * new vec4(pos, 1.0f);
            vec4 world = vals.mm * new vec4(pos, 1.0f);
            if (abs(world.x) <= 0.05) outColor = new vec3(1.0f, 0.84f, 0.0f);
            else if (abs(world.z) <= 0.05) outColor = new vec3(1.0f, 0.27f, 0.0f);
            else outColor = color;
        }
    }

    public struct PreBufferVals
    {
        public mat4 mvp;
        public mat4 mm;
        public mat4 preMvp;
    }

    public struct JitterVals
    {
        public mat4 view;
        public mat4 jitterProject;
    }
}
