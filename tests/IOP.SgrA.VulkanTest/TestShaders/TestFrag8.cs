﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.VulkanTest
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class TestFrag8 : GLSLFrag
    {
        [ShaderInput(0)]
        public vec4 inLightQD { get; set; }
        [ShaderInput(1)]
        public vec2 mcLongLat { get; set; }
        [FragOutput(0, false)]
        public vec4 OutColor { get; set; }

        public override void main()
        {
            vec3 color;//当前片元的颜色值
            if (abs(mcLongLat.y) > 75.0f)
            {
                color = new vec3(1.0f, 1.0f, 1.0f);//两极是白色
            }
            else
            {
                int colorNum = (int)(mcLongLat.x / 45.0f);//颜色号
                vec3[] colorArray = new vec3[]
                {
                   new vec3(1.0f, 0.0f, 0.0f), new vec3(0.0f, 1.0f, 0.0f),
                   new vec3(0.0f, 0.0f, 1.0f), new vec3(1.0f, 1.0f, 0.0f),
                   new vec3(1.0f, 0.0f, 1.0f), new vec3(0.0f, 1.0f, 1.0f),
                   new vec3(0.3f, 0.4f, 0.7f), new vec3(0.3f, 0.7f, 0.2f)
                };
                color = colorArray[colorNum];//根据索引获得颜色值
            }
            OutColor = new vec4(color, 1.0f) * inLightQD;//结合光照强度产生片元最终颜色值
        }
    }
}
