﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.VulkanTest
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class TestVert5 : GLSLVert
    {
        [DynamicUniformBuffer(0, 0, DescriptorSetTarget.Pipeline)]
        public ParticleVals Vals { get; set; }

        [VertInput(0, Format.R32G32Sfloat)]
        public vec2 Pos { get; set; }
        [VertInput(1, Format.R32G32Sfloat)]
        public vec2 Param { get; set; }
        [VertInput(2, Format.R32G32Sfloat)]
        public vec2 InTexCoor { get; set; }

        [ShaderOutput(0)]
        public vec2 OutTexCoor { get; set; }
        [ShaderOutput(1)]
        public float SjFactor { get; set; }
        [ShaderOutput(2)]
        public vec4 VPosition { get; set; }

        public override void main()
        {
            OutTexCoor = InTexCoor;
            gl_Position = Vals.MVP * new vec4(Pos.x, Pos.y, 0.0f, 1.0f);
            VPosition = new vec4(Pos.x, Pos.y, 0.0f, Param.g);
            SjFactor = (Vals.MaxLifeSpan - Param.g) / Vals.MaxLifeSpan;
        }
    }

    public struct ParticleVals
    {
        public mat4 MVP;
        public vec4 StartColor;
        public vec4 EndColor;
        public float MaxLifeSpan;
        public float BJ;
    }
}
