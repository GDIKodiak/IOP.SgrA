﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.VulkanTest
{
    [VertexInputBinding(0, 36)]
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class TestVert : GLSLVert
    {
        [VertInput(0, Format.R32G32B32Sfloat)]
        public vec3 pos { get; set; }
        [VertInput(1, Format.R32G32B32Sfloat)]
        public vec3 aTexCoor { get; set; }
        [VertInput(2, Format.R32G32B32Sfloat)]
        public vec3 inNormal { get; set; }
        [ShaderOutput(0)]
        public vec2 vTexPosition { get; set; }
        [ShaderOutput(1)]
        public vec4 outLightQD { get; set; }
        public gl_PerVertex gl_PerVertex { get; set; }

        [UniformBuffer(0, 0, DescriptorSetTarget.Pipeline)]
        public BufferVals myBufferVals {  get; set; }
        [PushConstant(128, 0)]
        public MyVals myConstantVals {  get; set; }

        const float Factor = 50.0f;


        public override void main()
        {
            var tt = Factor;
            var t2 = myConstantVals.mm;
            float m33 = t2[2, 2];
            float m43 = t2[3, 2];
            outLightQD = pointLight(myConstantVals.mm, myBufferVals.uCamera.xyz,
                myBufferVals.lightPosition.xyz, myBufferVals.lightAmbient,
                myBufferVals.lightDiffuse, myBufferVals.lightSpecular,
                inNormal, pos);
            gl_Position = myConstantVals.mvp * new vec4(pos, 1.0f);
            vTexPosition = (aTexCoor.xy - 0.5f) * 5.0f;
        }

        private vec4 pointLight([In]mat4 uMMatrix, [In]vec3 uCamera, [In]vec3 lightLocation,
            [In]vec4 lightAmbient, [In]vec4 lightDiffuse, [In]vec4 lightSpecular, [In]vec3 normal, 
            [In]vec3 aPosition)
        {
            vec4 ambient; vec4 diffuse; vec4 specular;
            ambient = lightAmbient;
            vec3 normalTarget = aPosition + normal;
            vec3 newNormal = (uMMatrix * new vec4(normalTarget, 1)).xyz - (uMMatrix * new vec4(aPosition, 1)).xyz;
            newNormal = normalize(newNormal);
            vec3 eye = normalize(uCamera - (uMMatrix * new vec4(aPosition, 1)).xyz);
            vec3 vp = normalize(lightLocation - (uMMatrix * new vec4(aPosition, 1)).xyz);
            vp = normalize(vp);
            vec3 halfVector = normalize(vp + eye);
            float shininess = 50.0f;
            float nDotViewPosition = max(0.0f, dot(newNormal, vp));
            diffuse = lightDiffuse * nDotViewPosition;
            float nDotViewHalfVector = dot(newNormal, halfVector);
            float powerFactor = max(0.0f, pow(nDotViewHalfVector, shininess));
            specular = lightSpecular * powerFactor;
            return ambient + diffuse + specular;
        }
    }


    public struct MyVals
    {
        public mat4 mvp;
        public mat4 mm;
    }

    public struct BufferVals
    {
        public vec4 uCamera;
        public vec4 lightPosition;
        public vec4 lightAmbient;
        public vec4 lightDiffuse;
        public vec4 lightSpecular;
    }

    public struct gl_PerVertex
    {
        public vec4 gl_Position;
    }
}
