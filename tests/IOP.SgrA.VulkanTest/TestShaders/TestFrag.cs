﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.VulkanTest
{
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class TestFrag : GLSLFrag
    {
        [ShaderInput(0)]
        public vec2 vTexPosition {  get; set; }
        [ShaderInput(1)]
        public vec4 inLightQD {  get; set; }
        [FragOutput(0, false)]
        public vec4 outColor {  get; set; }
        const float maxIterations = 99.0f;
        const float zoom = 0.02f;
        const float xCenter = -0.0002f;
        const float yCenter = 0.7383f;
        vec3 innerColor = new vec3(0.0f, 0.0f, 1.0f);//内部颜色
        vec3 outerColor1 = new vec3(1.0f, 0.0f, 0.0f);
        vec3 outerColor2 = new vec3(0.0f, 1.0f, 0.0f);

        public override void main()
        {
            float real = vTexPosition.x * zoom + xCenter;//变换当前位置
            float imag = vTexPosition.y * zoom + yCenter;
            float cReal = real;//c的实部
            float cImag = imag;//c的虚部
            float r2 = 0;//半径的平方
            float i;//迭代次数
            for (i = 0; i < maxIterations && r2 < 4.0; i++)
            {
                float tmpReal = real;
                real = (tmpReal * tmpReal) - (imag * imag) + cReal;
                imag = 2.0f * tmpReal * imag + cImag;
                r2 = (real * real) + (imag * imag);
            }
            vec3 color;
            if (r2 < 4.0) color = innerColor;
            else color = mix(outerColor1, outerColor2, fract(i * 0.07f));
            outColor = new vec4(color, 1.0f) * inLightQD;
        }

        
    }
}
