﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.VulkanTest
{
    public partial class TestVert4 : GLSLVert
    {
        [UniformBuffer(3, 0, DescriptorSetTarget.Pipeline)]
        public ConstantVals2 vals { get; set; }

        [VertInput(0, Format.R32G32B32Sfloat)]
        public vec3 pos { get; set; }
        [VertInput(1, Format.R32G32B32Sfloat)]
        public vec3 normal { get; set; }
        [VertInput(2, Format.R32G32B32Sfloat)]
        public vec3 uv { get; set; }
        [ShaderOutput(0)]
        public vec3 localPos { get; set; }
        [ShaderOutput(1)]
        public vec3 outUV { get; set; }
        [ShaderOutput(2)]
        public vec3 reveNormal { get; set; }
        [ShaderOutput(3)]
        public vec3 cPos { get; set; }
        [ShaderOutput(4)]
        public vec4 outPrePos { get; set; }
        [ShaderOutput(5)]
        public vec4 outCurrPos { get; set; }

        public override void main()
        {
            localPos = pos;
            mat4 rotView = new mat4(new mat3(vals.view));
            mat4 prerotView = new mat4(new mat3(vals.preView));
            vec4 clipPos = vals.jitterProject * rotView * new vec4(pos, 1.0f);
            outUV = uv;
            reveNormal = -1 * normal;
            cPos = vals.cameraPos.xyz;
            gl_Position = clipPos.xyww;

            vec4 prePos = vals.projection * vals.preView * new vec4(pos, 1.0f);
            vec4 currPos = vals.projection * vals.view * new vec4(pos, 1.0f);
            outPrePos = prePos;
            outCurrPos = currPos;
        }
    }

    public struct ConstantVals2
    {
        public mat4 projection;
        public mat4 view;
        public mat4 preView;
        public mat4 jitterProject;
        public vec4 cameraPos;
    }
}
