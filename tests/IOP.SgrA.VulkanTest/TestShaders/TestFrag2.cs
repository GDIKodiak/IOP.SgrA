﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.VulkanTest
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class TestFrag2 : GLSLFrag
    {
        const float maxIterations = 999.0f;//最大迭代次数
        const float zoom = 1.0f;//缩放系数
        const float xCenter = 0.0f;//中心x坐标
        const float yCenter = 0.0f;//中心y坐标
        vec3 innerColor = new vec3(0.0f, 0.0f, 1.0f);//内部颜色
        vec3 outerColor1 = new vec3(1.0f, 0.0f, 0.0f);//外部颜色1
        vec3 outerColor2 = new vec3(0.0f, 1.0f, 0.0f);//外部颜色2
        [ShaderInput(0)]
        public vec2 vTexPosition {  get; set; }
        [FragOutput(0, false)]
        public vec4 outColor { get; set; }

        public override void main()
        {
            float real = vTexPosition.x * zoom + xCenter;//变换当前位置
            float imag = vTexPosition.y * zoom + yCenter;
            float cReal = real;//c的实部
            float cImag = imag;//c的虚部
            float r2 = 0.0f;//半径的平方
            float i;//迭代次数
            for (i = 0.0f; i < maxIterations && r2 < 4.0; i++)
            {//循环迭代
                float tmpReal = real;//保存当前实部值
                real = (tmpReal * tmpReal) - (imag * imag) + cReal;//计算下一次迭代后实部的值
                imag = 2.0f * tmpReal * imag + cImag;//计算下一次迭代后虚部的值
                r2 = (real * real) + (imag * imag);//计算半径的平方
            }
            vec3 color;
            if (r2 < 4.0) color = innerColor;
            else color = mix(outerColor1, outerColor2, fract(i * 0.07f));
            outColor = new vec4(color, 1.0f);
        }
    }
}
