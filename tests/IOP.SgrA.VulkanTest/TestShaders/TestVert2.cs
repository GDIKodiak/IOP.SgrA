﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.VulkanTest
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class TestVert2 : GLSLVert
    {
        [PushConstant(64, 0)]
        public ConstantVals myConstantVals { get; set; }

        [VertInput(0, Format.R32G32B32Sfloat)]
        public vec3 pos { get; set; }
        [VertInput(1, Format.R32G32Sfloat)]
        public vec2 inTexCoor { get; set; }
        [ShaderOutput(0)]
        public vec2 outTexCoor { get; set; }

        public override void main()
        {
            outTexCoor = inTexCoor;
            gl_Position = myConstantVals.mvp * new vec4(pos, 1.0f);
        }
    }

    public struct ConstantVals
    {
        public mat4 mvp;
    }
}
