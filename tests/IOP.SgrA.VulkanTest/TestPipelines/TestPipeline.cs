﻿using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.VulkanTest
{
    public partial class TestPipeline : ScriptedVulkanPipeline
    {
        public TestVert Vert { get; set; }
        public TestFrag Frag { get; set; }
    }
}
