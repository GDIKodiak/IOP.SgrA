﻿using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.VulkanTest
{
    [RenderPass("Test", RenderPassMode.Swapchain)]
    [Subpass(0, PipelineBindPoint.Graphics)]
    [Subpass(1, PipelineBindPoint.Graphics)]
    public partial class TestRenderPass : ScriptedVulkanRenderPass
    {
        [Attachment(ImageLayout.ColorAttachmentOptimal, Format.R8G8B8A8Unorm, 
            LoadOp = AttachmentLoadOp.Clear,
            StoreOp = AttachmentStoreOp.Store)]
        [SubpassAttachment(0, SubpassAttachmentType.ColorAttachment, ImageLayout.ColorAttachmentOptimal)]
        [SubpassAttachment(1, SubpassAttachmentType.InputAttachment, ImageLayout.ShaderReadOnlyOptimal)]
        [ImageConfig(ImageType = ImageType.Type2D, ViewType = ImageViewType.Type2D,
            Usage = ImageUsageFlags.ColorAttachmentBit | ImageUsageFlags.InputAttachmentBit)]
        [AttachmentClearValue(0.0f, 0.0f, 0.0f, 0.0f)]
        RenderPassAttachment Color { get; set; }

        [Attachment(ImageLayout.ColorAttachmentOptimal, Format.R32G32B32A32Sfloat, 
            LoadOp = AttachmentLoadOp.Clear, StoreOp = AttachmentStoreOp.Store)]
        [SubpassAttachment(0, SubpassAttachmentType.ColorAttachment, ImageLayout.ColorAttachmentOptimal)]
        [SubpassAttachment(1, SubpassAttachmentType.InputAttachment, ImageLayout.ShaderReadOnlyOptimal)]
        [ImageConfig(ImageType = ImageType.Type2D, ViewType = ImageViewType.Type2D,
            Usage = ImageUsageFlags.ColorAttachmentBit | ImageUsageFlags.InputAttachmentBit)]
        [AttachmentClearValue(0.0f, 0.0f, 0.0f, 0.0f)]
        RenderPassAttachment Albedo { get; set; }

        [Attachment(ImageLayout.ColorAttachmentOptimal, Format.R32G32B32A32Sfloat,
            LoadOp = AttachmentLoadOp.Clear, StoreOp = AttachmentStoreOp.Store)]
        [SubpassAttachment(0, SubpassAttachmentType.ColorAttachment, ImageLayout.ColorAttachmentOptimal)]
        [SubpassAttachment(1, SubpassAttachmentType.InputAttachment, ImageLayout.ShaderReadOnlyOptimal)]
        [ImageConfig(ImageType = ImageType.Type2D, ViewType = ImageViewType.Type2D,
            Usage = ImageUsageFlags.ColorAttachmentBit | ImageUsageFlags.InputAttachmentBit)]
        [AttachmentClearValue(0.0f, 0.0f, 0.0f, 0.0f)]
        RenderPassAttachment MRA { get; set; }

        [Attachment(ImageLayout.ColorAttachmentOptimal, Format.R32G32B32A32Sfloat, 
            LoadOp = AttachmentLoadOp.Clear, StoreOp = AttachmentStoreOp.Store)]
        [SubpassAttachment(0, SubpassAttachmentType.ColorAttachment, ImageLayout.ColorAttachmentOptimal)]
        [SubpassAttachment(1, SubpassAttachmentType.InputAttachment, ImageLayout.ShaderReadOnlyOptimal)]
        [ImageConfig(ImageType = ImageType.Type2D, ViewType = ImageViewType.Type2D,
            Usage = ImageUsageFlags.ColorAttachmentBit | ImageUsageFlags.InputAttachmentBit)]
        [AttachmentClearValue(0.0f, 0.0f, 0.0f, 0.0f)]
        RenderPassAttachment Nromal { get; set; }

        [Attachment(ImageLayout.DepthStencilAttachmentOptimal, Format.D24UnormS8Uint, 
            LoadOp = AttachmentLoadOp.Clear, StoreOp = AttachmentStoreOp.Store)]
        [SubpassAttachment(0, SubpassAttachmentType.DepthStencilAttachment, ImageLayout.DepthStencilAttachmentOptimal)]
        [SubpassAttachment(1, SubpassAttachmentType.DepthStencilAttachment, ImageLayout.DepthStencilAttachmentOptimal)]
        [ImageConfig(ImageType = ImageType.Type2D, ViewType = ImageViewType.Type2D,
            Usage = ImageUsageFlags.DepthStencilAttachmentBit)]
        [AttachmentClearValue(0.0f, 0)]
        [GlobalConfig(UseGlobalSample = true)]
        RenderPassAttachment Depth { get; set; }

        [Attachment(ImageLayout.General, Format.R16G16B16A16Sfloat, 
            LoadOp = AttachmentLoadOp.Clear, StoreOp = AttachmentStoreOp.Store)]
        [SubpassAttachment(1, SubpassAttachmentType.ColorAttachment, ImageLayout.ColorAttachmentOptimal)]
        [ImageConfig(ImageType = ImageType.Type2D, ViewType = ImageViewType.Type2D,
            Usage = ImageUsageFlags.ColorAttachmentBit)]
        [AttachmentClearValue(0.0f, 0.0f, 0.0f, 0.0f)]
        [GlobalConfig(UseGlobalSample = true)]
        RenderPassAttachment Output { get; set; }

        [Attachment(ImageLayout.General, Format.R16G16Sfloat, 
            LoadOp = AttachmentLoadOp.Clear, StoreOp = AttachmentStoreOp.Store)]
        [SubpassAttachment(0, SubpassAttachmentType.ColorAttachment, ImageLayout.ColorAttachmentOptimal)]
        [ImageConfig(ImageType = ImageType.Type2D, ViewType = ImageViewType.Type2D,
            Usage = ImageUsageFlags.ColorAttachmentBit)]
        [AttachmentClearValue(0.0f, 0.0f, 0.0f, 0.0f)]
        RenderPassAttachment MotionVector { get; set; }


        [SubpassDependency(SrcSubpass = uint.MaxValue, DstSubpass = 0,
            SrcStageMask = PipelineStageFlags.TopOfPipeBit, DstStageMask = PipelineStageFlags.ColorAttachmentOutputBit,
            DstAccessMask = AccessFlags.ColorAttachmentWriteBit)]
        PassDependency PassDependency1 { get; set; }
        [SubpassDependency(SrcSubpass = 0, DstSubpass = 1, 
            SrcStageMask = PipelineStageFlags.ColorAttachmentOutputBit, DstStageMask = PipelineStageFlags.FragmentShaderBit, 
            SrcAccessMask = AccessFlags.ColorAttachmentWriteBit, DstAccessMask = AccessFlags.InputAttachmentReadBit)]
        PassDependency PassDependency2 { get; set; }
        [SubpassDependency(SrcSubpass = 1, DstSubpass = uint.MaxValue,
            SrcStageMask = PipelineStageFlags.ColorAttachmentOutputBit, DstStageMask = PipelineStageFlags.BottomOfPipeBit,
            SrcAccessMask = AccessFlags.ColorAttachmentWriteBit, DstAccessMask = 0)]
        PassDependency PassDependency3 { get; set; }
    }
}
