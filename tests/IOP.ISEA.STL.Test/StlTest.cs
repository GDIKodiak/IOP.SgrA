﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.STL.Test
{
    public class StlTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public async Task DecodeAsciiTest()
        {
            try
            {
                var path = @"..\..\..\..\..\media\asciiBunny.stl";
                using Stream stream = File.OpenRead(path);
                var d = await STLCodec.Decode(stream);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [Test]
        public async Task DecodeBinaryTest()
        {
            try
            {
                var path = @"..\..\..\..\..\media\bunny.stl";
                using Stream stream = File.OpenRead(path);
                var d = await STLCodec.Decode(stream);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
    }
}
