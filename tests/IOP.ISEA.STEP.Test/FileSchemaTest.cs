﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.Test
{
    public class FileSchemaTest
    {
        [Test]
        public void ParserTest()
        {
            try
            {
                string test = "FILE_SCHEMA(('AUTOMOTIVE_DESIGN { 1 0 10303 214 1 1 1 1 }'));\r\n";
                ExpressLineParser paser = new(0, test);
                var data = paser.Parse();
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Test]
        public void DecodeTest()
        {
            try
            {
                string test = "FILE_SCHEMA(('AUTOMOTIVE_DESIGN { 1 0 10303 214 1 1 1 1 }'));\r\n";
                ExpressLineParser paser = new(0, test);
                var data = paser.Parse();
                STEPDocument document = new();
                IExpressCodec codec = document.GetEntityCodec(SchemaAP21.FILE_SCHEMA);
                var entity = codec.Decode((data as HeaderEntity).Record, document);
                Assert.IsTrue(entity is FileSchema);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }
    }
}
