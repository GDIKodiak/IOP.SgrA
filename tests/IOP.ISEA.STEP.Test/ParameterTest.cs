﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.Test
{
    public class ParameterTest
    {
        [Test]
        public void InstanceNameParameterTest()
        {
            try
            {
                string test = "#02509=LINE('',#165,#164);\r\n";
                ExpressLineParser paser = new ExpressLineParser(0, test);
                var data = paser.Parse();
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }
    }
}
