﻿using IOP.ISEA.EXPRESS;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.STEP.Test
{
    public class ParseFileTest
    {
        [Test]
        public async Task ParseFileTest1()
        {
            try
            {
                var file = System.IO.Path.Combine(AppContext.BaseDirectory, "Resources", "test1.stp");
                STEPDocument document = new();
                var reader = await EXPRESSFileReader.ReadFromFile(file, document);
                reader.Dispose();
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Test]
        public async Task ForeachEntitiesTest1()
        {
            try
            {
                int i = 0;
                var file = System.IO.Path.Combine(AppContext.BaseDirectory, "Resources", "test1.stp");
                var document = await STEPDocument.CreateFromFile(file);
                foreach(var item in document.ForeachEntities())
                {
                    i++;
                }
                Debug.WriteLine(i);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }
    }
}
