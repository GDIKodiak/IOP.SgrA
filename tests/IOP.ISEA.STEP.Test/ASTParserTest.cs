﻿using IOP.ISEA.EXPRESS;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.STEP.Test
{
    public class ASTParserTest
    {
        [Test]
        public void DecodeTest1()
        {
            try
            {
                string line = "#53614=DESCRIPTIVE_REPRESENTATION_ITEM('PRODUCT_NO','....');\r\n";
                ExpressLineParser parser = new ExpressLineParser(0, line);
                var node = parser.Parse();
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }
    }
}
