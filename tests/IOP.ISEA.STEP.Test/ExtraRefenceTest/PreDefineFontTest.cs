﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IOP.ISEA.EXPRESS;
using IOP.ISEA.STEP.Entities;

namespace IOP.ISEA.STEP.Test.ExtraRefenceTest
{
    public class PreDefineFontTest
    {
        [Test]
        [TestCase("#147=DRAUGHTING_PRE_DEFINED_CURVE_FONT('continuous');\r\n")]
        public void DraughtingPreDefinedCurveFontParserTest(string test)
        {
            try
            {
                ExpressLineParser paser = new(0, test);
                var data = paser.Parse();
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Test]
        [TestCase("#147=DRAUGHTING_PRE_DEFINED_CURVE_FONT('continuous');\r\n")]
        public void DraughtingPreDefinedCurveFontDecodeTest(string test)
        {
            try
            {
                STEPDocument document = new();
                document.ParseLine(0, test);
                var entity = document.GetEntity(147);
                Assert.IsTrue(entity is DraughtingPreDefinedCurveFont);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }
    }
}
