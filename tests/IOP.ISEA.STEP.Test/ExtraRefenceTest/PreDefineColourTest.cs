﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IOP.ISEA.EXPRESS;
using IOP.ISEA.STEP.Entities;

namespace IOP.ISEA.STEP.Test.ExtraRefenceTest
{
    public class PreDefineColourTest
    {
        [Test]
        [TestCase("#1=DRAUGHTING_PRE_DEFINED_COLOUR('black');\r\n")]
        public void DraughtingPreDefinedColourTest(string test)
        {
            try
            {
                ExpressLineParser paser = new(0, test);
                var data = paser.Parse();
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Test]
        [TestCase("#1=DRAUGHTING_PRE_DEFINED_COLOUR('black');\r\n")]
        public void DraughtingPreDefinedColourDecodeTest(string test)
        {
            try
            {
                STEPDocument document = new();
                document.ParseLine(0, test);
                var entity = document.GetEntity(1);
                Assert.IsTrue(entity is DraughtingPreDefinedColour);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }
    }
}
