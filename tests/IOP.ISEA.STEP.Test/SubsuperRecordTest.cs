﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.Test
{
    public class SubsuperRecordTest
    {
        [Test]
        public void EmptyComplexEntityTest()
        {
            try
            {
                string test = "#182740=();\r\n";
                ExpressLineParser paser = new ExpressLineParser(0, test);
                var data = paser.Parse();
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Test]
        public void SimgleSubsuperRecordTest()
        {
            try
            {
                string test = "#24660=(BOUNDED_SURFACE());\r\n";
                ExpressLineParser paser = new ExpressLineParser(0, test);
                var data = paser.Parse();
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Test]
        public void DoubleSubsuperRecordTest()
        {
            try
            {
                string test = "#24660=(BOUNDED_SURFACE()B_SPLINE_SURFACE(3,3,((#24632,#24633,#24634,#24635),(\r\n" +
                    "#24636,#24637,#24638,#24639),(#24640,#24641,#24642,#24643),(#24644,#24645,\r\n" +
                    "#24646,#24647),(#24648,#24649,#24650,#24651),(#24652,#24653,#24654,#24655),(\r\n" +
                    "#24656,#24657,#24658,#24659)),.UNSPECIFIED.,.F.,.F.,.F.));\r\n";
                ExpressLineParser paser = new ExpressLineParser(0, test);
                var data = paser.Parse();
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }
    }
}
