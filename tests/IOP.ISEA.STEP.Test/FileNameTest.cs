﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.Test
{
    public class FileNameTest
    {
        [Test]
        public void ParserTest()
        {
            try
            {
                string test = "FILE_NAME('T6-602S_MA_ASM','2018-04-25T',('5008842'),(''),\r\n" +
                    "'TEST PADWEFW BY ADWF IAWFF, 001215','TEST ADWGAWG BY AWGAW INC, 154811','');\r\n";
                ExpressLineParser paser = new ExpressLineParser(0, test);
                var data = paser.Parse();
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Test]
        public void DecodeTest()
        {
            try
            {
                string test = "FILE_NAME('T6-602S_MA_ASM','2018-04-25T',('5008842'),(''),\r\n" +
                    "'CREO PARAMETRIC BY PTC INC, 2016050','CREO PARAMETRIC BY PTC INC, 2016050','');\r\n";
                ExpressLineParser paser = new(0, test);
                var data = paser.Parse();
                STEPDocument document = new();
                IExpressCodec codec = document.GetEntityCodec(SchemaAP21.FILE_NAME);
                var entity = codec.Decode((data as HeaderEntity).Record, document);
                Assert.IsTrue(entity is FileName);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }
    }
}
