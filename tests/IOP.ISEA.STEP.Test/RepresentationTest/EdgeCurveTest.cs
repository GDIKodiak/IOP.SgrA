﻿using IOP.ISEA.EXPRESS;
using IOP.ISEA.STEP.Entities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.STEP.Test.RepresentationTest
{
    public class EdgeCurveTest
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="test"></param>
        [Test]
        public async Task EdgeCurveTest1()
        {
            try
            {
                var file = System.IO.Path.Combine(AppContext.BaseDirectory, "Resources", "test2.stp");
                var document = await STEPDocument.CreateFromFile(file);
                Entity entity = document.GetEntity(22524);
                Assert.IsTrue(entity is EdgeCurve edge);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }
        [Test]
        public async Task OrientedEdgeTest1()
        {
            try
            {
                var file = System.IO.Path.Combine(AppContext.BaseDirectory, "Resources", "test2.stp");
                var document = await STEPDocument.CreateFromFile(file);
                Entity entity = document.GetEntity(22525);
                Assert.IsTrue(entity is OrientedEdge edge);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }
    }
}
