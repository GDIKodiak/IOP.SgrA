﻿using IOP.ISEA.EXPRESS;
using IOP.ISEA.STEP.Entities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.STEP.Test.RepresentationTest
{
    public class VertePointTest
    {
        [Test]
        public async Task VertePoint1()
        {
            try
            {
                var file = System.IO.Path.Combine(AppContext.BaseDirectory, "Resources", "test2.stp");
                var document = await STEPDocument.CreateFromFile(file);
                Entity entity = document.GetEntity(18195);
                Assert.IsTrue(entity is VertexPoint knots);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }
    }
}
