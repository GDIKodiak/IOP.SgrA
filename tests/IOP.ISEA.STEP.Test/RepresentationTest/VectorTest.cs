﻿using IOP.ISEA.STEP.Entities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.STEP.Test.RepresentationTest
{
    public class VectorTest
    {
        [Test]
        public void DecodeTest()
        {
            try
            {
                string line0 = "#163=DIRECTION('',(0.E0,2.615950099218E-2,-9.996577816972E-1));\r\n";
                string line1 = "#164=VECTOR('',#163,2.881865210173E0);\r\n";
                STEPDocument document = new STEPDocument();
                document.ParseLine(0, line0); 
                document.ParseLine(1, line1);
                var entity = document.GetEntity(164);
                Assert.IsTrue(entity is Vector);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }
    }
}
