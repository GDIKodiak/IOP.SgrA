﻿using IOP.ISEA.STEP.Entities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.STEP.Test.RepresentationTest
{
    public class StyledItemTest
    {
        [Test]
        public void DecodeTest1()
        {
            try
            {
                string line0 = "#143=CARTESIAN_POINT('',(0.E0,0.E0,0.E0));\r\n";
                string line1 = "#144=DIRECTION('',(0.E0,1.E0,0.E0));\r\n";
                string line2 = "#145=DIRECTION('',(1.E0,0.E0,0.E0));\r\n";
                string line3 = "#146=AXIS2_PLACEMENT_3D('DEF_CS',#143,#144,#145);\r\n";
                string line4 = "#147=DRAUGHTING_PRE_DEFINED_CURVE_FONT('continuous');\r\n";
                string line5 = "#148=CURVE_STYLE('',#147,POSITIVE_LENGTH_MEASURE(2.E-2),#123);\r\n";
                string line6 = "#149=PRESENTATION_STYLE_ASSIGNMENT((#148));\r\n";
                string line7 = "#150=STYLED_ITEM('',(#149),#146);\r\n";
                string line8 = "#123=DRAUGHTING_PRE_DEFINED_COLOUR('yellow');\r\n";
                STEPDocument document = new STEPDocument();
                document.ParseLines(0, line0, line1, line2, line3, line4, line5, line6, line7, line8);
                var entity = document.GetEntity(150);
                Assert.IsTrue(entity is StyledItem);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }
    }
}
