﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IOP.ISEA.EXPRESS;
using IOP.ISEA.STEP.Entities;

namespace IOP.ISEA.STEP.Test.RepresentationTest
{
    public class Placement3DTest
    {
        [Test]
        [TestCase("#146=AXIS2_PLACEMENT_3D('DEF_CS',#143,#144,#145);\r\n")]
        public void ParserTest(string test)
        {
            try
            {
                ExpressLineParser paser = new(0, test);
                var data = paser.Parse();
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Test]
        public void DecodeTest()
        {
            try
            {
                string line0 = "#158=CARTESIAN_POINT('',(-9.75E1,-1.5E0,5.85E1));\r\n";
                string line1 = "#159=DIRECTION('',(1.E0,0.E0,0.E0));\r\n";
                string line2 = "#160=DIRECTION('',(0.E0,-3.438090412010E-2,9.994088019584E-1));\r\n";
                string source = "#161=AXIS2_PLACEMENT_3D('',#158,#159,#160);\r\n";
                STEPDocument document = new STEPDocument();
                document.ParseLine(0, line0);
                document.ParseLine(1, line1);
                document.ParseLine(2, line2);
                document.ParseLine(3, source);
                var entity = document.GetEntity(161);
                Assert.IsTrue(entity is Axis2Placement3D);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }
    }
}
