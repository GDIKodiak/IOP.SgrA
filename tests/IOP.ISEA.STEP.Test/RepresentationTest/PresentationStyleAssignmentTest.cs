﻿using IOP.ISEA.STEP.Entities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.STEP.Test.RepresentationTest
{
    public class PresentationStyleAssignmentTest
    {
        [Test]
        public void DeocdeTest1()
        {
            try
            {
                string line0 = "#123=DRAUGHTING_PRE_DEFINED_COLOUR('yellow');\r\n";
                string line1 = "#147=DRAUGHTING_PRE_DEFINED_CURVE_FONT('continuous');\r\n";
                string line2 = "#148=CURVE_STYLE('',#147,POSITIVE_LENGTH_MEASURE(2.E-2),#123);\r\n";
                string line3 = "#149=PRESENTATION_STYLE_ASSIGNMENT((#148));\r\n";
                STEPDocument document = new STEPDocument();
                document.ParseLines(0, line0, line1, line2, line3);
                var entity = document.GetEntity(149);
                Assert.IsTrue(entity is PresentationStyleAssignment);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }
    }
}
