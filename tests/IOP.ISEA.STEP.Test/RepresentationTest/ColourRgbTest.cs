﻿using IOP.ISEA.STEP.Entities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.STEP.Test.RepresentationTest
{
    public class ColourRgbTest
    {
        [Test]
        public void DecodeTest1()
        {
            try
            {
                string line0 = "#124=COLOUR_RGB('',1.E0,1.E0,2.5E-1);\r\n";
                STEPDocument document = new STEPDocument();
                document.ParseLine(0, line0);
                var entity = document.GetEntity(124);
                Assert.IsTrue(entity is ColourRgb);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }
    }
}
