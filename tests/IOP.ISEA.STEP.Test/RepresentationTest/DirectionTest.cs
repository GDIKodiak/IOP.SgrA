﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IOP.ISEA.EXPRESS;
using IOP.ISEA.STEP.Entities;

namespace IOP.ISEA.STEP.Test.RepresentationTest
{
    public class DirectionTest
    {
        [Test]
        [TestCase("#174=DIRECTION('',(0.E0,-9.996577816973E-1,-2.615950099110E-2));\r\n")]
        public void ParserTest(string test)
        {
            try
            {
                ExpressLineParser paser = new(0, test);
                var data = paser.Parse();
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Test]
        [TestCase("#227=DIRECTION('',(0.E0,-4.771741140811E-1,8.788087760434E-1));\r\n")]
        public void DecodeTest(string test)
        {
            try
            {
                ExpressLineParser paser = new(0, test);
                var data = paser.Parse();
                STEPDocument document = new();
                IExpressCodec codec = document.GetEntityCodec(Schema.DIRECTION);
                var entity = codec.Decode((data as EntityInstance).Record, document);
                Assert.IsTrue(entity is Direction);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }
    }
}
