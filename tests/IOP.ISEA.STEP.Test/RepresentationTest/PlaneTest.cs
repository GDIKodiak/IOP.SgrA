﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IOP.ISEA.EXPRESS;
using IOP.ISEA.STEP.Entities;

namespace IOP.ISEA.STEP.Test.RepresentationTest
{
    public class PlaneTest
    {
        [Test]
        [TestCase("#137=PLANE('ASSY_HOR',#136);\r\n")]
        public void ParserTest(string test)
        {
            try
            {
                ExpressLineParser paser = new(0, test);
                var data = paser.Parse();
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Test]
        public void DecodeTest()
        {
            try
            {
                string line0 = "#133=CARTESIAN_POINT('',(0.E0,0.E0,0.E0));\r\n";
                string line1 = "#134=DIRECTION('',(0.E0,1.E0,0.E0));\r\n";
                string line2 = "#135=DIRECTION('',(0.E0,0.E0,1.E0));\r\n";
                string line3 = "#136=AXIS2_PLACEMENT_3D('',#133,#134,#135);\r\n";
                string line4 = "#137=PLANE('ASSY_HOR',#136);\r\n";
                STEPDocument document = new STEPDocument();
                document.ParseLine(0, line0); document.ParseLine(1, line1);
                document.ParseLine(2, line2); document.ParseLine(3, line3);
                document.ParseLine(4, line4);
                var entity = document.GetEntity(137);
                Assert.IsTrue(entity is Plane);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }
    }
}
