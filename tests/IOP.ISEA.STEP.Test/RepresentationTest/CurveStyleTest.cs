﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IOP.ISEA.EXPRESS;
using IOP.ISEA.STEP.Entities;

namespace IOP.ISEA.STEP.Test.RepresentationTest
{
    public class CurveStyleTest
    {
        [Test]
        public void DecodeTest()
        {
            try
            {
                string line0 = "#147=DRAUGHTING_PRE_DEFINED_CURVE_FONT('continuous');\r\n";
                string line1 = "#148=CURVE_STYLE('',#147,$,$);\r\n";
                STEPDocument document = new STEPDocument();
                document.ParseLine(0, line0);
                document.ParseLine(1, line1);
                var entity = document.GetEntity(148);
                Assert.IsTrue(entity is CurveStyle);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }
        [Test]
        public void DecodeTest2()
        {
            try
            {
                string line0 = "#123=DRAUGHTING_PRE_DEFINED_COLOUR('yellow');\r\n";
                string line1 = "#147=DRAUGHTING_PRE_DEFINED_CURVE_FONT('continuous');\r\n";
                string line2 = "#148=CURVE_STYLE('',#147,POSITIVE_LENGTH_MEASURE(2.E-2),#123);\r\n";
                STEPDocument document = new STEPDocument();
                document.ParseLine(0, line0);
                document.ParseLine(1, line1);
                document.ParseLine(2, line2);
                var entity = document.GetEntity(148);
                Assert.IsTrue(entity is CurveStyle);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }
    }
}
