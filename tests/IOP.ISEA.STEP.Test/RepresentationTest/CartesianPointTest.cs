﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IOP.ISEA.EXPRESS;
using IOP.ISEA.STEP.Entities;

namespace IOP.ISEA.STEP.Test.RepresentationTest
{
    public class CartesianPointTest
    {
        [Test]
        [TestCase("#158=CARTESIAN_POINT('',(-9.75E1,-1.5E0,5.85E1));\r\n")]
        public void ParserTest(string test)
        {
            try
            {
                ExpressLineParser paser = new(0, test);
                var data = paser.Parse();
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Test]
        [TestCase("#172=CARTESIAN_POINT('',(-9.75E1,-1.726971338685E0,6.599439102454E1));\r\n")]
        public void DecodeTest(string test)
        {
            try
            {
                ExpressLineParser paser = new(0, test);
                var data = paser.Parse();
                STEPDocument document = new();
                IExpressCodec codec = document.GetEntityCodec(Schema.CARTESIAN_POINT);
                var entity = codec.Decode((data as EntityInstance).Record, document);
                Assert.IsTrue(entity is CartesianPoint);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }
    }
}
