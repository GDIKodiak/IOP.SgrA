﻿using IOP.ISEA.STEP.Entities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.STEP.Test.RepresentationTest
{
    public class LineTest
    {
        [Test]
        public void DecodeTest()
        {
            try
            {
                string line0 = "#163=DIRECTION('',(0.E0,2.615950099218E-2,-9.996577816972E-1));\r\n";
                string line1 = "#164=VECTOR('',#163,2.881865210173E0);\r\n";
                string line2 = "#165=CARTESIAN_POINT('',(-9.75E1,-1.704316546275E0,6.512866199050E1));\r\n";
                string line3 = "#166=LINE('',#165,#164);\r\n";
                STEPDocument document = new STEPDocument();
                document.ParseLines(0, line0, line1, line2, line3);
                var entity = document.GetEntity(166);
                Assert.IsTrue(entity is Line);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }
    }
}
