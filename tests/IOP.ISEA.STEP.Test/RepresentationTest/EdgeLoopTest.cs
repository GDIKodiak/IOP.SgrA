﻿using IOP.ISEA.EXPRESS;
using IOP.ISEA.STEP.Entities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.STEP.Test.RepresentationTest
{
    public class EdgeLoopTest
    {
        [Test]
        public async Task EdgeLoopTest1()
        {
            try
            {
                var file = System.IO.Path.Combine(AppContext.BaseDirectory, "Resources", "test2.stp");
                var document = await STEPDocument.CreateFromFile(file);
                Entity entity = document.GetEntity(22574);
                Assert.IsTrue(entity is EdgeLoop loop);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }
    }
}
