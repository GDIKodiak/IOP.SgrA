﻿using IOP.ISEA.STEP.Entities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.STEP.Test.RepresentationTest
{
    public class CircleTest
    {
        [Test]
        public void DeocdeTest1()
        {
            try
            {
                string line0 = "#6270=CIRCLE('',#6269,7.000000000869E0);\r\n";
                string line1 = "#6269=AXIS2_PLACEMENT_3D('',#6266,#6267,#6268);\r\n";
                string line2 = "#6266=CARTESIAN_POINT('',(2.26E1,4.494693746407E2,5.155489436169E1));\r\n";
                string line3 = "#6267=DIRECTION('',(1.E0,-6.960438575530E-13,1.306533225242E-14));\r\n";
                string line4 = "#6268=DIRECTION('',(5.242790328207E-13,7.653125198940E-1,6.436588746327E-1));\r\n";
                STEPDocument document = new STEPDocument();
                document.ParseLines(0, line0, line1, line2, line3, line4);
                var entity = document.GetEntity(6270);
                Assert.IsTrue(entity is Circle);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }
    }
}
