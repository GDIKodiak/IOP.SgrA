﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.Test
{
    public class NamesMappingTest
    {
        [Test]
        public void EncodeTest()
        {
            try
            {
                NamesMappingNode node = new NamesMappingNode();
                NamesMappingNodeCodec codec = new NamesMappingNodeCodec();
                ISEADocument document = new ISEADocument();
                node.NameMappings.Add(new NamesMapping() { NodeName = "TESTA", NodeId = 1 });
                node.NameMappings.Add(new NamesMapping() { NodeName = "TESTBB", NodeId = 2 });
                node.NameMappings.Add(new NamesMapping() { NodeName = "TESTCCC", NodeId = 3 });
                node.NameMappings.Add(new NamesMapping() { NodeName = "TESTDDDD", NodeId = 4 });
                node.NameMappings.Add(new NamesMapping() { NodeName = "TESTEEEEE", NodeId = 5 });
                var memory = codec.Encode(node, document);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message + "\r\n" + e.StackTrace);
                throw;
            }
        }

        [Test]
        public void DecodeTest()
        {
            try
            {
                NamesMappingNode node = new NamesMappingNode();
                NamesMappingNodeCodec codec = new NamesMappingNodeCodec();
                ISEADocument document = new ISEADocument();
                node.NameMappings.Add(new NamesMapping() { NodeName = "TESTA", NodeId = 1 });
                node.NameMappings.Add(new NamesMapping() { NodeName = "TESTBB", NodeId = 2 });
                node.NameMappings.Add(new NamesMapping() { NodeName = "TESTCCC", NodeId = 3 });
                node.NameMappings.Add(new NamesMapping() { NodeName = "TESTDDDD", NodeId = 4 });
                node.NameMappings.Add(new NamesMapping() { NodeName = "TESTEEEEE", NodeId = 5 });
                var memory = codec.Encode(node, document);
                NamesMappingNode node2 = codec.Decode(memory, document) as NamesMappingNode;
                Assert.That(IsSame(node, node2));
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message + "\r\n" + e.StackTrace);
                throw;
            }
        }


        private bool IsSame(NamesMappingNode node1, NamesMappingNode node2)
        {
            bool r = true;
            var name1 = node1.NameMappings;
            var name2 = node2.NameMappings;
            for(int i = 0; i < name1.Count; i++)
            {
                if (name1[i].NodeId != name2[i].NodeId) r = false;
                if (name1[i].NodeName.Data != name2[i].NodeName.Data) r = false;
            }
            return r;
        }
    }
}
