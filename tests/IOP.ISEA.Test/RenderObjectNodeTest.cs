﻿using IOP.ISEA.OBJ;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace IOP.ISEA.Test
{
    public class RenderObjectNodeTest
    {
        [Test]
        public async Task EncodeTest()
        {
            try
            {
                var obj = await GetNode();
                ISEADocument document = new ISEADocument();
                RenderObjectNodeCodec codec = new RenderObjectNodeCodec();
                Memory<byte> r = codec.Encode(obj, document);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message + "\r\n" + e.StackTrace);
                throw;
            }
        }

        [Test]
        public async Task DecodeTest()
        {
            try
            {
                var obj = await GetNode();
                ISEADocument document = new ISEADocument();
                RenderObjectNodeCodec codec = new RenderObjectNodeCodec();
                Memory<byte> r = codec.Encode(obj, document);
                var obj2 = codec.Decode(r, document);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message + "\r\n" + e.StackTrace);
                throw;
            }
        }

        private async Task<RenderObjectNode> GetNode()
        {
            var texPath = Path.Combine(AppContext.BaseDirectory, "Resources", "TestImage.jpg");
            var objPath = Path.Combine(AppContext.BaseDirectory, "Resources", "car.obj");
            var file = new FileInfo(texPath);
            var extension = file.Extension.Replace(".", "");
            byte[] image = File.ReadAllBytes(texPath);
            ImageNode node = new ImageNode()
            {
                Extension = extension,
                ImageData = DataArray<byte>.Create(image)
            };
            JsonDocument document1 = JsonDocument.Parse(File.ReadAllText(Path.Combine(AppContext.BaseDirectory, "Resources", "test.json")));
            var config = document1.RootElement.GetProperty("Image").GetRawText();
            ConfigNode configNode = new ConfigNode()
            {
                Name = "Test1",
                Engine = "Vulkan",
                Type = "JSON",
                Config = config
            };
            TextureNode textureNode = new TextureNode()
            {
                Name = "Image",
                Image = node,
                TexConfig = configNode
            };

            using FileStream objFile = File.OpenRead(objPath);
            var obj = await OBJCodec.Decode(objFile);
            obj.CombineData(out Vector3[] data, 0, 0, true, true);
            Memory<byte> v = Cast(data);
            MeshNode mNode = new MeshNode();
            MeshNodeCodec codec = new MeshNodeCodec();
            mNode.VerticesComponentSequence = new List<int>() { 0 };
            mNode.VerticesComponents = new List<MeshData>()
            {
                new MeshData() { Data = DataArray<byte>.Create(v.ToArray()), DataType = MeshDataType.COMPLEX, Purpose = MeshDataPurpose.COMPLEX, Stride = 36, IsCompressed = 1 }
            };
            mNode.VerticesStride = 36;
            mNode.VerticesCount = v.Length / 9;

            RenderObjectNode r = new RenderObjectNode();
            r.Name = "C1024DWAF2";
            r.Position = new Vector3(10, 20, 30);
            r.Scale = new Vector3(0.5f, 0.5f, 0.5f);
            r.Comonents = new List<Node>() { mNode, textureNode };
            return r;
        }

        private Memory<byte> Cast(Vector3[] vectors)
        {
            var r = MemoryMarshal.AsBytes(new ReadOnlySpan<Vector3>(vectors));
            return new Memory<byte>(r.ToArray());
        }
    }
}
