﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.Test
{
    public class AsciiStringTest
    {
        [Test]
        public void EncodeTest()
        {
            try
            {
                string data = "JSON";
                Span<byte> d = Encoding.ASCII.GetBytes(data);
                int l = d.Length;
                Span<byte> local = new byte[d.Length + Constant.BYTELENGTH];
                local[0] = (byte)l;
                d.CopyTo(local[Constant.BYTELENGTH..]);
                ReadOnlySpan<byte> r = local;
                (AsciiString s, short ll) = AsciiString.Create(r);
                Assert.That(s.ToString(), Is.EqualTo(data));
                Assert.That(ll, Is.EqualTo(local.Length));
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message + "\r\n" + e.StackTrace);
                throw;
            }
        }

        [Test]
        public void DecodeTest()
        {
            try
            {
                string data = "JSON";
                Span<byte> d = Encoding.ASCII.GetBytes(data);
                int l = d.Length;
                Span<byte> local = new byte[d.Length + Constant.BYTELENGTH];
                local[0] = (byte)l;
                d.CopyTo(local[Constant.BYTELENGTH..]);
                ReadOnlySpan<byte> r = local;
                (AsciiString s, short ll) = AsciiString.Create(r);
                Span<byte> local2 = new byte[d.Length + Constant.BYTELENGTH];
                s.WriteTo(local2);
                for(int i = 0; i < local2.Length; i++)
                {
                    Assert.That(local2[i] == local[i]);
                }
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message + "\r\n" + e.StackTrace);
                throw;
            }
        }
    }
}
