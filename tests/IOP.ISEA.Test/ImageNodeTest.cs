﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.Test
{
    public class ImageNodeTest
    {
        [Test]
        public void EncodeTest()
        {
            try
            {
                var path = Path.Combine(AppContext.BaseDirectory, "Resources", "TestImage.jpg");
                var file = new FileInfo(path);
                var extension = file.Extension.Replace(".", "");
                byte[] image = File.ReadAllBytes(path);
                ISEADocument document = new ISEADocument();
                ImageNode node = new ImageNode()
                {
                    Extension = extension,
                    ImageData = DataArray<byte>.Create(image),
                    Name = "TTTT"
                };
                node.ImageData.IsCompressed = 1;
                ImageNodeCodec codec = new ImageNodeCodec();
                Memory<byte> memory = codec.Encode(node, document);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message + "\r\n" + e.StackTrace);
                throw;
            }
        }

        [Test]
        public void DecodeTest()
        {
            try
            {
                var path = Path.Combine(AppContext.BaseDirectory, "Resources", "TestImage.jpg");
                var file = new FileInfo(path);
                var extension = file.Extension.Replace(".", "");
                byte[] image = File.ReadAllBytes(path);
                ISEADocument document = new ISEADocument();
                ImageNode node = new ImageNode()
                {
                    Extension = extension,
                    ImageData = DataArray<byte>.Create(image),
                    Name = "TTTT"
                };
                node.ImageData.IsCompressed = 1;
                ImageNodeCodec codec = new ImageNodeCodec();
                Memory<byte> memory = codec.Encode(node, document);
                ImageNode node2 = codec.Decode(memory, document) as ImageNode;
                Assert.That(IsSame(node, node2));
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message + "\r\n" + e.StackTrace);
                throw;
            }
        }

        private bool IsSame(ImageNode node1, ImageNode node2)
        {
            var r = true;
            if (node1.Extension.Data != node2.Extension.Data) r = false;
            if (node1.Extension.Length != node2.Extension.Length) r = false;
            if (node1.ImageData.ArrayLength != node2.ImageData.ArrayLength) r = false;
            if (node1.ImageData.ArrayType != node2.ImageData.ArrayType) r = false;
            if (node1.ImageData.DataSize != node2.ImageData.DataSize) r = false;
            if (node1.Name.ToString() != node2.Name.ToString()) r = false;
            for(int i = 0; i < node1.ImageData.Arrays.Length; i++)
            {
                if(node1.ImageData.Arrays[i] != node2.ImageData.Arrays[i])
                {
                    r = false;
                    break;
                }
            }
            return r;
        }
    }
}
