﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.Test
{
    public class Utf8StringTest
    {
        [Test]
        public void EncodeTest()
        {
            try
            {
                Span<byte> d = Encoding.UTF8.GetBytes(data);
                int l = d.Length;
                Span<byte> local = new byte[d.Length + Constant.INTLENGTH];
                MemoryMarshal.Write(local, ref l);
                d.CopyTo(local.Slice(Constant.INTLENGTH));
                ReadOnlySpan<byte> r = local;
                (Utf8String s, int ll) = Utf8String.Create(r);
                Assert.That(s.ToString(), Is.EqualTo(data));
                Assert.That(ll, Is.EqualTo(local.Length));
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message + "\r\n" + e.StackTrace);
                throw;
            }
        }

        [Test]
        public void DecodeTest()
        {
            try
            {
                Span<byte> d = Encoding.UTF8.GetBytes(data);
                int l = d.Length;
                Span<byte> local = new byte[d.Length + Constant.INTLENGTH];
                MemoryMarshal.Write(local, ref l);
                d.CopyTo(local.Slice(Constant.INTLENGTH));
                ReadOnlySpan<byte> r = local;
                (Utf8String s, int ll) = Utf8String.Create(r);
                Span<byte> local2 = new byte[d.Length + Constant.INTLENGTH];
                s.WriteTo(local2);
                for(int i = 0; i < local2.Length; i++)
                {
                    Assert.That(local[i] == local2[i]);
                }
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message + "\r\n" + e.StackTrace);
                throw;
            }
        }

        private const string data = @"{ ApplicationOption"": { ""ApplicationName"": ""HelloVulkan"", ""ApplicationVersion"": ""1.0.0"",
""EngineName"": ""HelloVulkan"", ""EngineVersion"": ""1.0.0"", ""ApiVersion"": ""1.0.0"" } }";
    }
}
