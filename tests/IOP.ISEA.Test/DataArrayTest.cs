﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.Test
{
    public class DataArrayTest
    {
        [Test]
        public void EncodeTest()
        {
            try
            {
                //var path = Path.Combine(AppContext.BaseDirectory, "Resources", "TestImage.jpg");
                //Bitmap bitmap = new(path);
                //var im = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, PixelFormat.Format32bppRgb);
                //int stride = im.Stride;
                //int length = stride * bitmap.Height;
                //byte[] temp = new byte[stride * bitmap.Height];
                //Marshal.Copy(im.Scan0, temp, 0, temp.Length);
                //bitmap.UnlockBits(im);
                //DataArray<byte> array = new()
                //{
                //    IsCompressed = 1,
                //    Arrays = temp
                //};
                //Span<byte> r = array.ToSpan();
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message + "\r\n" + e.StackTrace);
                throw;
            }
        }

        [Test]
        public void DecodeTest()
        {
            try
            {
                //var path = Path.Combine(AppContext.BaseDirectory, "Resources", "TestImage.jpg");
                //Bitmap bitmap = new(path);
                //var im = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, PixelFormat.Format32bppRgb);
                //int stride = im.Stride;
                //int length = stride * bitmap.Height;
                //byte[] temp = new byte[stride * bitmap.Height];
                //Marshal.Copy(im.Scan0, temp, 0, temp.Length);
                //bitmap.UnlockBits(im);
                //DataArray<byte> array = DataArray<byte>.Create(temp); array.IsCompressed = 1;
                //Span<byte> r = array.ToSpan(); ReadOnlySpan<byte> input = r;
                //(DataArray<byte> array2, int l) = DataArray<byte>.Create(input);
                //Assert.IsTrue(IsSame(array, array2));
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message + "\r\n" + e.StackTrace);
                throw;
            }
        }

        [Test]
        public void EmptyTest()
        {
            try
            {
                DataArray<byte> array = DataArray<byte>.Empty;
                array.IsCompressed = 1;
                Span<byte> r = array.ToSpan(); ReadOnlySpan<byte> input = r;
                (DataArray<byte> array2, int l) = DataArray<byte>.Create(input);
                Assert.That(IsSame(array, array2));
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message + "\r\n" + e.StackTrace);
                throw;
            }
        }

        [Test]
        public void DecodeTest2()
        {
            try
            {
                //var path = Path.Combine(AppContext.BaseDirectory, "Resources", "TestImage.jpg");
                //Bitmap bitmap = new(path);
                //var im = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, PixelFormat.Format32bppRgb);
                //int stride = im.Stride;
                //int length = stride * bitmap.Height;
                //byte[] temp = new byte[stride * bitmap.Height];
                //Marshal.Copy(im.Scan0, temp, 0, temp.Length);
                //bitmap.UnlockBits(im);
                //DataArray<byte> array = DataArray<byte>.Create(temp);
                //Span<byte> r = array.ToSpan(); ReadOnlySpan<byte> input = r;
                //(DataArray<byte> array2, int l) = DataArray<byte>.Create(input);
                //Assert.IsTrue(IsSame(array, array2));
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message + "\r\n" + e.StackTrace);
                throw;
            }
        }

        private bool IsSame<TData>(DataArray<TData> d1, DataArray<TData> d2)
            where TData : struct
        {
            bool r = true;
            if (d1.ArrayLength != d2.ArrayLength) r = false;
            if (d1.ArrayType != d2.ArrayType) r = false;
            if (d1.DataSize != d2.DataSize) r = false;
            for(int i = 0; i < d1.Arrays.Length; i++)
            {
                TData l = d2.Arrays[i];
                if(!d1.Arrays[i].Equals(l))
                {
                    r = false;
                    break;
                }
            }
            return r;
        }
    }
}
