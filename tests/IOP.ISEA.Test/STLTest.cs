﻿using IOP.ISEA.STL;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.Test
{
    public class STLTest
    {
        [Test]
        public async Task DncodeTest()
        {
			try
			{
                var path = Path.Combine(AppContext.BaseDirectory, "Resources", "base_link.stl");
                STLDocument document = await STLDocument.LoadAsync(path);
                IndexesData data = document.CreateAdjTriangleSequence();
            }
			catch (Exception e)
			{
                Assert.Fail(e.Message);
			}
        }
    }
}
