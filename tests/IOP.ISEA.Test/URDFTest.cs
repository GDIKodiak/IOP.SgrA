﻿using IOP.ISEA.URDF;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.Test
{
    public class URDFTest
    {
        public const string URDFFILE = "E:\\Projects\\kr20_18urdf\\urdf\\kr1.urdf";

        [Test]
        public void ParseTest()
        {
            try
            {
                URDFDocument document = URDFDocument.ParseFromFile(URDFFILE);
                Assert.That(document.Links.Count > 0);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [Test]
        public void GenerateSystemTest()
        {
            try
            {
                URDFDocument document = URDFDocument.ParseFromFile(URDFFILE);
                var system = document.GenerateSystem();
                Debug.WriteLine(system.Joints["joint1"].VS.ToString());
                Debug.WriteLine(system.Joints["joint2"].VS.ToString());
                Debug.WriteLine(system.Joints["joint3"].VS.ToString());
                var r = system.FKinSpace(document.Joints[2].Name, [0.5, 0.6, 0]);
                Debug.Write(r.ToString());
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
    }
}
