﻿using IOP.ISEA.OBJ;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace IOP.ISEA.Test
{
    public class LoadTest
    {
        [Test]
        public async Task LoadTest1()
        {
            try
            {
                var path = AppContext.BaseDirectory;
                ISEADocument document = new();
                var obj = await GetNode(document);
                await document.Save(path, "TestFile");
                ISEADocument document1 = new();
                await document1.Load(Path.Combine(path, "TestFile.isea"));
            }
            catch
            {

                throw;
            }
        }

        [Test]
        public async Task LoadTest2()
        {
            try
            {
                var path = AppContext.BaseDirectory;
                ISEADocument document = new();
                var obj = await GetNode(document);
                await document.Save(path, "TestFile");
                ISEADocument document1 = new();
                await document1.LoadAllNodes(Path.Combine(path, "TestFile.isea"));
            }
            catch
            {

                throw;
            }
        }

        [Test]
        public async Task LoadTest3()
        {
            try
            {
                var path = AppContext.BaseDirectory;
                ISEADocument document = new();
                var obj = await GetNode(document);
                await document.Save(path, "TestFile");
                ISEADocument document1 = new();
                await document1.Load(Path.Combine(path, "TestFile.isea"));
                var node = document1.GetNodeFromName<MeshNode>("Car");
                Assert.That(node, Is.Not.Null);
            }
            catch
            {

                throw;
            }
        }

        [Test]
        public async Task LoadTest4()
        {
            try
            {
                var path = AppContext.BaseDirectory;
                ISEADocument document = new();
                var obj = await GetNode(document);
                await document.Save(path, "TestFile");
                ISEADocument document1 = new();
                await document1.Load(Path.Combine(path, "TestFile.isea"));
                Assert.That(document1.TryGetNodeFromName<RenderObjectNode>("C1024DWAF2", out var node));
                Assert.That(document1.TryGetNodeFromName<RenderObjectNode>("123456885", out var node2), Is.False);
            }
            catch
            {

                throw;
            }
        }

        private async Task<RenderObjectNode> GetNode(ISEADocument document)
        {
            var texPath = Path.Combine(AppContext.BaseDirectory, "Resources", "TestImage.jpg");
            var objPath = Path.Combine(AppContext.BaseDirectory, "Resources", "car.obj");
            var file = new FileInfo(texPath);
            var extension = file.Extension.Replace(".", "");
            byte[] image = File.ReadAllBytes(texPath);
            ImageNode node = new ImageNode()
            {
                Extension = extension,
                ImageData = DataArray<byte>.Create(image)
            };
            document.AddNode(node, out int id);

            JsonDocument document1 = JsonDocument.Parse(File.ReadAllText(Path.Combine(AppContext.BaseDirectory, "Resources", "test.json")));
            var config = document1.RootElement.GetProperty("Image").GetRawText();
            ConfigNode configNode = new ConfigNode()
            {
                Name = "Test1",
                Engine = "Vulkan",
                Type = "JSON",
                Config = config
            };
            document.AddNode(configNode, out id);

            TextureNode textureNode = new TextureNode()
            {
                Name = "Image",
                Image = node,
                TexConfig = configNode
            };
            document.AddNode(textureNode, out id);

            var obj = await OBJDocument.LoadAsync(objPath);
            obj.CombineData(out Vector3[] data, 0, 0, true, true);
            Memory<byte> v = Cast(data);
            MeshNode mNode = new MeshNode();
            MeshNodeCodec codec = new MeshNodeCodec();
            mNode.VerticesComponentSequence = new List<int>() { 0 };
            mNode.VerticesComponents = new List<MeshData>()
            {
                new MeshData() { Data = DataArray<byte>.Create(v.ToArray()), DataType = MeshDataType.COMPLEX, Purpose = MeshDataPurpose.COMPLEX, Stride = 36, IsCompressed = 1 }
            };
            mNode.Name = "Car";
            mNode.VerticesStride = 36;
            mNode.VerticesCount = v.Length / 9;
            document.AddNode(mNode, out id);

            RenderObjectNode r = new RenderObjectNode();
            r.Name = "C1024DWAF2";
            r.Position = new Vector3(10, 20, 30);
            r.Scale = new Vector3(0.5f, 0.5f, 0.5f);
            r.Comonents = new List<Node>() { mNode, textureNode };
            document.AddNode(r, out id);
            return r;
        }

        private Memory<byte> Cast(Vector3[] vectors)
        {
            var r = MemoryMarshal.AsBytes(new ReadOnlySpan<Vector3>(vectors));
            return new Memory<byte>(r.ToArray());
        }
    }
}
