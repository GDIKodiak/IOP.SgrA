﻿using IOP.ISEA.Generator;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace IOP.ISEA.Test
{
    public class GeneratorTest
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task ConfigTest()
        {
            try
            {
                var path = Path.Combine(AppContext.BaseDirectory, "Resources", "generator1.json");
                using var stream = File.OpenRead(path);
                ISEADocument isea = await ISEAGenerator.GenerateDocument(stream);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message + "\r\n" + e.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task ImageTest()
        {
            try
            {
                var path = Path.Combine(AppContext.BaseDirectory, "Resources", "generator2.json");
                using var stream = File.OpenRead(path);
                ISEADocument isea = await ISEAGenerator.GenerateDocument(stream);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message + "\r\n" + e.StackTrace);
                throw;
            }

        }

        [Test]
        public async Task TextureTest()
        {
            try
            {
                var path = Path.Combine(AppContext.BaseDirectory, "Resources", "generator3.json");
                using var stream = File.OpenRead(path);
                ISEADocument isea = await ISEAGenerator.GenerateDocument(stream);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message + "\r\n" + e.StackTrace);
                throw;
            }
        }

        [Test]
        public async Task JsonMeshTest()
        {
            try
            {
                var path = Path.Combine(AppContext.BaseDirectory, "Resources", "generator4.json");
                using var stream = File.OpenRead(path);
                ISEADocument isea = await ISEAGenerator.GenerateDocument(stream);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message + "\r\n" + e.StackTrace);
                throw;
            }
        }

        [Test]
        public async Task OBJMeshTest()
        {
            try
            {
                var path = Path.Combine(AppContext.BaseDirectory, "Resources", "generator5.json");
                using var stream = File.OpenRead(path);
                ISEADocument isea = await ISEAGenerator.GenerateDocument(stream);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message + "\r\n" + e.StackTrace);
                throw;
            }
        }

        [Test]
        public async Task STLMeshTest()
        {
            try
            {
                var path = Path.Combine(AppContext.BaseDirectory, "Resources", "generator7.json");
                using var stream = File.OpenRead(path);
                ISEADocument isea = await ISEAGenerator.GenerateDocument(stream);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message + "\r\n" + e.StackTrace);
                throw;
            }
        }

        [Test]
        public async Task RenderObjectTest()
        {
            try
            {
                var path = Path.Combine(AppContext.BaseDirectory, "Resources", "generator6.json");
                using var stream = File.OpenRead(path);
                ISEADocument isea = await ISEAGenerator.GenerateDocument(stream);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message + "\r\n" + e.StackTrace);
                throw;
            }
        }
    }
}
