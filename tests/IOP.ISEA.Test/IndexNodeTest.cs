﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.Test
{
    public class IndexNodeTest
    {
        [Test]
        public void SerializationTest()
        {
            try
            {
                ISEADocument document = new ISEADocument();
                IndexNode node = new IndexNode();
                node.Indexes.Add(1, new NodeIndex() { Id = 1, Length = 500, Offset = 350 });
                node.Indexes.Add(2, new NodeIndex() { Id = 2, Length = 750, Offset = 750 });
                node.Indexes.Add(3, new NodeIndex() { Id = 3, Length = 450, Offset = 452 });
                node.Indexes.Add(4, new NodeIndex() { Id = 4, Length = 333, Offset = 7854 });
                node.Indexes.Add(5, new NodeIndex() { Id = 5, Length = 678, Offset = 5588 });
                node.Indexes.Add(6, new NodeIndex() { Id = 6, Length = 322, Offset = 6785 });
                IndexNodeCodec nodeCodec = new IndexNodeCodec();
                Memory<byte> r = nodeCodec.Encode(node, document);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message + "\r\n" + e.StackTrace);
                throw;
            }
        }

        [Test]
        public void Deserialization()
        {
            try
            {
                ISEADocument document = new ISEADocument();
                IndexNode node = new IndexNode();
                node.Indexes.Add(1, new NodeIndex() { Id = 1, Length = 500, Offset = 350 });
                node.Indexes.Add(2, new NodeIndex() { Id = 2, Length = 750, Offset = 750 });
                node.Indexes.Add(3, new NodeIndex() { Id = 3, Length = 450, Offset = 452 });
                node.Indexes.Add(4, new NodeIndex() { Id = 4, Length = 333, Offset = 7854 });
                node.Indexes.Add(5, new NodeIndex() { Id = 5, Length = 678, Offset = 5588 });
                node.Indexes.Add(6, new NodeIndex() { Id = 6, Length = 322, Offset = 6785 });
                IndexNodeCodec nodeCodec = new IndexNodeCodec();
                Memory<byte> r = nodeCodec.Encode(node, document);
                Node node2 = nodeCodec.Decode(r, document);
                if (!Equals(node, node2)) throw new Exception("Failed");
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message + "\r\n" + e.StackTrace);
                throw;
            }
        }

        private bool Equals(Node node1, Node node2)
        {
            if(node1 is IndexNode index1 && node2 is IndexNode index2)
            {
                bool r = true;
                var header1 = index1.Header; var header2 = index2.Header;
                if (header1 != header2) r = false;
                foreach(var item in index1.Indexes)
                {
                    var item2 = index2.Indexes[item.Key];
                    if(item.Value != item2)
                    {
                        r = false;
                        break;
                    }
                }
                return r;
            }
            throw new InvalidCastException("Cast Failed");
        }
    }
}
