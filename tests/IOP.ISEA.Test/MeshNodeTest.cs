﻿using IOP.ISEA.OBJ;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.Test
{
    public class MeshNodeTest
    {
        [Test]
        public async Task EncodeTest()
        {
            try
            {
                var path = Path.Combine(AppContext.BaseDirectory, "Resources", "car.obj");
                var obj = await OBJDocument.LoadAsync(path);
                obj.CombineData(out Vector3[] data, 0, 0, true, true);
                Memory<byte> v = Cast(data);
                ISEADocument document = new ISEADocument();
                MeshNode node = new MeshNode();
                MeshNodeCodec codec = new MeshNodeCodec();
                node.VerticesComponentSequence = new List<int>() { 0 };
                node.VerticesComponents = new List<MeshData>()
                {
                    new MeshData() { Data = DataArray<byte>.Create(v.ToArray()), DataType = MeshDataType.COMPLEX, Purpose = MeshDataPurpose.COMPLEX, Stride = 36, IsCompressed = 1 }
                };
                node.Name = "Car";
                node.VerticesStride = 36;
                node.VerticesCount = v.Length / 9;
                Memory<byte> r = codec.Encode(node, document);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message + "\r\n" + e.StackTrace);
                throw;
            }
        }

        [Test]
        public async Task DecodeTest()
        {
            try
            {
                var path = Path.Combine(AppContext.BaseDirectory, "Resources", "car.obj");
                var obj = await OBJDocument.LoadAsync(path);
                obj.CombineData(out Vector3[] data, 0, 0, true, true);
                Memory<byte> v = Cast(data);
                ISEADocument document = new ISEADocument();
                MeshNode node = new MeshNode();
                MeshNodeCodec codec = new MeshNodeCodec();
                node.VerticesComponentSequence = new List<int>() { 0 };
                node.VerticesComponents = new List<MeshData>()
                {
                    new MeshData() { Data = DataArray<byte>.Create(v.ToArray()), DataType = MeshDataType.COMPLEX, Purpose = MeshDataPurpose.COMPLEX, Stride = 36, IsCompressed = 1 }
                };
                node.DrawIndirectCommands = new List<MeshData>()
                {
                    new MeshData() { Data = DataArray<byte>.Create(new byte[]{ 12, 36, 22, 55, 22, 88, 12, 77, 66, 22, 33, 66 }), DataType = MeshDataType.COMPLEX, Purpose = MeshDataPurpose.DRAWINDIRECT, Stride = 36, IsCompressed = 1 }
                };
                node.Name = "Car";
                node.VerticesStride = 36;
                node.VerticesCount = v.Length / 9;
                Memory<byte> r = codec.Encode(node, document);
                MeshNode node2 = codec.Decode(r, document) as MeshNode;
                Assert.That(IsSame(node, node2));
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message + "\r\n" + e.StackTrace);
                throw;
            }

        }

        private Memory<byte> Cast(Vector3[] vectors)
        {
            var r = MemoryMarshal.AsBytes(new ReadOnlySpan<Vector3>(vectors));
            return new Memory<byte>(r.ToArray());
        }

        private bool IsSame(MeshNode node1, MeshNode node2)
        {
            bool r = true;
            if (node1.VerticesStride != node2.VerticesStride) r = false;
            if (node1.Name.ToString() != node2.Name.ToString()) r = false;
            if (node1.VerticesCount != node2.VerticesCount) r = false;
            if (node1.MeshMode != node2.MeshMode) r = false;
            for(int i = 0; i < node1.VerticesComponents.Count; i++)
            {
                MeshData v1 = node1.VerticesComponents[i];
                MeshData v2 = node2.VerticesComponents[i];
                r = MeshDataIsSame(v1, v2);
            }
            r = MeshDataIsSame(node1.Indexes, node2.Indexes);
            for (int i = 0; i < node1.DrawIndirectCommands.Count; i++)
            {
                MeshData v3 = node1.DrawIndirectCommands[i];
                MeshData v4 = node2.DrawIndirectCommands[i];
                r = MeshDataIsSame(v3, v4);
            }
            return r;
        }

        private bool MeshDataIsSame(MeshData v1, MeshData v2)
        {
            bool r = true;
            if (v1.DataType != v2.DataType) r = false;
            if (v2.IsCompressed != v2.IsCompressed) r = false;
            if (v1.Purpose != v2.Purpose) r = false;
            if (v1.Stride != v2.Stride) r = false;
            if (v1.Data.ArrayLength != v2.Data.ArrayLength) r = false;
            if (v1.Data.ArrayType != v2.Data.ArrayType) r = false;
            if (v1.Data.IsCompressed != v2.Data.IsCompressed) r = false;
            for (int i = 0; i < v1.Data.Arrays.Length; i++)
            {
                if (v1.Data.Arrays[i] != v2.Data.Arrays[i])
                {
                    r = false;
                    break;
                }
            }
            return r;
        }
    }
}
