﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text.Json;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.Test
{
    public class TextureNodeTest
    {
        [Test]
        public void EncodeTest()
        {
            try
            {
                var path = Path.Combine(AppContext.BaseDirectory, "Resources", "TestImage.jpg");
                var file = new FileInfo(path);
                var extension = file.Extension.Replace(".", "");
                byte[] image = File.ReadAllBytes(path);
                ISEADocument document = new ISEADocument();
                ImageNode node = new ImageNode()
                {
                    Extension = extension,
                    ImageData = DataArray<byte>.Create(image)
                };
                JsonDocument document1 = JsonDocument.Parse(File.ReadAllText(Path.Combine(AppContext.BaseDirectory, "Resources", "test.json")));
                var config = document1.RootElement.GetProperty("Image").GetRawText();
                ConfigNode configNode = new ConfigNode()
                {
                    Name = "Test1",
                    Engine = "Vulkan",
                    Type = "JSON",
                    Config = config
                };
                TextureNode textureNode = new TextureNode()
                {
                    Name = "Image",
                    Image = node,
                    TexConfig = configNode,
                    Usage = TextureUsage.CombinedImageSampler,
                    Binding = 1,
                    ArrayElement = 0,
                    DescriptorCount = 1,
                    SetIndex = 1
                };
                TextureNodeCodec codec = new TextureNodeCodec();
                var r = codec.Encode(textureNode, document);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message + "\r\n" + e.StackTrace);
                throw;
            }
        }

        [Test]
        public void DecodeTest()
        {
            try
            {
                var path = Path.Combine(AppContext.BaseDirectory, "Resources", "TestImage.jpg");
                var file = new FileInfo(path);
                var extension = file.Extension.Replace(".", "");
                byte[] image = File.ReadAllBytes(path);
                ISEADocument document = new ISEADocument();
                ImageNode imageNode = new ImageNode()
                {
                    Extension = extension,
                    ImageData = DataArray<byte>.Create(image)
                };
                JsonDocument document1 = JsonDocument.Parse(File.ReadAllText(Path.Combine(AppContext.BaseDirectory, "Resources", "test.json")));
                var config = document1.RootElement.GetProperty("Image").GetRawText();
                ConfigNode configNode = new ConfigNode()
                {
                    Name = "Test1",
                    Engine = "Vulkan",
                    Type = "JSON",
                    Config = config
                };
                TextureNode node1 = new TextureNode()
                {
                    Name = "Image",
                    Image = imageNode,
                    TexConfig = configNode,
                    Usage = TextureUsage.CombinedImageSampler,
                    Binding = 1,
                    ArrayElement = 0,
                    DescriptorCount = 1,
                    SetIndex = 1
                };
                TextureNodeCodec codec = new TextureNodeCodec();
                var r = codec.Encode(node1, document);
                TextureNode node2 = codec.Decode(r, document) as TextureNode;
                Assert.That(IsSame(node1, node2));
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message + "\r\n" + e.StackTrace);
                throw;
            }
        }

        private bool IsSame(TextureNode node1, TextureNode node2)
        {
            bool r = true;
            if (node1.Image != node2.Image) r = false; ;
            if (node1.SamplerConfig != null || node2.SamplerConfig != null) r = false;
            if (node1.TexConfig != node2.TexConfig) r = false;
            if (node1.Name.Length != node2.Name.Length) r = false;
            if (node1.Name.Data != node2.Name.Data) r = false;
            if (node1.Usage != node2.Usage) r = false;
            if (node1.Binding != node2.Binding) r = false;
            if (node1.ArrayElement != node2.ArrayElement) r = false;
            if (node1.DescriptorCount != node2.DescriptorCount) r = false;
            if (node1.SetIndex != node2.SetIndex) r = false;
            return r;
        }
    }
}
