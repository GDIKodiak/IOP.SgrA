﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.Test
{
    public class ConfigNodeTest
    {
        [Test]
        public void EncodeTest()
        {
            try
            {
                ISEADocument document = new ISEADocument();
                ConfigNode node = new ConfigNode()
                {
                    Name = "Test1",
                    Engine = "Vulkan",
                    Type = "JSON",
                    Config = data
                };
                ConfigNodeCodec codec = new ConfigNodeCodec();
                Memory<byte> r = codec.Encode(node, document);

            }
            catch (Exception e)
            {
                Debug.Fail(e.Message + "\r\n" + e.StackTrace);
                throw;
            }
        }

        [Test]
        public void DecodeTest()
        {
            try
            {
                ISEADocument document = new ISEADocument();
                ConfigNode node = new ConfigNode()
                {
                    Name = "Test1",
                    Engine = "Vulkan",
                    Type = "JSON",
                    Config = data
                };
                ConfigNodeCodec codec = new ConfigNodeCodec();
                Memory<byte> r = codec.Encode(node, document);
                ConfigNode node2 = codec.Decode(r, document) as ConfigNode;
                Assert.That(IsSame(node, node2));
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message + "\r\n" + e.StackTrace);
                throw;
            }
        }

        private bool IsSame(ConfigNode node1, ConfigNode node2)
        {
            bool r = true;
            if (node1.Engine.Length != node2.Engine.Length) r = false;
            if (node1.Engine.Data != node2.Engine.Data) r = false;
            if (node1.Type.Length != node2.Type.Length) r = false;
            if (node1.Type.Length != node2.Type.Length) r = false;
            if (node1.Name.Length != node2.Name.Length) r = false;
            if (node1.Name.Data != node2.Name.Data) r = false;
            if (node1.Config.Length != node2.Config.Length) r = false;
            if (node1.Config.Data != node2.Config.Data) r = false;
            return r;
        }

        private const string data = @"{ ApplicationOption"": { ""ApplicationName"": ""HelloVulkan"", ""ApplicationVersion"": ""1.0.0"",
""EngineName"": ""HelloVulkan"", ""EngineVersion"": ""1.0.0"", ""ApiVersion"": ""1.0.0"" } }";
    }
}
