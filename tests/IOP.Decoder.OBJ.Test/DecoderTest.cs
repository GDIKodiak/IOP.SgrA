﻿using IOP.ISEA.OBJ;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.Decoder.OBJ.Test
{
    [TestClass]
    public class DecoderTest
    {
        [TestMethod]
        public async Task DecoderTest1()
        {
            try
            {
                OBJDocument document = await OBJDocument.LoadAsync("ch.obj");
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        public async Task CombineDataTest()
        {
            try
            {
                OBJDocument document = await OBJDocument.LoadAsync("ch.obj");
                document.CombineData(out IntPtr intPtr, out int total, out int vCount, 0, 0);
                float[] f = new float[total / 4];
                Marshal.Copy(intPtr, f, 0, total / 4);
                Marshal.FreeHGlobal(intPtr);
                document.Dispose();
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
    }
}
