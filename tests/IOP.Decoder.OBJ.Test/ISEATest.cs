﻿using IOP.ISEA;
using IOP.ISEA.OBJ;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.Decoder.OBJ.Test
{
    [TestClass]
    public class ISEATest
    {
        [TestMethod]
        public async Task DecoderTest1()
        {
            try
            {
                OBJDocument document = await OBJDocument.LoadAsync("ch.obj");
                ISEADocument document1 = new ISEADocument();
                MeshNode[] test = document1.CreateMeshNodeFromOBJ(document, 0, 0, 
                    new MeshDataPurpose[] { MeshDataPurpose.POSITION, MeshDataPurpose.NORMAL, MeshDataPurpose.TEXCOORDS });
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
            }
        }
    }
}
