﻿using IOP.Halo;
using IOP.SgrA.Editor.Plugin;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.TestPlugin
{
    public class TestPlugin : IEditorPlugin
    {
        ILogger<TestPlugin> Logger;
    
        public TestPlugin(ILogger<TestPlugin> logger)
        {
            Logger = logger;
        }

    public void Dispose()
    {
      throw new NotImplementedException();
    }

    public ValueTask Initialization()
        {
            Logger.LogInformation("This is a init test");
            return ValueTask.CompletedTask;  
        }

        public ValueTask OnAttachedToContext(IFrameworkContainer container)
        {
            return ValueTask.CompletedTask;
        }

        public void RegistService(IServiceCollection serviceDescriptors)
        {
        }
    }
}
