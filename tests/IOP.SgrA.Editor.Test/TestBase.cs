﻿using IOP.SgrA.Editor.Plugin;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Test
{
    public class TestBase
    {
#nullable disable
        public IPluginService PluginService { get; set; }
#nullable restore
        public IServiceCollection Service { get; set; } = new ServiceCollection();

        [OneTimeSetUp]
        public void InitTest()
        {
            Service.AddLogging((logger) => logger.AddDebug());
            PluginService = new PluginService(Service.BuildServiceProvider());
        }
    }
}
