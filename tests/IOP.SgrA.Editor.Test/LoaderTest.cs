﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Test
{
    public class LoaderTest : TestBase
    {
        [Test]
        public async Task LoadTest()
        {
            try
            {
                var plugins = PluginService.ForeachPlugins();
                if (plugins.Length <= 0) Assert.Fail("No plugins load");
                foreach(var item in plugins)
                {
                    await item.Plugin.Initialization();
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                Assert.Fail("plugins load failed");
            }
        }
    }
}
