﻿using IOP.SgrA.ECS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace IOP.SgrA.Test
{
    public class SceneTest
    {
        [Fact]
        public void AddSyncSystemTest()
        {
            try 
            { 
                Scene scene = new Scene();
                var type = scene.GetType();
                var method = type.GetMethod("InsertSort", BindingFlags.Instance | BindingFlags.NonPublic);
                var property = type.GetProperty("BeforeComponentSystem", BindingFlags.Instance | BindingFlags.NonPublic);
                List<SyncComponentSystem> list = property.GetValue(scene) as List<SyncComponentSystem>;
                method.Invoke(scene, new object[] { list, new TestSyncSystemA() });
                method.Invoke(scene, new object[] { list, new TestSyncSystemB() });
                method.Invoke(scene, new object[] { list, new TestSyncSystemC() });
                method.Invoke(scene, new object[] { list, new TestSyncSystemD() });
                int min = 0;
                for(int i = 0; i < list.Count; i++)
                {
                    int curr = list[i].Priority;
                    if (curr < min) Assert.Fail();
                    min = curr;
                }
            } 
            catch (Exception ex) 
            { 
                Assert.Fail(ex.Message);
            }
        }
    }

    public class TestSyncSystemA : SyncComponentSystem
    {
        public override int Priority => 55;

        public override SyncExecuteTrigger ExecuteTrigger => SyncExecuteTrigger.BeforeExecuteSystem;

        public override void Update(TimeSpan lastStamp)
        {
        }
    }

    public class TestSyncSystemB : SyncComponentSystem
    {
        public override int Priority => 22;

        public override SyncExecuteTrigger ExecuteTrigger => SyncExecuteTrigger.BeforeExecuteSystem;

        public override void Update(TimeSpan lastStamp)
        {
        }
    }

    public class TestSyncSystemC : SyncComponentSystem
    {
        public override int Priority => 66;

        public override SyncExecuteTrigger ExecuteTrigger => SyncExecuteTrigger.BeforeExecuteSystem;

        public override void Update(TimeSpan lastStamp)
        {
        }
    }

    public class TestSyncSystemD : SyncComponentSystem
    {
        public override int Priority => 23;

        public override SyncExecuteTrigger ExecuteTrigger => SyncExecuteTrigger.BeforeExecuteSystem;

        public override void Update(TimeSpan lastStamp)
        {
        }
    }
}
