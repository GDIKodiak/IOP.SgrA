﻿using IOP.SgrA.SilkNet.Vulkan;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace IOP.SgrA.Test
{
    public class MemoryTest
    {
        [Fact]
        public void MallocTest()
        {
            try
            {
                VulkanMemorySegment segment = new VulkanMemorySegment(0, 0, 10400);
                var r = new MemoryRequirements(1024, 100);
                bool r1 = segment.TryMalloc(out VulkanMemoryBlock block1, r);
                Assert.True(r1);
                r.Size = 4096; r.Alignment = 64;
                bool r2 = segment.TryMalloc(out VulkanMemoryBlock block2, r);
                Assert.True(r2);
                r.Size = 5100; r.Alignment = 100;
                bool r3 = segment.TryMalloc(out VulkanMemoryBlock block3, r);
                Assert.True(r3);
                r.Size = 8192; r.Alignment = 16;
                bool r4 = segment.TryMalloc(out VulkanMemoryBlock block4, r);
                Assert.False(r4);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                throw;
            }          
        }

        [Fact]
        public void RecoveryTest()
        {
            try
            {
                VulkanMemorySegment segment = new VulkanMemorySegment(0, 0, 10400);
                var r = new MemoryRequirements(1024, 100);
                bool r1 = segment.TryMalloc(out VulkanMemoryBlock block1, r);
                Assert.True(r1);
                r.Size = 4096; r.Alignment = 64;
                bool r2 = segment.TryMalloc(out VulkanMemoryBlock block2, r);
                Assert.True(r2);
                r.Size = 5120; r.Alignment = 100;
                bool r3 = segment.TryMalloc(out VulkanMemoryBlock block3, r);
                Assert.True(r3);
                r.Size = 8192; r.Alignment = 16;
                bool r4 = segment.TryMalloc(out VulkanMemoryBlock block4, r);
                Assert.False(r4);
                segment.Recovery(block2);
                Assert.True(block2.Flags < 0);
                segment.Recovery(block3);
                Assert.Null(block3.Owner);
                Assert.Null(block2.Owner);
                r.Size = 5120; r.Alignment = 32;
                bool r5 = segment.TryMalloc(out VulkanMemoryBlock block5, r);
                Assert.True(r5);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                throw;
            }
        }
    }
}
