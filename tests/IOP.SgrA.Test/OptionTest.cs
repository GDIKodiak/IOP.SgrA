﻿using IOP.SgrA.SilkNet.Vulkan;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Text.Json;
using Xunit;

namespace IOP.SgrA.Test
{
    public class OptionTest
    {
        [Theory]
        [InlineData(@"{""ApplicationName"":""test"", ""ApplicationVersion"":""5.2.3"",""EngineName"":""test2"", ""EngineVersion"":""1.8"",""ApiVersion"":""1""}")]
        public void ApplicationInfoOptionTest(string json)
        {
            ApplicationOption obj;
            obj = JsonSerializer.Deserialize<ApplicationOption>(json);

            void vaild()
            {
                Assert.Equal("test", obj.ApplicationName);
                Assert.Equal(new Versions(5, 2, 3), obj.ApplicationVersion);
                Assert.Equal("test2", obj.EngineName);
                Assert.Equal(new Versions(1, 8, 0), obj.EngineVersion);
                Assert.Equal(new Versions(1, 0, 0), obj.ApiVersion);
            }
            vaild();

            var json2 = JsonSerializer.Serialize(obj);
            obj = JsonSerializer.Deserialize<ApplicationOption>(json2);
            vaild();
        }

        [Theory]
        [InlineData(@"{""Format"":""D16UNorm"",""ImageType"":""Image2d"",""Extent"":{""Width"":1366,""Height"":768,""Depth"":1},""MipLevels"":1,""ArrayLayers"":1,""Samples"":""SampleCount1"",""InitialLayout"":""Undefined"",""SharingMode"":""Exclusive""}")]
        public void ImageCreateOptionTest(string json)
        {
            try
            {
                ImageCreateOption obj;
                obj = JsonSerializer.Deserialize<ImageCreateOption>(json);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Theory]
        [InlineData(@"{""ViewType"":""ImageView2d"",""Format"":""D16UNorm"",""Components"":{""R"":""R"",""G"":""G"",""B"":""B"",""A"":""A""},""SubresourceRange"":{""AspectMask"":""Depth"",""BaseMipLevel"":0,""LevelCount"":1,""BaseArrayLayer"":0,""LayerCount"":1}}")]
        public void ImageViewCreateOptionTest(string json)
        {
            try
            {
                ImageViewCreateOption obj;
                obj = JsonSerializer.Deserialize<ImageViewCreateOption>(json);
            }
            catch (Exception)
            {

                throw;
            }

        }

        [Theory]
        [InlineData(@"{""Attachments"":[{""Samples"":""SampleCount1Bit"",""LoadOp"":""Clear"",""StoreOp"":""Store"",""StencilLoadOp"":""DontCare"",""StencilStoreOp"":""DontCare"",""InitialLayout"":""Undefined"",""FinalLayout"":""PresentSrcKhr""},
{""Samples"":""SampleCount1Bit"",""LoadOp"":""Clear"",""StoreOp"":""DontCare"",""StencilLoadOp"":""DontCare"",""StencilStoreOp"":""DontCare"",""InitialLayout"":""Undefined"",""FinalLayout"":""DepthStencilAttachmentOptimal""}],""Subpasses"":[{""ColorAttachments"":[{""Attachment"":0,""Layout"":""ColorAttachmentOptimal""}],
""DepthStencilAttachment"":{""Attachment"":1,""Layout"":""DepthStencilAttachmentOptimal""},""PipelineBindPoint"":""Graphics""}],""Dependencies"":[{""SourceStageMask"":""ColorAttachmentOutput"",""SourceAccessMask"":""None"",""DestinationStageMask"":""ColorAttachmentOutput"",""DestinationAccessMask"":[""ColorAttachmentRead"",""ColorAttachmentWrite""]}]}")]
        public void RenderPassCreateOptionTest(string json)
        {
            try
            {
                RenderPassCreateOption obj;
                obj = JsonSerializer.Deserialize<RenderPassCreateOption>(json);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Theory]
        [InlineData(@"{""Flags"":""FreeDescriptorSet"", ""MaxSets"":1,""PoolSizes"":[{""Type"":""UniformBuffer"",""DescriptorCount"":1}]}")]
        public void DescriptorPoolCreateOptionTest(string json)
        {
            DescriptorPoolCreateOption obj;
            obj = JsonSerializer.Deserialize<DescriptorPoolCreateOption>(json);
        }

        [Theory]
        [InlineData(@"{""Bindings"":[{""Binding"":0,""DescriptorCount"":1,""DescriptorType"":""UniformBuffer"",""StageFlags"":""Vertex""}]}")]
        public void DescriptorSetLayoutCreateOptionTest(string json)
        {
            DescriptorSetLayoutCreateOption obj;
            obj = JsonSerializer.Deserialize<DescriptorSetLayoutCreateOption>(json);
        }

        [Theory]
        [InlineData(@"{""Stage"":""Vertex"",""Name"":""main""}")]
        public void PipelineShaderStageCreateOptionTest(string json)
        {
            PipelineShaderStageCreateOption obj;
            obj = JsonSerializer.Deserialize<PipelineShaderStageCreateOption>(json);
        }

        [Theory]
        [InlineData(@"{""VertexAttributeDescriptions"":[{""Binding"":0,""Format"":""R32G32B32SFloat"",""Location"":0,""Offset"":0},{""Binding"":0,""Format"":""R32G32B32SFloat"",""Location"":1,""Offset"":12}],""VertexBindingDescriptions"":[{""Binding"":0,""InputRate"":""Vertex"",""Stride"":24}]}")]
        public void PipelineVertexInputStateCreateOptionTest(string json)
        {
            PipelineVertexInputStateCreateOption obj;
            obj = JsonSerializer.Deserialize<PipelineVertexInputStateCreateOption>(json);
        }

        [Theory]
        [InlineData(@"{""Topology"":""TriangleList"",""PrimitiveRestartEnable"": false}")]
        public void PipelineInputAssemblyStateCreateOptionTest(string json)
        {
            PipelineInputAssemblyStateCreateOption obj;
            obj = JsonSerializer.Deserialize<PipelineInputAssemblyStateCreateOption>(json);
        }

        [Theory]
        [InlineData(@"{""Viewports"":[{""X"":0,""Y"":0,""Width"":1366,""Height"":768,""MinDepth"":0,""MaxDepth"":1}],""Scissors"":[{""Offset"":{""X"":0,""Y"":0},""Extent"":{""Width"":1366,""Height"":768}}]}")]
        public void PipelineViewportStateCreateOptionTest(string json)
        {
            PipelineViewportStateCreateOption obj;
            obj = JsonSerializer.Deserialize<PipelineViewportStateCreateOption>(json);
        }

        [Theory]
        [InlineData(@"{""PolygonMode"":""Fill"",""CullMode"":""None"",""FrontFace"":""CounterClockwise"",""DepthClampEnable"":true,""RasterizerDiscardEnable"":false,""DepthBiasEnable"":false,""DepthBiasConstantFactor"":0,""DepthBiasClamp"":0,""DepthBiasSlopeFactor"":0,""LineWidth"":1.0}")]
        public void PipelineRasterizationStateCreateOptionTest(string json)
        {
            PipelineRasterizationStateCreateOption obj;
            obj = JsonSerializer.Deserialize<PipelineRasterizationStateCreateOption>(json);
        }

        [Theory]
        [InlineData(@"{""Attachments"":[{""ColorWriteMask"":[""R"",""G"",""B"",""A""],""BlendEnable"":false,""AlphaBlendOp"":""Add"",""ColorBlendOp"":""Add"",""DestinationColorBlendFactor"":""Zero"",""SourceColorBlendFactor"":""Zero"",""SourceAlphaBlendFactor"":""Zero"",""DestinationAlphaBlendFactor"":""Zero""}],""LogicOpEnable"":false,""LogicOp"":""NoOp"",""BlendConstants"":[1.0,1.0,1.0,1.0]}")]
        public void PipelineColorBlendStateCreateOption(string json)
        {
            PipelineColorBlendStateCreateOption obj;
            obj = JsonSerializer.Deserialize<PipelineColorBlendStateCreateOption>(json);
        }

        [Theory]
        [InlineData(@"{""Back"":{""FailOp"":""Keep"",""PassOp"":""Keep"",""CompareOp"":""Always"",""CompareMask"":0,""Reference"":0,""DepthFailOp"":""Keep"",""WriteMask"":0},
""Front"":{""FailOp"":""Keep"",""PassOp"":""Keep"",""CompareOp"":""Always"",""CompareMask"":0,""Reference"":0,""DepthFailOp"":""Keep"",""WriteMask"":0},
""DepthWriteEnable"":true,""DepthTestEnable"":true,""DepthCompareOp"":""LessOrEqual"",""DepthBoundsTestEnable"":false,""MinDepthBounds"":0,""MaxDepthBounds"":0,""StencilTestEnable"":false}")]
        public void PipelineDepthStencilStateCreateOption(string json)
        {
            PipelineDepthStencilStateCreateOption obj;
            obj = JsonSerializer.Deserialize<PipelineDepthStencilStateCreateOption>(json);
        }

        [Theory]
        [InlineData(@"{""RasterizationSamples"":""SampleCount1"",""SampleShadingEnable"":false,""AlphaToCoverageEnable"":false,""AlphaToOneEnable"":false,""MinSampleShading"":0.0}")]
        public void PipelineMultisampleStateCreateOptionTest(string json)
        {
            PipelineMultisampleStateCreateOption obj;
            obj = JsonSerializer.Deserialize<PipelineMultisampleStateCreateOption>(json);
        }

        [Theory]
        [InlineData(@"{""DynamicStates"":[""Viewport"",""StencilWriteMask""]}")]
        public void PipelineDynamicStateCreateOptionTest(string json)
        {
            PipelineDynamicStateCreateOption obj;
            obj = JsonSerializer.Deserialize<PipelineDynamicStateCreateOption>(json);
        }
    }
}
