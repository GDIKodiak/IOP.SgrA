﻿using IOP.SgrA.ECS;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace IOP.SgrA.Test
{
    public class ArchetypeBitmapTest
    {
        [Fact]
        public void AndTest1()
        {
            try
            {
                ArchetypeBitMap a = new ArchetypeBitMap(256);
                ArchetypeBitMap b = new ArchetypeBitMap(257);
                ArchetypeBitMap c = a & b;
                Assert.True(c.LeftOffset == 0);
                Assert.True(c.Bits.Length == 0);
                Assert.True(c == ArchetypeBitMap.Zero);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [Fact]
        public void OrTest1()
        {
            try
            {
                ArchetypeBitMap a = new ArchetypeBitMap(256);
                ArchetypeBitMap b = new ArchetypeBitMap(257);
                ArchetypeBitMap c = a | b;
                Assert.True(c.LeftOffset == 257);
                Assert.True(c.Bits[^2] == long.MinValue);
                Assert.True(c.Bits[^1] == 1);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [Fact]
        public void FullMatchTest1()
        {
            try
            {
                ArchetypeBitMap a = new ArchetypeBitMap(255);
                ArchetypeBitMap b = new ArchetypeBitMap(256);
                ArchetypeBitMap c = new ArchetypeBitMap(257);
                ArchetypeBitMap d = a | b | c;
                ArchetypeBitMap e = a | c;
                bool r = ArchetypeBitMap.FullMatch(d, e);
                Assert.True(r);
                ArchetypeBitMap f = new ArchetypeBitMap(254);
                ArchetypeBitMap g = f | c;
                r = ArchetypeBitMap.FullMatch(d, g);
                Assert.False(r);
            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
