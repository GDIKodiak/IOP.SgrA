﻿using IOP.SgrA.ECS;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using Xunit;

namespace IOP.SgrA.Test
{
    public class ArchetypeDataListTest
    {
        [Fact]
        public void InitTest()
        {
            try
            {
                ArchetypeManager manager = new ArchetypeManager();
                var archetype = manager.CreateArchetype(out bool isHave, new IComponentData[] { new ComponentA(), new ComponentB(), new ComponentC() });
                ArchetypeDataList list = new ArchetypeDataList(archetype);
                list.Dispose();
            }
            catch (Exception)
            {

                throw;
            }
        }

        [Fact]
        public void CreateTest()
        {
            try
            {
                int index = 0;
                int count = 20000;
                ArchetypeManager manager = new ArchetypeManager();
                var archetype = manager.CreateArchetype(out bool isHave, new IComponentData[] { new ComponentA(), new ComponentB(), new ComponentC() });
                ArchetypeDataList list = new ArchetypeDataList(archetype);
                list.CreateEntities(ref index, count);
                TestQueryStructA[] array = new TestQueryStructA[count];
                MemberInfo[] memberInfos = typeof(TestQueryStructA).GetMembers().Where(x => x.MemberType == MemberTypes.Field || x.MemberType == MemberTypes.Property).ToArray();
                for (int i = 0; i < count; i++)
                {
                    array[i] = list.GetData<TestQueryStructA>(i, memberInfos);
                }
                for (int i = 0; i < count; i++)
                {
                    Assert.True(array[i].Entity.Index == i);
                }
                list.Dispose();
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Fact]
        public void CreateTest2()
        {
            try
            {
                Stopwatch stopwatch = new Stopwatch();
                int index = 0;
                int count = 20000;
                ArchetypeManager manager = new ArchetypeManager();
                var archetype = manager.CreateArchetype(out bool isHave, new IComponentData[] { new ComponentA(), new ComponentB(), new ComponentC() });
                ArchetypeDataList list = new ArchetypeDataList(archetype);
                stopwatch.Start();
                list.CreateEntities(ref index, count);
                stopwatch.Stop();
                Debug.WriteLine($"CreateTime: {stopwatch.ElapsedMilliseconds}");
                MemberInfo[] memberInfos = typeof(TestQueryStructA).GetMembers().Where(x => x.MemberType == MemberTypes.Field || x.MemberType == MemberTypes.Property).ToArray();
                TestQueryStructA[] array = new TestQueryStructA[200000];
                stopwatch.Restart();
                for (int i = 0; i < count; i++)
                {
                    array[i] = list.GetData<TestQueryStructA>(i, memberInfos);
                }
                stopwatch.Stop();
                Debug.WriteLine($"ForeachTime: {stopwatch.ElapsedMilliseconds}");
                for (int i = 0; i < count; i++)
                {
                    Assert.True(array[i].Entity.Index == i);
                }
                list.Dispose();

            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Fact]
        public void CreateTest3()
        {
            try
            {
                int index = 0;
                int count = 20000;
                ArchetypeManager manager = new ArchetypeManager();
                var archetype = manager.CreateArchetype(out bool isHave, new IComponentData[] { new ComponentA(), new ComponentB(), new ComponentC() });
                ArchetypeDataList list = new ArchetypeDataList(archetype);
                list.CreateEntities(ref index, count, out Entity[] entities);
                for (int i = 0; i < count; i++)
                {
                    Assert.True(entities[i].Index == i);
                }
                list.Dispose();
            }
            catch (Exception)
            {
                throw;
            }
        }

        [Fact]
        public void CreateTest4()
        {
            try
            {
                int index = 0;
                int count = 1000;
                ArchetypeManager manager = new ArchetypeManager();
                var archetype = manager.CreateArchetype(out bool isHave, new IComponentData[] { new ComponentA(), new ComponentB(), new ComponentC() });
                ArchetypeDataList list = new ArchetypeDataList(archetype);
                MemberInfo[] memberInfos = typeof(TestQueryStructA).GetMembers().Where(x => x.MemberType == MemberTypes.Field || x.MemberType == MemberTypes.Property).ToArray();
                TestQueryStructA structA = new TestQueryStructA()
                {
                    Entity = new Entity(10000, 50000),
                    ComponentA = new ComponentA() { A = 5 },
                    ComponentB = new ComponentB() { Position = Vector3.One }
                };
                list.CreateEntities(ref index, count, structA, memberInfos);
                list.GetDatas(0, count, memberInfos, out TestQueryStructA[] array);
                for (int i = 0; i < array.Length; i++)
                {
                    if (array[i].Entity.Index != i) throw new Exception("Failed");
                    if(array[i].ComponentA.A != 5) throw new Exception("Failed");
                    if(array[i].ComponentB.Position != Vector3.One) throw new Exception("Failed");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        [Fact]
        public void CreateTest5()
        {
            try
            {
                int index = 0;
                int count = 1000;
                ArchetypeManager manager = new ArchetypeManager();
                var archetype = manager.CreateArchetype(out bool isHave, new IComponentData[] { new ComponentA(), new ComponentB(), new ComponentC() });
                ArchetypeDataList list = new ArchetypeDataList(archetype);
                MemberInfo[] memberInfos = typeof(TestQueryStructA).GetMembers().Where(x => x.MemberType == MemberTypes.Field || x.MemberType == MemberTypes.Property).ToArray();
                TestQueryStructA structA = new TestQueryStructA()
                {
                    Entity = new Entity(10000, 50000),
                    ComponentA = new ComponentA() { A = 5 },
                    ComponentB = new ComponentB() { Position = Vector3.One }
                };
                list.CreateEntities(ref index, count, structA, memberInfos, out Entity[] entities);
                list.GetDatas(0, count, memberInfos, out TestQueryStructA[] array);
                for (int i = 0; i < array.Length; i++)
                {
                    if (array[i].Entity.Index != entities[i].Index) 
                        throw new Exception("Failed");
                    if (array[i].ComponentA.A != 5) throw new Exception("Failed");
                    if (array[i].ComponentB.Position != Vector3.One) throw new Exception("Failed");
                }
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Fact]
        public void QueryTest()
        {
            try
            {
                Stopwatch stopwatch = new Stopwatch();
                int index = 0;
                int count = 50000;
                ArchetypeManager manager = new ArchetypeManager();
                var archetype = manager.CreateArchetype(out bool isHave, new IComponentData[] { new ComponentA(), new ComponentB(), new ComponentC() });
                ArchetypeDataList list = new ArchetypeDataList(archetype);
                stopwatch.Start();
                list.CreateEntities(ref index, count);
                stopwatch.Stop();
                Debug.WriteLine($"CreateTime: {stopwatch.ElapsedMilliseconds}");
                MemberInfo[] memberInfos = typeof(TestQueryStructA).GetMembers().Where(x => x.MemberType == MemberTypes.Field || x.MemberType == MemberTypes.Property).ToArray();
                stopwatch.Restart();
                list.GetDatas(0, count, memberInfos, out TestQueryStructA[] array);
                Debug.WriteLine($"ForeachTime: {stopwatch.ElapsedMilliseconds}");
                for(int i =0; i < array.Length; i++)
                {
                    if (array[i].Entity.Index != i) throw new Exception("Failed");
                }
                stopwatch.Stop();
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Fact]
        public void QueryTest2()
        {
            try
            {
                Stopwatch stopwatch = new Stopwatch();
                int index = 0;
                int count = 1000000;
                ArchetypeManager manager = new ArchetypeManager();
                var archetype = manager.CreateArchetype(out bool isHave, new IComponentData[] { new ComponentA(), new ComponentB(), new ComponentC() });
                ArchetypeDataList list = new ArchetypeDataList(archetype);
                stopwatch.Start();
                list.CreateEntities(ref index, count);
                stopwatch.Stop();
                Debug.WriteLine($"CreateTime: {stopwatch.ElapsedMilliseconds}");
                MemberInfo[] memberInfos = typeof(TestQueryStructA).GetMembers().Where(x => x.MemberType == MemberTypes.Field || x.MemberType == MemberTypes.Property).ToArray();
                int[] vs = new int[] { 0 };
                stopwatch.Restart();
                list.Foreach<TestQueryStructA>(memberInfos, (group) =>
                {
                    if (vs[0] != group.Entity.Index) throw new Exception("Failed");
                    vs[0] += 1;
                });
                Debug.WriteLine($"ForeachTime: {stopwatch.ElapsedMilliseconds}");
                stopwatch.Stop();
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Fact]
        public void QueryTest3()
        {
            try
            {
                Stopwatch stopwatch = new Stopwatch();
                int index = 0;
                int count = 1000000;
                ArchetypeManager manager = new ArchetypeManager();
                var archetype = manager.CreateArchetype(out bool isHave, new IComponentData[] { new ComponentA(), new ComponentB(), new ComponentC() });
                ArchetypeDataList list = new ArchetypeDataList(archetype);
                stopwatch.Start();
                list.CreateEntities(ref index, count);
                stopwatch.Stop();
                Debug.WriteLine($"CreateTime: {stopwatch.ElapsedMilliseconds}");
                MemberInfo[] memberInfos = typeof(TestQueryStructA).GetMembers().Where(x => x.MemberType == MemberTypes.Field || x.MemberType == MemberTypes.Property).ToArray();
                stopwatch.Restart();
                int[] vs = new int[] { 4 };
                list.Foreach(memberInfos, () =>
                {
                    vs[0] += 1;
                    return new TestQueryStructA
                    {
                        ComponentA = new ComponentA() { A = vs[0] }
                    };
                });
                Debug.WriteLine($"ForeachTime: {stopwatch.ElapsedMilliseconds}");
                stopwatch.Restart();
                list.GetDatas(0, count, memberInfos, out TestQueryStructA[] test);
                stopwatch.Stop();
                Debug.WriteLine($"GetDatasTime: {stopwatch.ElapsedMilliseconds}");
                index = 5;
                for (int i = 0; i < test.Length; i++)
                {
                    if (test[i].ComponentA.A != index)
                        throw new Exception();
                    Assert.True(test[i].ComponentA.A == index);
                    index++;
                }
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Fact]
        public void QueryTest4()
        {
            try
            {
                Stopwatch stopwatch = new Stopwatch();
                int index = 0;
                int count = 20000;
                ArchetypeManager manager = new ArchetypeManager();
                var archetype = manager.CreateArchetype(out bool isHave, new IComponentData[] { new ComponentA(), new ComponentB(), new ComponentC() });
                ArchetypeDataList list = new ArchetypeDataList(archetype);
                stopwatch.Start();
                list.CreateEntities(ref index, count);
                stopwatch.Stop();
                Trace.WriteLine($"CreateTime: {stopwatch.ElapsedMilliseconds}");
                MemberInfo[] memberInfos = typeof(TestQueryStructA).GetMembers().Where(x => x.MemberType == MemberTypes.Field || x.MemberType == MemberTypes.Property).ToArray();
                list.Foreach(memberInfos, (ref TestQueryStructA group) =>
                {
                });
                stopwatch.Restart();
                int[] vs = new int[] { 0 };
                list.Foreach<TestQueryStructA>(memberInfos, (group) =>
                {
                    group.ComponentB.Position = new Vector3(vs[0], 0, 0);
                    vs[0] += 1;
                    return group;
                });
                Trace.WriteLine($"ForeachTime: {stopwatch.ElapsedMilliseconds}");
                stopwatch.Restart();
                list.GetDatas(0, count, memberInfos, out TestQueryStructA[] test);
                stopwatch.Stop();
                Trace.WriteLine($"GetDatasTime: {stopwatch.ElapsedMilliseconds}");
                index = 0;
                for (int i = 0; i < test.Length; i++)
                {
                    Assert.True(test[i].Entity.Index == index);
                    Assert.True(test[i].ComponentB.Position == new Vector3(index, 0, 0));
                    index++;
                }
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Fact]
        public void QueryTest5()
        {
            try
            {
                Stopwatch stopwatch = new Stopwatch();
                int index = 0;
                int count = 60000;
                ArchetypeManager manager = new ArchetypeManager();
                var archetype = manager.CreateArchetype(out bool isHave, new IComponentData[] { new ComponentA(), new ComponentB(), new ComponentC() });
                ArchetypeDataList list = new ArchetypeDataList(archetype);
                stopwatch.Start();
                list.CreateEntities(ref index, count);
                stopwatch.Stop();
                Trace.WriteLine($"CreateTime: {stopwatch.ElapsedMilliseconds}");
                MemberInfo[] memberInfos = typeof(TestQueryStructA).GetMembers().Where(x => x.MemberType == MemberTypes.Field || x.MemberType == MemberTypes.Property).ToArray();
                list.Foreach(memberInfos, (ref TestQueryStructA group) =>
                {
                });
                stopwatch.Restart();
                int[] vs = new int[] { 0 };
                list.Foreach(memberInfos, (ref TestQueryStructA group) =>
                {
                    group.ComponentB.Position = new Vector3(vs[0], 0, 0);
                    vs[0] += 1;
                });
                Trace.WriteLine($"ForeachTime: {stopwatch.ElapsedMilliseconds}");
                stopwatch.Restart();
                list.GetDatas(0, count, memberInfos, out TestQueryStructA[] test);
                stopwatch.Stop();
                Trace.WriteLine($"GetDatasTime: {stopwatch.ElapsedMilliseconds}");
                index = 0;
                for (int i = 0; i < test.Length; i++)
                {
                    Assert.True(test[i].Entity.Index == index);
                    Assert.True(test[i].ComponentB.Position == new Vector3(index, 0, 0));
                    index++;
                }
            }
            catch (Exception e)
            {

                Debug.Fail(e.Message);
                throw;
            }
        }

        [Fact]
        public void ParallelQueryTest()
        {
            try
            {
                Stopwatch stopwatch = new Stopwatch();
                int index = 0;
                int count = 60000;
                ArchetypeManager manager = new ArchetypeManager();
                var archetype = manager.CreateArchetype(out bool isHave, new IComponentData[] { new ComponentA(), new ComponentB(), new ComponentC() });
                ArchetypeDataList list = new ArchetypeDataList(archetype);
                stopwatch.Start();
                list.CreateEntities(ref index, count);
                stopwatch.Stop();
                Trace.WriteLine($"CreateTime: {stopwatch.ElapsedMilliseconds}");
                MemberInfo[] memberInfos = typeof(TestQueryStructA).GetMembers().Where(x => x.MemberType == MemberTypes.Field || x.MemberType == MemberTypes.Property).ToArray();
                list.Foreach<TestQueryStructA>(memberInfos, (group) =>
                {
                });
                stopwatch.Restart();
                list.ParallelForeach<TestQueryStructA>(memberInfos, (group) =>
                {
                });
                stopwatch.Stop();
                Trace.WriteLine($"ForeachTime: {stopwatch.ElapsedMilliseconds}");
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Fact]
        public void ParallelQueryTest2()
        {
            try
            {
                Stopwatch stopwatch = new Stopwatch();
                int index = 0;
                int count = 100000;
                ArchetypeManager manager = new ArchetypeManager();
                var archetype = manager.CreateArchetype(out bool isHave, new IComponentData[] { new ComponentA(), new ComponentB(), new ComponentC() });
                ArchetypeDataList list = new ArchetypeDataList(archetype);
                stopwatch.Start();
                list.CreateEntities(ref index, count);
                stopwatch.Stop();
                Debug.WriteLine($"CreateTime: {stopwatch.ElapsedMilliseconds}");
                MemberInfo[] memberInfos = typeof(TestQueryStructA).GetMembers().Where(x => x.MemberType == MemberTypes.Field || x.MemberType == MemberTypes.Property).ToArray();
                stopwatch.Restart();
                int[] vs = new int[] { 4 };
                list.ParallelForeach(memberInfos, () =>
                {
                    return new TestQueryStructA
                    {
                        ComponentB = new ComponentB { Position = new Vector3(vs[0], 0, 0) }
                    };
                });
                Debug.WriteLine($"ForeachTime: {stopwatch.ElapsedMilliseconds}");
                stopwatch.Restart();
                list.GetDatas(0, count, memberInfos, out TestQueryStructA[] test);
                stopwatch.Stop();
                Debug.WriteLine($"GetDatasTime: {stopwatch.ElapsedMilliseconds}");
                for (int i = 0; i < test.Length; i++)
                {
                    Assert.True(test[i].ComponentB.Position == new Vector3(vs[0], 0, 0));
                }
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Fact]
        public void ParallelQueryTest3()
        {
            try
            {
                Stopwatch stopwatch = new Stopwatch();
                int index = 0;
                int count = 1000000;
                ArchetypeManager manager = new ArchetypeManager();
                var archetype = manager.CreateArchetype(out bool isHave, new IComponentData[] { new ComponentA(), new ComponentB(), new ComponentC() });
                ArchetypeDataList list = new ArchetypeDataList(archetype);
                stopwatch.Start();
                list.CreateEntities(ref index, count);
                stopwatch.Stop();
                Trace.WriteLine($"CreateTime: {stopwatch.ElapsedMilliseconds}");
                MemberInfo[] memberInfos = typeof(TestQueryStructA).GetMembers().Where(x => x.MemberType == MemberTypes.Field || x.MemberType == MemberTypes.Property).ToArray();
                stopwatch.Restart();
                list.ParallelForeach<TestQueryStructA>(memberInfos, (group) =>
                {
                    group.ComponentB.Position = new Vector3(group.Entity.Index, 0, 0);
                    return group;
                });
                Trace.WriteLine($"ForeachTime: {stopwatch.ElapsedMilliseconds}");
                stopwatch.Restart();
                list.GetDatas(0, count, memberInfos, out TestQueryStructA[] test);
                stopwatch.Stop();
                Trace.WriteLine($"GetDatasTime: {stopwatch.ElapsedMilliseconds}");
                index = 0;
                for (int i = 0; i < test.Length; i++)
                {
                    Assert.True(test[i].Entity.Index == index);
                    Assert.True(test[i].ComponentB.Position == new Vector3(index, 0, 0));
                    index++;
                }
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Fact]
        public void WriteDataTest()
        {
            try
            {
                Stopwatch stopwatch = new Stopwatch();
                int index = 0;
                int count = 20000;
                ArchetypeManager manager = new ArchetypeManager();
                var archetype = manager.CreateArchetype(out bool isHave, new IComponentData[] { new ComponentA(), new ComponentB(), new ComponentC() });
                ArchetypeDataList list = new ArchetypeDataList(archetype);
                stopwatch.Start();
                list.CreateEntities(ref index, count);
                stopwatch.Stop();
                Trace.WriteLine($"CreateTime: {stopwatch.ElapsedMilliseconds}");
                MemberInfo[] memberInfos = typeof(TestQueryStructA).GetMembers().Where(x => x.MemberType == MemberTypes.Field || x.MemberType == MemberTypes.Property).ToArray();
                TestQueryStructA[] queryStructAs = new TestQueryStructA[count];
                for(int i = 0; i < count; i++)
                {
                    queryStructAs[i].ComponentB = new ComponentB { Position = new Vector3(i, 0, 0) };
                }
                stopwatch.Restart();
                for(int i = 0; i < count; i++)
                {
                    list.WriteData(i, memberInfos, queryStructAs[i]);
                }
                stopwatch.Stop();
                Trace.WriteLine($"WriteTime: {stopwatch.ElapsedMilliseconds}");
                stopwatch.Restart();
                list.GetDatas(0, count, memberInfos, out TestQueryStructA[] result);
                stopwatch.Stop();
                Trace.WriteLine($"GetDataTime: {stopwatch.ElapsedMilliseconds}");
                for(int i = 0; i < count; i++)
                {
                    Assert.True(result[i].ComponentB.Position == queryStructAs[i].ComponentB.Position);
                }

            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Fact]
        public void WriteDataTest2()
        {
            try
            {
                Stopwatch stopwatch = new Stopwatch();
                int index = 0;
                int count = 20000;
                ArchetypeManager manager = new ArchetypeManager();
                var archetype = manager.CreateArchetype(out bool isHave, new IComponentData[] { new ComponentA(), new ComponentB(), new ComponentC() });
                ArchetypeDataList list = new ArchetypeDataList(archetype);
                stopwatch.Start();
                list.CreateEntities(ref index, count);
                stopwatch.Stop();
                Trace.WriteLine($"CreateTime: {stopwatch.ElapsedMilliseconds}");
                MemberInfo[] memberInfos = typeof(TestQueryStructA).GetMembers().Where(x => x.MemberType == MemberTypes.Field || x.MemberType == MemberTypes.Property).ToArray();
                TestQueryStructA[] queryStructAs = new TestQueryStructA[count];
                for (int i = 0; i < count; i++)
                {
                    queryStructAs[i].ComponentB = new ComponentB { Position = new Vector3(i, 0, 0) };
                }
                stopwatch.Restart();
                list.WriteDatas(0, memberInfos, queryStructAs);
                stopwatch.Stop();
                Trace.WriteLine($"WriteTime: {stopwatch.ElapsedMilliseconds}");
                stopwatch.Restart();
                list.GetDatas(0, count, memberInfos, out TestQueryStructA[] result);
                stopwatch.Stop();
                Trace.WriteLine($"GetDataTime: {stopwatch.ElapsedMilliseconds}");
                for (int i = 0; i < count; i++)
                {
                    Assert.True(result[i].ComponentB.Position == queryStructAs[i].ComponentB.Position);
                }
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Fact]
        public void RecoveryEntityTest1()
        {
            Random random = new Random();
            try
            {
                int index = 0;
                int count = 20000;
                int number1 = random.Next(0, count);
                int number2 = random.Next(0, count);
                while (number1 == number2) number2 = random.Next(0, count);
                ArchetypeManager manager = new ArchetypeManager();
                var archetype = manager.CreateArchetype(out bool isHave, new IComponentData[] { new ComponentA(), new ComponentB(), new ComponentC() });
                ArchetypeDataList list = new ArchetypeDataList(archetype);
                list.CreateEntities(ref index, count, out Entity[] entites);
                list.RecoveryEntity(entites[number1]);
                list.RecoveryEntity(entites[number2]);
                MemberInfo[] memberInfos = typeof(TestQueryStructA).GetMembers().Where(x => x.MemberType == MemberTypes.Field || x.MemberType == MemberTypes.Property).ToArray();
                list.Foreach<TestQueryStructA>(memberInfos, (group) =>
                {
                    if (group.Entity.Index == number1 || group.Entity.Index == number2) throw new Exception("Failed");
                });
                list.CreateEntities(ref index, 2, out Entity[] newEntities);
                Assert.Equal(newEntities[0], new Entity() { Index = number1, Version = 1 });
                Assert.Equal(newEntities[1], new Entity() { Index = number2, Version = 1 });
                bool found1 = false;
                bool found2 = false;
                list.Foreach<TestQueryStructA>(memberInfos, (group) =>
                {
                    if (group.Entity.Index == number1) found1 = true;
                    if (group.Entity.Index == number2) found2 = true;
                });
                Assert.True(found1);
                Assert.True(found2);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Fact]
        public void RecoveryEntityTest2()
        {
            Random random = new Random();
            try
            {
                int index = 0;
                int count = 10;
                ArchetypeManager manager = new ArchetypeManager();
                var archetype = manager.CreateArchetype(out bool isHave, new IComponentData[] { new ComponentA(), new ComponentB(), new ComponentC() });
                ArchetypeDataList list = new ArchetypeDataList(archetype);
                list.CreateEntities(ref index, count, out Entity[] entites);
                Span<Entity> local = entites;
                Span<Entity> r = local.Slice(3, 5);
                list.RecoveryEntities(r.ToArray());
                int i = 0;
                MemberInfo[] memberInfos = typeof(TestQueryStructA).GetMembers().Where(x => x.MemberType == MemberTypes.Field || x.MemberType == MemberTypes.Property).ToArray();
                list.Foreach<TestQueryStructA>(memberInfos, (group) =>
                {
                    i++;
                });
                Assert.True(i == 5);
                list.CreateEntities(ref index, 3);
                Assert.True(index == 10);
                list.Foreach<TestQueryStructA>(memberInfos, (group) =>
                {
                    if (group.Entity.Index == 6 || group.Entity.Index == 7) throw new Exception("Failed");
                });
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Fact]
        public void RecoveryEntityTest3()
        {
            try
            {
                int index = 0;
                int count = 10;
                ArchetypeManager manager = new ArchetypeManager();
                var archetype = manager.CreateArchetype(out bool isHave, new IComponentData[] { new ComponentA(), new ComponentB(), new ComponentC() });
                ArchetypeDataList list = new ArchetypeDataList(archetype);
                list.CreateEntities(ref index, count, out Entity[] entites);
                Span<Entity> local = entites;
                Span<Entity> r = local.Slice(3, 5);
                list.RecoveryEntities(r.ToArray());
                TestQueryStructA structA = new TestQueryStructA()
                {
                    Entity = new Entity(10000, 50000),
                    ComponentA = new ComponentA() { A = 5 },
                    ComponentB = new ComponentB() { Position = Vector3.One }
                };
                MemberInfo[] memberInfos = typeof(TestQueryStructA).GetMembers().Where(x => x.MemberType == MemberTypes.Field || x.MemberType == MemberTypes.Property).ToArray();
                list.CreateEntities(ref index, 3, structA, memberInfos);
                list.Foreach<TestQueryStructA>(memberInfos, (group) =>
                {
                    if (group.Entity.Index == 6 || group.Entity.Index == 7) throw new Exception("Failed");
                    if (group.Entity.Index >= 3 && group.Entity.Index <= 5)
                    {
                        if (group.ComponentA.A != 5) throw new Exception("Failed");
                        if (group.ComponentB.Position != Vector3.One) throw new Exception("Failed");
                    }
                });
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Fact]
        public void RecoveryEntityTest4()
        {
            try
            {
                int index = 0;
                int count = 10;
                ArchetypeManager manager = new ArchetypeManager();
                var archetype = manager.CreateArchetype(out bool isHave, new IComponentData[] { new ComponentA(), new ComponentB(), new ComponentC() });
                ArchetypeDataList list = new ArchetypeDataList(archetype);
                list.CreateEntities(ref index, count, out Entity[] entites);
                Span<Entity> local = entites;
                Span<Entity> r = local.Slice(3, 5);
                list.RecoveryEntities(r.ToArray());
                TestQueryStructA structA = new TestQueryStructA()
                {
                    Entity = new Entity(10000, 50000),
                    ComponentA = new ComponentA() { A = 5 },
                    ComponentB = new ComponentB() { Position = Vector3.One }
                };
                MemberInfo[] memberInfos = typeof(TestQueryStructA).GetMembers().Where(x => x.MemberType == MemberTypes.Field || x.MemberType == MemberTypes.Property).ToArray();
                list.CreateEntities(ref index, 3, structA, memberInfos, out Entity[] entites2);
                foreach(var item in entites2)
                {
                    TestQueryStructA a = list.GetData<TestQueryStructA>(item.RunningIndex, memberInfos);
                    if (a.ComponentA.A != 5) throw new Exception("Failed");
                    if (a.ComponentB.Position != Vector3.One) throw new Exception("Failed");
                }
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }
    }
}
