﻿using IOP.SgrA.ECS;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Numerics;
using System.Text;
using Xunit;

namespace IOP.SgrA.Test
{
    public class EntityMamagerTest
    {
        [Fact]
        public void CreateTest()
        {
            try
            {
                Stopwatch stopwatch = new Stopwatch();
                int count = 20000;
                int index = 0;
                EntityManager entityManager = new EntityManager();
                var archetypeA = entityManager.CreateArchetype(new ComponentA(), new ComponentC());
                var archetypeB = entityManager.CreateArchetype(new ComponentA(), new ComponentB(), new ComponentC());
                stopwatch.Start();
                entityManager.CreateEntities(archetypeA, count / 2, ref index);
                entityManager.CreateEntities(archetypeB, count / 2, ref index);
                stopwatch.Stop();
                Trace.WriteLine($"CreateTime:{stopwatch.ElapsedMilliseconds}");
                int[] vs = new int[] { 0 };
                entityManager.Foreach<TestQueryStructC>((group) =>
                {
                    vs[0] += 1;
                });
                Assert.True(vs[0] == count);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Fact]
        public void CreateTest2()
        {
            try
            {
                Stopwatch stopwatch = new Stopwatch();
                int count = 20000;
                int index = 0;
                EntityManager entityManager = new EntityManager();
                var archetypeA = entityManager.CreateArchetype(new ComponentA(), new ComponentC());
                var archetypeB = entityManager.CreateArchetype(new ComponentA(), new ComponentB(), new ComponentC());
                stopwatch.Start();
                entityManager.CreateEntities(archetypeA, count / 2, ref index, out Entity[] entities1);
                entityManager.CreateEntities(archetypeB, count / 2, ref index, out Entity[] entities2);
                stopwatch.Stop();
                Trace.WriteLine($"CreateTime:{stopwatch.ElapsedMilliseconds}");
                index = 0;
                for (int i = 0; i < count / 2; i++)
                {
                    Assert.True(entities1[i].Index == index);
                    Assert.True(entities1[i].RunningIndex == i);
                    index++;
                }
                for (int i = 0; i < count / 2; i++)
                {
                    Assert.True(entities2[i].Index == index);
                    Assert.True(entities2[i].RunningIndex == i);
                    index++;
                }
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Fact]
        public void CreateTest3()
        {
            try
            {
                Stopwatch stopwatch = new Stopwatch();
                int count = 20000;
                int index = 0;
                EntityManager entityManager = new EntityManager();
                var archetypeA = entityManager.CreateArchetype(new ComponentA(), new ComponentC());
                var archetypeB = entityManager.CreateArchetype(new ComponentA(), new ComponentB(), new ComponentC());
                TestQueryStructD test = new TestQueryStructD() { ComponentA = new ComponentA() { A = 0.9f } };
                stopwatch.Start();
                entityManager.CreateEntities(archetypeA, count / 2, ref index, test);
                entityManager.CreateEntities(archetypeB, count / 2, ref index, test);
                stopwatch.Stop();
                Trace.WriteLine($"CreateTime:{stopwatch.ElapsedMilliseconds}");
                int[] vs = new int[] { 0 };
                entityManager.Foreach<TestQueryStructD>((group) =>
                {
                    Assert.True(group.ComponentA.A == 0.9f);
                    vs[0] += 1;
                });
                Assert.True(vs[0] == count);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Fact]
        public void CreateTest4()
        {
            try
            {
                Stopwatch stopwatch = new Stopwatch();
                int count = 20000;
                int index = 0;
                EntityManager entityManager = new EntityManager();
                var archetypeA = entityManager.CreateArchetype(new ComponentA(), new ComponentC());
                var archetypeB = entityManager.CreateArchetype(new ComponentA(), new ComponentB(), new ComponentC());
                TestQueryStructD test = new TestQueryStructD() { ComponentA = new ComponentA() { A = 0.9f } };
                stopwatch.Start();
                entityManager.CreateEntities(archetypeA, count / 2, ref index, test, out Entity[] entities1);
                entityManager.CreateEntities(archetypeB, count / 2, ref index, test, out Entity[] entities2);
                stopwatch.Stop();
                Trace.WriteLine($"CreateTime:{stopwatch.ElapsedMilliseconds}");
                int[] vs = new int[] { 0 };
                entityManager.Foreach<TestQueryStructD>((group) =>
                {
                    Assert.True(group.ComponentA.A == 0.9f);
                    vs[0] += 1;
                });
                Assert.True(vs[0] == count);
                index = 0;
                for (int i = 0; i < count / 2; i++)
                {
                    Assert.True(entities1[i].Index == index);
                    Assert.True(entities1[i].RunningIndex == i);
                    index++;
                }
                for (int i = 0; i < count / 2; i++)
                {
                    Assert.True(entities2[i].Index == index);
                    Assert.True(entities2[i].RunningIndex == i);
                    index++;
                }
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Fact]
        public void ForeachTest()
        {
            try
            {
                Stopwatch stopwatch = new Stopwatch();
                int count = 20000;
                int index = 0;
                EntityManager entityManager = new EntityManager();
                var archetypeA = entityManager.CreateArchetype(new ComponentA(), new ComponentC());
                var archetypeB = entityManager.CreateArchetype(new ComponentA(), new ComponentB(), new ComponentC());
                entityManager.CreateEntities(archetypeA, count / 2, ref index);
                entityManager.CreateEntities(archetypeB, count / 2, ref index);
                int[] vs = new int[] { 0 };
                int[] vs1 = new int[] { 0 };
                stopwatch.Start();
                entityManager.Foreach<TestQueryStructD>((group) =>
                {
                    vs[0] += 1;
                });
                stopwatch.Stop();
                Trace.WriteLine($"ForeachTime1: {stopwatch.ElapsedMilliseconds}");
                stopwatch.Restart();
                entityManager.Foreach<TestQueryStructB>((group) =>
                {
                    vs1[0] += 1;
                });
                stopwatch.Stop();
                Trace.WriteLine($"ForeachTime2: {stopwatch.ElapsedMilliseconds}");
                Assert.True(vs[0] == count);
                Assert.True(vs1[0] == count / 2);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Fact]
        public void ForeachTest2()
        {
            try
            {
                Stopwatch stopwatch = new Stopwatch();
                int count = 20000;
                int index = 0;
                EntityManager entityManager = new EntityManager();
                var archetypeA = entityManager.CreateArchetype(new ComponentA(), new ComponentC());
                var archetypeB = entityManager.CreateArchetype(new ComponentA(), new ComponentB(), new ComponentC());
                entityManager.CreateEntities(archetypeA, count / 2, ref index);
                entityManager.CreateEntities(archetypeB, count / 2, ref index);
                stopwatch.Start();
                int[] vs = new int[] { 0 };
                int[] vs1 = new int[] { 0 };
                entityManager.Foreach(() =>
                {
                    var r = new TestQueryStructD() { ComponentA = new ComponentA { A = vs[0] } };
                    vs[0] += 1;
                    return r;
                });
                stopwatch.Stop();
                Trace.WriteLine($"ForeachTime1: {stopwatch.ElapsedMilliseconds}");
                vs[0] = 0;
                entityManager.Foreach<TestQueryStructD>((group) =>
                {
                    Assert.True((int)group.ComponentA.A == vs[0]);
                    vs[0] += 1;
                });
                stopwatch.Restart();
                entityManager.Foreach((TestQueryStructB group) =>
                {
                    var r = new TestQueryStructB() { ComponentB = new ComponentB { Position = new Vector3(vs1[0], 0, 0) } };
                    vs1[0] += 1;
                    return r;
                });
                stopwatch.Stop();
                Trace.WriteLine($"ForeachTime2: {stopwatch.ElapsedMilliseconds}");
                vs1[0] = 0;
                entityManager.Foreach<TestQueryStructB>((group) =>
                {
                    Assert.True(group.ComponentB.Position == new Vector3(vs1[0], 0, 0));
                    vs1[0] += 1;
                });
                Assert.True(vs[0] == count);
                Assert.True(vs1[0] == count / 2);
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Fact]
        public void GetDataTest()
        {
            try
            {
                Stopwatch stopwatch = new Stopwatch();
                int count = 20000;
                int index = 0;
                EntityManager entityManager = new EntityManager();
                var archetypeA = entityManager.CreateArchetype(new ComponentA(), new ComponentC());
                var archetypeB = entityManager.CreateArchetype(new ComponentA(), new ComponentB(), new ComponentC());
                entityManager.CreateEntities(archetypeA, count / 4, ref index);
                entityManager.CreateEntities(archetypeB, count / 4, ref index);
                entityManager.CreateEntities(archetypeB, count / 4, ref index);
                entityManager.CreateEntities(archetypeA, count / 4, ref index);
                var a = entityManager.GetData<TestQueryStructC>(500, 500);
                Assert.True(a.Entity.Index == 500);
                Assert.True(a.Entity.RunningIndex == 500);
                var b = entityManager.GetData<TestQueryStructC>(10021, 10021 - count / 4);
                Assert.True(b.Entity.Index == 10021);
                Assert.True(b.Entity.RunningIndex == 10021 - count / 4);
                var c = entityManager.GetData<TestQueryStructC>(19865, 19865 - count / 2);
                Assert.True(c.Entity.Index == 19865);
                Assert.True(c.Entity.RunningIndex == 19865 - count / 2);

            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Fact]
        public void WriteDataTest()
        {
            try
            {
                Stopwatch stopwatch = new Stopwatch();
                int count = 20000;
                int index = 0;
                EntityManager entityManager = new EntityManager();
                var archetypeA = entityManager.CreateArchetype(new ComponentA(), new ComponentC());
                var archetypeB = entityManager.CreateArchetype(new ComponentA(), new ComponentB(), new ComponentC());
                entityManager.CreateEntities(archetypeA, count / 4, ref index);
                entityManager.CreateEntities(archetypeB, count / 4, ref index);
                entityManager.CreateEntities(archetypeB, count / 4, ref index);
                entityManager.CreateEntities(archetypeA, count / 4, ref index);
                entityManager.WriteData(10074, 5074, new TestQueryStructD { ComponentA = new ComponentA() { A = 852.312f } });
                stopwatch.Start();
                entityManager.Foreach<TestQueryStructA>((group) =>
                {
                    if(group.Entity.Index == 10074)
                    {
                        Assert.True(group.ComponentA.A == 852.312f);
                    }
                });
                stopwatch.Stop();
                Trace.WriteLine($"ForeachTime : {stopwatch.ElapsedMilliseconds}");
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        [Fact]
        public void EmptyStructTest()
        {
            EntityManager entityManager = new EntityManager();
            var archetypeA = entityManager.CreateArchetype(new ComponentA(), new EmptyStructA());
            var archetypeB = entityManager.CreateArchetype(new ComponentA(), new EmptyStructB());
            int count = 20000;
            int index = 0;
            entityManager.CreateEntities(archetypeA, count / 2, ref index);
            entityManager.CreateEntities(archetypeB, count / 2, ref index);
            index = 0;
            entityManager.Foreach<TestQueryStructE>((group) =>
            {
                index++;
            });
            Assert.True(index == count / 2);
            entityManager.Foreach<TestQueryStructF>((group) =>
            {
                index++;
            });
            Assert.True(index == count);
            index = 0;
            entityManager.Foreach<TestQueryStructD>((group) =>
            {
                index++;
            });
            Assert.True(index == count);
        }
    }
}
