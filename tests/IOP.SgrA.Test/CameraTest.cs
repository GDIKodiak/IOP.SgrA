﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using Xunit;

namespace IOP.SgrA.Test
{
    /// <summary>
    /// 摄影机测试
    /// </summary>
    public class CameraTest
    {

        [Fact]
        public void GetViewTest()
        {
            Vector3 d = new Vector3(100.0f, 150.0f, -0.0f);
            Vector3 t = new Vector3(0.0f, 0.0f, 0.0f);
            Vector3 u = new Vector3(0.0f, 1.0f, 0.0f);
            var m1 = Matrix4x4.CreateLookAt(d, t, u);
            Camera camera = new Camera(d, t, u);
            Assert.Equal(camera.ViewMatrix, m1);
        }

        [Fact]
        public void ToEulerAnglesTest()
        {
            Vector3 euler = new Vector3(0, 0, 45 * MathF.PI / 180.0f);
            var q = euler.ToInertiaZYXQuaternion();
            var e = q.ToInertiaZYXEulerAngles();
            Assert.True(Math.Abs(euler.Z - e.Z) < 0.001);

            euler = new Vector3(45 * MathF.PI / 180.0f, 0, 0);
            q = euler.ToInertiaZYXQuaternion();
            e = q.ToInertiaZYXEulerAngles();
            Assert.True(Math.Abs(euler.X - e.X) < 0.001);

            euler = new Vector3(0, 90 * MathF.PI / 180.0f, 45 * MathF.PI / 180.0f);
            q = euler.ToInertiaZYXQuaternion();
            e = q.ToInertiaZYXEulerAngles();
            Assert.True(Math.Abs(euler.Z - e.Z) < 0.001);
            Assert.True(Math.Abs(euler.Y - e.Y) < 0.001);
        }

        [Fact]
        public void ToEulerAnglesTest2()
        {
            Vector3 euler = new Vector3(26 * MathF.PI / 180.0f, 85 * MathF.PI / 180.0f, 33 * MathF.PI / 180.0f);
            var q1 = euler.ToInertiaZYXQuaternion();
            var q2 = euler.ToObjectZYXQuaternion();
            var e1 = q1.ToInertiaZYXEulerAngles();
            var e2 = q2.ToObjectZYXEulerAngles();

        }

        [Fact]
        public void ToQuaternionTest()
        {
            Vector3 euler = new Vector3(0 * MathF.PI / 180.0f, 90 * MathF.PI / 180.0f, 135 * MathF.PI / 180.0f);
            var q = euler.ToInertiaZYXQuaternion();
            var q1 = euler.ToObjectZYXQuaternion();
            var q2 = Quaternion.Conjugate(q);
            Assert.True(q1.X == q2.X);
            Assert.True(q1.Y == q2.Y);
            Assert.True(q1.Z == q2.Z);
            Assert.True(q1.W == q2.W);
        }
    }
}
