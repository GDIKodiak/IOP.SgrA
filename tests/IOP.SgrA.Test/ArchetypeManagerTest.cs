﻿using IOP.SgrA.ECS;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using Xunit;

namespace IOP.SgrA.Test
{
    public class ArchetypeManagerTest
    {
        [Fact]
        public void ArchetypeManagerTest1()
        {
            try
            {
                ArchetypeManager manager = new ArchetypeManager();
                var archetype = manager.CreateArchetype(out bool isHave, new IComponentData[] { new ComponentA(), new ComponentB(), new ComponentC() });
                Assert.True(archetype.ArchetypeOffest == 96);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [Fact]
        public void ArchetypeManagerTest2()
        {
            try
            {
                ArchetypeManager manager = new ArchetypeManager();
                var archetype = manager.CreateArchetype(out bool isHave, new IComponentData[] { new ComponentA(), new ComponentB(), new ComponentC(), new ComponentB(), new ComponentC(), new ComponentC() });
                var archetype2 = manager.CreateArchetype(out bool isHave2, new IComponentData[] { new ComponentA(), new ComponentA(), new ComponentA() });
                Assert.True(archetype.ArchetypeOffest == 96);
                Assert.True(archetype2.ArchetypeOffest == 20);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [Fact]
        public void ArchetypeManagerTest3()
        {
            try
            {
                ArchetypeManager manager = new ArchetypeManager();
                var archetype = manager.CreateArchetype<TestQueryStructA>(null, out bool isHave);
                var archetype2 = manager.CreateArchetype<TestQueryStructB>(null, out bool isHave2);
                var archetype3 = manager.CreateArchetype<TestQueryStructA>(null, out bool isHave3);
                Assert.True(archetype.ArchetypeOffest == 32);
                Assert.False(isHave);
                Assert.False(isHave2);
                Assert.True(isHave3);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [Fact]
        public void ArchetypeManagerTest4()
        {
            try
            {
                ArchetypeManager manager = new ArchetypeManager();
                var archetype = manager.CreateArchetype<TestQueryStructA>(null, out bool isHave);
                var archetype3 = manager.CreateArchetype<TestQueryStructA>(null, out bool isHave3);
                var archetype2 = manager.CreateArchetype(out bool isHave2, new IComponentData[] { new ComponentA(), new ComponentB(), new ComponentC(), new ComponentB(), new ComponentC(), new ComponentC() });
                var archetype4 = manager.CreateArchetype(out bool isHave4, new IComponentData[] { new ComponentA(), new ComponentB(), new ComponentC() });
                Assert.True(isHave3);
                Assert.False(isHave);
                Assert.True(archetype == archetype3);
                Assert.True(archetype.Name == archetype3.Name);

                Assert.True(isHave4);
                Assert.False(isHave2);
                Assert.True(archetype2 == archetype4);
                Assert.True(archetype2.Name == archetype4.Name);

                Assert.False(archetype2 == archetype3);
                Assert.False(archetype2.Name == archetype3.Name);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
