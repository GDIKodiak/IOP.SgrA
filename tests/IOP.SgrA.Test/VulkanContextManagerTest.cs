﻿using IOP.SgrA.SilkNet.Vulkan;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Xunit;

namespace IOP.SgrA.Test
{
    public class VulkanContextManagerTest
    {
        [Fact]
        public void CreateContextTest()
        {
            try
            {
                Stopwatch stopwatch = new Stopwatch();
                int size = 20000;
                VulkanContextManager manager = new VulkanContextManager();
                VRO testV = new VRO("test", 1);
                var testa = new TestVulkanRenderGroup();
                var testb = new TestVulkanRenderGroup();
                var a = manager.CreateArchetype(new ComponentA(), new ComponentB(), new ComponentC());
                var b = manager.CreateArchetype(new ComponentA(), new ComponentC());
                stopwatch.Start();
                //manager.CreateContexts(size / 2, a, testa, testV);
                stopwatch.Stop();
                Trace.WriteLine($"CreateTime1:{stopwatch.ElapsedMilliseconds}");
                stopwatch.Restart();
                //manager.CreateContexts(size / 2, b, testb, testV);
                stopwatch.Stop();
                Trace.WriteLine($"CreateTime2:{stopwatch.ElapsedMilliseconds}");
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
