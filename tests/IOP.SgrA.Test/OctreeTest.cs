﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using IOP.SgrA;
using Xunit;

namespace IOP.SgrA.Test
{
    public class OctreeTest
    {
        [Fact]
        public void CreateTest()
        {
            try
            {
                Stopwatch stopwatch = new Stopwatch();
                Octree<TestObject> octree = new(80, Vector3.Zero, 2);
                stopwatch.Start();
                TestObject obj1 = new(1, new Vector3(-20, -20, 20), new Vector3(20));
                bool r = octree.TryInsert(obj1);
                Assert.True(r);
                TestObject obj2 = new(2, new Vector3(-10, -10, 30), new Vector3(10));
                bool r2 = octree.TryInsert(obj2);
                Assert.True(r2);
                TestObject obj3 = new(3, new Vector3(-40, -40, 40), new Vector3(40));
                bool r3 = octree.TryInsert(obj3);
                Assert.False(r3);
                TestObject obj4 = new(4, new Vector3(40, 40, 40), new Vector3(20));
                bool r4 = octree.TryInsert(obj4);
                Assert.False(r4);
                TestObject obj5 = new(5, new Vector3(40, 40, -40), new Vector3(30));
                bool r5 = octree.TryInsert(obj5);
                Assert.False(r5);
                TestObject obj6 = new(6, new Vector3(-40, -40, -40), new Vector3(20));
                bool r6 = octree.TryInsert(obj6);
                Assert.False(r6);
                TestObject obj7 = new(7, new Vector3(30, 30, 30), new Vector3(10));
                bool r7 = octree.TryInsert(obj7);
                Assert.True(r7);
                TestObject obj8 = new(8, new Vector3(30, 30, -30), new Vector3(20));
                bool r8 = octree.TryInsert(obj8);
                Assert.False(r8);
                TestObject obj9 = new(9, new Vector3(30, 30, 10), new Vector3(10, 10, 30));
                bool r9 = octree.TryInsert(obj9);
                Assert.True(r9);
                stopwatch.Stop();
                Debug.WriteLine(stopwatch.ElapsedMilliseconds);
                stopwatch.Restart();
                octree.Maintenance(out var _);
                stopwatch.Stop();
                Debug.WriteLine(stopwatch.ElapsedMilliseconds);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        [Fact]
        public void MoveTest()
        {
            try
            {
                Octree<TestObject> octree = new(80, Vector3.Zero, 2);
                TestObject obj1 = new(1, new Vector3(-20, -20, 20), new Vector3(20));
                bool r = octree.TryInsert(obj1);
                Assert.True(r);
                TestObject obj2 = new(2, new Vector3(-10, -10, 30), new Vector3(10));
                bool r2 = octree.TryInsert(obj2);
                Assert.True(r2);
                TestObject obj3 = new(3, new Vector3(30, 10, 30), new Vector3(10));
                bool r3 = octree.TryInsert(obj3);
                Assert.True(r3);
                octree.MoveCenter(new Vector3(-20, -20, 0), out List<TestObject> outside);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }
    }

    public class TestObject : IBounded, ITransformable
    {
        public Bounds Bounds { get; set; }
        public Transform Transform { get; set; }
        public Bounds GetBounds() => Bounds;
        public Transform GetTransform() => Transform;

        public TestObject(int id, Vector3 center, Vector3 size)
        {
            Transform = new Transform(id, center, Quaternion.Identity, Vector3.One);
            Bounds = new Bounds(id, center, size);
        }
    }
}
