﻿using IOP.SgrA.ECS;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace IOP.SgrA.Test
{
    public struct ComponentA : IComponentData
    {
        public float A;
    }
    public struct ComponentB : IComponentData
    {
        public Vector3 Position { get; set; }
    }
    public struct ComponentC : IComponentData
    {
        public Matrix4x4 ViewMatrix;
    }

    public struct EmptyStructA : IComponentData
    {

    }

    public struct EmptyStructB : IComponentData
    {

    }

    public struct TestQueryStructA
    {
        public Entity Entity;
        public ComponentB ComponentB;
        public ComponentA ComponentA;
    }

    public struct TestQueryStructB
    {
        public ComponentB ComponentB;
    }

    public struct TestQueryStructC
    {
        public Entity Entity;
    }

    public struct TestQueryStructD
    {
        public ComponentA ComponentA;
    }

    public struct TestQueryStructE
    {
        public ComponentA ComponentA;
        public EmptyStructA EmptyStruct;
    }

    public struct TestQueryStructF
    {
        public ComponentA ComponentB;
        public EmptyStructB EmptyStruct;
    }
}
