﻿using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace IOP.SgrA.Test
{
    public class TestVulkanRenderGroup : IRenderGroup
    {
        public GroupType GroupType => GroupType.Primary;

        public int Priority => throw new NotImplementedException();

        public string Name => throw new NotImplementedException();

        public Area Viewport { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public Area Scissor { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        ViewportRect IRenderGroup.Viewport => throw new NotImplementedException();

        public Camera Camera => throw new NotImplementedException();

        public GroupRenderMode RenderMode => throw new NotImplementedException();

        private List<VulkanContext> _Contexts = new List<VulkanContext>();

        public void AddContexts(VulkanContext[] contexts)
        {
            _Contexts.AddRange(contexts);
        }

        public void AddContexts(VulkanContext[] contexts, IRenderObject renderObject)
        {
            _Contexts.AddRange(contexts);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void GroupRendering()
        {
            throw new NotImplementedException();
        }

        public void Initialization()
        {
            throw new NotImplementedException();
        }

        public bool IsEnable() => true;

        public void SetAmbient(Ambient ambient)
        {
            throw new NotImplementedException();
        }

        public void SetCamera(Camera camera)
        {
            throw new NotImplementedException();
        }

        public void SetLightPointSource(PointLight lightPointSource)
        {
            throw new NotImplementedException();
        }

        public void SetViewPort(float x, float y, float width, float height, float minDepth, float maxDepth, int index)
        {
            throw new NotImplementedException();
        }

        public void Disable()
        {
            throw new NotImplementedException();
        }

        public void Enable()
        {
            throw new NotImplementedException();
        }

        public PointLight CreatePointLight(string name, Vector3 position, LightStrength lightStrength)
        {
            throw new NotImplementedException();
        }

        public ParallelLight CreateParallelLight(string name, Vector3 direction, LightStrength lightStrength)
        {
            throw new NotImplementedException();
        }

        public void SetViewPort(float x, float y, float width, float height, float minDepth, float maxDepth)
        {
            throw new NotImplementedException();
        }

        public void GroupRendering(uint frameIndex)
        {
            throw new NotImplementedException();
        }

        public void RemoveContext(int index)
        {
            throw new NotImplementedException();
        }

        public void RemoveContext(int[] index)
        {
            throw new NotImplementedException();
        }

        public void PushDataToTexture<TStruct>(TStruct data, uint setIndex, uint binding) where TStruct : struct
        {
            throw new NotImplementedException();
        }

        public void PushContext(Context context)
        {
            throw new NotImplementedException();
        }

        public void BindingContext(Context[] context)
        {
            throw new NotImplementedException();
        }

        public void PushDataToTexture(Span<byte> data, uint setIndex, uint binding)
        {
            throw new NotImplementedException();
        }

        public void SetCustomPropery<T>(string name, T data) where T : struct
        {
            throw new NotImplementedException();
        }

        public T GetCustomPropery<T>(string name) where T : struct
        {
            throw new NotImplementedException();
        }

        public void BindingContext(Context[] context, IRenderObject renderObject, bool deepCopy = true)
        {
            throw new NotImplementedException();
        }

        public void UnbindContext(Context context)
        {
            throw new NotImplementedException();
        }

        public void PushContext(IRenderObject renderObject)
        {
            throw new NotImplementedException();
        }

        public void Destroy()
        {
            throw new NotImplementedException();
        }

        public void OnAttach(IRenderObject renderObject)
        {
            throw new NotImplementedException();
        }

        public void CloneToNewRender(IRenderObject newObj)
        {
            throw new NotImplementedException();
        }
    }
}
