﻿using IOP.Extension.DependencyInjection;
using IOP.SgrA.ECS;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace IOP.SgrA.Test
{
    public class EventBusTest
    {
        [Fact]
        public void EventBusTest1()
        {
            try
            {
                var service = new ServiceCollection();
                service.AddLogging((logger) => logger.AddDebug());
                IServiceProvider provider = service.BuildServiceProvider();
                SystemEventBus eventBus = provider.CreateAutowiredInstance<SystemEventBus>();
                var items = CreateSystems(provider, eventBus);
                for(int i = 0; i < 10; i++)
                {
                    eventBus.UpdateEvent(EventTrigger.SystemFrameBefore);
                    foreach (var item in items) item.Update(TimeSpan.Zero);
                    eventBus.UpdateEvent(EventTrigger.SystemFrameAfter);
                }
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message);
                throw;
            }
        }

        private IList<IComponentSystem> CreateSystems(IServiceProvider serviceProvider, SystemEventBus eventBus)
        {
            Type[] systems = new Type[] { typeof(ComponentSystemA), typeof(ComponentSystemB) };
            List<IComponentSystem> components = new List<IComponentSystem>();
            foreach (var item in systems)
            {
                var system = serviceProvider.CreateAutowiredInstance(item) as IComponentSystem;
                if (system is ComponentSystem componentSystem)
                {
                    componentSystem.EventBus = eventBus;
                    var methods = item.GetMethods();
                    foreach (var m in methods)
                    {
                        if (m.GetCustomAttributes(typeof(SubscribeEventAttribute), false).FirstOrDefault() is SubscribeEventAttribute attr)
                        {
                            eventBus.Subscribe(attr.Topic, componentSystem, m, attr.EventTrigger);
                        }
                    }
                    components.Add(system);
                }
            }
            return components;
        }
    }

    public class ComponentSystemA : ComponentSystem
    {
        public override void Initialization()
        {
        }

        public override void Update(TimeSpan timeSpan)
        {
            EventBus.Publish("TEST1", "Hello Wolrd", EventTrigger.SystemFrameAfter);
        }

        [SubscribeEvent("TEST2", EventTrigger.SystemFrameBefore)]
        public void Test2()
        {
            Debug.WriteLine("Have a Event");
        }
    }

    public class ComponentSystemB : ComponentSystem
    {
        public override void Initialization()
        {
        }

        public override void Update(TimeSpan timeSpan)
        {
            EventBus.Publish("TEST2", "Hello Wolrd", EventTrigger.SystemFrameBefore);
        }

        [SubscribeEvent("TEST1", EventTrigger.SystemFrameAfter)]
        public void Event1(string messgae)
        {
            Debug.WriteLine(messgae);
        }
    }
}
