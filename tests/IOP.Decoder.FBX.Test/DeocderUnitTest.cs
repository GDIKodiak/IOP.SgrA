﻿using IOP.Models.FBX;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipelines;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace IOP.Decoder.FBX.Test
{
    public class DeocderUnitTest
    {
        [Fact]
        public void FBXDecodeTest()
        {
            Pipe pipe = new Pipe();
            FBXDocument document;
            using (var model = new FileStream(@"..\..\..\..\..\media\Wp_Gun_UMP9.FBX", FileMode.Open)) 
            {
                document = FBXDecoder.Decode(model, "Wp_Gun_UMP9");
            }
            FBXObject fbxObject = new FBXObject(document);
        }

        [Fact]
        public void FBXDockerTest2()
        {
        }

        private async Task FillPipeline(Stream stream, PipeWriter writer)
        {
            try
            {
                while (true)
                {
                    Memory<byte> memory = writer.GetMemory(2048);
                    int bytesRead = await stream.ReadAsync(memory);
                    writer.Advance(bytesRead);
                    if (bytesRead == 0) break;
                    FlushResult result = await writer.FlushAsync();
                    if (result.IsCompleted) break;
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
                writer.Complete();
            }
        }

        private async Task ReadPipeline(PipeReader reader, FBXDocument document)
        {
            try
            {
                while (true)
                {
                    ReadResult result = await reader.ReadAsync();
                    ReadOnlySequence<byte> buffer = result.Buffer;
                    reader.AdvanceTo(buffer.End);
                    if (result.IsCompleted) break;
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
                reader.Complete();
            }
        }
    }
}
