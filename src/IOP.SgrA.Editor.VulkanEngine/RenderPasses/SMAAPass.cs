﻿using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.VulkanEngine
{
    [RenderPass("", RenderPassMode.Normal)]
    [Subpass(0, PipelineBindPoint.Graphics)]
    [Subpass(1, PipelineBindPoint.Graphics)]
    [Subpass(2, PipelineBindPoint.Graphics)]
    public partial class SMAAPass : ScriptedVulkanRenderPass
    {
        [Attachment(ImageLayout.ShaderReadOnlyOptimal, Format.R16G16B16A16Sfloat, StoreOp = AttachmentStoreOp.Store)]
        [SubpassAttachment(2, SubpassAttachmentType.ColorAttachment, ImageLayout.ColorAttachmentOptimal)]
        [ImageConfig(Usage = ImageUsageFlags.ColorAttachmentBit | ImageUsageFlags.SampledBit, ImageType = ImageType.Type2D, ViewType = ImageViewType.Type2D)]
        [AttachmentClearValue(0.0f, 0.0f, 0.0f, 0.0f)]
        public RenderPassAttachment OutColor {  get; set; }
        [Attachment(ImageLayout.ShaderReadOnlyOptimal, Format.R8G8B8A8Unorm, StoreOp = AttachmentStoreOp.Store)]
        [SubpassAttachment(0, SubpassAttachmentType.ColorAttachment, ImageLayout.ColorAttachmentOptimal)]
        [SubpassAttachment(1, SubpassAttachmentType.InputAttachment, ImageLayout.ShaderReadOnlyOptimal)]
        [ImageConfig(Usage = ImageUsageFlags.ColorAttachmentBit | ImageUsageFlags.InputAttachmentBit | ImageUsageFlags.SampledBit, ImageType = ImageType.Type2D, ViewType = ImageViewType.Type2D)]
        [AttachmentClearValue(0.0f, 0.0f, 0.0f, 0.0f)]
        public RenderPassAttachment EdgeArea { get; set; }
        [Attachment(ImageLayout.ShaderReadOnlyOptimal, Format.R8G8B8A8Unorm, StoreOp = AttachmentStoreOp.Store)]
        [SubpassAttachment(1, SubpassAttachmentType.ColorAttachment, ImageLayout.ColorAttachmentOptimal)]
        [SubpassAttachment(2, SubpassAttachmentType.InputAttachment, ImageLayout.ShaderReadOnlyOptimal)]
        [ImageConfig(Usage = ImageUsageFlags.ColorAttachmentBit | ImageUsageFlags.InputAttachmentBit | ImageUsageFlags.SampledBit, ImageType = ImageType.Type2D, ViewType = ImageViewType.Type2D)]
        [AttachmentClearValue(0.0f, 0.0f, 0.0f, 0.0f)]
        public RenderPassAttachment Weights { get; set; }

        [Attachment(ImageLayout.DepthStencilAttachmentOptimal, Format.D16Unorm, StoreOp = AttachmentStoreOp.Store)]
        [SubpassAttachment(0, SubpassAttachmentType.DepthStencilAttachment, ImageLayout.DepthStencilAttachmentOptimal)]
        [SubpassAttachment(1, SubpassAttachmentType.DepthStencilAttachment, ImageLayout.DepthStencilAttachmentOptimal)]
        [SubpassAttachment(2, SubpassAttachmentType.DepthStencilAttachment, ImageLayout.DepthStencilAttachmentOptimal)]
        [ImageConfig(Usage = ImageUsageFlags.DepthStencilAttachmentBit, SubresourceRangeAspect = ImageAspectFlags.DepthBit, 
            ImageType = ImageType.Type2D, ViewType = ImageViewType.Type2D)]
        [AttachmentClearValue(0.0f, 0)]
        public RenderPassAttachment Depth { get; set; }

        [SubpassDependency(SrcSubpass = uint.MaxValue, DstSubpass = 0, 
            SrcStageMask = PipelineStageFlags.TopOfPipeBit, DstStageMask = PipelineStageFlags.ColorAttachmentOutputBit, 
            SrcAccessMask = 0, DstAccessMask = AccessFlags.ColorAttachmentReadBit | AccessFlags.ColorAttachmentWriteBit, 
            DependencyFlags = DependencyFlags.ByRegionBit)]
        public PassDependency Dependency1 { get; set; }
        [SubpassDependency(SrcSubpass = 0, DstSubpass = 1, 
            SrcStageMask = PipelineStageFlags.ColorAttachmentOutputBit | PipelineStageFlags.LateFragmentTestsBit, 
            DstStageMask = PipelineStageFlags.FragmentShaderBit, 
            SrcAccessMask = AccessFlags.ColorAttachmentWriteBit | AccessFlags.DepthStencilAttachmentWriteBit, 
            DstAccessMask = AccessFlags.InputAttachmentReadBit, 
            DependencyFlags = DependencyFlags.ByRegionBit)]
        public PassDependency Dependency2 { get; set; }
        [SubpassDependency(SrcSubpass = 1, DstSubpass = 2, 
            SrcStageMask = PipelineStageFlags.ColorAttachmentOutputBit | PipelineStageFlags.LateFragmentTestsBit, 
            DstStageMask = PipelineStageFlags.FragmentShaderBit, 
            SrcAccessMask = AccessFlags.ColorAttachmentWriteBit | AccessFlags.DepthStencilAttachmentWriteBit, 
            DstAccessMask = AccessFlags.InputAttachmentReadBit, 
            DependencyFlags = DependencyFlags.ByRegionBit)]
        public PassDependency Dependency3 { get; set; }
        [SubpassDependency(SrcSubpass = 2, DstSubpass = uint.MaxValue, 
            SrcStageMask = PipelineStageFlags.ColorAttachmentOutputBit, DstStageMask = PipelineStageFlags.FragmentShaderBit, 
            SrcAccessMask = AccessFlags.ColorAttachmentWriteBit, DstAccessMask = AccessFlags.ShaderReadBit, 
            DependencyFlags = DependencyFlags.ByRegionBit)]
        public PassDependency Dependency4 { get; set; }
    }
}
