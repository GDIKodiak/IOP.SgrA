﻿using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.VulkanEngine
{
    [RenderPass("", RenderPassMode.OffScreen, FrameBufferCount = 2)]
    [Subpass(0, PipelineBindPoint.Graphics)]
    public partial class FXAAPass : ScriptedVulkanRenderPass
    {
        [Attachment(ImageLayout.ShaderReadOnlyOptimal, Format.R8G8B8A8Unorm, StoreOp = AttachmentStoreOp.Store)]
        [SubpassAttachment(0, SubpassAttachmentType.ColorAttachment, ImageLayout.ColorAttachmentOptimal)]
        [ImageConfig(Usage = ImageUsageFlags.ColorAttachmentBit | ImageUsageFlags.SampledBit, ImageType = ImageType.Type2D, ViewType = ImageViewType.Type2D)]
        [AttachmentClearValue(0.0f, 0.0f, 0.0f, 0.0f)]
        public RenderPassAttachment Color { get; set; }

        [Attachment(ImageLayout.DepthStencilAttachmentOptimal, Format.D16Unorm, StoreOp = AttachmentStoreOp.Store)]
        [SubpassAttachment(0, SubpassAttachmentType.DepthStencilAttachment, ImageLayout.DepthStencilAttachmentOptimal)]
        [ImageConfig(Usage = ImageUsageFlags.DepthStencilAttachmentBit, SubresourceRangeAspect = ImageAspectFlags.DepthBit, 
            ImageType = ImageType.Type2D, ViewType = ImageViewType.Type2D)]
        [AttachmentClearValue(0.0f, 0)]
        [GlobalConfig(UseGlobalSample = true)]
        public RenderPassAttachment Depth { get; set; }

        [SubpassDependency(SrcSubpass = 0, DstSubpass = uint.MaxValue, 
            SrcStageMask = PipelineStageFlags.ColorAttachmentOutputBit, DstStageMask = PipelineStageFlags.FragmentShaderBit, 
            SrcAccessMask = AccessFlags.ColorAttachmentWriteBit, DstAccessMask = AccessFlags.ShaderReadBit, 
            DependencyFlags = DependencyFlags.ByRegionBit)]
        public PassDependency PassDependency { get; set; }
    }
}
