﻿using IOP.SgrA.SilkNet.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.VulkanEngine
{
    /// <summary>
    /// 
    /// </summary>
    public class EngineAreaComponent : IRenderComponent
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get => "EngineArea"; }

        public PrimaryVulkanRenderGroup AreaGroup { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="areaGroup"></param>
        public EngineAreaComponent(PrimaryVulkanRenderGroup areaGroup)
        {
            AreaGroup = areaGroup;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newObj"></param>
        public void CloneToNewRender(IRenderObject newObj)
        {
            EngineAreaComponent component = new EngineAreaComponent(AreaGroup);
            newObj.AddComponent(component);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderObject"></param>
        public void OnAttach(IRenderObject renderObject) { }

        public void Destroy()
        {
        }
    }
}
