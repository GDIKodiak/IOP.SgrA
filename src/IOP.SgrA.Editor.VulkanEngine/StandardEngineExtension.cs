﻿using IOP.SgrA.SilkNet.Vulkan;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.VulkanEngine
{
    /// <summary>
    /// 
    /// </summary>
    public static class StandardEngineExtension
    {

        /// <summary>
        /// 创建引擎用区域渲染对象
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="name"></param>
        /// <param name="outputAttachment"></param>
        /// <param name="area"></param>
        /// <returns></returns>
        /// <exception cref="NullReferenceException"></exception>
        public static RenderObject CreateEngineAreaRenderingObject(this VulkanGraphicsManager manager, Scene scene, 
            PrimaryVulkanRenderGroup areaGroup, string name,
            VulkanImageView outputAttachment, VulkanImageView depth)
        {
            var lDevice = manager.VulkanDevice ?? throw new NullReferenceException("Please create logic device first");
            var sceneGroup = scene.RenderGroup ?? throw new NullReferenceException("Please create scene first");
            if (areaGroup == null) throw new NullReferenceException("source group is null");
            if (sceneGroup is not EngineCoreRenderGroup coreGroup) throw new NullReferenceException("EngineCoreRenderGroup lost");
            if (string.IsNullOrEmpty(name)) name = manager.NewStringToken();
            var samplerOp = new SamplerCreateOption()
            {
                MagFilter = Filter.Nearest,
                MinFilter = Filter.Nearest,
                MipmapMode = SamplerMipmapMode.Nearest,
                AddressModeU = SamplerAddressMode.ClampToEdge,
                AddressModeV = SamplerAddressMode.ClampToEdge,
                AddressModeW = SamplerAddressMode.ClampToEdge,
                MipLodBias = 0.0f,
                AnisotropyEnable = false,
                MaxAnisotropy = 2,
                CompareOp = CompareOp.Never,
                MinLod = 0,
                MaxLod = 0,
                CompareEnable = false,
                BorderColor = BorderColor.FloatOpaqueBlack
            };
            var vkSampler = lDevice.CreateSampler(manager.NewStringToken(), samplerOp);

            var tData = manager.CreateTextureDataFromImage(manager.NewStringToken(),
                lDevice, outputAttachment, ImageLayout.General, DescriptorType.CombinedImageSampler);
            var outputDepth = manager.CreateTextureDataFromImage(manager.NewStringToken(),
                lDevice, depth, ImageLayout.General, DescriptorType.CombinedImageSampler);
            var tex = manager.CreateTexture(manager.NewStringToken(), tData, vkSampler, 0, 0, 1, 0, DescriptorType.CombinedImageSampler);
            var tex2 = manager.CreateTexture(manager.NewStringToken(), outputDepth, vkSampler, 1, 0, 1, 0, DescriptorType.CombinedImageSampler);
            var com = new EngineAreaComponent(areaGroup);
            var render = manager.CreateRenderObject<RenderObject>(name, tex, tex2, com);
            coreGroup.BindingRenderAreaObject(render, areaGroup.RenderMode);
            return render;
        }
    }
}
