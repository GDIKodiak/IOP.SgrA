﻿using IOP.SgrA.SilkNet.Vulkan;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using CodeWF.EventBus;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.VulkanEngine
{
    /// <summary>
    /// 
    /// </summary>
    public static class VulkanEngineServiceExtensions
    {
#nullable disable
        /// <summary>
        /// 添加Vulkan引擎
        /// </summary>
        /// <param name="descriptors"></param>
        public static void AddVulkanEngine(this IServiceCollection service)
        {
            var cBuilder = new ConfigurationBuilder();
            service.AddSingleton<IConfiguration>(cBuilder.Build());
            service.AddSingleton<IGraphicsManager, VulkanGraphicsManager>();
            service.TryAddSingleton<IModuleService, VulkanModuleService>();
            service.AddSingleton<IEngineLoader, VulkanEngineLoader>((factory) =>
            {
                VulkanGraphicsManager graphicsManager = factory.GetRequiredService<IGraphicsManager>() as VulkanGraphicsManager;
                VulkanEngineLoader engineLoader = new(graphicsManager);
                return engineLoader;
            });
            service.AddSingleton<IEventBus, EventBus>((service) => EventBus.Default);
        }
    }
#nullable enable
}
