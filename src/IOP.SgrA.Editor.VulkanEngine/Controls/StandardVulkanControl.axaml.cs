using Avalonia;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Interactivity;
using Avalonia.Rendering.Composition;
using Avalonia.Threading;
using Avalonia.VisualTree;
using CodeWF.EventBus;
using IOP.Extension.DependencyInjection;
using IOP.Halo;
using IOP.SgrA.ECS;
using IOP.SgrA.Editor.VulkanEngine.Bus;
using IOP.SgrA.Input;
using IOP.SgrA.SilkNet.Vulkan;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ReactiveUI;
using Silk.NET.Vulkan;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;
using Semaphore = Silk.NET.Vulkan.Semaphore;

namespace IOP.SgrA.Editor.VulkanEngine.Controls
{
    /// <summary>
    /// 标准Vulkan离屏渲染控件
    /// </summary>
    public partial class StandardVulkanControl : UserControl, IRenderDispatcher
    {
        public static readonly StyledProperty<int> PixelWidthProperty =
            AvaloniaProperty.Register<StandardVulkanControl, int>(nameof(PixelWidth), 800);
        public static readonly StyledProperty<int> PixelHeightProperty =
            AvaloniaProperty.Register<StandardVulkanControl, int>(nameof(PixelHeight), 600);
        public static readonly StyledProperty<string> IdentifierProperty =
            AvaloniaProperty.Register<StandardVulkanControl, string>(nameof(Identifier), "");

        /// <summary>
        /// 像素宽度
        /// </summary>
        public int PixelWidth
        {
            get { return GetValue(PixelWidthProperty); }
            set { SetValue(PixelWidthProperty, value); }
        }
        /// <summary>
        /// 像素高度
        /// </summary>
        public int PixelHeight
        {
            get { return GetValue(PixelHeightProperty); }
            set { SetValue(PixelHeightProperty, value); }
        }
        /// <summary>
        /// 标识符
        /// </summary>
        public string Identifier
        {
            get { return GetValue(IdentifierProperty); }
            set { SetValue(IdentifierProperty, value); }
        }
        /// <summary>
        /// 服务
        /// </summary>
        public IServiceProvider ServiceProvider { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsRunning
        {
            get => stop == 0;
        }
        /// <summary>
        /// 当前是否正在执行渲染
        /// </summary>
        public bool IsRendering
        {
            get
            {
                SpinWait wait = new SpinWait();
                int local = _IsRendering;
                while (Interlocked.CompareExchange(ref _IsRendering, local, local) != local)
                {
                    wait.SpinOnce();
                    local = _IsRendering;
                }
                return local == 1;
            }
            protected set
            {
                SpinWait wait = new SpinWait();
                int update = value ? 1 : 0;
                int local = _IsRendering;
                while (Interlocked.CompareExchange(ref _IsRendering, update, local) != local)
                {
                    wait.SpinOnce();
                    local = _IsRendering;
                }
            }
        }
        /// <summary>
        /// FPS工具类
        /// </summary>
        public FPSUtil FPS { get; protected set; }

        /// <summary>
        /// 输入控制器
        /// </summary>
        public IInputStateController Input 
        {
            get => _Input;
        }
        /// <summary>
        /// 渲染区域
        /// </summary>
        protected Area RenderArea { get; set; }
        /// <summary>
        /// Vulkan绘图管理器
        /// </summary>
        protected VulkanGraphicsManager GraphicsManager { get; set; }
        /// <summary>
        /// 包装器
        /// </summary>
        protected VulkanRenderWapper RenderWapper { get; set; }
        /// <summary>
        /// 反走样渲染组
        /// </summary>
        protected PrimaryVulkanRenderGroup AAGroup { get; set; }
        /// <summary>
        /// 后处理渲染组
        /// </summary>
        protected PrimaryVulkanRenderGroup PostTreatmentGroup { get; set; }
        /// <summary>
        /// 输出渲染通道
        /// </summary>
        protected VulkanRenderPass OutputPass {  get; set; }
        /// <summary>
        /// 目标输出附件
        /// </summary>
        protected uint TargetAttachment {  get; set; }
        /// <summary>
        /// 工作队列
        /// </summary>
        protected WorkQueue WorkQueue { get; set; }
        /// <summary>
        /// 组件系统事件总线
        /// </summary>
        protected SystemEventBus SystemEventBus { get; set; }
        /// <summary>
        /// 外部事件总线
        /// </summary>
        protected IEventBus OuterEventBus { get; set; }
        /// <summary>
        /// 上下文管理器
        /// </summary>
        public IContextManager ContextManager { get; protected set; }
        /// <summary>
        /// 场景
        /// </summary>
        protected Scene Scene { get; set; }
        /// <summary>
        /// 日志组件
        /// </summary>
        protected ILogger<StandardVulkanControl> Logger { get; set; }

        /// <summary>
        /// 优先级队列
        /// </summary>
        protected readonly PriorityQueue<EngineQueueItem, int> PriorityQueue = new(128);
        /// <summary>
        /// 
        /// </summary>
        protected bool IsInit = false;
        protected ThreadIntervalMode LocalMode = ThreadIntervalMode.YieldMode;
        protected int PositionX;
        protected int PositionY;
        protected int XDelta;
        protected int YDelta;
        protected bool Fource = false;
        protected int stop = 0;
        protected int _IsRendering = 0;
        protected InputStateController _Input = new InputStateController();
        /// <summary>
        /// 令牌
        /// </summary>
        protected CancellationTokenSource CancellationToken = new CancellationTokenSource();
        /// <summary>
        /// 
        /// </summary>
        public StandardVulkanControl()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 创建渲染器
        /// </summary>
        /// <param name="provider"></param>
        /// <param name="modules"></param>
        /// <returns></returns>
        public async Task CreateRender(IServiceProvider serviceProvider, params Type[] modules)
        {
            ServiceProvider = serviceProvider ?? throw new ArgumentNullException(nameof(serviceProvider));
            var provider = ServiceProvider ?? throw new NullReferenceException(nameof(ServiceProvider));
            var factory = provider.GetService<ILoggerFactory>();
            OuterEventBus = provider.GetRequiredService<IEventBus>();
            ILogger<StandardVulkanControl> logger = factory?.CreateLogger<StandardVulkanControl>();
            try
            {
                IGraphicsManager manager = provider.GetRequiredService<IGraphicsManager>();
                if (manager is not VulkanGraphicsManager graphicsManager) throw new NotSupportedException("No Vulkan Environment");
                GraphicsManager = graphicsManager;
                graphicsManager.RenderConfigs.Multisampling = SampleCount.Sample2X;
                graphicsManager.RenderConfigs.PropertyChanged += RenderConfigs_PropertyChanged;
                var device = graphicsManager.VulkanDevice ?? throw new InvalidOperationException("Please create device first");
                var queue = device.WorkQueues.Where(x => x.Type.HasFlag(QueueType.Graphics)).FirstOrDefault() ??
                    throw new NotSupportedException("Target Phydevice is not supported to Graphics");
                WorkQueue = queue;
                ContextManager = graphicsManager.ContextManager;

                await Dispatcher.UIThread.InvokeAsync(() =>
                {
                    var mainGroupSemaphore = device.CreateSemaphore();
                    RenderArea = new Area(0, 0, (uint)PixelWidth, (uint)PixelHeight);
                    render.InitResource(GraphicsManager.NativeAPI, queue.Queue, GraphicsManager.VulkanDevice, mainGroupSemaphore);

                    var wapper = CreateRenderWapper(provider, modules);
                    RenderWapper = wapper;

                    var group = CreateMainGroup(RenderArea, device, graphicsManager, wapper, mainGroupSemaphore);
                    var smaa = CreateSMAAGroup(RenderArea, device, group.PrimaryCommandBuffer, graphicsManager, group, wapper);
                    AAGroup = smaa;

                    if (AAGroup != null)
                    {
                        var frameImage = AAGroup.RenderPass.GetFramebuffer(0).Attachments[0];
                        var imageSamplerOp = new SamplerCreateOption()
                        {
                            MagFilter = Filter.Linear,
                            MinFilter = Filter.Linear,
                            MipmapMode = SamplerMipmapMode.Linear,
                            AddressModeU = SamplerAddressMode.Repeat,
                            AddressModeV = SamplerAddressMode.Repeat,
                            AddressModeW = SamplerAddressMode.Repeat,
                            MipLodBias = 0.0f,
                            AnisotropyEnable = false,
                            MaxAnisotropy = 1,
                            CompareOp = CompareOp.Never,
                            MinLod = 0,
                            MaxLod = 0.25f,
                            CompareEnable = false,
                            BorderColor = BorderColor.FloatTransparentBlack
                        };
                        var imageSampler = device.CreateSampler(graphicsManager.NewStringToken(), imageSamplerOp);
                        var texData = graphicsManager.CreateTextureDataFromImage(graphicsManager.NewStringToken(),
                            device, frameImage, ImageLayout.ShaderReadOnlyOptimal, DescriptorType.CombinedImageSampler);
                        var texture = graphicsManager.CreateTexture(graphicsManager.NewStringToken(), texData, imageSampler, 1, 0, 1, 0);
                        PostTreatmentGroup.Pipeline.AddPipelineTexture(0, 1, texture);
                    }

                    CreateDefaultScene(provider, wapper, group);
                    graphicsManager.RenderConfigs.WorldMatrix = Matrix4x4.CreateRotationX(-MathF.PI * 0.5f) *
                    Matrix4x4.CreateRotationY(-MathF.PI * 0.5f);
                });
                RenderWapper?.LoadRenderModules();
            }
            catch (Exception e)
            {
                logger?.LogError(e, "");
            }
            finally
            {
                IsInit = true;
            }
        }

        /// <summary>
        /// 获取当前场景
        /// </summary>
        /// <returns></returns>
        public Scene GetCurrentScene() => Scene;
        /// <summary>
        /// 
        /// </summary>
        public void Initialization()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        public void Rendering()
        {
        }
        /// <summary>
        /// 开始渲染
        /// </summary>
        /// <param name="updateRate"></param>
        public void BeginRendering(double updateRate = 60)
        {
            if (IsRendering) return;
            CancellationToken = new CancellationTokenSource();
            Task.Factory.StartNew(() =>
            {
                var logger = GraphicsManager.Logger;
                try
                {
                    Interlocked.Exchange(ref stop, 0);
                    Rendering(updateRate);
                }
                catch (Exception e)
                {
                    logger.LogError(e.Message + "\r\n" + e.StackTrace);
                }
            }, CancellationToken.Token);
        }
        /// <summary>
        /// 
        /// </summary>
        public void Start()
        {
            BeginRendering();
        }
        /// <summary>
        /// 继续渲染
        /// </summary>
        public void Continue()
        {
            Interlocked.Exchange(ref stop, 0);
        }
        /// <summary>
        /// 停止渲染
        /// </summary>
        public void Stop()
        {
            render.CallStop();
            SpinWait wait = new SpinWait();
            Interlocked.Exchange(ref stop, 1);
            CancellationToken.Cancel();
            while (IsRendering) { wait.SpinOnce(); }
        }
        /// <summary>
        /// 销毁
        /// </summary>
        public void Destroy()
        {
            Stop();
        }
        /// <summary>
        /// 
        /// </summary>
        public void DestroyDispatcher() => Destroy();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="component"></param>
        public void PushDispatcherComponent(IRenderDispatcherComponent component)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public void RecreateRenderDispatcher(uint width, uint height)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ISystemEventBus GetSystemEventBus() => SystemEventBus;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="timeSpan"></param>
        public void SendTimeStamp(TimeSpan timeSpan)
        {
        }

        /// <summary>
        /// 创建离屏渲染通道
        /// </summary>
        /// <returns></returns>
        protected VulkanRenderPass CreateOffScreenRenderpass()
        {
            var Device = GraphicsManager.VulkanDevice ?? throw new InvalidOperationException("Please create device first");
            var pass = Device.CreateScriptedRenderPass<BlendRenderPass>(RenderArea);
            return pass;
        }
        /// <summary>
        /// 创建后处理渲染通道
        /// </summary>
        /// <returns></returns>
        protected VulkanRenderPass CreatePostTransparentRenderpass()
        {
            var Device = GraphicsManager.VulkanDevice;
            if (Device == null) throw new InvalidOperationException("Please create device first");
            var pass = Device.CreateScriptedRenderPass<PostTreatmentPass>(RenderArea);
            return pass;
        }
        /// <summary>
        /// 创建主执行非透明管线
        /// </summary>
        /// <returns></returns>
        protected VulkanPipeline CreateMainOpaquePipeline(Area area,
            VulkanGraphicsManager manager, VulkanRenderPass pass, PipelineCache cache)
        {
            var pipeline = manager.BuildScriptedShaderAndPipeline<CorePipe>("", pass, cache, area);
            OpaquePipeline = pipeline;
            return pipeline;
        }
        /// <summary>
        /// 创建主执行透明管线
        /// </summary>
        /// <param name="area"></param>
        /// <param name="manager"></param>
        /// <param name="pass"></param>
        /// <param name="cache"></param>
        /// <returns></returns>
        protected VulkanPipeline CreateMainTransparentPipeline(Area area,
            VulkanGraphicsManager manager, VulkanRenderPass pass, PipelineCache cache)
        {
            var pipeline = manager.BuildScriptedShaderAndPipeline<CoreTransparentPipe>("", pass, cache, area);
            TransparentPipeline = pipeline;
            return pipeline;
        }
        /// <summary>
        /// 创建主后处理管线
        /// </summary>
        /// <param name="area"></param>
        /// <param name="manager"></param>
        /// <param name="pass"></param>
        /// <param name="cache"></param>
        /// <returns></returns>
        protected VulkanPipeline CreateMainPostTransparentPipeline(Area area,
            VulkanGraphicsManager manager, VulkanRenderPass pass, PipelineCache cache)
        {
            var pipeline = manager.BuildScriptedShaderAndPipeline<PostTreatmentPipe>("", pass, cache, area);
            return pipeline;
        }

        #region FXAA
        /// <summary>
        /// 创建FXAA渲染通道
        /// </summary>
        /// <returns></returns>
        protected VulkanRenderPass CreateFXAARenderpass()
        {
            var Device = GraphicsManager.VulkanDevice ?? throw new InvalidOperationException("Please create device first");
            var pass = Device.CreateScriptedRenderPass<FXAAPass>(RenderArea);
            return pass;
        }
        /// <summary>
        /// 创建FXAA管线
        /// </summary>
        /// <param name="area"></param>
        /// <param name="manager"></param>
        /// <param name="pass"></param>
        /// <param name="cache"></param>
        /// <returns></returns>
        protected VulkanPipeline CreateFXAAPipeline(Area area, 
            VulkanGraphicsManager manager, VulkanRenderPass pass, PipelineCache cache)
        {
            var pipeline = manager.BuildScriptedShaderAndPipeline<CoreFXAAPipe>("CoreFXAAPipeline", pass, cache, area);
            return pipeline;
        }
        protected PrimaryVulkanRenderGroup CreateFXAAGroup(Area area, VulkanDevice lDevice, VulkanOffScreenRenderDispatcher dispatcher, 
            VulkanGraphicsManager manager, VulkanRenderPass pass, PrimaryVulkanRenderGroup mainGroup, VulkanRenderWapper wapper)
        {
            var pCommand = lDevice.CreateCommandPool((device, option) =>
            {
                option.Flags = CommandPoolCreateFlags.ResetCommandBufferBit;
                option.QueueFamilyIndex = device.WorkQueues[0].FamilyIndex;
            }).CreatePrimaryCommandBuffer();
            var pipeline = CreateFXAAPipeline(area, manager, pass, lDevice.PipelineCache);
            var sourcePass = mainGroup.RenderPass;
            var attach1 = sourcePass.Framebuffer[0].Attachments[0];
            var texData1 = manager.CreateTextureDataFromImage(manager.NewStringToken(), lDevice, attach1,
                ImageLayout.General, DescriptorType.CombinedImageSampler, 0, 0, 1);
            var samplerOp = new SamplerCreateOption()
            {
                MagFilter = Filter.Linear,
                MinFilter = Filter.Linear,
                MipmapMode = SamplerMipmapMode.Linear,
                AddressModeU = SamplerAddressMode.Repeat,
                AddressModeV = SamplerAddressMode.Repeat,
                AddressModeW = SamplerAddressMode.Repeat,
                MipLodBias = 0.0f,
                AnisotropyEnable = false,
                MaxAnisotropy = 1,
                CompareOp = CompareOp.Never,
                MinLod = 0,
                MaxLod = 0.25f,
                CompareEnable = false,
                BorderColor = BorderColor.FloatTransparentBlack
            };
            var vkSampler = lDevice.CreateSampler(manager.NewStringToken(), samplerOp);
            var tex1 = manager.CreateTexture(manager.NewStringToken(), texData1, vkSampler, 0, 0, 1, 0, DescriptorType.CombinedImageSampler);
            pipeline.AddPipelineTexture(0, 0, tex1);
            var fxaaGroup = manager.CreatePrimaryRenderGroup($"{BuildInRenderGroup.StandardEngineFXAAGroup}-{wapper.Identifier}", pipeline)
                .Binding(pCommand, lDevice, pass, [], [], []);
            return fxaaGroup;
        }
        #endregion

        #region SMAA
        /// <summary>
        /// 创建SMAA渲染通道
        /// </summary>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        protected VulkanRenderPass CreateSMAARenerpass()
        {
            var Device = GraphicsManager.VulkanDevice ?? throw new InvalidOperationException("Please create device first");
            var pass = Device.CreateScriptedRenderPass<SMAAPass>(RenderArea);
            return pass;
        }
        /// <summary>
        /// 创建SMAA第一轮管线
        /// </summary>
        /// <param name="area"></param>
        /// <param name="manager"></param>
        /// <param name="pass"></param>
        /// <param name="cache"></param>
        /// <returns></returns>
        protected VulkanPipeline CreateSMAAEdgePipeline(Area area,
            VulkanGraphicsManager manager, VulkanRenderPass pass, PipelineCache cache)
        {
            var pipeline = manager.BuildScriptedShaderAndPipeline<SMAAEdgePipe>("CoreEdgeSMAAPipe", pass, cache, area);
            return pipeline;
        }
        protected VulkanPipeline CreateSMAAWeightPipeline(Area area,
            VulkanGraphicsManager manager, VulkanRenderPass pass, PipelineCache cache)
        {
            var pipeline = manager.BuildScriptedShaderAndPipeline<SMAABlendWeightPipe>("CoreWeightSMAAPipe", pass, cache, area);
            return pipeline;
        }
        protected VulkanPipeline CreateSMAABlendPipeline(Area area,
            VulkanGraphicsManager manager, VulkanRenderPass pass, PipelineCache cache)
        {
            var pipeline = manager.BuildScriptedShaderAndPipeline<SMAABlendPipe>("CoreBlendSMAAPipe", pass, cache, area);
            return pipeline;
        }
        /// <summary>
        /// 创建SMAA渲染组
        /// </summary>
        /// <param name="area"></param>
        /// <param name="lDevice"></param>
        /// <param name="dispatcher"></param>
        /// <param name="manager"></param>
        /// <param name="pass"></param>
        /// <param name="mainGroup"></param>
        /// <param name="wapper"></param>
        /// <returns></returns>
        protected PrimaryVulkanRenderGroup CreateSMAAGroup(Area area, VulkanDevice lDevice, VulkanCommandBuffer commandBuffer,
            VulkanGraphicsManager manager, PrimaryVulkanRenderGroup mainGroup, VulkanRenderWapper wapper)
        {
            var pass = CreateSMAARenerpass();
            var pipeline = CreateSMAAEdgePipeline(area, manager, pass, lDevice.PipelineCache);
            pipeline.BuildUniformBuffer(0, 0, 64, SharingMode.Exclusive);
            var sourcePass = mainGroup.RenderPass;
            var attach1 = sourcePass.Framebuffer[0].Attachments[0];
            var texData1 = manager.CreateTextureDataFromImage(manager.NewStringToken(), lDevice, attach1, 
                ImageLayout.General, DescriptorType.CombinedImageSampler, 1, 0, 1);
            var samplerOp = new SamplerCreateOption()
            {
                MagFilter = Filter.Linear,
                MinFilter = Filter.Linear,
                MipmapMode = SamplerMipmapMode.Linear,
                AddressModeU = SamplerAddressMode.Repeat,
                AddressModeV = SamplerAddressMode.Repeat,
                AddressModeW = SamplerAddressMode.Repeat,
                MipLodBias = 0.0f,
                AnisotropyEnable = false,
                MaxAnisotropy = 1,
                CompareOp = CompareOp.Never,
                MinLod = 0,
                MaxLod = 0.25f,
                CompareEnable = false,
                BorderColor = BorderColor.FloatTransparentBlack
            };
            var vkSampler = lDevice.CreateSampler(manager.NewStringToken(), samplerOp);
            var tex1 = manager.CreateTexture(manager.NewStringToken(), texData1, vkSampler, 1, 0, 1, 0, DescriptorType.CombinedImageSampler);
            pipeline.AddPipelineTexture(0, 1, tex1);
            var smaaEdgeGroup = manager.CreatePrimaryRenderGroup($"{BuildInRenderGroup.StandardEngineSMAAGroup}-{wapper.Identifier}", pipeline)
                .Binding(commandBuffer, lDevice, pass, [], [], []);
            smaaEdgeGroup.CreateGroupRenderingAction((builder) => builder.Run((group) => SMAAMainFunc(group, area)));

            var weightPipe = CreateSMAAWeightPipeline(area, manager, pass, lDevice.PipelineCache);
            weightPipe.BuildUniformBuffer(0, 0, 64, SharingMode.Exclusive);
            var edgeAttach = pass.Framebuffer[0].Attachments[1];
            var edgeData = manager.CreateTextureDataFromImage(manager.NewStringToken(), lDevice, edgeAttach,
                ImageLayout.ShaderReadOnlyOptimal, DescriptorType.CombinedImageSampler, 1, 0, 1);
            var edgeTexture = manager.CreateTexture(manager.NewStringToken(), edgeData, vkSampler, 1, 0, 1, 0, DescriptorType.CombinedImageSampler);
            weightPipe.AddPipelineTexture(0, 1, edgeTexture);
            AddSMAADDSTextures(manager, weightPipe, lDevice);
            var weightGroup = manager.CreatePrimaryRenderGroup(manager.NewStringToken(), weightPipe).Binding(smaaEdgeGroup);

            var blendPipe = CreateSMAABlendPipeline(area, manager, pass, lDevice.PipelineCache);
            blendPipe.BuildUniformBuffer(0, 0, 64, SharingMode.Exclusive);
            var colorTex = manager.CreateTexture(manager.NewStringToken(), texData1, vkSampler, 1, 0, 1, 0, DescriptorType.CombinedImageSampler);
            blendPipe.AddPipelineTexture(0, 1, colorTex);
            var blendAttach = pass.Framebuffer[0].Attachments[2];
            var blendData = manager.CreateTextureDataFromImage(manager.NewStringToken(), lDevice, blendAttach,
                ImageLayout.ShaderReadOnlyOptimal, DescriptorType.CombinedImageSampler, 2, 0, 1);
            var blendTex = manager.CreateTexture(manager.NewStringToken(), blendData, vkSampler, 2, 0, 1, 0, DescriptorType.CombinedImageSampler);
            blendPipe.AddPipelineTexture(0, 2, blendTex);
            var blendGroup = manager.CreatePrimaryRenderGroup(manager.NewStringToken(), blendPipe).Binding(weightGroup);
            return smaaEdgeGroup;
        }

        private void AddSMAADDSTextures(VulkanGraphicsManager manager, VulkanPipeline pipeline, VulkanDevice lDevice)
        {
            var samplerOp = new SamplerCreateOption()
            {
                MagFilter = Filter.Linear,
                MinFilter = Filter.Linear,
                MipmapMode = SamplerMipmapMode.Linear,
                AddressModeU = SamplerAddressMode.Repeat,
                AddressModeV = SamplerAddressMode.Repeat,
                AddressModeW = SamplerAddressMode.Repeat,
                MipLodBias = 0.0f,
                AnisotropyEnable = false,
                MaxAnisotropy = 1,
                CompareOp = CompareOp.Never,
                MinLod = 0,
                MaxLod = 0,
                CompareEnable = false,
                BorderColor = BorderColor.FloatOpaqueWhite
            };
            var vkSampler = lDevice.CreateSampler(manager.NewStringToken(), samplerOp);
            var areaImage = manager.CreateEmptyTextureData<DdsVulkanTextureData>(manager.NewStringToken())
                .LoadTextureData(lDevice, StaticResource.AreaTexDX10, new ImageAndImageViewCreateOption()
                {
                    DescriptorType = DescriptorType.CombinedImageSampler,
                    ImageCreateOption = new ImageCreateOption()
                    {
                        ArrayLayers = 1,
                        Extent = new Extent3D(160, 560, 1),
                        Format = Format.R8G8B8A8Unorm,
                        ImageType = ImageType.Type2D,
                        MipLevels = 1,
                        SharingMode = SharingMode.Exclusive,
                        Samples = SampleCountFlags.Count1Bit
                    },
                    ImageViewCreateOption = new ImageViewCreateOption()
                    {
                        Components = new ComponentMapping(ComponentSwizzle.R, ComponentSwizzle.G, ComponentSwizzle.B, ComponentSwizzle.A),
                        Format = Format.R8G8B8A8Unorm,
                        SubresourceRange = new ImageSubresourceRange(ImageAspectFlags.ColorBit, 0, 1, 0, 1),
                        ViewType = ImageViewType.Type2D
                    }
                });
            var searchImage = manager.CreateEmptyTextureData<DdsVulkanTextureData>(manager.NewStringToken())
                .LoadTextureData(lDevice, StaticResource.SearchTex, new ImageAndImageViewCreateOption()
                {
                    DescriptorType = DescriptorType.CombinedImageSampler,
                    ImageCreateOption = new ImageCreateOption()
                    {
                        ArrayLayers = 1,
                        Extent = new Extent3D(64, 16, 1),
                        Format = Format.R8Unorm,
                        ImageType = ImageType.Type2D,
                        MipLevels = 1,
                        SharingMode = SharingMode.Exclusive,
                        Samples = SampleCountFlags.Count1Bit
                    },
                    ImageViewCreateOption = new ImageViewCreateOption()
                    {
                        Components = new ComponentMapping(ComponentSwizzle.R, ComponentSwizzle.G, ComponentSwizzle.B, ComponentSwizzle.A),
                        Format = Format.R8Unorm,
                        SubresourceRange = new ImageSubresourceRange(ImageAspectFlags.ColorBit, 0, 1, 0, 1),
                        ViewType = ImageViewType.Type2D
                    }
                });
            Task.WaitAll(areaImage, searchImage);
            var areaTex = manager.CreateTexture(manager.NewStringToken(), areaImage.Result, vkSampler, 2, 0, 1, 0, DescriptorType.CombinedImageSampler);
            var searchTex = manager.CreateTexture(manager.NewStringToken(), searchImage.Result, vkSampler, 3, 0, 1, 0, DescriptorType.CombinedImageSampler);
            pipeline.AddPipelineTexture(0, 2, areaTex);
            pipeline.AddPipelineTexture(0, 3, searchTex);
        }
        private void SMAAMainFunc(PrimaryVulkanRenderGroup mainGroup, Area area)
        {
            var smaaPass = mainGroup.RenderPass;
            var smaaEdgePipe = mainGroup.Pipeline;
            Span<byte> areaData = MemoryMarshal.AsBytes(stackalloc float[] { area.Width, area.Height });
            var edgeImage = smaaPass.Framebuffer[0].Attachments[1];

            var gBuffer = smaaPass.GetFramebuffer(0);
            var cmdBuffer = mainGroup.PrimaryCommandBuffer;
            cmdBuffer.BeginRenderPass(smaaPass, gBuffer, smaaPass.BeginOption, SubpassContents.Inline);
            cmdBuffer.BindPipeline(PipelineBindPoint.Graphics, smaaEdgePipe);
            cmdBuffer.SetViewport(new Viewport(0, 0, area.Width, area.Height, 0, 1));
            cmdBuffer.SetScissor(new Rect2D(new Offset2D(0, 0), new Extent2D(area.Width, area.Height)));
            DescriptorSet[] sets = [smaaEdgePipe.GetPipelineDescriptorSet(0)];
            var uniform = smaaEdgePipe.GetPipelineTexture(0, 0);
            uniform.UpdateTextureMemoryData(areaData);
            cmdBuffer.BindDescriptorSets(PipelineBindPoint.Graphics, smaaEdgePipe.PipelineLayout, 0, null, sets);
            cmdBuffer.Draw(4, 1, 0, 0);

            cmdBuffer.NextSubpass(SubpassContents.Inline);
            foreach (var weight in mainGroup.GetChildrens())
            {
                var weightPipe = weight.Pipeline;
                cmdBuffer.BindPipeline(PipelineBindPoint.Graphics, weightPipe);
                cmdBuffer.SetViewport(new Viewport(0, 0, area.Width, area.Height, 0, 1));
                cmdBuffer.SetScissor(new Rect2D(new Offset2D(0, 0), new Extent2D(area.Width, area.Height)));
                sets = [weightPipe.GetPipelineDescriptorSet(0)];
                uniform = weightPipe.GetPipelineTexture(0, 0);
                uniform.UpdateTextureMemoryData(areaData);
                cmdBuffer.BindDescriptorSets(PipelineBindPoint.Graphics, weightPipe.PipelineLayout, 0, null, sets);
                cmdBuffer.Draw(4, 1, 0, 0);

                cmdBuffer.NextSubpass(SubpassContents.Inline);
                foreach(var blend in weight.GetChildrens())
                {
                    var blendPipe = blend.Pipeline;
                    cmdBuffer.BindPipeline(PipelineBindPoint.Graphics, blendPipe);
                    cmdBuffer.SetViewport(new Viewport(0, 0, area.Width, area.Height, 0, 1));
                    cmdBuffer.SetScissor(new Rect2D(new Offset2D(0, 0), new Extent2D(area.Width, area.Height)));
                    sets = [blendPipe.GetPipelineDescriptorSet(0)];
                    uniform = blendPipe.GetPipelineTexture(0, 0);
                    uniform.UpdateTextureMemoryData(areaData);
                    cmdBuffer.BindDescriptorSets(PipelineBindPoint.Graphics, blendPipe.PipelineLayout, 0, null, sets);
                    cmdBuffer.Draw(4, 1, 0, 0);
                }
            }

            cmdBuffer.EndRenderPass();
        }
        #endregion

        /// <summary>
        /// 创建主渲染组
        /// </summary>
        /// <param name="area"></param>
        /// <param name="lDevice"></param>
        /// <param name="dispatcher"></param>
        /// <param name="manager"></param>
        /// <param name="pass"></param>
        /// <param name="wapper"></param>
        /// <returns></returns>
        protected PrimaryVulkanRenderGroup CreateMainGroup(Area area, VulkanDevice lDevice,
            VulkanGraphicsManager manager, VulkanRenderWapper wapper, VulkanSemaphore renderFinishSemaphore)
        {
            var pass = CreateOffScreenRenderpass();
            var fence = lDevice.CreateFence();
            var pCommand = lDevice.CreateCommandPool((device, option) =>
            {
                option.Flags = CommandPoolCreateFlags.ResetCommandBufferBit;
                option.QueueFamilyIndex = device.WorkQueues[0].FamilyIndex;
            }).CreatePrimaryCommandBuffer();
            var opaquePipe = CreateMainOpaquePipeline(area, manager, pass, lDevice.PipelineCache);
            var transparentPipe = CreateMainTransparentPipeline(area, manager, pass, lDevice.PipelineCache);
            var mainGroup = new EngineCoreRenderGroup($"{BuildInRenderGroup.StandardEngineMainGroup}-{wapper.Identifier}", manager.ServiceProvider, null, GroupRenderMode.Core);
            mainGroup.Binding(pCommand, lDevice, pass, [renderFinishSemaphore.RawHandle], [fence.RawHandle]);
            var opaqueGroup = manager.CreatePrimaryRenderGroup(manager.NewStringToken(), opaquePipe, GroupRenderMode.Opaque)
                .Binding(mainGroup);
            var transGroup = manager.CreatePrimaryRenderGroup(manager.NewStringToken(), transparentPipe, GroupRenderMode.Transparent)
                .Binding(mainGroup);
            mainGroup.OpaqueGroup = opaqueGroup;
            mainGroup.TransparentGroup = transGroup;

            var postPass = CreatePostTransparentRenderpass();
            var postPipeline = CreateMainPostTransparentPipeline(area, manager, postPass, lDevice.PipelineCache)
                .BuildUniformBuffer(0, 0, 64, SharingMode.Exclusive);
            var postGroup = manager.CreatePrimaryRenderGroup(manager.NewStringToken(), postPipeline)
                .Binding(pCommand, lDevice, postPass, [], []);
            PostTreatmentGroup = postGroup;
            OutputPass = postPass;

            mainGroup.CreateGroupRenderingAction((builder) =>
            {
                builder.Run((group) => MainGroupFunc(group));
            });
            return mainGroup;
        }
        /// <summary>
        /// 创建渲染包装器
        /// </summary>
        /// <param name="provider"></param>
        /// <param name="dispatcher"></param>
        /// <param name="modules"></param>
        /// <returns></returns>
        protected VulkanRenderWapper CreateRenderWapper(IServiceProvider provider, params Type[] modules)
        {
            VulkanRenderWapper wapper = GraphicsManager.CreateRenderWapper<VulkanRenderWapper>(null);
            wapper.RenderDispatcher = this;
            wapper.RenderArea = RenderArea;
            wapper.AddWapperModules(modules);
            return wapper;
        }
        /// <summary>
        /// 创建默认场景
        /// </summary>
        /// <param name="provider"></param>
        /// <param name="wapper"></param>
        /// <param name="group"></param>
        /// <returns></returns>
        protected Scene CreateDefaultScene(IServiceProvider provider, VulkanRenderWapper wapper, PrimaryVulkanRenderGroup group)
        {
            var scene = provider.CreateAutowiredInstance<OctreeScene>();
            scene.Name = !string.IsNullOrEmpty(Identifier) ? Identifier : $"{BuildInScene.StandardEngineScene}-{wapper.Identifier}";
            scene.RenderGroup = group;
            var eventBus = ServiceProvider.CreateAutowiredInstance<SystemEventBus>();
            SystemEventBus = eventBus;
            scene.ContextManager = ContextManager;
            scene.EventBus = eventBus;
            scene.ContextManager = ContextManager;
            scene.Input = Input;
            scene.RenderArea = RenderArea;
            VulkanLightEnvironment light = new(scene, GraphicsManager);
            scene.LightEnvironment = light;
            scene.IsEnabled = true;
            Scene = scene;
            return scene;
        }

        protected readonly List<VulkanSubmitSemaphore> SubmitSemaphores = [];
        protected readonly List<EngineQueueItem> OpaqueRendingItems = [];
        protected readonly List<EngineQueueItem> TransparentItems = [];
        protected readonly List<BufferHandle> Handles = [];
        protected VulkanPipeline OpaquePipeline;
        protected VulkanPipeline TransparentPipeline;
        protected long Counter = 0;
        protected Semaphore RenderFinishSemaphore;
        /// <summary>
        /// 主执行函数
        /// </summary>
        /// <param name="group"></param>
        protected void MainGroupFunc(PrimaryVulkanRenderGroup group)
        {
            var scene = Scene;
            try
            {
                SubmitSemaphores.Clear();
                var queue = group.WorkQueues[0].Queue;
                var cmdBuffer = group.PrimaryCommandBuffer;
                var semaphores = group.Semaphores;
                var pass = group.RenderPass;
                var lDevice = group.LogicDevice;
                var area = scene.RenderArea;

                PriorityQueue.Clear();
                OpaqueRendingItems.Clear();
                TransparentItems.Clear();
                foreach(var child in group.GetChildrens())
                {
                    foreach (var item in child.GetStaticObjects())
                    {
                        var com = item.GetComponents<EngineAreaComponent>().FirstOrDefault();
                        if (com == null) continue;
                        var g = com.AreaGroup;
                        if (g == null || !g.IsEnable() || g.Semaphores == null || g.Semaphores.Length < 1) continue;
                        EngineQueueItem queueItem = new EngineQueueItem(g, item, g.Priority);
                        PriorityQueue.Enqueue(queueItem, g.Priority);
                    }
                }
                while (PriorityQueue.TryDequeue(out var pGroup, out int p))
                {
                    var g = pGroup.Group;
                    g.GroupRendering();
                    if (!g.IsEnable()) continue;
                    SubmitSemaphores.Add(new VulkanSubmitSemaphore(g.Semaphores[0], PipelineStageFlags.BottomOfPipeBit));
                    if (pGroup.Priority < (int)GroupRenderMode.Transparent) OpaqueRendingItems.Add(pGroup);
                    else TransparentItems.Add(pGroup);
                }


                VulkanFrameBuffer framebuffer = group.RenderPass.GetFramebuffer(0);
                cmdBuffer.Reset();
                cmdBuffer.Begin();
                cmdBuffer.BeginRenderPass(pass, framebuffer, pass.BeginOption, SubpassContents.Inline);

                cmdBuffer.BindPipeline(PipelineBindPoint.Graphics, OpaquePipeline);
                cmdBuffer.SetViewport(new Viewport(0, 0, area.Width, area.Height, 0, 1));
                cmdBuffer.SetScissor(new Rect2D(new Offset2D(0, 0), new Extent2D(area.Width, area.Height)));
                foreach(var queueItem in OpaqueRendingItems)
                {
                    var obj = queueItem.AreaRender;
                    var p = queueItem.Priority;
                    var tex = obj.GetComponents<VulkanTexture>().FirstOrDefault();
                    if (tex == null) continue;
                    DescriptorSet[] stes = [tex.DescriptorSet];
                    cmdBuffer.BindDescriptorSets(PipelineBindPoint.Graphics, OpaquePipeline.PipelineLayout, 0, null, stes);
                    cmdBuffer.Draw(4, 1, 0, 0);
                }

                cmdBuffer.BindPipeline(PipelineBindPoint.Graphics, TransparentPipeline);
                cmdBuffer.SetViewport(new Viewport(0, 0, area.Width, area.Height, 0, 1));
                cmdBuffer.SetScissor(new Rect2D(new Offset2D(0, 0), new Extent2D(area.Width, area.Height)));
                foreach (var queueItem in TransparentItems)
                {
                    var obj = queueItem.AreaRender;
                    var p = queueItem.Priority;
                    var tex = obj.GetComponents<VulkanTexture>().FirstOrDefault();
                    if (tex == null) continue;
                    DescriptorSet[] stes = [tex.DescriptorSet];
                    cmdBuffer.BindDescriptorSets(PipelineBindPoint.Graphics, TransparentPipeline.PipelineLayout, 0, null, stes);
                    cmdBuffer.Draw(4, 1, 0, 0);
                }
                cmdBuffer.EndRenderPass();

                AAGroup.GroupRendering(0);

                var postPipe = PostTreatmentGroup.Pipeline;
                var postPass = PostTreatmentGroup.RenderPass;
                var outputImage = AAGroup.RenderPass.GetFramebuffer(0).Attachments[0];
                var uniform = postPipe.GetPipelineTexture(0, 0);
                uniform.UpdateTextureMemoryData(MemoryMarshal.AsBytes(stackalloc float[] { area.Width, area.Height }));
                cmdBuffer.BeginRenderPass(postPass, postPass.Framebuffer[0], postPass.BeginOption, SubpassContents.Inline);
                cmdBuffer.BindPipeline(PipelineBindPoint.Graphics, postPipe);
                cmdBuffer.SetViewport(new Viewport(0, 0, scene.RenderArea.Width, scene.RenderArea.Height, 0, 1));
                cmdBuffer.SetScissor(new Rect2D(new Offset2D(0, 0), new Extent2D(scene.RenderArea.Width, scene.RenderArea.Height)));
                cmdBuffer.BindDescriptorSets(PipelineBindPoint.Graphics, postPipe.PipelineLayout, 0, null, [postPipe.GetPipelineDescriptorSet(0)]);
                cmdBuffer.Draw(4, 1, 0, 0);
                cmdBuffer.EndRenderPass();
                cmdBuffer.End();

                RenderFinishSemaphore = semaphores[0];
                var submitInfo = new SubmitOption
                {
                    WaitDstStageMask = SubmitSemaphores.Select((t) => t.Stage).ToArray(),
                    WaitSemaphores = SubmitSemaphores.Select((t) => t.Semaphore).ToArray(),
                    Buffers = [cmdBuffer.RawHandle],
                    SignalSemaphores = [semaphores[0]]
                };
                lDevice.Submit(queue, new Fence(0), submitInfo);

            }
            catch (Exception e)
            {
                Logger.LogError(e.Message + "\r\n" + e.StackTrace);
                group.Disable();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="updateRate"></param>
        protected void Rendering(double updateRate = 60)
        {
            FPS = new FPSUtil(updateRate);
            var startTime = DateTime.Now;
            var endTime = startTime.AddMilliseconds(500);
            var logger = GraphicsManager.Logger;
            TimeSpan timer = TimeSpan.Zero;
            TimeSpan stamp = TimeSpan.Zero;
            SpinWait wait = new SpinWait();
            while (!CancellationToken.IsCancellationRequested)
            {
                int s = stop;
                if (Interlocked.CompareExchange(ref stop, s, s) != s)
                {
                    wait.SpinOnce();
                    continue;
                }
                if (s == 1)
                {
                    FPS.Sleep();
                    continue;
                }
                try
                {
                    IsRendering = true;
                    FPS.Begin();
                    RenderingForAvalonia(stamp);
                    _Input.RefreshMouseStateFrame();
                    _Input.RefreshKeyboardFrame();
                    if (LocalMode == ThreadIntervalMode.YieldMode) FPS.Yield();
                    else FPS.Sleep();
                    FPS.CallFPS();
                    stamp = FPS.GetLastStamp();
                    startTime = DateTime.Now;
                    if (startTime > endTime) endTime = startTime.AddMilliseconds(500);
                    if (timer <= TimeSpan.FromMilliseconds(100)) timer += stamp;
                    else
                    {
                        timer = TimeSpan.Zero;
                        _Input.MouseState.XDelta = 0;
                        _Input.MouseState.YDelta = 0;
                        _Input.MouseState.WDelta = 0;
                    }
                }
                catch (Exception e)
                {
                    logger.LogError(e.Message + "\r\n" + e.StackTrace);
                    break;
                }
                finally
                {
                    IsRendering = false;
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="timeSpan"></param>
        protected void RenderingForAvalonia(TimeSpan timeSpan)
        {
            if (!render.IsRenderInitialized) return;
            render.NextFrame();
            ContextManager.RefreshUpdateQueue();
            var eventBus = SystemEventBus;
            var scene = Scene;
            eventBus.UpdateEvent(EventTrigger.SystemFrameBefore);
            scene.ExecuteComponentSystem(timeSpan);
            eventBus.UpdateEvent(EventTrigger.SystemFrameAfter);
            scene.Rendering();
            var pass = OutputPass;
            var attach = pass.Framebuffer[0].Attachments[(int)TargetAttachment];
            render.TransferToOutput(attach, new PixelSize((int)scene.RenderArea.Width, (int)scene.RenderArea.Height));
            render.Present();
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            double aspect = (double)PixelWidth / PixelHeight;
            double h = (finalSize.Height % 4 == 0) ? finalSize.Height : ((int)finalSize.Height / 4 - 1) * 4;
            double w = Math.Round(h * aspect);
            if(w > finalSize.Width)
            {
                w = (finalSize.Width % 4 == 0) ? finalSize.Width : ((int)finalSize.Width / 4 - 1) * 4;
                h = w / aspect;
            }
            else w = w % 4 == 0 ? w : ((int)w / 4 - 1) * 4;
            finalSize = new Size(w, h);
            return base.ArrangeOverride(finalSize);
        }


        protected override void OnLoaded(RoutedEventArgs e)
        {
            base.OnLoaded(e);
            back.PointerEntered += Render_PointerEntered;
            back.PointerExited += Render_PointerExited;
            back.PointerMoved += Render_PointerMoved;
            back.PointerPressed += Render_PointerPressed;
            back.PointerReleased += Render_PointerReleased;
            back.PointerCaptureLost += Render_PointerCaptureLost;
            back.PointerWheelChanged += Render_PointerWheelChanged;
            BeginRendering(120);
        }

        protected override void OnDetachedFromVisualTree(VisualTreeAttachmentEventArgs e)
        {
            back.PointerEntered -= Render_PointerEntered;
            back.PointerExited -= Render_PointerExited;
            back.PointerMoved -= Render_PointerMoved;
            back.PointerPressed -= Render_PointerPressed;
            back.PointerReleased -= Render_PointerReleased;
            back.PointerCaptureLost -= Render_PointerCaptureLost;
            back.PointerWheelChanged -= Render_PointerWheelChanged;
            Stop();
            base.OnDetachedFromVisualTree(e);
        }

        private void Render_PointerEntered(object sender, PointerEventArgs e)
        {
            Fource = true;
            _Input.MouseState.XDelta = 0;
            _Input.MouseState.YDelta = 0;
        }
        private void Render_PointerExited(object sender, PointerEventArgs e)
        {
            Fource = false;
            _Input.MouseState.XDelta = 0;
            _Input.MouseState.YDelta = 0;
            _Input.MouseButtonStateChanged(new MouseStateFrame(DateTime.Now, SgrA.Input.MouseButton.ButtonLeft, InputAction.Release, InputMods.None));
            _Input.MouseButtonStateChanged(new MouseStateFrame(DateTime.Now, SgrA.Input.MouseButton.ButtonRight, InputAction.Release, InputMods.None));
            _Input.MouseButtonStateChanged(new MouseStateFrame(DateTime.Now, SgrA.Input.MouseButton.ButtonMiddle, InputAction.Release, InputMods.None));
        }
        private void Render_PointerMoved(object sender, PointerEventArgs e)
        {
            if (render == null) return;
            if (Fource)
            {
                int oldX = PositionX;
                int oldY = PositionY;
                var bounds = render.Bounds;
                double imageX = bounds.Width;
                double imageY = bounds.Height;
                double aspnctX = imageX / PixelWidth;
                double aspnctY = imageY / PixelHeight;
                var point = e.GetPosition(render);
                double pX = point.X / aspnctX;
                double pY = point.Y / aspnctY;
                PositionX = (int)pX; PositionY = (int)pY;
                int difX = oldX - PositionX;
                int difY = oldY - PositionY;
                var local = _Input.MouseState;
                if (difX != 0 || difY != 0)
                {
                    local.XDelta = difX;
                    local.YDelta = difY;
                }
                else
                {
                    local.XDelta = 0;
                    local.YDelta = 0;
                }
                local.X = (int)pX;
                local.Y = (int)pY;
            }
        }
        private void Render_PointerReleased(object sender, PointerReleasedEventArgs e)
        {
            if (Fource)
            {
                var m = e.GetCurrentPoint(this);
                var mouse = GetMouseButton(m.Properties);
                _Input.MouseButtonStateChanged(new MouseStateFrame(DateTime.Now, mouse, InputAction.Release, InputMods.None));
            }
        }
        private void Render_PointerPressed(object sender, PointerPressedEventArgs e)
        {
            var m = e.GetCurrentPoint(this);
            var mouse = GetMouseButton(m.Properties);
            _Input.MouseButtonStateChanged(new MouseStateFrame(DateTime.Now, mouse, InputAction.Press, InputMods.None));
        }
        private void Render_PointerCaptureLost(object sender, PointerCaptureLostEventArgs e)
        {
        }
        private void Render_PointerWheelChanged(object sender, PointerWheelEventArgs e)
        {
            if (Fource)
            {
                _Input.MouseState.WDelta = (int)e.Delta.Y;
            }
        }
        private void OnNewSceneModuleEvent(NewSceneModuleEventArgs args)
        {
            if (args == null || string.IsNullOrEmpty(args.SceneName)) return;
            if(Scene != null && Scene.Name == args.SceneName)
            {
                Scene.LoadSceneModuleDynamic(args.ModuleType);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RenderConfigs_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (sender is GlobalRenderConfigs config)
            {
                if (e.PropertyName == "WorldMatrix" && Scene != null)
                {
                    Scene.WorldMatrix = config.WorldMatrix;
                }
            }
        }
        /// <summary>
        /// 获取鼠标状态
        /// </summary>
        /// <param name="button"></param>
        /// <returns></returns>
        protected Input.MouseButton GetMouseButton(PointerPointProperties button)
        {
            return button.PointerUpdateKind switch
            {
                PointerUpdateKind.LeftButtonPressed => SgrA.Input.MouseButton.ButtonLeft,
                PointerUpdateKind.LeftButtonReleased => SgrA.Input.MouseButton.ButtonLeft,
                PointerUpdateKind.RightButtonPressed => SgrA.Input.MouseButton.ButtonRight,
                PointerUpdateKind.RightButtonReleased => SgrA.Input.MouseButton.ButtonRight,
                PointerUpdateKind.MiddleButtonPressed => SgrA.Input.MouseButton.ButtonMiddle,
                PointerUpdateKind.MiddleButtonReleased => SgrA.Input.MouseButton.ButtonMiddle,
                _ => SgrA.Input.MouseButton.ButtonLast
            };
        }
    }

    public struct EngineQueueItem
    {
        public PrimaryVulkanRenderGroup Group;
        public IRenderObject AreaRender;
        public int Priority;

        internal EngineQueueItem(PrimaryVulkanRenderGroup group, IRenderObject @object, int p)
        {
            Group = group;
            AreaRender = @object;
            Priority = p;
        }
    }
}
