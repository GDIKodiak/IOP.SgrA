﻿using Avalonia.Rendering.Composition;
using IOP.Halo;
using IOP.SgrA.SilkNet.Vulkan;
using Silk.NET.Vulkan;
using SharpDX.DXGI;
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Silk.NET.Vulkan.Extensions.KHR;
using Avalonia.Platform;
using System.Runtime.InteropServices;
using Avalonia;
using D3DDevice = SharpDX.Direct3D11.Device;
using Queue = Silk.NET.Vulkan.Queue;
using Semaphore = Silk.NET.Vulkan.Semaphore;
using System.Resources;
using System.Collections;
using Avalonia.Animation;
using Avalonia.LogicalTree;
using Avalonia.VisualTree;
using System.Threading;
using Avalonia.Threading;
using Avalonia.Interactivity;
using Avalonia.Input;

namespace IOP.SgrA.Editor.VulkanEngine
{
    /// <summary>
    /// 
    /// </summary>
    public class VulkanDrawingSurface : GPUDrawingSurface
    {
        /// <summary>
        /// 
        /// </summary>
        public const int FrameCounts = 3;
        protected Vk Api {  get; set; }
        protected Queue Queue { get; set; }
        protected VulkanDevice Device { get; set; }
        protected D3DDevice D3DDevice { get; set; }
        protected VulkanSemaphore VkImageAvailable {  get; set; }
        protected VulkanSemaphore VkRenderFinish { get; set; }
        /// <summary>
        /// 上一帧信号量
        /// </summary>
        protected VulkanSemaphore PreviewSemaphore { get; set; }
        protected VulkanCommandBuffer AcquireCommandBuffer {  get; set; }
        protected VulkanCommandBuffer TransferCommandBuffer {  get; set; }
        protected VulkanFence Fence { get; set; }
        protected PixelSize RenderSize 
        { 
            get
            {
                lock (SyncRoot)
                {
                    return _renderSize;
                }
            }
            set
            {
                lock (SyncRoot)
                {
                    _renderSize = value;
                }
            }
        }
        /// <summary>
        /// 当前帧下标
        /// </summary>
        protected int FrameIndex
        {
            get
            {
                SpinWait wait = new SpinWait();
                int local = _FrameIndex;
                while(Interlocked.CompareExchange(ref _FrameIndex, local, local) != local)
                {
                    wait.SpinOnce();
                    local = _FrameIndex;
                }
                return local;
            }
            set
            {
                if (value < 0) return;
                Interlocked.Exchange(ref _FrameIndex, value);
            }
        }

        protected readonly List<SurfaceFrame> SwapChainFrames = new(3);

        protected double RenderScaling {  get; set; }
        protected ulong Counter;
        protected readonly object SyncRoot = new object();

        private PixelSize _renderSize;
        private int _FrameIndex;

        public VulkanDrawingSurface()
        {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="api"></param>
        /// <param name="workQueue"></param>
        /// <param name="device"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public VulkanDrawingSurface(Vk api, Queue workQueue, VulkanDevice device, VulkanSemaphore preView)
        {
            Api = api;
            Queue = workQueue;
            Device = device;
            PreviewSemaphore = preView;
        }

        /// <summary>
        /// 初始化资源对象
        /// </summary>
        /// <param name="api"></param>
        /// <param name="workQueue"></param>
        /// <param name="device"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public void InitResource(Vk api, Queue workQueue, VulkanDevice device, VulkanSemaphore preView)
        {
            Api = api;
            Queue = workQueue;
            Device = device ?? throw new ArgumentNullException(nameof(device));
            PreviewSemaphore = preView;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public IPlatformHandle Export(SurfaceFrame frame)
        {
            if (!IsRenderInitialized) throw new InvalidOperationException("The reander is not readly");
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                if (frame.Texture2D == null) throw new InvalidOperationException("Texture2D lost in windows platform");
                var texture2D = frame.Texture2D;
                using var dxgi = texture2D!.QueryInterface<Resource1>();
                var handle = new PlatformHandle(dxgi.CreateSharedHandle(null, SharedResourceFlags.Read | SharedResourceFlags.Write),
                    KnownPlatformGraphicsExternalImageHandleTypes.D3D11TextureNtHandle);
                return handle;
            }
            else
            {
                var image = frame.Image.VulkanImage;
                var handle = new PlatformHandle(new IntPtr(ExportFd(image)),
                    KnownPlatformGraphicsExternalImageHandleTypes.VulkanOpaquePosixFileDescriptor);
                return handle;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        public void TransferToOutput(VulkanImageView source, PixelSize sourceSize)
        {
            if (!IsRenderInitialized) throw new InvalidOperationException("The reander is not readly");
            var frame = SwapChainFrames[FrameIndex];
            var dstSize = RenderSize;
            var iamgeBilt = new ImageBlit()
            {
                SrcSubresource = new ImageSubresourceLayers()
                {
                    AspectMask = ImageAspectFlags.ColorBit,
                    BaseArrayLayer = 0,
                    LayerCount = 1,
                    MipLevel = 0
                },
                SrcOffsets = new ImageBlit.SrcOffsetsBuffer
                {
                    Element0 = new Offset3D(),
                    Element1 = new Offset3D(sourceSize.Width, sourceSize.Height, 1)
                },
                DstSubresource = new ImageSubresourceLayers()
                {
                    AspectMask = ImageAspectFlags.ColorBit,
                    BaseArrayLayer = 0,
                    LayerCount = 1,
                    MipLevel = 0
                },
                DstOffsets = new ImageBlit.DstOffsetsBuffer()
                {
                    Element0 = new Offset3D(0, frame.PixelSize.Height - dstSize.Height, 0),
                    Element1 = new Offset3D(dstSize.Width, frame.PixelSize.Height, 1)
                }
            };
            TransferCommandBuffer.Reset();
            TransferCommandBuffer.Begin();
            TransferCommandBuffer.BlitImage(source.VulkanImage, ImageLayout.TransferSrcOptimal, frame.Image.VulkanImage,
                ImageLayout.TransferDstOptimal, iamgeBilt, Silk.NET.Vulkan.Filter.Linear);
            TransferCommandBuffer.TransitionImageLayout(frame.Image.VulkanImage, ImageLayout.TransferDstOptimal, ImageLayout.TransferSrcOptimal,
                new ImageSubresourceRange()
                {
                    AspectMask = ImageAspectFlags.ColorBit,
                    BaseArrayLayer = 0,
                    BaseMipLevel = 0,
                    LayerCount = 1,
                    LevelCount = 1
                });
            TransferCommandBuffer.End();
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                Submit(TransferCommandBuffer, [VkImageAvailable.RawHandle], [PipelineStageFlags.TransferBit], null, Fence.RawHandle, new KeyedMutexSubmitInfo()
                {
                    ReleaseKey = 1,
                    DeviceMemory = frame.Memory.RawHandle
                });
            }
            else Submit(TransferCommandBuffer, [VkImageAvailable.RawHandle], [PipelineStageFlags.TransferBit], [VkRenderFinish.RawHandle], Fence.RawHandle, null);
        }
        /// <summary>
        /// 
        /// </summary>
        public override void Present()
        {
            base.Present();
            Interlocked.Exchange(ref Counter, Counter + 1);
        }

        protected override void AcquireNextImage(ICompositionGpuInterop gpuInterop)
        {
            if (NeedStop) return;
            SpinWait wait = new SpinWait();
            if (!IsRenderInitialized) throw new InvalidOperationException("The reander is not readly");
            while (IsQueueUpdate) { wait.SpinOnce(); }
            var frame = AcquireNextFrame(); VulkanImage image;
            image = frame.Image.VulkanImage;
            AcquireCommandBuffer.Reset();
            AcquireCommandBuffer.Begin(CommandBufferUsageFlags.OneTimeSubmitBit);
            AcquireCommandBuffer.TransitionImageLayout(image, ImageLayout.TransferSrcOptimal, ImageLayout.TransferDstOptimal,
                new ImageSubresourceRange()
                {
                    AspectMask = ImageAspectFlags.ColorBit,
                    BaseArrayLayer = 0,
                    BaseMipLevel = 0,
                    LayerCount = 1,
                    LevelCount = 1
                });
            AcquireCommandBuffer.End();
            ulong c = Counter;
            while(Interlocked.CompareExchange(ref Counter, c, c) != c)
            {
                wait.SpinOnce();
                c = Counter;
            }
            if (c == 0)
            {
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                {
                    Submit(AcquireCommandBuffer, null, null, [VkImageAvailable.RawHandle], null, new KeyedMutexSubmitInfo()
                    {
                        AcquireKey = 0,
                        DeviceMemory = frame.Memory.RawHandle
                    });
                }
                else Submit(AcquireCommandBuffer, null, null, [VkImageAvailable.RawHandle], null, null);
            }
            else
            {
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                {
                    Submit(AcquireCommandBuffer, [PreviewSemaphore.RawHandle], [PipelineStageFlags.TopOfPipeBit], [VkImageAvailable.RawHandle], null, new KeyedMutexSubmitInfo()
                    {
                        AcquireKey = 0,
                        DeviceMemory = frame.Memory.RawHandle
                    });
                }
                else Submit(AcquireCommandBuffer, [PreviewSemaphore.RawHandle], [PipelineStageFlags.TopOfPipeBit], [VkImageAvailable.RawHandle], null, null);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected SurfaceFrame AcquireNextFrame()
        {
            var size = RenderSize;
            int frameIndex = FrameIndex;
            frameIndex = (frameIndex + 1) % FrameCounts;
            var frame = SwapChainFrames[frameIndex];
            while(frame.PresentImage != null && !frame.PresentImage.ImportCompleted.IsCompleted)
            {
                frameIndex = (frameIndex + 1) % FrameCounts;
                frame = SwapChainFrames[frameIndex];
            }
            if (size.Width > frame.PixelSize.Width || size.Height > frame.PixelSize.Height)
            {
                var old = frame;
                var newframe = CreateNewFrame(size);
                frame = newframe;
                old.Dispose();
            }
            FrameIndex = frameIndex;
            IsQueueUpdate = true;
            return frame;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gpuInterop"></param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        protected override ICompositionImportedGpuSemaphore CreateImageAvailableSemaphore(ICompositionGpuInterop gpuInterop)
        {
            if (!IsRenderInitialized) throw new InvalidOperationException("The reander is not readly");
            if (VkImageAvailable == null || VkImageAvailable.RawHandle.Handle == 0)
            {
                var vkSemaphore = CreateVulkanSemaphore();
                VkImageAvailable = vkSemaphore;
            }
            if(ImageAvailable == null || ImageAvailable.IsLost)
            {
                bool updateOld = false; ICompositionImportedGpuSemaphore old = ImageAvailable;
                if (old != null && old.IsLost) updateOld = true;
                var s = gpuInterop.ImportSemaphore(new PlatformHandle(new IntPtr(ExportFd(VkImageAvailable)), 
                    KnownPlatformGraphicsExternalSemaphoreHandleTypes.VulkanOpaquePosixFileDescriptor));
                ImageAvailable = s;
                if (updateOld) old?.DisposeAsync();
            }
            return ImageAvailable;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="gpuInterop"></param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        protected override ICompositionImportedGpuSemaphore CreateRenderFinishSemaphore(ICompositionGpuInterop gpuInterop)
        {
            if (!IsRenderInitialized) throw new InvalidOperationException("The reander is not readly");
            if (VkRenderFinish == null || VkImageAvailable.RawHandle.Handle == 0)
            {
                var vkSemaphore = CreateVulkanSemaphore();
                VkRenderFinish = vkSemaphore;
            }
            if (RenderFinish == null || RenderFinish.IsLost)
            {
                bool updateOld = false; ICompositionImportedGpuSemaphore old = RenderFinish;
                if (old != null && old.IsLost) updateOld = true;
                var s = gpuInterop.ImportSemaphore(new PlatformHandle(new IntPtr(ExportFd(VkRenderFinish)), 
                    KnownPlatformGraphicsExternalSemaphoreHandleTypes.VulkanOpaquePosixFileDescriptor));
                RenderFinish = s;
                if (updateOld) old?.DisposeAsync();
            }
            return RenderFinish;
        }
        /// <summary>
        /// 获取当前帧数据图片
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        protected override ICompositionImportedGpuImage GetCurrentFrameImage(ICompositionGpuInterop gpuInterop)
        {
            var frame = SwapChainFrames[FrameIndex];
            var size = frame.PixelSize;
            var inportImage = gpuInterop.ImportImage(Export(frame), new PlatformGraphicsExternalImageProperties()
            {
                Format = PlatformGraphicsExternalImageFormat.R8G8B8A8UNorm,
                Width = size.Width,
                Height = size.Height,
                MemorySize = frame.MemorySize
            });
            frame.PresentImage?.DisposeAsync();
            frame.PresentImage = inportImage;
            return inportImage;
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void UpdateFrame()
        {
            FrameAction?.Invoke();
            ICompositionImportedGpuImage image = GetCurrentFrameImage(GpuInterop);
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                _ = Surface.UpdateWithKeyedMutexAsync(image, 1, 0);
            else
            {
                ImageAvailable ??= CreateImageAvailableSemaphore(GpuInterop);
                RenderFinish ??= CreateRenderFinishSemaphore(GpuInterop);
                _ = Surface.UpdateWithSemaphoresAsync(image, RenderFinish, null);
            }
            IsQueueUpdate = false;
        }

        protected override void OnLoaded(RoutedEventArgs e)
        {
            base.OnLoaded(e);
            Initialize();
        }

        /// <summary>
        /// 初始化显示资源
        /// </summary>
        /// <param name="compositor"></param>
        /// <param name="compositionDrawingSurface"></param>
        /// <param name="gpuInterop"></param>
        /// <returns></returns>
        protected override (bool success, string info) InitializeGraphicsResources(Compositor compositor, 
            CompositionDrawingSurface compositionDrawingSurface, ICompositionGpuInterop gpuInterop)
        {
            try
            {
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                {
                    D3DDevice = CreateD3Device();
                }
                var semaphore1 = Device.CreateSemaphore();
                VkImageAvailable = semaphore1;
                var semaphore2 = Device.CreateSemaphore();
                VkRenderFinish = semaphore2;
                Fence = Device.CreateFence();
                CreateCommandBuffer();

                var root = this.GetVisualRoot();
                var size = PixelSize.FromSize(Bounds.Size, root.RenderScaling);
                RenderScaling = root.RenderScaling;
                RenderSize = size;
                CreateSwapChainFrames(size);
                return (true, "");
            }
            catch (Exception e)
            {
                return (false, e.ToString());
            }
        }

        protected int ExportFd(VulkanImage image)
        {
            if (!Api.TryGetDeviceExtension<KhrExternalMemoryFd>(Device.Instance, Device.RawHandle, out var ext))
                throw new InvalidOperationException();
            var info = new MemoryGetFdInfoKHR
            {
                Memory = image.DeviceMemory.RawHandle,
                SType = StructureType.MemoryGetFDInfoKhr,
                HandleType = ExternalMemoryHandleTypeFlags.OpaqueFDBit
            };
            var result = ext.GetMemoryF(Device.RawHandle, info, out var fd);
            if (result != Result.Success) throw new VulkanOperationException("GetMemoryF", result);
            return fd;
        }
        protected int ExportFd(VulkanSemaphore semaphore)
        {
            if (!Api.TryGetDeviceExtension<KhrExternalSemaphoreFd>(Device.Instance, Device.RawHandle, out var ext))
                throw new InvalidOperationException();
            var info = new SemaphoreGetFdInfoKHR()
            {
                SType = StructureType.SemaphoreGetFDInfoKhr,
                Semaphore = semaphore.RawHandle,
                HandleType = ExternalSemaphoreHandleTypeFlags.OpaqueFDBit
            };
            var result = ext.GetSemaphoreF(Device.RawHandle, info, out var fd);
            if (result != Result.Success) throw new VulkanOperationException("GetSemaphoreF", result);
            return fd;
        }

        protected Texture2D CreateMemoryHandle(D3DDevice device, PixelSize size, Silk.NET.Vulkan.Format format)
        {
            if (format != Silk.NET.Vulkan.Format.R8G8B8A8Unorm)
                throw new ArgumentException("Not supported format");
            return new Texture2D(device,
                new Texture2DDescription
                {
                    Format = SharpDX.DXGI.Format.R8G8B8A8_UNorm,
                    Width = size.Width,
                    Height = size.Height,
                    ArraySize = 1,
                    MipLevels = 1,
                    SampleDescription = new SampleDescription { Count = 1, Quality = 0 },
                    CpuAccessFlags = default,
                    OptionFlags = ResourceOptionFlags.SharedKeyedmutex | ResourceOptionFlags.SharedNthandle,
                    BindFlags = BindFlags.RenderTarget | BindFlags.ShaderResource
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        protected unsafe (PhysicalDeviceProperties2, PhysicalDeviceIDProperties) GetPhysicalDeviceProperties2(VulkanDevice device)
        {
            var physicalDeviceIDProperties = new PhysicalDeviceIDProperties()
            {
                SType = StructureType.PhysicalDeviceIDProperties
            };
            var physicalDeviceProperties2 = new PhysicalDeviceProperties2()
            {
                SType = StructureType.PhysicalDeviceProperties2,
                PNext = &physicalDeviceIDProperties
            };
            var phy = device.PhysicalDevice;
            Api.GetPhysicalDeviceProperties2(phy.RawHandle, &physicalDeviceProperties2);
            return (physicalDeviceProperties2, physicalDeviceIDProperties);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        protected unsafe D3DDevice CreateD3Device()
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                (_, PhysicalDeviceIDProperties physicalDeviceIDProperties) = GetPhysicalDeviceProperties2(Device);
                if (physicalDeviceIDProperties.DeviceLuidvalid)
                {
                    var d3dDevice = CreateDeviceByLuid(new Span<byte>(physicalDeviceIDProperties.DeviceLuid, 8));
                    return d3dDevice;
                }
                else throw new NotSupportedException("Device with the corresponding LUID not found");
            }
            else throw new NotSupportedException("Cannot create D3DDevice, is not windows platform");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="luid"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        protected D3DDevice CreateDeviceByLuid(Span<byte> luid)
        {
            var factory = new Factory1();
            var longLuid = MemoryMarshal.Cast<byte, long>(luid)[0];
            for (var c = 0; c < factory.GetAdapterCount1(); c++)
            {
                using var adapter = factory.GetAdapter1(0);
                if (adapter.Description1.Luid != longLuid)
                    continue;

                return new D3DDevice(adapter, DeviceCreationFlags.None, 
                    [ 
                        FeatureLevel.Level_12_1, FeatureLevel.Level_12_0, FeatureLevel.Level_11_1,
                        FeatureLevel.Level_11_0, FeatureLevel.Level_10_0, FeatureLevel.Level_9_3,
                        FeatureLevel.Level_9_2, FeatureLevel.Level_9_1,
                    ]);
            }

            throw new ArgumentException("Device with the corresponding LUID not found");
        }

        /// <summary>
        /// 创建信号量
        /// </summary>
        /// <returns></returns>
        /// <exception cref="VulkanOperationException"></exception>
        protected unsafe VulkanSemaphore CreateVulkanSemaphore()
        {
            var semaphoreExportInfo = new ExportSemaphoreCreateInfo
            {
                SType = StructureType.ExportSemaphoreCreateInfo,
                HandleTypes = ExternalSemaphoreHandleTypeFlags.OpaqueFDBit
            };
            var semaphoreCreateInfo = new SemaphoreCreateInfo
            {
                SType = StructureType.SemaphoreCreateInfo,
                PNext = &semaphoreExportInfo
            };
            var result = Api.CreateSemaphore(Device.RawHandle, semaphoreCreateInfo, null, out var semaphore);
            if (result != Result.Success) throw new VulkanOperationException("CreateSemaphore", result);
            VulkanSemaphore vulkanSemaphore = new VulkanSemaphore(Api, Device.RawHandle, semaphore);
            return vulkanSemaphore;
        }
        /// <summary>
        /// 创建交换链帧缓冲
        /// </summary>
        /// <param name="size"></param>
        protected unsafe void CreateSwapChainFrames(PixelSize size)
        {
            if(SwapChainFrames.Count > 0)
            {
                foreach(var frame in SwapChainFrames) { frame.Dispose(); }
                SwapChainFrames.Clear();
            }
            for(int i = 0; i < FrameCounts; i++)
            {
                var frame = CreateNewFrame(size);
                SwapChainFrames.Add(frame);
            }
        }
        /// <summary>
        /// 创建新的图像帧
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        /// <exception cref="NotSupportedException"></exception>
        /// <exception cref="VulkanOperationException"></exception>
        protected unsafe SurfaceFrame CreateNewFrame(PixelSize size)
        {
            var handleType = RuntimeInformation.IsOSPlatform(OSPlatform.Windows) ? 
                ExternalMemoryHandleTypeFlags.D3D11TextureBit :
                ExternalMemoryHandleTypeFlags.OpaqueFDBit;
            var format = Silk.NET.Vulkan.Format.R8G8B8A8Unorm;
            FormatFeatureFlags flags = FormatFeatureFlags.SampledImageBit | 
                FormatFeatureFlags.TransferDstBit | FormatFeatureFlags.TransferSrcBit;
            ImageUsageFlags usage = ImageUsageFlags.SampledBit | 
                ImageUsageFlags.TransferDstBit | ImageUsageFlags.TransferSrcBit;
            ImageTiling tiling;
            var phy = Device.PhysicalDevice;
            var prop = phy.GetFormatProperties(format);
            if (prop.OptimalTilingFeatures.HasFlag(flags)) tiling = ImageTiling.Optimal;
            else if (prop.LinearTilingFeatures.HasFlag(flags)) tiling = ImageTiling.Linear;
            else
            {
                var prop2 = phy.GetFormatProperties2(format);
                if (prop2.FormatProperties.OptimalTilingFeatures.HasFlag(flags)) tiling = ImageTiling.Optimal;
                else if (prop2.FormatProperties.LinearTilingFeatures.HasFlag(flags)) tiling = ImageTiling.Linear;
                throw new NotSupportedException($"Image Format {format} is not supported");
            }
            var externalMemoryCreateInfo = new ExternalMemoryImageCreateInfo
            {
                SType = StructureType.ExternalMemoryImageCreateInfo,
                HandleTypes = handleType
            };
            var imageCreateInfo = new ImageCreateInfo
            {
                PNext = &externalMemoryCreateInfo,
                SType = StructureType.ImageCreateInfo,
                ImageType = ImageType.Type2D,
                Format = format,
                Extent = new Extent3D((uint?)size.Width, (uint?)size.Height, 1),
                MipLevels = 1,
                ArrayLayers = 1,
                Samples = SampleCountFlags.Count1Bit,
                Tiling = tiling,
                Usage = usage,
                SharingMode = SharingMode.Exclusive,
                InitialLayout = ImageLayout.Undefined,
                Flags = ImageCreateFlags.CreateMutableFormatBit
            };
            var result = Api.CreateImage(Device.RawHandle, in imageCreateInfo, null, out Image pImage);
            if (result != Result.Success) throw new VulkanOperationException("CreateImage", result);
            Api.GetImageMemoryRequirements(Device.RawHandle, pImage, out var memoryRequirements);
            var fdExport = new ExportMemoryAllocateInfo
            {
                HandleTypes = handleType,
                SType = StructureType.ExportMemoryAllocateInfo
            };
            var dedicatedAllocation = new MemoryDedicatedAllocateInfoKHR
            {
                SType = StructureType.MemoryDedicatedAllocateInfoKhr,
                Image = pImage
            };
            ImportMemoryWin32HandleInfoKHR handleImport = default;
            Texture2D texture2D = null;
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                texture2D = CreateMemoryHandle(D3DDevice, size, Silk.NET.Vulkan.Format.R8G8B8A8Unorm);
                using var dxgi = texture2D!.QueryInterface<Resource1>();
                handleImport = new ImportMemoryWin32HandleInfoKHR
                {
                    PNext = &dedicatedAllocation,
                    SType = StructureType.ImportMemoryWin32HandleInfoKhr,
                    HandleType = ExternalMemoryHandleTypeFlags.D3D11TextureBit,
                    Handle = dxgi.CreateSharedHandle(null, SharedResourceFlags.Read | SharedResourceFlags.Write),
                };
            }
            bool success = Device.MemoryTypeFromProperties(memoryRequirements.MemoryTypeBits, MemoryPropertyFlags.DeviceLocalBit, out int mIndex);
            if (!success) throw new NotSupportedException("Current Device is not supported to create memory form DeviceLocalBit with image format R8G8B8A8Unorm");
            var memoryAllocateInfo = new MemoryAllocateInfo
            {
                PNext = RuntimeInformation.IsOSPlatform(OSPlatform.Windows) ? &handleImport : &fdExport,
                SType = StructureType.MemoryAllocateInfo,
                AllocationSize = memoryRequirements.Size,
                MemoryTypeIndex = (uint)mIndex
            };
            result = Api.AllocateMemory(Device.RawHandle, in memoryAllocateInfo, null, out DeviceMemory pMemory);
            if (result != Result.Success) throw new VulkanOperationException("AllocateMemory", result);
            VulkanDeviceMemory memory = new VulkanDeviceMemory(Api, Device.RawHandle, pMemory);
            ulong memorySize = memoryRequirements.Size;
            result = Api.BindImageMemory(Device.RawHandle, pImage, pMemory, 0);
            if (result != Result.Success) throw new VulkanOperationException("BindImageMemory", result);
            VulkanImage image = new VulkanImage(Api, Device.RawHandle, pImage);
            var componentMapping = new ComponentMapping(ComponentSwizzle.Identity, 
                ComponentSwizzle.Identity, ComponentSwizzle.Identity, ComponentSwizzle.Identity);
            var imageViewCreateInfo = new ImageViewCreateInfo
            {
                SType = StructureType.ImageViewCreateInfo,
                Image = pImage,
                ViewType = ImageViewType.Type2D,
                Format = Silk.NET.Vulkan.Format.R8G8B8A8Unorm,
                Components = componentMapping,
                SubresourceRange = new ImageSubresourceRange()
                {
                    AspectMask = ImageAspectFlags.ColorBit,
                    BaseArrayLayer = 0,
                    BaseMipLevel = 0,
                    LayerCount = 1,
                    LevelCount = 1
                }
            };
            result = Api.CreateImageView(Device.RawHandle, in imageViewCreateInfo, null, out ImageView pView);
            if (result != Result.Success) throw new VulkanOperationException("CreateImageView", result);
            VulkanImageView view = new VulkanImageView(Api, Device.RawHandle, pView, image);
            SurfaceFrame frame = new SurfaceFrame(view, texture2D, memory, memorySize, size);
            var localCommandBuffer = Device.CreateCommandPool((device, option) =>
            {
                option.Flags = CommandPoolCreateFlags.TransientBit;
                option.QueueFamilyIndex = device.WorkQueues[0].FamilyIndex;
            }).CreatePrimaryCommandBuffer();
            var fence = Device.CreateFence();
            localCommandBuffer.Begin();
            localCommandBuffer.TransitionImageLayout(image, ImageLayout.Undefined, ImageLayout.TransferSrcOptimal,
                new ImageSubresourceRange()
                {
                    AspectMask = ImageAspectFlags.ColorBit,
                    BaseArrayLayer = 0,
                    BaseMipLevel = 0,
                    LayerCount = 1,
                    LevelCount = 1
                });
            localCommandBuffer.End();
            localCommandBuffer.SubmitSingleCommand(Device, Queue, fence.RawHandle);
            Device.DestroyFence(fence);
            return frame;
        }
        /// <summary>
        /// 创建命令缓冲
        /// </summary>
        protected unsafe void CreateCommandBuffer()
        {
            var pCommand = Device.CreateCommandPool((device, option) =>
            {
                option.Flags = CommandPoolCreateFlags.ResetCommandBufferBit;
                option.QueueFamilyIndex = device.WorkQueues[0].FamilyIndex;
            }).CreatePrimaryCommandBuffers(2);
            AcquireCommandBuffer = pCommand[0];
            TransferCommandBuffer = pCommand[1];
        }

        protected unsafe void Submit(VulkanCommandBuffer command, ReadOnlySpan<Semaphore> waitSemaphores,
                ReadOnlySpan<PipelineStageFlags> waitDstStageMask = default,
                ReadOnlySpan<Semaphore> signalSemaphores = default,
                Silk.NET.Vulkan.Fence? fence = null,
                KeyedMutexSubmitInfo keyedMutex = null)
        {
            ulong acquireKey = keyedMutex?.AcquireKey ?? 0, releaseKey = keyedMutex?.ReleaseKey ?? 0;
            DeviceMemory devMem = keyedMutex?.DeviceMemory ?? default;
            uint timeout = uint.MaxValue;
            Win32KeyedMutexAcquireReleaseInfoKHR mutex = default;
            if (keyedMutex != null)
            {
                mutex = new Win32KeyedMutexAcquireReleaseInfoKHR
                {
                    SType = StructureType.Win32KeyedMutexAcquireReleaseInfoKhr,
                    AcquireCount = keyedMutex.AcquireKey.HasValue ? 1u : 0u,
                    ReleaseCount = keyedMutex.ReleaseKey.HasValue ? 1u : 0u,
                    PAcquireKeys = keyedMutex.AcquireKey.HasValue ? &acquireKey : null,
                    PReleaseKeys = keyedMutex.ReleaseKey.HasValue ? &releaseKey : null,
                    PAcquireSyncs = keyedMutex.AcquireKey.HasValue ? &devMem : null,
                    PReleaseSyncs = keyedMutex.ReleaseKey.HasValue ? &devMem : null,
                    PAcquireTimeouts = &timeout
                };
            }
            fixed (Semaphore* pWaitSemaphores = waitSemaphores, pSignalSemaphores = signalSemaphores)
            {
                fixed (PipelineStageFlags* pWaitDstStageMask = waitDstStageMask)
                {
                    var commandBuffer = command.RawHandle;
                    var submitInfo = new SubmitInfo
                    {
                        PNext = keyedMutex != null ? &mutex : null,
                        SType = StructureType.SubmitInfo,
                        WaitSemaphoreCount = waitSemaphores != null ? (uint)waitSemaphores.Length : 0,
                        PWaitSemaphores = pWaitSemaphores,
                        PWaitDstStageMask = pWaitDstStageMask,
                        CommandBufferCount = 1,
                        PCommandBuffers = &commandBuffer,
                        SignalSemaphoreCount = signalSemaphores != null ? (uint)signalSemaphores.Length : 0,
                        PSignalSemaphores = pSignalSemaphores,
                    };
                    Api.QueueSubmit(Queue, 1, in submitInfo, fence.HasValue ? fence.Value : default);
                    if (fence.HasValue)
                    {
                        Span<Silk.NET.Vulkan.Fence> fences = [fence.Value];
                        Result r;
                        do
                        {
                            r = Device.WaitForFences(fences, true, 100000000);
                        } while (r == Result.Timeout);
                        Device.ResetFences(fences);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="change"></param>
        protected override void OnPropertyChanged(AvaloniaPropertyChangedEventArgs change)
        {
            if (change.Property == BoundsProperty)
            {
                var root = this.GetVisualRoot();
                PixelSize size = PixelSize.FromSize(Bounds.Size, root.RenderScaling);
                RenderScaling = root.RenderScaling;
                RenderSize = size;
            }
            base.OnPropertyChanged(change);
        }
        /// <summary>
        /// 释放资源
        /// </summary>
        protected override void FreeResource()
        {
            SpinWait wait = new SpinWait();
            while (IsQueueUpdate) { wait.SpinOnce(); }
            foreach(var frame in SwapChainFrames)
            {
                while(frame.PresentImage != null && !frame.PresentImage.ImportCompleted.IsCompleted)
                {
                    wait.SpinOnce();
                }
                frame.Dispose();
            }
            SwapChainFrames.Clear();
            PreviewSemaphore?.Dispose();
            VkRenderFinish?.Dispose();
            VkImageAvailable?.Dispose();
            D3DDevice?.Dispose();
            Device = null;
        }

        protected override void OnDetachedFromVisualTree(VisualTreeAttachmentEventArgs e)
        {
            base.OnDetachedFromVisualTree(e);
        }
    }

    public class SurfaceFrame
    {
        internal VulkanImageView Image;
        internal Texture2D Texture2D;
        internal VulkanDeviceMemory Memory;
        internal ulong MemorySize;
        internal PixelSize PixelSize;
        internal ICompositionImportedGpuImage PresentImage;
        internal SurfaceFrame(VulkanImageView image, Texture2D texture, VulkanDeviceMemory memory, ulong memSize, PixelSize size)
        {
            Image = image;
            Texture2D = texture;
            Memory = memory;
            MemorySize = memSize;
            PixelSize = size;
        }

        internal void Dispose()
        {
            PresentImage?.DisposeAsync();
            Image?.Dispose();
            Memory?.Dispose();
            Texture2D?.Dispose();
        }
    }

    public class KeyedMutexSubmitInfo
    {
        public ulong? AcquireKey;
        public ulong? ReleaseKey;
        public DeviceMemory DeviceMemory;
    }
}
