﻿using Avalonia.Platform;
using IOP.SgrA.SilkNet.Vulkan;
using Microsoft.Extensions.Logging;
using Silk.NET.Vulkan;
using Silk.NET.Vulkan.Extensions.EXT;
using Silk.NET.Vulkan.Extensions.KHR;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.VulkanEngine
{
    /// <summary>
    /// Vulkan引擎加载器
    /// </summary>
    public class VulkanEngineLoader : IEngineLoader
    {
        private readonly VulkanGraphicsManager Manager;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="manager"></param>
        public VulkanEngineLoader(VulkanGraphicsManager manager)
        {
            Manager = manager ?? throw new ArgumentNullException(nameof(manager));
        }

        /// <summary>
        /// 加载引擎
        /// </summary>
        /// <returns></returns>
        public ValueTask LoadEngine()
        {
            try
            {
                Manager.CreateVulkanInstance((appInfo) =>
                {
                    appInfo.ApplicationName = "IOP.SgrA.Editor";
                    appInfo.ApplicationVersion = new Versions(1, 0, 0);
                    appInfo.EngineName = "IOP.SgrA";
                    appInfo.EngineVersion = new Versions(1, 0, 0);
                    appInfo.ApiVersion = new Versions(1, 3, 0);
                }, (instanceInfo) => 
                {
                    instanceInfo.EnabledExtensionNames = new string[] 
                    { 
                    };
                });
                return CreateDefalutEngine();
            }
            catch (Exception e)
            {
                Manager.Logger.LogError(e, $"{nameof(LoadEngine)} failed with exception");
            }
            return ValueTask.CompletedTask;
        }
        /// <summary>
        /// 加载携带调试模块的引擎
        /// </summary>
        /// <returns></returns>
        public ValueTask LoadEngineDebug()
        {
            try
            {
                Manager.CreateVulkanInstance((appInfo) =>
                {
                    appInfo.ApplicationName = "IOP.SgrA.Editor";
                    appInfo.ApplicationVersion = new Versions(1, 0, 0);
                    appInfo.EngineName = "IOP.SgrA";
                    appInfo.EngineVersion = new Versions(1, 0, 0);
                    appInfo.ApiVersion = new Versions(1, 3, 0);
                }, (instanceInfo) =>
                {
                    instanceInfo.EnabledExtensionNames = 
                    [
                        ExtDebugReport.ExtensionName
                    ];
                    instanceInfo.EnabledLayerNames = ["VK_LAYER_KHRONOS_validation"];
                }).CreateDebugReportCallback(DebugReport);
                return CreateDefalutEngine();
            }
            catch (Exception e)
            {
                Manager.Logger.LogError(e, $"{nameof(LoadEngineDebug)} failed with exception");
            }
            return ValueTask.CompletedTask;
        }
        /// <summary>
        /// 卸载引擎
        /// </summary>
        /// <returns></returns>
        public bool DisposeEngine()
        {
            Manager.Dispose();
            return true;
        }

        /// <summary>
        /// 创建默认引擎
        /// </summary>
        /// <returns></returns>
        private ValueTask CreateDefalutEngine()
        {
            var manager = Manager;
            if(manager.PhysicalDevices == null || manager.PhysicalDevices.Length <= 0)
            {
                manager.Logger.LogError("Cannot found physical GPU device");
                return ValueTask.CompletedTask;
            }
            var pDevice = manager.PhysicalDevices[0];
            manager.QueryPhysicsPropertiesLimit(pDevice);
            manager.CreateDefaultGlobalRenderConfig();
            List<string> requireDeviceExtensions = [];
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                requireDeviceExtensions.Add(KhrExternalMemoryWin32.ExtensionName);
                requireDeviceExtensions.Add(KhrExternalSemaphoreWin32.ExtensionName);
            }
            else
            {
                requireDeviceExtensions.Add(KhrExternalMemoryFd.ExtensionName);
                requireDeviceExtensions.Add(KhrExternalSemaphoreFd.ExtensionName);
            }
            var lDevice = manager.CreateVulkanDevice(pDevice, (device) =>
            {
                device.Name = "main";
                device.EnabledExtensionNames = [.. requireDeviceExtensions];
                device.EnabledLayerNames = null;
                //var f = new Features2<PhysicalDeviceSynchronization2Features>(new PhysicalDeviceSynchronization2Features()
                //{ SType = StructureType.PhysicalDeviceSynchronization2Features, Synchronization2 = true })
                //.SetNext(new Features2<PhysicalDeviceVulkan12Features>(new PhysicalDeviceVulkan12Features()
                //{ SType = StructureType.PhysicalDeviceVulkan12Features, SeparateDepthStencilLayouts = true }));
                //device.EnabledFeatures2 = f;
              
            }, (phyDevice) =>
            {
                QueueFamilyProperties[] properties = phyDevice.GetQueueFamilyProperties();
                manager.Logger.LogInformation($"Get queue familyProperties count : {properties.Length}");
                var result = new List<QueueFamilyIndices>();
                QueueFamilyIndices index = new();
                for (uint i = 0; i < properties.Length; i++)
                {
                    if (properties[i].QueueFlags.HasFlag(QueueFlags.GraphicsBit))
                    {
                        index.FamilyType |= QueueType.Graphics;
                        index.FamilyIndex = i;
                        result.Add(index);
                        index = new QueueFamilyIndices();
                        continue;
                    }
                }
                return result.ToArray();
            });
            lDevice.InitWorkQueue((device, indeics) =>
            {
                List<WorkQueue> list = new();
                foreach (var item in indeics)
                {
                    if (item.FamilyType.HasFlag(QueueType.Graphics))
                    {
                        Queue queue = device.GetQueue(item.FamilyIndex, 0);
                        var type = QueueType.Graphics;
                        WorkQueue work = new(type, item.FamilyIndex, 0, queue);
                        list.Add(work);
                        return list;
                    }
                }
                return list;
            });
            lDevice.CreateResourcesCommandPool(0);
            lDevice.CreatePipelineCache();
            return ValueTask.CompletedTask;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="flags"></param>
        /// <param name="objectType"></param>
        /// <param name="object"></param>
        /// <param name="location"></param>
        /// <param name="messageCode"></param>
        /// <param name="layerPrefix"></param>
        /// <param name="message"></param>
        /// <param name="userData"></param>
        /// <returns></returns>
        private uint DebugReport(DebugReportFlagsEXT flags, DebugReportObjectTypeEXT objectType, ulong @object, UIntPtr location, int messageCode, string layerPrefix, string message, IntPtr userData)
        {
            if (message != null) Manager.Logger.LogError(message);
            Console.WriteLine(message);
            return 0;
        }
    }
}
