﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.VulkanEngine
{
    /// <summary>
    /// 内建模块
    /// </summary>
    public static class BuildInModule
    {
        /// <summary>
        /// 标准引擎内核模块
        /// </summary>
        public const string StandardEngineCoreModule = nameof(StandardEngineCoreModule);
    }
    /// <summary>
    /// 内建渲染组
    /// </summary>
    public static class BuildInRenderGroup
    {
        /// <summary>
        /// 标准引擎主渲染组
        /// </summary>
        public const string StandardEngineMainGroup = "StandardEngineMain";
        public const string StandardEngineFXAAGroup = "StandardEngineFXAA";
        public const string StandardEngineSMAAGroup = "StandardEngineSMAA";
        public const string StandardEngineOITGroup = "StandardEngineOITGroup";
    }
    /// <summary>
    /// 内建场景
    /// </summary>
    public static class BuildInScene
    {
        public const string StandardEngineScene = "StandardEngineScene";
    }
}
