﻿using IOP.SgrA.SilkNet.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.VulkanEngine
{
    /// <summary>
    /// 
    /// </summary>
    public class EngineCoreRenderGroup : PrimaryVulkanRenderGroup
    {
        /// <summary>
        /// 
        /// </summary>
        public PrimaryVulkanRenderGroup OpaqueGroup{ get; internal set; }
        /// <summary>
        /// 
        /// </summary>
        public PrimaryVulkanRenderGroup TransparentGroup {  get; internal set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="serviceProvider"></param>
        /// <param name="shader"></param>
        /// <param name="mode"></param>
        public EngineCoreRenderGroup(string name, IServiceProvider serviceProvider, VulkanPipeline shader, GroupRenderMode mode = GroupRenderMode.Opaque) 
            : base(name, serviceProvider, shader, mode)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderObject"></param>
        /// <param name="mode"></param>
        public void BindingRenderAreaObject(IRenderObject renderObject, GroupRenderMode mode)
        {
            if(mode < GroupRenderMode.Transparent)
            {
                OpaqueGroup.BindingStaticRenderObject(renderObject);
            }
            else
            {
                TransparentGroup.BindingStaticRenderObject(renderObject);
            }
        }
    }
}
