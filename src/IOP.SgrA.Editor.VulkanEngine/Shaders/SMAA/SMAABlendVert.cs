﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.VulkanEngine
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class SMAABlendVert : GLSLVert
    {
        [UniformBuffer(0, 0, DescriptorSetTarget.Pipeline)]
        public SMAAUniform Params { get; set; }

        [ShaderOutput(0)]
        public vec2 ScreenPos { get; set; }
        [ShaderOutput(1)]
        public vec4 Offset { get; set; }

        public override void main()
        {
            vec2 screenPos = new vec2(0.0f);
            if (gl_VertexIndex == 0)
            {
                gl_Position = new vec4(-1.0f, -1.0f, 0.0f, 1.0f);
                screenPos = new vec2(0.0f, 1.0f);
            }
            else if (gl_VertexIndex == 1)
            {
                gl_Position = new vec4(1.0f, -1.0f, 0.0f, 1.0f);
                screenPos = new vec2(1.0f, 1.0f);
            }
            else if (gl_VertexIndex == 2)
            {
                gl_Position = new vec4(-1.0f, 1.0f, 0.0f, 1.0f);
                screenPos = new vec2(0.0f, 0.0f);
            }
            else if (gl_VertexIndex == 3)
            {
                gl_Position = new vec4(1.0f, 1.0f, 0.0f, 1.0f);
                screenPos = new vec2(1.0f, 0.0f);
            }
            vec4 metrics = SMAA_RT_METRICS();
            ScreenPos = screenPos;
            Offset = fma(metrics.xyxy, new vec4(1.0f, 0.0f, 0.0f, 1.0f), new vec4(screenPos.xy, screenPos.xy));
        }

        private vec4 SMAA_RT_METRICS()
        {
            return new vec4(1.0f / Params.Width, 1.0f / Params.Height, Params.Width, Params.Height);
        }
    }
}
