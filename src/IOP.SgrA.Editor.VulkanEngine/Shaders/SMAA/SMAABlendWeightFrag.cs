﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.VulkanEngine
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class SMAABlendWeightFrag : GLSLFrag
    {
        [UniformBuffer(0, 0, DescriptorSetTarget.Pipeline)]
        public SMAAUniform Params { get; set; }
        [SampledImage(0, 1, DescriptorSetTarget.Pipeline)]
        public sampler2D EdgesTex { get; set; }
        [SampledImage(0, 2, DescriptorSetTarget.Pipeline)]
        public sampler2D AreaTex { get; set; }
        [SampledImage(0, 3, DescriptorSetTarget.Pipeline)]
        public sampler2D SearchTex { get; set; }

        [ShaderInput(0)]
        public vec2 Texcoord { get; set; }
        [ShaderInput(1)]
        public vec2 Pixcoord { get; set; }
        [ShaderInput(2)]
        public vec4 Offset0 { get; set; }
        [ShaderInput(3)]
        public vec4 Offset1 { get; set; }
        [ShaderInput(4)]
        public vec4 Offset2 { get; set; }

        [FragOutput(0, false)]
        public vec4 OutColor { get; set; }

        private float SMAA_CORNER_ROUNDING = 25.0f;
        private float SMAA_CORNER_ROUNDING_NORM = 0.25f;
        private float SMAA_MAX_SEARCH_STEPS_DIAG = 16.0f;
        private int SMAA_MAX_SEARCH_STEPS = 16;
        private float SMAA_AREATEX_MAX_DISTANCE = 16.0f;
        private float SMAA_AREATEX_MAX_DISTANCE_DIAG = 20.0f;
        private vec2 SMAA_SEARCHTEX_SIZE = new vec2(66.0f, 33.0f);
        private vec2 SMAA_SEARCHTEX_PACKED_SIZE = new vec2(64.0f, 16.0f);
        private float SMAA_AREATEX_SUBTEX_SIZE = 0.14285714f; //1.0f / 7.0f

        public override void main()
        {
            OutColor = SMAABlendingWeightCalculationPS(Texcoord, Pixcoord, 
                Offset0, Offset1, Offset2, new vec4(0.0f));
        }

        /// <summary>
        /// Blending Weight Calculation Pixel Shader (Second Pass)
        /// </summary>
        /// <param name="texcoord"></param>
        /// <param name="pixcoord"></param>
        /// <param name="offset0"></param>
        /// <param name="offset1"></param>
        /// <param name="offset2"></param>
        /// <param name="subsampleIndices"></param>
        /// <returns></returns>
        private vec4 SMAABlendingWeightCalculationPS(vec2 texcoord,
            vec2 pixcoord, vec4 offset0, vec4 offset1, vec4 offset2,
            vec4 subsampleIndices)
        {
            vec4 weights = new vec4(0.0f);
            vec2 e = texture(EdgesTex, texcoord).rg;
            if (e.g > 0.0f) //Edge at north
            {
                weights.rg = SMAACalculateDiagWeights(texcoord, e, subsampleIndices);

                if (weights.r == -weights.g)
                {
                    vec2 d = new vec2(0.0f);
                    vec3 coords = new vec3(0.0f);
                    coords.x = SMAASearchXLeft(offset0.xy, offset2.x);
                    coords.y = offset1.y;
                    d.x = coords.x;

                    // Now fetch the left crossing edges, two at a time using bilinear
                    // filtering. Sampling at -0.25 (see @CROSSING_OFFSET) enables to
                    // discern what value each edge has:
                    float e1 = textureLod(EdgesTex, coords.xy, 0.0f).r;

                    // Find the distance to the right:
                    coords.z = SMAASearchXRight(offset0.zw, offset2.y);
                    d.y = coords.z;

                    // We want the distances to be in pixel units (doing this here allow to
                    // better interleave arithmetic and memory accesses):
                    d = abs(round(fma(SMAA_RT_METRICS().zz, d, -pixcoord.xx)));

                    // SMAAArea below needs a sqrt, as the areas texture is compressed
                    // quadratically:
                    vec2 sqrt_d = sqrt(d);

                    // Fetch the right crossing edges:
                    float e2 = textureLodOffset(EdgesTex, coords.zy, 0.0f, new ivec2(1, 0)).r;

                    // Ok, we know how this pattern looks like, now it is time for getting
                    // the actual area:
                    weights.rg = SMAAArea(sqrt_d, e1, e2, subsampleIndices.y);

                    //Fix corners:
                    coords.y = texcoord.y;
                    SMAADetectHorizontalCornerPattern(weights.rg, new vec4(coords.xyz, coords.y), d);
                }
                else e.r = 0.0f;
            }

            if (e.r > 0.0f) // Edge at west
            {
                vec2 d = new vec2(0.0f);

                // Find the distance to the top:
                vec3 coords = new vec3(0.0f);
                coords.y = SMAASearchYUp(offset1.xy, offset2.z);
                coords.x = offset0.x;
                d.x = coords.y;

                // Fetch the top crossing edges:
                float e1 = textureLod(EdgesTex, coords.xy, 0.0f).g;

                // Find the distance to the bottom:
                coords.z = SMAASearchYDown(offset1.zw, offset2.w);
                d.y = coords.z;

                // We want the distances to be in pixel units:
                d = abs(round(fma(SMAA_RT_METRICS().ww, d, -pixcoord.yy)));

                // SMAAArea below needs a sqrt, as the areas texture is compressed 
                // quadratically:
                vec2 sqrt_d = sqrt(d);

                // Fetch the bottom crossing edges:
                float e2 = textureLodOffset(EdgesTex, coords.xz, 0.0f, new ivec2(0, 1)).g;

                // Get the area for this direction:
                weights.ba = SMAAArea(sqrt_d, e1, e2, subsampleIndices.x);

                // Fix corners:
                coords.x = texcoord.x;
                SMAADetectVerticalCornerPattern(weights.ba, new vec4(coords.xyx, coords.z), d);
            }
            return weights;
        }

        private vec2 SMAACalculateDiagWeights(vec2 texcoord, vec2 e, vec4 subsampleIndices)
        {
            vec2 weights = new vec2(0.0f);
            vec4 d = new vec4(0.0f);
            vec2 end = new vec2(0.0f);
            if (e.r > 0.0f)
            {
                d.xz = SMAASearchDiag1(texcoord, new vec2(-1.0f, 1.0f), end);
                d.x += end.y > 0.9f ? 1.0f : 0.0f;
            }
            else d.xz = new vec2(0.0f);
            d.yw = SMAASearchDiag1(texcoord, new vec2(1.0f, -1.0f), end);
            if (d.x + d.y > 2.0f)
            {
                vec4 coords = fma(new vec4(-d.x + 0.25f, d.x, d.y, -d.y - 0.25f), SMAA_RT_METRICS().xyxy,
                    new vec4(texcoord.xy, texcoord.xy));
                vec4 c = new vec4(0.0f);
                c.xy = textureLodOffset(EdgesTex, coords.xy, 0.0f, new ivec2(-1, 0)).rg;
                c.zw = textureLodOffset(EdgesTex, coords.zw, 0.0f, new ivec2(1, 0)).rg;
                c.yxwz = SMAADecodeDiagBilinearAccess(c.xyzw);

                // Merge crossing edges at each side into a single value:
                vec2 cc = fma(new vec2(2.0f, 2.0f), c.xz, c.yw);

                // Remove the crossing edge if we didn't found the end of the line:
                SMAAMovc(new bvec2(step(0.9f, d.zw)), cc, new vec2(0.0f));

                // Fetch the areas for this line:
                weights += SMAAAreaDiag(d.xy, cc, subsampleIndices.z);
            }

            // Search for the line ends:
            d.xz = SMAASearchDiag2(texcoord, new vec2(-1.0f, -1.0f), end);
            if (textureLodOffset(EdgesTex, texcoord, 0.0f, new ivec2(1, 0)).r > 0.0f)
            {
                d.yw = SMAASearchDiag2(texcoord, new vec2(1.0f, 1.0f), end);
                d.y += end.y > 0.9f ? 1.0f : 0.0f;
            }
            else d.yw = new vec2(0.0f);

            if (d.x + d.y > 2.0f)
            {
                // Fetch the crossing edges:
                vec4 coords = fma(new vec4(-d.x, -d.x, d.y, d.y), SMAA_RT_METRICS().xyxy,
                    new vec4(texcoord.xy, texcoord.x, texcoord.y));
                vec4 c = new vec4(0.0f);
                c.x = textureLodOffset(EdgesTex, coords.xy, 0.0f, new ivec2(-1, 0)).g;
                c.y = textureLodOffset(EdgesTex, coords.xy, 0.0f, new ivec2(0, -1)).r;
                c.zw = textureLodOffset(EdgesTex, coords.zw, 0.0f, new ivec2(1, 0)).gr;
                vec2 cc = fma(new vec2(2.0f), c.xz, c.yw);

                // Remove the crossing edge if we didn't found the end of the line:
                SMAAMovc(new bvec2(step(0.9f, d.zw)), cc, new vec2(0.0f));

                // Fetch the areas for this line:
                weights += SMAAAreaDiag(d.xy, cc, subsampleIndices.w).gr;
            }

            return weights;
        }

        private float SMAASearchLength(vec2 e, float offset)
        {
            // The texture is flipped vertically, with left and right cases taking half
            // of the space horizontally:
            vec2 scale = SMAA_SEARCHTEX_SIZE * new vec2(0.5f, -1.0f);
            vec2 bias = SMAA_SEARCHTEX_SIZE * new vec2(offset, 1.0f);

            // Scale and bias to access texel centers:
            scale += new vec2(-1.0f, 1.0f);
            bias += new vec2(0.5f, -0.5f);

            // Convert from pixel coordinates to texcoords:
            // (We use SMAA_SEARCHTEX_PACKED_SIZE because the texture is cropped)
            scale *= new vec2(1.0f / SMAA_SEARCHTEX_PACKED_SIZE.x, 1.0f / SMAA_SEARCHTEX_PACKED_SIZE.y);
            bias *= new vec2(1.0f / SMAA_SEARCHTEX_PACKED_SIZE.x, 1.0f / SMAA_SEARCHTEX_PACKED_SIZE.y);

            // Lookup the search texture:
            float result = textureLod(SearchTex, fma(scale, e, bias), 0.0f).r;
            return result;
        }
        private float SMAASearchXLeft(vec2 texcoord, float end)
        {
            /**
              * @PSEUDO_GATHER4
              * This texcoord has been offset by (-0.25, -0.125) in the vertex shader to
              * sample between edge, thus fetching four edges in a row.
              * Sampling with different offsets in each direction allows to disambiguate
              * which edges are active from the four fetched ones.
            */
            vec2 e = new vec2(0.0f, 1.0f);
            vec4 metrics = SMAA_RT_METRICS();
            for(int i = 0; i < SMAA_MAX_SEARCH_STEPS; i++)
            {
                if (!(texcoord.x > end && e.g > 0.8281f && e.r == 0.0f)) break;
                e = textureLod(EdgesTex, texcoord, 0.0f).rg;
                texcoord = fma(new vec2(-2.0f, 0.0f), metrics.xy, texcoord);
            }

            float offset = fma(-(255.0f / 127.0f), SMAASearchLength(e, 0.0f), 3.25f);
            return fma(metrics.x, offset, texcoord.x);
        }
        private float SMAASearchXRight(vec2 texcoord, float end)
        {
            vec2 e = new vec2(0.0f, 1.0f);
            vec4 metrics = SMAA_RT_METRICS();
            for (int i = 0; i < SMAA_MAX_SEARCH_STEPS; i++)
            {
                if (!(texcoord.x < end && e.g > 0.8281 && e.r == 0.0)) break;
                e = textureLod(EdgesTex, texcoord, 0.0f).rg;
                texcoord = fma(new vec2(2.0f, 0.0f), metrics.xy, texcoord);
            }
            float offset = fma(-(255.0f / 127.0f), SMAASearchLength(e, 0.5f), 3.25f);
            return fma(-metrics.x, offset, texcoord.x);
        }
        private float SMAASearchYUp(vec2 texcoord, float end)
        {
            vec2 e = new vec2(1.0f, 0.0f);
            vec4 metrics = SMAA_RT_METRICS();
            for (int i = 0; i < SMAA_MAX_SEARCH_STEPS; i++)
            {
                if (!(texcoord.y > end && e.r > 0.8281 && e.g == 0.0)) break;
                e = textureLod(EdgesTex, texcoord, 0.0f).rg;
                texcoord = fma(new vec2(0.0f, -2.0f), metrics.xy, texcoord);
            }
            float offset = fma(-(255.0f / 127.0f), SMAASearchLength(e.gr, 0.0f), 3.25f);
            return fma(metrics.y, offset, texcoord.y);
        }
        private float SMAASearchYDown(vec2 texcoord, float end)
        {
            vec2 e = new vec2(1.0f, 0.0f);
            vec4 metrics = SMAA_RT_METRICS();
            for (int i = 0; i < SMAA_MAX_SEARCH_STEPS; i++)
            {
                if (!(texcoord.y < end && e.r > 0.8281 && e.g == 0.0)) break;
                e = textureLod(EdgesTex, texcoord, 0.0f).rg;
                texcoord = fma(new vec2(0.0f, 2.0f), metrics.xy, texcoord);
            }
            float offset = fma(-(255.0f / 127.0f), SMAASearchLength(e.gr, 0.5f), 3.25f);
            return fma(-metrics.y, offset, texcoord.y);
        }
        private vec2 SMAAArea(vec2 dist, float e1, float e2, float offset)
        {
            vec2 areaSize = new vec2(1.0f / 160.0f, 1.0f / 560.0f);
            // Rounding prevents precision errors of bilinear filtering:
            vec2 texcoord = fma(new vec2(SMAA_AREATEX_MAX_DISTANCE, SMAA_AREATEX_MAX_DISTANCE), round(4.0f * new vec2(e1, e2)), dist);

            // We do a scale and bias for mapping to texel space:
            texcoord = fma(areaSize, texcoord, 0.5f * areaSize);

            // Move to proper place, according to the subpixel offset:
            texcoord.y = fma(1.0f / 7.0f, offset, texcoord.y);

            return textureLod(AreaTex, texcoord, 0.0f).rg;
        }
        private vec2 SMAAAreaDiag(vec2 dist, vec2 e, float offset)
        {
            vec2 texcoord = fma(new vec2(SMAA_AREATEX_MAX_DISTANCE_DIAG, SMAA_AREATEX_MAX_DISTANCE_DIAG), e, dist);
            vec2 areaSize = new vec2(1.0f / 160.0f, 1.0f / 560.0f);

            // We do a scale and bias for mapping to texel space:
            texcoord = fma(areaSize, texcoord, 0.5f * areaSize);
            // Diagonal areas are on the second half of the texture:
            texcoord.x += 0.5f;

            // Move to proper place, according to the subpixel offset:
            texcoord.y += SMAA_AREATEX_SUBTEX_SIZE * offset;

            vec2 result = textureLod(AreaTex, texcoord, 0.0f).rg;
            return result;
        }

        private vec2 SMAASearchDiag1(vec2 texcoord, vec2 dir, [Out] vec2 e)
        {
            vec4 coord = new vec4(texcoord, -1.0f, 1.0f);
            vec3 t = new vec3(SMAA_RT_METRICS().xy, 1.0f);
            while (coord.z < SMAA_MAX_SEARCH_STEPS_DIAG - 1.0f &&
                coord.w > 0.9f)
            {
                coord.xyz = fma(t, new vec3(dir, 1.0f), coord.xyz);
                e = textureLod(EdgesTex, coord.xy, 0.0f).rg;
                coord.w = dot(e, new vec2(0.5f, 0.5f));
            }
            return coord.zw;
        }
        private vec2 SMAASearchDiag2(vec2 texcoord, vec2 dir, [Out] vec2 e)
        {
            vec4 coord = new vec4(texcoord, -1.0f, 1.0f);
            coord.x += 0.25f * SMAA_RT_METRICS().x;
            vec3 t = new vec3(SMAA_RT_METRICS().xy, 1.0f);
            while (coord.z < SMAA_MAX_SEARCH_STEPS_DIAG - 1.0f &&
                coord.w > 0.9f)
            {
                coord.xyz = fma(t, new vec3(dir, 1.0f), coord.xyz);

                // @SearchDiag2Optimization
                // Fetch both edges at once using bilinear filtering:
                e = textureLod(EdgesTex, coord.xy, 0.0f).rg;
                e = SMAADecodeDiagBilinearAccess(e);

                // Non-optimized version:
                // e.g = SMAASampleLevelZero(edgesTex, coord.xy).g;
                // e.r = SMAASampleLevelZeroOffset(edgesTex, coord.xy, int2(1, 0)).r;
                coord.w = dot(e, new vec2(0.5f));
            }
            return coord.zw;
        }

        void SMAADetectHorizontalCornerPattern([In, Out] vec2 weights, vec4 texcoord, vec2 d)
        {
            vec2 leftRight = step(d.xy, d.yx);
            vec2 rounding = (1.0f - SMAA_CORNER_ROUNDING_NORM) * leftRight;

            rounding /= leftRight.x + leftRight.y; // Reduce blending for pixels in the center of a line.

            vec2 factor = new vec2(1.0f, 1.0f);
            factor.x -= rounding.x * textureLodOffset(EdgesTex, texcoord.xy, 0.0f, new ivec2(0, 1)).r;
            factor.x -= rounding.y * textureLodOffset(EdgesTex, texcoord.zw, 0.0f, new ivec2(1, 1)).r;
            factor.y -= rounding.x * textureLodOffset(EdgesTex, texcoord.xy, 0.0f, new ivec2(0, -2)).r;
            factor.y -= rounding.y * textureLodOffset(EdgesTex, texcoord.zw, 0.0f, new ivec2(1, -2)).r;

            weights *= clamp(factor, 0.0f, 1.0f);
        }
        void SMAADetectVerticalCornerPattern([In, Out] vec2 weights, vec4 texcoord, vec2 d)
        {
            vec2 leftRight = step(d.xy, d.yx);
            vec2 rounding = (1.0f - SMAA_CORNER_ROUNDING_NORM) * leftRight;

            rounding /= leftRight.x + leftRight.y;

            vec2 factor = new vec2(1.0f, 1.0f);
            factor.x -= rounding.x * textureLodOffset(EdgesTex, texcoord.xy, 0.0f, new ivec2(1, 0)).g;
            factor.y -= rounding.y * textureLodOffset(EdgesTex, texcoord.zw, 0.0f, new ivec2(1, 1)).g;
            factor.x -= rounding.x * textureLodOffset(EdgesTex, texcoord.xy, 0.0f, new ivec2(-2, 0)).g;
            factor.y -= rounding.y * textureLodOffset(EdgesTex, texcoord.zw, 0.0f, new ivec2(-2, 1)).g;

            weights *= clamp(factor, 0.0f, 1.0f);
        }

        private vec4 SMAA_RT_METRICS()
        {
            return new vec4(1.0f / Params.Width, 1.0f / Params.Height, Params.Width, Params.Height);
        }
        private vec4 SMAADecodeDiagBilinearAccess(vec4 e)
        {
            e.rb = e.rb * abs(5.0f * e.rb - 5.0f * 0.75f);
            return round(e);
        }
        private vec2 SMAADecodeDiagBilinearAccess(vec2 e)
        {
            e.r = e.r * abs(5.0f * e.r - 5.0f * 0.75f);
            return round(e);
        }
        private void SMAAMovc(bvec2 cond, [In, Out] vec2 variable, vec2 value)
        {
            if (cond.x) variable.x = value.x;
            if (cond.y) variable.y = value.y;
        }
    }
}
