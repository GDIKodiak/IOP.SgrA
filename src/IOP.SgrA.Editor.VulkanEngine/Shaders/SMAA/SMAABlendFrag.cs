﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.VulkanEngine
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class SMAABlendFrag : GLSLFrag
    {
        [UniformBuffer(0, 0, DescriptorSetTarget.Pipeline)]
        public SMAAUniform Params { get; set; }
        [SampledImage(0, 1, DescriptorSetTarget.Pipeline)]
        public sampler2D ColorTex { get; set; }
        [SampledImage(0, 2, DescriptorSetTarget.Pipeline)]
        public sampler2D WeightTex { get; set; }

        [ShaderInput(0)]
        public vec2 Texcoord {  get; set; }
        [ShaderInput(1)]
        public vec4 Offset { get; set; }

        [FragOutput(0, false, 
            ColorBlendOp = BlendOp.Add, AlphaBlendOp = BlendOp.Add,
            SrcColorBlendFactor = BlendFactor.SrcAlpha, SrcAlphaBlendFactor = BlendFactor.One,
            DstColorBlendFactor = BlendFactor.OneMinusSrcAlpha, DstAlphaBlendFactor = BlendFactor.Zero)]
        public vec4 OutColor { get; set; }
        public override void main()
        {
            vec4 color = SMAANeighborhoodBlendingPS(Texcoord, Offset);
            OutColor = color;
            //vec4 color = texture(WeightTex, Texcoord);
            //OutColor = new vec4(color.rgb, 1.0f);
        }

        private vec4 SMAANeighborhoodBlendingPS(vec2 texcoord, vec4 offset)
        {
            vec4 metrics = SMAA_RT_METRICS();
            vec4 color = new vec4(0.0f);
            vec4 a = new vec4(0.0f);
            vec4 topLeft = texture(WeightTex, texcoord); //Top/Left
            float bottom = texture(WeightTex, offset.zw).g; //Bottom
            float right = texture(WeightTex, offset.xy).a; //Right
            a += new vec4(topLeft.x, bottom, topLeft.y, right);

            vec4 w = a * a * a;
            float sum = dot(w, new vec4(1.0f, 1.0f, 1.0f, 1.0f));

            if(sum <= 0.0f)
            {
                color = textureLod(ColorTex, texcoord, 0.0f);
            }
            else
            {
                vec4 o = a * metrics.yyxx;

                color = fma(texture(ColorTex, Texcoord + new vec2(0.0f, -o.r)), 
                    new vec4(w.r, w.r, w.r, w.r), color);
                color = fma(texture(ColorTex, Texcoord + new vec2(0.0f, o.g)), 
                    new vec4(w.g, w.g, w.g, w.g), color);
                color = fma(texture(ColorTex, Texcoord + new vec2(-o.b, 0.0f)), 
                    new vec4(w.b, w.b, w.b, w.b), color);
                color = fma(texture(ColorTex, Texcoord + new vec2(o.a, 0.0f)),
                    new vec4(w.a, w.a, w.a, w.a), color);
                color /= sum;
            }
            return color;
        }

        private void SMAAMovc(bvec2 cond, [In, Out] vec2 variable, vec2 value)
        {
            if (cond.x) variable.x = value.x;
            if (cond.y) variable.y = value.y;
        }
        private void SMAAMovc(bvec4 cond, [In, Out] vec4 variable, vec4 value)
        {
            SMAAMovc(cond.xy, variable.xy, value.xy);
            SMAAMovc(cond.zw, variable.zw, value.zw);
        }
        private vec4 SMAA_RT_METRICS()
        {
            return new vec4(1.0f / Params.Width, 1.0f / Params.Height, Params.Width, Params.Height);
        }
    }
}
