﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.VulkanEngine
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class SMAAColorEdgeDetectionFrag : GLSLFrag
    {
        [SampledImage(0, 1, DescriptorSetTarget.Pipeline)]
        public sampler2D ColorTex { get; set; }
        [ShaderInput(0)]
        public vec2 Texcoord { get; set; }
        [ShaderInput(1)]
        public vec4 Offset0 {  get; set; }
        [ShaderInput(2)]
        public vec4 Offset1 { get; set; }
        [ShaderInput(3)]
        public vec4 Offset2 { get; set; }

        [FragOutput(0, false)]
        public vec4 OutputColor { get; set; }

        private float SMAA_THRESHOLD = 0.1f;

        public override void main()
        {
            vec2 threshold = new vec2(SMAA_THRESHOLD, SMAA_THRESHOLD);

            vec3 weights = new vec3(0.2126f, 0.7152f, 0.0722f);
            float L = dot(texture(ColorTex, Texcoord).rgb, weights);

            float Lleft = dot(texture(ColorTex, Offset0.xy).rgb, weights);
            float Ltop = dot(texture(ColorTex, Offset0.zw).rgb, weights);

            vec4 delta = new vec4(0.0f);
            delta.yx = abs(L - new vec2(Lleft, Ltop));
            vec2 edges = step(threshold, delta.xy);

            if (dot(edges, new vec2(1.0f, 1.0f)) == 0.0f)
                discard();

            float Lright = dot(texture(ColorTex, Offset1.xy).rgb, weights);
            float Lbottom = dot(texture(ColorTex, Offset1.zw).rgb, weights);
            delta.zw = abs(L - new vec2(Lright, Lbottom));

            vec2 maxDelta = max(delta.xy, delta.zw);

            float Lleftleft = dot(texture(ColorTex, Offset2.xy).rgb, weights);
            float Ltoptop = dot(texture(ColorTex, Offset2.zw).rgb, weights);
            delta.zw = abs(new vec2(Lleft, Ltop) - new vec2(Lleftleft, Ltoptop));

            maxDelta = max(maxDelta.xy, delta.zw);
            float finalDelta = max(maxDelta.x, maxDelta.y);

            edges *= step(finalDelta, 2.0f * delta.xy);
            OutputColor = new vec4(edges.xy, new vec2(0.0f));
        }
    }
}
