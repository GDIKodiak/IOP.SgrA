﻿using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.VulkanEngine
{
    public partial class PostTreatmentPipe : ScriptedVulkanPipeline
    {
        public PostTreatmentFrag Frag { get; set; }

        public PostTreatmentVert Vert { get; set; }

        public override PipelineInputAssemblyStateCreateOption CreateInputAssemblyState()
        {
            return new PipelineInputAssemblyStateCreateOption()
            {
                PrimitiveRestartEnable = false,
                Topology = PrimitiveTopology.TriangleStrip
            };
        }
        public override PipelineRasterizationStateCreateOption CreateRasterizationState()
        {
            return new PipelineRasterizationStateCreateOption()
            {
                PolygonMode = PolygonMode.Fill,
                CullMode = CullModeFlags.None,
                FrontFace = FrontFace.CounterClockwise,
                DepthClampEnable = false,
                RasterizerDiscardEnable = false,
                DepthBiasEnable = false,
                DepthBiasConstantFactor = 0,
                DepthBiasClamp = 0,
                DepthBiasSlopeFactor = 0,
                LineWidth = 1.0f
            };
        }

        public override PipelineDepthStencilStateCreateOption CreateDepthStencilState()
        {
            return new PipelineDepthStencilStateCreateOption()
            {
                Back = new StencilOpState()
                {
                    FailOp = StencilOp.Keep,
                    PassOp = StencilOp.Keep,
                    CompareOp = CompareOp.Always,
                    CompareMask = 0,
                    Reference = 0,
                    DepthFailOp = StencilOp.Keep,
                    WriteMask = 0
                },
                Front = new StencilOpState()
                {
                    FailOp = StencilOp.Keep,
                    PassOp = StencilOp.Keep,
                    CompareOp = CompareOp.Always,
                    CompareMask = 0,
                    Reference = 0,
                    DepthFailOp = StencilOp.Keep,
                    WriteMask = 0
                },
                DepthWriteEnable = false,
                DepthTestEnable = true,
                DepthCompareOp = CompareOp.GreaterOrEqual,
                DepthBoundsTestEnable = false,
                MinDepthBounds = 0,
                MaxDepthBounds = 0,
                StencilTestEnable = false
            };
        }

        public override PipelineDynamicStateCreateOption CreateDynamicState()
        {
            return new PipelineDynamicStateCreateOption()
            {
                DynamicStates =
                [
                    DynamicState.Viewport,
                    DynamicState.Scissor
                ]
            };
        }
    }
}
