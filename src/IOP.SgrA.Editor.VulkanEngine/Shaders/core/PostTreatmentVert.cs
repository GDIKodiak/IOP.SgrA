﻿using Avalonia.Controls;
using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;

namespace IOP.SgrA.Editor.VulkanEngine
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class PostTreatmentVert : GLSLVert
    {
        [UniformBuffer(0, 0, DescriptorSetTarget.Pipeline)]
        public ScreenUniform Uniform { get; set; }
        [ShaderOutput(0)]
        public vec2 ScreenPos { get; set; }

        public override void main()
        {
            vec2 screenPos = new vec2(0.0f);
            if (gl_VertexIndex == 0)
            {
                gl_Position = new vec4(-1.0f, -1.0f, 0.0f, 1.0f);
                screenPos = new vec2(0.0f, 0.0f);
            }
            else if (gl_VertexIndex == 1)
            {
                gl_Position = new vec4(1.0f, -1.0f, 0.0f, 1.0f);
                screenPos = new vec2(1.0f, 0.0f);
            }
            else if (gl_VertexIndex == 2)
            {
                gl_Position = new vec4(-1.0f, 1.0f, 0.0f, 1.0f);
                screenPos = new vec2(0.0f, 1.0f);
            }
            else if (gl_VertexIndex == 3)
            {
                gl_Position = new vec4(1.0f, 1.0f, 0.0f, 1.0f);
                screenPos = new vec2(1.0f, 1.0f);
            }
            //screenPos = fma(new vec2(1.0f / Uniform.Width, 1.0f / Uniform.Height), new vec2(0.5f, 0.5f), screenPos);
            ScreenPos = screenPos;
        }
    }
}
