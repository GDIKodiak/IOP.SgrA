﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.VulkanEngine
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class CoreVert : GLSLVert
    {
        [ShaderOutput(0)]
        public vec2 screenPos {  get; set; }

        public override void main()
        {
            if (gl_VertexIndex == 0)
            {
                gl_Position = new vec4(-1.0f, -1.0f, 0.0f, 1.0f);
                screenPos =  new vec2(0.0f, 1.0f);
            }
            else if (gl_VertexIndex == 1)
            {
                gl_Position = new vec4(1.0f, -1.0f, 0.0f, 1.0f);
                screenPos = new vec2(1.0f, 1.0f);
            }
            else if (gl_VertexIndex == 2)
            {
                gl_Position = new vec4(-1.0f, 1.0f, 0.0f, 1.0f);
                screenPos = new vec2(0.0f, 0.0f);
            }
            else if (gl_VertexIndex == 3)
            {
                gl_Position = new vec4(1.0f, 1.0f, 0.0f, 1.0f);
                screenPos = new vec2(1.0f, 0.0f);
            }
        }
    }
}
