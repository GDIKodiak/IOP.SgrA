﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.VulkanEngine
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class CoreFXAAFrag : GLSLFrag
    {
        [SampledImage(0, 0, SilkNet.Vulkan.DescriptorSetTarget.Pipeline)]
        public sampler2D in_color { get; set; }
        [ShaderInput(0)]
        public vec2 in_uv { get; set; }
        [FragOutput(0, false)]
        public vec4 out_color { get; set; }
        [PushConstant(12, 0)]
        public FXAAConstant constantVal { get; set; }

        int UP_LEFT = 0;
        int UP = 1;
        int UP_RIGHT = 2;
        int LEFT = 3;
        int CENTER = 4;
        int RIGHT = 5;
        int DOWN_LEFT = 6;
        int DOWN = 7;
        int DOWN_RIGHT = 8;
        int STEP_COUNT_MAX = 12;
        float EDGE_THRESHOLD_MIN = 0.0312f;
        float EDGE_THRESHOLD_MAX = 0.125f;
        float SUBPIXEL_QUALITY = 0.75f;
        float GRADIENT_SCALE = 0.25f;

        public vec2[] STEP_MAT = new vec2[]
        {
            new vec2(-1.0f, 1.0f), new vec2(0.0f, 1.0f), new vec2(1.0f, 1.0f),
            new vec2(-1.0f, 0.0f), new vec2(0.0f, 0.0f), new vec2(1.0f, 0.0f),
            new vec2(-1.0f,-1.0f), new vec2(0.0f, -1.0f),new vec2(1.0f,-1.0f)
        };
        float RGB2LUMA(vec3 rgb_color)
        {
            return dot(new vec3(0.299f, 0.578f, 0.114f), rgb_color);
        }
        float QUALITY(int i)
        {
            if (i < 5) return 1.0f;
            if (i == 5) return 1.5f;
            if (i < 10) return 2.0f;
            if (i == 10) return 4.0f;
            if (i == 11) return 8.0f;
            return 8.0f;
        }

        vec3 ACESToneMapping(vec3 color, float adapted_lum)
        {
            float A = 2.51f;
            float B = 0.03f;
            float C = 2.43f;
            float D = 0.59f;
            float E = 0.14f;

            color *= adapted_lum;
            return (color * (A * color + B)) / (color * (C * color + D) + E);
        }

        public override void main()
        {
            vec2 uv_step = new vec2(1.0f / constantVal.screenWidth, 1.0f / constantVal.screenHeight);
            float[] luma_mat = new float[9];
            for (int i = 0; i < 9; i++) luma_mat[i] = RGB2LUMA(texture(in_color, in_uv + uv_step * STEP_MAT[i]).rgb);
            float luma_max = max(luma_mat[CENTER], max(max(luma_mat[LEFT], luma_mat[RIGHT]), max(luma_mat[UP], luma_mat[DOWN])));
            float luma_min = min(luma_mat[CENTER], min(min(luma_mat[LEFT], luma_mat[RIGHT]), min(luma_mat[UP], luma_mat[DOWN])));
            if (luma_max - luma_min < max(EDGE_THRESHOLD_MIN, luma_max * EDGE_THRESHOLD_MAX))
            {
                out_color = texture(in_color, in_uv);
                return;
            }
            float luma_horizontal = abs(luma_mat[UP_LEFT] + luma_mat[DOWN_LEFT] - 2.0f * luma_mat[LEFT]) + 
                abs(luma_mat[UP_RIGHT] + luma_mat[DOWN_RIGHT] - 2.0f * luma_mat[RIGHT]) + 
                abs(luma_mat[UP] + luma_mat[DOWN] - 2.0f * luma_mat[CENTER]);
            float luma_vertial = abs(luma_mat[UP_LEFT] + luma_mat[UP_RIGHT] - 2.0f * luma_mat[UP]) + 
                abs(luma_mat[DOWN_LEFT] + luma_mat[DOWN_RIGHT] - 2.0f * luma_mat[DOWN]) + 
                abs(luma_mat[LEFT] + luma_mat[RIGHT] - 2.0f * luma_mat[CENTER]);
            bool is_horizontal = abs(luma_horizontal) > abs(luma_vertial);
            float grandient_down_left = is_horizontal ? luma_mat[DOWN] - luma_mat[CENTER] : luma_mat[LEFT] - luma_mat[CENTER]; 
            float grandient_up_right = is_horizontal ? luma_mat[UP] - luma_mat[CENTER] : luma_mat[RIGHT] - luma_mat[CENTER];
            bool is_down_left = abs(grandient_down_left) > abs(grandient_up_right);
            float gradient_start = is_down_left ? grandient_down_left : grandient_up_right;
            vec2 step_tangent = is_horizontal ? new vec2(1.0f, 0.0f) * uv_step : new vec2(0.0f, 1.0f) * uv_step;
            vec2 step_normal;
            if (is_down_left)
            {
                if (is_horizontal) step_normal = -1.0f * new vec2(0.0f, 1.0f) * uv_step;
                else step_normal = -1.0f * new vec2(1.0f, 0.0f) * uv_step;
            }
            else
            {
                if (is_horizontal) step_normal = 1.0f * new vec2(0.0f, 1.0f) * uv_step;
                else step_normal = 1.0f * new vec2(1.0f, 0.0f) * uv_step;
            }
            vec2 uv_start = in_uv + 0.5f * step_normal;
            float luma_average_start = luma_mat[CENTER] + 0.5f * gradient_start;

            vec2 uv_forward = uv_start;
            vec2 uv_backward = uv_start;
            float delta_luma_forward = 0.0f;
            float delta_luma_backward = 0.0f;
            bool reached_forward = false;
            bool reached_backward = false;
            bool reached_both;

            for (int i = 1; i < STEP_COUNT_MAX; i++)
            {
                if (!reached_forward) uv_forward += QUALITY(i) * step_tangent;
                if (!reached_backward) uv_backward += -QUALITY(i) * step_tangent;
                delta_luma_forward = RGB2LUMA(texture(in_color, uv_forward).rgb) - luma_average_start;
                delta_luma_backward = RGB2LUMA(texture(in_color, uv_backward).rgb) - luma_average_start;

                reached_forward = abs(delta_luma_forward) > GRADIENT_SCALE * abs(gradient_start);
                reached_backward = abs(delta_luma_backward) > GRADIENT_SCALE * abs(gradient_start);
                reached_both = reached_forward && reached_backward;

                if (reached_both) break;
            }

            float length_forward = max(abs(uv_forward - uv_start).x, abs(uv_forward - uv_start).y);
            float length_backward = max(abs(uv_backward - uv_start).x, abs(uv_backward - uv_start).y);
            bool is_forward_near = length_forward < length_backward;
            float pixel_offset = is_forward_near ? length_forward : length_backward;
            pixel_offset /= (length_forward + length_backward) + 0.5f;
            if (((is_forward_near ? delta_luma_forward : delta_luma_backward) < 0.0) ==
                (luma_mat[CENTER] < luma_average_start)) pixel_offset = 0.0f;

            float luma_average_center = 0.0f;
            float[] average_weight_mat = new float[9]
            {
                    0.0f, 2.0f, 1.0f,
                    2.0f, 0.0f, 2.0f,
                    1.0f, 2.0f, 1.0f
            };
            for (int i = 0; i < 9; i++) luma_average_center += average_weight_mat[i] * luma_mat[i];
            luma_average_center /= 12.0f;

            float subpixel_luma_range = clamp(abs(luma_average_center - luma_mat[CENTER]) / (luma_max - luma_min), 0.0f, 1.0f);
            float subpixel_offset = (-2.0f * subpixel_luma_range + 3.0f) * subpixel_luma_range * subpixel_luma_range;
            subpixel_offset = subpixel_offset * subpixel_offset * SUBPIXEL_QUALITY;
            pixel_offset = max(pixel_offset, subpixel_offset);

            out_color = texture(in_color, in_uv + pixel_offset * step_normal);
            //out_color = texture(in_color, in_uv);
            //out_color = new vec4(pow(out_color.rgb, new vec3(1.0f / 2.2f)), out_color.a);
        }
    }

    public struct FXAAConstant
    {
        public int frameCount;
        public int screenWidth;
        public int screenHeight;
    }
}
