﻿using Avalonia.Controls;
using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.VulkanEngine
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class CoreFrag : GLSLFrag
    {
        [SampledImage(0, 0, DescriptorSetTarget.RenderObject)]
        public sampler2D target {  get; set; }
        [SampledImage(0, 1, DescriptorSetTarget.RenderObject)]
        public sampler2D depth {  get; set; }
        [ShaderInput(0)]
        public vec2 ScreenPos {  get; set; }
        [FragOutput(0, false, ColorBlendOp = BlendOp.Add, AlphaBlendOp = BlendOp.Add, 
            SrcColorBlendFactor = BlendFactor.SrcAlpha, SrcAlphaBlendFactor = BlendFactor.Zero,
            DstColorBlendFactor = BlendFactor.OneMinusSrcAlpha, DstAlphaBlendFactor = BlendFactor.One)]
        public vec4 OutColor {  get; set; }
        public override void main()
        {
            ivec2 coords = new ivec2(gl_FragCoord.xy);
            vec4 col = texture(target, ScreenPos);
            var d = texelFetch(depth, coords, 0);

            col = new vec4(col.rgb * col.a, 1.0f);
            OutColor = col;
            gl_FragDepth = d.r;
        }
    }
}
