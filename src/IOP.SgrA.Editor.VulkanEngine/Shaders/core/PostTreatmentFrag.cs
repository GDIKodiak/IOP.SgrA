﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;

namespace IOP.SgrA.Editor.VulkanEngine
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class PostTreatmentFrag : GLSLFrag
    {
        [UniformBuffer(0, 0, DescriptorSetTarget.Pipeline)]
        public ScreenUniform Uniform { get; set; }
        [SampledImage(0, 1, DescriptorSetTarget.Pipeline)]
        public sampler2D Target { get; set; }
        [ShaderInput(0)]
        public vec2 ScreenPos { get; set; }
        [FragOutput(0, false)]
        public vec4 OutColor { get; set; }

        public override void main()
        {
            vec4 color = texture(Target, ScreenPos);
            color.rgb = ACESToneMapping(color.rgb, 0.5f);
            OutColor = color;
        }

        private vec3 ACESToneMapping(vec3 color, float adapted_lum)
        {
            float A = 2.51f;
            float B = 0.03f;
            float C = 2.43f;
            float D = 0.59f;
            float E = 0.14f;

            color *= adapted_lum;
            return (color * (A * color + B)) / (color * (C * color + D) + E);
        }
    }
}
