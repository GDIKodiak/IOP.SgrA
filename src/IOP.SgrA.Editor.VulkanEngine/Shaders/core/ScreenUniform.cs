﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.VulkanEngine
{
    public struct ScreenUniform
    {
        public float Width;
        public float Height;
    }
}
