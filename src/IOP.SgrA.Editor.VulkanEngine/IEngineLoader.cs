﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.VulkanEngine
{
    /// <summary>
    /// 引擎加载器
    /// </summary>
    public interface IEngineLoader
    {
        /// <summary>
        /// 加载发布环境引擎
        /// </summary>
        /// <returns></returns>
        ValueTask LoadEngine();
        /// <summary>
        /// 加载调试环境引擎
        /// </summary>
        /// <returns></returns>
        ValueTask LoadEngineDebug();
        /// <summary>
        /// 释放引擎
        /// </summary>
        /// <returns></returns>
        bool DisposeEngine();
    }
}
