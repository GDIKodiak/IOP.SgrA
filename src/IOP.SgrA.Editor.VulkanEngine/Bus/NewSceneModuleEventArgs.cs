﻿using CodeWF.EventBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.VulkanEngine.Bus
{
    public class NewSceneModuleEventArgs : Command
    {
        /// <summary>
        /// 
        /// </summary>
        public string SceneName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Type ModuleType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneName"></param>
        /// <param name="moduleType"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public NewSceneModuleEventArgs(string sceneName, Type moduleType)
        {
            if (string.IsNullOrEmpty(sceneName)) throw new ArgumentNullException(nameof(sceneName));
            SceneName = sceneName;
            ModuleType = moduleType ?? throw new ArgumentNullException(nameof(moduleType));
        }
    }
}
