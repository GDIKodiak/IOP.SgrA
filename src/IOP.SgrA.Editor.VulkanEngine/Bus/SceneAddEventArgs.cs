﻿using CodeWF.EventBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.VulkanEngine.Bus
{
    /// <summary>
    /// 
    /// </summary>
    public class SceneAddEventArgs : Command
    {
        public string SceneName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public IRenderObject RenderObject { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderObject"></param>
        public SceneAddEventArgs(string sceneName, IRenderObject renderObject)
        {
            SceneName = sceneName;
            RenderObject = renderObject;
        }
    }
}
