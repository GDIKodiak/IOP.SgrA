﻿using CodeWF.EventBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.VulkanEngine.Bus
{
    /// <summary>
    /// 
    /// </summary>
    public class NewSceneEventArgs : Command
    {
        /// <summary>
        /// 
        /// </summary>
        public Scene Scene { get; set; }

        public NewSceneEventArgs(Scene scene)
        {

            Scene = scene;
        }
    }
}
