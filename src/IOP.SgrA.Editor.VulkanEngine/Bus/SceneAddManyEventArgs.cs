﻿using CodeWF.EventBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.VulkanEngine.Bus
{
    public class SceneAddManyEventArgs : Command
    {
        /// <summary>
        /// 
        /// </summary>
        public string SceneName { get; set; }
        /// <summary>
        /// 
        /// </summary>

        public readonly List<IRenderObject> Objects = new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneName"></param>
        /// <param name="objs"></param>
        public SceneAddManyEventArgs(string sceneName, IEnumerable<IRenderObject> objs) 
        {
            SceneName = sceneName;
            Objects.AddRange(objs);
        }

    }
}
