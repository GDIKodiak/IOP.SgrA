﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Graph
{
    /// <summary>
    /// 图元数据链接器
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class GraphMetadataLinker<T> : IGraphMetadataLinker<T>
    {
        /// <summary>
        /// 输入
        /// </summary>
        public (IGraphNode, IGraphMetadata<T>) Input { get; private set; }
        /// <summary>
        /// 输出
        /// </summary>
        public (IGraphNode, IGraphMetadata<T>) Output { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="output"></param>
        public GraphMetadataLinker((IGraphNode, IGraphMetadata<T>) input, (IGraphNode, IGraphMetadata<T>) output)
        {
            Input = input;
            Output = output;
        }
    }
}
