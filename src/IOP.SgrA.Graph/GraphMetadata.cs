﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Graph
{
    /// <summary>
    /// 图元数据
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class GraphMetadata<T> : IGraphMetadata<T>
    {
        /// <summary>
        /// 元数据名
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// 用途
        /// </summary>
        public MetadataUsage Usage { get; private set; }
        /// <summary>
        /// 类型
        /// </summary>
        public Type MetadataType => typeof(T);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="usage"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public GraphMetadata(string name, MetadataUsage usage) 
        {
            if(string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));
            Name = name;
            Usage = usage;
        }
    }
}
