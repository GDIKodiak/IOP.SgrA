﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Graph
{
    /// <summary>
    /// 元数据用途
    /// </summary>
    public enum MetadataUsage
    {
        /// <summary>
        /// 输入元数据
        /// </summary>
        InputMetadata,
        /// <summary>
        /// 输出元数据
        /// </summary>
        OutputMetadata
    }
}
