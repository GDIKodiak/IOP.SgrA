﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Graph
{
    /// <summary>
    /// 图节点
    /// </summary>
    public interface IGraphNode
    {
        /// <summary>
        /// 输入
        /// </summary>
        IEnumerable<IGraphMetadata> Inputs { get; }
        /// <summary>
        /// 输出
        /// </summary>
        IEnumerable<IGraphMetadata> Outputs { get; }
    }
}
