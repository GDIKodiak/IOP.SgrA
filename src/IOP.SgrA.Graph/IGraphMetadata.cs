﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Graph
{
    /// <summary>
    /// 图元数据
    /// </summary>
    public interface IGraphMetadata
    {
        /// <summary>
        /// 名称
        /// </summary>
        string Name { get; }
        /// <summary>
        /// 获取元数据用途
        /// </summary>
        /// <returns></returns>
        MetadataUsage Usage { get; }
        /// <summary>
        /// 获取元数据类型
        /// </summary>
        /// <returns></returns>
        Type MetadataType { get; }
    }
    /// <summary>
    /// 图元数据
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IGraphMetadata<T> : IGraphMetadata
    {

    }
}
