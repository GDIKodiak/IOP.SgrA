﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Graph
{
    /// <summary>
    /// 图元数据链接器
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IGraphMetadataLinker<T>
    {
        /// <summary>
        /// 输入
        /// </summary>
        (IGraphNode, IGraphMetadata<T>) Input { get; }
        /// <summary>
        /// 输出
        /// </summary>
        (IGraphNode, IGraphMetadata<T>) Output { get; }
    }
}
