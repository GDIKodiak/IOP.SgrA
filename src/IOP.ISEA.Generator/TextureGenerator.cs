﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;
using System.Text.Json;
using System.Threading.Tasks;

namespace IOP.ISEA.Generator
{
    /// <summary>
    /// 纹理节点生成器
    /// </summary>
    public class TextureGenerator : INodeGenerator
    {
        /// <summary>
        /// 解码器类型
        /// </summary>
        [JsonIgnore]
        public int CodecType => (int)StandardNodeType.TEXTURE;
        /// <summary>
        /// 属性标签类型
        /// </summary>
        public string PropertyLabel { get; set; } = StandardNodeType.TEXTURE.ToString();
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 纹理用途
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public TextureUsage Usage { get; set; }
        /// <summary>
        /// 描述符集下标
        /// </summary>
        public uint SetIndex { get; set; } = 0;
        /// <summary>
        /// 绑定位置
        /// </summary>
        public uint Binding { get; set; } = 0;
        /// <summary>
        /// 数组数量
        /// </summary>
        public uint ArrayElement { get; set; } = 0;
        /// <summary>
        /// 描述符数量
        /// </summary>
        public uint DescriptorCount { get; set; } = 1;
        /// <summary>
        /// 采样器节点
        /// </summary>
        public string SamplerNode { get; set; }
        /// <summary>
        /// 图片节点
        /// </summary>
        public string ImageNode { get; set; }
        /// <summary>
        /// 配置节点
        /// </summary>
        public string ConfigNode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        /// <param name="generator"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Task<Node[]> Generate(ISEADocument document, ISEAGenerator generator)
        {
            TextureNode texture = new TextureNode();
            texture.Name = Name;
            texture.Usage = Usage;
            texture.SetIndex = SetIndex;
            texture.Binding = Binding;
            texture.ArrayElement = ArrayElement;
            texture.DescriptorCount = DescriptorCount;
            if (!string.IsNullOrEmpty(SamplerNode))
            {
                if (generator.TryGetCacheNode(SamplerNode, out Node[] sn)) BindSamplerNode(texture, sn);
                else generator.SetDelayTask(SamplerNode, (node) => BindSamplerNode(texture, node));
            }
            if (!string.IsNullOrEmpty(ConfigNode))
            {
                if (generator.TryGetCacheNode(ConfigNode, out Node[] sn)) BindConfigNode(texture, sn);
                else generator.SetDelayTask(ConfigNode, (node) => BindConfigNode(texture, node));
            }
            if (!string.IsNullOrEmpty(ImageNode))
            {
                if (generator.TryGetCacheNode(ImageNode, out Node[] sn)) BindImageNode(texture, sn);
                else generator.SetDelayTask(ImageNode, (node) => BindImageNode(texture, node));
            }
            document.AddNode(texture, out _);
            return Task.FromResult(new Node[] { texture });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="texture"></param>
        /// <param name="node"></param>
        private void BindSamplerNode(TextureNode texture,  Node[] node)
        {
            if (node == null || node.Length < 1) return;
            if (node[0] is ConfigNode sampler)
            {
                texture.SamplerConfig = sampler;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="texture"></param>
        /// <param name="node"></param>
        private void BindImageNode(TextureNode texture, Node[] node)
        {
            if (node == null || node.Length < 1) return;
            if (node[0] is ImageNode image)
            {
                texture.Image = image;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="texture"></param>
        /// <param name="node"></param>
        private void BindConfigNode(TextureNode texture, Node[] node)
        {
            if (node == null || node.Length < 1) return;
            if (node[0] is ConfigNode config)
            {
                texture.TexConfig = config;
            }
        }
    }
}
