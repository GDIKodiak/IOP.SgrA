﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace IOP.ISEA.Generator
{
    /// <summary>
    /// 网格节点生成器
    /// </summary>
    public class MeshNodeGenerator
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 网格模式
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public MeshMode MeshMode { get; set; } = MeshMode.Vertex;
        /// <summary>
        /// 顶点数量
        /// </summary>
        public int VerticesCount { get; set; }
        /// <summary>
        /// 顶点数据步进(单位字节)
        /// </summary>
        public int VerticesStride { get; set; }
        /// <summary>
        /// LOD级别
        /// </summary>
        public int LODLevel { get; set; } = 0;
    }
}
