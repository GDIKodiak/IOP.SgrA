﻿using IOP.Extension.Json;
using IOP.ISEA.OBJ;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace IOP.ISEA.Generator
{
    /// <summary>
    /// OBJ文件生成器
    /// </summary>
    public class OBJMeshGenerator : MeshNodeGenerator, INodeGenerator
    {
        /// <summary>
        /// 
        /// </summary>
        public int CodecType => (int)StandardNodeType.MESH;
        /// <summary>
        /// 属性标签
        /// </summary>
        public string PropertyLabel { get; set; } = "OBJMESH";
        /// <summary>
        /// 文件路径
        /// </summary>
        public string FilePath { get; set; }
        /// <summary>
        /// 网格属性合并顺序
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumArrayConverter))]
        public MeshDataPurpose[] DataPurpose { get; set; }
        /// <summary>
        /// OBJ文件起始组
        /// </summary>
        public int GroupStart { get; set; }
        /// <summary>
        /// OBJ文件结束组
        /// </summary>
        public int GroupEnd { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        /// <param name="generator"></param>
        /// <returns></returns>
        /// <exception cref="FileNotFoundException"></exception>
        public async Task<Node[]> Generate(ISEADocument document, ISEAGenerator generator)
        {
            if (string.IsNullOrEmpty(FilePath)) throw new FileNotFoundException("FilePath is empty");
            string convert = FilePath.Replace('\\', '/');
            string root = string.IsNullOrEmpty(generator.Root) || string.IsNullOrWhiteSpace(generator.Root) ? AppContext.BaseDirectory : generator.Root;
            if (DataPurpose == null || DataPurpose.Length <= 0) DataPurpose = new MeshDataPurpose[] { MeshDataPurpose.POSITION };
            if (Path.IsPathRooted(convert))
            {
                if (File.Exists(convert))
                {
                    using OBJDocument obj = await OBJDocument.LoadAsync(convert);
                    MeshNode[] meshes = document.CreateMeshNodeFromOBJ(obj, GroupStart, obj.Groups.Count - 1, DataPurpose);
                    List<Node> r = new List<Node>(); r.AddRange(meshes);
                    return r.ToArray();
                }
                else throw new FileNotFoundException(convert);
            }
            else
            {
                var path = Path.GetFullPath(convert, root);
                if (File.Exists(path))
                {
                    using OBJDocument obj = await OBJDocument.LoadAsync(convert);
                    MeshNode[] meshes = document.CreateMeshNodeFromOBJ(obj, GroupStart, obj.Groups.Count - 1, DataPurpose);
                    List<Node> r = new List<Node>(); r.AddRange(meshes);
                    return r.ToArray();
                }
                else throw new FileNotFoundException(path);
            }
        }
    }
}
