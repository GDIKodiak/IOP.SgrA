﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.Generator
{
    /// <summary>
    /// 节点生成器接口
    /// </summary>
    public interface INodeGenerator
    {
        /// <summary>
        /// 编解码器Id
        /// </summary>
        int CodecType { get; }
        /// <summary>
        /// 属性标签
        /// </summary>
        string PropertyLabel { get; }
        /// <summary>
        /// 名称
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// 生成
        /// </summary>
        /// <returns></returns>
        Task<Node[]> Generate(ISEADocument document, ISEAGenerator generator);
    }
}
