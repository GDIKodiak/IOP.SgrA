﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace IOP.ISEA.Generator
{
    /// <summary>
    /// 
    /// </summary>
    public class RenderObjectGenerator : INodeGenerator
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public int CodecType => (int)StandardNodeType.RENDEROBJECT;
        /// <summary>
        /// 
        /// </summary>
        public string PropertyLabel => StandardNodeType.RENDEROBJECT.ToString();
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 子渲染对象
        /// </summary>
        public string[] Childrens { get; set; }
        /// <summary>
        /// 组件
        /// </summary>
        public string[] Components { get; set; }
        /// <summary>
        /// 局部坐标相对位移
        /// </summary>
        public float[] Position { get; set; } = new float[] { 0, 0, 0 };
        /// <summary>
        /// 局部坐标相对旋转
        /// </summary>
        public float[] Rotation { get; set; } = new float[] { 0, 0, 0, 0 };
        /// <summary>
        /// 局部坐标相对缩放
        /// </summary>
        public float[] Scale { get; set; } = new float[] { 0, 0, 0 };

        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        /// <param name="generator"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Task<Node[]> Generate(ISEADocument document, ISEAGenerator generator)
        {
            if (string.IsNullOrEmpty(Name)) throw new ArgumentNullException(nameof(Name)); 
            RenderObjectNode renderObject = new RenderObjectNode();
            renderObject.Name = Name;
            if(Position != null && Position.Length > 0)
            {
                renderObject.Position = CreateVcetor3(Position);
            }
            if(Rotation != null && Rotation.Length > 0)
            {
                renderObject.Rotation = CreateQuaternion(Rotation);
            }
            if(Scale != null && Scale.Length > 0)
            {
                renderObject.Scale = CreateVcetor3(Scale);
            }
            if(Childrens != null && Childrens.Length > 0)
            {
                List<RenderObjectNode> renders = new List<RenderObjectNode>(Childrens.Length);
                for(int i = 0;i < Childrens.Length; i++)
                {
                    if (generator.TryGetCacheNode(Childrens[i], out Node[] n)) BindChildren(renderObject, n, renders);
                    else generator.SetDelayTask(Childrens[i], (node) => BindChildren(renderObject, node, renders));
                }
                renderObject.Childrens = renders;
            }
            if(Components != null && Components.Length > 0)
            {
                List<Node> component = new List<Node>();
                foreach (var item in Components)
                {
                    if (generator.TryGetCacheNode(item, out Node[] n)) component.AddRange(n);
                    else generator.SetDelayTask(item, (node) => component.AddRange(node));
                }
                renderObject.Comonents = component;
            }
            document.AddNode(renderObject, out _);
            return Task.FromResult(new Node[] { renderObject });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private Vector3 CreateVcetor3(float[] data)
        {
            Vector3 p = new Vector3();
            switch (data.Length)
            {
                case 0:
                    break;
                case 1:
                    p.X = data[0];
                    break;
                case 2:
                    p.X = data[0];
                    p.Y = data[1];
                    break;
                default:
                    p.X = data[0];
                    p.Y = data[1];
                    p.Z = data[2];
                    break;
            }
            return p;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private Quaternion CreateQuaternion(float[] data)
        {
            Quaternion q = new Quaternion();
            switch (data.Length)
            {
                case 0:
                    break;
                case 1:
                    q.X = data[0];
                    break;
                case 2:
                    q.X = data[0];
                    q.Y = data[1];
                    break;
                case 3:
                    q.X = data[0];
                    q.Y = data[1];
                    q.Z = data[2];
                    break;
                default:
                    q.X = data[0];
                    q.Y = data[1];
                    q.Z = data[2];
                    q.W = data[3];
                    break;
            }
            return q;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderObject"></param>
        /// <param name="nodes"></param>
        /// <param name="renders"></param>
        private void BindChildren(RenderObjectNode renderObject, Node[] nodes, List<RenderObjectNode> renders)
        {
            if (nodes == null || nodes.Length < 1) return;
            if (nodes[0] is RenderObjectNode child)
            {
                if (child.Parent != null) throw new InvalidOperationException("Invalid Parent Object, RenderObject Allowed Only one parent");
                renders.Add(child);
                child.Parent = renderObject;
            }
        }
    }
}
