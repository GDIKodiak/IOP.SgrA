﻿using IOP.ISEA.STL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.Generator
{
    /// <summary>
    /// STL网格生成器
    /// </summary>
    public class STLMeshGenerator : MeshNodeGenerator, INodeGenerator
    {
        /// <summary>
        /// 解码器类型
        /// </summary>
        public int CodecType => (int)StandardNodeType.MESH;
        /// <summary>
        /// 属性标签
        /// </summary>
        public string PropertyLabel { get; set; } = "STLMESH";
        /// <summary>
        /// 文件路径
        /// </summary>
        public string FilePath { get; set; }
        /// <summary>
        /// 生成
        /// </summary>
        /// <param name="document"></param>
        /// <param name="generator"></param>
        /// <returns></returns>
        /// <exception cref="FileNotFoundException"></exception>
        public async Task<Node[]> Generate(ISEADocument document, ISEAGenerator generator)
        {
            if (string.IsNullOrEmpty(FilePath)) throw new FileNotFoundException("FilePath is empty");
            string convert = FilePath.Replace('\\', '/');
            string root = string.IsNullOrEmpty(generator.Root) || string.IsNullOrWhiteSpace(generator.Root) ? AppContext.BaseDirectory : generator.Root;
            if (Path.IsPathRooted(convert))
            {
                if (File.Exists(convert))
                {
                    using STLDocument stl = await STLDocument.LoadAsync(convert);
                    MeshNode mesh = document.CreateMeshNodeFromSTL(stl);
                    return new Node[] { mesh };
                }
                else throw new FileNotFoundException(convert);
            }
            else
            {
                var path = Path.GetFullPath(convert, root);
                if (File.Exists(path))
                {
                    using STLDocument stl = await STLDocument.LoadAsync(convert);
                    MeshNode mesh = document.CreateMeshNodeFromSTL(stl);
                    return new Node[] { mesh };
                }
                else throw new FileNotFoundException(path);
            }
        }
    }
}
