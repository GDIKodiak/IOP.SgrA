﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;


namespace IOP.ISEA.Generator
{
    /// <summary>
    /// 配置节点生成器
    /// </summary>
    public class ConfigGenerator : INodeGenerator
    {
        /// <summary>
        /// 编解码器类型
        /// </summary>
        [JsonIgnore]
        public int CodecType => (int)StandardNodeType.CONFIG;
        /// <summary>
        /// 属性标签
        /// </summary>
        public string PropertyLabel { get; set; } = StandardNodeType.CONFIG.ToString();
        /// <summary>
        /// 配置所属引擎
        /// </summary>
        public string Engine { get; set; }
        /// <summary>
        /// 配置所属文件类型
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 配置名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 配置
        /// </summary>
        [JsonConverter(typeof(RowStringConverter))]
        public string Config { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        /// <param name="generator"></param>
        /// <returns></returns>
        public Task<Node[]> Generate(ISEADocument document, ISEAGenerator generator)
        {
            ConfigNode node = new ConfigNode();
            node.Name = Name;
            node.Engine = Engine;
            node.Config = Config;
            node.Type = Type;
            document.AddNode(node, out _);
            return Task.FromResult(new Node[] { node });
        }
    }
}
