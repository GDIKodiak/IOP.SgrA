﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace IOP.ISEA.Generator
{
    /// <summary>
    /// 
    /// </summary>
    public class ISEAGenerator
    {
        /// <summary>
        /// 根目录
        /// </summary>
        public string Root { get; set; } = string.Empty;
        /// <summary>
        /// 输出文件名
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// 节点构建器
        /// </summary>
        public readonly List<INodeGenerator> Generators = new List<INodeGenerator>();

        /// <summary>
        /// 
        /// </summary>
        public ISEAGenerator()
        {
            GeneratorMap.Add(StandardNodeType.CONFIG.ToString(), typeof(ConfigGenerator));
            GeneratorMap.Add(StandardNodeType.IMAGE.ToString(), typeof(ImageGenerator));
            GeneratorMap.Add(StandardNodeType.TEXTURE.ToString(), typeof(TextureGenerator));
            GeneratorMap.Add("JSONMESH", typeof(JsonMeshGenerator));
            GeneratorMap.Add("OBJMESH", typeof(OBJMeshGenerator));
            GeneratorMap.Add("STLMESH", typeof(STLMeshGenerator));
            GeneratorMap.Add(StandardNodeType.RENDEROBJECT.ToString(), typeof(RenderObjectGenerator));
        }

        /// <summary>
        /// 生成器字典
        /// </summary>
        private readonly Dictionary<string, Type> GeneratorMap = new Dictionary<string, Type>();
        /// <summary>
        /// 缓存节点
        /// </summary>
        private readonly Dictionary<string, Node[]> CacheNode = new Dictionary<string, Node[]>();
        /// <summary>
        /// 延迟任务节点
        /// </summary>
        private readonly Dictionary<string, Action<Node[]>> DelayTasks = new Dictionary<string, Action<Node[]>>();

        /// <summary>
        /// 获取被缓存的节点
        /// </summary>
        /// <param name="name"></param>
        /// <param name="node"></param>
        /// <returns></returns>
        public bool TryGetCacheNode(string name, out Node[] node)
        {
            if (string.IsNullOrEmpty(name))
            {
                node = default;
                return false;
            }
            return CacheNode.TryGetValue(name, out node);
        }
        /// <summary>
        /// 设置延迟任务
        /// </summary>
        /// <param name="nodeName"></param>
        /// <param name="delayFunc"></param>
        public void SetDelayTask(string nodeName, Action<Node[]> delayFunc)
        {
            if (string.IsNullOrEmpty(nodeName)) return;
            if(DelayTasks.TryGetValue(nodeName, out Action<Node[]> node))
            {
                node += delayFunc;
                DelayTasks[nodeName] = node;
            }
            else DelayTasks.Add(nodeName, delayFunc);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="jsonStream"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public static async Task<ISEADocument> GenerateDocument(Stream jsonStream)
        {
            JsonDocument document = await JsonDocument.ParseAsync(jsonStream);
            ISEAGenerator generator = new ISEAGenerator();
            var map = generator.GeneratorMap;
            var cache = generator.CacheNode;
            var delay = generator.DelayTasks;
            foreach (var element in document.RootElement.EnumerateObject())
            {
                if(element.Name == nameof(Root))
                {
                    string v = element.Value.GetString();
                    generator.Root = string.IsNullOrEmpty(v) ? string.Empty : v;
                }
                else if(element.Name == nameof(Generators))
                {
                    foreach(var item in element.Value.EnumerateArray())
                    {
                        var label = item.GetProperty("PropertyLabel").GetString();
                        if (!string.IsNullOrEmpty(label))
                        {
                            if (map.TryGetValue(label, out Type type))
                            {
                                INodeGenerator nodeGener = JsonSerializer.Deserialize(item.GetRawText(), type) as INodeGenerator;
                                if (string.IsNullOrEmpty(nodeGener.Name)) throw new ArgumentNullException("NodeGenerator must be have name");
                                generator.Generators.Add(nodeGener);
                            }
                        }
                    }
                }
            }

            ISEADocument result = new ISEADocument();
            foreach(var item in generator.Generators)
            {
                var node = await item.Generate(result, generator);
                cache.Add(item.Name, node);
                if (delay.TryGetValue(item.Name, out Action<Node[]> action))
                {
                    action?.Invoke(node);
                    delay.Remove(item.Name);
                }
            }

            foreach(var item in delay)
            {
                throw new NullReferenceException($"Cannot found node with name {item.Key}");
            }
            return result;
        }
    }
}
