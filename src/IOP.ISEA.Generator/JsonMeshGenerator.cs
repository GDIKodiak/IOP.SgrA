﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace IOP.ISEA.Generator
{
    /// <summary>
    /// JSON网格节点生成器
    /// </summary>
    public class JsonMeshGenerator : MeshNodeGenerator, INodeGenerator
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public int CodecType => (int)StandardNodeType.MESH;
        /// <summary>
        /// 属性标签
        /// </summary>
        public string PropertyLabel { get; set; } = "JSONMESH";
        /// <summary>
        /// 顶点数据
        /// </summary>
        public float[] Vertices { get; set; } = Array.Empty<float>();
        /// <summary>
        /// 索引数据
        /// </summary>
        public uint[] Indexes { get; set; } = Array.Empty<uint>();

        /// <summary>
        /// 生成
        /// </summary>
        /// <param name="document"></param>
        /// <param name="generator"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Task<Node[]> Generate(ISEADocument document, ISEAGenerator generator)
        {
            if (Vertices == null || Vertices.Length <= 0) throw new NullReferenceException("No Vertices data");
            var mesh = document.CreateEmptyMeshNode(Name, VerticesCount);
            mesh.LODLevel = LODLevel;
            if (MeshMode == MeshMode.Indexes)
            {
                if (Indexes == null || Indexes.Length <= 0) MeshMode = MeshMode.Vertex;
                else mesh.AddIndexesData(Indexes, true);
            }
            mesh.AddMeshData(Vertices, (ushort)VerticesStride, 0, MeshDataPurpose.COMPLEX, true);
            return Task.FromResult(new Node[] { mesh });
        }
    }
}
