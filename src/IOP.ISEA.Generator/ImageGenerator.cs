﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace IOP.ISEA.Generator
{
    /// <summary>
    /// 图片节点生成器
    /// </summary>
    public class ImageGenerator : INodeGenerator
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public int CodecType => (int)StandardNodeType.IMAGE;
        /// <summary>
        /// 
        /// </summary>
        public string PropertyLabel { get; set; } = StandardNodeType.IMAGE.ToString();
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 文件路径
        /// </summary>
        public string FilePath { get; set; }
        /// <summary>
        /// 生成器
        /// </summary>
        /// <param name="document"></param>
        /// <param name="generator"></param>
        /// <returns></returns>
        public Task<Node[]> Generate(ISEADocument document, ISEAGenerator generator)
        {
            if (string.IsNullOrEmpty(FilePath)) throw new FileNotFoundException("FilePath is empty");
            string convert = FilePath.Replace('\\', '/');
            string root = string.IsNullOrEmpty(generator.Root) || string.IsNullOrWhiteSpace(generator.Root) ? AppContext.BaseDirectory : generator.Root;
            if (Path.IsPathRooted(convert))
            {
                if (File.Exists(convert))
                {
                    var node = document.CreateImageNode(Name, convert);
                    return Task.FromResult(new Node[] { node });
                }
                else throw new FileNotFoundException(convert);
            }
            else
            {
                var path = Path.GetFullPath(convert, root);
                if (File.Exists(path))
                {
                    var node = document.CreateImageNode(Name, path);
                    return Task.FromResult(new Node[] { node });
                }
                else throw new FileNotFoundException(path);
            }
        }
    }
}
