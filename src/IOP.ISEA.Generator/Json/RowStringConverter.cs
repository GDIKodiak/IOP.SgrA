﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace IOP.ISEA.Generator
{
    /// <summary>
    /// 
    /// </summary>
    public class RowStringConverter : JsonConverterFactory
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="typeToConvert"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public override bool CanConvert(Type typeToConvert)
        {
            return typeToConvert == typeof(string);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="typeToConvert"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public override JsonConverter CreateConverter(Type typeToConvert, JsonSerializerOptions options)
        {
            JsonConverter converter = new ConverterRowString();
            return converter;
        }
    }
}
