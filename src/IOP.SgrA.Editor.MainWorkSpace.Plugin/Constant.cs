﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.MainWorkSpace.Plugin
{
    public static class Constant
    {
        /// <summary>
        /// PBR渲染通道
        /// </summary>
        public const string PBRPass = "PBRPass";
        public const string PBRGroup = "PBR";
        /// <summary>
        /// 标准直线渲染组
        /// </summary>
        public const string LineGroup = "StandardLine";
        /// <summary>
        /// 单色渲染对象组
        /// </summary>
        public const string MonochromeGroup = "Monochrome";
        public const string EditorGridLineGroup = "EditorGridLine";

        /// <summary>
        /// 天空穹渲染组
        /// </summary>
        public const string SkyDomeGroup = "SkyDome";
        /// <summary>
        /// 天空穹烘培渲染组
        /// </summary>
        public const string SkyDomeBakeGroup = "SkyDomeBake";
        /// <summary>
        /// 全局环境光渲染组
        /// </summary>
        public const string IBLGroup = "IBL";
        /// <summary>
        /// 环境光渲染组
        /// </summary>
        public const string IBLBakeGroup = "IBLBake";
        /// <summary>
        /// 环境光辐照度计算渲染组
        /// </summary>
        public const string IBLIrradianceGroup = "IBLIrradiance";

        /// <summary>
        /// 全局光照环境贴图
        /// </summary>
        public const string IBLTEXTURE = "IBLTEXTURE";
        /// <summary>
        /// 全局环境光辐照度贴图
        /// </summary>
        public const string IBLIRRADIANCE = "IBLIRRADIANCE";
    }
}
