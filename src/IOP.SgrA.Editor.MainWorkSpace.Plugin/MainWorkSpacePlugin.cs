﻿using Avalonia;
using Avalonia.Rendering.SceneGraph;
using CodeWF.EventBus;
using IOP.Extension.DependencyInjection;
using IOP.Halo;
using IOP.SgrA.ECS;
using IOP.SgrA.Editor.Modules;
using IOP.SgrA.Editor.Modules.ECS;
using IOP.SgrA.Editor.Plugin;
using IOP.SgrA.Editor.VulkanEngine.Bus;
using IOP.SgrA.SilkNet.Vulkan;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ReactiveUI;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Numerics;
using System.Reflection;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace IOP.SgrA.Editor.MainWorkSpace.Plugin
{
    /// <summary>
    /// 主工作空间
    /// </summary>
    public class MainWorkSpacePlugin : IEditorPlugin
    {
        /// <summary>
        /// 
        /// </summary>
        [Autowired]
        public ILogger<MainWorkSpacePlugin> Logger { get; set; }
        public IGraphicsManager GraphicsManager { get; private set; }
        protected IFrameworkContainer Container;
        protected MainWorkSpaceView View = null;
        protected Scene Scene = null;
        protected IEventBus EventBus { get; set; }

        public MainWorkSpacePlugin(IEventBus eventBus)
        {
            EventBus = eventBus;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ValueTask Initialization()
        {
            return ValueTask.CompletedTask;
        }

        public async ValueTask OnAttachedToContext(IFrameworkContainer container)
        {
            Container = container;
            try
            {
                var view = container.CreateAutowiredInstance<MainWorkSpaceView>();
                var vm = container.CreateAutowiredInstance<MainWorkSpaceVM>();
                GraphicsManager = container.GetRequiredService<IGraphicsManager>();
                vm.Initialition();
                await view.render.CreateRender(Container.Services);
                var scene = view.render.GetCurrentScene();
                Scene = scene;
                scene.Initialization();
                InitArchetype(scene);
                scene.AddSceneModules(typeof(CameraModule), typeof(PBRModule), typeof(MonochromeModule),
                    typeof(EditorGridModule), typeof(EnvironmentModule), typeof(OITModule), typeof(CoordinateHelperModule));
                await scene.LoadSceneModules();
                EventBus.Publish(new NewSceneEventArgs(scene));
                scene.AddComponentSystem(typeof(InputSystem), typeof(SceneObjectSystem), typeof(TransformHelperSystem));
                container.OpenNewWorkSpace("Scene", view, vm);
            }
            catch (Exception e)
            {
                Logger?.LogError(e, "");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceDescriptors"></param>
        public void RegistService(IServiceCollection serviceDescriptors)
        {
        }

        /// <summary>
        /// 当视图初始化时
        /// </summary>
        /// <param name="view"></param>
        /// <param name="vm"></param>
        /// <returns></returns>
        private ValueTask OnInitView(MainWorkSpaceView view, MainWorkSpaceVM vm)
        {
            return ValueTask.CompletedTask;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="view"></param>
        /// <param name="vm"></param>
        /// <returns></returns>
        private ValueTask OnClosedView(MainWorkSpaceView view, MainWorkSpaceVM vm)
        {
            return ValueTask.CompletedTask;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="scene"></param>
        private void InitArchetype(Scene scene)
        {
            var contextM = scene.ContextManager;
            contextM.CreateArchetype("Monochrome", new MonochromeComponent());
        }

        public void Dispose()
        {
            if (View == null) return;
            var render = View.render;
            render.Destroy();
            SpinWait wait = new SpinWait();
            while (render.IsRunning)
            {
                wait.SpinOnce();
            }
        }
    }
}
