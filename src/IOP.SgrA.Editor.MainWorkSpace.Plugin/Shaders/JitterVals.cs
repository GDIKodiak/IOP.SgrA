﻿using IOP.SgrA.GLSL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.MainWorkSpace.Plugin
{
    public struct JitterVals
    {
        public mat4 view;
        public mat4 jitterProject;
    }
}
