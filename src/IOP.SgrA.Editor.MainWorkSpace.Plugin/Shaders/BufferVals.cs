﻿using IOP.SgrA.GLSL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.MainWorkSpace.Plugin
{
    public struct BufferVals
    {
        public mat4 mvp;
        public mat4 mm;
    }

    public struct PreBufferVals
    {
        public mat4 mvp;
        public mat4 mm;
        public mat4 preMvp;
    }

    /// <summary>
    /// View Proj Buffer
    /// </summary>
    public struct VPBufferVals
    {
        public mat4 view;
        public mat4 proj;
    }
}
