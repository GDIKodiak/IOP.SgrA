#version 460
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec3 uv;
layout (location = 0) out vec3 localPos;

out gl_PerVertex {
    vec4 gl_Position;
};

void main() {
    localPos = pos;
    gl_Position = vec4(pos, 1.0);
}