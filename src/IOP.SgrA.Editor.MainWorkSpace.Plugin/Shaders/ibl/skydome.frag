#version 460
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 uv;
layout (location = 2) in vec3 normal;
layout (location = 3) in vec3 cameraPos;
layout (location = 4) in vec4 inPrePos;
layout (location = 5) in vec4 inCurPos;
layout (location = 0) out vec4 outColor;
layout (location = 1) out vec2 motionVector;

struct ParallelLight {
    vec4 direction;
    vec4 color;
};
struct BakeParam {
    uint bakeMode;
    uint cubeIndex;
};

layout (std140, set = 0, binding = 0) uniform SkyParam {
    vec4 morningColor;
    vec4 noonColor;
    vec4 nightfallColor;
    vec4 nightColor;
    vec4 risingsunHorizon1;
    vec4 risingsunHorizon2;
    vec4 morningHorizon;
    vec4 noonHorizon;
    vec4 nightfallHorizon;
    vec4 nightHorizon;
} skyColors;
layout (std430, set = 1, binding = 0) buffer ParallelLBuffer {
    ParallelLight[] parallelLights;
} parallelLBuffer;
uniform layout (set = 2, binding = 0, rgba16) imageCube iblImage;
layout(push_constant) uniform PushConsts {
	BakeParam param;
} pushConsts;
const float PI = 3.1415926535897932384626433832795;
const uint ViewSamples = 8;
const uint LightSamples = 4;
const float I_sun = 10.0f;
const float R_e = 6360e3;
const float R_a = 6420e3;
const vec3 beta_R = vec3(5.8e-6f, 13.5e-6f, 33.1e-6f);
const float beta_M = 2e-5f; 
const float H_R = 7994;
const float H_M = 1200;
const float Mie_G = -0.76;
const vec3 C = vec3(0, -R_e, 0);

vec2 totalDepthRM;
vec3 I_R, I_M;
vec3 sundir;

vec2 densitiesRM(vec3 p) {
    float h = max(0., length(p - C) - R_e); // calculate height from Earth surface
    return vec2(exp(-h/8e3), exp(-h/12e2));
}
float escape(vec3 p, vec3 d, float R) {
    vec3 v = p - C;
    float b = dot(v, d);
    float det = b * b - dot(v, v) + R*R;
    if (det < 0.) return -1.;
    det = sqrt(det);
    float t1 = -b - det, t2 = -b + det;
    return (t1 >= 0.) ? t1 : t2;
}
vec2 scatterDepthInt(vec3 o, vec3 d, float L, float steps) {
    // Accumulator
    vec2 depthRMs = vec2(0.);

    // Set L to be step distance and pre-multiply d with it
    L /= steps; d *= L;
    
    // Go from point P to A
    for (float i = 0.; i < steps; ++i)
        // Simply accumulate densities
        depthRMs += densitiesRM(o + d * i);

    return depthRMs * L;
}
void scatterIn(vec3 o, vec3 d, float L, float steps) {

	// Set L to be step distance and pre-multiply d with it
	L /= steps; d *= L;

	// Go from point O to B
	for (float i = 0.; i < steps; ++i) {

		// Calculate position of point P_i
		vec3 p = o + d * i;

		// Calculate densities
		vec2 dRM = densitiesRM(p) * L;

		// Accumulate T(P_i -> O) with the new P_i
		totalDepthRM += dRM;

		// Calculate sum of optical depths. totalDepthRM is T(P_i -> O)
		// scatterDepthInt calculates integral part for T(A -> P_i)
		// So depthRMSum becomes sum of both optical depths
		vec2 depthRMsum = totalDepthRM + scatterDepthInt(p, sundir, escape(p, sundir, R_a), LightSamples);

		// Calculate e^(T(A -> P_i) + T(P_i -> O)
		vec3 A = exp(-beta_R * depthRMsum.x - vec3(beta_M) * 1.1 * depthRMsum.y);

		// Accumulate I_R and I_M
		I_R += A * dRM.x;
		I_M += A * dRM.y;
	}
}
vec3 scatter(vec3 o, vec3 d, float L, vec3 Lo) {

	// Zero T(P -> O) accumulator
	totalDepthRM = vec2(0.);

	// Zero I_M and I_R
	I_R = I_M = vec3(0.);

	// Compute T(P -> O) and I_M and I_R
	scatterIn(o, d, L, ViewSamples);

	// mu = cos(alpha)
	float mu = dot(d, sundir);

	// Calculate Lo extinction
	return Lo * exp(-beta_R * totalDepthRM.x - vec3(beta_M) * 1.1 * totalDepthRM.y)
		+ I_sun * (1. + mu * mu) * (
			I_R * beta_R * .0597 +
			I_M * vec3(beta_M) * .0196 / pow(1.58 - 1.52 * mu, 1.5));
}
vec3 scatteringColor(vec3 dir) {
    vec3 fsPostion = pos * R_e;
    vec3 center = vec3(0.0, -R_e, 0.0);
    vec3 viewDir = normalize(fsPostion - cameraPos);
    vec3 eyePos = cameraPos;
    vec3 col = vec3(0);
    float L = escape(eyePos, viewDir, R_a);
    col = scatter(eyePos, viewDir, L, col);
    return col;
}
vec4 getSkyColor(vec3 dir) {
    vec3 posWS = normalize(pos);
    vec3 lDir = (dir + 1) * 0.5;
    float sun = distance(posWS, dir);
    sun = 1 - smoothstep(0.7, 1, sun * 30);
    if (sun > 0 && lDir.y > 0.4) {
        return vec4(1.0, 1.0, 1.0, 0.0);
    }
    else {
        if (lDir.y > 0.4995 && lDir.y < 1.005) 
        {
            vec4 daySkyColor = mix(mix(
                skyColors.morningColor, skyColors.noonColor, clamp(smoothstep(0, 0.9, lDir.z) * 3, 0.0, 1.0)),
                skyColors.nightfallColor, smoothstep(0, 0.3, smoothstep(0.9, 1, lDir.z) * 0.3))
                * step(0.5, lDir.y);
            vec4 dayHorizonColor = mix(mix(mix(
                skyColors.risingsunHorizon2, skyColors.morningColor, clamp(smoothstep(0, 0.2, lDir.z), 0.0, 1.0) * 2), 
                skyColors.noonHorizon, smoothstep(0, 0.5, smoothstep(0.2, 0.9, lDir.z) * 0.5)),
                skyColors.nightfallHorizon, smoothstep(0.9, 1.0, lDir.z))
                * step(0.5, lDir.y);
            vec4 dayMixColor = mix(dayHorizonColor, daySkyColor, 
                smoothstep(0, 0.5 + 0.4 * smoothstep(0.9, 1, lDir.z), posWS.y));
            return vec4(dayMixColor.xyz, 0.0);
        }
        else 
        {
            vec4 nightSkyColor = mix(skyColors.morningColor, 
                mix(skyColors.nightColor, skyColors.nightfallColor, smoothstep(0.5, 1, lDir.z)), clamp(smoothstep(0, 0.08, lDir.z), 0.0, 1.0)) 
                * step(-0.5, -lDir.y);
            vec4 nightHorizonColor = mix(skyColors.risingsunHorizon2 ,mix(skyColors.risingsunHorizon1, 
                mix(skyColors.nightHorizon, skyColors.nightfallHorizon, smoothstep(0, 0.1, smoothstep(0.7, 1, lDir.z) * 0.1)), 
                clamp(smoothstep(0.1, 0.7, lDir.z) * 9900, 0.0, 1.0)),
                clamp(smoothstep(0.0, 0.1, lDir.z) * 5, 0.0, 1.0))
                * step(-0.5, -lDir.y);
            float posWSY = 0.5 + 0.4 * smoothstep(0.9, 1, lDir.z);
            float curve = (cos(9.9 * 3.1415926 * lDir.z) - 1) * 0.2;
            if(lDir.z < 0) {
                curve = 0;
            } 
            else if (lDir.z > 0.202) {
                curve = 0;
            }
            posWSY += curve;
            vec4 nightMixColor = mix(nightHorizonColor, nightSkyColor, smoothstep(0, posWSY, posWS.y));
            return vec4(nightMixColor.xyz, 0.0);
        }
    }
}
vec4 getSimpleSkyColor() {
    vec3 posWS = normalize(pos);
    float ny = (posWS.y + 1) * 0.5;
    return mix(vec4(0.255f, 0.411f, 0.882f, 0.0f), vec4(0.530f, 0.808f, 0.921f, 0.0f), ny);
}

void main() {
    //vec3 dir = parallelLBuffer.parallelLights[0].direction.xyz;
    //vec4 skyColor = getSkyColor(dir);

    //vec3 col = vec3(0.0);
    //col = scatteringColor(dir);

    outColor = getSimpleSkyColor();
    outColor.a = 0.0000001;
    vec2 prePos = inPrePos.xy / inPrePos.w;
    vec2 newPos = inCurPos.xy / inCurPos.w;
    motionVector = (newPos - prePos) * 0.5;
    
    if(pushConsts.param.bakeMode == 1) {
        ivec2 size = imageSize(iblImage);
        ivec3 xyz = ivec3(gl_FragCoord.x, size.y - gl_FragCoord.y, pushConsts.param.cubeIndex);
        imageStore(iblImage, xyz, outColor);
        discard;
    }
}