#version 460
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable
layout(std140, set = 3, binding = 0) uniform Constant {
    mat4 projection;
    mat4 view;
    mat4 preView;
    mat4 jitterProject;
    vec4 cameraPos;
} vals;

layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec3 uv;
layout (location = 0) out vec3 localPos;
layout (location = 1) out vec3 outUV;
layout (location = 2) out vec3 reveNormal;
layout (location = 3) out vec3 cPos;
layout (location = 4) out vec4 outPrePos;
layout (location = 5) out vec4 outCurrPos;
out gl_PerVertex {
    vec4 gl_Position;
};

void main() {
    localPos = pos;
    mat4 rotView = mat4(mat3(vals.view));
    mat4 prerotView = mat4(mat3(vals.preView));
    vec4 clipPos =  vals.jitterProject * rotView * vec4(pos, 1.0);
    outUV = uv;
    reveNormal = -1 * normal;
    cPos = vals.cameraPos.xyz;
    gl_Position = clipPos.xyww;

    vec4 prePos =    vals.projection * vals.preView * vec4(pos, 1.0);
    vec4 currPos =   vals.projection * vals.view * vec4(pos, 1.0);
    outPrePos = prePos;
    outCurrPos = currPos;
}