﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.MainWorkSpace.Plugin
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class PBRLightFrag : GLSLFrag
    {
        [InputAttachment(0, 0, 0)]
        public subpassInput inAlbedo { get; set; }
        [InputAttachment(0, 1, 1)]
        public subpassInput inMRA { get; set; }
        [InputAttachment(0, 2, 2)]
        public subpassInput inNormal { get; set; }
        [InputAttachment(0, 3, 3)]
        public subpassInput worldPos { get; set; }
        [UniformBuffer(1, 0, DescriptorSetTarget.Pipeline)]
        public UBuffer vals { get; set; }
        [StorageBuffer(2, 0, DescriptorSetTarget.Pipeline, Layout = BufferLayout.std430)]
        public PointLBuffer pointLBuffer { get; set; }
        [ShaderInput(0)]
        public vec3 uv { get; set; }
        [FragOutput(0, false)]
        public vec4 outColor { get; set; }
        const float PI = 3.14159265359f;
        const float Range = 100;

        private vec3 fresnelSchlick(float cosTheta, vec3 F0)
        {
            return F0 + (1.0f - F0) * pow(clamp(1.0f - cosTheta, 0.0f, 1.0f), 5.0f);
        }
        private float DistributionGGX(vec3 N, vec3 H, float roughness)
        {
            float a = roughness * roughness;
            float a2 = a * a;
            float NdotH = max(dot(N, H), 0.0f);
            float NdotH2 = NdotH * NdotH;

            float nom = a2;
            float denom = NdotH2 * (a2 - 1.0f) + 1.0f;
            denom = PI * denom * denom;

            return nom / denom;
        }
        private float GeometrySchlickGGX(float NdotV, float roughness)
        {
            float r = (roughness + 1.0f);
            float k = (r * r) / 8.0f;

            float nom = NdotV;
            float denom = NdotV * (1.0f - k) + k;

            return nom / denom;
        }
        private float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
        {
            float NdotV = max(dot(N, V), 0.0f);
            float NdotL = max(dot(N, L), 0.0f);
            float ggx2 = GeometrySchlickGGX(NdotV, roughness);
            float ggx1 = GeometrySchlickGGX(NdotL, roughness);

            return ggx1 * ggx2;
        }

        public override void main()
        {
            vec3 N = subpassLoad(inNormal).rgb;
            vec4 wp = subpassLoad(worldPos).rgba;
            vec3 w = wp.rgb;
            float depth = wp.a;
            vec3 mra = subpassLoad(inMRA).rgb;
            vec3 albedo = subpassLoad(inAlbedo).rgb;
            albedo = pow(albedo, new vec3(2.2f));
            vec3 V = normalize(vals.camPos.xyz - w);

            vec3 F0 = new vec3(0.04f);
            F0 = mix(F0, albedo, mra.r);

            vec3 Lo = new vec3(0.0f);
            for (int i = 0; i < vals.pointLCount; i++)
            {
                vec3 pl = pointLBuffer.pointLights[i].position.xyz;
                vec3 pc = pointLBuffer.pointLights[i].color.xyz;
                vec3 L = normalize(pl - w);
                vec3 H = normalize(V + L);

                float d = length(pl - w);
                float x = d / Range;
                float sm = pow(1 - pow(x / Range, 4.0f), 2.0f);
                sm = clamp(sm, 0.0f, 1.0f);
                float attenuation = sm / (x * x);
                vec3 radiance = pc * attenuation;

                float NDF = DistributionGGX(N, H, mra.g);
                float G = GeometrySmith(N, V, L, mra.g);
                vec3 F = fresnelSchlick(max(dot(H, V), 0.0f), F0);

                vec3 numerator = NDF * G * F;
                float denominator = 4.0f * max(dot(N, V), 0.0f) * max(dot(N, L), 0.0f) + 0.0001f;
                vec3 specular = numerator / denominator;

                vec3 kS = F;
                vec3 kD = new vec3(1.0f) - kS;
                kD *= 1.0f - mra.r;

                float NdotL = max(dot(N, L), 0.0f);
                Lo += (kD * albedo / PI + specular) * radiance * NdotL;
            }

            vec3 ambient = albedo * mra.b;
            vec3 color = ambient + Lo;

            color = color / (color + new vec3(1.0f));
            color = pow(color, new vec3(1.0f / 2.2f));

            outColor = new vec4(color, depth);
        }
    }

    public struct PointLight
    {
        public vec4 position;
        public vec4 color;
    };
    public struct UBuffer
    {
        public vec4 camPos;
        public vec4 camDir;
        public vec4 ambient;
        public uint pointLCount;
    }
    public struct PointLBuffer
    {
        public PointLight[] pointLights;
    }
}
