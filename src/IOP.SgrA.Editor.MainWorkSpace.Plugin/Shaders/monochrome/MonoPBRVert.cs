﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.MainWorkSpace.Plugin
{
    [VertexInputBinding(0, 36)]
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class MonoPBRVert : GLSLVert
    {
        [UniformBuffer(0, 0, DescriptorSetTarget.RenderObject)]
        public BufferVals vals { get; set; }
        [UniformBuffer(0, 1, DescriptorSetTarget.RenderObject)]
        public MonoMaterial material { get; set; }

        [VertInput(0, Format.R32G32B32Sfloat)]
        public vec3 pos { get; set; }
        [VertInput(1, Format.R32G32B32Sfloat)]
        public vec3 normal { get; set; }
        [ShaderOutput(0)]
        public vec3 baseColor { get; set; }
        [ShaderOutput(1)]
        public vec3 MRA { get; set; }
        [ShaderOutput(2)]
        public vec3 n { get; set; }
        [ShaderOutput(3)]
        public vec3 outPos {  get; set; }
        [ShaderOutput(4)]
        public vec2 motionVector {  get; set; }

        vec3 normalFromObjectToWorld(mat4 uMatrix, vec3 normal, vec3 postion)
        {
            vec3 target = postion + normal;
            vec3 newNormal = (uMatrix * new vec4(target, 1)).xyz - (uMatrix * new vec4(postion, 1)).xyz;
            newNormal = normalize(newNormal);
            return newNormal;
        }

        public override void main()
        {
            baseColor = material.albedo.xyz;
            MRA = material.mra.xyz;
            n = normalFromObjectToWorld(vals.mm, normal, pos);
            outPos = (vals.mm * new vec4(pos, 1)).xyz;
            gl_Position = vals.mvp * new vec4(pos, 1.0f);
        }
    }

    public struct MonoMaterial
    {
        public vec4 albedo;
        public vec4 mra;
    }
}
