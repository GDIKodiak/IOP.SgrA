﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.MainWorkSpace.Plugin
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class MonoPBRFrag : GLSLFrag
    {
        [ShaderInput(0)]
        public vec3 inAlbedo { get; set; }
        [ShaderInput(1)]
        public vec3 mra {  get; set; }
        [ShaderInput(2)]
        public vec3 normal {  get; set; }
        [ShaderInput(3)]
        public vec3 worldPos {  get; set; }
        [ShaderInput(4)]
        public vec2 motionVector {  get; set; }
        [FragOutput(0, false)]
        public vec4 outAlbedo {  get; set; }
        [FragOutput(1, false)]
        public vec4 outMRA { get; set; }
        [FragOutput(2, false)]
        public vec4 outNormal { get; set; }
        [FragOutput(3, false)]
        public vec4 outPos { get; set; }
        [FragOutput(4, false)]
        public vec2 outMotion { get; set; }

        public override void main()
        {
            outAlbedo = new vec4(inAlbedo.xyz, 1.0f);
            outMRA = new vec4(mra.xyz, 1.0f);
            outNormal = new vec4(normal.xyz, 1.0f);
            outPos = new vec4(worldPos.xyz, gl_FragCoord.z);
            outMotion = motionVector;
        }
    }
}
