﻿using Avalonia.Remote.Protocol;
using IOP.SgrA.Editor.VulkanEngine;
using IOP.SgrA.Modeling;
using IOP.SgrA.SilkNet.Vulkan;
using Microsoft.Extensions.Logging;
using Silk.NET.Vulkan;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Semaphore = Silk.NET.Vulkan.Semaphore;

namespace IOP.SgrA.Editor.MainWorkSpace.Plugin
{
    /// <summary>
    /// 全局环境光模块
    /// </summary>
    [ModulePriority(ModulePriority.ISEA)]
    public class IBLModule : VulkanModule, ISceneModule
    {
        private readonly ILogger<IBLModule> Logger;
        private const uint IBLWidth = 512;
        /// <summary>
        /// 场景
        /// </summary>
        public Scene Scene { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        public IBLModule(ILogger<IBLModule> logger)
        {
            Logger = logger;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="manager"></param>
        /// <returns></returns>
        protected override async Task Load(VulkanGraphicsManager manager)
        {
            var lDevice = manager.VulkanDevice;
            var pipeCache = lDevice.PipelineCache;
            var basePath = Path.GetDirectoryName(GetType().Assembly.Location);
            var shaders = Path.Combine(basePath, "Assets", "Shaders");
            var group = Scene.RenderGroup;
            try
            {
                if (group == null) throw new NullReferenceException(nameof(Scene.RenderGroup));
                if (group is not PrimaryVulkanRenderGroup mainGroup) throw new InvalidCastException($"scene render group is not {nameof(PrimaryVulkanRenderGroup)}, " +
                    $"invalid to get render pass");
                VulkanShaderInfo[] infos = new VulkanSPVShaderInfo[]
                {
                    new VulkanSPVShaderInfo(ShaderTypes.VertexShader, "skyDomeVert", new FileInfo(Path.Combine(shaders,"skydomeVert.spv"))),
                    new VulkanSPVShaderInfo(ShaderTypes.FragmentShader, "skyDomeFrag", new FileInfo(Path.Combine(shaders, "skydomeFrag.spv"))),
                };
                await Task.WhenAll(infos[0].LoadShader(), infos[1].LoadShader());
                var r = manager.TryGetCamera("MainCamera", out Camera camera);
                if (!r) throw new NullReferenceException("Cannot get camera with name MainCamera");
                var area = Scene.RenderArea;

                var vro = manager.CreateEmptyMesh<VRO>("SkyDomeMESH");
                vro.CreateUnitSphere(10, lDevice, SharingMode.Exclusive);
                var skyDome = manager.CreateRenderObject<RenderObject>("SkyDome", vro);

                var skydomeCube = manager.CreateLocalImageTextureData(Constant.IBLTEXTURE, lDevice, new ImageAndImageViewCreateOption()
                {
                    ImageCreateOption = new ImageCreateOption()
                    {
                        ArrayLayers = 6,
                        Format = Format.R16G16B16A16Sfloat,
                        ImageType = ImageType.Type2D,
                        InitialLayout = ImageLayout.General,
                        MipLevels = 1,
                        Samples = SampleCountFlags.Count1Bit,
                        SharingMode = SharingMode.Exclusive,
                        Tiling = ImageTiling.Optimal,
                        Usage = ImageUsageFlags.StorageBit | ImageUsageFlags.SampledBit,
                        Flags = ImageCreateFlags.CreateCubeCompatibleBit
                    },
                    ImageViewCreateOption = new ImageViewCreateOption()
                    {
                        Components = new ComponentMapping(ComponentSwizzle.R, ComponentSwizzle.G, ComponentSwizzle.B, ComponentSwizzle.A),
                        Format = Format.R16G16B16A16Sfloat,
                        SubresourceRange = new ImageSubresourceRange(ImageAspectFlags.ColorBit, 0, 1, 0, 6),
                        ViewType = ImageViewType.TypeCube
                    }
                }, new Extent3D(IBLWidth, IBLWidth, 1), FormatFeatureFlags.StorageImageBit | FormatFeatureFlags.SampledImageBit);

                //CreateIBLGroup(manager, lDevice, infos, pipeCache, skyDome, skydomeCube, skyDomeGroup, mainGroup);
            }
            catch (Exception e)
            {
                Logger?.LogError(e, "");
            }
        }
        protected override Task Unload(VulkanGraphicsManager manager)
        {
            return Task.CompletedTask;
        }

        #region SkyDome
        /// <summary>
        /// 创建天空穹渲染通道
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="area"></param>
        /// <returns></returns>
        private VulkanRenderPass CreateSkyDomePass(VulkanGraphicsManager manager, Area area)
        {
            var lDevice = manager.VulkanDevice ?? throw new NullReferenceException("Please create Vulkan deivce first");
            var pass = lDevice.CreateScriptedRenderPass<IBLRenderPass>(area);
            return pass;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="cubeWidth"></param>
        /// <returns></returns>
        /// <exception cref="NullReferenceException"></exception>
        private VulkanRenderPass CreateSkyDomeBakePass(VulkanGraphicsManager manager, uint cubeWidth)
        {
            var lDevice = manager.VulkanDevice ?? throw new NullReferenceException("Please create Vulkan deivce first");
            RenderPassCreateOption option = new RenderPassCreateOption()
            {
                Attachments = new AttachmentDescription[]
                {
                    new AttachmentDescription()
                    {
                        Format = Format.R16G16B16A16Sfloat,
                        InitialLayout = ImageLayout.Undefined,
                        FinalLayout = ImageLayout.ColorAttachmentOptimal,
                        LoadOp = AttachmentLoadOp.Clear,
                        StoreOp = AttachmentStoreOp.DontCare,
                        StencilLoadOp = AttachmentLoadOp.DontCare,
                        StencilStoreOp = AttachmentStoreOp.DontCare,
                        Samples = SampleCountFlags.Count1Bit
                    },
                    new AttachmentDescription()
                    {
                        Format = Format.D16Unorm,
                        InitialLayout = ImageLayout.Undefined,
                        FinalLayout = ImageLayout.DepthStencilAttachmentOptimal,
                        LoadOp = AttachmentLoadOp.Clear,
                        StoreOp = AttachmentStoreOp.DontCare,
                        StencilLoadOp = AttachmentLoadOp.DontCare,
                        StencilStoreOp = AttachmentStoreOp.DontCare,
                        Samples = SampleCountFlags.Count1Bit
                    },
                },
                Subpasses = new SubpassDescriptionOption[]
                {
                    new SubpassDescriptionOption()
                    {
                        ColorAttachments = new AttachmentReference[]
                        {
                            new AttachmentReference(0, ImageLayout.ColorAttachmentOptimal)
                        },
                        DepthStencilAttachment = new AttachmentReference(1, ImageLayout.DepthStencilAttachmentOptimal),
                        PipelineBindPoint = PipelineBindPoint.Graphics
                    }
                },
                Dependencies = new SubpassDependency[]
                {
                    new SubpassDependency()
                    {
                        SrcAccessMask = AccessFlags.ColorAttachmentWriteBit,
                        DstAccessMask = 0,
                        SrcStageMask = PipelineStageFlags.ColorAttachmentOutputBit,
                        DstStageMask = PipelineStageFlags.BottomOfPipeBit,
                        SrcSubpass = 0,
                        DstSubpass = VulkanGraphicsManager.VK_SUBPASS_EXTERNAL,
                        DependencyFlags = DependencyFlags.ByRegionBit
                    }
                }
            };
            ImageAndImageViewCreateOption[] attachment = new ImageAndImageViewCreateOption[]
            {
                new ImageAndImageViewCreateOption()
                {
                    ImageCreateOption = new ImageCreateOption
                    {
                        ArrayLayers = 1,
                        Extent = new Extent3D(cubeWidth, cubeWidth, 1),
                        Format = Format.R16G16B16A16Sfloat,
                        ImageType = ImageType.Type2D,
                        InitialLayout = ImageLayout.Undefined,
                        MipLevels = 1,
                        Samples = SampleCountFlags.Count1Bit,
                        SharingMode = SharingMode.Exclusive,
                        Tiling = ImageTiling.Optimal,
                        Usage = ImageUsageFlags.ColorAttachmentBit
                    },
                    ImageViewCreateOption = new ImageViewCreateOption()
                    {
                        Components = new ComponentMapping(ComponentSwizzle.R, ComponentSwizzle.G, ComponentSwizzle.B, ComponentSwizzle.A),
                        Format = Format.R16G16B16A16Sfloat,
                        SubresourceRange = new ImageSubresourceRange(ImageAspectFlags.ColorBit, 0, 1, 0, 1),
                        ViewType = ImageViewType.Type2D
                    }
                },
                new ImageAndImageViewCreateOption()
                {
                    ImageCreateOption = new ImageCreateOption()
                    {
                        ArrayLayers = 1,
                        Extent = new Extent3D(cubeWidth, cubeWidth, 1),
                        Format = Format.D16Unorm,
                        ImageType = ImageType.Type2D,
                        InitialLayout = ImageLayout.Undefined,
                        MipLevels = 1,
                        Samples = SampleCountFlags.Count1Bit,
                        SharingMode = SharingMode.Exclusive,
                        Tiling = ImageTiling.Optimal,
                        Usage = ImageUsageFlags.DepthStencilAttachmentBit,
                    },
                    ImageViewCreateOption = new ImageViewCreateOption()
                    {
                        Components = new ComponentMapping(ComponentSwizzle.R, ComponentSwizzle.G, ComponentSwizzle.B, ComponentSwizzle.A),
                        Format = Format.D16Unorm,
                        SubresourceRange = new ImageSubresourceRange(ImageAspectFlags.DepthBit, 0, 1, 0, 1),
                        ViewType = ImageViewType.Type2D
                    }
                },
            };
            var pass = lDevice.CreateRenderPass($"SkyDomeIBLPass", option, RenderPassMode.Normal);
            pass.CreateFrameBuffer(attachment, cubeWidth, cubeWidth);
            pass.ConfigBeginInfo((o) =>
            {
                o.RenderArea = new Rect2D { Extent = new Extent2D(cubeWidth, cubeWidth), Offset = new Offset2D { X = 0, Y = 0 } };
                o.ClearValues = new ClearValue[]
                {
                    new ClearValue() { Color = new ClearColorValue(0.0f, 0.0f, 0.0f, 0.0f) },
                    new ClearValue() { DepthStencil = new ClearDepthStencilValue(0.0f, 0) }
                };
            });
            return pass;
        }

        /// <summary>
        /// 创建天空穹管线布局
        /// </summary>
        /// <returns></returns>
        private DescriptorSetLayoutCreateOption[] CreateSkyDomeSetLayout()
        {
            DescriptorSetLayoutCreateOption[] options = new DescriptorSetLayoutCreateOption[]
            {
                new DescriptorSetLayoutCreateOption()
                {
                    Bindings = new DescriptorSetLayoutBindingOption[]
                    {
                        new DescriptorSetLayoutBindingOption()
                        {
                            Binding = 0,
                            DescriptorCount = 1,
                            DescriptorType = DescriptorType.UniformBuffer,
                            StageFlags = ShaderStageFlags.FragmentBit
                        }
                    },
                    DescriptorSetTarget = DescriptorSetTarget.Pipeline,
                    SetIndex = 0,
                },
                new DescriptorSetLayoutCreateOption()
                {
                    Bindings = new DescriptorSetLayoutBindingOption[]
                    {
                        new DescriptorSetLayoutBindingOption()
                        {
                            Binding = 0,
                            DescriptorCount = 1,
                            DescriptorType = DescriptorType.StorageBuffer,
                            StageFlags = ShaderStageFlags.FragmentBit
                        }
                    },
                    DescriptorSetTarget = DescriptorSetTarget.Pipeline,
                    SetIndex = 1,
                },
                new DescriptorSetLayoutCreateOption()
                {
                    Bindings = new DescriptorSetLayoutBindingOption[]
                    {
                        new DescriptorSetLayoutBindingOption()
                        {
                            Binding = 0,
                            DescriptorCount = 1,
                            DescriptorType = DescriptorType.StorageImage,
                            StageFlags = ShaderStageFlags.FragmentBit
                        }
                    },
                    DescriptorSetTarget = DescriptorSetTarget.Pipeline,
                    SetIndex = 2,
                },
                new DescriptorSetLayoutCreateOption()
                {
                    Bindings = new DescriptorSetLayoutBindingOption[]
                    {
                        new DescriptorSetLayoutBindingOption()
                        {
                            Binding = 0,
                            DescriptorCount = 1,
                            DescriptorType = DescriptorType.UniformBuffer,
                            StageFlags = ShaderStageFlags.VertexBit
                        }
                    },
                    DescriptorSetTarget = DescriptorSetTarget.Pipeline,
                    SetIndex = 3,
                }
            };
            return options;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private PipelineLayoutCreateOption CreateSkyDomePipelineLayout()
        {
            return new PipelineLayoutCreateOption()
            {
                PushConstantRanges = new PushConstantRange[]
                {
                    new PushConstantRange(ShaderStageFlags.FragmentBit, 0, 8)
                }
            };
        }
        /// <summary>
        /// 创建环境光烘培管线
        /// </summary>
        /// <param name="area"></param>
        /// <returns></returns>
        private GraphicsPipelineCreateOption CreateSkyDomeIBLBakePipeline(uint cubeWidth)
        {
            return new GraphicsPipelineCreateOption()
            {
                Stages = new PipelineShaderStageCreateOption[]
                {
                    new PipelineShaderStageCreateOption() { Name = "main" },
                    new PipelineShaderStageCreateOption() { Name = "main" }
                },
                VertexInputState = new PipelineVertexInputStateCreateOption
                {
                    VertexAttributeDescriptions = new VertexInputAttributeDescription[]
                    {
                        new VertexInputAttributeDescription(0, 0, Format.R32G32B32Sfloat, 0),
                        new VertexInputAttributeDescription(1, 0, Format.R32G32B32Sfloat, 12),
                        new VertexInputAttributeDescription(2, 0, Format.R32G32B32Sfloat, 24)
                    },
                    VertexBindingDescriptions = new VertexInputBindingDescription[]
                    {
                        new VertexInputBindingDescription(0, 36, VertexInputRate.Vertex)
                    }
                },
                InputAssemblyState = new PipelineInputAssemblyStateCreateOption()
                {
                    Topology = PrimitiveTopology.TriangleList,
                    PrimitiveRestartEnable = false
                },
                ViewportState = new PipelineViewportStateCreateOption()
                {
                    Viewports = new Viewport[]
                    {
                        new Viewport(0, 0, cubeWidth, cubeWidth, 0, 1.0f)
                    },
                    Scissors = new Rect2D[]
                    {
                        new Rect2D(new Offset2D(0, 0), new Extent2D(cubeWidth, cubeWidth))
                    }
                },
                RasterizationState = new PipelineRasterizationStateCreateOption()
                {
                    PolygonMode = PolygonMode.Fill,
                    CullMode = CullModeFlags.None,
                    FrontFace = FrontFace.CounterClockwise,
                    DepthClampEnable = false,
                    RasterizerDiscardEnable = false,
                    DepthBiasEnable = false,
                    DepthBiasConstantFactor = 0,
                    DepthBiasClamp = 0,
                    DepthBiasSlopeFactor = 0,
                    LineWidth = 1.0f
                },
                ColorBlendState = new PipelineColorBlendStateCreateOption()
                {
                    Attachments = new PipelineColorBlendAttachmentState[]
                    {
                        new PipelineColorBlendAttachmentState()
                        {
                            ColorWriteMask = ColorComponentFlags.RBit |
                            ColorComponentFlags.GBit|
                            ColorComponentFlags.BBit |
                            ColorComponentFlags.ABit,
                            BlendEnable = false,
                            AlphaBlendOp = BlendOp.Add,
                            ColorBlendOp = BlendOp.Add,
                            DstColorBlendFactor = BlendFactor.Zero,
                            SrcAlphaBlendFactor = BlendFactor.Zero,
                            DstAlphaBlendFactor = BlendFactor.Zero,
                            SrcColorBlendFactor = BlendFactor.Zero
                        },
                        new PipelineColorBlendAttachmentState()
                        {
                            ColorWriteMask = ColorComponentFlags.RBit |
                            ColorComponentFlags.GBit|
                            ColorComponentFlags.BBit |
                            ColorComponentFlags.ABit,
                            BlendEnable = false,
                            AlphaBlendOp = BlendOp.Add,
                            ColorBlendOp = BlendOp.Add,
                            DstColorBlendFactor = BlendFactor.Zero,
                            SrcAlphaBlendFactor = BlendFactor.Zero,
                            DstAlphaBlendFactor = BlendFactor.Zero,
                            SrcColorBlendFactor = BlendFactor.Zero
                        }
                    },
                    LogicOpEnable = false,
                    LogicOp = LogicOp.NoOp,
                    BlendConstants = (1.0f, 1.0f, 1.0f, 1.0f)
                },
                DepthStencilState = new PipelineDepthStencilStateCreateOption()
                {
                    Back = new StencilOpState()
                    {
                        FailOp = StencilOp.Keep,
                        PassOp = StencilOp.Keep,
                        CompareOp = CompareOp.Always,
                        CompareMask = 0,
                        Reference = 0,
                        DepthFailOp = StencilOp.Keep,
                        WriteMask = 0
                    },
                    Front = new StencilOpState()
                    {
                        FailOp = StencilOp.Keep,
                        PassOp = StencilOp.Keep,
                        CompareOp = CompareOp.Always,
                        CompareMask = 0,
                        Reference = 0,
                        DepthFailOp = StencilOp.Keep,
                        WriteMask = 0
                    },
                    DepthWriteEnable = true,
                    DepthTestEnable = true,
                    DepthCompareOp = CompareOp.Greater,
                    DepthBoundsTestEnable = false,
                    MinDepthBounds = 0,
                    MaxDepthBounds = 0,
                    StencilTestEnable = false
                },
                MultisampleState = new PipelineMultisampleStateCreateOption()
                {
                    RasterizationSamples = SampleCountFlags.Count1Bit,
                    SampleShadingEnable = false,
                    AlphaToCoverageEnable = false,
                    AlphaToOneEnable = false,
                    MinSampleShading = 0.0f
                },
            };
        }

        private void SkyDomeMain(PrimaryVulkanRenderGroup group)
        {
            try
            {
                var queue = group.WorkQueues[0].Queue;
                var cmdBuffer = group.PrimaryCommandBuffer;
                var semaphores = group.Semaphores;
                var pass = group.RenderPass;
                var lDevice = group.LogicDevice;
                var camera = group.Camera;
                var pipe = group.Pipeline;

                VulkanFrameBuffer framebuffer = group.RenderPass.GetFramebuffer(0);
                cmdBuffer.Reset();
                cmdBuffer.Begin();

                foreach (var child in group.GetChildrens())
                {
                    child.GroupRendering();
                }

                cmdBuffer.BeginRenderPass(pass, framebuffer, pass.BeginOption, SubpassContents.Inline);
                cmdBuffer.BindPipeline(PipelineBindPoint.Graphics, pipe);
                var viewPort = camera.Viewport;
                cmdBuffer.SetViewport(new Viewport(viewPort.X, viewPort.Y, viewPort.Width, viewPort.Height, viewPort.MinDepth, viewPort.MaxDepth));
                cmdBuffer.SetScissor(new Rect2D(null, new Extent2D((uint)viewPort.Width, (uint)viewPort.Height)));

                Matrix4x4 jProjection = camera.ProjectionMatrix;
                jProjection.HaltonJitter((int)viewPort.Width, (int)viewPort.Height, FrameIndex);
                foreach (var item in group.GetStaticObjects())
                {
                    var cPos = new Vector4(camera.EyePosition * 0.01f, 0.0f);
                    SkyDomeItem(item, pipe, cmdBuffer, camera.ProjectionMatrix,
                        camera.BasicViewMatrix, cPos, jProjection, false, 0);
                }
                cmdBuffer.EndRenderPass();
                cmdBuffer.End();
                var submitInfo = new SubmitOption
                {
                    WaitDstStageMask = null,
                    WaitSemaphores = null,
                    Buffers = [cmdBuffer.RawHandle],
                    SignalSemaphores = [semaphores[0]]
                };
                lDevice.Submit(queue, new Fence(0), submitInfo);
                FrameIndex++;
                FrameIndex %= 8;
            }
            catch (Exception e)
            {
                group.Disable();
                group.Logger?.LogError(e, e.Message);
            }
        }
        private void SkyDomeBakeMain(PrimaryVulkanRenderGroup group)
        {
            var cmdBuffer = group.PrimaryCommandBuffer;
            var pipe = group.Pipeline;
            var pass = group.RenderPass;
            VulkanFrameBuffer framebuffer = pass.GetFramebuffer(0);

            cmdBuffer.BeginRenderPass(pass, framebuffer, pass.BeginOption, SubpassContents.Inline);
            cmdBuffer.BindPipeline(PipelineBindPoint.Graphics, pipe);

            Matrix4x4 projection = Matrix4x4.CreatePerspectiveFieldOfView(90.0f.ToRadians(), 1.0f, 0.1f, 1.0f);
            Matrix4x4[] datas = ArrayPool<Matrix4x4>.Shared.Rent(6);
            Span<Matrix4x4> views = datas;
            views = views[..6];
            views[0] = Matrix4x4.Identity.CreateLookAt(Vector3.Zero, Vector3.UnitX, -Vector3.UnitY);
            views[1] = Matrix4x4.Identity.CreateLookAt(Vector3.Zero, -Vector3.UnitX, -Vector3.UnitY);
            views[2] = Matrix4x4.Identity.CreateLookAt(Vector3.Zero, Vector3.UnitY, Vector3.UnitZ);
            views[3] = Matrix4x4.Identity.CreateLookAt(Vector3.Zero, -Vector3.UnitY, Vector3.UnitZ);
            views[4] = Matrix4x4.Identity.CreateLookAt(Vector3.Zero, Vector3.UnitZ, -Vector3.UnitY);
            views[5] = Matrix4x4.Identity.CreateLookAt(Vector3.Zero, -Vector3.UnitZ, -Vector3.UnitY);
            try
            {
                for (int i = 0; i < 6; i++)
                {
                    foreach (var item in group.GetStaticObjects())
                    {
                        SkyDomeItem(item, pipe, cmdBuffer, projection, views[i], Vector4.Zero, projection, true, (uint)i);
                    }
                }
            }
            finally
            {
                ArrayPool<Matrix4x4>.Shared.Return(datas);
            }
            cmdBuffer.EndRenderPass();
        }
        private Matrix4x4 PreViewMatrix = Matrix4x4.Identity;
        private int FrameIndex = 0;
        private void SkyDomeItem(IRenderObject item,  
            VulkanPipeline pipe, VulkanCommandBuffer cmd, 
            Matrix4x4 projection, Matrix4x4 view, Vector4 camerPos, Matrix4x4 jProjection,  bool bakeMode, uint front)
        {
            int size = Marshal.SizeOf<Matrix4x4>();
            int vSize = Marshal.SizeOf<Vector4>();
            int tSize = size * 4;
            byte[] data = ArrayPool<byte>.Shared.Rent(tSize + vSize);
            try
            {
                uint mode = bakeMode ? (uint)1 : 0;
                Vector4 pos = camerPos;
                Matrix4x4 p = projection;
                Matrix4x4 v = view;
                Matrix4x4 preV = PreViewMatrix;
                Span<byte> span = data;
                span = span[..(tSize + vSize)];
                p.ToBytes(ref span, 0);
                v.ToBytes(ref span, size);
                preV.ToBytes(ref span, size * 2);
                jProjection.ToBytes(ref span, size * 3);
                pos.ToBytes(ref span, size * 4);
                var tex = pipe.GetPipelineTexture(3, 0);
                tex.UpdateTextureMemoryData(span);

                var vro = item.GetComponents<VRO>().First();
                DescriptorSet[] sets =
                [
                    pipe.GetPipelineDescriptorSet(0),
                    pipe.GetPipelineDescriptorSet(1),
                    pipe.GetPipelineDescriptorSet(2),
                    pipe.GetPipelineDescriptorSet(3),
                ];
                cmd.BindDescriptorSets(PipelineBindPoint.Graphics, pipe.PipelineLayout, 0, null, sets);
                cmd.BindVertexBuffers(0, [0], vro.VerticesBufferInfo.Buffer);
                cmd.PushConstants(pipe.PipelineLayout, ShaderStageFlags.FragmentBit, 0, MemoryMarshal.AsBytes(stackalloc uint[] { mode, front }));
                cmd.Draw(vro.VecticesCount, 1, 0, 0);

                PreViewMatrix = view;
            }
            finally
            {
                ArrayPool<byte>.Shared.Return(data);
            }
        }
        #endregion

        #region IBLPreRender
        /// <summary>
        /// 创建全局环境光渲染组
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="lDevice"></param>
        /// <param name="infos"></param>
        /// <param name="cache"></param>
        /// <param name="sphere"></param>
        /// <param name="skydomeMain"></param>
        /// <returns></returns>
        private PrimaryVulkanRenderGroup CreateIBLGroup(VulkanGraphicsManager manager, VulkanDevice lDevice,
            VulkanShaderInfo[] infos, PipelineCache cache, IRenderObject sphere, VulkanImageTextureData skydomeCube,
            PrimaryVulkanRenderGroup skydomeMain, PrimaryVulkanRenderGroup sceneMain)
        {
            var pass = CreateIBLPrePass(manager, IBLWidth);
            VulkanShaderInfo[] irradianceInfo = new VulkanShaderInfo[] { infos[0], infos[1] };
            var irradianceData = manager.CreateLocalImageTextureData(Constant.IBLIRRADIANCE, lDevice, new ImageAndImageViewCreateOption()
            {
                ImageCreateOption = new ImageCreateOption()
                {
                    ArrayLayers = 6,
                    Format = Format.R16G16B16A16Sfloat,
                    ImageType = ImageType.Type2D,
                    InitialLayout = ImageLayout.General,
                    MipLevels = 1,
                    Samples = SampleCountFlags.Count1Bit,
                    SharingMode = SharingMode.Exclusive,
                    Tiling = ImageTiling.Optimal,
                    Usage = ImageUsageFlags.StorageBit | ImageUsageFlags.SampledBit,
                    Flags = ImageCreateFlags.CreateCubeCompatibleBit
                },
                ImageViewCreateOption = new ImageViewCreateOption()
                {
                    Components = new ComponentMapping(ComponentSwizzle.R, ComponentSwizzle.G, ComponentSwizzle.B, ComponentSwizzle.A),
                    Format = Format.R16G16B16A16Sfloat,
                    SubresourceRange = new ImageSubresourceRange(ImageAspectFlags.ColorBit, 0, 1, 0, 6),
                    ViewType = ImageViewType.TypeCube
                }
            }, 
            new Extent3D(IBLWidth, IBLWidth, 1), FormatFeatureFlags.StorageImageBit | FormatFeatureFlags.SampledImageBit);
            var sampler = lDevice.CreateSampler(manager.NewStringToken(), new SamplerCreateOption()
            {
                AddressModeU = SamplerAddressMode.ClampToEdge,
                AddressModeV = SamplerAddressMode.ClampToEdge,
                AddressModeW = SamplerAddressMode.ClampToEdge,
                AnisotropyEnable = false,
                MaxAnisotropy = 0,
                BorderColor = BorderColor.FloatOpaqueWhite,
                CompareEnable = false,
                CompareOp = CompareOp.Never,
                MagFilter = Filter.Linear,
                MinFilter = Filter.Nearest,
                MaxLod = 0.0f,
                MinLod = 0.0f,
                MipLodBias = 0.0f,
                MipmapMode = SamplerMipmapMode.Nearest,
                UnnormalizedCoordinates = false
            });
            var irradianceTex = manager.CreateTexture(manager.NewStringToken(), irradianceData, null, 0, 0, 1, 0, DescriptorType.StorageImage);
            var skyDomeTex = manager.CreateTexture(manager.NewStringToken(), skydomeCube, sampler, 0, 0, 1, 1, DescriptorType.CombinedImageSampler);

            var irradiancePipe = manager.CreateEmptyPipeline("IBLIrradiance")
                .BuildPipelineLayout(CreateIrradianceSetLayout(), null)
                .BindPipelineTexture(skyDomeTex, 0, 0)
                .BindPipelineTexture(irradianceTex, 1, 0)
                .BuildGraphicsPipeline(pass, cache, irradianceInfo, CreateIrradiancePipeline(IBLWidth));
            var pCommand = lDevice.CreateCommandPool((device, option) =>
            {
                option.Flags = CommandPoolCreateFlags.ResetCommandBufferBit;
                option.QueueFamilyIndex = device.WorkQueues[0].FamilyIndex;
            }).CreatePrimaryCommandBuffer();
            var sCommand = lDevice.CreateCommandPool((device, option) =>
            {
                option.Flags = CommandPoolCreateFlags.ResetCommandBufferBit;
                option.QueueFamilyIndex = device.WorkQueues[0].FamilyIndex;
            }).CreateSecondaryCommandBuffer(3);
            var pEvent = lDevice.CreateEvent(manager.NewStringToken());
            var semaphores = lDevice.CreateSemaphore(manager.NewStringToken());

            var mainGroup = manager.CreatePrimaryRenderGroup(Constant.IBLGroup, null)
                .Binding(pCommand, lDevice, pass, new Semaphore[] { semaphores.RawHandle, skydomeMain.Semaphores[0] }, Array.Empty<Fence>(), new Event[] { pEvent.RawHandle });
            mainGroup.CreateGroupRenderingAction((b) => b.Run((g) => IBLMain(g)));
            mainGroup.AddChildren(skydomeMain);
            var irradianceGroup = manager.CreateSecondaryVulkanRenderGroup(Constant.IBLIrradianceGroup, irradiancePipe)
                .Binding(mainGroup, sCommand[0], Array.Empty<VulkanSemaphore>(), Array.Empty<VulkanFence>());
            irradianceGroup.CreateGroupRenderingAction((b) => b.Run((g) => IrradianceMain(g)));

            mainGroup.Priority = 1000;
            sceneMain.AddChildren(mainGroup);
            return mainGroup;
        }
        /// <summary>
        /// 创建IBL预渲染通道
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="cebeWidth"></param>
        /// <returns></returns>
        private VulkanRenderPass CreateIBLPrePass(VulkanGraphicsManager manager, uint cubeWidth)
        {
            var lDevice = manager.VulkanDevice ?? throw new NullReferenceException("Please create Vulkan deivce first");
            RenderPassCreateOption option = new RenderPassCreateOption()
            {
                Attachments = new AttachmentDescription[]
                {
                    new AttachmentDescription()
                    {
                        Format = Format.R8G8B8A8Unorm,
                        InitialLayout = ImageLayout.Undefined,
                        FinalLayout = ImageLayout.ColorAttachmentOptimal,
                        LoadOp = AttachmentLoadOp.Clear,
                        StoreOp = AttachmentStoreOp.DontCare,
                        StencilLoadOp = AttachmentLoadOp.DontCare,
                        StencilStoreOp = AttachmentStoreOp.DontCare,
                        Samples = SampleCountFlags.Count1Bit
                    }
                },
                Subpasses = new SubpassDescriptionOption[]
                {
                    new SubpassDescriptionOption()
                    {
                        ColorAttachments = new AttachmentReference[]
                        {
                            new AttachmentReference(0, ImageLayout.ColorAttachmentOptimal)
                        },
                        DepthStencilAttachment = new AttachmentReference(VulkanGraphicsManager.VK_ATTACHMENT_UNUSED),
                        PipelineBindPoint = PipelineBindPoint.Graphics
                    }
                },
                Dependencies = new SubpassDependency[]
                {
                    new SubpassDependency()
                    {
                        SrcAccessMask = AccessFlags.ColorAttachmentWriteBit,
                        DstAccessMask = 0,
                        SrcStageMask = PipelineStageFlags.ColorAttachmentOutputBit,
                        DstStageMask = PipelineStageFlags.BottomOfPipeBit,
                        SrcSubpass = 0,
                        DstSubpass = VulkanGraphicsManager.VK_SUBPASS_EXTERNAL,
                        DependencyFlags = DependencyFlags.ByRegionBit
                    }
                }
            };
            var pass = lDevice.CreateRenderPass($"IBLPreRender", option, RenderPassMode.Normal);
            ImageAndImageViewCreateOption[] attachment = new ImageAndImageViewCreateOption[]
            {
                new ImageAndImageViewCreateOption()
                {
                    ImageCreateOption = new ImageCreateOption
                    {
                        ArrayLayers = 1,
                        Extent = new Extent3D(cubeWidth, cubeWidth, 1),
                        Format = Format.R8G8B8A8Unorm,
                        ImageType = ImageType.Type2D,
                        InitialLayout = ImageLayout.Undefined,
                        MipLevels = 1,
                        Samples = SampleCountFlags.Count1Bit,
                        SharingMode = SharingMode.Exclusive,
                        Tiling = ImageTiling.Optimal,
                        Usage = ImageUsageFlags.ColorAttachmentBit
                    },
                    ImageViewCreateOption = new ImageViewCreateOption()
                    {
                        Components = new ComponentMapping(ComponentSwizzle.R, ComponentSwizzle.G, ComponentSwizzle.B, ComponentSwizzle.A),
                        Format = Format.R8G8B8A8Unorm,
                        SubresourceRange = new ImageSubresourceRange(ImageAspectFlags.ColorBit, 0, 1, 0, 1),
                        ViewType = ImageViewType.Type2D
                    }
                }
            };
            pass.CreateFrameBuffer(attachment, cubeWidth, cubeWidth);
            pass.ConfigBeginInfo((o) =>
            {
                o.RenderArea = new Rect2D { Extent = new Extent2D(cubeWidth, cubeWidth), Offset = new Offset2D { X = 0, Y = 0 } };
                o.ClearValues = new ClearValue[]
                {
                    new ClearValue() { Color = new ClearColorValue(0.0f, 0.0f, 0.0f, 0.0f) },
                };
            });
            return pass;
        }
        /// <summary>
        /// 创建漫反射辐照图管线布局
        /// </summary>
        /// <returns></returns>
        private DescriptorSetLayoutCreateOption[] CreateIrradianceSetLayout()
        {
            DescriptorSetLayoutCreateOption[] options =
            [
                new DescriptorSetLayoutCreateOption()
                {
                    Bindings =
                    [
                        new DescriptorSetLayoutBindingOption()
                        {
                            Binding = 0,
                            DescriptorCount = 1,
                            DescriptorType = DescriptorType.CombinedImageSampler,
                            StageFlags = ShaderStageFlags.FragmentBit
                        }
                    ],
                    DescriptorSetTarget = DescriptorSetTarget.Pipeline,
                    SetIndex = 0,
                },
                new DescriptorSetLayoutCreateOption()
                {
                    Bindings = new DescriptorSetLayoutBindingOption[]
                    {
                        new DescriptorSetLayoutBindingOption()
                        {
                            Binding = 0,
                            DescriptorCount = 1,
                            DescriptorType = DescriptorType.StorageImage,
                            StageFlags = ShaderStageFlags.FragmentBit
                        }
                    },
                    DescriptorSetTarget = DescriptorSetTarget.Pipeline,
                    SetIndex = 1,
                }
            ];
            return options;
        }
        /// <summary>
        /// 创建辐照度渲染管线
        /// </summary>
        /// <param name="cubeWidth"></param>
        /// <returns></returns>
        private GraphicsPipelineCreateOption CreateIrradiancePipeline(uint cubeWidth)
        {
            return new GraphicsPipelineCreateOption()
            {
                Stages =
                [
                    new PipelineShaderStageCreateOption() { Name = "main" },
                    new PipelineShaderStageCreateOption() { Name = "main" }
                ],
                VertexInputState = new PipelineVertexInputStateCreateOption
                {
                    VertexAttributeDescriptions = new VertexInputAttributeDescription[]
                    {
                        new VertexInputAttributeDescription(0, 0, Format.R32G32B32Sfloat, 0),
                        new VertexInputAttributeDescription(1, 0, Format.R32G32B32Sfloat, 12),
                        new VertexInputAttributeDescription(2, 0, Format.R32G32B32Sfloat, 24)
                    },
                    VertexBindingDescriptions = new VertexInputBindingDescription[]
                    {
                        new VertexInputBindingDescription(0, 36, VertexInputRate.Vertex)
                    }
                },
                InputAssemblyState = new PipelineInputAssemblyStateCreateOption()
                {
                    Topology = PrimitiveTopology.TriangleList,
                    PrimitiveRestartEnable = false
                },
                ViewportState = new PipelineViewportStateCreateOption()
                {
                    Viewports = new Viewport[]
                    {
                        new Viewport(0, 0, cubeWidth, cubeWidth, 0, 1.0f)
                    },
                    Scissors = new Rect2D[]
                    {
                        new Rect2D(new Offset2D(0, 0), new Extent2D(cubeWidth, cubeWidth))
                    }
                },
                RasterizationState = new PipelineRasterizationStateCreateOption()
                {
                    PolygonMode = PolygonMode.Fill,
                    CullMode = CullModeFlags.None,
                    FrontFace = FrontFace.CounterClockwise,
                    DepthClampEnable = false,
                    RasterizerDiscardEnable = false,
                    DepthBiasEnable = false,
                    DepthBiasConstantFactor = 0,
                    DepthBiasClamp = 0,
                    DepthBiasSlopeFactor = 0,
                    LineWidth = 1.0f
                },
                ColorBlendState = new PipelineColorBlendStateCreateOption()
                {
                    Attachments =
                    [
                        new PipelineColorBlendAttachmentState()
                        {
                            ColorWriteMask = ColorComponentFlags.RBit |
                            ColorComponentFlags.GBit|
                            ColorComponentFlags.BBit |
                            ColorComponentFlags.ABit,
                            BlendEnable = false,
                            AlphaBlendOp = BlendOp.Add,
                            ColorBlendOp = BlendOp.Add,
                            DstColorBlendFactor = BlendFactor.Zero,
                            SrcAlphaBlendFactor = BlendFactor.Zero,
                            DstAlphaBlendFactor = BlendFactor.Zero,
                            SrcColorBlendFactor = BlendFactor.Zero
                        }
                    ],
                    LogicOpEnable = false,
                    LogicOp = LogicOp.NoOp,
                    BlendConstants = (1.0f, 1.0f, 1.0f, 1.0f)
                },
                DepthStencilState = new PipelineDepthStencilStateCreateOption()
                {
                    Back = new StencilOpState()
                    {
                        FailOp = StencilOp.Keep,
                        PassOp = StencilOp.Keep,
                        CompareOp = CompareOp.Always,
                        CompareMask = 0,
                        Reference = 0,
                        DepthFailOp = StencilOp.Keep,
                        WriteMask = 0
                    },
                    Front = new StencilOpState()
                    {
                        FailOp = StencilOp.Keep,
                        PassOp = StencilOp.Keep,
                        CompareOp = CompareOp.Always,
                        CompareMask = 0,
                        Reference = 0,
                        DepthFailOp = StencilOp.Keep,
                        WriteMask = 0
                    },
                    DepthWriteEnable = false,
                    DepthTestEnable = false,
                    DepthCompareOp = CompareOp.Greater,
                    DepthBoundsTestEnable = false,
                    MinDepthBounds = 0,
                    MaxDepthBounds = 0,
                    StencilTestEnable = false
                },
                MultisampleState = new PipelineMultisampleStateCreateOption()
                {
                    RasterizationSamples = SampleCountFlags.Count1Bit,
                    SampleShadingEnable = false,
                    AlphaToCoverageEnable = false,
                    AlphaToOneEnable = false,
                    MinSampleShading = 0.0f
                },
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="group"></param>
        private void IBLMain(PrimaryVulkanRenderGroup group)
        {
            var queue = group.WorkQueues[0].Queue;
            var cmdBuffer = group.PrimaryCommandBuffer;
            var pass = group.RenderPass;
            var semaphores = group.Semaphores;
            var lDevice = group.LogicDevice;
            var @event = group.Events[0];

            foreach(var child in group.GetChildrens())
            {
                child.GroupRendering();
            }

            VulkanFrameBuffer framebuffer = group.RenderPass.GetFramebuffer(0);
 
            cmdBuffer.Reset();
            cmdBuffer.Begin();
            cmdBuffer.ResetEvent(@event, PipelineStageFlags.TopOfPipeBit);
            cmdBuffer.BeginRenderPass(pass, framebuffer, pass.BeginOption, SubpassContents.SecondaryCommandBuffers);
            group.SecondaryGroupRendering(pass, framebuffer);
            cmdBuffer.ExecuteCommands(group.GetUpdateSecondaryBuffers());
            cmdBuffer.EndRenderPass();
            cmdBuffer.SetEvent(@event, PipelineStageFlags.BottomOfPipeBit);
            cmdBuffer.End();
            var submitInfo = new SubmitOption
            {
                WaitDstStageMask = new PipelineStageFlags[] { PipelineStageFlags.BottomOfPipeBit },
                WaitSemaphores = new Semaphore[] { semaphores[1] },
                Buffers = new CommandBuffer[] { cmdBuffer.RawHandle },
                SignalSemaphores = new Semaphore[] { semaphores[0] }
            };
            lDevice.Submit(queue, new Fence(0), submitInfo);
        }
        /// <summary>
        /// 辐照图烘培主函数
        /// </summary>
        /// <param name="group"></param>
        private void IrradianceMain(SecondaryVulkanRenderGroup group)
        {
            var cmd = group.SecondaryCommandBuffer;
            var pipeline = group.Pipeline;
            CommandBufferInheritanceInfo info = new()
            {
                Framebuffer = group.Framebuffer.RawHandle,
                OcclusionQueryEnable = false,
                RenderPass = group.RenderPass.RawHandle,
                Subpass = 0
            };
            var tex = pipeline.GetPipelineTexture(0, 0);
            tex.UpdateDescriptorSets();
            cmd.Reset();
            cmd.Begin(CommandBufferUsageFlags.OneTimeSubmitBit |
                CommandBufferUsageFlags.RenderPassContinueBit, info);
            cmd.BindPipeline(PipelineBindPoint.Graphics, group.Pipeline);
            cmd.BindDescriptorSets(PipelineBindPoint.Graphics, pipeline.PipelineLayout, 0,
                null, pipeline.GetPipelineDescriptorSet(0), pipeline.GetPipelineDescriptorSet(1));
        }
        #endregion
    }
}
