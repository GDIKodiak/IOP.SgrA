﻿using IOP.SgrA.SilkNet.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.MainWorkSpace.Plugin
{
    [ModulePriority(ModulePriority.Core)]
    public class CameraModule : VulkanModule, ISceneModule
    {
        /// <summary>
        /// 
        /// </summary>
        public Scene Scene { get; set; }

        protected override Task Load(VulkanGraphicsManager manager)
        {
            var area = Scene.RenderArea;
            var camera = manager.CreateCamera("MainCamera", new Vector3(0, 150, 150), Vector3.Zero, new Vector3(0, 1, 0));
            camera.EncircleForX(Vector3.Zero, -3 * MathF.PI / 180.0f);
            float aspcet = (float)area.Width / area.Height;
            camera.SetProjectMatrix(-aspcet, 1.0f, 1.0f, 1000, reversedZ: true);
            camera.SetViewport(0, 0, area.Width, area.Height, 0.0f, 1.0f);
            Scene.RenderGroup.SetCamera(camera);
            Scene.LightEnvironment.AddPointLight("Light", new Vector3(0, 150, 100), new Vector4(23.47f, 21.31f, 20.79f, 1.0f));
            Scene.LightEnvironment.AddParallelLight("ParallelLight", new Vector3(0, -0.5f, -0.5f), new Vector4(15.5f));
            Scene.LightEnvironment.Ambient = new Ambient(1.0f, 1.0f, 1.0f, 1.0f);
            return Task.CompletedTask;
        }

        protected override Task Unload(VulkanGraphicsManager manager)
        {
            return Task.CompletedTask;
        }
    }
}
