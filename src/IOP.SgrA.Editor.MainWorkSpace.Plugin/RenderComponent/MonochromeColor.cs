﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.MainWorkSpace.Plugin
{
    /// <summary>
    /// 
    /// </summary>
    public class MonochromeColor : IRenderComponent
    {
        /// <summary>
        /// 
        /// </summary>
        public Vector3 Color { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Name => nameof(MonochromeColor);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="r"></param>
        /// <param name="g"></param>
        /// <param name="b"></param>
        public MonochromeColor(byte r, byte g, byte b)
        {
            Color = new Vector3(r / 255, g / 255, b / 255);
        }
        public MonochromeColor(Vector3 color)
        {
            Color = color;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newObj"></param>
        public void CloneToNewRender(IRenderObject newObj)
        {
            var c = new MonochromeColor(Color);
            newObj.AddOrUpdateComponent(c, (n, r) => c);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderObject"></param>
        public void OnAttach(IRenderObject renderObject) { }
        public void Destroy()
        {
        }
    }
}
