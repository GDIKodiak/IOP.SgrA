﻿using IOP.SgrA.SilkNet.Vulkan;
using Silk.NET.Vulkan;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.MainWorkSpace.Plugin
{
    /// <summary>
    /// 
    /// </summary>
    public class LineMiddleware : IProductionLineMiddleware<SecondaryVulkanRenderGroup>
    {
        private static Vector2[] Halton_2_3 = new Vector2[]
        {
            new(0.0f, -1.0f / 3.0f),
            new(-1.0f / 2.0f, 1.0f / 3.0f),
            new(1.0f / 2.0f, -7.0f / 9.0f),
            new(-3.0f / 4.0f, -1.0f / 9.0f),
            new(1.0f / 4.0f, 5.0f / 9.0f),
            new(-1.0f / 4.0f, -5.0f / 9.0f),
            new(3.0f / 4.0f, 1.0f / 9.0f),
            new(-7.0f / 8.0f, 7.0f / 9.0f)
        };
        private int FrameIndex = 0;


        public void Invoke(SecondaryVulkanRenderGroup group, RenderingProductionLineDelegate<SecondaryVulkanRenderGroup> next)
        {
            var camera = group.Camera;
            var view = camera.ViewMatrix;
            var project = camera.ProjectionMatrix;
            var cmd = group.SecondaryCommandBuffer;
            CommandBufferInheritanceInfo info = new()
            {
                Framebuffer = group.Framebuffer.RawHandle,
                OcclusionQueryEnable = false,
                RenderPass = group.RenderPass.RawHandle,
                Subpass = 0
            };
            cmd.Reset();
            cmd.Begin(CommandBufferUsageFlags.OneTimeSubmitBit |
                CommandBufferUsageFlags.RenderPassContinueBit, info);
            cmd.BindPipeline(PipelineBindPoint.Graphics, group.Pipeline);
            var viewPort = camera.Viewport;
            cmd.SetViewport(new Viewport(viewPort.X, viewPort.Y, viewPort.Width, viewPort.Height, viewPort.MinDepth, viewPort.MaxDepth));
            cmd.SetScissor(new Rect2D(null, new Extent2D((uint)viewPort.Width, (uint)viewPort.Height)));
            cmd.SetLineWidth(2.0f);

            Vector2 jitter = new Vector2((Halton_2_3[FrameIndex].X - 0.5f) / viewPort.Width * 2,
                (Halton_2_3[FrameIndex].Y - 0.5f) / viewPort.Height * 2);
            Matrix4x4 jProjection = project;
            int size = Marshal.SizeOf<Matrix4x4>();
            byte[] bytes = ArrayPool<byte>.Shared.Rent(size * 2); 
            Span<byte> data = bytes; data = data[..(size * 2)];
            jProjection[2, 0] += jitter.X;
            jProjection[2, 1] += jitter.Y;
            jProjection.ToBytes(ref data, 0);
            view.ToBytes(ref data, size);
            var tex = group.Pipeline.GetPipelineTexture(1, 0);
            tex.UpdateTextureMemoryData(data, 0);

            foreach (var item in group.GetContexts())
            {
                item.SetViewMatrix(in view);
                item.SetProjectionMatrix(in project);
                item.SetCamera(camera);
                group.ItemProductionLine(item);
            }
            cmd.End();

            FrameIndex++;
            FrameIndex %= 8;
            ArrayPool<byte>.Shared.Return(bytes);
        }
    }
}
