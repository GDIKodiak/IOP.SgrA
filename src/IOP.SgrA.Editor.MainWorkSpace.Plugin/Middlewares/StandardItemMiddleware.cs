﻿using IOP.SgrA.SilkNet.Vulkan;
using Silk.NET.Vulkan;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.MainWorkSpace.Plugin
{
    /// <summary>
    /// 标准项中间件
    /// </summary>
    public class StandardItemMiddleware : IProductionLineMiddleware<VulkanContext>
    {
        public void Invoke(VulkanContext context, RenderingProductionLineDelegate<VulkanContext> next)
        {
            var render = context.GetContextRenderObject();
            var pipeline = context.VulkanPipeline;
            int size = Marshal.SizeOf<Matrix4x4>();
            ref MVPMatrix local = ref context.GetMVPMatrix();
            Matrix4x4 preview = context.GetPreMVPMatrix().GetFinalMatrix();
            var t = render.GetTransform();
            local.ModelMatrix = t.CreateMatrix();
            Matrix4x4 mat = local.GetFinalMatrix();
            byte[] data = ArrayPool<byte>.Shared.Rent(size * 3);
            Span<byte> span = data;
            span = span[..(size * 3)];
            mat.ToBytes(ref span, 0);
            local.ModelMatrix.ToBytes(ref span, size);
            preview.ToBytes(ref span, size * 2);

            var vro = render.GetComponents<VRO>().First();
            var tex = render.GetComponents<VulkanTexture>().First();
            var cmd = context.CommandBuffer;

            tex.UpdateTextureMemoryData(span);
            DescriptorSet[] sets = [tex.DescriptorSet, pipeline.GetPipelineDescriptorSet(1)];
            cmd.BindDescriptorSets(PipelineBindPoint.Graphics, pipeline.PipelineLayout, 0, null, sets);
            cmd.BindVertexBuffers(0, [0], vro.VerticesBufferInfo.Buffer);
            cmd.Draw(vro.VecticesCount, 1, 0, 0);
            ArrayPool<byte>.Shared.Return(data);
            context.BackupsCurrnetMVP();
        }
    }
}
