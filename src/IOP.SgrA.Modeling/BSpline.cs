﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;

namespace IOP.SgrA.Modeling
{
    /// <summary>
    /// 
    /// </summary>
    public static class BSpline
    {
        /// <summary>
        /// 计算BSpline插值坐标
        /// </summary>
        /// <param name="t">参数</param>
        /// <param name="degree">阶数</param>
        /// <param name="controlPoints">控制点</param>
        /// <param name="knots"></param>
        /// <param name="weights">权重</param>
        /// <see cref="https://github.com/thibauts/b-spline"/>
        /// <returns></returns>
        public static Vector3 Interpolate(float t, int degree, in ReadOnlySpan<Vector3> controlPoints, float[] knots, float[] weights)
        {
            int n = controlPoints.Length;
            int d = 3;
            if (degree < 1) throw new ArgumentOutOfRangeException("degree must be at least 1 (linear)");
            if (degree > (n - 1)) throw new ArgumentOutOfRangeException("degree must be less than or equal to control point count - 1");
            if (weights == null) throw new ArgumentNullException(nameof(weights));
            if (weights.Length != n) throw new ArgumentException("weights array's length must be same as control point's length");
            if (knots == null) throw new ArgumentNullException(nameof(knots));
            if (knots.Length != n + degree + 1) throw new ArgumentException("bad knot vector length");

            float[,] points = new float[n, d];
            for(int i = 0; i < n; i++)
            {
                Vector3 vector = controlPoints[i];
                points[i, 0] = vector.X;
                points[i, 1] = vector.Y;
                points[i, 2] = vector.Z;
            }

            Span<int> domain = stackalloc int[]
            {
                degree,
                knots.Length - 1 - degree
            };

            // remap t to the domain where the spline is defined
            var low = knots[domain[0]];
            var high = knots[domain[1]];
            t = t * (high - low) + low;
            if (t < low || t > high) throw new ArgumentOutOfRangeException("out of bounds");

            int s = 0;
            // find s (the spline segment) for the [t] value provided
            for (s = domain[0]; s < domain[1]; s++)
            {
                if (t >= knots[s] && t <= knots[s + 1])
                {
                    break;
                }
            }

            // convert points to homogeneous coordinates
            float[,] v = new float[n, (d + 1)];
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < d; j++)
                {
                    v[i, j] = points[i, j] * weights[i];
                }
                v[i, d] = weights[i];
            }

            // l (level) goes from 1 to the curve degree + 
            float alpha;
            for (int l = 1; l <= degree + 1; l++)
            {
                // build level l of the pyramid
                for (int i = s; i > s - degree - 1 + l; i--)
                {
                    alpha = (t - knots[i]) / (knots[i + degree + 1 - l] - knots[i]);

                    // interpolate each component
                    for (int j = 0; j < d + 1; j++)
                    {
                        v[i, j] = (1 - alpha) * v[i - 1, j] + alpha * v[i, j];
                    }
                }
            }

            // convert back to cartesian and return
            Span<float> result = stackalloc float[d];
            for(int i = 0; i < d; i++)
            {
                result[i] = v[s, i] / v[s, d];
            }

            return new Vector3(result[0], result[1], result[2]);
        }
        /// <summary>
        /// 创建BSpline坐标序列
        /// </summary>
        /// <param name="pointLength">输出点数量，用于计算步进</param>
        /// <param name="degree"></param>
        /// <param name="points"></param>
        /// <param name="knots"></param>
        /// <param name="weights"></param>
        /// <returns></returns>
        public static Span<Vector3> CreateBSpline(int pointLength, int degree, Vector3[] points, float[]? knots, float[]? weights)
        {
            if(points == null) throw new ArgumentNullException(nameof(points));
            if(pointLength <= 2) pointLength = 2;
            float tStep = 1.0f / pointLength;
            List<Vector3> result = new List<Vector3>();
            ReadOnlySpan<Vector3> p = points;

            int n = points.Length;
            if (weights == null)
            {
                // build weight vector of length [n]
                weights = new float[n];
                for (int i = 0; i < n; i++)
                {
                    weights[i] = 1;
                }
            }
            else
            {
                if (weights.Length != n) throw new ArgumentException("weights array's length must be same as control point's length");
            }
            if (knots == null)
            {
                knots = new float[n + degree + 1];
                for (int i = 0; i < n + degree + 1; i++)
                {
                    knots[i] = i;
                }
            }
            else
            {
                if (knots.Length != n + degree + 1) throw new ArgumentException("bad knot vector length");
            }

            for (float t = 0; t <= 1; t += tStep)
            {
                result.Add(Interpolate(t, degree, in p, knots, weights));
            }
            return result.ToArray();
        }
    }
}
