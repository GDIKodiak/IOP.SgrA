﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Modeling
{
    public static class Box
    {
        /// <summary>
        /// 创建无法线单色立方体
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="color"></param>
        /// <returns></returns>
        public static Span<Vector3> CreateMonoBox(float x, float y, float z, Vector3 color, Vector3 axisDir, 
            out uint VCount, out Vector3 min, out Vector3 max)
        {
            List<Vector3> result = new(72);
            VCount = 36;

            Matrix4x4 trans;
            if (axisDir == Vector3.UnitX) trans = Matrix4x4.Identity;
            else trans = axisDir.GetRotateFromUnitX();


            min = Vector3.Transform(new Vector3(-x, -y, -z), trans);
            max = Vector3.Transform(new Vector3(x, y, z), trans);

            result.Add(Vector3.Transform(new Vector3(-x, y, z), trans));
            result.Add(color);
            result.Add(Vector3.Transform(new Vector3(-x, -y, z), trans));
            result.Add(color);
            result.Add(Vector3.Transform(new Vector3(x, y, z), trans));
            result.Add(color);
            result.Add(Vector3.Transform(new Vector3(x, y, z), trans));
            result.Add(color);
            result.Add(Vector3.Transform(new Vector3(-x, -y, z), trans));
            result.Add(color);
            result.Add(Vector3.Transform(new Vector3(x, -y, z), trans));
            result.Add(color);

            result.Add(Vector3.Transform(new Vector3(x, y, -z), trans));
            result.Add(color);
            result.Add(Vector3.Transform(new Vector3(x, -y, -z), trans));
            result.Add(color);
            result.Add(Vector3.Transform(new Vector3(-x, -y, -z), trans));
            result.Add(color);
            result.Add(Vector3.Transform(new Vector3(x, y, -z), trans));
            result.Add(color);
            result.Add(Vector3.Transform(new Vector3(-x, -y, -z), trans));
            result.Add(color);
            result.Add(Vector3.Transform(new Vector3(-x, y, -z), trans));
            result.Add(color);

            result.Add(Vector3.Transform(new Vector3(-x, y, -z), trans));
            result.Add(color);
            result.Add(Vector3.Transform(new Vector3(-x, -y, -z), trans));
            result.Add(color);
            result.Add(Vector3.Transform(new Vector3(-x, -y, z), trans));
            result.Add(color);
            result.Add(Vector3.Transform(new Vector3(-x, y, -z), trans));
            result.Add(color);
            result.Add(Vector3.Transform(new Vector3(-x, -y, z), trans));
            result.Add(color);
            result.Add(Vector3.Transform(new Vector3(-x, y, z), trans));
            result.Add(color);

            result.Add(Vector3.Transform(new Vector3(x, y, z), trans));
            result.Add(color);
            result.Add(Vector3.Transform(new Vector3(x, -y, z), trans));
            result.Add(color);
            result.Add(Vector3.Transform(new Vector3(x, -y, -z), trans));
            result.Add(color);
            result.Add(Vector3.Transform(new Vector3(x, y, z), trans));
            result.Add(color);
            result.Add(Vector3.Transform(new Vector3(x, -y, -z), trans));
            result.Add(color);
            result.Add(Vector3.Transform(new Vector3(x, y, -z), trans));
            result.Add(color);

            result.Add(Vector3.Transform(new Vector3(-x, y, -z), trans));
            result.Add(color);
            result.Add(Vector3.Transform(new Vector3(-x, y, z), trans));
            result.Add(color);
            result.Add(Vector3.Transform(new Vector3(x, y, z), trans));
            result.Add(color);
            result.Add(Vector3.Transform(new Vector3(-x, y, -z), trans));
            result.Add(color);
            result.Add(Vector3.Transform(new Vector3(x, y, z), trans));
            result.Add(color);
            result.Add(Vector3.Transform(new Vector3(x, y, -z), trans));
            result.Add(color);

            result.Add(Vector3.Transform(new Vector3(-x, -y, z), trans));
            result.Add(Vector3.Transform(new Vector3(0, 1, 0), trans));
            result.Add(Vector3.Transform(new Vector3(-x, -y, -z), trans));
            result.Add(Vector3.Transform(new Vector3(0, 1, 1), trans));
            result.Add(Vector3.Transform(new Vector3(x, -y, -z), trans));
            result.Add(Vector3.Transform(new Vector3(1, 1, 1), trans));
            result.Add(Vector3.Transform(new Vector3(-x, -y, z), trans));
            result.Add(Vector3.Transform(new Vector3(0, 1, 0), trans));
            result.Add(Vector3.Transform(new Vector3(x, -y, -z), trans));
            result.Add(Vector3.Transform(new Vector3(1, 1, 1), trans));
            result.Add(Vector3.Transform(new Vector3(x, -y, z), trans));
            result.Add(Vector3.Transform(new Vector3(1, 1, 0), trans));

            return result.ToArray();
        }
        /// <summary>
        /// 创建一个无法线使用3维纹理的立方体
        /// 纹理坐标原点位于左上角
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="axisDir"></param>
        /// <param name="VCount"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static Span<Vector3> CreateBoxWithTexcoord3(float x, float y, float z, Vector3 axisDir,
            out uint VCount, out Vector3 min, out Vector3 max)
        {
            List<Vector3> result = new(72);
            VCount = 36;

            Matrix4x4 trans;
            if (axisDir == Vector3.UnitX) trans = Matrix4x4.Identity;
            else trans = axisDir.GetRotateFromUnitX();


            min = Vector3.Transform(new Vector3(-x, -y, -z), trans);
            max = Vector3.Transform(new Vector3(x, y, z), trans);

            result.Add(Vector3.Transform(new Vector3(-x, y, z), trans));
            result.Add(Vector3.Transform(new Vector3(0, 0, 0), trans));
            result.Add(Vector3.Transform(new Vector3(-x, -y, z), trans));
            result.Add(Vector3.Transform(new Vector3(0, 1, 0), trans));
            result.Add(Vector3.Transform(new Vector3(x, y, z), trans));
            result.Add(Vector3.Transform(new Vector3(1, 0, 0), trans));
            result.Add(Vector3.Transform(new Vector3(x, y, z), trans));
            result.Add(Vector3.Transform(new Vector3(1, 0, 0), trans));
            result.Add(Vector3.Transform(new Vector3(-x, -y, z), trans));
            result.Add(Vector3.Transform(new Vector3(0, 1, 0), trans));
            result.Add(Vector3.Transform(new Vector3(x, -y, z), trans));
            result.Add(Vector3.Transform(new Vector3(1, 1, 0), trans));

            result.Add(Vector3.Transform(new Vector3(x, y, -z), trans));
            result.Add(Vector3.Transform(new Vector3(1, 0, 1), trans));
            result.Add(Vector3.Transform(new Vector3(x, -y, -z), trans));
            result.Add(Vector3.Transform(new Vector3(1, 1, 1), trans));
            result.Add(Vector3.Transform(new Vector3(-x, -y, -z), trans));
            result.Add(Vector3.Transform(new Vector3(0, 1, 1), trans));
            result.Add(Vector3.Transform(new Vector3(x, y, -z), trans));
            result.Add(Vector3.Transform(new Vector3(1, 0, 1), trans));
            result.Add(Vector3.Transform(new Vector3(-x, -y, -z), trans));
            result.Add(Vector3.Transform(new Vector3(0, 1, 1), trans));
            result.Add(Vector3.Transform(new Vector3(-x, y, -z), trans));
            result.Add(Vector3.Transform(new Vector3(0, 0, 1), trans));

            result.Add(Vector3.Transform(new Vector3(-x, y, -z), trans));
            result.Add(Vector3.Transform(new Vector3(0, 0, 1), trans));
            result.Add(Vector3.Transform(new Vector3(-x, -y, -z), trans));
            result.Add(Vector3.Transform(new Vector3(0, 1, 1), trans));
            result.Add(Vector3.Transform(new Vector3(-x, -y, z), trans));
            result.Add(Vector3.Transform(new Vector3(0, 1, 0), trans));
            result.Add(Vector3.Transform(new Vector3(-x, y, -z), trans));
            result.Add(Vector3.Transform(new Vector3(0, 0, 1), trans));
            result.Add(Vector3.Transform(new Vector3(-x, -y, z), trans));
            result.Add(Vector3.Transform(new Vector3(0, 1, 0), trans));
            result.Add(Vector3.Transform(new Vector3(-x, y, z), trans));
            result.Add(Vector3.Transform(new Vector3(0, 0, 0), trans));

            result.Add(Vector3.Transform(new Vector3(x, y, z), trans));
            result.Add(Vector3.Transform(new Vector3(1, 0, 0), trans));
            result.Add(Vector3.Transform(new Vector3(x, -y, z), trans));
            result.Add(Vector3.Transform(new Vector3(1, 1, 0), trans));
            result.Add(Vector3.Transform(new Vector3(x, -y, -z), trans));
            result.Add(Vector3.Transform(new Vector3(1, 1, 1), trans));
            result.Add(Vector3.Transform(new Vector3(x, y, z), trans));
            result.Add(Vector3.Transform(new Vector3(1, 0, 0), trans));
            result.Add(Vector3.Transform(new Vector3(x, -y, -z), trans));
            result.Add(Vector3.Transform(new Vector3(1, 1, 1), trans));
            result.Add(Vector3.Transform(new Vector3(x, y, -z), trans));
            result.Add(Vector3.Transform(new Vector3(1, 0, 1), trans));

            result.Add(Vector3.Transform(new Vector3(-x, y, -z), trans));
            result.Add(Vector3.Transform(new Vector3(0, 0, 1), trans));
            result.Add(Vector3.Transform(new Vector3(-x, y, z), trans));
            result.Add(Vector3.Transform(new Vector3(0, 0, 0), trans));
            result.Add(Vector3.Transform(new Vector3(x, y, z), trans));
            result.Add(Vector3.Transform(new Vector3(1, 0, 0), trans));
            result.Add(Vector3.Transform(new Vector3(-x, y, -z), trans));
            result.Add(Vector3.Transform(new Vector3(0, 0, 1), trans));
            result.Add(Vector3.Transform(new Vector3(x, y, z), trans));
            result.Add(Vector3.Transform(new Vector3(1, 0, 0), trans));
            result.Add(Vector3.Transform(new Vector3(x, y, -z), trans));
            result.Add(Vector3.Transform(new Vector3(1, 0, 1), trans));

            result.Add(Vector3.Transform(new Vector3(-x, -y, z), trans));
            result.Add(Vector3.Transform(new Vector3(0, 1, 0), trans));
            result.Add(Vector3.Transform(new Vector3(-x, -y, -z), trans));
            result.Add(Vector3.Transform(new Vector3(0, 1, 1), trans));
            result.Add(Vector3.Transform(new Vector3(x, -y, -z), trans));
            result.Add(Vector3.Transform(new Vector3(1, 1, 1), trans));
            result.Add(Vector3.Transform(new Vector3(-x, -y, z), trans));
            result.Add(Vector3.Transform(new Vector3(0, 1, 0), trans));
            result.Add(Vector3.Transform(new Vector3(x, -y, -z), trans));
            result.Add(Vector3.Transform(new Vector3(1, 1, 1), trans));
            result.Add(Vector3.Transform(new Vector3(x, -y, z), trans));
            result.Add(Vector3.Transform(new Vector3(1, 1, 0), trans));

            return result.ToArray();
        }
    }
}
