﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Modeling
{
    /// <summary>
    /// 
    /// </summary>
    public static class CoordinateArrow
    {
        /// <summary>
        /// 创建坐标系箭头
        /// </summary>
        /// <param name="color"></param>
        /// <param name="axisDir"></param>
        /// <returns></returns>
        public static List<Vector3> CreateCoordinateArrow(Vector3 color, Vector3 axisDir, out uint vCount, out Vector3 min, out Vector3 max)
        {
            List<Vector3> result = new();
            Matrix4x4 trans;
            if (axisDir == Vector3.UnitX) trans = Matrix4x4.Identity;
            else trans = axisDir.GetRotateFromUnitX();

            min = Vector3.Transform(new Vector3(0.0f, -8, -8), trans); 
            max = Vector3.Transform(new Vector3(124, 8, 8), trans);

            Span<Vector3> cone = Cone.CreateMonoCone(100, 24, 6, 2f, axisDir, color, out uint v);
            vCount = v;
            result.AddRange(cone.ToArray());

            Span<Vector3> cylinder = Cylinder.CreateMonoCylinder(100, 1, 2f, axisDir, color, out v);
            vCount += v;
            result.AddRange(cylinder.ToArray());

            return result;
        }
    }
}
