﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Modeling
{
    /// <summary>
    /// 
    /// </summary>
    public static class Cone
    {
        /// <summary>
        /// 创建单色无法线圆锥
        /// </summary>
        /// <param name="bottomX"></param>
        /// <param name="height"></param>
        /// <param name="radius"></param>
        /// <param name="angleSpan"></param>
        /// <param name="axisDir"></param>
        /// <param name="color"></param>
        /// <param name="vCount"></param>
        /// <returns></returns>
        public static Span<Vector3> CreateMonoCone(float bottomX, float height, float radius, float angleSpan, Vector3 axisDir, Vector3 color, out uint vCount)
        {
            List<Vector3> alVertix = new List<Vector3>();
            float r = radius;
            vCount = 0;

            Matrix4x4 trans;
            if (axisDir == Vector3.UnitX) trans = Matrix4x4.Identity;
            else trans = axisDir.GetRotateFromUnitX();

            for (float vAngle = 360f; vAngle > 0; vAngle -= angleSpan)
            {
                double x1 = bottomX + height;
                double y1 = 0.0;
                double z1 = 0.0;
                double x2 = bottomX;
                double y2 = r * MathF.Sin(vAngle.ToRadians());
                double z2 = r * MathF.Cos(vAngle.ToRadians());
                double x3 = bottomX;
                double y3 = r * MathF.Sin((vAngle - angleSpan).ToRadians());
                double z3 = r * MathF.Cos((vAngle - angleSpan).ToRadians());

                Vector3 p1 = Vector3.Transform(new Vector3((float)x1, (float)y1, (float)z1), trans);
                Vector3 p2 = Vector3.Transform(new Vector3((float)x2, (float)y2, (float)z2), trans);
                Vector3 p3 = Vector3.Transform(new Vector3((float)x3, (float)y3, (float)z3), trans);
                alVertix.Add(p1);
                alVertix.Add(color);
                alVertix.Add(p2);
                alVertix.Add(color);
                alVertix.Add(p3);
                alVertix.Add(color);
            }

            for (float vAngle = 360f; vAngle > 0; vAngle -= angleSpan)
            {
                double x1 = bottomX;
                double y1 = 0.0f;
                double z1 = 0.0f;
                double x2 = bottomX;
                double y2 = r * MathF.Sin(vAngle.ToRadians());
                double z2 = r * MathF.Cos(vAngle.ToRadians());
                double x3 = bottomX;
                double y3 = r * MathF.Sin((vAngle - angleSpan).ToRadians());
                double z3 = r * MathF.Cos((vAngle - angleSpan).ToRadians());

                Vector3 p1 = Vector3.Transform(new Vector3((float)x1, (float)y1, (float)z1), trans);
                Vector3 p2 = Vector3.Transform(new Vector3((float)x2, (float)y2, (float)z2), trans);
                Vector3 p3 = Vector3.Transform(new Vector3((float)x3, (float)y3, (float)z3), trans);
                alVertix.Add(p1);
                alVertix.Add(color);
                alVertix.Add(p2);
                alVertix.Add(color);
                alVertix.Add(p3);
                alVertix.Add(color);
            }

            vCount += (uint)alVertix.Count / 2;
            return alVertix.ToArray();
        }
    }
}
