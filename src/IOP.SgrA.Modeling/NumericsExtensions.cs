﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Modeling
{
    public static class NumericsExtensions
    {
        /// <summary>
        /// 获取从X轴旋转到指定轴的旋转矩阵
        /// </summary>
        /// <param name="axis"></param>
        /// <returns></returns>
        public static Matrix4x4 GetRotateFromUnitX(this Vector3 axis)
        {
            axis = Vector3.Normalize(axis);
            Vector3 cross = Vector3.Cross(axis, -Vector3.UnitX);
            float cosX = Vector3.Dot(axis, Vector3.UnitX);
            float radian = MathF.Acos(cosX);
            Quaternion quaternion = Quaternion.CreateFromAxisAngle(cross, radian);
            Matrix4x4 resut = Matrix4x4.CreateFromQuaternion(quaternion);
            return resut;
        }
    }
}
