﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Modeling.BRep
{
    /// <summary>
    /// B-Rep顶点
    /// </summary>
    public class BVertex
    {
        public int Index;
        public Vector3 Position;
        public Vector3 Normal;
        public Vector3 UV;
        public int EdgeIndex;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="position"></param>
        /// <param name="normal"></param>
        /// <param name="uv"></param>
        public BVertex(int index, Vector3 position, Vector3 normal, Vector3 uv)
        {
            Index = index;
            Position = position;
            Normal = normal;
            UV = uv;
            EdgeIndex = -1;
        }

        /// <summary>
        /// 是否一致
        /// </summary>
        /// <param name="other"></param>
        /// <param name="err"></param>
        /// <returns></returns>
        public bool IsSame(BVertex other, float err = float.Epsilon) 
        {
            float t1 = MathF.Abs(Position.X - other.Position.X);
            float t2 = MathF.Abs(Position.Y - other.Position.Y);
            float t3 = MathF.Abs(Position.Z - other.Position.Z);
            return t1 <= err && t2 <= err && t3 <= err;
        }
    }
}
