﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Modeling.BRep
{
    /// <summary>
    /// B-Rep三角形
    /// </summary>
    public class BTriangle
    {
        public int Index;
        /// <summary>
        /// 
        /// </summary>
        public int Edge1;
        /// <summary>
        /// 
        /// </summary>
        public int Edge2;
        /// <summary>
        /// 
        /// </summary>
        public int Edge3;
        /// <summary>
        /// 
        /// </summary>
        public int V1;
        /// <summary>
        /// 
        /// </summary>
        public int V2;
        /// <summary>
        /// 
        /// </summary>
        public int V3;
        /// <summary>
        /// 
        /// </summary>
        public Vector3 Normal;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e1"></param>
        /// <param name="e2"></param>
        /// <param name="e3"></param>
        public BTriangle(int index, int e1, int e2, int e3, int v1, int v2, int v3, Vector3 normal)
        {
            Index = index;
            Edge1 = e1; 
            Edge2 = e2; 
            Edge3 = e3;
            V1 = v1;
            V2 = v2;
            V3 = v3;
            Normal = normal;
        }

        public int GetAdjVertexFromEdge(BEdge edge)
        {
            if(edge.Vertex1 == V1)
            {
                if (edge.Vertex2 == V2) return V3;
                else if (edge.Vertex2 == V3) return V2;
            }
            else if(edge.Vertex1 == V2)
            {
                if (edge.Vertex2 == V1) return V3;
                else if (edge.Vertex2 == V3) return V1;
            }
            else if(edge.Vertex1 == V3)
            {
                if (edge.Vertex2 == V1) return V2;
                else if (edge.Vertex2 == V2) return V1;
            }
            return -1;
        }
    }
}
