﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Modeling.BRep
{
    /// <summary>
    /// 
    /// </summary>
    public class BEdge
    {
        /// <summary>
        /// 下标
        /// </summary>
        public int Index;
        /// <summary>
        /// 起始点
        /// </summary>
        public int Vertex1;
        /// <summary>
        /// 终止点
        /// </summary>
        public int Vertex2;
        /// <summary>
        /// 左面
        /// </summary>
        public int FaceLeft;
        /// <summary>
        /// 右面
        /// </summary>
        public int FaceRight;
        /// <summary>
        /// 左面边1
        /// </summary>
        public int LeftFacePredEdge;
        /// <summary>
        /// 左面边2
        /// </summary>
        public int LeftFaceSuccEdge;
        /// <summary>
        /// 右面边1
        /// </summary>
        public int RightFacePredEdge;
        /// <summary>
        /// 右面边2
        /// </summary>
        public int RightFaceSuccEdge;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="vStart"></param>
        /// <param name="vEnd"></param>
        public BEdge(int index, int vStart, int vEnd)
        {
            Index = index;
            Vertex1 = vStart;
            Vertex2 = vEnd;
            FaceLeft = -1;
            FaceRight = -1;
            LeftFacePredEdge = -1;
            LeftFaceSuccEdge = -1;
            RightFacePredEdge = -1;
            RightFaceSuccEdge = -1;
        }

        public bool IsSame(BEdge other) 
        {
            return (Vertex1 == other.Vertex1 && Vertex2 == other.Vertex2)
                || (Vertex2 == other.Vertex1 && Vertex1 == other.Vertex2);
        }
    }
}
