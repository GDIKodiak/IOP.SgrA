﻿using Microsoft.Extensions.Logging;
using Python.Runtime;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace IOP.SgrA.PythonEnvironment
{
    /// <summary>
    /// 
    /// </summary>
    public class PyEnvironment
    {
        private static Channel<PyFuncExecutor<object>> PyFuncChannel = Channel.CreateBounded<PyFuncExecutor<object>>(64);
        private static CancellationTokenSource TokenSource = null;
        private ILogger<PyEnvironment> Logger;
        private PyTaskScheduler Scheduler;
        private int _IsInit;


        public bool IsInitialization
        {
            get
            {
                SpinWait wait = new SpinWait();
                int old = _IsInit;
                while (Interlocked.CompareExchange(ref _IsInit, old, old) != old)
                {
                    wait.SpinOnce();
                    old = _IsInit;
                }
                return old == 1;
            }
            private set
            {
                int v = value ? 1 : 0;
                SpinWait wait = new SpinWait();
                int old = _IsInit;
                while (Interlocked.CompareExchange(ref _IsInit, v, old) != old)
                {
                    wait.SpinOnce();
                    old = _IsInit;
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        internal PyEnvironment(ILoggerFactory logger)
        {
            Logger = logger.CreateLogger<PyEnvironment>();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pyFunc"></param>
        /// <returns></returns>
        public async ValueTask<PyFuncExecutor<object>> NewPyFunc(Func<object> pyFunc)
        {
            PyFuncExecutor<object> executor = new(pyFunc);
            await PyFuncChannel.Writer.WriteAsync(executor);
            return executor;
        }
        /// <summary>
        /// 
        /// </summary>
        public void Init()
        {
            if (IsInitialization) return;
            TokenSource?.Cancel();
            TokenSource = new CancellationTokenSource();
            Scheduler = new PyTaskScheduler();
            Task.Factory.StartNew(async () =>
            {
                var reader = PyFuncChannel.Reader;
                try
                {
                    
                    while (!TokenSource.IsCancellationRequested)
                    {
                        while (reader.TryRead(out PyFuncExecutor<object> obj))
                        {
                            if (obj == null) continue;
                            var awaiter = obj.GetAwaiter();
                            try
                            {
                                var state = obj.Func?.Invoke();
                                awaiter.MoveToComplete(state);
                            }
                            catch (Exception e)
                            {
                                Logger?.LogError(e, "");
                                awaiter.MoveToComplete(null);
                            }
                        }
                        if (!await reader.WaitToReadAsync(TokenSource.Token)) break;
                    }
                }
                finally
                {
                }
            }, TokenSource.Token, TaskCreationOptions.LongRunning, Scheduler);
            IsInitialization = true;
        }
        /// <summary>
        /// 
        /// </summary>
        public void Finish()
        {
            TokenSource.Cancel();
            PyFuncChannel.Writer.Complete();
            Scheduler.Dispose();
        }
    }
}
