﻿using Python.Runtime;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.PythonEnvironment
{
    /// <summary>
    /// Python单线程任务调度器
    /// </summary>
    public sealed class PyTaskScheduler : TaskScheduler, IDisposable
    {
        private BlockingCollection<Task> tasksCollection = new BlockingCollection<Task>();
        private readonly Thread mainThread = null;
        private SynchronizationContext Context = null;

        public PyTaskScheduler()
        {
            mainThread = new Thread(Execute);
            mainThread.IsBackground = true;
            if (!mainThread.IsAlive)
            {
                mainThread.Start();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override IEnumerable<Task> GetScheduledTasks()
        {
            foreach(var task in tasksCollection)
            {
                yield return task;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="task"></param>
        protected override void QueueTask(Task task)
        {
            if (task != null) tasksCollection.Add(task);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="task"></param>
        /// <param name="taskWasPreviouslyQueued"></param>
        /// <returns></returns>
        protected override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued)
        {
            return false;
        }

        private void Execute()
        {
            IntPtr ptr = PythonEngine.BeginAllowThreads();
            Py.GILState gil = Py.GIL();
            Context = SynchronizationContext.Current;
            try
            {
                foreach (var task in tasksCollection.GetConsumingEnumerable())
                {
                    TryExecuteTask(task);
                }
            }
            catch (Exception) { }
            finally
            {
                gil.Dispose();
                PythonEngine.EndAllowThreads(ptr);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            tasksCollection.CompleteAdding();
            tasksCollection.Dispose();
            if (mainThread != null)
            {
                GC.SuppressFinalize(mainThread);
            }
        }
    }
}
