﻿using Microsoft.Extensions.Logging;
using Python.Included;
using Python.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.PythonEnvironment
{
    public class PythonSetup
    {
        private static int _IsInit;
        private static PyEnvironment _Environment;
        private static readonly object _SyncRoot = new();
        public static PyEnvironment CurrentEnvironment
        {
            get => _Environment;
        }
        /// <summary>
        /// 是否初始化
        /// </summary>
        public static bool IsInitialization
        {
            get
            {
                SpinWait wait = new SpinWait();
                int old = _IsInit;
                while (Interlocked.CompareExchange(ref _IsInit, old, old) != old)
                {
                    wait.SpinOnce();
                    old = _IsInit;
                }
                return old == 1;
            }
            private set
            {
                int v = value ? 1 : 0;
                SpinWait wait = new SpinWait();
                int old = _IsInit;
                while (Interlocked.CompareExchange(ref _IsInit, v, old) != old)
                {
                    wait.SpinOnce();
                    old = _IsInit;
                }
            }
        }
        /// <summary>
        /// Python根目录
        /// </summary>
        public static string PythonRootPath { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public static async Task Setup(ILoggerFactory factory)
        {
            if (IsInitialization) return;
            if (!string.IsNullOrEmpty(PythonRootPath))
            {
                if (!Directory.Exists(PythonRootPath)) Directory.CreateDirectory(PythonRootPath);
                Installer.InstallDirectory = PythonRootPath;
                Installer.InstallPath = Path.GetFullPath("..", PythonRootPath);
            }
            await Installer.SetupPython();
            PythonEngine.Initialize();
            lock (_SyncRoot)
            {
                _Environment ??= new PyEnvironment(factory);
                _Environment.Init();
            }
            IsInitialization = true;
        }
    }
}
