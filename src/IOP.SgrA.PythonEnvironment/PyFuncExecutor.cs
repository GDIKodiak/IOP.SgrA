﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.PythonEnvironment
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PyFuncExecutor<T>
    {
        internal PyFuncExecutorAwaiter<T> awaiter;
        internal Func<T> Func;
        internal Action Continuation;
        internal T _Result;
        internal bool _IsCompleted;
        internal PyFuncExecutor(Func<T> func)
        {
            func ??= () => default;
            Func = func;
            awaiter = new PyFuncExecutorAwaiter<T>(this);
        }
        /// <summary>
        /// 
        /// </summary>
        public bool IsCompleted => _IsCompleted;
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public T GetResult() => awaiter.GetResult();
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public PyFuncExecutorAwaiter<T> GetAwaiter() 
        {
            return awaiter;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public struct PyFuncExecutorAwaiter<T> : INotifyCompletion
    {
        private PyFuncExecutor<T> _Owner;
        /// <summary>
        /// 
        /// </summary>
        public bool IsCompleted 
        {
            get => _Owner._IsCompleted; 
            internal set => _Owner._IsCompleted = value;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public T GetResult() => _Owner._Result;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Owner"></param>
        public PyFuncExecutorAwaiter(PyFuncExecutor<T> Owner)
        {
            _Owner = Owner;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        internal void MoveToComplete(T value)
        {
            IsCompleted = true;
            _Owner._Result = value;
            OnCompleted(_Owner.Continuation);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="continuation"></param>
        public void OnCompleted(Action continuation)
        {
            if(IsCompleted)
            {
                Action c = continuation;
                ThreadPool.QueueUserWorkItem((state) =>
                {
                    c?.Invoke();
                });
            }
            else { _Owner.Continuation += continuation; }
        }
    }
}
