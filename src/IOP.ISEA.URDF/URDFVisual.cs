﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.URDF
{
    /// <summary>
    /// 可视化节点
    /// </summary>
    public class URDFVisual
    {
        /// <summary>
        /// 
        /// </summary>
        public string? Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Matrix<double>? Origin {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public IURDFGeometry Geometry { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public IURDFMaterial? Material { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="geometry"></param>
        public URDFVisual(IURDFGeometry geometry)
        {
            Geometry = geometry;
        }
    }
}
