﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.URDF
{
    /// <summary>
    /// 
    /// </summary>
    public class URDFJoint
    {
        private static VectorBuilder<double> V = Vector<double>.Build;
        /// <summary>
        /// 名称
        /// </summary>
        public string Name {  get; set; }
        /// <summary>
        /// 父级连杆
        /// </summary>
        public string ParentLink { get; set; }
        /// <summary>
        /// 子级连杆
        /// </summary>
        public string ChildLink { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        public JointType Type { get; set; }
        /// <summary>
        /// 置换矩阵
        /// </summary>
        public Matrix<double>? Origin {  get; set; }
        /// <summary>
        /// 旋转轴
        /// </summary>
        public Vector<double> Axis { get; set; }
        /// <summary>
        /// 限制
        /// </summary>
        public URDFLimit? Limit { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="parentLink"></param>
        /// <param name="childLink"></param>
        /// <param name="type"></param>
        public URDFJoint(string name, string parentLink, string childLink, JointType type)
        {
            Name = name;
            ParentLink = parentLink;
            ChildLink = childLink;
            Type = type;
            Axis = V.DenseOfArray([1, 0, 0]);
        }
    }

    public enum JointType
    {
        /// <summary>
        /// 固定关节
        /// </summary>
        Fixed,
        /// <summary>
        /// 无限制旋转关节
        /// </summary>
        Continuous,
        /// <summary>
        /// 限制旋转关节
        /// </summary>
        Revolute,
        /// <summary>
        /// 滑动关节
        /// </summary>
        Prismatic,
        /// <summary>
        /// 平面关节
        /// </summary>
        Planer,
        /// <summary>
        /// 浮动关节
        /// </summary>
        Floating
    }
}
