﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.URDF
{
    /// <summary>
    /// 限制
    /// </summary>
    public class URDFLimit
    {
        public double Lower {  get; set; }

        public double Upper { get; set; }

        public double Effort {  get; set; }

        public double Velocity {  get; set; }
    }
}
