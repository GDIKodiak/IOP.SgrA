﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.URDF
{
    /// <summary>
    /// 惯性坐标系
    /// </summary>
    public class URDFInertial
    {
        private static MatrixBuilder<double> M = Matrix<double>.Build;
        /// <summary>
        /// 转置矩阵
        /// 4x4
        /// </summary>
        public Matrix<double>? Origin { get; set; }
        /// <summary>
        /// 惯性矩阵
        /// 3x3
        /// </summary>
        public Matrix<double> Inertia { get; set; }
        /// <summary>
        /// 质量
        /// </summary>
        public double Mass { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public URDFInertial()
        {
            Inertia = M.DenseIdentity(3, 3);
        }
    }
}
