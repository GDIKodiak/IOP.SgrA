﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.URDF
{
    /// <summary>
    /// 
    /// </summary>
    public interface IURDFGeometry
    {
        /// <summary>
        /// 
        /// </summary>
        public GeometryType Type { get; }
    }

    public enum GeometryType
    {
        None,
        Box,
        Cylinder,
        Sphere,
        Mesh
    }
}
