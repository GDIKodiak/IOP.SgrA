﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.URDF
{
    public class URDFColorMaterial : IURDFMaterial
    {
        /// <summary>
        /// 
        /// </summary>
        public string? Name {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public MaterialType Type => MaterialType.Color;
        /// <summary>
        /// 
        /// </summary>
        public double R {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public double G { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public double B { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public double A { get; set; } = 1.0;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="color"></param>
        /// <param name="name"></param>
        public URDFColorMaterial(string color, string name = "")
        {
            Name = name;
            if(!string.IsNullOrEmpty(color))
            {
                string[] values = color.Split(' ');
                if (values.Length > 0 && double.TryParse(values[0], out double r)) R = r;
                if (values.Length > 1 && double.TryParse(values[1], out double g)) G = g;
                if (values.Length > 2 && double.TryParse(values[2], out double b)) B = b;
                if (values.Length > 3 && double.TryParse(values[3], out double a)) A = a;
            }
        }
    }
}
