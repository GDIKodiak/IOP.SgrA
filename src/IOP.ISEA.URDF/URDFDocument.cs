﻿using IOP.SgrA.Kinematics;
using MathNet.Numerics.Distributions;
using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace IOP.ISEA.URDF
{
    public class URDFDocument
    {
        public const string ROOT = "robot";
        public const string LINK = "link";
        public const string NAME = "name";
        public const string INERTIAL = "inertial";
        public const string INERTIA = "inertia";
        public const string MASS = "mass";
        public const string VALUE = "value";
        public const string ORIGIN = "origin";
        public const string VISUAL = "visual";
        public const string GEOMETRY = "geometry";
        public const string MESH = "mesh";
        public const string FILENAME = "filename";
        public const string MATERIAL = "material";
        public const string COLOR = "color";
        public const string RGBA = "rgba";
        public const string COLLISION = "collision";
        public const string JOINT = "joint";
        public const string TYPE = "type";
        public const string FIXED = "fixed";
        public const string CONTINUOUS = "continuous";
        public const string REVOLUTE = "revolute";
        public const string PRISMATIC = "prismatic";
        public const string PLANER = "planer";
        public const string FLOATING = "floating";
        public const string PARENT = "parent";
        public const string CHILD = "child";
        public const string AXIS = "axis";
        public const string LIMIT = "limit";
        public const string LOWER = "lower";
        public const string UPPER = "upper";
        public const string EFFORT = "effort";
        public const string VELOCITY = "velocity";

        private static MatrixBuilder<double> M = Matrix<double>.Build;
        private static VectorBuilder<double> V = Vector<double>.Build;

        /// <summary>
        /// 名称
        /// </summary>
        public string? Name {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string FilePath { get; private set; } = "";
        /// <summary>
        /// 有序轴列表
        /// </summary>
        public readonly List<URDFJoint> Joints = new List<URDFJoint>();
        /// <summary>
        /// 链接列表
        /// </summary>
        public readonly Dictionary<string, URDFLink> Links = new Dictionary<string, URDFLink>();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static URDFDocument Parse(Stream stream, string filePath = "")
        {
            XmlReader reader = XmlReader.Create(stream);
            XmlDocument document = new XmlDocument();
            document.Load(reader);
            return Parse(document, filePath);
        }
        public static URDFDocument Parse(XmlDocument xmlDocument, string filePath = "")
        {
            URDFDocument document = new URDFDocument();
            document.FilePath = filePath;
            var roots = xmlDocument.GetElementsByTagName(ROOT);
            if (roots == null || roots.Count <= 0) throw new XmlException("cannot found tag by name robot");
            var root = roots[0]! as XmlElement ?? throw new XmlException("cannot found tag by name robot");
            var name = root.GetAttribute(NAME);
            if (!string.IsNullOrEmpty(name)) document.Name = name;
            else document.Name = "";
            var links = root.GetElementsByTagName(LINK);
            if(links != null)
            {
                for(int i = 0; i < links.Count; i++)
                {
                    if(links[i] is XmlElement child)
                    {
                        var link = ParseLink(document, child);
                        document.Links.TryAdd(link.Name, link);
                    }
                }
            }
            var joints = root.GetElementsByTagName(JOINT);
            if(joints != null)
            {
                for(int i = 0; i < joints.Count; i++)
                {
                    if(joints[i] is XmlElement child)
                    {
                        var joint = ParseJoint(child);
                        document.Joints.Add(joint);
                    }
                }
            }
            return document;
        }
        /// <summary>
        /// 从文件加载
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        /// <exception cref="FileNotFoundException"></exception>
        /// <exception cref="FileLoadException"></exception>
        public static URDFDocument ParseFromFile(string fileName)
        {
            if (string.IsNullOrEmpty(fileName) || !File.Exists(fileName)) throw new FileNotFoundException($"Cannot found file for target path {fileName}", fileName);
            FileInfo fileInfo = new FileInfo(fileName);
            if (!fileInfo.Extension.Equals(".urdf", StringComparison.CurrentCultureIgnoreCase)) throw new FileLoadException($"target file {fileName} is not URDF file");
            using FileStream stream = fileInfo.Open(FileMode.Open, FileAccess.Read, FileShare.Read);
            return Parse(stream, fileInfo.FullName);
        }
        /// <summary>
        /// 从字符串加载
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static URDFDocument ParseFromStringData(string data)
        {
            XmlReader reader = XmlReader.Create(new StringReader(data));
            XmlDocument document = new XmlDocument();
            document.Load(reader);
            return Parse(document);
        }

        /// <summary>
        /// 从当前URDF文件创建机器人模型
        /// </summary>
        /// <returns></returns>
        public RobotSystem GenerateSystem()
        {
            Matrix<double> tempT = M.DenseIdentity(4, 4);
            Matrix<double> tempM = M.DenseIdentity(4, 4);
            Vector<double> tempp = V.Dense(3);
            Vector<double> tempw = V.Dense(3);
            RobotSystem system = new RobotSystem();
            if(!string.IsNullOrEmpty(Name)) system.Name = Name;
            foreach (var joint in Joints)
            {
                string pre = joint.ParentLink;
                if (!Links.TryGetValue(pre, out URDFLink? ulink)) throw new InvalidDataException($"cannot found link from name {pre}");
                if (!system.Links.TryGetValue(pre, out Link? splink))
                {
                    Link link = new Link(ulink!.Name);
                    link.T = tempT;
                    link.M = tempM;
                    system.Links.Add(pre, link);
                    splink = link;
                }
                else
                {
                    tempT = splink.T;
                    tempM = splink.M;
                }
                Screw s;
                if (joint.Origin != null)
                {
                    tempT = M.InverseSE3(joint.Origin);
                    tempM *= joint.Origin;
                    tempp = (tempM * V.Dense([0, 0, 0, 1])).SubVector(0, 3);
                    tempw = tempM.SubMatrix(0, 3, 0, 3) * joint.Axis;
                    s = new Screw(tempw, M.Vector2so3(-tempw) * tempp);
                }
                else s = new Screw(joint.Axis, V.Dense([0, 0, 0]));
                string next = joint.ChildLink;
                if (!Links.TryGetValue(next, out URDFLink? nlink)) throw new InvalidDataException($"cannot found link from name {next}");
                if(!system.Links.TryGetValue(next, out Link? nplink))
                {
                    Link link = new Link(nlink!.Name);
                    link.T = tempT;
                    link.M = tempM;
                    system.Links.Add(next, link);
                    nplink = link;
                }
                Joint j = new Joint(joint.Name, joint.Axis, s, splink, nplink);
                var limit = joint.Limit;
                if(limit != null)
                {
                    j.MaxTheta = limit.Upper;
                    j.MinTheta = limit.Lower;
                }
                else
                {
                    j.MaxTheta = double.MaxValue;
                    j.MinTheta = double.MinValue;
                }
                splink.NextJoint = j;
                nplink.PreviewJoint = j;
                system.Joints.TryAdd(j.Name, j);
            }
            foreach(var link in system.Links)
            {
                var l = link.Value;
                if(l.NextJoint == null)
                {
                    system.EndLinks.TryAdd(link.Key, l);
                }
            }
            return system;
        }

        /// <summary>
        /// 获取连杆的可视化属性
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, URDFVisual?> GetLinksVisual()
        {
            return new Dictionary<string, URDFVisual?>(Links.Select((pair) =>
            {
                if (pair.Value.Visual != null)
                {
                    return new KeyValuePair<string, URDFVisual?>(pair.Key, pair.Value.Visual);
                }
                return new KeyValuePair<string, URDFVisual?>(pair.Key, null);
            }));
        }

        /// <summary>
        /// 解析连杆节点
        /// </summary>
        /// <param name="linkNode"></param>
        /// <returns></returns>
        private static URDFLink ParseLink(URDFDocument document, XmlElement linkNode)
        {
            URDFLink link = new URDFLink();
            var name = linkNode.GetAttribute(NAME);
            if (string.IsNullOrEmpty(name)) name = Guid.NewGuid().ToString("N");
            link.Name = name;
            link.Inertial = GetInertial(linkNode);
            link.Visual = GetVisual(document, linkNode);
            link.Collision = GetCollision(document, linkNode);
            return link;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="jointNode"></param>
        /// <returns></returns>
        /// <exception cref="InvalidDataException"></exception>
        private static URDFJoint ParseJoint(XmlElement jointNode)
        {
            var name = jointNode.GetAttribute(NAME);
            if (string.IsNullOrEmpty(name)) throw new InvalidDataException($"{JOINT} node must be have attribute {NAME}");
            var type = jointNode.GetAttribute(TYPE);
            if (string.IsNullOrEmpty(type)) throw new InvalidDataException($"{JOINT} node must be have attribute {TYPE}");
            JointType jointType = GetJointType(type);
            var parent = jointNode[PARENT] ?? throw new InvalidDataException($"{JOINT} node must be have child Node {PARENT}");
            var parentLink = parent.GetAttribute(LINK);
            if (string.IsNullOrEmpty(parentLink)) throw new InvalidDataException($"{PARENT} node must be have attribute {LINK}");
            var child = jointNode[CHILD] ?? throw new InvalidDataException($"{JOINT} node must be have child Node {CHILD}");
            var childLink = child.GetAttribute(LINK);
            if (string.IsNullOrEmpty(childLink)) throw new InvalidDataException($"{CHILD} node must be have attribute {LINK}");
            URDFJoint joint = new URDFJoint(name, parentLink, childLink, jointType);
            joint.Origin = GetOriginTransform(jointNode);
            var v = GetAxisVector(jointNode);
            if(v != null) joint.Axis = v;
            joint.Limit = GetLimit(jointNode);
            return joint;
        }
        /// <summary>
        /// 获取惯性坐标系
        /// </summary>
        /// <param name="rootNode"></param>
        /// <returns></returns>
        private static URDFInertial? GetInertial(XmlElement rootNode)
        {
            var inertialNode = rootNode[INERTIAL];
            if (inertialNode != null) 
            {
                URDFInertial inertial = new URDFInertial();
                inertial.Inertia = GetInertia(inertialNode)!;
                var massNode = inertialNode[MASS];
                if (massNode != null)
                {
                    var mass = massNode.GetAttribute(VALUE);
                    if(double.TryParse(mass, out double massV)) inertial.Mass = massV;
                }
                inertial.Origin = GetOriginTransform(inertialNode);
                return inertial;
            }
            return null;
        }
        /// <summary>
        /// 获取惯性矩阵
        /// </summary>
        /// <param name="inertial"></param>
        /// <returns></returns>
        private static Matrix<double> GetInertia(XmlElement inertial)
        {
            var inertia = inertial[INERTIA];
            if (inertia != null)
            {
                double.TryParse(inertia.GetAttribute("ixx"), out double ixx);
                double.TryParse(inertia.GetAttribute("ixy"), out double ixy);
                double.TryParse(inertia.GetAttribute("ixz"), out double ixz);
                double.TryParse(inertia.GetAttribute("iyy"), out double iyy);
                double.TryParse(inertia.GetAttribute("iyz"), out double iyz);
                double.TryParse(inertia.GetAttribute("izz"), out double izz);
                Matrix<double> result = M.DenseOfArray(new double[,] { { ixx, ixy, ixz }, { ixy, iyy, iyz }, { ixz, iyz, izz } });
                return result;
            }
            throw new InvalidDataException($"{INERTIAL} node must be have {INERTIA} node");
        }
        /// <summary>
        /// 获取父节点中关于子节点的原始转换矩阵
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        private static Matrix<double>? GetOriginTransform(XmlElement parent) 
        {
            var origin = parent[ORIGIN];
            if(origin != null)
            {
                string xyz = origin.GetAttribute("xyz");
                string rpy = origin.GetAttribute("rpy");
                if (!string.IsNullOrEmpty(xyz) && !string.IsNullOrEmpty(rpy))
                {
                    string[] ps = xyz.Split(" ");
                    string[] rs = rpy.Split(" ");
                    if (ps.Length == 3 && rs.Length == 3)
                    {
                        double.TryParse(ps[0], out double x);
                        double.TryParse(ps[1], out double y);
                        double.TryParse(ps[2], out double z);
                        double.TryParse(rs[0], out double r);
                        double.TryParse(rs[1], out double p);
                        double.TryParse(rs[2], out double yy);
                        Matrix<double> rotation = M.CreateRotationFromRPY(r, p, yy);
                        Matrix<double> translate = M.CreateTranslation(x, y, z);
                        Matrix<double> result = translate * rotation;
                        return result;
                    }
                }
            }
            return null;
        }
        /// <summary>
        /// 获取轴向向量
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        private static Vector<double>? GetAxisVector(XmlElement parent)
        {
            var axis = parent[AXIS];
            if (axis != null)
            {
                string xyz = axis.GetAttribute("xyz");
                if (!string.IsNullOrEmpty(xyz))
                {
                    string[] ps = xyz.Split(" ");
                    if(ps.Length == 3)
                    {
                        double.TryParse(ps[0], out double x);
                        double.TryParse(ps[1], out double y);
                        double.TryParse(ps[2], out double z);
                        Vector<double> vector = V.DenseOfArray([x, y, z]);
                        return vector;
                    }
                }
            }
            return null;
        }
        /// <summary>
        /// 获取可视化节点信息
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        private static URDFVisual? GetVisual(URDFDocument document, XmlElement parent)
        {
            var visual = parent[VISUAL];
            if(visual != null)
            {
                string basePath = document.FilePath;
                var geometryNode = visual[GEOMETRY] ?? throw new InvalidDataException($"{VISUAL} node must be have child node {GEOMETRY}");
                IURDFGeometry geometry = GetGeometry(basePath, geometryNode);
                URDFVisual result = new URDFVisual(geometry);
                var origin = visual[ORIGIN];
                if(origin != null)
                {
                    Matrix<double> matrix = GetOriginTransform(visual)!;
                    result.Origin = matrix;
                }
                var materialNode = visual[MATERIAL];
                if(materialNode != null) 
                {
                    IURDFMaterial material = GetMaterial(materialNode);
                    result.Material = material;
                }
                var name = visual.GetAttribute(NAME);
                if(!string.IsNullOrEmpty(name)) result.Name = name;
                return result;
            }
            return null;
        }
        /// <summary>
        /// 获取碰撞信息
        /// </summary>
        /// <param name="document"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        private static URDFCollision? GetCollision(URDFDocument document, XmlElement parent) 
        {
            var collisionNode = parent[COLLISION];
            if(collisionNode != null)
            {
                var geometryNode = collisionNode[GEOMETRY] ?? throw new InvalidDataException($"{COLLISION} node must be bave {GEOMETRY} node");
                IURDFGeometry geometry = GetGeometry(document.FilePath, geometryNode);
                URDFCollision collision = new URDFCollision(geometry);
                var name = collisionNode.GetAttribute(NAME);
                if(!string.IsNullOrEmpty(name)) collision.Name = name;
                collision.Origin = GetOriginTransform(collisionNode);
                collision.Inertial = GetInertial(collisionNode);
                return collision;
            }
            return null;
        }
        /// <summary>
        /// 获取限制
        /// </summary>
        /// <param name="document"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        private static URDFLimit? GetLimit(XmlElement parent) 
        {
            var limitNode = parent[LIMIT];
            if(limitNode != null)
            {
                URDFLimit limit = new URDFLimit();
                var lower = limitNode.GetAttribute(LOWER);
                if (!string.IsNullOrEmpty(lower) && double.TryParse(lower, out double l)) limit.Lower = l; 
                var upper = limitNode.GetAttribute(UPPER);
                if (!string.IsNullOrEmpty(upper) && double.TryParse(upper, out double u)) limit.Upper = u;
                var effort = limitNode.GetAttribute(EFFORT);
                if (!string.IsNullOrEmpty(effort) && double.TryParse(effort, out double e)) limit.Effort = e;
                var velocity = limitNode.GetAttribute(VELOCITY);
                if(!string.IsNullOrEmpty(velocity) && double.TryParse(velocity, out double v)) limit.Velocity = v;
                return limit;
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="basePath"></param>
        /// <param name="geometry"></param>
        /// <returns></returns>
        private static IURDFGeometry GetGeometry(string basePath, XmlElement geometry)
        {
            foreach (var element in geometry) 
            { 
                if(element is XmlElement local)
                {
                    if(local.LocalName == MESH)
                    {
                        string fileName = local.GetAttribute(FILENAME) ?? throw new InvalidDataException("Mesh node must be have attribute filename");
                        URDFMesh mesh = new URDFMesh(basePath, fileName);
                        return mesh;
                    }
                }
            }
            throw new NotSupportedException("not supported geometry node");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="materialNode"></param>
        /// <returns></returns>
        /// <exception cref="InvalidDataException"></exception>
        /// <exception cref="NotSupportedException"></exception>
        private static IURDFMaterial GetMaterial(XmlElement materialNode)
        {
            foreach (var element in materialNode)
            {
                if(element is XmlElement local)
                {
                    if(local.LocalName == COLOR)
                    {
                        string color = local.GetAttribute(RGBA);
                        if (string.IsNullOrEmpty(color)) throw new InvalidDataException($"{COLOR} node must be have attribute {RGBA}");
                        URDFColorMaterial material = new URDFColorMaterial(color);
                        var name = local.GetAttribute(NAME);
                        if(!string.IsNullOrEmpty(name)) material.Name = name;
                        return material;
                    }
                }
            }
            throw new NotSupportedException("not supported material node");
        }
        /// <summary>
        /// 获取轴状态
        /// </summary>
        /// <param name="jointType"></param>
        /// <returns></returns>
        /// <exception cref="NotSupportedException"></exception>
        private static JointType GetJointType(string jointType) 
        {
            return jointType switch
            {
                FIXED => JointType.Fixed,
                CONTINUOUS => JointType.Continuous,
                REVOLUTE => JointType.Revolute,
                PRISMATIC => JointType.Prismatic,
                PLANER => JointType.Planer,
                FLOATING => JointType.Floating,
                _ => throw new NotSupportedException("Not supported joint type")
            };
        }
    }
}
