﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.URDF
{
    /// <summary>
    /// 
    /// </summary>
    public interface IURDFMaterial
    {
        /// <summary>
        /// 
        /// </summary>
        string? Name { get; }
        /// <summary>
        /// 
        /// </summary>
        MaterialType Type { get; }
    }

    public enum MaterialType
    {
        None,
        Color,
        Texture
    }
}
