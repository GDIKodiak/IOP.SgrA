﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.URDF
{
    /// <summary>
    /// 
    /// </summary>
    public class URDFCollision
    {
        /// <summary>
        /// 
        /// </summary>
        public string? Name {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Matrix<double>? Origin {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public IURDFGeometry Geometry { get; set; }
        /// <summary>
        /// 惯性坐标系
        /// </summary>
        public URDFInertial? Inertial { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="geometry"></param>
        public URDFCollision(IURDFGeometry geometry)
        {
            Geometry = geometry;
        }
    }
}
