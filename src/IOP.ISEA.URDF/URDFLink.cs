﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.URDF
{
    /// <summary>
    /// 链接
    /// </summary>
    public class URDFLink
    {
        private static MatrixBuilder<double> M = Matrix<double>.Build;
        /// <summary>
        /// 名称
        /// </summary>
        public string Name {  get; set; }
        /// <summary>
        /// 惯性坐标系
        /// </summary>
        public URDFInertial? Inertial { get; set; }
        /// <summary>
        /// 可视化
        /// </summary>
        public URDFVisual? Visual { get; set; }
        /// <summary>
        /// 碰撞节点
        /// </summary>
        public URDFCollision? Collision { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public URDFLink()
        {
            Name = string.Empty;
        }
    }
}
