﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.URDF
{
    /// <summary>
    /// 
    /// </summary>
    public class URDFMesh : IURDFGeometry
    {
        /// <summary>
        /// 
        /// </summary>
        public const string PACKAGEPREFIX = "package://";
        /// <summary>
        /// 
        /// </summary>
        public const string PACKAGEFILE = "package.xml";
        /// <summary>
        /// 
        /// </summary>
        public GeometryType Type => GeometryType.Mesh;
        /// <summary>
        /// 
        /// </summary>
        public string Filename { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public URDFMesh(string basePath, string filename)
        {
            if(string.IsNullOrEmpty(filename)) throw new ArgumentNullException(nameof(filename));
            filename = ParseFilename(basePath, filename);
            Filename = filename;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="basePath"></param>
        /// <returns></returns>
        private string ParseFilename(string basePath, string filename)
        {
            if (filename.Contains(PACKAGEPREFIX))
            {
                DirectoryInfo packageDirectory = SearchPackageDirectory(basePath);
                string packagePath = packageDirectory.FullName;
                filename = filename.Replace(PACKAGEPREFIX, "");
                string[] paths = filename.Split("/");
                if (paths.Length <= 1) throw new FileNotFoundException("Invalid package path");
                string localPath = string.Join(Path.DirectorySeparatorChar, paths[1..]);
                string meshPath = Path.Combine(packagePath, localPath);
                return meshPath;
            }
            else
            {
                if (!File.Exists(filename)) throw new FileNotFoundException($"cannot found mesh path with {filename}");
                return filename;
            }
        }

        private DirectoryInfo SearchPackageDirectory(string basePath)
        {
            FileInfo baseFile = new FileInfo(basePath);
            if (!baseFile.Exists) throw new FileNotFoundException($"Cannot found file with path {basePath}");
            var info = baseFile.Directory ?? throw new DirectoryNotFoundException($"Cannot found Directory with path {basePath}");
            foreach (FileInfo file in info.GetFiles()) 
            {
                if (file.Name == PACKAGEFILE && file.Extension.Equals(".xml", StringComparison.CurrentCultureIgnoreCase)) 
                {
                    return info;
                }
            }
            var parent = Directory.GetParent(info.FullName);
            while (parent != null)
            {
                foreach(var child in parent.GetFiles())
                {
                    if (child.Name == PACKAGEFILE)
                    {
                        return parent;
                    }
                }
            }
            throw new FileNotFoundException($"Cannot found {PACKAGEFILE}.xml");
        }
    }
}
