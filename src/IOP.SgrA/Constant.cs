﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 
    /// </summary>
    public static class Constant
    {
        /// <summary>
        /// 
        /// </summary>
        public const string BNTEXA = ".bntexa";
        /// <summary>
        /// 
        /// </summary>
        public const string BNHDT = ".bnhdt";
        /// <summary>
        /// 
        /// </summary>
        public const string BN3DTEX = ".bn3dtex";
        /// <summary>
        /// 
        /// </summary>
        public const string BNTEX = ".bntex";
        /// <summary>
        /// 
        /// </summary>
        public const string JEPG = ".jpg";
        /// <summary>
        /// 
        /// </summary>
        public const string BMP = ".bmp";
        /// <summary>
        /// 
        /// </summary>
        public const string PNG = ".png";
        /// <summary>
        /// 
        /// </summary>
        public const string TIFF = ".tif";
        /// <summary>
        /// 
        /// </summary>
        public const string DDS = ".dds";
        /// <summary>
        /// 深度拷贝
        /// </summary>
        public const string DEEPCOPY = "DeepCopy";
    }
}
