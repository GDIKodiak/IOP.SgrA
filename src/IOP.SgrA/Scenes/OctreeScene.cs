﻿using System;
using System.Buffers;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA
{
    /// <summary>
    /// 使用八叉树进行管理的场景
    /// </summary>
    public class OctreeScene : Scene
    {
        /// <summary>
        /// 场景中心
        /// </summary>
        public override Vector3 Center => SceneTree == null? Vector3.Zero : SceneTree.Center;

        /// <summary>
        /// 场景树
        /// </summary>
        protected Octree<IRenderObject> SceneTree { get; set; }
        /// <summary>
        /// 插入请求队列
        /// </summary>
        protected ConcurrentQueue<InsertRequest> InsertRequests { get; set; } = new ConcurrentQueue<InsertRequest>();
        /// <summary>
        /// 移动请求队列
        /// </summary>
        protected ConcurrentQueue<Vector3> MoveRequests { get; set; } = new ConcurrentQueue<Vector3>();
        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="center"></param>
        /// <param name="sceneWidth"></param>
        /// <param name="treeDepth"></param>
        public virtual void Initialization(Vector3 center, float sceneWidth, uint treeDepth)
        {
            SceneTree = new Octree<IRenderObject>(sceneWidth, center, treeDepth);
            base.Initialization();
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public override void Initialization()
        {
            SceneTree = new Octree<IRenderObject>(10240, Vector3.Zero, 10);
            base.Initialization();
        }
        /// <summary>
        /// 插入渲染对象
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="action"></param>
        public override void Insert(IRenderObject obj, Action<bool, IRenderObject> action = null)
        {
            InsertRequest request = new InsertRequest(obj, action);
            InsertRequests.Enqueue(request);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="lastStamp"></param>
        public override void ExecuteComponentSystem(TimeSpan lastStamp)
        {
            base.ExecuteComponentSystem(lastStamp);
            MaintenanceSceneTree();
        }
        /// <summary>
        /// 移动中心
        /// </summary>
        /// <param name="newCenter"></param>
        public override void MoveCenter(Vector3 newCenter) => MoveRequests.Enqueue(newCenter);

        /// <summary>
        /// 维护场景树
        /// </summary>
        protected virtual void MaintenanceSceneTree()
        {
            IRenderObject[] cache = ArrayPool<IRenderObject>.Shared.Rent(SceneTree.Count);
            Span<IRenderObject> objs = cache; int length;
            if (!MoveRequests.IsEmpty)
            {
                Vector3 last;
                while (MoveRequests.TryDequeue(out last)) { }
                SceneTree.MoveCenter(last, in objs, out length);
                foreach(var item in LoadingFuncs)
                {
                    item?.Invoke(this, last);
                }
            }
            else SceneTree.Maintenance(in objs, out length);
            if (length > 0 && UnloadingFuncs.Count > 0)
            {
                IRenderObject[] outside = objs[..length].ToArray();
                foreach (var item in UnloadingFuncs)
                {
                    item?.Invoke(this, outside);
                }
            }
            while(InsertRequests.TryDequeue(out var req))
            {
                bool result = false;
                if (req.Object != null)
                    result = SceneTree.TryInsert(req.Object);
                req.InsertResult?.Invoke(result, req.Object);
            }
            ArrayPool<IRenderObject>.Shared.Return(cache);
        }
    }

    /// <summary>
    /// 插入请求
    /// </summary>
    public struct InsertRequest
    {
        /// <summary>
        /// 渲染对象
        /// </summary>
        public IRenderObject Object;
        /// <summary>
        /// 插入结果回执
        /// </summary>
        public Action<bool, IRenderObject> InsertResult;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="action"></param>
        public InsertRequest(IRenderObject obj, Action<bool, IRenderObject> action)
        {
            Object = obj;
            InsertResult = action;
        }
    }
}
