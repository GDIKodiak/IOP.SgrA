﻿using IOP.Extension.DependencyInjection;
using IOP.SgrA.ECS;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using static System.Formats.Asn1.AsnWriter;

namespace IOP.SgrA
{
    /// <summary>
    /// 基础场景
    /// </summary>
    public class Scene : IScene
    {
        /// <summary>
        /// 日志
        /// </summary>
        [Autowired]
        public ILogger<Scene> Logger { get; protected internal set; }
        /// <summary>
        /// 服务
        /// </summary>
        [Autowired]
        public IServiceProvider ServiceProvider { get; protected internal set; }
        /// <summary>
        /// 模块服务
        /// </summary>
        [Autowired]
        public IModuleService ModuleService { get; protected internal set; }
        /// <summary>
        /// 光源环境
        /// </summary>
        public ILightEnvironment LightEnvironment { get; set; }
        /// <summary>
        /// 渲染区域
        /// </summary>
        public Area RenderArea { get; set; }
        /// <summary>
        /// 场景中心坐标
        /// </summary>
        public virtual Vector3 Center { get; }
        /// <summary>
        /// 场景世界坐标系矩阵
        /// </summary>
        public Matrix4x4 WorldMatrix { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        public bool IsEnabled { get; set; }
        /// <summary>
        /// 是否加载
        /// </summary>
        public bool IsLoading { get; set; }
        /// <summary>
        /// 场景名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 是否完成初始化
        /// </summary>
        public bool IsInitialization { get; protected set; } = false;
        /// <summary>
        /// 是否销毁资源
        /// </summary>
        public bool IsDispose { get => _IsDispose; protected set => _IsDispose = value; }
        /// <summary>
        /// 渲染组
        /// </summary>
        public IRenderGroup RenderGroup { get; set; }
        /// <summary>
        /// 上下文管理器
        /// </summary>
        public IContextManager ContextManager { get; set; }
        /// <summary>
        /// 事件总线
        /// </summary>
        public ISystemEventBus EventBus { get; set; }
        /// <summary>
        /// 输入
        /// </summary>
        public IInputStateController Input { get; set; }
        /// <summary>
        /// 组件系统
        /// </summary>
        public readonly ConcurrentBag<IComponentSystem> ComponentSystems = new();
        /// <summary>
        /// 信号组
        /// </summary>
        protected readonly CountdownEvent SystemCountdownEvent = new(64);
        /// <summary>
        /// 
        /// </summary>
        protected readonly ConcurrentQueue<Action> PostQueue = new ConcurrentQueue<Action>();
        /// <summary>
        /// 执行超时时间
        /// </summary>
        protected int ExecuteOutTime { get; set; } = 5000;
        /// <summary>
        /// 加载函数列表
        /// </summary>
        protected List<Func<IScene, Vector3, Task>> LoadingFuncs { get; set; } = new List<Func<IScene, Vector3, Task>>();
        /// <summary>
        /// 卸载函数列表
        /// </summary>
        protected List<Func<IScene, IEnumerable<IRenderObject>, Task>> UnloadingFuncs { get; set; } = new List<Func<IScene, IEnumerable<IRenderObject>, Task>>();
        /// <summary>
        /// 是否销毁
        /// </summary>
        protected bool _IsDispose;
        /// <summary>
        /// 场景模块
        /// </summary>
        protected readonly Dictionary<ModulePriority, List<IGraphicsModule>> SceneModules = new Dictionary<ModulePriority, List<IGraphicsModule>>()
        {
            { ModulePriority.Core, new List<IGraphicsModule>() },
            { ModulePriority.ISEA, new List<IGraphicsModule>() },
            { ModulePriority.RenderPass, new List<IGraphicsModule>() },
            { ModulePriority.RenderGroup, new List<IGraphicsModule>() },
            { ModulePriority.Assets, new List<IGraphicsModule>() },
            { ModulePriority.Priority5, new List<IGraphicsModule>() },
            { ModulePriority.Priority6, new List<IGraphicsModule>() },
            { ModulePriority.Priority7, new List<IGraphicsModule>() },
            { ModulePriority.None, new List<IGraphicsModule>() }
        };
        /// <summary>
        /// 
        /// </summary>
        protected readonly Dictionary<Type, IGraphicsModule> ModulesDic = new Dictionary<Type, IGraphicsModule>();
        /// <summary>
        /// 
        /// </summary>
        protected List<SyncComponentSystem> BeforeComponentSystem { get; } = new(128);
        /// <summary>
        /// 
        /// </summary>
        protected List<SyncComponentSystem> AfterCompoentSystem { get; } = new(128);

        /// <summary>
        /// 
        /// </summary>
        public Scene()
        {
            Center = Vector3.Zero;
            WorldMatrix = Matrix4x4.Identity;
        }

        /// <summary>
        /// 渲染
        /// </summary>
        public virtual void Rendering(uint fIndex = 0)
        {
            if (!RenderGroup.IsEnable()) { PostQueue.Clear(); return; }
            LightEnvironment?.SyncLightBuffer();
            while (PostQueue.TryDequeue(out var post)) { post?.Invoke(); }
            RenderGroup?.GroupRendering(fIndex);
        }
        /// <summary>
        /// 初始化
        /// </summary>
        public virtual void Initialization()
        {
            if (IsInitialization) return;
            RenderGroup?.Initialization();
            ContextManager.RefreshUpdateQueue();
            IsInitialization = true;
        }
        /// <summary>
        /// 执行组件系统
        /// </summary>
        public virtual void ExecuteComponentSystem(TimeSpan lastStamp)
        {
            var length = ComponentSystems.Count;
            SystemCountdownEvent.Reset(length);
            foreach (var item in BeforeComponentSystem)
            {
                item.Update(lastStamp);
            }
            int i = 0;
            foreach (var item in ComponentSystems)
            {
                if (i == length) break;
                i++;
                if (!item.IsInit) item.Initialization();
                Task.Run(() =>
                {
                    try
                    {
                        item.Update(lastStamp);
                    }
                    catch (Exception e)
                    {
                        Logger.LogError(e.Message + "\r\n" + e.StackTrace);
                    }
                    finally
                    {
                        SystemCountdownEvent.Signal();
                    }
                });
            }
            SystemCountdownEvent.Wait();
            foreach (var item in AfterCompoentSystem)
            {
                item.Update(lastStamp);
            }
        }
        /// <summary>
        /// 插入渲染对象至场景管理
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="action"></param>
        public virtual void Insert(IRenderObject obj, Action<bool, IRenderObject> action = null) { }
        /// <summary>
        /// 移动场景中心
        /// </summary>
        /// <param name="newCenter"></param>
        public virtual void MoveCenter(Vector3 newCenter) { }
        /// <summary>
        /// 手动执行加载以触发加载函数
        /// </summary>
        public virtual void LoadResources()
        {
            foreach (var item in LoadingFuncs)
            {
                item?.Invoke(this, Center);
            }
        }
        /// <summary>
        /// 注册动态加载用函数
        /// </summary>
        /// <param name="func"></param>
        public virtual void RegistLoadingFunction(Func<IScene, Vector3, Task> func)
        {
            if (func == null) return;
            LoadingFuncs.Add(func);
        }
        /// <summary>
        /// 注册动态卸载函数
        /// </summary>
        /// <param name="func"></param>
        public virtual void RegistUnloadingFunction(Func<IScene, IEnumerable<IRenderObject>, Task> func)
        {
            if (func == null) return;
            UnloadingFuncs.Add(func);
        }
        /// <summary>
        /// 注册同步函数至队列
        /// </summary>
        /// <param name="action"></param>
        public virtual void Post(Action action) => PostQueue.Enqueue(action);

        /// <summary>
        /// 添加组件
        /// </summary>
        /// <param name="systems"></param>
        public virtual void AddComponentSystem(params Type[] systems)
        {
            PostQueue.Enqueue(() => AddComponentSystemSync(systems));
        }

        /// <summary>
        /// 添加场景模块
        /// </summary>
        /// <param name="types"></param>
        public virtual void AddSceneModules(params Type[] types)
        {
            if (types == null || !types.Any()) return;
            ModuleInfo[] infos = ModuleService.CreateModules(ModulesDic, types);
            foreach (var info in infos)
            {
                if (info.Module is ISceneModule sceneModule)
                {
                    sceneModule.Scene = this;
                    SceneModules[info.ModulePriority].Add(info.Module);
                    ModulesDic.TryAdd(info.Module.GetType(), info.Module);
                }
                else Logger.LogWarning($"The target vulkan module {info.Module.GetType().FullName} is not a scene module, please add interface {nameof(ISceneModule)} before add scene module");
            }
        }
        /// <summary>
        /// 实时加载场景模块
        /// </summary>
        /// <typeparam name="TModule"></typeparam>
        public virtual void LoadSceneModuleDynamic<TModule>()
            where TModule : ISceneModule, IGraphicsModule
        {
            var type = typeof(TModule);
            LoadSceneModuleDynamic(type);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        public virtual void LoadSceneModuleDynamic(Type type)
        {
            ModuleInfo[] infos = ModuleService.CreateModules(ModulesDic, type);
            foreach (var info in infos)
            {
                if (ModulesDic.TryGetValue(type, out _)) continue;
                if (info.Module is ISceneModule sceneModule)
                {
                    sceneModule.Scene = this;
                    SceneModules[info.ModulePriority].Add(info.Module);
                    ModulesDic.TryAdd(info.Module.GetType(), info.Module);
                    _ = info.Module.Load();
                }
                else Logger.LogWarning($"The target vulkan module {info.Module.GetType().FullName} is not a scene module, please add interface {nameof(ISceneModule)} before add scene module");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TModule"></typeparam>
        /// <param name="module"></param>
        /// <returns></returns>
        public virtual bool TryGetSceneModule<TModule>(out TModule module)
            where TModule : IGraphicsModule
        {
            var type = typeof(TModule);
            module = default;
            if (ModulesDic.TryGetValue(type, out var m))
            {
                if (m is TModule tm)
                {
                    module = tm;
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 加载场景模块
        /// </summary>
        /// <returns></returns>
        public virtual Task LoadSceneModules()
        {
            var countdownEvent = new CountdownEvent(64);
            foreach (var item in SceneModules)
            {
                if (item.Value.Any())
                {
                    var list = item.Value;
                    countdownEvent.Reset(list.Count);
                    for (int i = 0; i < list.Count; i++)
                    {
                        if (!list[i].IsLoaded)
                        {
                            list[i].ContextManager = ContextManager;
                            list[i].Load().ContinueWith((task) => countdownEvent.Signal());
                        }
                        else countdownEvent.Signal();
                    }
                    countdownEvent.Wait();
                }
            }
            IsEnabled = true;
            return Task.CompletedTask;
        }

        /// <summary>
        /// 销毁资源
        /// </summary>
        public virtual void Dispose()
        {
            if (!IsDispose)
            {
                IsDispose = true;
                ComponentSystems.Clear();
            }
        }

        private void AddComponentSystemSync(params Type[] systems)
        {
            if (systems == null || !systems.Any()) return;
            var systemT = typeof(IComponentSystem);
            foreach (var item in systems)
            {
                try
                {
                    if (item.GetInterfaces().Any(x => x == systemT))
                    {
                        object s = ServiceProvider.CreateAutowiredInstance(item);
                        if (s is not ComponentSystem cs) continue;
                        cs.Owner = this;
                        cs.ContextManager = ContextManager;
                        cs.Input = Input;
                        cs.EventBus = EventBus;
                        var methods = item.GetMethods();
                        foreach (var m in methods)
                        {
                            if (m.GetCustomAttributes(typeof(SubscribeEventAttribute), false).FirstOrDefault() is SubscribeEventAttribute attr)
                            {
                                EventBus.Subscribe(attr.Topic, this, m, attr.EventTrigger);
                            }
                        }
                        if (IsInitialization) cs.Initialization();
                        if (cs is SyncComponentSystem sync)
                        {
                            if (sync.ExecuteTrigger == SyncExecuteTrigger.BeforeExecuteSystem)
                            {
                                InsertSort(BeforeComponentSystem, sync);
                            }
                            else
                            {
                                InsertSort(AfterCompoentSystem, sync);
                            }
                        }
                        else ComponentSystems.Add(cs);
                    }
                }
                catch (Exception e)
                {
                    Logger?.LogError($"Add system {item.Name} failed with exception : {e.Message}");
                }
            }
        }

        private void InsertSort(List<SyncComponentSystem> list, SyncComponentSystem system)
        {
            if (list.Count <= 0)
            {
                list.Add(system);
                return;
            }
            int index = list.BinarySearch(system, new SyncComponentComponer());
            if (index < 0)
            {
                list.Insert(~index, system);
            }
            else list.Insert(index, system);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public struct SyncComponentComponer : IComparer<SyncComponentSystem>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public int Compare(SyncComponentSystem x, SyncComponentSystem y)
        {
            if (x.Priority < y.Priority) return -1;
            else if (x.Priority == y.Priority) return 0;
            else return 1;
        }
    }
}
