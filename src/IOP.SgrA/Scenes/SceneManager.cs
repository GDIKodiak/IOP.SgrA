﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using IOP.Extension.DependencyInjection;
using System.Threading;

namespace IOP.SgrA
{
    /// <summary>
    /// 场景管理器
    /// </summary>
    public class SceneManager : ISceneManager
    {
        /// <summary>
        /// 服务提供者
        /// </summary>
        private readonly IServiceProvider _ServiceProvider;
        /// <summary>
        /// 日志
        /// </summary>
        private readonly ILogger<SceneManager> _Logger;
        /// <summary>
        /// 场景字典
        /// </summary>
        private readonly ConcurrentDictionary<string, IScene> _SceneDic = new ConcurrentDictionary<string, IScene>();

        private bool _IsDispose = false;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <param name="logger"></param>
        public SceneManager(IServiceProvider serviceProvider,
            ILogger<SceneManager> logger)
        {
            _ServiceProvider = serviceProvider;
            _Logger = logger;
        }

        /// <summary>
        /// 创建新的场景
        /// </summary>
        /// <typeparam name="TScene"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public TScene CreateNewScene<TScene>(string name)
            where TScene : class, IScene
        {
            if (_SceneDic.ContainsKey(name)) throw new ArgumentException($"the key {name} is already exists");
            TScene scene = _ServiceProvider.CreateAutowiredInstance<TScene>();
            scene.Name = name;
            _SceneDic.AddOrUpdate(name, scene, (key, value) => value);
            return scene;
        }
        /// <summary>
        /// 删除场景
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool DeleteScene(string name)
        {
            if(_SceneDic.TryRemove(name, out IScene scene))
            {
                if (scene != null) scene.Dispose();
                return true;
            }
            return false;
        }
        /// <summary>
        /// 获取场景
        /// </summary>
        /// <typeparam name="TScene"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public TScene GetScene<TScene>(string name) where TScene : class, IScene
        {

            if(_SceneDic.TryGetValue(name, out IScene scene))
            {
                if (scene is TScene result) 
                    return result;
            }
            return null;
        }
        /// <summary>
        /// 加载场景
        /// </summary>
        /// <typeparam name="TScene"></typeparam>
        /// <param name="name"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public async Task LoadScene<TScene>(string name, params object[] param) 
            where TScene : class, IScene, ILoadableScene<TScene>
        {
            if (_SceneDic.TryGetValue(name, out IScene scene))
            {
                if (scene is TScene result)
                {
                    await result.Load(param);
                    return;
                }
            }
            TScene sc = CreateNewScene<TScene>(name);
            await sc.Load(param);
        }
        /// <summary>
        /// 加载场景
        /// </summary>
        /// <typeparam name="TScene"></typeparam>
        /// <param name="name"></param>
        /// <param name="loadFinish"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public async Task LoadScene<TScene>(string name, Action<TScene> loadFinish, params object[] param) 
            where TScene : class, IScene, ILoadableScene<TScene>
        {
            if (_SceneDic.TryGetValue(name, out IScene scene))
            {
                if (scene is TScene result)
                {
                    result.Loaded += loadFinish;
                    await result.Load(param);
                    return;
                }
            }
            TScene sc = CreateNewScene<TScene>(name);
            sc.Loaded += loadFinish;
            await sc.Load(param);
        }
        /// <summary>
        /// 卸载场景
        /// </summary>
        /// <typeparam name="TScene"></typeparam>
        /// <param name="name"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public async Task UnLoadScene<TScene>(string name, params object[] param) 
            where TScene :class, IScene, ILoadableScene<TScene>
        {
            if (_SceneDic.TryGetValue(name, out IScene scene))
            {
                if (scene is TScene result)
                {
                    await result.UnLoad(param);
                    return;
                }
            }
            TScene sc = CreateNewScene<TScene>(name);
            await sc.UnLoad(param);
        }
        /// <summary>
        /// 卸载场景
        /// </summary>
        /// <typeparam name="TScene"></typeparam>
        /// <param name="name"></param>
        /// <param name="unloadFinish"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public async Task UnLoadScene<TScene>(string name, Action<TScene> unloadFinish, params object[] param) 
            where TScene :class, IScene, ILoadableScene<TScene>
        {
            if (_SceneDic.TryGetValue(name, out IScene scene))
            {
                if (scene is TScene result)
                {
                    result.UnLoaded += unloadFinish;
                    await result.UnLoad(param);
                    return;
                }
            }
            TScene sc = CreateNewScene<TScene>(name);
            sc.UnLoaded += unloadFinish;
            await sc.UnLoad(param);
        }

        /// <summary>
        /// 销毁资源
        /// </summary>
        public void Dispose()
        {
            if (Volatile.Read(ref _IsDispose)) return;
            Volatile.Write(ref _IsDispose, true);
            foreach (var item in _SceneDic)
            {
                item.Value.Dispose();
            }
        }
    }
}
