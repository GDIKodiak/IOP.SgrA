﻿using System;
using IOP.OpenType;
using System.Collections.Generic;
using System.Text;
using System.Numerics;
using System.Runtime.InteropServices;

namespace IOP.SgrA
{
    /// <summary>
    /// 字形Id字符创建者
    /// </summary>
    public class StaticGlyphIdCharacterCreater : CharacterCreater
    {
        /// <summary>
        /// 上限
        /// </summary>
        public float Ascender { get; private set; }
        /// <summary>
        /// 下限
        /// </summary>
        public float Descender { get; private set; }
        /// <summary>
        /// 下间距
        /// </summary>
        public float LingGap { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public float BorderThickness { get; private set; }

        /// <summary>
        /// 表
        /// </summary>
        private readonly Dictionary<string, CharacterInfo> CharacterTable = new Dictionary<string, CharacterInfo>();

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="fontMetrics"></param>
        /// <param name="mappings"></param>
        /// <param name="fontTextureInfo"></param>
        public StaticGlyphIdCharacterCreater(FontMetrics fontMetrics, List<GlyphMapping> mappings, FontTextureInfo fontTextureInfo)
        {
            ArrayLayers = fontTextureInfo.ArrayLayers;
            Height = fontTextureInfo.Height;
            Width = fontTextureInfo.Width;
            EmLength = fontTextureInfo.EmLength;
            BorderThickness = fontTextureInfo.BorderThickness;
            CreateCreaterInfos(fontMetrics, mappings);
        }

        /// <summary>
        /// 创建字符网格
        /// </summary>
        /// <param name="str"></param>
        /// <param name="column"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        public override (float[], uint) CreateTextMeshData(string str, int column, int row)
        {
            CharacterInfo[] infos = GetCharacters(str);
            Span<Vector3> r = new Vector3[(infos.Length * 6 * 2)];
            Span<float> result;
            int index = 0;
            int strIndex = 0;
            float currentX = 0;
            float currentY = 0;
            for (int y = 0; y < row; y++)
            {
                for (int x = 0; x < column; x++)
                {
                    if (strIndex >= infos.Length)
                    {
                        result = MemoryMarshal.Cast<Vector3, float>(r);
                        return (result.ToArray(), (uint)r.Length / 2);
                    }
                    CharacterInfo local = infos[strIndex];
                    Bounding bounding = local.Bounding;
                    Bounding uvBounding = local.UVBounding;
                    Vector3 r0 = new Vector3(currentX + bounding.XMin, currentY + bounding.YMin, 0);
                    Vector3 uvR0 = new Vector3(uvBounding.XMin, uvBounding.YMax, local.PageIndex);
                    Vector3 r1 = new Vector3(currentX + bounding.XMin, currentY + bounding.YMax, 0);
                    Vector3 uvR1 = new Vector3(uvBounding.XMin, uvBounding.YMin, local.PageIndex);
                    Vector3 r2 = new Vector3(currentX + bounding.XMax, currentY + bounding.YMin, 0);
                    Vector3 uvR2 = new Vector3(uvBounding.XMax, uvBounding.YMax, local.PageIndex);
                    Vector3 r3 = new Vector3(currentX + bounding.XMax, currentY + bounding.YMax, 0);
                    Vector3 uvR3 = new Vector3(uvBounding.XMax, uvBounding.YMin, local.PageIndex);

                    r[index++] = r0;
                    r[index++] = uvR0;
                    r[index++] = r1;
                    r[index++] = uvR1;
                    r[index++] = r2;
                    r[index++] = uvR2;
                    r[index++] = r1;
                    r[index++] = uvR1;
                    r[index++] = r3;
                    r[index++] = uvR3;
                    r[index++] = r2;
                    r[index++] = uvR2;

                    currentX += local.AdvanceWidth;
                    strIndex++;
                }
                currentY -= LineHeight;
                currentX = 0;
            }
            r = r.Slice(0, index);
            result = MemoryMarshal.Cast<Vector3, float>(r);
            return (result.ToArray(), (uint)r.Length);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public override CharacterInfo GetCharacter(char c)
        {
            if (CharacterTable.TryGetValue(new string(c, 1), out CharacterInfo info)) return info;
            return null;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="chars"></param>
        /// <returns></returns>
        public override CharacterInfo[] GetCharacters(string chars)
        {
            List<CharacterInfo> infos = new List<CharacterInfo>();
            for (int i = 0; i < chars.Length; i++)
            {
                char c = chars[i];
                if (CharacterTable.TryGetValue(new string(c, 1), out CharacterInfo info)) infos.Add(info);
            }
            return infos.ToArray();
        }

        private void CreateCreaterInfos(FontMetrics fontMetrics, List<GlyphMapping> mappings)
        {
            uint column = Width / EmLength;
            uint row = Height / EmLength;
            uint pageLength = column * row;
            float widthSpan = 1 / (float)column;
            float heightSpan = 1 / (float)row;
            Ascender = fontMetrics.Ascender;
            Descender = fontMetrics.Descender;
            LingGap = fontMetrics.LingGap;
            LineHeight = fontMetrics.LineHeight;
            var metrics = fontMetrics.Metrics;
            for (int i = 0; i < mappings.Count; i++)
            {
                GlyphMapping mapping = mappings[i];
                Metrics met = metrics[(int)mapping.GlyphId];
                uint pIndex = mapping.GlyphId / pageLength;
                uint pageIndex = mapping.GlyphId - pageLength * pIndex;
                int cIndex = (int)(pageIndex % column);
                int rIndex = (int)(pageIndex / column);
                CharacterInfo character = new CharacterInfo()
                {
                    AdvanceWidth = met.AdvanceWidth,
                    Bounding = met.Bounding,
                    Character = mapping.Character,
                    UVBounding = new Bounding((met.UVBounding.XMin - BorderThickness + cIndex) * widthSpan,
                    (met.UVBounding.XMax + BorderThickness + cIndex) * widthSpan,
                    (met.UVBounding.YMin - BorderThickness + rIndex) * heightSpan,
                    (met.UVBounding.YMax + BorderThickness + rIndex) * heightSpan),
                    LeftSideBearings = met.LeftSideBearings,
                    RightSideBearings = met.RightSideBearings,
                    PageIndex = (int)pIndex,
                    Gid = mapping.GlyphId
                };
                CharacterTable.TryAdd(character.Character, character);
            }
        }
    }
}
