﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 文字纹理信息
    /// </summary>
    public class FontTextureInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public uint ArrayLayers { get; set; }
        /// <summary>
        /// 高度
        /// </summary>
        public uint Height { get; set; }
        /// <summary>
        /// 宽度
        /// </summary>
        public uint Width { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public uint EmLength { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public float BorderThickness { get; set; } = 0.05f;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="layers"></param>
        /// <param name="height"></param>
        /// <param name="width"></param>
        /// <param name="emLength"></param>
        public FontTextureInfo(uint layers, uint height, uint width, uint emLength)
        {
            ArrayLayers = layers;
            Height = height;
            Width = width;
            EmLength = emLength;
        }
    }
}
