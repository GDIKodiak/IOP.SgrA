﻿using IOP.OpenType;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 字符信息
    /// </summary>
    public class CharacterInfo
    {
        /// <summary>
        /// 字形Id
        /// </summary>
        public uint Gid { get; set; }
        /// <summary>
        /// 字符
        /// </summary>
        public string Character { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public float LeftSideBearings { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public float RightSideBearings { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public float AdvanceWidth { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PageIndex { get; set; }
        /// <summary>
        /// 包围盒
        /// </summary>
        public Bounding Bounding { get; set; }
        /// <summary>
        /// 纹理坐标包围盒
        /// </summary>
        public Bounding UVBounding { get; set; }
    }
}
