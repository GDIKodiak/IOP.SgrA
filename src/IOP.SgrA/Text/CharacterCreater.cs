﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 文字映射器
    /// </summary>
    public abstract class CharacterCreater
    {
        /// <summary>
        /// 层数
        /// </summary>
        public uint ArrayLayers { get; protected internal set; }
        /// <summary>
        /// 高度
        /// </summary>
        public uint Height { get; protected internal set; }
        /// <summary>
        /// 宽度
        /// </summary>
        public uint Width { get; protected internal set; }
        /// <summary>
        /// EM方形高度
        /// </summary>
        public uint EmLength { get; protected internal set; }
        /// <summary>
        /// 行高
        /// </summary>
        public float LineHeight { get; protected internal set; }
        /// <summary>
        /// 获取字符信息
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public abstract CharacterInfo GetCharacter(char c);
        /// <summary>
        /// 获取多个字符信息
        /// </summary>
        /// <param name="chars"></param>
        /// <returns></returns>
        public abstract CharacterInfo[] GetCharacters(string chars);
        /// <summary>
        /// 创建文字网格
        /// </summary>
        /// <param name="str"></param>
        /// <param name="column"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        public abstract (float[], uint) CreateTextMeshData(string str, int column, int row);
    }
}
