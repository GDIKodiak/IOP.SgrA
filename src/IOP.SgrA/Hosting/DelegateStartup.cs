﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;

namespace IOP.SgrA.Hosting
{
    /// <summary>
    /// 使用委托构建启动类
    /// </summary>
    public class DelegateStartup : ISgrAStartup
    {
        /// <summary>
        /// 配置服务器委托
        /// </summary>
        public readonly Action<IGraphicsBuilder, IHostEnvironment> ConfigureAction;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="configureAction"></param>
        public DelegateStartup(Action<IGraphicsBuilder, IHostEnvironment> configureAction)
        {
            ConfigureAction = configureAction ?? throw new NullReferenceException("The ConfigureAction is null in This DelegateStartup");
        }

        /// <summary>
        /// 配置服务器
        /// </summary>
        /// <param name="server"></param>
        /// <param name="env"></param>
        public void Configure(IGraphicsBuilder server, IHostEnvironment env) => ConfigureAction(server, env);

        /// <summary>
        /// 配置服务
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
        }
    }
}
