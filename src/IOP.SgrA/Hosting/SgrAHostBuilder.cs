﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace IOP.SgrA.Hosting
{
    /// <summary>
    /// 
    /// </summary>
    public class SgrAHostBuilder : ISgrAHostBuilder
    {
        /// <summary>
        /// 服务集合
        /// </summary>
        private IServiceCollection _Services;
        /// <summary>
        /// 主机构建者上下文
        /// </summary>
        private HostBuilderContext _HostBuilderContext;
        /// <summary>
        /// 服务提供者
        /// </summary>
        private IServiceProvider _ServicesProvider;
        /// <summary>
        /// 启动项集合
        /// </summary>
        private IEnumerable<ISgrAStartup> _Startups;


        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="services">服务集合</param>
        /// <param name="builderContext">主机构建者上下文</param>
        public SgrAHostBuilder(IServiceCollection services, HostBuilderContext builderContext)
        {
            _Services = services ?? throw new ArgumentNullException(nameof(services));
            _HostBuilderContext = builderContext ?? throw new ArgumentNullException(nameof(builderContext));
        }
        /// <summary>
        /// 
        /// </summary>
        public void Build()
        {
            _ServicesProvider = _Services.BuildServiceProvider();
            RunStartup();
        }

        /// <summary>
        /// 配置
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public ISgrAHostBuilder Configure(Action<IGraphicsBuilder, IHostEnvironment> builder)
        {
            if (builder == null) throw new ArgumentNullException(nameof(builder));
            var startup = new DelegateStartup(builder);
            _Services.AddSingleton<ISgrAStartup>((factory) => startup);
            _Services.AddSingleton<IGraphicsBuilder, GraphiesBuilder>();
            return this;
        }

        /// <summary>
        /// 配置服务
        /// </summary>
        /// <param name="configureServices"></param>
        /// <returns></returns>
        public ISgrAHostBuilder ConfigureServices(Action<IServiceCollection> configureServices)
        {
            if (configureServices == null) throw new ArgumentNullException(nameof(configureServices));
            configureServices(_Services);
            return this;
        }

        /// <summary>
        /// 配置服务
        /// </summary>
        /// <param name="configureServices"></param>
        /// <returns></returns>
        public ISgrAHostBuilder ConfigureServices(Action<HostBuilderContext, IServiceCollection> configureServices)
        {
            if (configureServices == null) throw new ArgumentNullException(nameof(configureServices));
            configureServices(_HostBuilderContext, _Services);
            return this;
        }

        /// <summary>
        /// 执行Startup文件方法
        /// </summary>
        private void RunStartup()
        {
            EnsureStartup();
            foreach (var item in _Startups)
                item.ConfigureServices(_Services);
        }

        /// <summary>
        /// 确认是否存在Startup文件
        /// </summary>
        private void EnsureStartup()
        {
            if (_Startups != null) return;
            _Startups = _ServicesProvider.GetServices<ISgrAStartup>();
            if (!_Startups.Any())
                throw new InvalidOperationException($"No application configured. Please specify startup via IHostBuilder.UseStartup, IHostBuilder.Configure, injecting {nameof(ISgrAStartup)} or specifying the startup assembly via Startup in the server host configuration.");
        }
    }
}
