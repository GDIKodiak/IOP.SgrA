﻿using IOP.SgrA.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace IOP.SgrA
{
    /// <summary>
    /// 主机扩展
    /// </summary>
    public static class HostExtension
    {
        /// <summary>
        /// 配置SgrA主机
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="configure"></param>
        /// <returns></returns>
        public static IHostBuilder ConfigureSgrAHost(this IHostBuilder builder, Action<ISgrAHostBuilder> configure)
        {
            builder.ConfigureServices((context, services) =>
            {
                ISgrAHostBuilder hostBuilder = new SgrAHostBuilder(services, context);
                configure?.Invoke(hostBuilder);
                hostBuilder.Build();
                services.AddSingleton(hostBuilder);
            });
            return builder;
        }

        /// <summary>
        /// 使用启动项
        /// </summary>
        /// <typeparam name="TStartup"></typeparam>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static ISgrAHostBuilder UseStartup<TStartup>(this ISgrAHostBuilder builder)
            where TStartup : class ,ISgrAStartup
        {
            builder.ConfigureServices(services => 
            {
                services.TryAddSingleton<IGraphicsBuilder, GraphiesBuilder>();
                services.TryAddSingleton<ISgrAStartup, TStartup>(); 
            });
            return builder;
        }
    }
}
