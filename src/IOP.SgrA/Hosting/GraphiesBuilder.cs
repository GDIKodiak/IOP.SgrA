﻿using System;

namespace IOP.SgrA.Hosting
{
    /// <summary>
    /// 图形构建者
    /// </summary>
    public class GraphiesBuilder : IGraphicsBuilder
    {
        /// <summary>
        /// 服务器服务
        /// </summary>
        public IServiceProvider Service { get; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="services"></param>
        public GraphiesBuilder(IServiceProvider services)
        {
            Service = services ?? throw new ArgumentNullException(nameof(services));
        }
    }
}
