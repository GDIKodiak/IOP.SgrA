﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.CompilerServices;
using System.Threading;

namespace IOP.SgrA
{
    /// <summary>
    /// FPS工具
    /// </summary>
    public class FPSUtil
    {
        /// <summary>
        /// 刷新率
        /// </summary>
        public double RefreshRate { get; private set; }
        /// <summary>
        /// 帧率
        /// </summary>
        public double FrameRate { get; private set; }
        /// <summary>
        /// 当前刷新率
        /// </summary>
        public double CurrentFPS { get; private set; }

        private int FPSCount;

        /// <summary>
        /// 当前时间
        /// </summary>
        private DateTime currentTime = DateTime.Now;
        /// <summary>
        /// 开始时间
        /// </summary>
        private DateTime startTime = DateTime.Now;
        /// <summary>
        /// 时间戳
        /// </summary>
        private TimeSpan TimeStamp = TimeSpan.FromMilliseconds(33);
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="refreshRate"></param>
        public FPSUtil(double refreshRate)
        {
            UpdateRate(refreshRate);
        }

        /// <summary>
        /// 更新刷新率
        /// </summary>
        /// <param name="refreshRate"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void UpdateRate(double refreshRate)
        {
            RefreshRate = refreshRate;
            FrameRate = (1000 / refreshRate) + 1;
        }

        /// <summary>
        /// 计算帧率
        /// </summary>
        /// <returns></returns>
        public void CallFPS()
        {
            FPSCount++;
            if(FPSCount == 100)
            {
                FPSCount = 0;
                var end = DateTime.Now;
                CurrentFPS = 1000.0 / ((end - startTime).TotalMilliseconds / 100.0);
                startTime = end;
            }
            TimeStamp = DateTime.Now - currentTime;
        }
        /// <summary>
        /// 获取上一帧计算时间
        /// </summary>
        /// <returns></returns>
        public TimeSpan GetLastStamp() => TimeStamp;
        /// <summary>
        /// 开始记录
        /// </summary>
        public void Begin() => currentTime = DateTime.Now;
        /// <summary>
        /// 如果当前帧计算时间小于刷新帧率，则自旋并让出当前线程的CPU时间片
        /// </summary>
        public void Yield()
        {
            var end = currentTime.AddMilliseconds(FrameRate);
            while(DateTime.Now < end)
            {
                Thread.Yield();
            }
        }
        /// <summary>
        /// 如果当前帧计算时间小于刷新帧率，则主线程在差值时间内睡眠
        /// </summary>
        public void Sleep()
        {
            var end = currentTime.AddMilliseconds(FrameRate);
            var now = DateTime.Now;
            if (now < end)
            {
                Thread.Sleep(end - now);
            }
        }
    }
}
