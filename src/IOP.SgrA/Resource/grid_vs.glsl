#version 330
uniform mat4 view_matrix;
uniform mat4 projection_matrix;

layout (location = 0) in vec3 position;

out vec4 vs_fs_color;

void main(void) {
    vec4 pos1 = vec4(position, 1.0);
    vs_fs_color = vec4(1.0, 1.0, 1.0, 1.0);
    gl_Position = projection_matrix * view_matrix * pos1;
}