﻿using System.Numerics;

namespace IOP.SgrA
{
    /// <summary>
    /// 环境光
    /// </summary>
    public struct Ambient
    {
        /// <summary>
        /// R
        /// </summary>
        public float R;
        /// <summary>
        /// G
        /// </summary>
        public float G;
        /// <summary>
        /// B
        /// </summary>
        public float B;
        /// <summary>
        /// A
        /// </summary>
        public float A;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="r"></param>
        /// <param name="g"></param>
        /// <param name="b"></param>
        /// <param name="a"></param>
        public Ambient(float r, float g, float b, float a)
        {
            R = r;
            G = g;
            B = b;
            A = a;
        }

        /// <summary>
        /// 强制转换
        /// </summary>
        /// <param name="ambient"></param>
        public static implicit operator Vector4(Ambient ambient) => new Vector4(ambient.R, ambient.G, ambient.B, ambient.A); 
        /// <summary>
        /// 隐式重vector4创建
        /// </summary>
        /// <param name="vector"></param>
        public static implicit operator Ambient(Vector4 vector) => new Ambient(vector.X, vector.Y, vector.Z, vector.W);
    }
}
