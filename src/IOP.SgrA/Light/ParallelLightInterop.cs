﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA
{
    /// <summary>
    /// 
    /// </summary>
    public struct ParallelLightInterop
    {
        /// <summary>
        /// 方向
        /// </summary>
        public Vector4 Direction;
        /// <summary>
        /// 
        /// </summary>
        public Vector4 Strength;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="direction"></param>
        /// <param name="strength"></param>
        public ParallelLightInterop(Vector3 direction, Vector4 strength)
        {
            Direction = Vector4.Normalize(new Vector4(direction, 0.0f));
            Strength = strength;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="direction"></param>
        /// <param name="strength"></param>
        public ParallelLightInterop(Vector4 direction, Vector4 strength)
        {
            Direction = Vector4.Normalize(direction);
            Strength = strength;
        }
    }
}
