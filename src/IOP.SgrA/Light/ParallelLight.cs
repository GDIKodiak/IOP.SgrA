﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 平行光
    /// </summary>
    public class ParallelLight : IRenderComponent
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 方向
        /// </summary>
        public Vector4 Direction { get; private set; }
        /// <summary>
        /// 光源强度
        /// </summary>
        public LightStrength Strength { get; private set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="direction"></param>
        /// <param name="strength"></param>
        public ParallelLight(Vector3 direction, LightStrength strength)
        {
            Direction = new Vector4(direction, 0);
            Strength = strength;
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="direction"></param>
        /// <param name="strength"></param>
        public ParallelLight(Vector4 direction, LightStrength strength)
        {
            Direction = direction;
            Strength = strength;
        }

        /// <summary>
        /// 变更位置
        /// </summary>
        /// <param name="direction"></param>
        public void ChangeDirection(Vector3 direction) => Direction = new Vector4(direction, 0);
        /// <summary>
        /// 变更颜色
        /// </summary>
        /// <param name="strength"></param>
        public void ChangeStrength(LightStrength strength) => Strength = strength;
        /// <summary>
        /// 重置
        /// </summary>
        /// <param name="direction"></param>
        /// <param name="strength"></param>
        public void Reset(Vector3 direction, LightStrength strength)
        {
            Direction = new Vector4(direction, 0);
            Strength = strength;
        }
        /// <summary>
        /// 拷贝至新的渲染对象
        /// </summary>
        /// <param name="newObj"></param>
        public void CloneToNewRender(IRenderObject newObj) => newObj.AddComponent(this);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderObject"></param>
        public void OnAttach(IRenderObject renderObject) { }
        /// <summary>
        /// 
        /// </summary>
        public void Destroy()
        {
        }

        /// <summary>
        /// 隐式转换为点光源
        /// </summary>
        /// <param name="light"></param>
        public static implicit operator PointLight(ParallelLight light) => new PointLight(light.Direction, light.Strength);
    }
}
