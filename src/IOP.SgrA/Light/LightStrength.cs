﻿using System.Numerics;

namespace IOP.SgrA
{
    /// <summary>
    /// 光源强度
    /// </summary>
    public struct LightStrength
    {
        /// <summary>
        /// 
        /// </summary>
        public float R;
        /// <summary>
        /// 
        /// </summary>
        public float G;
        /// <summary>
        /// 
        /// </summary>
        public float B;
        /// <summary>
        /// 
        /// </summary>
        public float A;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="r"></param>
        /// <param name="g"></param>
        /// <param name="b"></param>
        /// <param name="a"></param>
        public LightStrength(float r, float g, float b, float a)
        {
            R = r;
            G = g;
            B = b;
            A = a;
        }

        /// <summary>
        /// 隐式转换至Vector4
        /// </summary>
        /// <param name="strength"></param>
        public static implicit operator Vector4(LightStrength strength) => new Vector4(strength.R, strength.G, strength.B, strength.A);
        /// <summary>
        /// 从Vector4隐式转换
        /// </summary>
        /// <param name="vector"></param>
        public static implicit operator LightStrength(Vector4 vector) => new LightStrength(vector.X, vector.Y, vector.Z, vector.W);
    }
}
