﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Numerics;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 点光源
    /// </summary>
    public class PointLight : IRenderComponent
    {
        private Vector4 _Position;
        private LightStrength _Strength;
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// 位置
        /// </summary>
        public Vector4 Position 
        { 
            get => _Position;
            set
            {
                _Position = value;
                Environment?.ChangePointLight(Name, new Vector3(value.X, value.Y, value.Z));
            }
        }
        /// <summary>
        /// 光源强度
        /// </summary>
        public LightStrength Strength 
        {
            get => _Strength;
            set
            {
                _Strength = value;
                Environment?.ChangePointLight(Name, value);
            }
        }
        /// <summary>
        /// 所属光源环境
        /// </summary>
        protected internal ILightEnvironment Environment { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="environment"></param>
        /// <param name="name"></param>
        /// <param name="position"></param>
        /// <param name="strength"></param>
        public PointLight(ILightEnvironment environment, string name, Vector3 position, LightStrength strength)
        {
            if (string.IsNullOrEmpty(name)) name = nameof(PointLight);
            Environment = environment;
            string tName = environment.AddPointLight(name, position, strength);
            Name = tName;
            Position = new Vector4(position, 1.0f);
            Strength = strength;

        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="position"></param>
        /// <param name="strength"></param>
        public PointLight(Vector3 position, LightStrength strength)
        {
            Position = new Vector4(position, 0);
            Strength = strength;
            Name = Guid.NewGuid().ToString("N");
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="position"></param>
        /// <param name="strength"></param>
        public PointLight(Vector4 position, LightStrength strength)
        {
            Position = position;
            Strength = strength;
            Name = Guid.NewGuid().ToString("N");
        }

        /// <summary>
        /// 变更位置
        /// </summary>
        /// <param name="position"></param>
        public void ChangePosition(Vector3 position) => Position = new Vector4(position, 0);
        /// <summary>
        /// 变更颜色
        /// </summary>
        /// <param name="strength"></param>
        public void ChangeStrength(LightStrength strength) => Strength = strength;
        /// <summary>
        /// 重置
        /// </summary>
        /// <param name="position"></param>
        /// <param name="strength"></param>
        public void Reset(Vector3 position, LightStrength strength)
        {
            Position = new Vector4(position, 0);
            Strength = strength;
        }
        /// <summary>
        /// 拷贝至新的渲染对象
        /// </summary>
        /// <param name="newObj"></param>
        /// <exception cref="NotImplementedException"></exception>
        public void CloneToNewRender(IRenderObject newObj) => newObj.AddComponent(this);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderObject"></param>
        public void OnAttach(IRenderObject renderObject) { }
        /// <summary>
        /// 摧毁组件
        /// </summary>
        public void Destroy()
        {
            Environment?.RemovePointLight(Name);
            Environment = null;
        }

        /// <summary>
        /// 隐式转换为平行光
        /// </summary>
        /// <param name="light"></param>
        public static implicit operator ParallelLight(PointLight light) => new ParallelLight(light.Position, light.Strength);
    }
}
