﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA
{
    /// <summary>
    /// 
    /// </summary>
    public struct PointLightInterop
    {
        /// <summary>
        /// 
        /// </summary>
        public Vector4 Position;
        /// <summary>
        /// 
        /// </summary>
        public Vector4 Strength;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="position"></param>
        /// <param name="strength"></param>
        public PointLightInterop(Vector3 position, Vector4 strength)
        {
            Position = new Vector4(position, 1.0f);
            Strength = strength;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="position"></param>
        /// <param name="strength"></param>
        public PointLightInterop(Vector4 position, Vector4 strength)
        {
            Position = position;
            Strength = strength;
        }
    }
}
