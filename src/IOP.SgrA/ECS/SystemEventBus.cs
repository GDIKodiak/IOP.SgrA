﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

namespace IOP.SgrA.ECS
{
    /// <summary>
    /// 事件总线
    /// </summary>
    public class SystemEventBus : ISystemEventBus
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly Dictionary<string, ConcurrentBag<ObserverInfo>> _ActionDictionary = new Dictionary<string, ConcurrentBag<ObserverInfo>>();
        /// <summary>
        /// 观察者
        /// </summary>
        private readonly Dictionary<object, ConcurrentBag<ObserverInfo>> _Observer = new Dictionary<object, ConcurrentBag<ObserverInfo>>();
        /// <summary>
        /// 
        /// </summary>
        private readonly ConcurrentQueue<EventWapper> _FrameBeforeEvents = new ConcurrentQueue<EventWapper>();
        /// <summary>
        /// 
        /// </summary>
        private readonly ConcurrentQueue<EventWapper> _FrameAfterEvents = new ConcurrentQueue<EventWapper>();
        /// <summary>
        /// 
        /// </summary>
        private ILogger<SystemEventBus> _Logger;
        /// <summary>
        /// 
        /// </summary>
        private readonly object _SyncRoot = new object();
        private volatile bool _IsDispose = false;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        public SystemEventBus(ILogger<SystemEventBus> logger)
        {
            _Logger = logger;
        }

        /// <summary>
        /// 发布
        /// </summary>
        /// <typeparam name="TMessage"></typeparam>
        /// <param name="eventName"></param>
        /// <param name="message"></param>
        /// <param name="trigger"></param>
        public void Publish<TMessage>(string eventName, TMessage message, EventTrigger trigger)
        {
            if (string.IsNullOrEmpty(eventName))
            {
                _Logger?.LogWarning($"SystemEvent publish failed, paramter {nameof(eventName)} is null");
                return;
            }
            EventWapper wapper = new EventWapper()
            {
                Message = message,
                MessageType = typeof(TMessage),
                Topic = eventName
            };
            switch (trigger)
            {
                case EventTrigger.SystemFrameBefore:
                    _FrameBeforeEvents.Enqueue(wapper);
                    break;
                case EventTrigger.SystemFrameAfter:
                    _FrameAfterEvents.Enqueue(wapper);
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// 订阅
        /// </summary>
        /// <param name="eventName"></param>
        /// <param name="observer"></param>
        /// <param name="callBack"></param>
        /// <param name="trigger"></param>
        /// <exception cref="NotImplementedException"></exception>
        public void Subscribe(string eventName, object observer, MethodInfo callBack, EventTrigger trigger)
        {
            if (string.IsNullOrEmpty(eventName))
            {
                _Logger?.LogWarning($"SystemEvent subscribe failed, paramter {nameof(eventName)} is null");
                return;
            }
            if(observer == null)
            {
                _Logger?.LogWarning($"SystemEvent subscribe failed, paramter {nameof(observer)} is null");
                return;
            }
            if(callBack == null)
            {
                _Logger?.LogWarning($"SystemEvent subscribe failed, paramter {nameof(callBack)} is null");
                return;
            }
            if(callBack.DeclaringType != observer.GetType())
            {
                _Logger?.LogWarning($"SystemEvent subscribe failed, paramter {nameof(callBack)} is not belong to parameter {nameof(observer)}");
                return;
            }
            if (!_ActionDictionary.TryGetValue(eventName, out ConcurrentBag<ObserverInfo> infos))
            {
                lock (_SyncRoot)
                {
                    if (!_ActionDictionary.TryGetValue(eventName, out infos))
                    {
                        infos = new ConcurrentBag<ObserverInfo>();
                        _ActionDictionary.Add(eventName, infos);
                    }
                }
            }
            try
            {
                var ob = NewOberver(eventName, observer, callBack, trigger);
                infos.Add(ob);

            }
            catch (Exception e)
            {
                _Logger?.LogError(e.Message + "\r\n" + e.StackTrace);
            }
        }
        /// <summary>
        /// 刷新事件
        /// </summary>
        public void UpdateEvent(EventTrigger trigger)
        {
            try
            {
                if(trigger == EventTrigger.SystemFrameBefore)
                {
                    while (_FrameBeforeEvents.TryDequeue(out EventWapper e))
                    {
                        RunEvent(e, trigger);
                    }
                }
                else if(trigger == EventTrigger.SystemFrameAfter)
                {
                    while (_FrameAfterEvents.TryDequeue(out EventWapper e))
                    {
                        RunEvent(e, trigger);
                    }
                }
            }
            catch (Exception e)
            {
                _Logger?.LogError(e.Message + "\r\n" + e.StackTrace);
            }
        }

        /// <summary>
        /// 销毁资源
        /// </summary>
        public void Dispose()
        {
            if (_IsDispose) return;
            _IsDispose = true;
            foreach(var item in _ActionDictionary)
            {
                item.Value.Clear();
            }
            foreach(var item in _Observer)
            {
                item.Value.Clear();
            }
            _ActionDictionary.Clear();
            _Observer.Clear();
        }

        /// <summary>
        /// 运行事件
        /// </summary>
        /// <param name="e"></param>
        /// <param name="trigger"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void RunEvent(EventWapper e, EventTrigger trigger)
        {
            if (string.IsNullOrEmpty(e.Topic)) return;
            if (_ActionDictionary.TryGetValue(e.Topic, out ConcurrentBag<ObserverInfo> info))
            {
                foreach (var item in info)
                {
                    if (item.Trigger == trigger)
                    {
                        if (item.ParameterCount <= 0) item.Function.DynamicInvoke();
                        else
                        {
                            if (item.MessageType == e.MessageType)
                            {
                                item.Function.DynamicInvoke(e.Message);
                            }
                        }
                    }
                }
            }
        }
        /// <summary>
        /// 新建观察者
        /// </summary>
        /// <param name="eventName"></param>
        /// <param name="observer"></param>
        /// <param name="callBack"></param>
        /// <param name="trigger"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        private ObserverInfo NewOberver(string eventName, object observer, MethodInfo callBack, EventTrigger trigger)
        {
            ObserverInfo info = new ObserverInfo();
            info.Topic = eventName;
            info.Observer = observer;
            info.ObserverType = observer.GetType();
            info.Trigger = trigger;
            Delegate func = NewFunction(observer, callBack, info);
            info.Function = func;
            if (!_Observer.TryGetValue(observer, out ConcurrentBag<ObserverInfo> infos))
            {
                lock (_SyncRoot)
                {
                    if (!_Observer.TryGetValue(observer, out infos))
                    {
                        infos = new ConcurrentBag<ObserverInfo>();
                        _Observer.Add(observer, infos);
                    }
                }
            }
            infos.Add(info);
            return info;
        }
        /// <summary>
        /// 新建函数
        /// </summary>
        /// <param name="observer"></param>
        /// <param name="callBack"></param>
        /// <param name="info"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        private Delegate NewFunction(object observer, MethodInfo callBack, ObserverInfo info)
        {
            var param = callBack.GetParameters();
            if (param == null || param.Length > 1) throw new ArgumentNullException("Invalid callback method");
            ConstantExpression ob = Expression.Constant(observer);
            if (param.Length == 0)
            {
                info.ParameterCount = 0;
                MethodCallExpression call = Expression.Call(ob, callBack);
                Expression<Action> lambda = Expression.Lambda<Action>(call, new ParameterExpression[0]);
                Action action = lambda.Compile();
                return action;
            }
            else
            {
                info.ParameterCount = 1;
                info.MessageType = param[0].ParameterType;
                ParameterExpression parameter = Expression.Parameter(param[0].ParameterType, "x");
                MethodCallExpression call = Expression.Call(ob, callBack, parameter);
                LambdaExpression lambda = Expression.Lambda(call, parameter);
                Delegate @delegate = lambda.Compile();
                return @delegate;
            }
        }
    }

    /// <summary>
    /// 事件包装
    /// </summary>
    public struct EventWapper
    {
        /// <summary>
        /// 
        /// </summary>
        public string Topic;
        /// <summary>
        /// 消息类型
        /// </summary>
        public Type MessageType;
        /// <summary>
        /// 消息
        /// </summary>
        public object Message;

    }
    /// <summary>
    /// 观察者信息
    /// </summary>
    public class ObserverInfo
    {
        /// <summary>
        /// 主题
        /// </summary>
        public string Topic;
        /// <summary>
        /// 观察者类型
        /// </summary>
        public Type ObserverType;
        /// <summary>
        /// 消息类型
        /// </summary>
        public Type MessageType;
        /// <summary>
        /// 触发器类型
        /// </summary>
        public EventTrigger Trigger;
        /// <summary>
        /// 观察者
        /// </summary>
        public object Observer;
        /// <summary>
        /// 回调函数
        /// </summary>
        public Delegate Function;
        /// <summary>
        /// 参数数量
        /// </summary>
        public int ParameterCount;
    }
}
