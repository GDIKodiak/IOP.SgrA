﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.ECS
{
    /// <summary>
    /// 同步的组件系统
    /// </summary>
    public abstract class SyncComponentSystem : ComponentSystem
    {
        /// <summary>
        /// 优先级
        /// </summary>
        public abstract int Priority {  get; }
        /// <summary>
        /// 同步触发时机
        /// </summary>
        public abstract SyncExecuteTrigger ExecuteTrigger { get; }
    }

    /// <summary>
    /// 同步执行触发时机
    /// </summary>
    public enum SyncExecuteTrigger
    {
        /// <summary>
        /// 组件系统执行之前
        /// </summary>
        BeforeExecuteSystem,
        /// <summary>
        /// 组件系统执行之后
        /// </summary>
        AfterExecuteSystem,
    }
}
