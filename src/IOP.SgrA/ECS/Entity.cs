﻿using System.Runtime.InteropServices;

namespace IOP.SgrA.ECS
{
    /// <summary>
    /// 实体
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct Entity
    {
        /// <summary>
        /// 下标
        /// </summary>
        public int Index;
        /// <summary>
        /// 版本
        /// </summary>
        public int Version;
        /// <summary>
        /// 此实体在实体容器中的具体下标
        /// </summary>
        public int RunningIndex;
        /// <summary>
        /// 实体状态
        /// </summary>
        public int State;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="index"></param>
        public Entity(int index)
        {
            Index = index;
            Version = 0;
            RunningIndex = 0;
            State = 0;
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="index"></param>
        /// <param name="runningIndex"></param>
        public Entity(int index, int runningIndex)
        {
            Index = index;
            Version = 0;
            RunningIndex = runningIndex;
            State = 0;
        }
        /// <summary>
        /// 恒等于
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <returns></returns>
        public static bool operator == (Entity lhs, Entity rhs) => lhs.Index == rhs.Index && lhs.Version == rhs.Version;
        /// <summary>
        /// 不等于
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <returns></returns>
        public static bool operator !=(Entity lhs, Entity rhs) => lhs.Index != rhs.Index || lhs.Version != rhs.Version;
        /// <summary>
        /// 比较
        /// </summary>
        /// <param name="compare"></param>
        /// <returns></returns>
        public override bool Equals(object compare) => this == (Entity)compare;
        /// <summary>
        /// 获取哈希
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode() => Index;

        /// <summary>
        /// 
        /// </summary>
        public static Entity NULL = new Entity() { Index = -1, RunningIndex = -1, State = -1, Version = 0 };
        /// <summary>
        /// 
        /// </summary>
        public static Entity Zero = new Entity();
    }
}
