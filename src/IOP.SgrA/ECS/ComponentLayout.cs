﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.ECS
{
    /// <summary>
    /// 组件布局
    /// </summary>
    public struct ComponentLayout
    {
        /// <summary>
        /// 组件类型
        /// </summary>
        public Type ComponentType;
        /// <summary>
        /// 起始下标
        /// </summary>
        public int StartIndex;
        /// <summary>
        /// 组件偏移
        /// </summary>
        public int ComponentOffest;
        /// <summary>
        /// 组件数
        /// </summary>
        public int ComponentCount;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="type"></param>
        /// <param name="startIndex"></param>
        /// <param name="componentOffest"></param>
        /// <param name="componentCount"></param>
        public ComponentLayout(Type type, int startIndex, int componentOffest, int componentCount)
        {
            ComponentType = type;
            StartIndex = startIndex;
            ComponentOffest = componentOffest;
            ComponentCount = componentCount;
        }
    }
}
