﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Linq.Expressions;
using System.Reflection;
using System.Linq;
using System.Buffers;

namespace IOP.SgrA.ECS
{
    /// <summary>
    /// 块
    /// </summary>
    public class Chunk
    {
        const int CHUNKSIZE = 16 * 1024;
        /// <summary>
        /// 块当前持有实体数量
        /// </summary>
        public int Count { get; private set; }
        /// <summary>
        /// 块持有的实体上限
        /// </summary>
        public int Length { get; private set; }
        /// <summary>
        /// 内存
        /// </summary>
        public Memory<byte> Memory { get; private set; }
        /// <summary>
        /// 下一段内存
        /// </summary>
        public Chunk Next { get; private set; }
        /// <summary>
        /// 总下标
        /// </summary>
        public long RunningIndex { get; private set; }
        /// <summary>
        /// 块下标
        /// </summary>
        internal int ChnukIndex = 0;
        /// <summary>
        /// 初始化实体用函数
        /// </summary>
        internal Action<int, Memory<byte>, int, int> InitAction = null;
        /// <summary>
        /// 初始化实体用函数
        /// </summary>
        internal Func<int, Memory<byte>, int, int, Entity> InitFunc = null;
        /// <summary>
        /// 使用数值初始化委托函数表
        /// </summary>
        internal readonly Dictionary<Type, Delegate> _InitDataActionDic = new Dictionary<Type, Delegate>();
        /// <summary>
        /// 使用数值初始化委托函数表
        /// </summary>
        internal readonly Dictionary<Type, Delegate> _InitDataFuncDic = new Dictionary<Type, Delegate>();
        /// <summary>
        /// 
        /// </summary>
        internal IMemoryOwner<byte> _Owner = null;
        /// <summary>
        /// 块布局
        /// </summary>
        private readonly Dictionary<Type, ComponentLayout> _Layouts = new Dictionary<Type, ComponentLayout>();
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="layouts"></param>
        /// <param name="finalCount"></param>
        public Chunk(ComponentLayout[] layouts, int finalCount)
        {
            foreach (var item in layouts) _Layouts.TryAdd(item.ComponentType, item);
            Length = finalCount;
            SetBuffer();
        }
        /// <summary>
        /// 创建新的实体
        /// </summary>
        /// <param name="size"></param>
        /// <param name="startIndex"></param>
        public void CreateEntities(ref int startIndex, int size)
        {
            if (Count + size > Length) throw new OutOfMemoryException("target chunk is not enought to create new entities");
            var initA = InitAction;
            if(InitAction == null)
            {
                initA = CreateNewInitAction();
                InitAction = initA;
            }
            int entityIndex = startIndex;
            int currentIndex = Count;
            int rIndex = (int)RunningIndex;
            Memory<byte> b = Memory;
            for(int i = currentIndex; i < currentIndex + size; i++)
            {
                initA(i, b, entityIndex, rIndex);
                entityIndex++;
                rIndex++;
            }
            RunningIndex += size;
            Count += size;
            startIndex = entityIndex;
        }
        /// <summary>
        /// 使用初始值创建实体
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="startIndex"></param>
        /// <param name="size"></param>
        /// <param name="initData"></param>
        /// <param name="infos"></param>
        public void CreateEntities<TGroup>(ref int startIndex, int size, TGroup initData, MemberInfo[] infos)
            where TGroup : struct
        {
            var type = typeof(TGroup);
            if (Count + size > Length) throw new OutOfMemoryException("target chunk is not enought to create new entities");
            Action<int, Memory<byte>, int, int, TGroup> initAction = GetInitDataAction<TGroup>(infos);
            int entityIndex = startIndex;
            int currentIndex = Count;
            int rIndex = (int)RunningIndex;
            Memory<byte> b = Memory;
            for (int i = currentIndex; i < currentIndex + size; i++)
            {
                initAction(i, b, entityIndex, rIndex, initData);
                entityIndex++;
                rIndex++;
            }
            RunningIndex += size;
            Count += size;
            startIndex = entityIndex;
        }
        /// <summary>
        /// 使用初始值创建实体
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="startIndex"></param>
        /// <param name="size"></param>
        /// <param name="initData"></param>
        /// <param name="infos"></param>
        /// <param name="entities"></param>
        public void CreateEntities<TGroup>(ref int startIndex, int size, TGroup initData, MemberInfo[] infos, ref Span<Entity> entities)
            where TGroup : struct
        {
            var type = typeof(TGroup);
            if (Count + size > Length) throw new OutOfMemoryException("target chunk is not enought to create new entities");
            Func<int, Memory<byte>, int, int, TGroup, Entity> initFunc = GetInitDataFunc<TGroup>(infos);
            int entityIndex = startIndex;
            int currentIndex = Count;
            int rIndex = (int)RunningIndex;
            int local = 0;
            Memory<byte> b = Memory;
            for (int i = currentIndex; i < currentIndex + size; i++)
            {
                entities[local++] = initFunc(i, b, entityIndex, rIndex, initData);
                entityIndex++;
                rIndex++;
            }
            RunningIndex += size;
            Count += size;
            startIndex = entityIndex;
        }
        /// <summary>
        /// 创建新的实体
        /// </summary>
        /// <param name="startIndex"></param>
        /// <param name="size"></param>
        /// <param name="entities"></param>
        /// <returns></returns>
        public void CreateEntities(ref int startIndex, int size, ref Span<Entity> entities)
        {
            if (Count + size > Length) throw new OutOfMemoryException("target chunk is not enought to create new entities");
            var initA = InitFunc;
            if (InitFunc == null)
            {
                initA = CreateNewInitFunc();
                InitFunc = initA;
            }
            int entityIndex = startIndex;
            int currentIndex = Count;
            int rIndex = (int)RunningIndex;
            int local = 0;
            Memory<byte> b = Memory;
            for (int i = currentIndex; i < currentIndex + size; i++)
            {
                entities[local++] = initA(i, b, entityIndex, rIndex);
                entityIndex++;
                rIndex++;
            }
            RunningIndex += size;
            Count += size;
            startIndex = entityIndex;
        }

        /// <summary>
        /// 设置下一块内存
        /// </summary>
        /// <param name="chunk"></param>
        public void SetNext(Chunk chunk)
        {
            Next = chunk;
            chunk.InitAction = InitAction;
            chunk.ChnukIndex = ChnukIndex + 1;
            chunk = this;
            while(chunk.Next != null)
            {
                chunk.Next.RunningIndex = chunk.RunningIndex;
                chunk = chunk.Next;
            }
        }
        /// <summary>
        /// 释放内存
        /// </summary>
        public void Free()
        {
            var local = this;
            local.RunningIndex = 0;
            local.Length = 0;
            local.Count = 0;
            local.ChnukIndex = 0;
            Memory = default;
            _Owner.Dispose();
            while (local.Next != null)
            {
                local = local.Next;
                local.Free();
                local.Next = null;
            }
        }

        /// <summary>
        /// 设置内存
        /// </summary>
        internal void SetBuffer()
        {
            _Owner = MemoryPool<byte>.Shared.Rent(CHUNKSIZE);
            Memory = _Owner.Memory;
        }

        /// <summary>
        /// 获取初始化数据生成委托
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="infos"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private Action<int, Memory<byte>, int, int, TGroup> GetInitDataAction<TGroup>(MemberInfo[] infos)
            where TGroup : struct
        {
            var type = typeof(TGroup);
            Action<int, Memory<byte>, int, int, TGroup> action;
            if (_InitDataActionDic.TryGetValue(type, out Delegate @delegate))
            {
                action = @delegate as Action<int, Memory<byte>, int, int, TGroup>;
                if (action == null)
                {
                    action = CreateNewInitAction<TGroup>(infos);
                    _InitDataActionDic[type] = action;
                }
            }
            else
            {
                action = CreateNewInitAction<TGroup>(infos);
                _InitDataActionDic.Add(type, action);
            }
            return action;
        }
        /// <summary>
        /// 获取初始化数据生成委托
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="infos"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private Func<int, Memory<byte>, int, int, TGroup, Entity> GetInitDataFunc<TGroup>(MemberInfo[] infos)
            where TGroup : struct
        {
            var type = typeof(TGroup);
            Func<int, Memory<byte>, int, int, TGroup, Entity> func;
            if (_InitDataFuncDic.TryGetValue(type, out Delegate @delegate))
            {
                func = @delegate as Func<int, Memory<byte>, int, int, TGroup, Entity>;
                if(func == null)
                {
                    func = CreateNewInitFunc<TGroup>(infos);
                    _InitDataFuncDic[type] = func;
                }
            }
            else
            {
                func = CreateNewInitFunc<TGroup>(infos);
                _InitDataFuncDic.Add(type, func);
            }
            return func;
        }
        /// <summary>
        /// 创建新的初始化函数
        /// </summary>
        /// <returns></returns>
        private Action<int, Memory<byte>, int, int> CreateNewInitAction()
        {
            try
            {
                ParameterExpression x = Expression.Parameter(typeof(int), "x");
                ParameterExpression y = Expression.Parameter(typeof(Memory<byte>), "y");
                ParameterExpression z = Expression.Parameter(typeof(int), "z");
                ParameterExpression a = Expression.Parameter(typeof(int), "a");
                List<Expression> expressions = new List<Expression>(_Layouts.Count);
                foreach (var item in _Layouts.Values)
                {
                    ConstantExpression startIndex = Expression.Constant(item.StartIndex);
                    ConstantExpression offest = Expression.Constant(item.ComponentOffest);
                    BinaryExpression bm = Expression.MakeBinary(ExpressionType.Multiply, offest, x);
                    BinaryExpression finalOffest = Expression.MakeBinary(ExpressionType.Add, startIndex, bm);
                    if (item.ComponentType == typeof(Entity))
                    {
                        var constructor = item.ComponentType.GetConstructor(new Type[] { typeof(int), typeof(int) });
                        var newEntity = Expression.New(constructor, z, a);
                        var method = typeof(MemoryHelper).GetMethods().Where(x => x.IsGenericMethod && x.Name == "WriteStruct").First().MakeGenericMethod(item.ComponentType);
                        MethodCallExpression setStruct = Expression.Call(method, new Expression[] { y, newEntity, finalOffest, offest });
                        expressions.Add(setStruct);
                    }
                    else
                    {
                        var method = typeof(MemoryHelper).GetMethods().Where(x => x.IsGenericMethod && x.Name == "WriteStruct").First().MakeGenericMethod(item.ComponentType);
                        MethodCallExpression setStruct = Expression.Call(method, new Expression[] { y, Expression.New(item.ComponentType), finalOffest, offest });
                        expressions.Add(setStruct);
                    }
                }
                BlockExpression block = Expression.Block(expressions);
                Expression<Action<int, Memory<byte>, int, int>> initExpression = Expression.Lambda<Action<int, Memory<byte>, int, int>>(block, x, y, z, a);
                Action<int, Memory<byte>, int, int> action = initExpression.Compile();
                return action;
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// 创建新的初始化函数
        /// </summary>
        /// <returns></returns>
        private Func<int, Memory<byte>, int, int, Entity> CreateNewInitFunc()
        {
            try
            {
                var entityType = typeof(Entity);
                ParameterExpression x = Expression.Parameter(typeof(int), "x");
                ParameterExpression y = Expression.Parameter(typeof(Memory<byte>), "y");
                ParameterExpression z = Expression.Parameter(typeof(int), "z");
                ParameterExpression a = Expression.Parameter(typeof(int), "a");
                List<Expression> expressions = new List<Expression>(_Layouts.Count);
                ParameterExpression entity = Expression.Variable(entityType, "result");
                foreach (var item in _Layouts.Values)
                {
                    ConstantExpression startIndex = Expression.Constant(item.StartIndex);
                    ConstantExpression offest = Expression.Constant(item.ComponentOffest);
                    BinaryExpression bm = Expression.MakeBinary(ExpressionType.Multiply, offest, x);
                    BinaryExpression finalOffest = Expression.MakeBinary(ExpressionType.Add, startIndex, bm);
                    if (item.ComponentType == entityType)
                    {
                        var constructor = item.ComponentType.GetConstructor(new Type[] { typeof(int), typeof(int) });
                        var newEntity = Expression.New(constructor, z, a);
                        BinaryExpression assgin = Expression.Assign(entity, newEntity);
                        expressions.Add(assgin);
                        var method = typeof(MemoryHelper).GetMethods().Where(x => x.IsGenericMethod && x.Name == "WriteStruct").First().MakeGenericMethod(item.ComponentType);
                        MethodCallExpression setStruct = Expression.Call(method, new Expression[] { y, entity, finalOffest, offest });
                        expressions.Add(setStruct);
                    }
                    else
                    {
                        var method = typeof(MemoryHelper).GetMethods().Where(x => x.IsGenericMethod && x.Name == "WriteStruct").First().MakeGenericMethod(item.ComponentType);
                        MethodCallExpression setStruct = Expression.Call(method, new Expression[] { y, Expression.New(item.ComponentType), finalOffest, offest });
                        expressions.Add(setStruct);
                    }
                }
                expressions.Add(entity);
                BlockExpression block = Expression.Block(entityType, new ParameterExpression[] { entity }, expressions);
                Expression<Func<int, Memory<byte>, int, int, Entity>> initExpression = Expression.Lambda<Func<int, Memory<byte>, int, int, Entity>>(block, x, y, z, a);
                Func<int, Memory<byte>, int, int, Entity> result = initExpression.Compile();
                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }
        /// <summary>
        /// 创建新的初始化函数
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <returns></returns>
        private Action<int, Memory<byte>, int, int, TGroup> CreateNewInitAction<TGroup>(MemberInfo[] infos)
            where TGroup : struct
        {
            try
            {
                Type gType = typeof(TGroup);
                ParameterExpression x = Expression.Parameter(typeof(int), "x");
                ParameterExpression y = Expression.Parameter(typeof(Memory<byte>), "y");
                ParameterExpression z = Expression.Parameter(typeof(int), "z");
                ParameterExpression a = Expression.Parameter(typeof(int), "a");
                ParameterExpression g = Expression.Parameter(gType, "g");
                List<Expression> expressions = new List<Expression>(_Layouts.Count);
                Dictionary<Type, bool> localDic = new Dictionary<Type, bool>(_Layouts.Count);
                foreach (var info in infos)
                {
                    var lType = info.MemberType == MemberTypes.Field ? (info as FieldInfo).FieldType : (info as PropertyInfo).PropertyType;
                    if (_Layouts.TryGetValue(lType, out ComponentLayout layout))
                    {
                        if (layout.ComponentType == typeof(Entity)) continue;
                        localDic.Add(lType, true);
                        ConstantExpression startIndex = Expression.Constant(layout.StartIndex);
                        ConstantExpression offest = Expression.Constant(layout.ComponentOffest);
                        BinaryExpression bm = Expression.MakeBinary(ExpressionType.Multiply, offest, x);
                        Expression property = Expression.PropertyOrField(g, info.Name);
                        BinaryExpression finalOffest = Expression.MakeBinary(ExpressionType.Add, startIndex, bm);
                        var method = typeof(MemoryHelper).GetMethods().Where(x => x.IsGenericMethod && x.Name == "WriteStruct").First().MakeGenericMethod(layout.ComponentType);
                        MethodCallExpression setStruct = Expression.Call(method, new Expression[] { y, property, finalOffest, offest });
                        expressions.Add(setStruct);
                    }
                }
                foreach(var item in _Layouts.Values)
                {
                    if (localDic.ContainsKey(item.ComponentType)) continue;
                    ConstantExpression startIndex = Expression.Constant(item.StartIndex);
                    ConstantExpression offest = Expression.Constant(item.ComponentOffest);
                    BinaryExpression bm = Expression.MakeBinary(ExpressionType.Multiply, offest, x);
                    BinaryExpression finalOffest = Expression.MakeBinary(ExpressionType.Add, startIndex, bm);
                    if (item.ComponentType == typeof(Entity))
                    {
                        var constructor = item.ComponentType.GetConstructor(new Type[] { typeof(int), typeof(int) });
                        var newEntity = Expression.New(constructor, z, a);
                        var method = typeof(MemoryHelper).GetMethods().Where(x => x.IsGenericMethod && x.Name == "WriteStruct").First().MakeGenericMethod(item.ComponentType);
                        MethodCallExpression setStruct = Expression.Call(method, new Expression[] { y, newEntity, finalOffest, offest });
                        expressions.Add(setStruct);
                    }
                    else
                    {
                        var method = typeof(MemoryHelper).GetMethods().Where(x => x.IsGenericMethod && x.Name == "WriteStruct").First().MakeGenericMethod(item.ComponentType);
                        MethodCallExpression setStruct = Expression.Call(method, new Expression[] { y, Expression.New(item.ComponentType), finalOffest, offest });
                        expressions.Add(setStruct);
                    }
                }
                BlockExpression block = Expression.Block(expressions);
                Expression<Action<int, Memory<byte>, int, int, TGroup>> initExpression = Expression.Lambda<Action<int, Memory<byte>, int, int, TGroup>>(block, x, y, z, a, g);
                Action<int, Memory<byte>, int, int, TGroup> action = initExpression.Compile();
                return action;
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// 创建新的初始化函数
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="infos"></param>
        /// <returns></returns>
        private Func<int, Memory<byte>, int, int, TGroup, Entity> CreateNewInitFunc<TGroup>(MemberInfo[] infos)
            where TGroup : struct
        {
            try
            {
                var entityType = typeof(Entity);
                Type gType = typeof(TGroup);
                ParameterExpression x = Expression.Parameter(typeof(int), "x");
                ParameterExpression y = Expression.Parameter(typeof(Memory<byte>), "y");
                ParameterExpression z = Expression.Parameter(typeof(int), "z");
                ParameterExpression a = Expression.Parameter(typeof(int), "a");
                ParameterExpression g = Expression.Parameter(gType, "g");
                List<Expression> expressions = new List<Expression>(_Layouts.Count);
                ParameterExpression entity = Expression.Variable(entityType, "result");
                Dictionary<Type, bool> localDic = new Dictionary<Type, bool>(_Layouts.Count);
                foreach (var info in infos)
                {
                    var lType = info.MemberType == MemberTypes.Field ? (info as FieldInfo).FieldType : (info as PropertyInfo).PropertyType;
                    if (_Layouts.TryGetValue(lType, out ComponentLayout layout))
                    {
                        if (layout.ComponentType == entityType) continue;
                        localDic.Add(lType, true);
                        ConstantExpression startIndex = Expression.Constant(layout.StartIndex);
                        ConstantExpression offest = Expression.Constant(layout.ComponentOffest);
                        BinaryExpression bm = Expression.MakeBinary(ExpressionType.Multiply, offest, x);
                        BinaryExpression finalOffest = Expression.MakeBinary(ExpressionType.Add, startIndex, bm);
                        Expression property = Expression.PropertyOrField(g, info.Name);
                        var method = typeof(MemoryHelper).GetMethods().Where(x => x.IsGenericMethod && x.Name == "WriteStruct").First().MakeGenericMethod(layout.ComponentType);
                        MethodCallExpression setStruct = Expression.Call(method, new Expression[] { y, property, finalOffest, offest });
                        expressions.Add(setStruct);
                    }
                }
                foreach (var item in _Layouts.Values)
                {
                    if (localDic.ContainsKey(item.ComponentType)) continue;
                    ConstantExpression startIndex = Expression.Constant(item.StartIndex);
                    ConstantExpression offest = Expression.Constant(item.ComponentOffest);
                    BinaryExpression bm = Expression.MakeBinary(ExpressionType.Multiply, offest, x);
                    BinaryExpression finalOffest = Expression.MakeBinary(ExpressionType.Add, startIndex, bm);
                    if (item.ComponentType == typeof(Entity))
                    {
                        var constructor = item.ComponentType.GetConstructor(new Type[] { typeof(int), typeof(int) });
                        var newEntity = Expression.New(constructor, z, a);
                        BinaryExpression assgin = Expression.Assign(entity, newEntity);
                        expressions.Add(assgin);
                        var method = typeof(MemoryHelper).GetMethods().Where(x => x.IsGenericMethod && x.Name == "WriteStruct").First().MakeGenericMethod(item.ComponentType);
                        MethodCallExpression setStruct = Expression.Call(method, new Expression[] { y, newEntity, finalOffest, offest });
                        expressions.Add(setStruct);
                    }
                    else
                    {
                        var method = typeof(MemoryHelper).GetMethods().Where(x => x.IsGenericMethod && x.Name == "WriteStruct").First().MakeGenericMethod(item.ComponentType);
                        MethodCallExpression setStruct = Expression.Call(method, new Expression[] { y, Expression.New(item.ComponentType), finalOffest, offest });
                        expressions.Add(setStruct);
                    }
                }
                expressions.Add(entity);
                BlockExpression block = Expression.Block(entityType, new ParameterExpression[] { entity }, expressions);
                Expression<Func<int, Memory<byte>, int, int, TGroup, Entity>> initExpression = Expression.Lambda<Func<int, Memory<byte>, int, int, TGroup, Entity>>(block, x, y, z, a, g);
                Func<int, Memory<byte>, int, int, TGroup, Entity> result = initExpression.Compile();
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
