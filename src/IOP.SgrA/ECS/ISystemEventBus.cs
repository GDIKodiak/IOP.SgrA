﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace IOP.SgrA.ECS
{
    /// <summary>
    /// 事件总线
    /// </summary>
    public interface ISystemEventBus : IDisposable
    {
        /// <summary>
        /// 推送
        /// </summary>
        /// <param name="eventName"></param>
        /// <param name="message"></param>
        /// <param name="trigger"></param>
        void Publish<TMessage>(string eventName, TMessage message, EventTrigger trigger);
        /// <summary>
        /// 订阅
        /// </summary>
        /// <param name="eventName"></param>
        /// <param name="observer"></param>
        /// <param name="callBack"></param>
        /// <param name="trigger"></param>
        void Subscribe(string eventName, object observer, MethodInfo callBack, EventTrigger trigger);
    }

    /// <summary>
    /// 事件触发器
    /// </summary>
    public enum EventTrigger
    {
        /// <summary>
        /// 帧计算前
        /// </summary>
        SystemFrameBefore,
        /// <summary>
        /// 帧计算后
        /// </summary>
        SystemFrameAfter
    }
}
