﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.ECS
{
    /// <summary>
    /// 引用处理函数委托
    /// </summary>
    /// <typeparam name="TGroup"></typeparam>
    /// <param name="group"></param>
    public delegate void RefHandle<TGroup>(ref TGroup group) where TGroup : struct;
}
