﻿using IOP.SgrA.ECS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 组件系统
    /// </summary>
    public abstract class ComponentSystem : IComponentSystem
    {
        /// <summary>
        /// 是否初始化
        /// </summary>
        public bool IsInit { get; protected set; }
        /// <summary>
        /// 输入管理
        /// </summary>
        public IInputStateController Input { get; set; }
        /// <summary>
        /// 上下文管理器
        /// </summary>
        public IContextManager ContextManager { get; set; }
        /// <summary>
        /// 事件总线
        /// </summary>
        public ISystemEventBus EventBus { get; set; }
        /// <summary>
        /// 所有者
        /// </summary>
        public Scene Owner { get; set; }
        /// <summary>
        /// 初始化
        /// </summary>
        public virtual void Initialization()
        {
            if (IsInit) return;
            IsInit = !IsInit;
        }
        /// <summary>
        /// 更新
        /// </summary>
        public abstract void Update(TimeSpan lastStamp);
    }
}
