﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

namespace IOP.SgrA.ECS
{
    /// <summary>
    /// 实体管理器
    /// </summary>
    public class EntityManager : IEntityManager
    {

        /// <summary>
        /// 原型管理器
        /// </summary>
        internal ArchetypeManager ArchetypeManager { get; } = new ArchetypeManager();
        /// <summary>
        /// 原型数据列表
        /// </summary>
        private readonly ConcurrentDictionary<ArchetypeBitMap, ArchetypeDataList> _ArchetypeDatas = new ConcurrentDictionary<ArchetypeBitMap, ArchetypeDataList>();
        /// <summary>
        /// 类型元信息缓存
        /// </summary>
        private readonly ConcurrentDictionary<Type, GroupCache> _GroupInfoCache = new ConcurrentDictionary<Type, GroupCache>();
        /// <summary>
        /// 实体分布
        /// </summary>
        private readonly List<EntityDistribution> _Distribution = new List<EntityDistribution>();
        private bool _IsDispose = false;

        /// <summary>
        /// 添加组件程序集
        /// </summary>
        /// <param name="assembly"></param>
        public void AddComponentAssembly(Assembly assembly) => ArchetypeManager.AddComponentAssembly(assembly);
        /// <summary>
        /// 加载组件
        /// </summary>
        public void LoadComponents() => ArchetypeManager.LoadComponents();
        /// <summary>
        /// 获取用于渲染目标的原型
        /// </summary>
        /// <returns></returns>
        public Archetype RenderTargetArchetype()
        {
            if (ArchetypeManager.TryGetArchetype(nameof(RenderTarget), out Archetype archetype)) return archetype;
            archetype = ArchetypeManager.CreateArchetype(nameof(RenderTarget), out bool isHave, new RenderTargetComponent());
            if (!isHave)
            {
                ArchetypeDataList list = new ArchetypeDataList(archetype);
                _ArchetypeDatas.AddOrUpdate(list.Archetype.BitMap, list, (key, value) => list);
            }
            return archetype;
        }
        /// <summary>
        /// 获取一个无主的原型
        /// </summary>
        /// <returns></returns>
        public Archetype UnownedArchetype() => ArchetypeManager.UnownedArchetype();
        /// <summary>
        /// 创建原型
        /// </summary>
        /// <param name="components"></param>
        /// <returns></returns>
        public Archetype CreateArchetype(params IComponentData[] components)
        {
            var archetype = ArchetypeManager.CreateArchetype(out bool isHave, components);
            if (!isHave)
            {
                ArchetypeDataList list = new ArchetypeDataList(archetype);
                _ArchetypeDatas.AddOrUpdate(list.Archetype.BitMap, list, (key, value) => list);
            }
            return archetype;
        }
        /// <summary>
        /// 从指定类型创建原型
        /// </summary>
        /// <typeparam name="TComponentGorup"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public Archetype CreateArchetype<TComponentGorup>(string name = "") 
            where TComponentGorup : struct
        {
            var archetype = ArchetypeManager.CreateArchetype<TComponentGorup>(name, out bool isHave);
            if (!isHave)
            {
                ArchetypeDataList list = new ArchetypeDataList(archetype);
                _ArchetypeDatas.AddOrUpdate(list.Archetype.BitMap, list, (key, value) => list);
            }
            return archetype;
        }
        /// <summary>
        /// 创建原型
        /// </summary>
        /// <param name="name"></param>
        /// <param name="components"></param>
        /// <returns></returns>
        public Archetype CreateArchetype(string name, params IComponentData[] components)
        {
            var archetype = ArchetypeManager.CreateArchetype(name, out bool isHave, components);
            if (!isHave)
            {
                ArchetypeDataList list = new ArchetypeDataList(archetype);
                _ArchetypeDatas.AddOrUpdate(list.Archetype.BitMap, list, (key, value) => value);
            }
            return archetype;
        }
        /// <summary>
        /// 尝试获取原型
        /// </summary>
        /// <param name="name"></param>
        /// <param name="archetype"></param>
        /// <returns></returns>
        public bool TryGetArchetype(string name, out Archetype archetype) => ArchetypeManager.TryGetArchetype(name, out archetype);

        /// <summary>
        /// 创建实体
        /// </summary>
        /// <param name="archetype"></param>
        /// <param name="size"></param>
        /// <param name="index"></param>
        public void CreateEntities(Archetype archetype, int size, ref int index)
        {
            if (_ArchetypeDatas.TryGetValue(archetype.BitMap, out ArchetypeDataList list))
            {
                int start = index;
                list.CreateEntities(ref index, size);
                int end = index;
                if (start == end) return;
                SetDistribution(archetype, start, end - 1);
            }
            else throw new NullReferenceException("Invalid Archetype");
        }
        /// <summary>
        /// 创建实体
        /// </summary>
        /// <param name="archetype"></param>
        /// <param name="size"></param>
        /// <param name="index"></param>
        /// <param name="entities"></param>
        public void CreateEntities(Archetype archetype, int size, ref int index, out Entity[] entities)
        {
            if (_ArchetypeDatas.TryGetValue(archetype.BitMap, out ArchetypeDataList list))
            {
                int start = index;
                list.CreateEntities(ref index, size, out entities);
                int end = index;
                if (start == end) return;
                SetDistribution(archetype, start, end - 1);
            }
            else throw new NullReferenceException("Invalid Archetype");
        }
        /// <summary>
        /// 创建实体
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="archetype"></param>
        /// <param name="size"></param>
        /// <param name="index"></param>
        /// <param name="initData"></param>
        public void CreateEntities<TGroup>(Archetype archetype, int size, ref int index, TGroup initData)
            where TGroup : struct
        {
            var type = typeof(TGroup);
            GroupCache infos = GetGroupCache(type);
            if (infos == null) return;
            if (_ArchetypeDatas.TryGetValue(archetype.BitMap, out ArchetypeDataList list))
            {
                int start = index;
                list.CreateEntities(ref index, size, initData, infos.MemberInfos);
                int end = index;
                if (start == end) return;
                SetDistribution(archetype, start, end - 1);
            }
            else throw new NullReferenceException("Invalid Archetype");
        }
        /// <summary>
        /// 创建实体
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="archetype"></param>
        /// <param name="size"></param>
        /// <param name="index"></param>
        /// <param name="initData"></param>
        /// <param name="entities"></param>
        public void CreateEntities<TGroup>(Archetype archetype, int size, ref int index, TGroup initData, out Entity[] entities)
            where TGroup : struct
        {
            var type = typeof(TGroup);
            GroupCache infos = GetGroupCache(type);
            if (infos == null) { entities = default; }
            if (_ArchetypeDatas.TryGetValue(archetype.BitMap, out ArchetypeDataList list))
            {
                int start = index;
                list.CreateEntities(ref index, size, initData, infos.MemberInfos, out entities);
                int end = index;
                if (start == end) return;
                SetDistribution(archetype, start, end - 1);
            }
            else throw new NullReferenceException("Invalid Archetype");
        }

        /// <summary>
        /// 回收实体
        /// </summary>
        /// <param name="entity"></param>
        public void RecoveryEntity(Entity entity)
        {
            EntityDistribution distribution = GetDistribution(entity.Index, out _);
            if(_ArchetypeDatas.TryGetValue(distribution.Archetype, out var list))
            {
                list.RecoveryEntity(entity);
            }
        }
        /// <summary>
        /// 回收实体
        /// </summary>
        /// <param name="entities"></param>
        public void RecoveryEntities(Entity[] entities)
        {
            if (entities == null || !entities.Any()) return;
            EntityDistribution distribution = GetDistribution(entities[0].Index, out int midd);
            if (_ArchetypeDatas.TryGetValue(distribution.Archetype, out var list)) list.RecoveryEntity(entities[0]);
            for (int i = 1; i < entities.Length; i++)
            {
                distribution = GetDistribution(midd, entities[i].Index, out midd);
                if (distribution.Archetype == list.Archetype.BitMap) list.RecoveryEntity(entities[i]);
                else
                {
                    if (_ArchetypeDatas.TryGetValue(distribution.Archetype, out list)) 
                        list.RecoveryEntity(entities[i]);
                }
            }
        }

        /// <summary>
        /// 执行遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="foreachAction"></param>
        public void Foreach<TGroup>(Action<TGroup> foreachAction)
            where TGroup : struct
        {
            var type = typeof(TGroup);
            GroupCache infos = GetGroupCache(type);
            if (infos == null) return;
            ArchetypeDataList local;
            foreach(var item in _ArchetypeDatas)
            {
                local = item.Value;
                if (local.Archetype.FullMatch(infos.BitMap))
                {
                    local.Foreach(infos.MemberInfos, foreachAction);
                }
            }
        }
        /// <summary>
        /// 执行遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="foreachAction"></param>
        public void Foreach<TGroup>(Action<TGroup, int> foreachAction)
            where TGroup : struct
        {
            int index = 0;
            var type = typeof(TGroup);
            GroupCache infos = GetGroupCache(type);
            if (infos == null) return;
            ArchetypeDataList local;
            foreach (var item in _ArchetypeDatas)
            {
                local = item.Value;
                if (local.Archetype.FullMatch(infos.BitMap))
                {
                    local.Foreach(infos.MemberInfos, foreachAction, ref index);
                }
            }
        }
        /// <summary>
        /// 执行遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="foreachFunc"></param>
        public void Foreach<TGroup>(Func<TGroup> foreachFunc)
            where TGroup : struct
        {
            var type = typeof(TGroup);
            GroupCache infos = GetGroupCache(type);
            if (infos == null) return;
            ArchetypeDataList local;
            foreach (var item in _ArchetypeDatas)
            {
                local = item.Value;
                if (local.Archetype.FullMatch(infos.BitMap))
                {
                    local.Foreach(infos.MemberInfos, foreachFunc);
                }
            }
        }
        /// <summary>
        /// 执行遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="foreachFunc"></param>
        public void Foreach<TGroup>(Func<int, TGroup> foreachFunc)
            where TGroup : struct
        {
            int index = 0;
            var type = typeof(TGroup);
            GroupCache infos = GetGroupCache(type);
            if (infos == null) return;
            ArchetypeDataList local;
            foreach (var item in _ArchetypeDatas)
            {
                local = item.Value;
                if (local.Archetype.FullMatch(infos.BitMap))
                {
                    local.Foreach(infos.MemberInfos, foreachFunc, ref index);
                }
            }
        }
        /// <summary>
        /// 执行遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="foreachFunc"></param>
        public void Foreach<TGroup>(Func<TGroup, TGroup> foreachFunc)
            where TGroup : struct
        {
            var type = typeof(TGroup);
            GroupCache infos = GetGroupCache(type);
            if (infos == null) return;
            ArchetypeDataList local;
            foreach (var item in _ArchetypeDatas)
            {
                local = item.Value;
                if (local.Archetype.FullMatch(infos.BitMap))
                {
                    local.Foreach(infos.MemberInfos, foreachFunc);
                }
            }
        }
        /// <summary>
        /// 执行遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="foreachFunc"></param>
        public void Foreach<TGroup>(Func<TGroup, int, TGroup> foreachFunc)
            where TGroup : struct
        {
            int index = 0;
            var type = typeof(TGroup);
            GroupCache infos = GetGroupCache(type);
            if (infos == null) return;
            ArchetypeDataList local;
            foreach (var item in _ArchetypeDatas)
            {
                local = item.Value;
                if (local.Archetype.FullMatch(infos.BitMap))
                {
                    local.Foreach(infos.MemberInfos, foreachFunc, ref index);
                }
            }
        }
        /// <summary>
        /// 执行遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="handle"></param>
        public void Foreach<TGroup>(RefHandle<TGroup> handle)
            where TGroup : struct
        {
            var type = typeof(TGroup);
            GroupCache infos = GetGroupCache(type);
            if (infos == null) return;
            ArchetypeDataList local;
            foreach (var item in _ArchetypeDatas)
            {
                local = item.Value;
                if (local.Archetype.FullMatch(infos.BitMap))
                {
                    local.Foreach(infos.MemberInfos, handle);
                }
            }
        }

        /// <summary>
        /// 获取实体数据
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="runningIndex"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public TGroup GetData<TGroup>(int index, int runningIndex)
            where TGroup : struct
        {
            var type = typeof(TGroup);
            GroupCache infos = GetGroupCache(type);
            if (infos == null) return default;
            EntityDistribution distribution = GetDistribution(index, out _);
            if (_ArchetypeDatas.TryGetValue(distribution.Archetype, out ArchetypeDataList list))
            {
                if (list.Archetype.FullMatch(infos.BitMap))
                {
                    var r = list.GetData<TGroup>(runningIndex, infos.MemberInfos);
                    return r;
                }
                else throw new InvalidCastException($"Target group {type.FullName} cannot match target archetype");
            }
            else throw new NullReferenceException("Invalid Archetype");
        }
        /// <summary>
        /// 获取实体数据
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        public TGroup GetData<TGroup>(Entity entity) where TGroup : struct => GetData<TGroup>(entity.Index, entity.RunningIndex);

        /// <summary>
        /// 写入实体数据
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="index"></param>
        /// <param name="group"></param>
        /// <param name="runningIndex"></param>
        public void WriteData<TGroup>(int index, int runningIndex, TGroup group)
            where TGroup : struct
        {
            var type = typeof(TGroup);
            GroupCache infos = GetGroupCache(type);
            if (infos == null) return;
            EntityDistribution distribution = GetDistribution(index, out _);
            if (_ArchetypeDatas.TryGetValue(distribution.Archetype, out ArchetypeDataList list))
            {
                if (list.Archetype.FullMatch(infos.BitMap))
                {
                    list.WriteData(runningIndex, infos.MemberInfos, group);
                }
                else throw new InvalidCastException($"Target group {type.FullName} cannot match target archetype");
            }
            else throw new NullReferenceException("Invalid Archetype");
        }
        /// <summary>
        /// 写入数据
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="entity"></param>
        /// <param name="group"></param>
        public void WriteData<TGroup>(Entity entity, TGroup group) where TGroup : struct => WriteData(entity.Index, entity.RunningIndex, group);

        /// <summary>
        /// 获取组信息缓存
        /// </summary>
        /// <param name="groupType"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private GroupCache GetGroupCache(Type groupType)
        {
            if (!_GroupInfoCache.TryGetValue(groupType, out GroupCache infos))
            {
                var interFaceType = typeof(IComponentData);
                ArchetypeBitMap bitMap = ArchetypeBitMap.Zero;
                MemberInfo[] memberInfos = groupType.GetMembers().Where(x => x.MemberType == MemberTypes.Field || x.MemberType == MemberTypes.Property).ToArray();
                List<MemberInfo> result = new List<MemberInfo>(memberInfos.Length);
                for (int i = 0; i < memberInfos.Length; i++)
                {
                    MemberInfo local = memberInfos[i];
                    Type lType = local.MemberType == MemberTypes.Field ? (local as FieldInfo).FieldType : (local as PropertyInfo).PropertyType;
                    if (ArchetypeManager.TryGetComponentInfo(lType, out ComponentTypeInfo info))
                    {
                        bitMap |= info.BitMap;
                        result.Add(local);
                    }
                }
                if (bitMap == ArchetypeBitMap.Zero) return null;
                infos = new GroupCache(groupType, result.ToArray(), bitMap);
                _GroupInfoCache.AddOrUpdate(groupType, infos, (key, value) => value);
            }
            return infos;

        }

        /// <summary>
        /// 获取指定下标的实体分布
        /// </summary>
        /// <param name="index"></param>
        /// <param name="targetIndex"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private EntityDistribution GetDistribution(int index, out int targetIndex)
        {
            int count = _Distribution.Count;
            int left = 0;
            int right = count - 1;
            int midd = (left + right) / 2;
            return GetDistribution(index, count, left, right, midd, out targetIndex);
        }
        /// <summary>
        /// 获取指定下标的实体分布
        /// </summary>
        /// <param name="index"></param>
        /// <param name="midd"></param>
        /// <param name="targetIndex"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private EntityDistribution GetDistribution(int midd, int index, out int targetIndex)
        {
            int count = _Distribution.Count;
            int left = 0;
            int right = count - 1;
            return GetDistribution(index, count, left, right, midd, out targetIndex);
        }
        /// <summary>
        /// 获取指定下标的实体分布
        /// </summary>
        /// <param name="index"></param>
        /// <param name="midd"></param>
        /// <param name="targetIndex"></param>
        /// <param name="count"></param>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private EntityDistribution GetDistribution(int index, int count, int left, int right, int midd, out int targetIndex)
        {
            var distribution = _Distribution[midd];
            targetIndex = midd;
            bool found = false;
            while (!found)
            {
                if (index >= distribution.StartIndex && index <= distribution.EndIndex)
                {
                    found = true;
                    targetIndex = midd;
                }
                else if (index < distribution.StartIndex)
                {
                    right = midd - 1;
                    if (right < 0) throw new NullReferenceException($"Cannot found entity with index : {index}");
                    midd = (left + right) / 2;
                    distribution = _Distribution[midd];
                }
                else if (index > distribution.EndIndex)
                {
                    left = midd + 1;
                    if (left >= count) throw new NullReferenceException($"Cannot found entity with index : {index}");
                    midd = (left + right) / 2;
                    distribution = _Distribution[midd];
                }
            }
            return distribution;
        }
        /// <summary>
        /// 设置实体分布
        /// </summary>
        /// <param name="archetype"></param>
        /// <param name="startIndex"></param>
        /// <param name="endIndex"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void SetDistribution(Archetype archetype, int startIndex, int endIndex)
        {
            EntityDistribution distribution;
            if (_Distribution.Any())
            {
                distribution = _Distribution[^1];
                if (distribution.Archetype == archetype.BitMap && distribution.EndIndex == startIndex)
                {
                    distribution.EndIndex = endIndex;
                    _Distribution[^1] = distribution;
                }
                else
                {
                    distribution = new EntityDistribution() { Archetype = archetype.BitMap, StartIndex = startIndex, EndIndex = endIndex };
                    _Distribution.Add(distribution);
                }
            }
            else
            {
                distribution = new EntityDistribution() { Archetype = archetype.BitMap, StartIndex = startIndex, EndIndex = endIndex };
                _Distribution.Add(distribution);
            }
        }

        /// <summary>
        /// 释放资源
        /// </summary>
        public void Dispose()
        {
            if (!_IsDispose)
            {
                _IsDispose = true;
                foreach(var item in _ArchetypeDatas)
                {
                    item.Value.Dispose();
                }
                _ArchetypeDatas.Clear();
                _GroupInfoCache.Clear();
                _Distribution.Clear();
            }
        }
    }
}
