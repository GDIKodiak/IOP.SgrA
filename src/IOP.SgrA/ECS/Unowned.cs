﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.ECS
{
    /// <summary>
    /// 无主组件
    /// </summary>
    public struct Unowned : IComponentData
    {
        /// <summary>
        /// 
        /// </summary>
        public int State;
    }
}
