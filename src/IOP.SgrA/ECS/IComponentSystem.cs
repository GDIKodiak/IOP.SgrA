﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.ECS
{
    /// <summary>
    /// 组件接口
    /// </summary>
    public interface IComponentSystem
    {
        /// <summary>
        /// 
        /// </summary>
        bool IsInit { get; }
        /// <summary>
        /// 初始化
        /// </summary>
        void Initialization();
        /// <summary>
        /// 更新
        /// </summary>
        void Update(TimeSpan lastStamp);
    }
}
