﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.ECS
{
    /// <summary>
    /// 实体分布
    /// </summary>
    public struct EntityDistribution
    {
        /// <summary>
        /// 开始下标
        /// </summary>
        public int StartIndex { get; set; }
        /// <summary>
        /// 结束下标
        /// </summary>
        public int EndIndex { get; set; }
        /// <summary>
        /// 实体原型
        /// </summary>
        public ArchetypeBitMap Archetype { get; set; }
    }
}
