﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.ECS
{
    /// <summary>
    /// 原型
    /// </summary>
    public class Archetype
    {
        /// <summary>
        /// 原型名称
        /// </summary>
        public string Name { get; internal set; }
        /// <summary>
        /// 比特字典
        /// </summary>
        public ArchetypeBitMap BitMap { get; internal set; }
        /// <summary>
        /// 组件类型
        /// </summary>
        public List<ComponentTypeInfo> ComponentTypes { get; internal set; } = new List<ComponentTypeInfo>();
        /// <summary>
        /// 单个原型所有组件在内存中的总偏移量
        /// </summary>
        public int ArchetypeOffest { get; internal set; }

        /// <summary>
        /// 判断比较者的Bitmap是否为此原型Bitmap的非空子集
        /// </summary>
        /// <param name="comparer"></param>
        /// <returns></returns>
        public bool FullMatch(ArchetypeBitMap comparer)
        {
            return ArchetypeBitMap.FullMatch(BitMap, comparer);
        }
    }
}
