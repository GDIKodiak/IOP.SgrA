﻿using System;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;

namespace IOP.SgrA.ECS
{
    /// <summary>
    /// 原型比特字典
    /// </summary>
    public struct ArchetypeBitMap
    {
        /// <summary>
        /// 左偏移
        /// </summary>
        public int LeftOffset { get; private set; }
        /// <summary>
        /// 比特数组
        /// </summary>
        public long[] Bits { get; private set; }

        private static readonly ArchetypeBitMap _Zero = new ArchetypeBitMap(0);
        private const long OffsetBasis = 2166136261L;
        private const long Prime = 16777619L;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="offset"></param>
        public ArchetypeBitMap(int offset)
        {
            LeftOffset = offset;
            offset -= 1;
            if(offset < 0)
            {
                Bits = Array.Empty<long>();
            }
            else
            {
                int l = offset % GetLongBits();
                var length = offset / GetLongBits() + 1;
                Bits = new long[length];
                long last = (long)1 << l;
                Bits[length - 1] = last;
            }
        }

        /// <summary>
        /// 按位与
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static ArchetypeBitMap operator &(ArchetypeBitMap a, ArchetypeBitMap b)
        {
            if (a.LeftOffset == 0 && b.LeftOffset == 0) return Zero;
            var min = Math.Min(a.LeftOffset - 1, b.LeftOffset - 1);
            var length = min < 0 ? 0 : min / GetLongBits() + 1;
            long[] newBits = new long[length];
            long[] aBits = a.Bits;
            long[] bBits = b.Bits;
            int zeroOffset = 0;
            if (Avx2.IsSupported)
            {
                for(int i = 0; i < length; i += 4)
                {
                    if(length >= 4 && (i + 3) < length)
                    {
                        Vector256<long> x1 = Vector256.Create(aBits[i], aBits[i + 1], aBits[i + 2], aBits[i + 3]);
                        Vector256<long> x2 = Vector256.Create(bBits[i], bBits[i + 1], bBits[i + 2], bBits[i + 3]);
                        Vector256<long> r = Avx2.And(x1, x2);
                        newBits[i] = r.GetElement(0); 
                        newBits[i + 1] = r.GetElement(1);
                        newBits[i + 2] = r.GetElement(2);
                        newBits[i + 3] = r.GetElement(3);
                        if (newBits[i] == 0) zeroOffset++;
                        else zeroOffset = 0;
                        if (newBits[i + 1] == 0) zeroOffset++;
                        else zeroOffset = 0;
                        if (newBits[i + 2] == 0) zeroOffset++;
                        else zeroOffset = 0;
                        if (newBits[i + 3] == 0) zeroOffset++;
                        else zeroOffset = 0;
                    }
                    else
                    {
                        int s = length - i;
                        for(int j = 0; j < s; j++)
                        {
                            newBits[i + j] = aBits[i + j] & bBits[i + j];
                            if (newBits[i + j] == 0) zeroOffset++;
                            else zeroOffset = 0;
                        }
                    }
                }
            }
            else
            {
                for(int i = 0; i < length; i++)
                {
                    newBits[i] = aBits[i] & bBits[i];
                    if (newBits[i] == 0) zeroOffset++;
                    else zeroOffset = 0;
                }
            }
            int finalLength = length - zeroOffset;
            if(finalLength != length)
            {
                if (finalLength == 0) return Zero;
                else
                {
                    long[] c = new long[finalLength];
                    Span<long> copyA = c;
                    Span<long> copyB = newBits;
                    copyB[..finalLength].CopyTo(copyA);
                    newBits = c;
                    length = finalLength;
                    min = length * GetLongBits();
                }
            }
            ArchetypeBitMap rBitmap = new ArchetypeBitMap();
            rBitmap.LeftOffset = min + 1;
            rBitmap.Bits = newBits;
            return rBitmap;
        }
        /// <summary>
        /// 按位或
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static ArchetypeBitMap operator |(ArchetypeBitMap a, ArchetypeBitMap b)
        {
            if (a.LeftOffset == 0 && b.LeftOffset == 0) return Zero;
            var max = Math.Max(a.LeftOffset - 1, b.LeftOffset - 1);
            var min = Math.Min(a.LeftOffset - 1, b.LeftOffset - 1);
            var minLength = min < 0 ? 0 : min / GetLongBits() + 1;
            var maxLength = max < 0 ? 0 : max / GetLongBits() + 1;
            long[] newBits = new long[maxLength];
            long[] aBits = a.Bits;
            long[] bBits = b.Bits;
            int zeroOffset = 0;
            if (Avx2.IsSupported)
            {
                for (int i = 0; i < minLength; i += 4)
                {
                    if (minLength >= 4 && (i + 3) < minLength)
                    {
                        Vector256<long> x1 = Vector256.Create(aBits[i], aBits[i + 1], aBits[i + 2], aBits[i + 3]);
                        Vector256<long> x2 = Vector256.Create(bBits[i], bBits[i + 1], bBits[i + 2], bBits[i + 3]);
                        Vector256<long> r = Avx2.Or(x1, x2);
                        newBits[i] = r.GetElement(0);
                        newBits[i + 1] = r.GetElement(1);
                        newBits[i + 2] = r.GetElement(2);
                        newBits[i + 3] = r.GetElement(3);
                        if (newBits[i] == 0) zeroOffset++;
                        else zeroOffset = 0;
                        if (newBits[i + 1] == 0) zeroOffset++;
                        else zeroOffset = 0;
                        if (newBits[i + 2] == 0) zeroOffset++;
                        else zeroOffset = 0;
                        if (newBits[i + 3] == 0) zeroOffset++;
                        else zeroOffset = 0;
                    }
                    else
                    {
                        int s = minLength - i;
                        for (int j = 0; j < s; j++)
                        {
                            newBits[i + j] = aBits[i + j] | bBits[i + j];
                            if (newBits[i + j] == 0) zeroOffset++;
                            else zeroOffset = 0;
                        }
                    }
                }
                for (int j = minLength; j < maxLength; j++)
                {
                    newBits[j] = a.LeftOffset <= b.LeftOffset ? bBits[j] : aBits[j];
                    if (newBits[j] == 0) zeroOffset++;
                    else zeroOffset = 0;
                }
            }
            else
            {
                for(int i = 0; i < minLength; i++)
                {
                    newBits[i] = aBits[i] | bBits[i];
                    if (newBits[i] == 0) zeroOffset++;
                    else zeroOffset = 0;
                }
                for(int j = minLength; j < maxLength; j++)
                {
                    newBits[j] = a.LeftOffset <= b.LeftOffset ? bBits[j] : aBits[j];
                    if (newBits[j] == 0) zeroOffset++;
                    else zeroOffset = 0;
                }
            }
            int finalLength = maxLength - zeroOffset;
            if (finalLength != maxLength)
            {
                if (finalLength == 0) return Zero;
                else
                {
                    long[] c = new long[finalLength];
                    Span<long> copyA = c;
                    Span<long> copyB = newBits;
                    copyB[..finalLength].CopyTo(copyA);
                    newBits = c;
                    maxLength = finalLength;
                    max = maxLength * GetLongBits();
                }
            }
            ArchetypeBitMap rBitmap = new ArchetypeBitMap();
            rBitmap.LeftOffset = max + 1;
            rBitmap.Bits = newBits;
            return rBitmap;
        }

        /// <summary>
        /// 相等
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator == (ArchetypeBitMap a, ArchetypeBitMap b)
        {
            if (a.LeftOffset != b.LeftOffset) return false;
            var min = a.LeftOffset - 1;
            var minLength = min < 0 ? 0 : min / GetLongBits() + 1;
            long[] aBits = a.Bits;
            long[] bBits = b.Bits;
            if (Avx2.IsSupported)
            {
                for (int i = 0; i < minLength; i += 4)
                {
                    if (minLength >= 4 && (i + 3) < minLength)
                    {
                        Vector256<long> x1 = Vector256.Create(aBits[i], aBits[i + 1], aBits[i + 2], aBits[i + 3]);
                        Vector256<long> x2 = Vector256.Create(bBits[i], bBits[i + 1], bBits[i + 2], bBits[i + 3]);
                        Vector256<long> r = Avx2.CompareEqual(x1, x2);
                        long l0 = r.GetElement(0); long l1 = r.GetElement(1); long l2 = r.GetElement(2); long l3 = r.GetElement(3);
                        if (l0 == 0 || l1 == 0 || l2 == 0 || l3 == 0) return false;
                    }
                    else
                    {
                        int s = minLength - i;
                        for (int j = 0; j < s; j++)
                        {
                            if (aBits[i + j] != bBits[i + j]) return false;
                        }
                    }
                }
                return true;
            }
            else
            {
                for (int i = 0; i < minLength; i++)
                {
                    if (aBits[i] != bBits[i]) return false;
                }
                return true;
            }
        }
        /// <summary>
        /// 不等
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator !=(ArchetypeBitMap a, ArchetypeBitMap b) => !(a == b);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            return this == (ArchetypeBitMap)obj;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            if (LeftOffset <= 0) return int.MinValue;
            long hash = OffsetBasis;
            long prime = Prime;
            for(int i = 0; i < Bits.Length; i++)
            {
                hash *= prime;
                hash ^= Bits[i];
            }
            return (int)hash;
        }

        /// <summary>
        /// 匹配
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool Match(ArchetypeBitMap a, ArchetypeBitMap b)
        {
            var min = Math.Min(a.LeftOffset - 1, b.LeftOffset - 1);
            var length = min < 0 ? 0 : min / GetLongBits() + 1;
            long[] aBits = a.Bits;
            long[] bBits = b.Bits;
            if (Avx2.IsSupported)
            {
                for (int i = 0; i < length; i += 4)
                {
                    if (length >= 4 && (i + 3) < length)
                    {
                        Vector256<long> x1 = Vector256.Create(aBits[i], aBits[i + 1], aBits[i + 2], aBits[i + 3]);
                        Vector256<long> x2 = Vector256.Create(bBits[i], bBits[i + 1], bBits[i + 2], bBits[i + 3]);
                        Vector256<int> lr = Avx2.And(x1, x2).AsInt32();
                        lr = Avx2.HorizontalAdd(lr, lr);
                        lr = Avx2.HorizontalAdd(lr, lr);
                        lr = Avx2.HorizontalAdd(lr, lr);
                        if (lr.GetElement(0) != 0) return true;
                    }
                    else
                    {
                        int s = length - i;
                        for (int j = 0; j < s; j++)
                        {
                            long lr = aBits[i + j] & bBits[i + j];
                            if (lr != 0) return true;
                        }
                    }
                }
                return false;
            }
            else
            {
                for (int i = 0; i < length; i++)
                {
                    long lr = aBits[i] & bBits[i];
                    if (lr != 0) return true;
                }
                return false;
            }
        }
        /// <summary>
        /// 判断BitmapB是否为BitmapA的非空子集
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool FullMatch(ArchetypeBitMap a, ArchetypeBitMap b)
        {
            if (a.LeftOffset < b.LeftOffset) return false;
            var min = Math.Min(a.LeftOffset - 1, b.LeftOffset - 1);
            var length = min < 0 ? 0 : min / GetLongBits() + 1;
            long[] aBits = a.Bits;
            long[] bBits = b.Bits;
            if (Avx2.IsSupported)
            {
                for (int i = 0; i < length; i += 4)
                {
                    if (length >= 4 && (i + 3) < length)
                    {
                        Vector256<long> x1 = Vector256.Create(aBits[i], aBits[i + 1], aBits[i + 2], aBits[i + 3]);
                        Vector256<long> x2 = Vector256.Create(bBits[i], bBits[i + 1], bBits[i + 2], bBits[i + 3]);
                        Vector256<long> r = Avx2.Or(x1, x2);
                        r = Avx2.CompareEqual(x1, r);
                        long l0 = r.GetElement(0); long l1 = r.GetElement(1);
                        long l2 = r.GetElement(2); long l3 = r.GetElement(3);
                        if (l0 == 0 || l1 == 0 || l2 == 0 || l3 == 0) return false;
                    }
                    else
                    {
                        int s = length - i;
                        for (int j = 0; j < s; j++)
                        {
                            long lr = aBits[i + j] | bBits[i + j];
                            if (lr != aBits[i + j]) return false;
                        }
                    }
                }
                return true;
            }
            else
            {
                for (int i = 0; i < length; i++)
                {
                    long lr = aBits[i] | bBits[i];
                    if (lr != aBits[i]) return false;
                }
                return true;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public static ArchetypeBitMap Zero => _Zero;

        /// <summary>
        /// 获取长整形比特数
        /// </summary>
        private static int GetLongBits() => (sizeof(long) * 8);
    }
}
