﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace IOP.SgrA.ECS
{
    /// <summary>
    /// 实体管理器接口
    /// </summary>
    public interface IEntityManager : IDisposable
    {
        /// <summary>
        /// 添加组件程序集
        /// </summary>
        /// <param name="assembly"></param>
        void AddComponentAssembly(Assembly assembly);
        /// <summary>
        /// 加载组件
        /// </summary>
        void LoadComponents();
        /// <summary>
        /// 获取一个用于渲染目标的原型
        /// </summary>
        /// <returns></returns>
        Archetype RenderTargetArchetype();
        /// <summary>
        /// 获取一个无主的原型
        /// </summary>
        /// <returns></returns>
        Archetype UnownedArchetype();
        /// <summary>
        /// 创建原型
        /// </summary>
        /// <param name="components"></param>
        /// <returns></returns>
        Archetype CreateArchetype(params IComponentData[] components);
        /// <summary>
        /// 创建原型
        /// </summary>
        /// <param name="name"></param>
        /// <param name="components"></param>
        /// <returns></returns>
        Archetype CreateArchetype(string name, params IComponentData[] components);
        /// <summary>
        /// 从指定结构体创建原型
        /// </summary>
        /// <typeparam name="TComponentGorup"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        Archetype CreateArchetype<TComponentGorup>(string name = "") where TComponentGorup : struct;
        /// <summary>
        /// 尝试获取原型
        /// </summary>
        /// <param name="name"></param>
        /// <param name="archetype"></param>
        /// <returns></returns>
        bool TryGetArchetype(string name, out Archetype archetype);
        /// <summary>
        /// 创建实体
        /// </summary>
        /// <param name="archetype"></param>
        /// <param name="size"></param>
        /// <param name="index"></param>
        void CreateEntities(Archetype archetype, int size, ref int index);
        /// <summary>
        /// 创建实体
        /// </summary>
        /// <param name="archetype"></param>
        /// <param name="size"></param>
        /// <param name="index"></param>
        /// <param name="entities"></param>
        void CreateEntities(Archetype archetype, int size, ref int index, out Entity[] entities);
        /// <summary>
        /// 创建实体
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="archetype"></param>
        /// <param name="size"></param>
        /// <param name="index"></param>
        /// <param name="initData"></param>
        void CreateEntities<TGroup>(Archetype archetype, int size, ref int index, TGroup initData) where TGroup : struct;
        /// <summary>
        /// 创建实体
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="archetype"></param>
        /// <param name="size"></param>
        /// <param name="index"></param>
        /// <param name="initData"></param>
        /// <param name="entities"></param>
        void CreateEntities<TGroup>(Archetype archetype, int size, ref int index, TGroup initData, out Entity[] entities) where TGroup : struct;
        /// <summary>
        /// 回收实体
        /// </summary>
        /// <param name="entity"></param>
        void RecoveryEntity(Entity entity);
        /// <summary>
        /// 回收实体
        /// </summary>
        /// <param name="entities"></param>
        void RecoveryEntities(Entity[] entities);
        /// <summary>
        /// 遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="foreachAction"></param>
        void Foreach<TGroup>(Action<TGroup> foreachAction) where TGroup : struct;
        /// <summary>
        /// 遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="foreachAction"></param>
        void Foreach<TGroup>(Action<TGroup, int> foreachAction) where TGroup : struct;
        /// <summary>
        /// 遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="foreachFunc"></param>
        void Foreach<TGroup>(Func<TGroup> foreachFunc) where TGroup : struct;
        /// <summary>
        /// 遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="foreachFunc"></param>
        void Foreach<TGroup>(Func<int, TGroup> foreachFunc) where TGroup : struct;
        /// <summary>
        /// 遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="handle"></param>
        void Foreach<TGroup>(RefHandle<TGroup> handle) where TGroup : struct;
        /// <summary>
        /// 遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="foreachFunc"></param>
        void Foreach<TGroup>(Func<TGroup, TGroup> foreachFunc) where TGroup : struct;
        /// <summary>
        /// 遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="foreachFunc"></param>
        void Foreach<TGroup>(Func<TGroup, int, TGroup> foreachFunc) where TGroup : struct;
        /// <summary>
        /// 获取指定Entity数据
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        TGroup GetData<TGroup>(Entity entity) where TGroup : struct;
        /// <summary>
        /// 向指定Entity写入数据
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="entity"></param>
        /// <param name="group"></param>
        void WriteData<TGroup>(Entity entity, TGroup group) where TGroup : struct;
    }
}
