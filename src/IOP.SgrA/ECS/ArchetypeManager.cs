﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Linq.Expressions;

namespace IOP.SgrA.ECS
{
    /// <summary>
    /// 原型管理器
    /// </summary>
    public class ArchetypeManager
    {
        /// <summary>
        /// 原型列表
        /// </summary>
        public readonly ConcurrentDictionary<ArchetypeBitMap, Archetype> Archetypes = new ConcurrentDictionary<ArchetypeBitMap, Archetype>();
        /// <summary>
        /// 名称映射表
        /// </summary>
        private readonly ConcurrentDictionary<string, ArchetypeBitMap> _NameMapper = new ConcurrentDictionary<string, ArchetypeBitMap>();
        /// <summary>
        /// 组件类型字典
        /// </summary>
        private ConcurrentDictionary<Type, ComponentTypeInfo> _ComponentTypes { get; } = new ConcurrentDictionary<Type, ComponentTypeInfo>();
        /// <summary>
        /// 程序集列表
        /// </summary>
        private readonly ConcurrentBag<Assembly> _Assemblies = new ConcurrentBag<Assembly>();
        private const int MAXCOMPONENTS = 256;
        private const int BLOCKSIZE = 16 * 1024;
        private int numberOffest = 1;
        private static readonly int entitySizeof = Marshal.SizeOf<Entity>();
        private static readonly int unownedSizeof = Marshal.SizeOf<Unowned>();
        private static readonly ComponentTypeInfo entityTypeInfo = new ComponentTypeInfo(typeof(Entity), entitySizeof, ArchetypeBitMap.Zero);
        private static readonly ComponentTypeInfo unownedTyleInfo = new ComponentTypeInfo(typeof(Unowned), unownedSizeof, new ArchetypeBitMap(1));
        private static SpinLock _SpinLock = new SpinLock(true);
        /// <summary>
        /// 构造函数
        /// </summary>
        public ArchetypeManager()
        {
            _ComponentTypes.AddOrUpdate(typeof(Entity), entityTypeInfo, (key, value) => value);
            _ComponentTypes.AddOrUpdate(typeof(Unowned), unownedTyleInfo, (key, value) => value);
        }
        /// <summary>
        /// 添加组件程序集
        /// </summary>
        /// <param name="assembly"></param>
        public void AddComponentAssembly(Assembly assembly) => _Assemblies.Add(assembly);
        /// <summary>
        /// 加载组件
        /// </summary>
        public void LoadComponents()
        {
            try
            {
                foreach (var assembly in _Assemblies)
                {
                    var types = assembly.GetTypes();
                    var faces = typeof(IComponentData);
                    foreach (var type in types)
                    {
                        var info = CreateNewComponentInfo(type);
                        if (info != null) AddComponentTypeInfo(info);
                    }
                }
                _Assemblies.Clear();
            }
            catch (Exception)
            {

                throw;
            }
        }
        /// <summary>
        /// 添加组件信息
        /// </summary>
        /// <param name="info"></param>
        public void AddComponentTypeInfo(ComponentTypeInfo info) => _ComponentTypes.AddOrUpdate(info.Type, info, (key, value) => info);
        /// <summary>
        /// 获取组件信息
        /// </summary>
        /// <param name="type"></param>
        /// <param name="info"></param>
        /// <returns></returns>
        public bool TryGetComponentInfo(Type type, out ComponentTypeInfo info) => _ComponentTypes.TryGetValue(type, out info);

        /// <summary>
        /// 获取一个无主的/无用的原型
        /// </summary>
        /// <returns></returns>
        public Archetype UnownedArchetype()
        {
            if (TryGetArchetype(nameof(Unowned), out Archetype archetype))
            {
                return archetype;
            }
            archetype = CreateArchetype(out _, new Unowned());
            return archetype;
        }
        /// <summary>
        /// 创建原型
        /// </summary>
        /// <param name="components"></param>
        /// <param name="isHave"></param>
        /// <returns></returns>
        public Archetype CreateArchetype(out bool isHave, params IComponentData[] components)
        {
            var name = Guid.NewGuid().ToString("N");
            return CreateArchetype(name, out isHave, components);
        }
        /// <summary>
        /// 创建原型
        /// </summary>
        /// <param name="name"></param>
        /// <param name="isHave"></param>
        /// <param name="components"></param>
        /// <returns></returns>
        public Archetype CreateArchetype(string name, out bool isHave, params IComponentData[] components)
        {
            isHave = true;
            ArchetypeBitMap bitMap = ArchetypeBitMap.Zero;
            Archetype r = new Archetype();
            bool getLock = false;
            try
            {
                _SpinLock.Enter(ref getLock);
                r.ComponentTypes.Add(entityTypeInfo);
                int finalOffest = entitySizeof;
                foreach (var item in components)
                {
                    var type = item.GetType();
                    if (type == entityTypeInfo.Type) continue;
                    if (TryGetComponentInfo(type, out ComponentTypeInfo componentInfo))
                    {
                        if ((componentInfo.BitMap & bitMap) == ArchetypeBitMap.Zero)
                        {
                            bitMap |= componentInfo.BitMap;
                            r.ComponentTypes.Add(componentInfo);
                            finalOffest += componentInfo.Offest;
                        }
                    }
                    else
                    {
                        var newComponent = CreateNewComponentInfo(type);
                        if (newComponent == null) continue;
                        else
                        {
                            bitMap |= newComponent.BitMap;
                            r.ComponentTypes.Add(newComponent);
                            finalOffest += newComponent.Offest;
                            AddComponentTypeInfo(newComponent);
                        }
                    }
                    if (finalOffest > BLOCKSIZE) throw new IndexOutOfRangeException($"Target component's total size is out of max block size: {BLOCKSIZE}, this archetype is too big");
                }
                if (Archetypes.TryGetValue(bitMap, out Archetype archetype))
                {
                    r = archetype;
                    return r;
                }
                else
                {
                    r.ArchetypeOffest = finalOffest;
                    r.BitMap = bitMap;
                    r.Name = name;
                    Archetypes.AddOrUpdate(bitMap, r, (key, value) => r);
                    _NameMapper.AddOrUpdate(name, bitMap, (key, value) => bitMap);
                    isHave = false;
                    return r;
                }
            }
            finally
            {
                if (getLock) _SpinLock.Exit();
            }
        }
        /// <summary>
        /// 通过特定类型创建原型
        /// </summary>
        /// <typeparam name="TComponentGorup"></typeparam>
        /// <param name="name"></param>
        /// <param name="isHave"></param>
        /// <returns></returns>
        /// <exception cref="IndexOutOfRangeException"></exception>
        public Archetype CreateArchetype<TComponentGorup>(string name, out bool isHave)
        {
            isHave = true;
            ArchetypeBitMap bitMap = ArchetypeBitMap.Zero;
            Archetype r = new Archetype();
            bool getLock = false;
            name = string.IsNullOrEmpty(name) ? Guid.NewGuid().ToString("N") : name;
            try
            {
                _SpinLock.Enter(ref getLock);
                r.ComponentTypes.Add(entityTypeInfo);
                int finalOffest = entitySizeof;
                var type = typeof(TComponentGorup);
                foreach(var item in type.GetMembers(BindingFlags.Public | BindingFlags.Instance))
                {
                    Type fType = null;
                    if (item.MemberType.HasFlag(MemberTypes.Field))
                    {
                        fType = (item as FieldInfo).FieldType;
                    }
                    else if (item.MemberType.HasFlag(MemberTypes.Property))
                    {
                        fType = (item as PropertyInfo).PropertyType;
                    }
                    else continue;
                    if (fType == entityTypeInfo.Type) continue;
                    if (TryGetComponentInfo(fType, out ComponentTypeInfo componentInfo))
                    {
                        if((componentInfo.BitMap & bitMap) == ArchetypeBitMap.Zero)
                        {
                            bitMap |= componentInfo.BitMap;
                            r.ComponentTypes.Add(componentInfo);
                            finalOffest += componentInfo.Offest;
                        }
                    }
                    else
                    {
                        var newComponent = CreateNewComponentInfo(fType);
                        if (newComponent == null) continue;
                        else
                        {
                            bitMap |= newComponent.BitMap;
                            r.ComponentTypes.Add(newComponent);
                            finalOffest += newComponent.Offest;
                            AddComponentTypeInfo(newComponent);
                        }
                    }
                    if (finalOffest > BLOCKSIZE) throw new IndexOutOfRangeException($"Target component's total size is out of max block size: {BLOCKSIZE}, this archetype is too big");
                }
                if(Archetypes.TryGetValue(bitMap, out Archetype archetype))
                {
                    r = archetype;
                    return r;
                }
                else
                {
                    r.ArchetypeOffest = finalOffest;
                    r.BitMap = bitMap;
                    r.Name = name;
                    Archetypes.AddOrUpdate(bitMap, r, (key, value) => r);
                    _NameMapper.AddOrUpdate(name, bitMap, (key, value) => bitMap);
                    isHave = false;
                    return r;
                }
            }
            finally
            {
                if (getLock) _SpinLock.Exit();
            }
        }
        /// <summary>
        /// 尝试获取原型
        /// </summary>
        /// <param name="name"></param>
        /// <param name="archetype"></param>
        /// <returns></returns>
        public bool TryGetArchetype(string name, out Archetype archetype)
        {
            if(_NameMapper.TryGetValue(name, out ArchetypeBitMap bitMap))
            {
                if(Archetypes.TryGetValue(bitMap, out archetype))
                {
                    return true;
                }
            }
            archetype = null;
            return false;
        }

        /// <summary>
        /// 创建新的组件信息
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private ComponentTypeInfo CreateNewComponentInfo(Type type)
        {
            if (type.GetInterfaces().Any(x => x == typeof(IComponentData)) && type.IsValueType)
            {
                if (StructTypeCheck(type)) throw new NotSupportedException($"Component with name {type.FullName} is contains references in struct");
                var map = InitBitMap();
                var offest = Marshal.SizeOf(type);
                ComponentTypeInfo info = new ComponentTypeInfo(type, offest, map);
                return info;
            }
            else return null;
        }

        /// <summary>
        /// 初始化比特字典
        /// </summary>
        /// <returns></returns>
        private ArchetypeBitMap InitBitMap()
        {
            int offset = Interlocked.Increment(ref numberOffest);
            return new ArchetypeBitMap(offset);
        }
        /// <summary>
        /// 结构体类型检查
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private bool StructTypeCheck(Type type)
        {
            var runtimeType = typeof(RuntimeHelpers);
            MethodInfo info = runtimeType.GetMethod("IsReferenceOrContainsReferences");
            info = info.MakeGenericMethod(type);
            return (bool)info.Invoke(null, null);
        }
    }
}
