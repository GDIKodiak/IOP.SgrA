﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.ECS
{
    /// <summary>
    /// 外键实体
    /// </summary>
    public struct ForeignEntity : IComponentData
    {
        /// <summary>
        /// 父实体
        /// </summary>
        public Entity Parent;
        /// <summary>
        /// 子实体
        /// </summary>
        public Entity Children;
    }
}
