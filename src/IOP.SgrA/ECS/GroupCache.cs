﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace IOP.SgrA.ECS
{
    /// <summary>
    /// 查询组缓存
    /// </summary>
    public class GroupCache
    {
        /// <summary>
        /// 组类型
        /// </summary>
        public Type GroupType { get; }
        /// <summary>
        /// 组成员信息
        /// </summary>
        public MemberInfo[] MemberInfos { get; }
        /// <summary>
        /// bit字典
        /// </summary>
        public ArchetypeBitMap BitMap { get; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="type"></param>
        /// <param name="infos"></param>
        /// <param name="bitMap"></param>
        public GroupCache(Type type, MemberInfo[] infos, ArchetypeBitMap bitMap)
        {
            GroupType = type;
            MemberInfos = infos;
            BitMap = bitMap;
        }
    }
}
