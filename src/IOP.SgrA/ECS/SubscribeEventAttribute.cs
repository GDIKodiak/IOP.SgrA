﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.CompilerServices;

namespace IOP.SgrA.ECS
{
    /// <summary>
    /// 订阅事件标签
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class SubscribeEventAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        public EventTrigger EventTrigger { get; } = EventTrigger.SystemFrameAfter;
        /// <summary>
        /// 主题
        /// </summary>
        public string Topic { get; } = string.Empty;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="trigger"></param>
        public SubscribeEventAttribute(string topic, EventTrigger trigger)
        {
            Topic = string.IsNullOrEmpty(topic) ? string.Empty : topic;
            EventTrigger = trigger;
        }
    }
}
