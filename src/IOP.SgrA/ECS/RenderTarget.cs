﻿using IOP.SgrA.ECS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.ECS
{
    /// <summary>
    /// 渲染目标组件
    /// </summary>
    public struct RenderTargetComponent : IComponentData
    {
        /// <summary>
        /// 
        /// </summary>
        public int State;
    }
    /// <summary>
    /// 渲染目标
    /// </summary>
    public struct RenderTarget
    {
        /// <summary>
        /// 
        /// </summary>
        public Entity Entity;
        /// <summary>
        /// 
        /// </summary>
        public RenderTargetComponent Target;
    }
}
