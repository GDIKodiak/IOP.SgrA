﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace IOP.SgrA.ECS
{
    /// <summary>
    /// 
    /// </summary>
    public static class MemoryHelper
    {
        /// <summary>
        /// 写入结构体至内存
        /// </summary>
        /// <typeparam name="TStruct"></typeparam>
        /// <param name="memory"></param>
        /// <param name="struct"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        public static void WriteStruct<TStruct>(Memory<byte> memory, TStruct @struct, int startIndex, int length)
            where TStruct : struct
        {
            Span<byte> span = memory.Span;
            MemoryMarshal.Write(span.Slice(startIndex, length), ref @struct);
        }
        /// <summary>
        /// 从内存读取结构体
        /// </summary>
        /// <typeparam name="TStruct"></typeparam>
        /// <param name="memory"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static TStruct ReadStruct<TStruct>(Memory<byte> memory, int startIndex, int length)
            where TStruct : struct
        {
            Span<byte> span = memory.Span;
            TStruct @struct = MemoryMarshal.Read<TStruct>(span.Slice(startIndex, length));
            return @struct;
        }
    }
}
