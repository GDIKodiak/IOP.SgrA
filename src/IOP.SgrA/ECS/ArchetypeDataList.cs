﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq.Expressions;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Threading;

namespace IOP.SgrA.ECS
{
    /// <summary>
    /// 原型列表
    /// </summary>
    public class ArchetypeDataList : IDisposable
    {
        /// <summary>
        /// 原型
        /// </summary>
        public Archetype Archetype { get; }
        /// <summary>
        /// 元素数量
        /// </summary>
        public int Count => (int)_End.RunningIndex;
        /// <summary>
        /// 每一个块允许的实体数量
        /// </summary>
        public int ChunkEntitiesLength { get; private set; }
        /// <summary>
        /// 最大值
        /// </summary>
        const int MAXSIZE = int.MaxValue;
        /// <summary>
        /// 块大小
        /// </summary>
        const int CHUNKSIZE = 16 * 1024;
        /// <summary>
        /// 布局
        /// </summary>
        private readonly Dictionary<Type, ComponentLayout> _Layouts = new Dictionary<Type, ComponentLayout>();
        /// <summary>
        /// 查询表
        /// </summary>
        private readonly Dictionary<Type, Delegate> _QueryFuncTable = new Dictionary<Type, Delegate>();
        /// <summary>
        /// 更新表
        /// </summary>
        private readonly Dictionary<Type, Delegate> _UpdateFuncTable = new Dictionary<Type, Delegate>();
        /// <summary>
        /// 处于回收状态的实体
        /// </summary>
        private readonly Queue<Entity> _RecoveredEntities = new Queue<Entity>();
        /// <summary>
        /// 回收实体表
        /// </summary>
        private readonly Dictionary<int, Entity> _RecoveredTable = new Dictionary<int, Entity>();
        /// <summary>
        /// 数组
        /// </summary>
        private ChunkWrapper[] _Array;
        /// <summary>
        /// 首内存块
        /// </summary>
        private Chunk _First;
        /// <summary>
        /// 尾内存块
        /// </summary>
        private Chunk _End;

        /// <summary>
        /// 当前块下标
        /// </summary>
        private int _ChunkIndex;

        private int _CountdownCount = 1;
        private const int COUNTOFFEST = 512;
        private readonly Type _EntityType = typeof(Entity);

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="archetype"></param>
        public ArchetypeDataList(Archetype archetype)
            : this(archetype, 32) { }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="archetype"></param>
        /// <param name="size"></param>
        public ArchetypeDataList(Archetype archetype, int size)
        {
            if (archetype == null) throw new ArgumentNullException(nameof(archetype));
            Archetype = archetype;
            int count = CHUNKSIZE / archetype.ArchetypeOffest;
            int offest = 0;
            foreach (var item in archetype.ComponentTypes)
            {
                _Layouts.Add(item.Type, new ComponentLayout(item.Type, offest, item.Offest, count));
                offest += item.Offest * count;
            }
            ChunkEntitiesLength = count;
            _Array = new ChunkWrapper[size];
            _First = _End = new Chunk(_Layouts.Select(x => x.Value).ToArray(), ChunkEntitiesLength);
            _Array[_ChunkIndex] = _End;
        }

        /// <summary>
        /// 获取单个数据
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="runningIndex"></param>
        /// <param name="members"></param>
        /// <returns></returns>
        public TGroup GetData<TGroup>(int runningIndex, MemberInfo[] members)
            where TGroup : struct
        {
            if (runningIndex > _End.RunningIndex) throw new IndexOutOfRangeException();
            int chunkIndex = runningIndex / ChunkEntitiesLength;
            int offest = runningIndex % ChunkEntitiesLength;
            Chunk local = _Array[chunkIndex];
            Func<Chunk, int, TGroup> func = GetQueryFunc<TGroup>(members);
            return func(local, offest);
        }
        /// <summary>
        /// 获取多个数据
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="startIndex"></param>
        /// <param name="endIndex"></param>
        /// <param name="members"></param>
        /// <param name="groups"></param>
        public void GetDatas<TGroup>(int startIndex, int endIndex, MemberInfo[] members, out TGroup[] groups)
            where TGroup : struct
        {
            if (startIndex > endIndex) throw new IndexOutOfRangeException("The startIndex is greater than endIndex");
            if (endIndex > _End.RunningIndex) throw new IndexOutOfRangeException();
            Func<Chunk, int, TGroup> func = GetQueryFunc<TGroup>(members);
            int diff = endIndex - startIndex;
            groups = new TGroup[diff];
            int chunkIndex = startIndex / ChunkEntitiesLength;
            int offest = startIndex % ChunkEntitiesLength;
            Chunk local = _Array[chunkIndex];
            for (int i = 0; i < diff; i++)
            {
                groups[i] = func(local, offest++);
                if(offest >= local.Count)
                {
                    local = _Array[++chunkIndex];
                    offest = 0;
                }
            }
        }
        /// <summary>
        /// 写入数据
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="runningIndex"></param>
        /// <param name="members"></param>
        /// <param name="group"></param>
        public void WriteData<TGroup>(int runningIndex, MemberInfo[] members, TGroup group)
            where TGroup : struct
        {
            if (runningIndex > _End.RunningIndex) throw new IndexOutOfRangeException();
            int chunkIndex = runningIndex / ChunkEntitiesLength;
            int offest = runningIndex % ChunkEntitiesLength;
            Chunk local = _Array[chunkIndex];
            Action<TGroup, Chunk, int> action = GetUpdateFunc<TGroup>(members);
            action(group, local, offest);
        }
        /// <summary>
        /// 写入多个数据
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="startIndex"></param>
        /// <param name="members"></param>
        /// <param name="groups"></param>
        public void WriteDatas<TGroup>(int startIndex, MemberInfo[] members, TGroup[] groups)
        {
            int endIndex = startIndex + groups.Length;
            if (startIndex > endIndex) throw new IndexOutOfRangeException("The startIndex is greater than endIndex");
            if (endIndex > _End.RunningIndex) throw new IndexOutOfRangeException();
            int diff = endIndex - startIndex;
            Action<TGroup, Chunk, int> action = GetUpdateFunc<TGroup>(members);
            int chunkIndex = startIndex / ChunkEntitiesLength;
            int offest = startIndex % ChunkEntitiesLength;
            Chunk local = _Array[chunkIndex];
            for (int i = 0; i < diff; i++)
            {
                action(groups[i], local, offest++);
                if (offest >= local.Count)
                {
                    local = _Array[++chunkIndex];
                    offest = 0;
                }
            }
        }
        /// <summary>
        /// 遍历所有数据
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="members"></param>
        /// <param name="foreachAction"></param>
        public void Foreach<TGroup>(MemberInfo[] members, Action<TGroup> foreachAction)
            where TGroup : struct
        {
            Func<Chunk, int, TGroup> func = GetQueryFunc<TGroup>(members);
            int chunkIndex = 0;
            int offest = 0;
            int runningIndex = 0;
            Chunk local = _Array[chunkIndex];
            for (int i = 0; i < Count; i++)
            {
                if (_RecoveredTable.ContainsKey(runningIndex++))
                {
                    offest++;
                    if (offest >= local.Count)
                    {
                        local = _Array[++chunkIndex];
                        offest = 0;
                    }
                    continue;
                }
                TGroup group = func(local, offest++);
                foreachAction(group);
                if(offest >= local.Count)
                {
                    local = _Array[++chunkIndex];
                    offest = 0;
                }
            }
        }
        /// <summary>
        /// 遍历所有数据
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="members"></param>
        /// <param name="foreachFunc"></param>
        public void Foreach<TGroup>(MemberInfo[] members, Func<TGroup> foreachFunc)
            where TGroup : struct
        {
            Action<TGroup, Chunk, int> uAction = GetUpdateFunc<TGroup>(members);
            int chunkIndex = 0;
            int offest = 0;
            int runningIndex = 0;
            Chunk local = _Array[chunkIndex];
            for (int i = 0; i < Count; i++)
            {
                if (_RecoveredTable.ContainsKey(runningIndex++))
                {
                    offest++;
                    if (offest >= local.Count)
                    {
                        local = _Array[++chunkIndex];
                        offest = 0;
                    }
                    continue;
                }
                TGroup r = foreachFunc();
                uAction(r, local, offest++);
                if(offest >= local.Count)
                {
                    local = _Array[++chunkIndex];
                    offest = 0;
                }
            }
        }
        /// <summary>
        /// 遍历所有数据
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="members"></param>
        /// <param name="foreachFunc"></param>
        public void Foreach<TGroup>(MemberInfo[] members, Func<TGroup, TGroup> foreachFunc)
            where TGroup : struct
        {
            Func<Chunk, int, TGroup> qFunc = GetQueryFunc<TGroup>(members);
            Action<TGroup, Chunk, int> uAction = GetUpdateFunc<TGroup>(members);
            int chunkIndex = 0;
            int offest = 0;
            int runningIndex = 0;
            Chunk local = _Array[chunkIndex];
            for (int i = 0; i < Count; i++)
            {
                if (_RecoveredTable.ContainsKey(runningIndex++))
                {
                    offest++;
                    if (offest >= local.Count)
                    {
                        local = _Array[++chunkIndex];
                        offest = 0;
                    }
                    continue;
                }
                TGroup group = qFunc(local, offest);
                TGroup r = foreachFunc(group);
                uAction(r, local, offest++);
                if(offest >= local.Count)
                {
                    local = _Array[++chunkIndex];
                    offest = 0;
                }
            }
        }
        /// <summary>
        /// 遍历所有数据
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="members"></param>
        /// <param name="handle"></param>
        public void Foreach<TGroup>(MemberInfo[] members, RefHandle<TGroup> handle) 
            where TGroup : struct
        {
            Func<Chunk, int, TGroup> qFunc = GetQueryFunc<TGroup>(members);
            Action<TGroup, Chunk, int> uAction = GetUpdateFunc<TGroup>(members);
            int chunkIndex = 0;
            int offest = 0;
            int runningIndex = 0;
            Chunk local = _Array[chunkIndex];
            for (int i = 0; i < Count; i++)
            {
                if (_RecoveredTable.ContainsKey(runningIndex++))
                {
                    offest++;
                    if (offest >= local.Count)
                    {
                        local = _Array[++chunkIndex];
                        offest = 0;
                    }
                    continue;
                }
                TGroup group = qFunc(local, offest);
                handle(ref group);
                uAction(group, local, offest++);
                if (offest >= local.Count)
                {
                    local = _Array[++chunkIndex];
                    offest = 0;
                }
            }
        }
        /// <summary>
        /// 遍历所有数据
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="members"></param>
        /// <param name="foreachAction"></param>
        /// <param name="index"></param>
        public void Foreach<TGroup>(MemberInfo[] members, Action<TGroup, int> foreachAction, ref int index)
            where TGroup : struct
        {
            Func<Chunk, int, TGroup> func = GetQueryFunc<TGroup>(members);
            int chunkIndex = 0;
            int offest = 0;
            int runningIndex = 0;
            Chunk local = _Array[chunkIndex];
            for (int i = 0; i < Count; i++)
            {
                if (_RecoveredTable.ContainsKey(runningIndex++))
                {
                    offest++;
                    if (offest >= local.Count)
                    {
                        local = _Array[++chunkIndex];
                        offest = 0;
                    }
                    continue;
                }
                TGroup group = func(local, offest++);
                foreachAction(group, index++);
                if (offest >= local.Count)
                {
                    local = _Array[++chunkIndex];
                    offest = 0;
                }
            }
        }
        /// <summary>
        /// 遍历所有数据
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="members"></param>
        /// <param name="foreachFunc"></param>
        /// <param name="index"></param>
        public void Foreach<TGroup>(MemberInfo[] members, Func<int, TGroup> foreachFunc, ref int index)
            where TGroup : struct
        {
            Action<TGroup, Chunk, int> uAction = GetUpdateFunc<TGroup>(members);
            int chunkIndex = 0;
            int offest = 0;
            int runningIndex = 0;
            Chunk local = _Array[chunkIndex];
            for (int i = 0; i < Count; i++)
            {
                if (_RecoveredTable.ContainsKey(runningIndex++))
                {
                    offest++;
                    if (offest >= local.Count)
                    {
                        local = _Array[++chunkIndex];
                        offest = 0;
                    }
                    continue;
                }
                TGroup r = foreachFunc(index++);
                uAction(r, local, offest++);
                if (offest >= local.Count)
                {
                    local = _Array[++chunkIndex];
                    offest = 0;
                }
            }
        }
        /// <summary>
        /// 遍历所有数据
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="members"></param>
        /// <param name="foreachFunc"></param>
        /// <param name="index"></param>
        public void Foreach<TGroup>(MemberInfo[] members, Func<TGroup, int, TGroup> foreachFunc, ref int index)
        {
            Func<Chunk, int, TGroup> qFunc = GetQueryFunc<TGroup>(members);
            Action<TGroup, Chunk, int> uAction = GetUpdateFunc<TGroup>(members);
            int chunkIndex = 0;
            int offest = 0;
            int runningIndex = 0;
            Chunk local = _Array[chunkIndex];
            for (int i = 0; i < Count; i++)
            {
                if (_RecoveredTable.ContainsKey(runningIndex++))
                {
                    offest++;
                    if (offest >= local.Count)
                    {
                        local = _Array[++chunkIndex];
                        offest = 0;
                    }
                    continue;
                }
                TGroup group = qFunc(local, offest);
                TGroup r = foreachFunc(group, index++);
                uAction(r, local, offest++);
                if (offest >= local.Count)
                {
                    local = _Array[++chunkIndex];
                    offest = 0;
                }
            }
        }

        /// <summary>
        /// 并行遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="members"></param>
        /// <param name="foreachAction"></param>
        public void ParallelForeach<TGroup>(MemberInfo[] members, Action<TGroup> foreachAction)
            where TGroup : struct
        {
            Func<Chunk, int, TGroup> func = GetQueryFunc<TGroup>(members);
            using var countDown = new CountdownEvent(_CountdownCount);
            for (int i = 0; i < _CountdownCount; i++)
            {
                ValueTask valueTask = new ValueTask(Task.Factory.StartNew((index) =>
                {
                    int l = (int)index;
                    int runningIndex = l * ChunkEntitiesLength * COUNTOFFEST;
                    Chunk local = _Array[l * COUNTOFFEST];
                    for (int r = 0; r < COUNTOFFEST; r++)
                    {
                        if (local == null) break;
                        for (int i = 0; i < local.Count; i++)
                        {
                            if (_RecoveredTable.ContainsKey(runningIndex++)) continue;
                            TGroup group = func(local, i);
                            foreachAction(group);
                        }
                        local = local.Next;
                    }
                    countDown.Signal();
                }, i));
            }
            countDown.Wait();
        }
        /// <summary>
        /// 并行遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="members"></param>
        /// <param name="foreachFunc"></param>
        public void ParallelForeach<TGroup>(MemberInfo[] members, Func<TGroup> foreachFunc)
        {
            Action<TGroup, Chunk, int> uAction = GetUpdateFunc<TGroup>(members);
            using var countDown = new CountdownEvent(_CountdownCount);
            for (int i = 0; i < _CountdownCount; i++)
            {
                ValueTask valueTask = new ValueTask(Task.Factory.StartNew((index) =>
                {
                    int l = (int)index;
                    int runningIndex = l * ChunkEntitiesLength * COUNTOFFEST;
                    Chunk local = _Array[l * COUNTOFFEST];
                    for (int r = 0; r < COUNTOFFEST; r++)
                    {
                        if (local == null) break;
                        for (int i = 0; i < local.Count; i++)
                        {
                            if (_RecoveredTable.ContainsKey(runningIndex++)) continue;
                            TGroup group = foreachFunc();
                            uAction(group, local, i);
                        }
                        local = local.Next;
                    }
                    countDown.Signal();
                }, i));
            }
            countDown.Wait();
        }
        /// <summary>
        /// 并行遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="members"></param>
        /// <param name="foreachFunc"></param>
        public void ParallelForeach<TGroup>(MemberInfo[] members, Func<TGroup, TGroup> foreachFunc)
            where TGroup: struct
        {
            Func<Chunk, int, TGroup> qFunc = GetQueryFunc<TGroup>(members);
            Action<TGroup, Chunk, int> uAction = GetUpdateFunc<TGroup>(members);
            using var countDown = new CountdownEvent(_CountdownCount);
            for (int i = 0; i < _CountdownCount; i++)
            {
                ValueTask valueTask = new ValueTask(Task.Factory.StartNew((index) =>
                {
                    int l = (int)index;
                    int runningIndex = l * ChunkEntitiesLength * COUNTOFFEST;
                    Chunk local = _Array[l * COUNTOFFEST];
                    for (int r = 0; r < COUNTOFFEST; r++)
                    {
                        if (local == null) break;
                        for (int i = 0; i < local.Count; i++)
                        {
                            if (_RecoveredTable.ContainsKey(runningIndex++)) continue;
                            TGroup group = qFunc(local, i);
                            TGroup result = foreachFunc(group);
                            uAction(result, local, i);
                        }
                        local = local.Next;
                    }
                    countDown.Signal();
                }, i));
            }
            countDown.Wait();
        }

        /// <summary>
        /// 创建实体
        /// </summary>
        /// <param name="size"></param>
        /// <param name="startIndex"></param>
        public void CreateEntities(ref int startIndex, int size)
        {
            if (_End.RunningIndex + size >= MAXSIZE) throw new OutOfMemoryException($"This Array is not have enough size to create entity, the final size is {_End.RunningIndex + size} and it greater than {MAXSIZE}");
            int reusing = 0;
            ReusingEntities(ref reusing, ref size);
            if (size <= 0) return;
            if (_End.Count + size >= _End.Length)
            {
                var diff = _End.Length - _End.Count;
                _End.CreateEntities(ref startIndex, diff);
                int end = size - diff;
                int start = diff;
                int offest;
                do
                {
                    offest = end > ChunkEntitiesLength ? ChunkEntitiesLength : end;
                    Chunk chunk = new Chunk(_Layouts.Select(x => x.Value).ToArray(), ChunkEntitiesLength);
                    _End.SetNext(chunk);
                    _End = chunk;
                    _ChunkIndex = _End.ChnukIndex;
                    if (_ChunkIndex % COUNTOFFEST == 0) _CountdownCount++;
                    if (_ChunkIndex >= _Array.Length)
                        Array.Resize(ref _Array, _Array.Length * 2);
                    _Array[_ChunkIndex] = chunk;
                    _End.CreateEntities(ref startIndex, offest);
                    end -= offest;
                    start += offest;
                } while (end > 0);
            }
            else _End.CreateEntities(ref startIndex, size);
        }
        /// <summary>
        /// 创建实体
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="startIndex"></param>
        /// <param name="size"></param>
        /// <param name="initData"></param>
        /// <param name="members"></param>
        public void CreateEntities<TGroup>(ref int startIndex, int size, TGroup initData, MemberInfo[] members)
            where TGroup : struct
        {
            if (_End.RunningIndex + size >= MAXSIZE) throw new OutOfMemoryException($"This Array is not have enough size to create entity, the final size is {_End.RunningIndex + size} and it greater than {MAXSIZE}");
            int reusing = 0;
            ReusingEntities(ref reusing, ref size, initData, members);
            if (size <= 0) return;
            if (_End.Count + size >= _End.Length)
            {
                var diff = _End.Length - _End.Count;
                _End.CreateEntities(ref startIndex, diff, initData, members);
                int end = size - diff;
                int start = diff;
                int offest;
                do
                {
                    offest = end > ChunkEntitiesLength ? ChunkEntitiesLength : end;
                    Chunk chunk = new Chunk(_Layouts.Select(x => x.Value).ToArray(), ChunkEntitiesLength);
                    _End.SetNext(chunk);
                    _End = chunk;
                    _ChunkIndex = _End.ChnukIndex;
                    if (_ChunkIndex % COUNTOFFEST == 0) _CountdownCount++;
                    if (_ChunkIndex >= _Array.Length)
                        Array.Resize(ref _Array, _Array.Length * 2);
                    _Array[_ChunkIndex] = chunk;
                    _End.CreateEntities(ref startIndex, offest, initData, members);
                    end -= offest;
                    start += offest;
                } while (end > 0);
            }
            else _End.CreateEntities(ref startIndex, size, initData, members);
        }
        /// <summary>
        /// 创建实体
        /// </summary>
        /// <param name="startIndex"></param>
        /// <param name="size"></param>
        /// <param name="entities"></param>
        public void CreateEntities(ref int startIndex, int size, out Entity[] entities)
        {
            if (_End.RunningIndex + size >= MAXSIZE) throw new OutOfMemoryException($"This Array is not have enough size to create entity, the final size is {_End.RunningIndex + size} and it greater than {MAXSIZE}");
            entities = new Entity[size];
            Span<Entity> local = entities;
            Span<Entity> chunkSpan;
            int reusing = 0;
            ReusingEntities(ref reusing, ref local, ref size);
            if (size <= 0) return;
            local = local.Slice(reusing);
            if (_End.Count + size >= _End.Length)
            {
                var diff = _End.Length - _End.Count;
                chunkSpan = local.Slice(0, diff);
                _End.CreateEntities(ref startIndex, diff, ref chunkSpan);
                int end = size - diff;
                int start = diff;
                int offest;
                do
                {
                    offest = end > ChunkEntitiesLength ? ChunkEntitiesLength : end;
                    Chunk chunk = new Chunk(_Layouts.Select(x => x.Value).ToArray(), ChunkEntitiesLength);
                    _End.SetNext(chunk);
                    _End = chunk;
                    _ChunkIndex = _End.ChnukIndex;
                    if (_ChunkIndex % COUNTOFFEST == 0) _CountdownCount++;
                    if (_ChunkIndex >= _Array.Length)
                        Array.Resize(ref _Array, _Array.Length * 2);
                    _Array[_ChunkIndex] = chunk;
                    chunkSpan = local.Slice(start, offest);
                    _End.CreateEntities(ref startIndex, offest, ref chunkSpan);
                    end -= offest;
                    start += offest;
                } while (end > 0);
            }
            else _End.CreateEntities(ref startIndex, size, ref local);
        }
        /// <summary>
        /// 创建实体
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="startIndex"></param>
        /// <param name="size"></param>
        /// <param name="initData"></param>
        /// <param name="members"></param>
        /// <param name="entities"></param>
        public void CreateEntities<TGroup>(ref int startIndex, int size, TGroup initData, MemberInfo[] members, out Entity[] entities)
            where TGroup : struct
        {
            if (_End.RunningIndex + size >= MAXSIZE) throw new OutOfMemoryException($"This Array is not have enough size to create entity, the final size is {_End.RunningIndex + size} and it greater than {MAXSIZE}");
            entities = new Entity[size];
            Span<Entity> local = entities;
            Span<Entity> chunkSpan;
            int reusing = 0;
            ReusingEntities(ref reusing, ref local, ref size, initData, members);
            if (size <= 0) return;
            local = local.Slice(reusing);
            if (_End.Count + size >= _End.Length)
            {
                var diff = _End.Length - _End.Count;
                chunkSpan = local.Slice(0, diff);
                _End.CreateEntities(ref startIndex, diff, initData, members, ref chunkSpan);
                int end = size - diff;
                int start = diff;
                int offest;
                do
                {
                    offest = end > ChunkEntitiesLength ? ChunkEntitiesLength : end;
                    Chunk chunk = new Chunk(_Layouts.Select(x => x.Value).ToArray(), ChunkEntitiesLength);
                    _End.SetNext(chunk);
                    _End = chunk;
                    _ChunkIndex = _End.ChnukIndex;
                    if (_ChunkIndex % COUNTOFFEST == 0) _CountdownCount++;
                    if (_ChunkIndex >= _Array.Length)
                        Array.Resize(ref _Array, _Array.Length * 2);
                    _Array[_ChunkIndex] = chunk;
                    chunkSpan = local.Slice(start, offest);
                    _End.CreateEntities(ref startIndex, offest, initData, members, ref chunkSpan);
                    end -= offest;
                    start += offest;
                } while (end > 0);
            }
            else _End.CreateEntities(ref startIndex, size, initData, members, ref local);
        }

        /// <summary>
        /// 回收实体
        /// </summary>
        /// <param name="entity"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void RecoveryEntity(Entity entity)
        {
            int index = entity.RunningIndex;
            if (index < 0) return;
            int chunkIndex = index / ChunkEntitiesLength;
            int offest = index % ChunkEntitiesLength;
            Chunk local = _Array[chunkIndex];
            if(_Layouts.TryGetValue(_EntityType, out ComponentLayout layout))
            {
                var finalIndex = layout.StartIndex + offest * layout.ComponentOffest;
                Entity o = MemoryHelper.ReadStruct<Entity>(local.Memory, finalIndex, layout.ComponentOffest);
                if(entity == o)
                {
                    _RecoveredEntities.Enqueue(o);
                    if (!_RecoveredTable.TryAdd(index, o)) _RecoveredTable[index] = o;
                }
            }
        }
        /// <summary>
        /// 回收实体
        /// </summary>
        /// <param name="entities"></param>
        public void RecoveryEntities(Entity[] entities)
        {
            foreach (var item in entities) RecoveryEntity(item);
        }

        /// <summary>
        /// 销毁
        /// </summary>
        public void Dispose()
        {
            _Array = null;
            _First.Free();
            _First = null;
            _End = null;
            _Layouts.Clear();
            _QueryFuncTable.Clear();
            _UpdateFuncTable.Clear();
            ChunkEntitiesLength = 0;
            _ChunkIndex = 0;
        }

        /// <summary>
        /// 创建查询表达式
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="members"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private Expression<Func<Chunk, int , TGroup>> CreateQueryExpression<TGroup>(MemberInfo[] members)
        {
            var type = typeof(TGroup);
            ParameterExpression x = Expression.Parameter(typeof(Chunk), "x");
            ParameterExpression y = Expression.Parameter(typeof(int), "y");
            MemberExpression ptr = Expression.Property(x, "Memory");
            List<MemberAssignment> membersAssign = new List<MemberAssignment>();
            foreach(var item in members)
            {
                var lType = item.MemberType == MemberTypes.Field ? (item as FieldInfo).FieldType : (item as PropertyInfo).PropertyType;
                if (_Layouts.TryGetValue(lType, out ComponentLayout layout))
                {
                    ConstantExpression startIndex = Expression.Constant(layout.StartIndex);
                    ConstantExpression size = Expression.Constant(layout.ComponentOffest);
                    BinaryExpression bm = Expression.MakeBinary(ExpressionType.Multiply, y, size);
                    BinaryExpression offestExpression = Expression.MakeBinary(ExpressionType.Add, startIndex, bm);
                    var method = typeof(MemoryHelper).GetMethods().Where(x => x.IsGenericMethod && x.Name == "ReadStruct").First().MakeGenericMethod(lType);
                    MethodCallExpression getStruct = Expression.Call(method, new Expression[] { ptr, offestExpression, size });
                    MemberAssignment member = Expression.Bind(item, getStruct);
                    membersAssign.Add(member);
                }
            }
            var str = Expression.MemberInit(Expression.New(type), membersAssign);
            Expression<Func<Chunk, int, TGroup>> result = Expression.Lambda<Func<Chunk, int, TGroup>>(str, x, y);
            return result;
        }
        /// <summary>
        /// 创建更新表达式
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="members"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private Expression<Action<TGroup, Chunk, int>> CreateUpdateExpression<TGroup>(MemberInfo[] members)
        {
            var type = typeof(TGroup);
            ParameterExpression x = Expression.Parameter(type, "x");
            ParameterExpression y = Expression.Parameter(typeof(Chunk), "y");
            ParameterExpression z = Expression.Parameter(typeof(int), "z");
            MemberExpression ptr = Expression.Property(y, "Memory");
            List<Expression> expressions = new List<Expression>();
            foreach(var item in members)
            {
                var lType = item.MemberType == MemberTypes.Field ? (item as FieldInfo).FieldType : (item as PropertyInfo).PropertyType;
                if (_Layouts.TryGetValue(lType, out ComponentLayout layout))
                {
                    if (layout.ComponentType == _EntityType) continue;
                    ConstantExpression startIndex = Expression.Constant(layout.StartIndex);
                    ConstantExpression size = Expression.Constant(layout.ComponentOffest);
                    BinaryExpression bm = Expression.MakeBinary(ExpressionType.Multiply, z, size);
                    BinaryExpression offestExpression = Expression.MakeBinary(ExpressionType.Add, startIndex, bm);
                    MemberExpression field = Expression.PropertyOrField(x, item.Name);
                    var method = typeof(MemoryHelper).GetMethods().Where(x => x.IsGenericMethod && x.Name == "WriteStruct").First().MakeGenericMethod(lType);
                    MethodCallExpression setStruct = Expression.Call(method, new Expression[] { ptr, field, offestExpression, size });
                    expressions.Add(setStruct);
                }
            }
            Expression block = Expression.Block(expressions);
            Expression<Action<TGroup, Chunk, int>> action = Expression.Lambda<Action<TGroup, Chunk, int>>(block, x, y, z);
            return action;
        }
        /// <summary>
        /// 获取查询函数表达式
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="members"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private Func<Chunk, int, TGroup> GetQueryFunc<TGroup>(MemberInfo[] members)
        {
            Func<Chunk, int, TGroup> func;
            var type = typeof(TGroup);
            if (_QueryFuncTable.TryGetValue(type, out Delegate @delegate)) func = @delegate as Func<Chunk, int, TGroup>;
            else
            {
                Expression<Func<Chunk, int, TGroup>> expression = CreateQueryExpression<TGroup>(members);
                func = expression.Compile();
                _QueryFuncTable.Add(type, func);
            }
            return func;
        }
        /// <summary>
        /// 获取更新函数表达式
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="members"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private Action<TGroup, Chunk, int> GetUpdateFunc<TGroup>(MemberInfo[] members)
        {
            var type = typeof(TGroup);
            Action<TGroup, Chunk, int> action;
            if (_UpdateFuncTable.TryGetValue(type, out Delegate @delegate))
            {
                action = @delegate as Action<TGroup, Chunk, int>;
                return action;
            }
            else
            {
                Expression<Action<TGroup, Chunk, int>> actionE = CreateUpdateExpression<TGroup>(members);
                action = actionE.Compile();
                _UpdateFuncTable.Add(type, action);
                return action;
            }
        }
        /// <summary>
        /// 重用实体
        /// </summary>
        /// <param name="reusing"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void ReusingEntities(ref int reusing, ref int size)
        {
            Chunk local;
            if (_Layouts.TryGetValue(_EntityType, out ComponentLayout layout))
            {
                while ( reusing < size && _RecoveredEntities.TryDequeue(out Entity entity))
                {
                    reusing++;
                    entity.Version++;
                    int runningIndex = entity.RunningIndex;
                    int chunkIndex = runningIndex / ChunkEntitiesLength;
                    int offset = runningIndex % ChunkEntitiesLength;
                    local = _Array[chunkIndex];
                    MemoryHelper.WriteStruct(local.Memory, entity, layout.StartIndex + offset * layout.ComponentOffest, layout.ComponentOffest);
                    _RecoveredTable.Remove(runningIndex);
                }
                size -= reusing;
            }
        }
        /// <summary>
        /// 重用实体
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="reusing"></param>
        /// <param name="size"></param>
        /// <param name="initData"></param>
        /// <param name="infos"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void ReusingEntities<TGroup>(ref int reusing, ref int size, TGroup initData, MemberInfo[] infos)
            where TGroup : struct
        {
            Chunk local;
            if (_Layouts.TryGetValue(_EntityType, out ComponentLayout layout))
            {
                while (reusing < size && _RecoveredEntities.TryDequeue(out Entity entity))
                {
                    reusing++;
                    entity.Version++;
                    int runningIndex = entity.RunningIndex;
                    int chunkIndex = runningIndex / ChunkEntitiesLength;
                    int offset = runningIndex % ChunkEntitiesLength;
                    local = _Array[chunkIndex];
                    WriteData(entity.RunningIndex, infos, initData);
                    MemoryHelper.WriteStruct(local.Memory, entity, layout.StartIndex + offset * layout.ComponentOffest, layout.ComponentOffest);
                    _RecoveredTable.Remove(runningIndex);
                }
                size -= reusing;
            }
        }
        /// <summary>
        /// 重用实体
        /// </summary>
        /// <param name="reusing"></param>
        /// <param name="entities"></param>
        /// <param name="size"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void ReusingEntities(ref int reusing, ref Span<Entity> entities, ref int size)
        {
            Chunk local;
            if (_Layouts.TryGetValue(_EntityType, out ComponentLayout layout))
            {
                while (reusing < size && _RecoveredEntities.TryDequeue(out Entity entity))
                {
                    entity.Version++;
                    int runningIndex = entity.RunningIndex;
                    int chunkIndex = runningIndex / ChunkEntitiesLength;
                    int offset = runningIndex % ChunkEntitiesLength;
                    local = _Array[chunkIndex];
                    MemoryHelper.WriteStruct(local.Memory, entity, layout.StartIndex + offset * layout.ComponentOffest, layout.ComponentOffest);
                    entities[reusing++] = entity;
                    _RecoveredTable.Remove(runningIndex);
                }
                size -= reusing;
            }
        }
        /// <summary>
        /// 重用实体
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="reusing"></param>
        /// <param name="entities"></param>
        /// <param name="size"></param>
        /// <param name="initData"></param>
        /// <param name="infos"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void ReusingEntities<TGroup>(ref int reusing, ref Span<Entity> entities, ref int size, TGroup initData, MemberInfo[] infos)
            where TGroup : struct
        {
            Chunk local;
            if (_Layouts.TryGetValue(_EntityType, out ComponentLayout layout))
            {
                while (reusing < size && _RecoveredEntities.TryDequeue(out Entity entity))
                {
                    entity.Version++;
                    int runningIndex = entity.RunningIndex;
                    int chunkIndex = runningIndex / ChunkEntitiesLength;
                    int offset = runningIndex % ChunkEntitiesLength;
                    local = _Array[chunkIndex];
                    WriteData(entity.RunningIndex, infos, initData);
                    MemoryHelper.WriteStruct(local.Memory, entity, layout.StartIndex + offset * layout.ComponentOffest, layout.ComponentOffest);
                    entities[reusing++] = entity;
                    _RecoveredTable.Remove(runningIndex);
                }
                size -= reusing;
            }
        }

        /// <summary>
        /// 块包装器
        /// </summary>
        private readonly struct ChunkWrapper
        {
            /// <summary>
            /// 片段
            /// </summary>
            private readonly Chunk _value;
            /// <summary>
            /// 构造函数
            /// </summary>
            /// <param name="segment"></param>
            private ChunkWrapper(Chunk segment) => _value = segment;
            /// <summary>
            /// 转换器
            /// </summary>
            /// <param name="s"></param>
            public static implicit operator ChunkWrapper(Chunk s) => new ChunkWrapper(s);
            /// <summary>
            /// 转换器
            /// </summary>
            /// <param name="s"></param>
            public static implicit operator Chunk(ChunkWrapper s) => s._value;
        }
    }
}
