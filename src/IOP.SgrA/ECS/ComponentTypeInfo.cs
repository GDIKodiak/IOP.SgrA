﻿using System;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace IOP.SgrA.ECS
{
    /// <summary>
    /// 组件信息
    /// </summary>
    public class ComponentTypeInfo
    {
        /// <summary>
        /// 类型
        /// </summary>
        public Type Type { get; }
        /// <summary>
        /// 实际组件偏移量
        /// </summary>
        public int Offest { get; }
        /// <summary>
        /// 组件BitMap
        /// </summary>
        public ArchetypeBitMap BitMap { get; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="type"></param>
        /// <param name="offest"></param>
        /// <param name="bitMap"></param>
        public ComponentTypeInfo(Type type, int offest, ArchetypeBitMap bitMap)
        {
            Type = type;
            Offest = offest;
            BitMap = bitMap;
        }
    }
}
