﻿using IOP.SgrA.Input;
using System;
using System.Collections.Generic;
using System.Numerics;

namespace IOP.SgrA
{
    /// <summary>
    /// 输入状态控制器接口
    /// </summary>
    public interface IInputStateController
    {
        /// <summary>
        /// 获取鼠标坐标
        /// </summary>
        /// <returns></returns>
        public Vector2 GetMousePosition();
        /// <summary>
        /// 获取鼠标增量
        /// </summary>
        /// <returns></returns>
        public Vector2 GetMousePositionDelta();
        /// <summary>
        /// 获取鼠标滚轮增量
        /// </summary>
        /// <returns></returns>
        public int GetMouseWheelDelta();
        /// <summary>
        /// 获取鼠标按钮状态
        /// </summary>
        /// <param name="mouseButton"></param>
        /// <returns></returns>
        public MouseButtonState GetMouseButtonState(MouseButton mouseButton);
        /// <summary>
        /// 获取键盘按键状态
        /// </summary>
        /// <param name="pressKey"></param>
        /// <returns></returns>
        public PressKeyState GetPressKeyState(PressKey pressKey);
        /// <summary>
        /// 变更点击有效区间
        /// </summary>
        /// <param name="millisecond"></param>
        public void ChangeClickPeriod(int millisecond);
        /// <summary>
        /// 隐藏鼠标
        /// </summary>
        /// <param name="enable"></param>
        public void CursorDisabled(bool enable);
    }
}
