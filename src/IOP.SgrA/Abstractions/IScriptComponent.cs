﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA
{
    /// <summary>
    /// 脚本组件
    /// </summary>
    public interface IScriptComponent : IAttachRenderComponent
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="timeSpan"></param>
        void Update(TimeSpan timeSpan);
    }
}
