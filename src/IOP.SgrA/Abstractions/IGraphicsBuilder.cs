﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 图形接口构建者接口
    /// </summary>
    public interface IGraphicsBuilder
    {
        /// <summary>
        /// 服务
        /// </summary>
        IServiceProvider Service { get; }
    }
}
