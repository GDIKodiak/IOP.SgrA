﻿using System.Numerics;
using System;
using IOP.SgrA.Input;

namespace IOP.SgrA
{
    /// <summary>
    /// 渲染组接口
    /// </summary>
    public interface IRenderGroup : IRenderComponent, IDisposable
    {
        /// <summary>
        /// 
        /// </summary>
        Area Scissor { get; }
        /// <summary>
        /// 渲染区域
        /// </summary>
        ViewportRect Viewport { get; }
        /// <summary>
        /// 摄影机
        /// </summary>
        Camera Camera { get; }
        /// <summary>
        /// 优先级
        /// </summary>
        int Priority { get; }
        /// <summary>
        /// 渲染组类型
        /// </summary>
        GroupType GroupType { get; }
        /// <summary>
        /// 组渲染模式
        /// </summary>
        GroupRenderMode RenderMode { get; }
        /// <summary>
        /// 初始化
        /// </summary>
        void Initialization();
        /// <summary>
        /// 组渲染
        /// </summary>
        void GroupRendering();
        /// <summary>
        /// 组渲染
        /// </summary>
        /// <param name="frameIndex"></param>
        void GroupRendering(uint frameIndex);
        /// <summary>
        /// 判断是否可用
        /// </summary>
        /// <returns></returns>
        bool IsEnable();
        /// <summary>
        /// 设置摄像机
        /// </summary>
        /// <param name="camera"></param>
        void SetCamera(Camera camera);
        /// <summary>
        /// 设置视口
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="maxDepth"></param>
        /// <param name="minDepth"></param>
        void SetViewPort(float x, float y, float width, float height, float minDepth, float maxDepth);
        /// <summary>
        /// 推送数据至纹理
        /// </summary>
        /// <typeparam name="TStruct"></typeparam>
        /// <param name="data"></param>
        /// <param name="binding"></param>
        /// <param name="setIndex"></param>
        void PushDataToTexture<TStruct>(TStruct data, uint setIndex, uint binding) where TStruct : struct;
        /// <summary>
        /// 推送数据至纹理
        /// </summary>
        /// <param name="data"></param>
        /// <param name="setIndex"></param>
        /// <param name="binding"></param>
        void PushDataToTexture(Span<byte> data, uint setIndex, uint binding);
        /// <summary>
        /// 推送上下文
        /// </summary>
        /// <param name="context"></param>
        void PushContext(Context context);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderObject"></param>
        void PushContext(IRenderObject renderObject);
        /// <summary>
        /// 绑定上下文
        /// </summary>
        /// <param name="context"></param>
        void BindingContext(Context[] context);
        /// <summary>
        /// 绑定上下文
        /// </summary>
        /// <param name="context"></param>
        /// <param name="renderObject"></param>
        /// <param name="deepCopy"></param>
        void BindingContext(Context[] context, IRenderObject renderObject, bool deepCopy = false);
        /// <summary>
        /// 解除绑定
        /// </summary>
        /// <param name="context"></param>
        void UnbindContext(Context context);
        /// <summary>
        /// 设置自定义属性
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <param name="data"></param>
        void SetCustomPropery<T>(string name, T data) where T : struct;
        /// <summary>
        /// 获取自定义属性
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        T GetCustomPropery<T>(string name) where T : struct;
        /// <summary>
        /// 禁用
        /// </summary>
        void Disable();
        /// <summary>
        /// 启用
        /// </summary>
        void Enable();
    }
}
