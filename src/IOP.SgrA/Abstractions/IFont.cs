﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 字体接口
    /// </summary>
    public interface IFont : IRenderComponent
    {
        /// <summary>
        /// 创建文字网格
        /// </summary>
        /// <param name="str"></param>
        /// <param name="column"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        (float[], uint) CreateTextMeshData(string str, int column, int row);
        /// <summary>
        /// 获取字体纹理
        /// </summary>
        /// <returns></returns>
        ITexture GetFontTexture();
    }
}
