﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 渲染调度器组件
    /// </summary>
    public interface IRenderDispatcherComponent
    {
        /// <summary>
        /// 指示组件是否处于加载状态
        /// </summary>
        bool IsLoading { get; }
        /// <summary>
        /// 是否启用
        /// </summary>
        bool IsEnabled { get; }
        /// <summary>
        /// 渲染
        /// </summary>
        void Rendering(uint fIndex);
        /// <summary>
        /// 执行组件系统
        /// </summary>
        void ExecuteComponentSystem(TimeSpan lastStamp);
        /// <summary>
        /// 初始化
        /// </summary>
        void Initialization();
    }
}
