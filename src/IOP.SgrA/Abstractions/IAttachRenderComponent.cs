﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA
{
    /// <summary>
    /// 附加的渲染组件
    /// </summary>
    public interface IAttachRenderComponent : IRenderComponent
    {
        /// <summary>
        /// 所属者
        /// </summary>
        IRenderObject Owner { get; }
        /// <summary>
        /// 当组件被附加时
        /// </summary>
        /// <param name="object"></param>
        void OnAttach(IRenderObject @object);
        /// <summary>
        /// 当解除附加
        /// </summary>
        /// <param name="object"></param>
        void OnDetach(IRenderObject @object);
    }
}
