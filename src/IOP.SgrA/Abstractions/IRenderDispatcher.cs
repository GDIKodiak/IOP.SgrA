﻿using IOP.SgrA.ECS;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 渲染调度器
    /// </summary>
    public interface IRenderDispatcher
    {
        /// <summary>
        /// 输入
        /// </summary>
        IInputStateController Input { get; }
        /// <summary>
        /// 上下文管理器
        /// </summary>
        IContextManager ContextManager { get; }
        /// <summary>
        /// 初始化
        /// </summary>
        void Initialization();
        /// <summary>
        /// 执行渲染
        /// </summary>
        void Rendering();
        /// <summary>
        /// 停止进行渲染
        /// </summary>
        void Stop();
        /// <summary>
        /// 开始执行渲染
        /// </summary>
        void Start();
        /// <summary>
        /// 推送组件
        /// </summary>
        /// <returns></returns>
        void PushDispatcherComponent(IRenderDispatcherComponent component);
        /// <summary>
        /// 重建渲染调度器
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        void RecreateRenderDispatcher(uint width, uint height);
        /// <summary>
        /// 摧毁渲染调度器
        /// </summary>
        void DestroyDispatcher();
        /// <summary>
        /// 获取事件总线
        /// </summary>
        /// <returns></returns>
        ISystemEventBus GetSystemEventBus();
        /// <summary>
        /// 获取当前场景
        /// </summary>
        /// <returns></returns>
        Scene GetCurrentScene();
        /// <summary>
        /// 发送时间戳
        /// </summary>
        /// <param name="timeSpan"></param>
        void SendTimeStamp(TimeSpan timeSpan);
    }
}
