﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 渲染组件
    /// </summary>
    public interface IRenderComponent : ICloneableComponent
    {
        /// <summary>
        /// 名称
        /// </summary>
        string Name { get; }
        /// <summary>
        /// 摧毁渲染组件
        /// </summary>
        void Destroy();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderObject"></param>
        void OnAttach(IRenderObject renderObject);
    }
}
