﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 采样器接口
    /// </summary>
    public interface ISampler
    {
        /// <summary>
        /// 名称
        /// </summary>
        string Name { get; }
    }
}
