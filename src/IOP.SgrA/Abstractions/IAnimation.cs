﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 动画基类接口
    /// </summary>
    public interface IAnimation : IRenderComponent
    {
        /// <summary>
        /// 播放
        /// </summary>
        /// <param name="timeSpan"></param>
        void Play(TimeSpan timeSpan);
        /// <summary>
        /// 当组件被挂载时
        /// </summary>
        /// <param name="object"></param>
        void OnAttach(IRenderObject @object);
    }
}
