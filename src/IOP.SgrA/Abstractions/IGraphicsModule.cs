﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IOP.SgrA
{
    /// <summary>
    /// 模块接口
    /// </summary>
    public interface IGraphicsModule
    {
        /// <summary>
        /// 上下文管理器
        /// </summary>
        IContextManager ContextManager { get; set; }
        /// <summary>
        /// 父类模块
        /// </summary>
        IGraphicsModule Parent {  get; }
        /// <summary>
        /// 优先级
        /// </summary>
        ModulePriority Priority { get; }
        /// <summary>
        /// 子模块
        /// </summary>
        /// <returns></returns>
        IEnumerable<IGraphicsModule> GetChildrens();
        /// <summary>
        /// 是否完成加载
        /// </summary>
        bool IsLoaded { get; }
        /// <summary>
        /// 加载
        /// </summary>
        event Action<IGraphicsModule> Loaded;
        /// <summary>
        /// 卸载
        /// </summary>
        event Action<IGraphicsModule> Unloaded;
        /// <summary>
        /// 加载
        /// </summary>
        /// <returns></returns>
        Task Load();
        /// <summary>
        /// 卸载
        /// </summary>
        /// <returns></returns>
        Task Unload();
    }
}
