﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA
{
    /// <summary>
    /// 场景接口
    /// </summary>
    public interface IScene : IRenderDispatcherComponent, IDisposable
    {
        /// <summary>
        /// 场景名
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// 场景中心
        /// </summary>
        Vector3 Center { get; }
        /// <summary>
        /// 添加场景模块
        /// </summary>
        /// <param name="types"></param>
        void AddSceneModules(params Type[] types);
        /// <summary>
        /// 加载场景模块
        /// </summary>
        /// <returns></returns>
        Task LoadSceneModules();
        /// <summary>
        /// 注册场景动态加载函数
        /// </summary>
        /// <param name="func"></param>
        void RegistLoadingFunction(Func<IScene, Vector3, Task> func);
        /// <summary>
        /// 将渲染对象插入至场景
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="action"></param>
        void Insert(IRenderObject obj, Action<bool, IRenderObject> action = null);
        /// <summary>
        /// 移动场景中心
        /// </summary>
        /// <param name="newCenter"></param>
        void MoveCenter(Vector3 newCenter);
        /// <summary>
        /// 注册同步函数至队列
        /// </summary>
        /// <param name="action"></param>
        void Post(Action action);
        /// <summary>
        /// 尝试获取场景模块
        /// </summary>
        /// <typeparam name="TModule"></typeparam>
        /// <param name="module"></param>
        /// <returns></returns>
        bool TryGetSceneModule<TModule>(out TModule module) where TModule : IGraphicsModule;
    }
    /// <summary>
    /// 可加载的场景
    /// </summary>
    /// <typeparam name="TScene"></typeparam>
    public interface ILoadableScene<TScene>
        where TScene : IScene
    {
        /// <summary>
        /// 加载完成事件
        /// </summary>
        event Action<TScene> Loaded;
        /// <summary>
        /// 卸载完成事件
        /// </summary>
        event Action<TScene> UnLoaded;
        /// <summary>
        /// 加载
        /// </summary>
        /// <returns></returns>
        Task Load(params object[] param);
        /// <summary>
        /// 卸载
        /// </summary>
        /// <returns></returns>
        Task UnLoad(params object[] param);
    }
}
