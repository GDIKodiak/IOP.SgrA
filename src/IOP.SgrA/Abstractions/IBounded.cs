﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA
{
    /// <summary>
    /// 有边界对象
    /// </summary>
    public interface IBounded
    {
        /// <summary>
        /// 获取边界
        /// </summary>
        /// <returns></returns>
        Bounds GetBounds();
    }
}
