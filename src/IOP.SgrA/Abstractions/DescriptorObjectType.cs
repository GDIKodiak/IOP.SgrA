﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA
{
    /// <summary>
    /// 描述符对象接口
    /// </summary>
    public interface IDescriptorObject
    {
        /// <summary>
        /// 描述符类型
        /// </summary>
        DescriptorObjectType DescriptorObjectType { get; }
    }
    /// <summary>
    /// 描述符对象类型
    /// </summary>
    public enum DescriptorObjectType
    {
        /// <summary>
        /// 
        /// </summary>
        None,
        /// <summary>
        /// 合并采样器图像
        /// </summary>
        CombinedImageSampler,
        /// <summary>
        /// 可采样图像
        /// </summary>
        SampledImage,
        /// <summary>
        /// 可读写图像
        /// </summary>
        StorageImage,
        /// <summary>
        /// 自适应结构化缓冲
        /// </summary>
        UniformTexelBuffer,
        /// <summary>
        /// 可读写结构化缓冲
        /// </summary>
        StorageTexelBuffer,
        /// <summary>
        /// 自适应缓冲
        /// </summary>
        UniformBuffer,
        /// <summary>
        /// 可读写缓冲
        /// </summary>
        StorageBuffer,
        /// <summary>
        /// 动态自适应缓冲
        /// </summary>
        UniformBufferDynamic,
        /// <summary>
        /// 可读写动态缓冲
        /// </summary>
        StorageBufferDynamic,
        /// <summary>
        /// 输入附件
        /// </summary>
        InputAttachment
    }
}
