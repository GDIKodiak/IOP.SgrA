﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA
{
    /// <summary>
    /// 动态缓冲纹理接口
    /// </summary>
    public interface IDynamicBufferTexture : IDescriptorObject, IRenderComponent
    {
        /// <summary>
        /// 总大小
        /// </summary>
        public ulong Size { get; }
        /// <summary>
        /// 描述符集下标
        /// </summary>
        public uint SetIndex { get; }
        /// <summary>
        /// 纹理绑定位置
        /// </summary>
        public uint Binding { get; }
        /// <summary>
        /// 块大小
        /// </summary>
        public uint BlockSize { get; }
        /// <summary>
        /// 块容量
        /// </summary>
        public uint BlockCapacity {  get; }
        /// <summary>
        /// 更新纹理内存数据
        /// </summary>
        /// <param name="updateData"></param>
        void UpdateTextureMemoryData(byte[] updateData);
        /// <summary>
        /// 更新纹理内存数据
        /// </summary>
        /// <param name="updateData"></param>
        /// <param name="startIndex"></param>
        void UpdateTextureMemoryData(byte[] updateData, int startIndex);
        /// <summary>
        /// 更新纹理内存数据
        /// </summary>
        /// <param name="updateData"></param>
        void UpdateTextureMemoryData(Span<byte> updateData);
        /// <summary>
        /// 更新纹理内存数据
        /// </summary>
        /// <param name="updateData"></param>
        /// <param name="startIndex"></param>
        void UpdateTextureMemoryData(Span<byte> updateData, int startIndex);
        /// <summary>
        /// 更新纹理内存数据
        /// </summary>
        /// <typeparam name="TStruct"></typeparam>
        /// <param name="struct"></param>
        void UpdateTextureMemoryData<TStruct>(TStruct @struct) where TStruct : unmanaged;
        /// <summary>
        /// 更新纹理数据
        /// </summary>
        /// <typeparam name="TStruct"></typeparam>
        /// <param name="struct"></param>
        /// <param name="startIndex"></param>
        void UpdateTextureMemoryData<TStruct>(TStruct @struct, int startIndex) where TStruct : unmanaged;
        /// <summary>
        /// 申请纹理块
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        ITextureBlock MallocBlock();
        /// <summary>
        /// 尝试申请纹理块
        /// </summary>
        /// <param name="block"></param>
        /// <returns></returns>
        bool TryMallocBlock(out ITextureBlock block);
        /// <summary>
        /// 回收纹理块
        /// </summary>
        /// <param name="block"></param>
        void Recovery(ITextureBlock block);
    }
}
