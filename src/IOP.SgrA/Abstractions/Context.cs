﻿using IOP.SgrA.ECS;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 通用上下文
    /// </summary>
    public abstract class Context : IDisposable
    {
        /// <summary>
        /// 原型
        /// </summary>
        public Archetype Archetype { get; set; }
        /// <summary>
        /// 实体
        /// </summary>
        public Entity Entity { get; set; }
        /// <summary>
        /// 摄影机
        /// </summary>
        public Camera Camera { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public int State { get; set; }

        /// <summary>
        /// MVP矩阵
        /// </summary>
        private MVPMatrix _MVPMatrix = MVPMatrix.Identity();
        /// <summary>
        /// 上一帧MVP矩阵
        /// </summary>
        private MVPMatrix _PreMVPMatrix = MVPMatrix.Identity();
        /// <summary>
        /// 自定义属性
        /// </summary>
        private IDictionary<string, ValueType> _CustomProperies { get; } = new Dictionary<string, ValueType>();

        /// <summary>
        /// 子上下文
        /// </summary>
        protected IList<Context> Childrens { get; set; } = new List<Context>();
        /// <summary>
        /// 绑定的渲染组
        /// </summary>
        protected IRenderGroup RenderGroup { get; set; } = null;
        /// <summary>
        /// 渲染对象
        /// </summary>
        protected IRenderObject RenderObject { get; set; } = null;
        /// <summary>
        /// 父上下文
        /// </summary>
        public Context Parent { get; set; } = null;

        /// <summary>
        /// 设置观察矩阵
        /// </summary>
        /// <param name="matrix"></param>
        public void SetViewMatrix(in Matrix4x4 matrix) => _MVPMatrix.ViewMatrix = matrix;
        /// <summary>
        /// 设置映射矩阵
        /// </summary>
        /// <param name="matrix"></param>
        public void SetProjectionMatrix(in Matrix4x4 matrix) => _MVPMatrix.ProjectionMatrix = matrix;
        /// <summary>
        /// 设置模型矩阵
        /// </summary>
        /// <param name="matrix"></param>
        public void SetModelMatrix(in Matrix4x4 matrix) => _MVPMatrix.ModelMatrix = matrix;
        /// <summary>
        /// 设置摄影机
        /// </summary>
        /// <param name="camera"></param>
        public void SetCamera(Camera camera) => Camera = camera;
        /// <summary>
        /// 获取MVP矩阵
        /// </summary>
        /// <returns></returns>
        public ref MVPMatrix GetMVPMatrix() => ref _MVPMatrix;
        /// <summary>
        /// 获取上一帧MVP矩阵
        /// </summary>
        /// <returns></returns>
        public ref MVPMatrix GetPreMVPMatrix() => ref _PreMVPMatrix;
        /// <summary>
        /// 备份当前MVP矩阵
        /// </summary>
        public void BackupsCurrnetMVP() => _PreMVPMatrix = _MVPMatrix;

        /// <summary>
        /// 获取自定义属性
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public T GetCustomPropery<T>(string name)
            where T : struct
        {
            if (_CustomProperies.TryGetValue(name, out ValueType value))
            {
                return (T)value;
            }
            else
            {
                _CustomProperies.Add(name, default(T));
                return default;
            }
        }
        /// <summary>
        /// 设置自定义属性
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <param name="data"></param>
        public void SetCustomPropery<T>(string name, T data)
            where T : struct
        {
            if (_CustomProperies.ContainsKey(name))
            {
                _CustomProperies[name] = data;
            }
            else _CustomProperies.TryAdd(name, data);
        }
        /// <summary>
        /// 绑定渲染对象
        /// </summary>
        /// <param name="renderObject"></param>
        public virtual void BindRenderObject(IRenderObject renderObject) => RenderObject = renderObject;
        /// <summary>
        /// 绑定渲染组
        /// </summary>
        /// <param name="group"></param>
        public virtual void BindRenderGroup(IRenderGroup group) => RenderGroup = group;
        /// <summary>
        /// 获取上下文所属渲染对象
        /// </summary>
        /// <returns></returns>
        public abstract IRenderObject GetContextRenderObject();
        /// <summary>
        /// 获取上下文所属渲染组
        /// </summary>
        /// <returns></returns>
        public abstract IRenderGroup GetContextRenderGroup();
        /// <summary>
        /// 将上下文推送至渲染组
        /// </summary>
        public abstract void PushToRenderGroup();

        /// <summary>
        /// 遍历子上下文
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<Context> GetChildrens()
        {
            foreach (var item in Childrens) yield return item;
        }
        /// <summary>
        /// 添加子上下文
        /// </summary>
        /// <param name="context"></param>
        public void AddChildren(Context context) => Childrens.Add(context);
        /// <summary>
        /// 移除子上下文
        /// </summary>
        /// <param name="context"></param>
        public void RemoveChildren(Context context) => Childrens.Remove(context);

        /// <summary>
        /// 销毁资源
        /// </summary>
        public virtual void Dispose()
        {
            _CustomProperies.Clear();
        }
    }
}
