﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA
{
    /// <summary>
    /// 窗口配置类
    /// </summary>
    /// <typeparam name="TWindow"></typeparam>
    /// <typeparam name="TGraphics"></typeparam>
    public interface IWindowConfig<TWindow, TGraphics>
        where TWindow : class, IGenericWindow
        where TGraphics : class, IGraphicsManager
    {
        /// <summary>
        /// 初始化
        /// </summary>
        void Initialization();
        /// <summary>
        /// 配置
        /// </summary>
        /// <param name="window"></param>
        /// <param name="manager"></param>
        Task Config(TWindow window, TGraphics manager);
    }
}
