﻿using System;

namespace IOP.SgrA
{
    /// <summary>
    /// 管线构建者
    /// </summary>
    /// <typeparam name="TContext"></typeparam>
    public interface IProductionLineBuilder<TContext>
    {
        /// <summary>
        /// 管线名称
        /// </summary>
        string ProductLineName { get; }
        /// <summary>
        /// 服务提供者
        /// </summary>
        IServiceProvider ServiceProvider { get; }
        /// <summary>
        /// 构建
        /// </summary>
        /// <returns></returns>
        RenderingProductionLineDelegate<TContext> Build();
        /// <summary>
        /// 清除所有中间件
        /// </summary>
        void Clear();
        /// <summary>
        /// 添加管线中间件
        /// </summary>
        /// <param name="middleware"></param>
        /// <returns></returns>
        IProductionLineBuilder<TContext> Use(Func<RenderingProductionLineDelegate<TContext>, RenderingProductionLineDelegate<TContext>> middleware);
        /// <summary>
        /// 短路中间件
        /// </summary>
        /// <param name="endPoint"></param>
        /// <returns></returns>
        IProductionLineBuilder<TContext> Run(RenderingProductionLineDelegate<TContext> endPoint);
        /// <summary>
        /// 添加中间件
        /// </summary>
        /// <typeparam name="TMiddleware"></typeparam>
        /// <returns></returns>
        IProductionLineBuilder<TContext> UseMiddleware<TMiddleware>()
            where TMiddleware : class, IProductionLineMiddleware<TContext>;
    }
}
