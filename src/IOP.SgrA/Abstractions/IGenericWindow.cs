﻿using System;
using System.Threading.Tasks;

namespace IOP.SgrA
{
    /// <summary>
    /// 泛型窗口
    /// </summary>
    public interface IGenericWindow : IDisposable
    {
        /// <summary>
        /// 名称
        /// </summary>
        string Name { get; }
        /// <summary>
        /// 窗口配置
        /// </summary>
        WindowOption Option { get; }
        /// <summary>
        /// 初始化
        /// </summary>
        void Initialization();
        /// <summary>
        /// 运行窗口
        /// </summary>
        void RunWindow();
        /// <summary>
        /// 运行窗口
        /// </summary>
        /// <param name="updateRate"></param>
        void RunWindow(double updateRate);
        /// <summary>
        /// 当加载结束时
        /// </summary>
        /// <param name="args"></param>
        void OnLoaded(EventArgs args);
        /// <summary>
        /// 运行配置任务
        /// </summary>
        /// <returns></returns>
        Task RunConfigTask();
    }
}
