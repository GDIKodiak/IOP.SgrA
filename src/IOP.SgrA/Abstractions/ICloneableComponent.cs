﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 可被拷贝的组件
    /// </summary>
    public interface ICloneableComponent
    {
        /// <summary>
        /// 拷贝组件至新的渲染对象
        /// </summary>
        /// <returns></returns>
        void CloneToNewRender(IRenderObject newObj);
    }
}
