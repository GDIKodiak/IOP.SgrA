﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 场景模块
    /// </summary>
    public interface ISceneModule
    {
        /// <summary>
        /// 所属场景
        /// </summary>
        Scene Scene { get; set; }
    }
}
