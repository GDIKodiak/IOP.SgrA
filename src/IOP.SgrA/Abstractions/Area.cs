﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 区域
    /// </summary>
    public struct Area
    {
        /// <summary>
        /// X坐标
        /// </summary>
        public int X;
        /// <summary>
        /// Y坐标
        /// </summary>
        public int Y;
        /// <summary>
        /// 宽度
        /// </summary>
        public uint Width;
        /// <summary>
        /// 高度
        /// </summary>
        public uint Height;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public Area(int x, int y, uint width, uint height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }
    }
}
