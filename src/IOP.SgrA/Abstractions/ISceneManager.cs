﻿using System;
using System.Threading.Tasks;

namespace IOP.SgrA
{
    /// <summary>
    /// 场景管理器接口
    /// </summary>
    public interface ISceneManager : IDisposable
    {
        /// <summary>
        /// 新建场景
        /// </summary>
        /// <typeparam name="TScene"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        TScene CreateNewScene<TScene>(string name) where TScene : class, IScene;
        /// <summary>
        /// 获取场景
        /// </summary>
        /// <typeparam name="TScene"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        TScene GetScene<TScene>(string name) where TScene : class, IScene;
        /// <summary>
        /// 删除场景
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        bool DeleteScene(string name);
        /// <summary>
        /// 加载场景
        /// </summary>
        /// <typeparam name="TScene"></typeparam>
        /// <returns></returns>
        Task LoadScene<TScene>(string name, params object[] param) 
            where TScene : class, IScene, ILoadableScene<TScene>;
        /// <summary>
        /// 加载场景
        /// </summary>
        /// <typeparam name="TScene"></typeparam>
        /// <param name="loadFinish"></param>
        /// <param name="name"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        Task LoadScene<TScene>(string name, Action<TScene> loadFinish, params object[] param) 
            where TScene : class, IScene, ILoadableScene<TScene>;
        /// <summary>
        /// 卸载场景
        /// </summary>
        /// <typeparam name="TScene"></typeparam>
        /// <returns></returns>
        Task UnLoadScene<TScene>(string name, params object[] param) 
            where TScene : class, IScene, ILoadableScene<TScene>;
        /// <summary>
        /// 卸载场景
        /// </summary>
        /// <typeparam name="TScene"></typeparam>
        /// <param name="name"></param>
        /// <param name="unloadFinish"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        Task UnLoadScene<TScene>(string name, Action<TScene> unloadFinish, params object[] param) 
            where TScene : class, IScene, ILoadableScene<TScene>;
    }
}
