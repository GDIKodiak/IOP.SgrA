﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace IOP.SgrA
{
    /// <summary>
    /// SgrA启动项
    /// </summary>
    public interface ISgrAStartup
    {
        /// <summary>
        /// 配置服务
        /// </summary>
        /// <param name="services"></param>
        void ConfigureServices(IServiceCollection services);

        /// <summary>
        /// 构建函数
        /// </summary>
        /// <param name="server"></param>
        /// <param name="env"></param>
        void Configure(IGraphicsBuilder server, IHostEnvironment env);
    }
}
