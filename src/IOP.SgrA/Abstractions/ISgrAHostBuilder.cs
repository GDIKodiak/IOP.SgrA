﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 主机构建者接口
    /// </summary>
    public interface ISgrAHostBuilder
    {
        /// <summary>
        /// 构建主机
        /// </summary>
        void Build();

        /// <summary>
        /// 配置服务
        /// </summary>
        /// <param name="configureServices"></param>
        /// <returns></returns>
        ISgrAHostBuilder ConfigureServices(Action<IServiceCollection> configureServices);
        /// <summary>
        /// 配置服务
        /// </summary>
        /// <param name="configureServices"></param>
        /// <returns></returns>
        ISgrAHostBuilder ConfigureServices(Action<HostBuilderContext, IServiceCollection> configureServices);
        /// <summary>
        /// 配置服务器
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        ISgrAHostBuilder Configure(Action<IGraphicsBuilder, IHostEnvironment> builder);
    }
}
