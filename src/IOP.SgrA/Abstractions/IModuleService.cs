﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IOP.SgrA
{
    /// <summary>
    /// 模块服务
    /// </summary>
    public interface IModuleService : IDisposable
    {
        /// <summary>
        /// 创建模块
        /// </summary>
        /// <param name="types"></param>
        /// <returns></returns>
        ModuleInfo[] CreateModules(params Type[] types);
        /// <summary>
        /// 创建模块
        /// </summary>
        /// <param name="extraModule"></param>
        /// <param name="types"></param>
        /// <returns></returns>
        ModuleInfo[] CreateModules(Dictionary<Type, IGraphicsModule> extraModule, params Type[] types);
    }
}
