﻿namespace IOP.SgrA
{
    /// <summary>
    /// 模块信息
    /// </summary>
    public class ModuleInfo
    {
        /// <summary>
        /// 模块优先级
        /// </summary>
        public ModulePriority ModulePriority { get; private set; }
        /// <summary>
        /// 模块
        /// </summary>
        public IGraphicsModule Module { get; private set; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="modulePriority"></param>
        /// <param name="module"></param>
        public ModuleInfo(ModulePriority modulePriority, IGraphicsModule module)
        {
            ModulePriority = modulePriority;
            Module = module;
        }
    }
}
