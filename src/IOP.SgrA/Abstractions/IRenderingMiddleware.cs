﻿namespace IOP.SgrA
{
    /// <summary>
    /// 渲染中间件
    /// </summary>
    /// <typeparam name="TContext"></typeparam>
    public interface IRenderingMiddleware<TContext>
    {
        /// <summary>
        /// 执行
        /// </summary>
        /// <param name="context">上下文</param>
        /// <param name="next">下一个中间件</param>
        /// <returns></returns>
        void InvokeAsync(TContext context, RenderingProductionLineDelegate<TContext> next);
    }
}
