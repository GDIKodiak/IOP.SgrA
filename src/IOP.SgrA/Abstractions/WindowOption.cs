﻿using Microsoft.Extensions.Options;
using System;
using System.Numerics;

namespace IOP.SgrA
{
    /// <summary>
    /// Vulkan窗口配置
    /// </summary>
    public class WindowOption : IOptions<WindowOption>
    {
        /// <summary>
        /// 宽度
        /// </summary>
        public uint Width { get; set; } = 800;
        /// <summary>
        /// 长度
        /// </summary>
        public uint Height { get; set; } = 600;
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; } = "";
        /// <summary>
        /// 刷新率
        /// </summary>
        public double UpdateRate { get; set; } = 60.0;
        /// <summary>
        /// 监视器内容缩放比例
        /// </summary>
        public Vector2 ContentScale { get; internal set; } = Vector2.One;
        /// <summary>
        /// 线程渲染间隔执行模式
        /// </summary>
        public ThreadIntervalMode IntervalMode { get; set; } = ThreadIntervalMode.YieldMode;

        /// <summary>
        /// 值
        /// </summary>
        public WindowOption Value => this;
    }

    /// <summary>
    /// 线程渲染间隔执行模式
    /// </summary>
    public enum ThreadIntervalMode
    {
        /// <summary>
        /// Yield模式
        /// </summary>
        YieldMode,
        /// <summary>
        /// Sleep模式
        /// </summary>
        SleepMode
    }
}
