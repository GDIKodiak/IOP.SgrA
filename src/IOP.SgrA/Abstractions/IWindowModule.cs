﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 窗口模块
    /// </summary>
    public interface IWindowModule<TWindow>
        where TWindow : IGenericWindow
    {
        /// <summary>
        /// 窗口
        /// </summary>
        TWindow Window { get; set; }
    }
}
