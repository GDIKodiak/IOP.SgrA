﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 渲染管线委托
    /// </summary>
    /// <typeparam name="TContext"></typeparam>
    /// <param name="context"></param>
    public delegate void RenderingProductionLineDelegate<in TContext>(TContext context);
}
