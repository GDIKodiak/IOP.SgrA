﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 渲染管线构建者
    /// </summary>
    public interface IRenderingProductionLineBuilder<TContext>
    {
        /// <summary>
        /// 服务提供者
        /// </summary>
        public IServiceProvider Services { get; }

        /// <summary>
        /// 构建管线
        /// </summary>
        /// <returns></returns>
        RenderingProductionLineDelegate<TContext> Build();

        /// <summary>
        /// 添加管线中间件
        /// </summary>
        /// <param name="middleware"></param>
        /// <returns></returns>
        IRenderingProductionLineBuilder<TContext> Use(Func<RenderingProductionLineDelegate<TContext>, RenderingProductionLineDelegate<TContext>> middleware);

        /// <summary>
        /// 短路中间件
        /// </summary>
        /// <param name="endPoint"></param>
        /// <returns></returns>
        IRenderingProductionLineBuilder<TContext> Run(RenderingProductionLineDelegate<TContext> endPoint);

        /// <summary>
        /// 添加中间件
        /// </summary>
        /// <typeparam name="TMiddleware"></typeparam>
        /// <returns></returns>
        IRenderingProductionLineBuilder<TContext> UseMiddleware<TMiddleware>()
            where TMiddleware : class, IRenderingMiddleware<TContext>;
    }
}
