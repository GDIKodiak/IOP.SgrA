﻿using IOP.SgrA.ECS;
using System;
using System.Reflection;

namespace IOP.SgrA
{
    /// <summary>
    /// 上下文管理器
    /// </summary>
    public interface IContextManager : IDisposable
    {
        /// <summary>
        /// 获取上下文
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        Context GetContext(int index);
        /// <summary>
        /// 获取上下文
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Context GetContext(Entity entity);

        /// <summary>
        /// 创建上下文
        /// </summary>
        /// <param name="size"></param>
        /// <param name="archetype"></param>
        /// <param name="renderGroup"></param>
        /// <param name="renderObject"></param>
        /// <param name="deepCopy"></param>
        /// <returns></returns>
        void CreateContexts(int size, Archetype archetype, IRenderGroup renderGroup, IRenderObject renderObject, bool deepCopy = false);
        /// <summary>
        /// 创建上下文
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="size"></param>
        /// <param name="archetype"></param>
        /// <param name="renderGroup"></param>
        /// <param name="renderObject"></param>
        /// <param name="deepCopy"></param>
        void CreateContexts(Context parent, int size, Archetype archetype, IRenderGroup renderGroup, IRenderObject renderObject, bool deepCopy = false);
        /// <summary>
        /// 创建上下文
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="size"></param>
        /// <param name="archetype"></param>
        /// <param name="renderGroup"></param>
        /// <param name="renderObject"></param>
        /// <param name="initData"></param>
        /// <param name="deepCopy"></param>
        void CreateContexts<TGroup>(int size, Archetype archetype, IRenderGroup renderGroup, IRenderObject renderObject, TGroup initData, bool deepCopy = false) where TGroup : struct;
        /// <summary>
        /// 创建上下文
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="parent"></param>
        /// <param name="size"></param>
        /// <param name="archetype"></param>
        /// <param name="renderGroup"></param>
        /// <param name="renderObject"></param>
        /// <param name="initData"></param>
        /// <param name="deepCopy"></param>
        void CreateContexts<TGroup>(Context parent, int size, Archetype archetype, IRenderGroup renderGroup, IRenderObject renderObject, TGroup initData, bool deepCopy = false) where TGroup : struct;
        /// <summary>
        /// 创建上下文
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="size"></param>
        /// <param name="archetype"></param>
        /// <param name="renderGroup"></param>
        /// <param name="renderObject"></param>
        /// <param name="offset"></param>
        /// <param name="deepCopy"></param>
        void CreateContexts(Context parent, int size, Archetype archetype, IRenderGroup renderGroup, IRenderObject renderObject, Transform offset, bool deepCopy = false);
        /// <summary>
        /// 创建上下文
        /// </summary>
        /// <param name="size"></param>
        /// <param name="archetype"></param>
        /// <param name="renderGroup"></param>
        /// <param name="renderObject"></param>
        /// <param name="transform"></param>
        /// <param name="deepCopy"></param>
        void CreateContexts(int size, Archetype archetype, IRenderGroup renderGroup, IRenderObject renderObject, Transform transform, bool deepCopy = false);

        /// <summary>
        /// 移除上下文
        /// </summary>
        /// <param name="index"></param>
        void RemoveContext(int index);
        /// <summary>
        /// 移除上下文
        /// </summary>
        /// <param name="entity"></param>
        void RemoveContext(Entity entity);
        /// <summary>
        /// 移除上下文
        /// </summary>
        /// <param name="indexes"></param>
        void RemoveContexts(int[] indexes);
        /// <summary>
        /// 移除上下文
        /// </summary>
        /// <param name="entities"></param>
        void RemoveContexts(Entity[] entities);

        /// <summary>
        /// 获取一个渲染目标原型
        /// </summary>
        /// <returns></returns>
        Archetype RenderTargetArchetype();
        /// <summary>
        /// 获取一个无主的原型
        /// </summary>
        /// <returns></returns>
        Archetype UnownedArchetype();
        /// <summary>
        /// 创建原型
        /// </summary>
        /// <param name="components"></param>
        /// <returns></returns>
        Archetype CreateArchetype(params IComponentData[] components);
        /// <summary>
        /// 创建原型
        /// </summary>
        /// <param name="name"></param>
        /// <param name="components"></param>
        /// <returns></returns>
        Archetype CreateArchetype(string name, params IComponentData[] components);
        /// <summary>
        /// 从指定结构体创建原型
        /// </summary>
        /// <typeparam name="TComponentGorup"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        Archetype CreateArchetype<TComponentGorup>(string name = "") where TComponentGorup : struct;
        /// <summary>
        /// 尝试获取原型
        /// </summary>
        /// <param name="name"></param>
        /// <param name="archetype"></param>
        /// <returns></returns>
        public bool TryGetArchetype(string name, out Archetype archetype);

        /// <summary>
        /// 添加组件程序集
        /// </summary>
        /// <param name="assembly"></param>
        void AddComponentAssembly(Assembly assembly);
        /// <summary>
        /// 加载组件
        /// </summary>
        void LoadComponents();
        /// <summary>
        /// 遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="foreachAction"></param>
        void Foreach<TGroup>(Action<TGroup> foreachAction) where TGroup : struct;
        /// <summary>
        /// 遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="foreachAction"></param>
        void Foreach<TGroup>(Action<TGroup, int> foreachAction) where TGroup : struct;
        /// <summary>
        /// 遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="foreachFunc"></param>
        void Foreach<TGroup>(Func<TGroup> foreachFunc) where TGroup : struct;
        /// <summary>
        /// 遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="foreachFunc"></param>
        void Foreach<TGroup>(Func<int, TGroup> foreachFunc) where TGroup : struct;
        /// <summary>
        /// 遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="foreachFunc"></param>
        void Foreach<TGroup>(Func<TGroup, int, TGroup> foreachFunc) where TGroup : struct;
        /// <summary>
        /// 遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="foreachFunc"></param>
        void Foreach<TGroup>(Func<TGroup, TGroup> foreachFunc) where TGroup : struct;
        /// <summary>
        /// 遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="handle"></param>
        void Foreach<TGroup>(RefHandle<TGroup> handle) where TGroup : struct;
        /// <summary>
        /// 获取数据
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="entity"></param>
        void GetData<TGroup>(Entity entity) where TGroup : struct;
        /// <summary>
        /// 写入数据
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="entity"></param>
        /// <param name="group"></param>
        void WriteData<TGroup>(Entity entity, TGroup group) where TGroup : struct;
        /// <summary>
        /// 刷新提交队列
        /// 此方法在多线程环境下调用将会导致不正确的结果
        /// </summary>
        void RefreshUpdateQueue();
    }
}
