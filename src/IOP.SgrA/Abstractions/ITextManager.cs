﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 文字管理器
    /// </summary>
    public interface ITextManager
    {
        /// <summary>
        /// 尝试获取字体
        /// </summary>
        /// <param name="name"></param>
        /// <param name="font"></param>
        /// <returns></returns>
        bool TryGetFont(string name, out IFont font);
        /// <summary>
        /// 添加字体
        /// </summary>
        /// <param name="font"></param>
        void AddFont(IFont font);
        /// <summary>
        /// 移除字体
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        bool RemoveFont(string name);
        /// <summary>
        /// 创建文字渲染对象
        /// </summary>
        /// <param name="text"></param>
        /// <param name="font"></param>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        IRenderObject GetOrCreateTextRender(string text, IFont font, int column, int row);
        /// <summary>
        /// 移除文字渲染器
        /// </summary>
        /// <param name="textRneder"></param>
        void RemoveTextRender(IRenderObject textRneder);
    }
}
