﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 模块优先级
    /// </summary>
    public enum ModulePriority
    {
        /// <summary>
        /// 核心
        /// </summary>
        Core = 0,
        /// <summary>
        /// IOP SgrA engine assets
        /// </summary>
        ISEA = 1,
        /// <summary>
        /// 渲染通道
        /// </summary>
        RenderPass = 2,
        /// <summary>
        /// 渲染组
        /// </summary>
        RenderGroup = 3,
        /// <summary>
        /// 资产
        /// </summary>
        Assets = 4,
        /// <summary>
        /// 优先级3
        /// </summary>
        Priority5 = 5,
        /// <summary>
        /// 优先级4
        /// </summary>
        Priority6 = 6,
        /// <summary>
        /// 优先级5
        /// </summary>
        Priority7 = 7,
        /// <summary>
        /// 没有优先级
        /// </summary>
        None = 128
    }
}
