﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace IOP.SgrA
{
    /// <summary>
    /// 通用纹理接口
    /// </summary>
    public interface ITextureData : IDisposable
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; }
        /// <summary>
        /// 宽度
        /// </summary>
        uint Width { get; }
        /// <summary>
        /// 高度
        /// </summary>
        uint Height { get; }
        /// <summary>
        /// 数组数量
        /// </summary>
        uint ArrayLayers { get; }
        /// <summary>
        /// 缓冲
        /// </summary>
        IntPtr Buffer { get; }
        /// <summary>
        /// 字节大小
        /// </summary>
        ulong BytesSize { get; }
        /// <summary>
        /// 加载纹理
        /// </summary>
        /// <returns></returns>
        Task Load(byte[] data);
        /// <summary>
        /// 加载纹理
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        Task Load(Stream stream);
        /// <summary>
        /// 加载纹理
        /// </summary>
        /// <param name="fileInfo"></param>
        /// <returns></returns>
        Task Load(FileInfo fileInfo);
        /// <summary>
        /// 更新内存数据
        /// </summary>
        /// <param name="updateData"></param>
        void UpdateMemoryData(byte[] updateData);
        /// <summary>
        /// 获取内存数据
        /// </summary>
        /// <returns></returns>
        Span<byte> GetMemoryData();
        /// <summary>
        /// 清理
        /// </summary>
        void Clear();
    }
}
