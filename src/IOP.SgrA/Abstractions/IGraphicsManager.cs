﻿using System;
using System.Threading;
using System.Numerics;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace IOP.SgrA
{
    /// <summary>
    /// 图形管理器接口
    /// </summary>
    public interface IGraphicsManager : IDisposable
    {
        /// <summary>
        /// 图形接口名称
        /// </summary>
        string GraphicsName { get; }
        /// <summary>
        /// 配置项根节点名
        /// </summary>
        string ConfigurationRootName { get; }
        /// <summary>
        /// 服务提供者
        /// </summary>
        IServiceProvider ServiceProvider { get; }
        /// <summary>
        /// 获取绘图管理器服务
        /// </summary>
        /// <typeparam name="TService"></typeparam>
        /// <returns></returns>
        TService GetGraphicsManagerService<TService>() where TService : IGraphicsManagerService;
        /// <summary>
        /// 创建窗口
        /// </summary>
        /// <typeparam name="TWindow"></typeparam>
        /// <param name="name"></param>
        /// <param name="option"></param>
        /// <returns></returns>
        TWindow CreateWindow<TWindow>(string name, WindowOption option) where TWindow : class, IGenericWindow;
        /// <summary>
        /// 摧毁窗口
        /// </summary>
        /// <param name="window"></param>
        void DestroyWindow(IGenericWindow window);
        /// <summary>
        /// 获取上下文管理器
        /// </summary>
        /// <returns></returns>
        IContextManager GetContextManager();

        /// <summary>
        /// 加载着色器
        /// </summary>
        /// <param name="shaders"></param>
        void LoadShaders(ShaderInfo[] shaders);
        /// <summary>
        /// 加载着色器
        /// </summary>
        /// <param name="shaders"></param>
        /// <returns></returns>
        Task LoadShadersAsync(ShaderInfo[] shaders);
        /// <summary>
        /// 删除着色器
        /// </summary>
        /// <param name="shaders"></param>
        void DeleteShaders(ShaderInfo[] shaders);

        /// <summary>
        /// 尝试获取渲染组
        /// </summary>
        /// <param name="name"></param>
        /// <param name="group"></param>
        /// <returns></returns>
        bool TryGetRenderGroup(string name, out IRenderGroup group);
        /// <summary>
        /// 是否包含渲染组
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        bool IsContainsGroup(string name);

        /// <summary>
        /// 创建网格
        /// </summary>
        /// <param name="name"></param>
        /// <param name="data"></param>
        /// <param name="vectexCount"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        IMesh CreateMesh(string name, Vector3[] data, uint vectexCount, Vector3 min, Vector3 max);
        /// <summary>
        /// 创建索引网格
        /// </summary>
        /// <param name="name"></param>
        /// <param name="data"></param>
        /// <param name="indexed"></param>
        /// <param name="vectexCount"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        IMesh CreateIndexedMesh(string name, Vector3[] data, uint[] indexed, uint vectexCount, Vector3 min, Vector3 max);
        /// <summary>
        /// 创建网格组件
        /// </summary>
        /// <typeparam name="TV"></typeparam>
        /// <param name="name"></param>
        /// <param name="data"></param>
        /// <param name="vertexCount"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        IMesh CreateMesh<TV>(string name, TV[] data, uint vertexCount, Vector3 min, Vector3 max)
            where TV : unmanaged;

        /// <summary>
        /// 创建一致性缓冲
        /// </summary>
        /// <param name="size"></param>
        /// <param name="binding"></param>
        /// <param name="arrayElement"></param>
        /// <param name="descriptorCount"></param>
        /// <param name="setIndex"></param>
        /// <returns></returns>
        ITexture CreateUniformBuffer(ulong size, uint binding = 0, uint arrayElement = 0,
           uint descriptorCount = 1, uint setIndex = 0);

        /// <summary>
        /// 创建渲染对象
        /// </summary>
        /// <typeparam name="TRenderObject"></typeparam>
        /// <param name="name"></param>
        /// <param name="components"></param>
        /// <returns></returns>
        TRenderObject CreateRenderObject<TRenderObject>(string name, params IRenderComponent[] components)
            where TRenderObject : RenderObject, new();
        /// <summary>
        /// 尝试获取渲染对象
        /// </summary>
        /// <typeparam name="TRenderObject"></typeparam>
        /// <param name="name"></param>
        /// <param name="renderObject"></param>
        /// <returns></returns>
        bool TryGetRenderObject<TRenderObject>(string name, out TRenderObject renderObject)
            where TRenderObject : RenderObject, new();
        /// <summary>
        /// 移除渲染对象
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        bool RemoveRenderObject(string name);

        /// <summary>
        /// 在指定场景中创建上下文
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="scene"></param>
        /// <param name="size"></param>
        /// <param name="archetypeName"></param>
        /// <param name="groupName"></param>
        /// <param name="renderObject"></param>
        /// <param name="initData"></param>
        /// <param name="deepCopy"></param>
        void CreateContext<TGroup>(Scene scene, int size, string archetypeName, string groupName, IRenderObject renderObject, TGroup initData, bool deepCopy = false)
            where TGroup : struct;
        /// <summary>
        /// 在指定场景中创建上下文
        /// </summary>
        /// <param name="scene"></param>
        /// <param name="size"></param>
        /// <param name="archetypeName"></param>
        /// <param name="groupName"></param>
        /// <param name="renderObject"></param>
        /// <param name="deepCopy"></param>
        void CreateContext(Scene scene, int size, string archetypeName, string groupName, IRenderObject renderObject, bool deepCopy = false);

        /// <summary>
        /// 创建一个动画并附加到一个渲染对象上
        /// </summary>
        /// <typeparam name="TRenderObject"></typeparam>
        /// <typeparam name="TAnimation"></typeparam>
        /// <param name="object"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        TRenderObject CreateAnimation<TRenderObject, TAnimation>(TRenderObject @object, string name)
            where TRenderObject : IRenderObject
            where TAnimation : Animation;
        /// <summary>
        /// 创建粒子发生器并附加到一个渲染对象上
        /// </summary>
        /// <typeparam name="TRenderObject"></typeparam>
        /// <typeparam name="TParticle"></typeparam>
        /// <param name="object"></param>
        /// <param name="contextManager"></param>
        /// <param name="particleObject"></param>
        /// <param name="renderGroup"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        TRenderObject CreateParticleGenerator<TRenderObject, TParticle>(TRenderObject @object, string name,
            IContextManager contextManager, IRenderObject particleObject, IRenderGroup renderGroup)
            where TRenderObject : IRenderObject
            where TParticle : ParticleGenerator;
        /// <summary>
        /// 创建一个粒子发生器并附加到一个空的渲染对象上
        /// </summary>
        /// <typeparam name="TParticle"></typeparam>
        /// <typeparam name="TRenderObject"></typeparam>
        /// <param name="contextManager"></param>
        /// <param name="particleObject"></param>
        /// <param name="renderGroup"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        TRenderObject CreateParticleGenerator<TRenderObject, TParticle>(string name, IContextManager contextManager, IRenderObject particleObject, IRenderGroup renderGroup)
            where TRenderObject : RenderObject, new()
            where TParticle : ParticleGenerator;
        
        /// <summary>
        /// 创建摄影机
        /// </summary>
        /// <param name="name"></param>
        /// <param name="position"></param>
        /// <param name="target"></param>
        /// <param name="up"></param>
        /// <returns></returns>
        Camera CreateCamera(string name, Vector3 position, Vector3 target, Vector3 up);
        /// <summary>
        /// 尝试获取摄影机
        /// </summary>
        /// <param name="name"></param>
        /// <param name="camera"></param>
        /// <returns></returns>
        bool TryGetCamera(string name, out Camera camera);
        /// <summary>
        /// 遍历摄影机
        /// </summary>
        /// <returns></returns>
        IEnumerable<Camera> GetCameras();

        /// <summary>
        /// 获取新的Id
        /// </summary>
        /// <returns></returns>
        int GetNewId();
        /// <summary>
        /// 获取一个新的字符串令牌
        /// </summary>
        /// <returns></returns>
        string NewStringToken();
        /// <summary>
        /// 管理器检查
        /// </summary>
        /// <returns></returns>
        (bool, string) ManagerCheck();
    }
}
