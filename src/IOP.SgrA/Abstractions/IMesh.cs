﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 网格接口
    /// </summary>
    public interface IMesh : IRenderComponent
    {
        /// <summary>
        /// Id
        /// </summary>
        int Id { get; }
        /// <summary>
        /// 顶点数量
        /// </summary>
        uint VecticesCount { get; set; }
        /// <summary>
        /// 索引数量
        /// </summary>
        uint IndexesCount { get; set; }
        /// <summary>
        /// LOD级别
        /// </summary>
        int LODLevel { get; set; }
        /// <summary>
        /// 最小值向量
        /// </summary>
        Vector3 MinVector { get; set; }
        /// <summary>
        /// 最大值向量
        /// </summary>
        Vector3 MaxVector { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="minVector"></param>
        /// <param name="maxVector"></param>
        /// <param name="offset"></param>
        void UpdateMeshData(Span<byte> data, Vector3 minVector, Vector3 maxVector, uint offset = 0);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="minVector"></param>
        /// <param name="maxVector"></param>
        /// <param name="offset"></param>
        void UpdateMeshData(byte[] data, Vector3 minVector, Vector3 maxVector, uint offset = 0);
    }
}
