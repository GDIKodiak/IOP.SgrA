﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using IOP.Extension.DependencyInjection;

namespace IOP.SgrA
{
    /// <summary>
    /// 管线构建者
    /// </summary>
    /// <typeparam name="TContext"></typeparam>
    public class ProductionLineBuilder<TContext> : IProductionLineBuilder<TContext>
    {
        /// <summary>
        /// 管线名
        /// </summary>
        public string ProductLineName { get; }

        /// <summary>
        /// 服务提供者
        /// </summary>
        public IServiceProvider ServiceProvider { get; }

        private readonly ILogger<ProductionLineBuilder<TContext>> Logger;

        /// <summary>
        /// 中间件列表
        /// </summary>
        private readonly IList<Func<RenderingProductionLineDelegate<TContext>, RenderingProductionLineDelegate<TContext>>> _Middlewares =
            new List<Func<RenderingProductionLineDelegate<TContext>, RenderingProductionLineDelegate<TContext>>>();

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="name"></param>
        /// <param name="service"></param>
        public ProductionLineBuilder(string name, IServiceProvider service)
        {
            if (string.IsNullOrEmpty(name)) name = Guid.NewGuid().ToString("N");
            if (service == null) throw new ArgumentNullException(nameof(service));
            ProductLineName = name;
            ServiceProvider = service;
            Logger = service.GetRequiredService<ILogger<ProductionLineBuilder<TContext>>>();
        }

        /// <summary>
        /// 构建管线
        /// </summary>
        /// <returns></returns>
        public RenderingProductionLineDelegate<TContext> Build()
        {
            RenderingProductionLineDelegate<TContext> productline = next => { return; };
            foreach (var middleware in _Middlewares.Reverse())
            {
                productline = middleware(productline);
            }

            return productline;
        }
        /// <summary>
        /// 清理
        /// </summary>
        public void Clear()
        {
            _Middlewares.Clear();
        }
        /// <summary>
        /// 添加短路中间件
        /// </summary>
        /// <param name="endPoint"></param>
        /// <returns></returns>
        public IProductionLineBuilder<TContext> Run(RenderingProductionLineDelegate<TContext> endPoint)
        {
            endPoint = endPoint ?? throw new ArgumentNullException(nameof(endPoint));
            return Use(next => endPoint);
        }
        /// <summary>
        /// 添加中间件
        /// </summary>
        /// <param name="middleware"></param>
        /// <returns></returns>
        public IProductionLineBuilder<TContext> Use(Func<RenderingProductionLineDelegate<TContext>, RenderingProductionLineDelegate<TContext>> middleware)
        {
            _Middlewares.Add(middleware);
            return this;
        }
        /// <summary>
        /// 添加中间件
        /// </summary>
        /// <typeparam name="TMiddleware"></typeparam>
        /// <returns></returns>
        public IProductionLineBuilder<TContext> UseMiddleware<TMiddleware>()
            where TMiddleware : class, IProductionLineMiddleware<TContext>
        {
            try
            {
                var middleware = ServiceProvider.CreateAutowiredInstance<TMiddleware>();
                return Use(next =>
                {
                    return context => { middleware.Invoke(context, next); };
                });
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message + "\r\n" + e.StackTrace);
                return this;
            }
        }
    }
}
