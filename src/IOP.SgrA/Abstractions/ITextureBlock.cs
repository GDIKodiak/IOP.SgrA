﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA
{
    /// <summary>
    /// 纹理块
    /// </summary>
    public interface ITextureBlock : IRenderComponent
    {
        /// <summary>
        /// 块大小
        /// </summary>
        uint BlockSize {  get; }
        /// <summary>
        /// 块偏移
        /// </summary>
        uint BlockOffset {  get; }
        /// <summary>
        /// 所属纹理
        /// </summary>
        ITextureData Buffer { get; }
        /// <summary>
        /// 更新块数据
        /// </summary>
        /// <param name="updateData"></param>
        void UpdateBlockData(byte[] updateData);
        /// <summary>
        /// 更新块数据
        /// </summary>
        /// <param name="updateData"></param>
        void UpdateBlockData(Span<byte> updateData);
        /// <summary>
        /// 更新块数据
        /// </summary>
        /// <typeparam name="TStruct"></typeparam>
        /// <param name="struct"></param>
        void UpdateBlockData<TStruct>(TStruct @struct) where TStruct : unmanaged;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="updateData"></param>
        /// <param name="offset"></param>
        void UpdateBlockData(byte[] updateData, uint offset);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="updateData"></param>
        /// <param name="offset"></param>
        void UpdateBlockData(Span<byte> updateData, uint offset);
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TStruct"></typeparam>
        /// <param name="struct"></param>
        /// <param name="offset"></param>
        void UpdateBlockData<TStruct>(TStruct @struct, uint offset) where TStruct : unmanaged;
    }
}
