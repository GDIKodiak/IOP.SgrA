﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA
{
    /// <summary>
    /// 可变换对象
    /// </summary>
    public interface ITransformable
    {
        /// <summary>
        /// 获取变换
        /// </summary>
        /// <returns></returns>
        Transform GetTransform();
    }
}
