﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 纹理接口
    /// </summary>
    public interface ITexture : IDescriptorObject, IRenderComponent
    {
        /// <summary>
        /// 宽度
        /// </summary>
        public uint Width { get; }
        /// <summary>
        /// 长度
        /// </summary>
        public uint Height { get; }
        /// <summary>
        /// 纹理数组数
        /// </summary>
        public uint ArrayLayers { get; }
        /// <summary>
        /// 描述符集下标
        /// </summary>
        public uint SetIndex { get; }
        /// <summary>
        /// 纹理绑定位置
        /// </summary>
        public uint Binding { get; }
        /// <summary>
        /// 变更采样器
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        bool UpdateSampler(ISampler sampler);
        /// <summary>
        /// 变更纹理数据
        /// </summary>
        /// <param name="textureData"></param>
        /// <returns></returns>
        bool UpdateTextureData(ITextureData textureData);
        /// <summary>
        /// 更新纹理内存数据
        /// </summary>
        /// <param name="updateData"></param>
        void UpdateTextureMemoryData(byte[] updateData);
        /// <summary>
        /// 更新纹理内存数据
        /// </summary>
        /// <param name="updateData"></param>
        /// <param name="startIndex"></param>
        void UpdateTextureMemoryData(byte[] updateData, int startIndex);
        /// <summary>
        /// 更新纹理内存数据
        /// </summary>
        /// <param name="updateData"></param>
        void UpdateTextureMemoryData(Span<byte> updateData);
        /// <summary>
        /// 更新纹理内存数据
        /// </summary>
        /// <param name="updateData"></param>
        /// <param name="startIndex"></param>
        void UpdateTextureMemoryData(Span<byte> updateData, int startIndex);
        /// <summary>
        /// 更新纹理内存数据
        /// </summary>
        /// <typeparam name="TStruct"></typeparam>
        /// <param name="struct"></param>
        void UpdateTextureMemoryData<TStruct>(TStruct @struct) where TStruct : unmanaged;
        /// <summary>
        /// 更新纹理数据
        /// </summary>
        /// <typeparam name="TStruct"></typeparam>
        /// <param name="struct"></param>
        /// <param name="startIndex"></param>
        void UpdateTextureMemoryData<TStruct>(TStruct @struct, int startIndex) where TStruct : unmanaged;
        /// <summary>
        /// 获取纹理内存数据
        /// </summary>
        /// <returns></returns>
        Span<byte> GetTextureMemoryData();
        /// <summary>
        /// 深度拷贝
        /// </summary>
        /// <returns></returns>
        ITexture DeepCopy();
    }
}
