﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA
{
    /// <summary>
    /// 光源环境接口
    /// </summary>
    public interface ILightEnvironment
    {
        /// <summary>
        /// 所属场景
        /// </summary>
        Scene Owner { get; }
        /// <summary>
        /// 场景全局环境光
        /// </summary>
        Ambient Ambient { get; set; }
        /// <summary>
        /// 点光源
        /// </summary>
        IEnumerable<PointLightInterop> PointLights { get; }
        /// <summary>
        /// 平行光源
        /// </summary>
        IEnumerable<ParallelLightInterop> ParallelLights { get; }
        /// <summary>
        /// 获取当前平行光源数量
        /// </summary>
        /// <returns></returns>
        uint GetParallelLightCount();
        /// <summary>
        /// 获取当前点光源数量
        /// </summary>
        /// <returns></returns>
        uint GetPointLightCount();
        /// <summary>
        /// 添加平行光源
        /// </summary>
        /// <param name="lightName"></param>
        /// <param name="direction"></param>
        /// <param name="color"></param>
        /// <returns></returns>
        string AddParallelLight(string lightName, Vector3 direction, Vector4 color);
        /// <summary>
        /// 添加点光源
        /// </summary>
        /// <param name="lightName"></param>
        /// <param name="position"></param>
        /// <param name="color"></param>
        string AddPointLight(string lightName, Vector3 position, Vector4 color);
        /// <summary>
        /// 添加平行光源
        /// </summary>
        /// <param name="name"></param>
        void RemoveParallelLight(string name);
        /// <summary>
        /// 移除点光源
        /// </summary>
        /// <param name="name"></param>
        void RemovePointLight(string name);
        /// <summary>
        /// 变更点光源
        /// </summary>
        /// <param name="name"></param>
        /// <param name="newPosition"></param>
        void ChangePointLight(string name, Vector3 newPosition);
        /// <summary>
        /// 变更点光源
        /// </summary>
        /// <param name="name"></param>
        /// <param name="newColor"></param>
        void ChangePointLight(string name, Vector4 newColor);
        /// <summary>
        /// 变更点光源
        /// </summary>
        /// <param name="newPosition"></param>
        /// <param name="newColor"></param>
        /// <param name="name"></param>
        void ChangePointLight(string name, Vector3 newPosition, Vector4 newColor);
        /// <summary>
        /// 变更平行光源
        /// </summary>
        /// <param name="name"></param>
        /// <param name="newDirection"></param>
        void ChangeParallelLight(string name, Vector3 newDirection);
        /// <summary>
        /// 更新平行光源
        /// </summary>
        /// <param name="name"></param>
        /// <param name="newColor"></param>
        void ChangeParallelLight(string name, Vector4 newColor);
        /// <summary>
        /// 更新平行光源
        /// </summary>
        /// <param name="name"></param>
        /// <param name="newDirection"></param>
        /// <param name="newColor"></param>
        void ChangeParallelLight(string name, Vector3 newDirection, Vector4 newColor);
        /// <summary>
        /// 同步光源缓冲
        /// </summary>
        void SyncLightBuffer();
    }
}
