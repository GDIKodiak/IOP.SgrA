﻿using System;

namespace IOP.SgrA
{
    /// <summary>
    /// 通用着色器管线接口
    /// </summary>
    public interface IShaderPipeline : IDisposable
    {
        /// <summary>
        /// 着色器名
        /// </summary>
        string Name { get; }
        /// <summary>
        /// 着色器Id
        /// </summary>
        int Id { get; }
        /// <summary>
        /// 是否构建完成
        /// </summary>
        /// <returns></returns>
        (bool, string) IsBuildFinish();
    }
    /// <summary>
    /// 通用着色器管线接口
    /// </summary>
    public interface IShaderPipeline<Tpipeline> : IShaderPipeline
    {
        /// <summary>
        /// 具体着色器程序对象
        /// </summary>
        Tpipeline Pipeline { get; }
    }
}
