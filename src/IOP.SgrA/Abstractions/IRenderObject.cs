﻿using System;
using System.Collections.Generic;

namespace IOP.SgrA
{
    /// <summary>
    /// 统一渲染对象接口
    /// </summary>
    public interface IRenderObject : ITransformable, IBounded
    {
        /// <summary>
        /// 
        /// </summary>
        string Name { get; }
        /// <summary>
        /// 对象Id
        /// </summary>
        int Id { get; }
        /// <summary>
        /// 当前渲染对象LOD级别
        /// </summary>
        int LODLevel { get; set; }

        /// <summary>
        /// 添加组件
        /// </summary>
        /// <param name="component"></param>
        void AddComponent(IRenderComponent component);
        /// <summary>
        /// 添加或者更新组件
        /// </summary>
        /// <param name="component"></param>
        /// <param name="func"></param>
        void AddOrUpdateComponent(IRenderComponent component, Func<string, IRenderComponent, IRenderComponent> func);
        /// <summary>
        /// 添加多个组件
        /// </summary>
        /// <param name="component"></param>
        void AddComponents(params IRenderComponent[] component);
        /// <summary>
        /// 获取多个组件
        /// </summary>
        /// <typeparam name="TComponent"></typeparam>
        /// <returns></returns>
        IEnumerable<TComponent> GetComponents<TComponent>() where TComponent : IRenderComponent;
        /// <summary>
        /// 获取组件
        /// </summary>
        /// <typeparam name="TComponent"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        TComponent GetComponent<TComponent>(string name) where TComponent : IRenderComponent;
        /// <summary>
        /// 尝试获取组件
        /// </summary>
        /// <typeparam name="TComponent"></typeparam>
        /// <param name="name"></param>
        /// <param name="component"></param>
        /// <returns></returns>
        bool TryGetComponent<TComponent>(string name, out TComponent component) where TComponent : IRenderComponent;
        /// <summary>
        /// 尝试获取渲染组组件（可能不存在）
        /// </summary>
        /// <param name="renderGroup"></param>
        /// <returns></returns>
        bool TryGetRenderGroup(out IRenderGroup renderGroup);
        /// <summary>
        /// 移除组件
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        bool RemoveComponent(string name);
        /// <summary>
        /// 是否包含某个接口
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        bool IsContainsComponent(string name);
        /// <summary>
        /// 更新
        /// </summary>
        void Update();
        /// <summary>
        /// 获取父渲染对象
        /// </summary>
        /// <typeparam name="TRenderObject"></typeparam>
        /// <returns></returns>
        TRenderObject GetParent<TRenderObject>() where TRenderObject : class, IRenderObject;
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IRenderObject GetParent();
        /// <summary>
        /// 添加子对象
        /// </summary>
        /// <param name="child"></param>
        void AddChildren(IRenderObject child);
        /// <summary>
        /// 获取子渲染对象
        /// </summary>
        /// <returns></returns>
        IEnumerable<IRenderObject> GetChildrens();
        /// <summary>
        /// 移除子渲染对象
        /// </summary>
        /// <param name="child"></param>
        /// <returns></returns>
        bool RemoveChildren(IRenderObject child);
        /// <summary>
        /// 深度拷贝
        /// </summary>
        /// <param name="graphicsManager"></param>
        /// <param name="newName"></param>
        /// <returns></returns>
        IRenderObject DeepCopy(IGraphicsManager graphicsManager, string newName = null);
        /// <summary>
        /// 摧毁渲染对象
        /// </summary>
        void Destroy(IGraphicsManager graphicsManager);
    }
}
