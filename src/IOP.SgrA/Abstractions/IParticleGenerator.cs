﻿using IOP.SgrA.ECS;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 粒子发生器接口
    /// </summary>
    public interface IParticleGenerator : IAnimation
    {
        /// <summary>
        /// 创建粒子原型
        /// </summary>
        /// <returns></returns>
        Archetype CreateParticleArchetype(params IComponentData[] exteraData);
        /// <summary>
        /// 创建粒子渲染上下文
        /// </summary>
        /// <returns></returns>
        void CreateParticleContext(int size);
        /// <summary>
        /// 回收粒子渲染上下文
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        bool RemoveParticleContext(Context context);
    }
}
