﻿namespace IOP.SgrA
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TContext"></typeparam>
    public interface IProductionLineMiddleware<TContext>
    {
        /// <summary>
        /// 执行
        /// </summary>
        /// <param name="context">上下文</param>
        /// <param name="next">下一个中间件</param>
        /// <returns></returns>
        void Invoke(TContext context, RenderingProductionLineDelegate<TContext> next);
    }
}
