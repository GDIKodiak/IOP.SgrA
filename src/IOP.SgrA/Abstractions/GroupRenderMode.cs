﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA
{
    /// <summary>
    /// 
    /// </summary>
    public enum GroupRenderMode : int
    {
        /// <summary>
        /// 
        /// </summary>
        Core = 0,
        /// <summary>
        /// 
        /// </summary>
        Background = 1000,
        /// <summary>
        /// 
        /// </summary>
        Opaque = 2000,
        /// <summary>
        /// 
        /// </summary>
        Transparent = 3000,
    }
}
