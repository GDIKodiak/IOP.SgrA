﻿using IOP.SgrA.ECS;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 粒子发生器
    /// </summary>
    public abstract class ParticleGenerator : Animation, IParticleGenerator
    {
        /// <summary>
        /// 上下文管理器
        /// </summary>
        protected IContextManager ContextManager { get; set; }
        /// <summary>
        /// 粒子渲染对象
        /// </summary>
        protected IRenderObject Particle { get; set; }
        /// <summary>
        /// 粒子渲染组
        /// </summary>
        protected IRenderGroup ParticleRenderGroup { get; set; }
        /// <summary>
        /// 粒子原型
        /// </summary>
        protected Archetype ParticleArchetype { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="contextManager"></param>
        /// <param name="particle"></param>
        /// <param name="renderGroup"></param>
        public ParticleGenerator(IContextManager contextManager, IRenderObject particle, IRenderGroup renderGroup)
        {
            ContextManager = contextManager ?? throw new ArgumentNullException(nameof(contextManager));
            Particle = particle ?? throw new ArgumentNullException(nameof(particle));
            ParticleRenderGroup = renderGroup ?? throw new ArgumentNullException(nameof(renderGroup));
        }
        /// <summary>
        /// 创建粒子对象原型
        /// </summary>
        /// <param name="exteraData"></param>
        /// <returns></returns>
        public virtual Archetype CreateParticleArchetype(params IComponentData[] exteraData)
        {
            if (ContextManager == null) throw new NullReferenceException("No ContextManager");
            List<IComponentData> datas = new List<IComponentData>() { new ParticleComponentData() };
            if (exteraData != null && exteraData.Length > 0) datas.AddRange(exteraData);
            var archetype = ContextManager.CreateArchetype($"{Name}{nameof(Archetype)}", datas.ToArray());
            ParticleArchetype = archetype;
            return archetype;
        }
        /// <summary>
        /// 创建粒子渲染上下文
        /// </summary>
        /// <returns></returns>
        /// <param name="size"></param>
        public virtual void CreateParticleContext(int size)
        {
            if (ContextManager == null) throw new NullReferenceException(nameof(ContextManager));
            if (ParticleArchetype == null) throw new NullReferenceException(nameof(ParticleArchetype));
            if (Particle == null) throw new NullReferenceException(nameof(Particle));
            if (ParticleRenderGroup == null) throw new NullReferenceException(nameof(ParticleRenderGroup));
            ContextManager.CreateContexts(size, ParticleArchetype, ParticleRenderGroup, Particle);
        }
        /// <summary>
        /// 移除粒子渲染上下文
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public virtual bool RemoveParticleContext(Context context)
        {
            ContextManager.RemoveContext(context.Entity);
            return true;
        }
    }
}
