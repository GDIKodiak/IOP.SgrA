﻿using IOP.SgrA.ECS;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 粒子组件
    /// </summary>
    public struct ParticleComponentData : IComponentData
    {
        /// <summary>
        /// 
        /// </summary>
        public int State;
    }
}
