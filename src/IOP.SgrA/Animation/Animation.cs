﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 动画基类
    /// </summary>
    public abstract class Animation : IAnimation
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 渲染对象
        /// </summary>
        protected IRenderObject RenderObject { get; set; }

        /// <summary>
        /// 当附加至渲染组件时
        /// </summary>
        /// <param name="object"></param>
        public abstract void OnAttach(IRenderObject @object);
        /// <summary>
        /// 播放动画
        /// </summary>
        /// <param name="timeSpan"></param>
        public abstract void Play(TimeSpan timeSpan);
        /// <summary>
        /// 拷贝至新的渲染对象
        /// </summary>
        /// <param name="newObj"></param>
        public abstract void CloneToNewRender(IRenderObject newObj);
        /// <summary>
        /// 摧毁动画
        /// </summary>
        public virtual void Destroy()
        {
            RenderObject = null;
        }
    }
}
