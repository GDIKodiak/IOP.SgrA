﻿using System;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace IOP.SgrA
{
    /// <summary>
    /// 数值类型扩展
    /// </summary>
    public static class NumericsExtension
    {
        /// <summary>
        /// 
        /// </summary>
        public static Vector2[] Halton_2_3 = new Vector2[]
        {
            new(0.0f, -1.0f / 3.0f),
            new(-1.0f / 2.0f, 1.0f / 3.0f),
            new(1.0f / 2.0f, -7.0f / 9.0f),
            new(-3.0f / 4.0f, -1.0f / 9.0f),
            new(1.0f / 4.0f, 5.0f / 9.0f),
            new(-1.0f / 4.0f, -5.0f / 9.0f),
            new(3.0f / 4.0f, 1.0f / 9.0f),
            new(-7.0f / 8.0f, 7.0f / 9.0f)
        };
        /// <summary>
        /// 将矩阵变换为字节数组
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns></returns>
        public static byte[] ToBytes(this Matrix4x4 matrix)
        {
            Span<float> matNumber = stackalloc float[]
            {
                matrix.M11, matrix.M12, matrix.M13, matrix.M14,
                matrix.M21, matrix.M22, matrix.M23, matrix.M24,
                matrix.M31, matrix.M32, matrix.M33, matrix.M34,
                matrix.M41, matrix.M42, matrix.M43, matrix.M44
            };
            Span<byte> bytes = MemoryMarshal.AsBytes(matNumber);
            return bytes.ToArray();
        }
        /// <summary>
        /// 将矩阵变换为指定字节数组
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="arrays"></param>
        public static void ToBytes(this Matrix4x4 matrix, byte[] arrays)
        {
            if (arrays.Length < Marshal.SizeOf<Matrix4x4>()) throw new OutOfMemoryException("the input arrays length is not enough to load target matrix");
            Span<float> matNumber = stackalloc float[]
            {
                matrix.M11, matrix.M12, matrix.M13, matrix.M14,
                matrix.M21, matrix.M22, matrix.M23, matrix.M24,
                matrix.M31, matrix.M32, matrix.M33, matrix.M34,
                matrix.M41, matrix.M42, matrix.M43, matrix.M44
            };
            Span<byte> bytes = MemoryMarshal.AsBytes(matNumber);
            bytes.CopyTo(arrays.AsSpan());
        }
        /// <summary>
        /// 将矩阵变换为指定字节数组
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="arrays"></param>
        /// <param name="startIndex"></param>
        public static void ToBytes(this Matrix4x4 matrix, ref Span<byte> arrays, int startIndex)
        {
            int size = Marshal.SizeOf<Matrix4x4>();
            int end = startIndex + size;
            if (arrays.Length < end) throw new OutOfMemoryException("the input arrays length is not enough to load target matrix");
            Span<float> matNumber = stackalloc float[]
{
                matrix.M11, matrix.M12, matrix.M13, matrix.M14,
                matrix.M21, matrix.M22, matrix.M23, matrix.M24,
                matrix.M31, matrix.M32, matrix.M33, matrix.M34,
                matrix.M41, matrix.M42, matrix.M43, matrix.M44
            };
            Span<byte> bytes = MemoryMarshal.AsBytes(matNumber);
            bytes.CopyTo(arrays.Slice(startIndex, size));
        }
        /// <summary>
        /// 将向量变换为指定字节数组
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="arrays"></param>
        /// <param name="startIndex"></param>
        public static void ToBytes(this Vector4 vector, ref Span<byte> arrays, int startIndex)
        {
            int size = Marshal.SizeOf<Vector4>();
            int end = startIndex + size;
            if (arrays.Length < end) throw new OutOfMemoryException("the input arrays length is not enough to load target vector");
            MemoryMarshal.Write(arrays.Slice(startIndex, size), ref vector);
        }
        /// <summary>
        /// 角度转弧度
        /// </summary>
        /// <param name="degree"></param>
        /// <returns></returns>
        public static float ToRadians(this float degree) => MathF.PI * degree / 180.0f;
        /// <summary>
        /// Halton抖动
        /// </summary>
        /// <param name="projection"></param>
        /// <param name="frameIndex"></param>
        /// <param name="height"></param>
        /// <param name="width"></param>
        public static void HaltonJitter(this Matrix4x4 projection, int width, int height, int frameIndex)
        {
            if (frameIndex < 0) frameIndex = 0;
            if (frameIndex >= 8) frameIndex = Halton_2_3.Length;
            Vector2 jitter = new Vector2((Halton_2_3[frameIndex].X - 0.5f) / width * 2,
                (Halton_2_3[frameIndex].Y - 0.5f) / height * 2);
            projection.M31 += jitter.X;
            projection.M32 += jitter.Y;
        }

        /// <summary>
        /// 将惯性--物体转换欧拉角(roll, pitch, yaw)以Z-Y-X顺序转换为四元数
        /// </summary>
        /// <param name="eulerAngles"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Quaternion ToObjectZYXQuaternion(this Vector3 eulerAngles)
        {
            double cr = Math.Cos(eulerAngles.X * 0.5);
            double sr = Math.Sin(eulerAngles.X * 0.5);
            double cp = Math.Cos(eulerAngles.Y * 0.5);
            double sp = Math.Sin(eulerAngles.Y * 0.5);
            double cy = Math.Cos(eulerAngles.Z * 0.5);
            double sy = Math.Sin(eulerAngles.Z * 0.5);
            Quaternion q;
            q.W = (float)(cr * cp * cy + sr * sp * sy);
            q.X = (float)(-cr * sp * cy - sr * cp * sy);
            q.Y = (float)(sr * sp * cy - cr * cp * sy);
            q.Z = (float)(cr * sp * sy - sr * cp * cy);
            return q;
        }
        /// <summary>
        /// 将物体--惯性转换欧拉角(roll, pitch, yaw)以Z-Y-X顺序转换为四元数
        /// </summary>
        /// <param name="eulerAngles"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Quaternion ToInertiaZYXQuaternion(this Vector3 eulerAngles)
        {
            double cr = Math.Cos(eulerAngles.X * 0.5);
            double sr = Math.Sin(eulerAngles.X * 0.5);
            double cp = Math.Cos(eulerAngles.Y * 0.5);
            double sp = Math.Sin(eulerAngles.Y * 0.5);
            double cy = Math.Cos(eulerAngles.Z * 0.5);
            double sy = Math.Sin(eulerAngles.Z * 0.5);
            Quaternion q;
            q.W = (float)(cr * cp * cy + sr * sp * sy);
            q.X = (float)(cr * sp * cy + sr * cp * sy);
            q.Y = (float)(cr * cp * sy - sr * sp * cy);
            q.Z = (float)(sr * cp * cy - cr * sp * sy);
            return q;
        }

        /// <summary>
        /// 将物体--惯性转换四元数转换为欧拉角(roll, pitch, yaw)
        /// </summary>
        /// <param name="q"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 ToInertiaZYXEulerAngles(this Quaternion q)
        {
            double yaw, pitch, roll;

            double sp = -2.0 * (q.Y * q.Z - q.W * q.X);
            if(Math.Abs(sp) >= 1)
            {
                pitch = (Math.PI / 2) * sp;
                yaw = Math.Atan2(-q.X * q.Z + q.W * q.Y, 0.5 - q.Y * q.Y - q.Z * q.Z);
                roll = 0;
            }
            else
            {
                pitch = Math.Asin(sp);
                yaw = Math.Atan2(q.X * q.Z + q.W * q.Y, 0.5 - q.X * q.X - q.Y * q.Y);
                roll = Math.Atan2(q.X * q.Y + q.W * q.Z, 0.5 - q.X * q.X - q.Z * q.Z);
            }

            return new Vector3((float)roll, (float)pitch, (float)yaw);
        }
        /// <summary>
        /// 将惯性--物体转换四元数转换为欧拉角(roll, pitch, yaw)
        /// </summary>
        /// <param name="q"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 ToObjectZYXEulerAngles(this Quaternion q)
        {
            double roll, pitch, yaw;
            double sp = -2.0f * (q.Y * q.Z + q.W * q.X);
            if(Math.Abs(sp) >= 1)
            {
                pitch = Math.PI / 2 * sp;
                yaw = Math.Atan2(-q.X * q.Z - q.W * q.Y, 0.5 - q.Y * q.Y - q.Z * q.Z);
                roll = 0.0;
            }
            else
            {
                pitch = Math.Asin(sp);
                yaw = Math.Atan2(q.X * q.Z - q.W * q.Y, 0.5 - q.X * q.X - q.Y * q.Y);
                roll = Math.Atan2(q.X * q.Y - q.W * q.Z, 0.5 - q.X * q.X - q.Z * q.Z);
            }
            return new Vector3((float)roll, (float)pitch, (float)yaw);
        }

        /// <summary>
        /// 将惯性--物体转换旋转矩阵转换为ZYX欧拉角
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 ToObjectZYXEulerAngles(this Matrix4x4 matrix)
        {
            double h, p, b;
            double sp = -matrix.M23;
            if (sp <= -1.0f) p = -Math.PI / 2;
            else if (sp >= 1.0) p = Math.PI / 2;
            else p = Math.Asin(sp);
            if (sp > 0.9999)
            {
                b = 0.0f;
                h = Math.Atan2(-matrix.M31, matrix.M11);
            }
            else
            {
                h = Math.Atan2(matrix.M13, matrix.M33);
                b = Math.Atan2(matrix.M21, matrix.M22);
            }
            return new Vector3((float)b, (float)p, (float)h);
        }

        /// <summary>
        /// 将欧拉角(Roll, Pitch, Yaw)转换为旋转矩阵
        /// </summary>
        /// <param name="rollPitchYaw"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Matrix4x4 ToRotationMatrix(this Vector3 rollPitchYaw)
        {
            double cy = Math.Cos(rollPitchYaw.Z);
            double sy = Math.Sin(rollPitchYaw.Z);
            double cp = Math.Cos(rollPitchYaw.Y);
            double sp = Math.Sin(rollPitchYaw.Y);
            double cr = Math.Cos(rollPitchYaw.X);
            double sr = Math.Sin(rollPitchYaw.X);
            var rz = new Matrix4x4((float)cr, (float)-sr, 0, 0, (float)sr, (float)cr, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
            var rx = new Matrix4x4(1, 0, 0, 0, 0, (float)cp, (float)-sp, 0, 0, (float)sp, (float)cp, 0, 0, 0, 0, 1);
            var ry = new Matrix4x4((float)cy, 0, (float)sy, 0, 0, 1, 0, 0, (float)-sy, 0, (float)cy, 0, 0, 0, 0, 1);
            return rx * ry;
        }

        /// <summary>
        /// 创建摄影机视图矩阵
        /// </summary>
        /// <param name="m"></param>
        /// <param name="p"></param>
        /// <param name="t"></param>
        /// <param name="u"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public static Matrix4x4 CreateLookAt(this Matrix4x4 m, Vector3 p, Vector3 t, Vector3 u)
        {
            var d = p - t;
            if (d == Vector3.Zero) d = Vector3.UnitZ;
            var r = Vector3.Cross(u, d);
            if (r == Vector3.Zero) throw new ArgumentException($"Illegal vector, the target direction vector {d} is parallel with up vector {u}");
            d = Vector3.Normalize(d);
            r = Vector3.Normalize(r);
            var v = Vector3.Cross(d, r);
            return new Matrix4x4(r.X, v.X, d.X, 0.0f,
                r.Y, v.Y, d.Y, 0.0f,
                r.Z, v.Z, d.Z, 0.0f,
                0.0f, 0.0f, 0.0f, 1.0f);
        }
    }
}
