﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA
{
    /// <summary>
    /// 射线
    /// </summary>
    public struct Ray
    {
        /// <summary>
        /// 起始位置
        /// </summary>
        public Vector3 Origin;
        /// <summary>
        /// 方向
        /// </summary>
        public Vector3 Direction;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="origin"></param>
        /// <param name="direction"></param>
        public Ray(Vector3 origin, Vector3 direction)
        {
            Origin = origin;
            Direction = direction;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override readonly string ToString()
        {
            return $"Origin:[{Origin.X},{Origin.Y},{Origin.Z}], Direction:[{Direction.X},{Direction.Y},{Direction.Z}]";
        }
    }
}
