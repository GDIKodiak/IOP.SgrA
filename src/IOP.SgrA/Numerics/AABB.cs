﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA
{
    /// <summary>
    /// 
    /// </summary>
    public struct AABB
    {
        /// <summary>
        /// 
        /// </summary>
        public Vector3 Center;
        /// <summary>
        /// 
        /// </summary>
        public Vector3 Extents;
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Vector3 Min() => Center - Extents;
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Vector3 Max() => Center + Extents;
        /// <summary>
        /// 某特定区域是否完全在此包围盒内
        /// </summary>
        /// <param name="center"></param>
        /// <param name="extents"></param>
        /// <returns></returns>
        public bool FullContains(Vector3 center, Vector3 extents)
        {
            Vector3 pMin = center - extents;
            Vector3 pMax = center + extents;
            Vector3 min = Min();
            Vector3 max = Max();
            if (pMin.X < min.X || pMin.Y < min.Y || pMin.Z < min.Z ||
                pMax.X > max.X || pMax.Y > max.Y || pMax.Z > max.Z)
                return false;
            else return true;
        }
        /// <summary>
        /// 此包围盒是否完全存在于某一区域内
        /// </summary>
        /// <param name="center"></param>
        /// <param name="extents"></param>
        /// <returns></returns>
        public bool FullContainsIn(Vector3 center, Vector3 extents)
        {
            Vector3 pMin = center - extents;
            Vector3 pMax = center + extents;
            Vector3 min = Min();
            Vector3 max = Max();
            return min.X >= pMin.X && min.Y >= pMin.Y && min.Z >= pMin.Z &&
                max.X <= pMax.X && max.Y <= pMax.Y && max.Z <= pMax.Z;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="center"></param>
        /// <param name="extents"></param>
        public AABB(Vector3 center, Vector3 extents)
        {
            Center = center;
            Extents = extents;
        }
    }
}
