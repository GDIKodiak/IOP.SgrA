﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA
{
    /// <summary>
    /// 通用包围盒接口
    /// </summary>
    public interface IBoundingBox
    {
        /// <summary>
        /// 将包围盒导出为轴对齐包围盒
        /// </summary>
        /// <returns></returns>
        Bounds ToAABB();
    }
}
