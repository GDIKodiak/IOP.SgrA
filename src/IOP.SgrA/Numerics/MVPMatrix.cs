﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// MVP矩阵
    /// </summary>
    public struct MVPMatrix
    {
        /// <summary>
        /// 模型矩阵
        /// </summary>
        public Matrix4x4 ModelMatrix;
        /// <summary>
        /// 视角矩阵
        /// </summary>
        public Matrix4x4 ViewMatrix;
        /// <summary>
        /// 映射矩阵
        /// </summary>
        public Matrix4x4 ProjectionMatrix;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="model"></param>
        /// <param name="view"></param>
        /// <param name="projection"></param>
        public MVPMatrix(Matrix4x4 model, Matrix4x4 view, Matrix4x4 projection)
        {
            ModelMatrix = model;
            ViewMatrix = view;
            ProjectionMatrix = projection;
        }
        /// <summary>
        /// 生成一个新的标准MVP矩阵
        /// </summary>
        /// <returns></returns>
        public static MVPMatrix Identity() { return new MVPMatrix { ModelMatrix = Matrix4x4.Identity, ViewMatrix = Matrix4x4.Identity, ProjectionMatrix = Matrix4x4.Identity }; }
        /// <summary>
        /// 获取最终矩阵
        /// </summary>
        /// <returns></returns>
        public Matrix4x4 GetFinalMatrix() => ModelMatrix * ViewMatrix * ProjectionMatrix;
    }
}
