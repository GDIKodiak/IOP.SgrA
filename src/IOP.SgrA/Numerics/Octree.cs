﻿using System;
using System.Buffers;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 八叉树
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Octree<T>
        where T : class, IBounded, ITransformable
    {
        /// <summary>
        /// 节点字典
        /// </summary>
        internal protected readonly ConcurrentDictionary<ulong, OctreeNode<T>> Nodes = new();
        /// <summary>
        /// 对象索引字典
        /// </summary>
        internal protected readonly ConcurrentDictionary<T, OctreeNode<T>> Indexes = new();
        /// <summary>
        /// 最大深度
        /// </summary>
        internal uint MAXDEPTH = 21;
        /// <summary>
        /// 松散系数
        /// </summary>
        public float Loose { get; protected set; } = 1.5f;
        /// <summary>
        /// 最大深度
        /// </summary>
        public uint MaxDepth { get; protected set; }
        /// <summary>
        /// 八叉树中心坐标
        /// </summary>
        public Vector3 Center { get; protected set; }
        /// <summary>
        /// 八叉树左下角外围坐标
        /// </summary>
        public Vector3 LeftDown { get; protected set; }
        /// <summary>
        /// 八叉树的最大宽度
        /// </summary>
        public float HalfWidth { get; protected set; }
        /// <summary>
        /// 最小单元格宽度
        /// </summary>
        public float GridHalfWidth { get; protected set; }
        /// <summary>
        /// 对象数量
        /// </summary>
        public int Count => Indexes.Count;
        /// <summary>
        /// 莫顿码最大值
        /// </summary>
        public ulong MaxCode
        {
            get => (ulong)Math.Pow(2, (MaxDepth * 3));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="width">整个八叉树最外围正方形宽度</param>
        /// <param name="center">中心坐标</param>
        /// <param name="maxDepth">最大深度</param>
        /// <param name="loose">松散系数</param>
        public Octree(float width, Vector3 center, uint maxDepth, float loose = 1.5f)
        {
            InitTree(width, center, maxDepth, loose);
        }

        /// <summary>
        /// 尝试加入至树
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool TryInsert(T obj)
        {
            if (obj == null) return false;
            if(TryGetGridIndex(obj, out Vector3 index, out Vector3 center, out AABB box))
            {
                float halfWidth = GridHalfWidth;
                ulong code = Morton3(index); bool insert = false; ulong local;
                while (halfWidth <= HalfWidth && code > 0)
                {
                    if(box.FullContainsIn(center, new Vector3(halfWidth * Loose)))
                    {
                        insert = true;
                        break;
                    }
                    else
                    {
                        local = code & 7;
                        code >>= 3;
                        center = GetParentCenter(center, halfWidth, local);
                        halfWidth *= 2;
                    }
                }
                if (insert)
                {
                    if(Nodes.TryGetValue(code, out OctreeNode<T> node))
                    {
                        node.Objects.AddLast(obj);
                        Indexes.AddOrUpdate(obj, node, (key, value) => node);
                    }
                    else
                    {
                        OctreeNode<T> newNode = new(code, center, halfWidth);
                        local = code & 7;
                        ulong pCode = code >> 3;
                        if(Nodes.TryGetValue(pCode, out OctreeNode<T> n))
                        {
                            n.ChildMask |= (byte)(1 << (byte)local);
                        }
                        else
                        {
                            center = GetParentCenter(center, halfWidth, local);
                            halfWidth *= 2;
                            OctreeNode<T> pNode = new OctreeNode<T>(pCode, center, halfWidth);
                            pNode.ChildMask |= (byte)(1 << (byte)local);
                            Nodes.AddOrUpdate(pCode, pNode, (key, value) => pNode);
                        }
                        newNode.Objects.AddLast(obj);
                        Nodes.AddOrUpdate(code, newNode, (key, value) => newNode);
                        Indexes.AddOrUpdate(obj, newNode, (key, value) => newNode);
                    }
                }
                return insert;
            }
            return false;
        }

        /// <summary>
        /// 获取节点深度
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public ulong GetNodeDepth(OctreeNode<T> node)
        {
            if (node == null) return 0;
            return (ulong)(Math.Log2(node.MortonCode) / 3);
        }
        /// <summary>
        /// 生成莫顿码
        /// </summary>
        /// <param name="gridIndex"></param>
        /// <returns></returns>
        public ulong Morton3(Vector3 gridIndex)
        {
            ulong r = (Part1By2((ulong)gridIndex.Z) << 2) +
                (Part1By2((ulong)gridIndex.Y) << 1) +
                Part1By2((ulong)gridIndex.X);
            return r;
        }
        /// <summary>
        /// 移动中心
        /// </summary>
        /// <param name="newCenter"></param>
        /// <param name="outsideObject"></param>
        public void MoveCenter(Vector3 newCenter, out List<T> outsideObject)
        {
            Span<T> o = new T[Count];
            MoveCenter(newCenter, in o, out int l);
            outsideObject = new List<T>(o[..l].ToArray());
        }
        /// <summary>
        /// 移动中心
        /// </summary>
        /// <param name="newCenter"></param>
        /// <param name="outside"></param>
        /// <param name="length"></param>
        /// <exception cref="IndexOutOfRangeException"></exception>
        public void MoveCenter(Vector3 newCenter, in Span<T> outside, out int length)
        {
            if (outside.Length < Count) throw new IndexOutOfRangeException();
            Vector3 old = Center;
            Center = newCenter;
            float halfWidth = HalfWidth;
            LeftDown = new Vector3(newCenter.X - halfWidth,
                newCenter.Y - halfWidth, newCenter.Z + halfWidth);
            Vector3 p = newCenter - old; int end = 0; length = 0;
            T[] locals = ArrayPool<T>.Shared.Rent(Count); Span<T> s = locals;
            foreach (var item in Nodes.Values)
            {
                Span<T> local = s.Slice(end, item.Objects.Count);
                item.Move(p, in local, out int l);
                end += l;
            }
            for(int i = 0; i < end; i++)
            {
                T obj = s[i];
                Indexes.TryRemove(obj, out _);
                if (!TryInsert(obj))
                    outside[length++] = obj;
            }
            ArrayPool<T>.Shared.Return(locals);
        }
        /// <summary>
        /// 维护树形数据
        /// </summary>
        /// <param name="outsideObject"></param>
        public void Maintenance(out List<T> outsideObject)
        {
            outsideObject = new List<T>(Count);
            Span<T> o = new T[Count];
            Maintenance(in o, out int l);
            outsideObject.AddRange(o[..l].ToArray());
        }
        /// <summary>
        /// 维护树形数据
        /// </summary>
        /// <param name="outside"></param>
        /// <param name="length"></param>
        public void Maintenance(in Span<T> outside, out int length)
        {
            if (outside.Length < Count) throw new IndexOutOfRangeException();
            T[] locals = ArrayPool<T>.Shared.Rent(Count); Span<T> s = locals;
            int end = 0; length = 0;
            foreach (var item in Nodes.Values)
            {
                Span<T> local = s.Slice(end, item.Objects.Count);
                item.Maintenance(in local, out int l);
                end += l;
            }
            for (int i = 0; i < end; i++)
            {
                T obj = s[i];
                Indexes.TryRemove(obj, out _);
                if (!TryInsert(obj))
                    outside[length++] = obj;
            }
            ArrayPool<T>.Shared.Return(locals);
        }

        /// <summary>
        /// 初始化树
        /// </summary>
        protected virtual void InitTree(float width, Vector3 center, uint maxDepth, float loose)
        {
            maxDepth = Math.Min(MAXDEPTH, maxDepth);
            MaxDepth = maxDepth;
            Center = center;
            Loose = loose;
            float halfWidth = width * 0.5f;
            HalfWidth = halfWidth;
            LeftDown = new Vector3(center.X - halfWidth,
                center.Y - halfWidth, center.Z + halfWidth);
            GridHalfWidth = halfWidth / MathF.Pow(2, maxDepth);
            OctreeNode<T> root = new(1, center, HalfWidth);
            Nodes.AddOrUpdate(1, root, (key, value) => root);
        }
        /// <summary>
        /// 尝试获取目标所在网格下标
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="gridIndex"></param>
        /// <param name="center"></param>
        /// <param name="bounds"></param>
        /// <returns></returns>
        protected virtual bool TryGetGridIndex(T obj, out Vector3 gridIndex, out Vector3 center, out AABB bounds)
        {
            gridIndex = Vector3.Zero;
            center = Vector3.Zero; 
            Vector3 rCenter = Center; float bWidth = HalfWidth;
            Transform transform = obj.GetTransform();
            Bounds b = obj.GetBounds();
            bounds = b.Transform(transform.CreateMatrix());
            Vector3 p = transform.Position;
            Vector3 extents = bounds.Extents;
            Vector3 boundary = new(rCenter.X - bWidth, rCenter.Y - bWidth,
                rCenter.Z + bWidth);
            float ll = (boundary - rCenter).LengthSquared();
            float tl = (p - extents - rCenter).LengthSquared();
            float tr = (p + extents - rCenter).LengthSquared();
            if (tl > ll || tr > ll) return false;
            float w = GridHalfWidth * 2;
            Vector3 d = (p - LeftDown) / w;
            gridIndex = new Vector3((int)Math.Abs(d.X),
                (int)Math.Abs(d.Y), (int)Math.Abs(d.Z));
            center = new Vector3(LeftDown.X + gridIndex.X * w + GridHalfWidth,
                LeftDown.Y + gridIndex.Y * w + GridHalfWidth,
                LeftDown.Z - gridIndex.Z * w - GridHalfWidth);
            return true;
        }
        /// <summary>
        /// 将输入拆分成每隔2位一个数字
        /// 即z--z--z--z--z--z--z--z--z--z--
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        internal static ulong Part1By2(ulong n)
        {
            n &= 0x1fffff;
            n = (n | (n << 32)) & 0x1f00000000ffff;
            n = (n | (n << 16)) & 0x1f0000ff0000ff;
            n = (n | (n << 8)) & 0x100f00f00f00f00f;
            n = (n | (n << 4)) & 0x10c30c30c30c30c3;
            n = (n | (n << 2)) & 0x1249249249249249;
            return n;
        }
        /// <summary>
        /// 获取子网格所属父节点中心
        /// </summary>
        /// <param name="childCenter"></param>
        /// <param name="childHalfWidth"></param>
        /// <param name="localCode"></param>
        /// <returns></returns>
        internal static Vector3 GetParentCenter(Vector3 childCenter, float childHalfWidth, ulong localCode)
        {
            return localCode switch
            {
                1 => childCenter + new Vector3(-childHalfWidth, childHalfWidth, -childHalfWidth),
                2 => childCenter + new Vector3(childHalfWidth, -childHalfWidth, -childHalfWidth),
                3 => childCenter + new Vector3(-childHalfWidth, -childHalfWidth, -childHalfWidth),
                4 => childCenter + new Vector3(childHalfWidth, childHalfWidth, childHalfWidth),
                5 => childCenter + new Vector3(-childHalfWidth, childHalfWidth, childHalfWidth),
                6 => childCenter + new Vector3(childHalfWidth, -childHalfWidth, childHalfWidth),
                7 => childCenter + new Vector3(-childHalfWidth, -childHalfWidth, childHalfWidth),
                _ => childCenter + new Vector3(childHalfWidth, childHalfWidth, -childHalfWidth),
            };
        }

    }
}
