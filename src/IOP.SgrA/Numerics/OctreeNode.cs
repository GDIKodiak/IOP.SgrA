﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 八叉树节点
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class OctreeNode<T>
        where T : class, IBounded, ITransformable
    {
        /// <summary>
        /// 节点莫顿码
        /// </summary>
        public ulong MortonCode { get; protected set; }
        /// <summary>
        /// 子节点掩码
        /// </summary>
        public byte ChildMask { get; set; }
        /// <summary>
        /// 中心
        /// </summary>
        public Vector3 Center { get; protected set; }
        /// <summary>
        /// 该节点网格宽度
        /// </summary>
        public float HalfWidth { get; protected set; }
        /// <summary>
        /// 处于当前节点内的对象列表
        /// </summary>
        public LinkedList<T> Objects { get; protected set; } = new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="center"></param>
        /// <param name="halfWdith"></param>
        public OctreeNode(ulong code, Vector3 center, float halfWdith)
        {
            MortonCode = code;
            Center = center;
            HalfWidth = halfWdith;
        }
        /// <summary>
        /// 当中心移动后移动节点
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="outsideObjects"></param>
        public void Move(Vector3 offset, out T[] outsideObjects)
        {
            Span<T> c = new T[Objects.Count];
            Move(offset, in c, out int l);
            outsideObjects = c[..l].ToArray();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="outside"></param>
        /// <param name="length"></param>
        /// <exception cref="IndexOutOfRangeException"></exception>
        public void Move(Vector3 offset, in Span<T> outside, out int length)
        {
            if (outside.Length < Objects.Count) throw new IndexOutOfRangeException();
            var center = Center + offset;
            Center = center;
            Maintenance(in outside, out length);
        }

        /// <summary>
        /// 维护当前节点对象数据
        /// </summary>
        /// <param name="outsideObjects"></param>
        public void Maintenance(out T[] outsideObjects)
        {
            Span<T> c = new T[Objects.Count];
            Maintenance(in c, out int l);
            outsideObjects = c[..l].ToArray();
        }
        /// <summary>
        /// 维护节点数据
        /// </summary>
        /// <param name="outside"></param>
        /// <param name="length"></param>
        public void Maintenance(in Span<T> outside, out int length)
        {
            if (outside.Length < Objects.Count) throw new IndexOutOfRangeException();
            var current = Objects.First; length = 0;
            var center = Center; var halfWidth = HalfWidth;
            while (current != null)
            {
                T obj = current.Value;
                var bounds = obj.GetBounds();
                var t = obj.GetTransform();
                AABB aabb = bounds.Transform(t.CreateMatrix());
                if (!aabb.FullContainsIn(center, new Vector3(halfWidth)))
                {
                    outside[length++] = obj;
                    var remove = current;
                    current = current.Next;
                    Objects.Remove(remove);
                }
                else current = current.Next;
            }
        }
    }
}
