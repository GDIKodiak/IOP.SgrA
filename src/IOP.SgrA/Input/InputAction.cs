﻿namespace IOP.SgrA.Input
{
    /// <summary>
    /// 输入动作
    /// </summary>
    public enum InputAction : int
    {
        /// <summary>
        /// 释放
        /// </summary>
        Release = 0,
        /// <summary>
        /// 按下
        /// </summary>
        Press = 1,
        /// <summary>
        /// 重复
        /// </summary>
        Repeat = 2,
        /// <summary>
        /// 弹起
        /// </summary>
        Up = 3,
        /// <summary>
        /// 点击
        /// </summary>
        Click = 128,
    }
}
