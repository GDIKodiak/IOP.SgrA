﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using System.Runtime.CompilerServices;
using System.Collections.Concurrent;

namespace IOP.SgrA.Input
{
    /// <summary>
    /// 输入状态控制器
    /// </summary>
    public class InputStateController : IInputStateController
    {
        /// <summary>
        /// 鼠标状态
        /// </summary>
        public MouseInputState MouseState { get; } = new MouseInputState();
        /// <summary>
        /// 键盘状态
        /// </summary>
        public KeyboardInputState KeyboardState { get; } = new KeyboardInputState();
        /// <summary>
        /// 输入命令
        /// </summary>
        protected ConcurrentQueue<Action> InputCommand = new ConcurrentQueue<Action>();
        /// <summary>
        /// 鼠标状态帧队列
        /// </summary>
        private readonly Queue<MouseStateFrame> _MouseClickQueue = new Queue<MouseStateFrame>(16);
        /// <summary>
        /// 键盘状态帧队列
        /// </summary>
        private readonly Queue<PressKeyStateFrame> _KeyboardClickQueue = new Queue<PressKeyStateFrame>(121);
        /// <summary>
        /// 周期
        /// </summary>
        private TimeSpan _Period = TimeSpan.FromMilliseconds(200);
        /// <summary>
        /// 获取鼠标按钮状态
        /// </summary>
        /// <param name="mouseButton"></param>
        /// <returns></returns>
        public MouseButtonState GetMouseButtonState(MouseButton mouseButton) => MouseState[mouseButton];
        /// <summary>
        /// 获取键盘按键状态
        /// </summary>
        /// <param name="pressKey"></param>
        /// <returns></returns>
        public PressKeyState GetPressKeyState(PressKey pressKey) => KeyboardState[pressKey];
        /// <summary>
        /// 获取鼠标坐标
        /// </summary>
        /// <returns></returns>
        public Vector2 GetMousePosition() => new Vector2(MouseState.X, MouseState.Y);
        /// <summary>
        /// 获取鼠标坐标增量
        /// </summary>
        /// <returns></returns>
        public Vector2 GetMousePositionDelta() => new(MouseState.XDelta, MouseState.YDelta);
        /// <summary>
        /// 获取鼠标滚轮增量
        /// </summary>
        /// <returns></returns>
        public int GetMouseWheelDelta() => MouseState.WDelta;
        /// <summary>
        /// 刷新鼠标状态帧
        /// </summary>
        public void RefreshMouseStateFrame()
        {
            while(_MouseClickQueue.TryDequeue(out MouseStateFrame frame))
            {
                if (frame.Action == InputAction.Click) 
                    MouseButtonStateChanged(frame);
            }
        }
        /// <summary>
        /// 刷新键盘状态帧
        /// </summary>
        public void RefreshKeyboardFrame()
        {
            while (_KeyboardClickQueue.TryDequeue(out PressKeyStateFrame frame))
            {
                if (frame.Action == InputAction.Click)
                    KeyboardStateChanged(frame);
            }
        }
        /// <summary>
        /// 刷新输入命令
        /// </summary>
        public void RefreshInputCommand()
        {
            while(InputCommand.TryDequeue(out Action action))
            {
                action?.Invoke();
            }
        }
        /// <summary>
        /// 变更点击有效区间
        /// </summary>
        /// <param name="millisecond"></param>
        public void ChangeClickPeriod(int millisecond) => _Period = TimeSpan.FromMilliseconds(millisecond);
        /// <summary>
        /// 鼠标状态发生变更
        /// </summary>
        /// <param name="state"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void MouseButtonStateChanged(MouseStateFrame state)
        {
            MouseButtonState button = MouseState[state.Button];
            if (state.Action == InputAction.Release)
            {
                if(button.Action == InputAction.Press)
                {
                    var diff = state.FrameTime - button.LastStateTime;
                    if(diff <= _Period)
                    {
                        button.LastStateTime = state.FrameTime;
                        button.Action = InputAction.Click;
                        button.Mods = state.Mods;
                        state.Action = InputAction.Click;
                        _MouseClickQueue.Enqueue(state);
                    }
                    else
                    {
                        button.LastStateTime = state.FrameTime;
                        button.Action = InputAction.Up;
                        button.Mods = state.Mods;
                        state.Action = InputAction.Up;
                        _MouseClickQueue.Enqueue(state);
                    }
                }
                else
                {
                    button.LastStateTime = state.FrameTime;
                    button.Action = state.Action;
                    button.Mods = state.Mods;
                }
            }
            else if(state.Action == InputAction.Click)
            {
                button.LastStateTime = DateTime.Now;
                button.Action = InputAction.Release;
            }
            else if(state.Action == InputAction.Up)
            {
                button.LastStateTime = DateTime.Now;
                button.Action = InputAction.Release;
            }
            else
            {
                button.LastStateTime = state.FrameTime;
                button.Action = state.Action;
                button.Mods = state.Mods;
            }
        }
        /// <summary>
        /// 键盘状态发生变更
        /// </summary>
        /// <param name="state"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void KeyboardStateChanged(PressKeyStateFrame state)
        {
            PressKeyState key = KeyboardState[state.Key];
            if (state.Action == InputAction.Release)
            {
                if (key.Action == InputAction.Press)
                {
                    var diff = state.FrameTime - key.LastStateTime;
                    if (diff <= _Period)
                    {
                        key.LastStateTime = state.FrameTime;
                        key.Action = InputAction.Click;
                        key.Mods = state.Mods;
                        state.Action = InputAction.Click;
                        _KeyboardClickQueue.Enqueue(state);
                    }
                    else
                    {
                        key.LastStateTime = state.FrameTime;
                        key.Action = state.Action;
                        key.Mods = state.Mods;
                    }
                }
                else
                {
                    key.LastStateTime = state.FrameTime;
                    key.Action = state.Action;
                    key.Mods = state.Mods;
                }
            }
            else if (state.Action == InputAction.Click)
            {
                key.LastStateTime = DateTime.Now;
                key.Action = InputAction.Release;
            }
            else if (state.Action == InputAction.Repeat)
            {
                key.Action = InputAction.Press;
            }
            else
            {
                key.LastStateTime = state.FrameTime;
                key.Action = state.Action;
                key.Mods = state.Mods;
            }
        }
        /// <summary>
        /// 隐藏光标
        /// </summary>
        /// <param name="enable"></param>
        public virtual void CursorDisabled(bool enable) { }
    }
}
