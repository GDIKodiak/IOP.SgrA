﻿namespace IOP.SgrA.Input
{
    /// <summary>
    /// 鼠标按钮事件
    /// </summary>
    public class MouseButtonEventArgs : MouseEventArgs
    {
        /// <summary>
        /// 鼠标按钮
        /// </summary>
        public MouseButton Button { get; }
        /// <summary>
        /// 鼠标动作
        /// </summary>
        public InputAction MouseAction { get; }
        /// <summary>
        /// 输入模组
        /// </summary>
        public InputMods Mods { get; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="button"></param>
        /// <param name="action"></param>
        /// <param name="mods"></param>
        public MouseButtonEventArgs(int x, int y, MouseButton button, InputAction action, InputMods mods)
            :base(x, y)
        {
            Button = button;
            MouseAction = action;
            Mods = mods;
        }
    }
}
