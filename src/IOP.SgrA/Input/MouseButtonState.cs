﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.Input
{
    /// <summary>
    /// 鼠标按钮状态
    /// </summary>
    public class MouseButtonState
    {
        /// <summary>
        /// 按钮
        /// </summary>
        public MouseButton Button { get; private set; }
        /// <summary>
        /// 按钮动作
        /// </summary>
        public InputAction Action { get; internal set; } = InputAction.Release;
        /// <summary>
        /// 模组
        /// </summary>
        public InputMods Mods { get; internal set; } = InputMods.None;
        /// <summary>
        /// 上次状态变更时间
        /// </summary>
        public DateTime LastStateTime { get; internal set; } = DateTime.Now;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="button"></param>
        public MouseButtonState(MouseButton button)
        {
            Button = button;
        }
    }
}
