﻿using System;

namespace IOP.SgrA.Input
{
    /// <summary>
    /// 按键状态
    /// </summary>
    public class PressKeyState
    {
        /// <summary>
        /// 按钮
        /// </summary>
        public PressKey Key { get; private set; }
        /// <summary>
        /// 按钮动作
        /// </summary>
        public InputAction Action { get; internal set; } = InputAction.Release;
        /// <summary>
        /// 模组
        /// </summary>
        public InputMods Mods { get; internal set; } = InputMods.None;
        /// <summary>
        /// 上次状态变更时间
        /// </summary>
        public DateTime LastStateTime { get; internal set; } = DateTime.Now;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="key"></param>
        public PressKeyState(PressKey key) => Key = key;
    }
}
