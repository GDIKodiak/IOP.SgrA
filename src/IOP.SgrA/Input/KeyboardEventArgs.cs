﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.Input
{
    /// <summary>
    /// 键盘事件参数
    /// </summary>
    public class KeyboardEventArgs : EventArgs
    {
        /// <summary>
        /// 按键
        /// </summary>
        public PressKey Key { get; }
        /// <summary>
        /// 键盘动作
        /// </summary>
        public InputAction KeyboardAction { get; }
        /// <summary>
        /// 模组
        /// </summary>
        public InputMods Mods { get; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="key"></param>
        /// <param name="action"></param>
        /// <param name="mods"></param>
        public KeyboardEventArgs(PressKey key, InputAction action, InputMods mods)
        {
            Key = key;
            KeyboardAction = action;
            Mods = mods;
        }
    }
}
