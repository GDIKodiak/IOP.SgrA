﻿using System;

namespace IOP.SgrA.Input
{
    /// <summary>
    /// 输入模组
    /// </summary>
    [Flags]
    public enum InputMods : int
    {
        /// <summary>
        /// 
        /// </summary>
        None = 0x0000,
        /// <summary>
        /// 
        /// </summary>
        ModShift = 0x0001,
        /// <summary>
        /// 
        /// </summary>
        ModControl = 0x0002,
        /// <summary>
        /// 
        /// </summary>
        ModAlt = 0x0004,
        /// <summary>
        /// 
        /// </summary>
        ModSuper = 0x0008,
        /// <summary>
        /// 
        /// </summary>
        ModCapsLock = 0x0010,
        /// <summary>
        /// 
        /// </summary>
        ModNumLock = 0x0020
    }
}
