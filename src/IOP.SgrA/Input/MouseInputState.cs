﻿using System.Collections.Generic;

namespace IOP.SgrA.Input
{
    /// <summary>
    /// 鼠标输入状态
    /// </summary>
    public class MouseInputState
    {
        /// <summary>
        /// X轴坐标
        /// </summary>
        public int X { get; set; }
        /// <summary>
        /// Y轴坐标
        /// </summary>
        public int Y { get; set; }
        /// <summary>
        /// X轴差值
        /// </summary>
        public int XDelta { get; set; }
        /// <summary>
        /// Y轴差值
        /// </summary>
        public int YDelta { get; set; }
        /// <summary>
        /// 滚轮差值
        /// </summary>
        public int WDelta { get; set; }
        /// <summary>
        /// 获取鼠标状态
        /// </summary>
        /// <param name="button"></param>
        /// <returns></returns>
        public MouseButtonState this[MouseButton button] => Buttons[button];

        /// <summary>
        /// 按钮状态
        /// </summary>
        internal static Dictionary<MouseButton, MouseButtonState> Buttons { get; } = new Dictionary<MouseButton, MouseButtonState>()
        {
            { MouseButton.ButtonLeft, new MouseButtonState(MouseButton.ButtonLeft) },
            { MouseButton.ButtonRight, new MouseButtonState(MouseButton.ButtonRight) },
            { MouseButton.ButtonMiddle, new MouseButtonState(MouseButton.ButtonMiddle) },
            { MouseButton.Button4, new MouseButtonState(MouseButton.Button4) },
            { MouseButton.Button5, new MouseButtonState(MouseButton.Button5) },
            { MouseButton.Button6, new MouseButtonState(MouseButton.Button6) },
            { MouseButton.Button7, new MouseButtonState(MouseButton.Button7) },
            { MouseButton.Button8, new MouseButtonState(MouseButton.Button8) }
        };
    }
}
