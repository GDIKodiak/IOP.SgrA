﻿using System;

namespace IOP.SgrA.Input
{
    /// <summary>
    /// 键盘帧
    /// </summary>
    public struct PressKeyStateFrame
    {
        /// <summary>
        /// 帧时间
        /// </summary>
        public DateTime FrameTime;
        /// <summary>
        /// 按钮
        /// </summary>
        public PressKey Key;
        /// <summary>
        /// 动作
        /// </summary>
        public InputAction Action;
        /// <summary>
        /// 
        /// </summary>
        public InputMods Mods;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="time"></param>
        /// <param name="key"></param>
        /// <param name="action"></param>
        /// <param name="mods"></param>
        public PressKeyStateFrame(DateTime time, PressKey key, InputAction action, InputMods mods)
        {
            FrameTime = time;
            Key = key;
            Action = action;
            Mods = mods;
        }
    }
}
