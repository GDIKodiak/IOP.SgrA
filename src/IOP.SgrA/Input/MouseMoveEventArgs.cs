﻿namespace IOP.SgrA.Input
{
    /// <summary>
    /// 鼠标移动事件
    /// </summary>
    public class MouseMoveEventArgs : MouseEventArgs
    {
        /// <summary>
        /// X增量
        /// </summary>
        public int XDelta { get; }
        /// <summary>
        /// Y增量
        /// </summary>
        public int YDelta { get; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="xDelta"></param>
        /// <param name="yDelta"></param>
        public MouseMoveEventArgs(int x, int y, int xDelta, int yDelta)
            :base(x,y) 
        {
            XDelta = xDelta;
            YDelta = yDelta;
        }
    }
}
