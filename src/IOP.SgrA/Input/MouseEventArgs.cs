﻿using System;

namespace IOP.SgrA.Input
{
    /// <summary>
    /// 鼠标事件
    /// </summary>
    public class MouseEventArgs : EventArgs
    {
        /// <summary>
        /// X坐标
        /// </summary>
        public int X { get; set; }
        /// <summary>
        /// Y坐标
        /// </summary>
        public int Y { get; set; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public MouseEventArgs(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
}
