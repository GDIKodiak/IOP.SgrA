﻿using System;

namespace IOP.SgrA.Input
{
    /// <summary>
    /// 鼠标状态帧
    /// </summary>
    public struct MouseStateFrame
    {
        /// <summary>
        /// 帧时间
        /// </summary>
        public DateTime FrameTime;
        /// <summary>
        /// 按钮
        /// </summary>
        public MouseButton Button;
        /// <summary>
        /// 动作
        /// </summary>
        public InputAction Action;
        /// <summary>
        /// 
        /// </summary>
        public InputMods Mods;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="time"></param>
        /// <param name="button"></param>
        /// <param name="action"></param>
        /// <param name="mods"></param>
        public MouseStateFrame(DateTime time, MouseButton button, InputAction action, InputMods mods)
        {
            FrameTime = time;
            Button = button;
            Action = action;
            Mods = mods;
        }
    }
}
