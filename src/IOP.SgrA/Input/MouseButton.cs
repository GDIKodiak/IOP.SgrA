﻿namespace IOP.SgrA.Input
{
    /// <summary>
    /// 鼠标按钮
    /// </summary>
    public enum MouseButton : int
    {
        /// <summary>
        /// 
        /// </summary>
        ButtonLeft = 0,
        /// <summary>
        /// 
        /// </summary>
        ButtonRight = 1,
        /// <summary>
        /// 
        /// </summary>
        ButtonMiddle = 2,
        /// <summary>
        /// 
        /// </summary>
        Button4 = 3,
        /// <summary>
        /// 
        /// </summary>
        Button5 = 4,
        /// <summary>
        /// 
        /// </summary>
        Button6 = 5,
        /// <summary>
        /// 
        /// </summary>
        Button7 = 6,
        /// <summary>
        /// 
        /// </summary>
        Button8 = 7,
        /// <summary>
        /// 
        /// </summary>
        ButtonLast = Button8,

    }
}
