﻿namespace IOP.SgrA.Input
{
    /// <summary>
    /// 按键
    /// </summary>
    public enum PressKey : int
    {
        /// <summary>
        /// 未知
        /// </summary>
        Unknown = -1,
        /// <summary>
        /// 空格
        /// </summary>
        Space = 32,
        /// <summary>
        /// 撇号
        /// </summary>
        Apostrophe = 39,
        /// <summary>
        /// 逗号
        /// </summary>
        Comma = 44,
        /// <summary>
        /// 减号
        /// </summary>
        Minus = 45,
        /// <summary>
        /// 点号
        /// </summary>
        Period = 46,
        /// <summary>
        /// 斜杠
        /// </summary>
        Slash = 47,
        /// <summary>
        /// 零号键
        /// </summary>
        Key_0 = 48,
        /// <summary>
        /// 一号键
        /// </summary>
        Key_1 = 49,
        /// <summary>
        /// 二号键
        /// </summary>
        Key_2 = 50,
        /// <summary>
        /// 三号键
        /// </summary>
        Key_3 = 51,
        /// <summary>
        /// 四号键
        /// </summary>
        Key_4 = 52,
        /// <summary>
        /// 五号键
        /// </summary>
        Key_5 = 53,
        /// <summary>
        /// 六号键
        /// </summary>
        Key_6 = 54,
        /// <summary>
        /// 七号键
        /// </summary>
        Key_7 = 55,
        /// <summary>
        /// 八号键
        /// </summary>
        Key_8 = 56,
        /// <summary>
        /// 九号键
        /// </summary>
        Key_9 = 57,
        /// <summary>
        /// 分号
        /// </summary>
        Semicolon = 59,
        /// <summary>
        /// 等于
        /// </summary>
        Equal = 61,
        /// <summary>
        /// A
        /// </summary>
        A = 65,
        /// <summary>
        /// B
        /// </summary>
        B = 66,
        /// <summary>
        /// C
        /// </summary>
        C = 67,
        /// <summary>
        /// D
        /// </summary>
        D = 68,
        /// <summary>
        /// E
        /// </summary>
        E = 69,
        /// <summary>
        /// F
        /// </summary>
        F = 70,
        /// <summary>
        /// G
        /// </summary>
        G = 71,
        /// <summary>
        /// H
        /// </summary>
        H = 72,
        /// <summary>
        /// I
        /// </summary>
        I = 73,
        /// <summary>
        /// J
        /// </summary>
        J = 74,
        /// <summary>
        /// K
        /// </summary>
        K = 75,
        /// <summary>
        /// L
        /// </summary>
        L = 76,
        /// <summary>
        /// M
        /// </summary>
        M = 77,
        /// <summary>
        /// N
        /// </summary>
        N = 78,
        /// <summary>
        /// O
        /// </summary>
        O = 79,
        /// <summary>
        /// P
        /// </summary>
        P = 80,
        /// <summary>
        /// Q
        /// </summary>
        Q = 81,
        /// <summary>
        /// R
        /// </summary>
        R = 82,
        /// <summary>
        /// S
        /// </summary>
        S = 83,
        /// <summary>
        /// T
        /// </summary>
        T = 84,
        /// <summary>
        /// U
        /// </summary>
        U = 85,
        /// <summary>
        /// V
        /// </summary>
        V = 86,
        /// <summary>
        /// W
        /// </summary>
        W = 87,
        /// <summary>
        /// X
        /// </summary>
        X = 88,
        /// <summary>
        /// Y
        /// </summary>
        Y = 89,
        /// <summary>
        /// Z
        /// </summary>
        Z = 90,
        /// <summary>
        /// 左中括号
        /// </summary>
        LeftBracket = 91,
        /// <summary>
        /// 反斜杠
        /// </summary>
        Backslash = 92,
        /// <summary>
        /// 右中括号
        /// </summary>
        RightBracket = 93,
        /// <summary>
        /// 重音号
        /// </summary>
        GraveAccent = 96,
        /// <summary>
        /// non-US #1
        /// </summary>
        World1 = 161,
        /// <summary>
        /// non-US #2
        /// </summary>
        World2 = 162,
        /// <summary>
        /// ESC
        /// </summary>
        Escape = 256,
        /// <summary>
        /// 回车
        /// </summary>
        Enter = 257,
        /// <summary>
        /// table
        /// </summary>
        Tab = 258,
        /// <summary>
        /// 回退
        /// </summary>
        Backspace = 259,
        /// <summary>
        /// 插入
        /// </summary>
        Insert = 260,
        /// <summary>
        /// 删除
        /// </summary>
        Delete = 261,
        /// <summary>
        /// 右
        /// </summary>
        Right = 262,
        /// <summary>
        /// 左
        /// </summary>
        Left = 263,
        /// <summary>
        /// 下
        /// </summary>
        Down = 264,
        /// <summary>
        /// 上
        /// </summary>
        Up = 265,
        /// <summary>
        /// 上一页
        /// </summary>
        PageUp = 266,
        /// <summary>
        /// 下一页
        /// </summary>
        PageDown = 267,
        /// <summary>
        /// 主页
        /// </summary>
        Home = 268,
        /// <summary>
        /// 尾部
        /// </summary>
        End = 269,
        /// <summary>
        /// 大小写转换
        /// </summary>
        CapsLock = 280,
        /// <summary>
        /// 锁屏
        /// </summary>
        ScrollLock = 281,
        /// <summary>
        /// 小键盘锁
        /// </summary>
        NumLock = 282,
        /// <summary>
        /// 截屏
        /// </summary>
        PrintScreen = 283,
        /// <summary>
        /// 暂停
        /// </summary>
        Pause = 284,
        /// <summary>
        /// F1
        /// </summary>
        F1 = 290,
        /// <summary>
        /// F2
        /// </summary>
        F2 = 291,
        /// <summary>
        /// F3
        /// </summary>
        F3 = 292,
        /// <summary>
        /// F4
        /// </summary>
        F4 = 293,
        /// <summary>
        /// F5
        /// </summary>
        F5 = 294,
        /// <summary>
        /// F6
        /// </summary>
        F6 = 295,
        /// <summary>
        /// F7
        /// </summary>
        F7 = 296,
        /// <summary>
        /// F8
        /// </summary>
        F8 = 297,
        /// <summary>
        /// F9
        /// </summary>
        F9 = 298,
        /// <summary>
        /// F10
        /// </summary>
        F10 = 299,
        /// <summary>
        /// F11
        /// </summary>
        F11 = 300,
        /// <summary>
        /// F12
        /// </summary>
        F12 = 301,
        /// <summary>
        /// F13
        /// </summary>
        F13 = 302,
        /// <summary>
        /// F14
        /// </summary>
        F14 = 303,
        /// <summary>
        /// F15
        /// </summary>
        F15 = 304,
        /// <summary>
        /// F16
        /// </summary>
        F16 = 305,
        /// <summary>
        /// F17
        /// </summary>
        F17 = 306,
        /// <summary>
        /// F18
        /// </summary>
        F18 = 307,
        /// <summary>
        /// F19
        /// </summary>
        F19 = 308,
        /// <summary>
        /// F20
        /// </summary>
        F20 = 309,
        /// <summary>
        /// F21
        /// </summary>
        F21 = 310,
        /// <summary>
        /// F22
        /// </summary>
        F22 = 311,
        /// <summary>
        /// F23
        /// </summary>
        F23 = 312,
        /// <summary>
        /// F24
        /// </summary>
        F24 = 313,
        /// <summary>
        /// F25
        /// </summary>
        F25 = 314,
        /// <summary>
        /// 
        /// </summary>
        KeyKp0 = 320,
        /// <summary>
        /// 
        /// </summary>
        KeyKp1 = 321,
        /// <summary>
        /// 
        /// </summary>
        KeyKp2 = 322,
        /// <summary>
        /// 
        /// </summary>
        KeyKp3 = 323,
        /// <summary>
        /// 
        /// </summary>
        KeyKp4 = 324,
        /// <summary>
        /// 
        /// </summary>
        KeyKp5 = 325,
        /// <summary>
        /// 
        /// </summary>
        KeyKp6 = 326,
        /// <summary>
        /// 
        /// </summary>
        KeyKp7 = 327,
        /// <summary>
        /// 
        /// </summary>
        KeyKp8 = 328,
        /// <summary>
        /// 
        /// </summary>
        KeyKp9 = 329,
        /// <summary>
        /// 
        /// </summary>
        KeyKpDecimal = 330,
        /// <summary>
        /// 
        /// </summary>
        KeyKpDivide = 331,
        /// <summary>
        /// 
        /// </summary>
        KeyKpMultiply = 332,
        /// <summary>
        /// 
        /// </summary>
        KeyKpSubtract = 333,
        /// <summary>
        /// 
        /// </summary>
        KeyKpAdd = 334,
        /// <summary>
        /// 
        /// </summary>
        KeyKpEnter = 335,
        /// <summary>
        /// 
        /// </summary>
        KeyKpEqual = 336,
        /// <summary>
        /// 
        /// </summary>
        KeyKpLeftShift = 340,
        /// <summary>
        /// 
        /// </summary>
        KeyKpLeftControl = 341,
        /// <summary>
        /// 
        /// </summary>
        KeyKpLeftAlt = 342,
        /// <summary>
        /// 
        /// </summary>
        KeyKpLeftSuper = 343,
        /// <summary>
        /// 
        /// </summary>
        KeyKpRightShift = 344,
        /// <summary>
        /// 
        /// </summary>
        KeyKpRightControl = 345,
        /// <summary>
        /// 
        /// </summary>
        KeyKpRightAlt = 346,
        /// <summary>
        /// 
        /// </summary>
        KeyKpRightSuper = 347,
        /// <summary>
        /// 
        /// </summary>
        KeyKpMenu = 348,
        /// <summary>
        /// 
        /// </summary>
        KeyKpLast = KeyKpMenu
    }
}
