﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA
{
    /// <summary>
    /// 父模块
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class ParentModuleAttribute : Attribute
    {
        /// <summary>
        /// 父模块
        /// </summary>
        public Type ParentType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentType"></param>
        public ParentModuleAttribute(Type parentType)
        {
            ParentType = parentType;
        }
    }
}
