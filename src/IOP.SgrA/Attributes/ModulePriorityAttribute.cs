﻿using System;

namespace IOP.SgrA
{
    /// <summary>
    /// 模块优先级标签
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class ModulePriorityAttribute : Attribute
    {
        /// <summary>
        /// 优先级
        /// </summary>
        public ModulePriority ModulePriority { get; } = ModulePriority.None;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="modulePriority"></param>
        public ModulePriorityAttribute(ModulePriority modulePriority)
        {
            ModulePriority = modulePriority;
        }
    }
}
