﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 视口矩形
    /// </summary>
    public struct ViewportRect
    {
        /// <summary>
        /// 宽度
        /// </summary>
        public float Width;
        /// <summary>
        /// 长度
        /// </summary>
        public float Height;
        /// <summary>
        /// X
        /// </summary>
        public float X;
        /// <summary>
        /// Y
        /// </summary>
        public float Y;
        /// <summary>
        /// 最小深度
        /// </summary>
        public float MinDepth;
        /// <summary>
        /// 最大深度
        /// </summary>
        public float MaxDepth;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="maxDepth"></param>
        /// <param name="minDepth"></param>
        public ViewportRect(float x, float y, float width, float height, float minDepth, float maxDepth)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
            MinDepth = minDepth;
            MaxDepth = maxDepth;
        }
    }
}
