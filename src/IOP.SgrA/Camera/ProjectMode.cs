﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 投影模式
    /// </summary>
    public enum ProjectMode
    {
        /// <summary>
        /// 正交
        /// </summary>
        Orthogonal,
        /// <summary>
        /// 透视
        /// </summary>
        Perspective
    }
}
