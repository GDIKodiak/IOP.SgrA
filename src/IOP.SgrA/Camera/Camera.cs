﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 摄像机
    /// </summary>
    public class Camera
    {
        /// <summary>
        /// 摄像机坐标
        /// </summary>
        public Vector3 EyePosition { get { return _EyePosition; } private set { _EyePosition = value; } }
        /// <summary>
        /// 相对于摄像机向上的向量
        /// </summary>
        public Vector3 TargetUp { get { return _TargetUp; } private set { _TargetUp = value; } }
        /// <summary>
        /// 摄影机姿态
        /// </summary>
        public Quaternion Quaternion { get { return _Quaternion; } private set { _Quaternion = value; } }
        /// <summary>
        /// 摄影机欧拉角(roll, pitch, yaw)
        /// </summary>
        public Vector3 EulerAngles { get { return _EulerAngles; } private set { _EulerAngles = value; } }
        /// <summary>
        /// 基矩阵
        /// </summary>
        public Matrix4x4 BasicViewMatrix { get { return _BasicViewMatrix; } private set { _BasicViewMatrix = value; } }
        /// <summary>
        /// 视角矩阵
        /// </summary>
        public Matrix4x4 ViewMatrix { get { return Matrix4x4.CreateTranslation(-EyePosition) * _BasicViewMatrix; } }
        /// <summary>
        /// 映射矩阵
        /// </summary>
        public Matrix4x4 ProjectionMatrix { get { return _ProjectionMatrix; } private set { _ProjectionMatrix = value; } }
        /// <summary>
        /// 摄影机朝向向量
        /// </summary>
        public Vector3 N { get; private set; }
        /// <summary>
        /// 摄影机右向量
        /// </summary>
        public Vector3 U { get; private set; }
        /// <summary>
        /// 摄影机顶向量
        /// </summary>
        public Vector3 V { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public float Left { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public float Top { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public float Near { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public float Far { get; private set; }
        /// <summary>
        /// 投影模式
        /// </summary>
        public ProjectMode ProjectMode { get; private set; }
        /// <summary>
        /// 视口
        /// </summary>
        public ViewportRect Viewport { get; private set; }
        /// <summary>
        /// 裁剪区域
        /// </summary>
        public Area Scissor { get; private set; }
        /// <summary>
        /// 俯仰角琐
        /// </summary>
        public bool PitchLock { get; set; } = true;
        /// <summary>
        /// 是否深度反转
        /// </summary>
        public bool ReversedZ { get; set; } = false;

        /// <summary>
        /// 摄影机Y轴
        /// </summary>
        public static readonly Vector3 CameraY = new Vector3(0.0f, 1.0f, 0.0f);
        /// <summary>
        /// 摄影机X轴
        /// </summary>
        public static readonly Vector3 CameraX = new Vector3(1.0f, 0.0f, 0.0f);
        /// <summary>
        /// 摄影机Z轴
        /// </summary>
        public static readonly Vector3 CameraZ = new Vector3(0.0f, 0.0f, 1.0f);

        private static readonly float MinPitch = -89.5f * MathF.PI / 180.0f;
        private static readonly float MaxPitch = 89.5f * MathF.PI / 180.0f;

        private Quaternion _Quaternion;
        private Vector3 _EulerAngles;
        private Vector3 _TargetUp = CameraY;
        private Vector3 _EyePosition;
        private Matrix4x4 _BasicViewMatrix = Matrix4x4.Identity;
        private Matrix4x4 _ProjectionMatrix = Matrix4x4.Identity;

        /// <summary>
        /// 构造函数
        /// </summary>
        public Camera()
        {
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="createPosition"></param>
        /// <param name="createTarget"></param>
        /// <param name="createTargetUp"></param>
        public Camera(Vector3 createPosition, Vector3 createTarget, Vector3 createTargetUp)
        {
            InitCamera(createPosition, createTarget, createTargetUp);
        }

        /// <summary>
        /// 重置摄影机
        /// </summary>
        /// <param name="createPosition"></param>
        /// <param name="createTarget"></param>
        /// <param name="createTargetUp"></param>
        /// <returns></returns>
        public Camera ResetCamera(Vector3 createPosition, Vector3 createTarget, Vector3 createTargetUp)
        {
            InitCamera(createPosition, createTarget, createTargetUp);
            return this;
        }

        /// <summary>
        /// 设置投影矩阵
        /// </summary>
        /// <param name="left"></param>
        /// <param name="top"></param>
        /// <param name="near"></param>
        /// <param name="far"></param>
        /// <param name="projectMode"></param>
        /// <param name="reversedZ"></param>
        public void SetProjectMatrix(float left, float top, float near, float far, 
            ProjectMode projectMode = ProjectMode.Perspective, bool reversedZ = false)
        {
            Left = left;
            Top = top;
            Near = near;
            Far = far;
            ReversedZ = reversedZ;
            ProjectMode = projectMode;
            _ProjectionMatrix = projectMode switch
            {
                ProjectMode.Orthogonal => Matrix4x4.CreateOrthographicOffCenter(left, -left, -top, top, near, far),
                _ => Matrix4x4.CreatePerspectiveOffCenter(left, -left, -top, top, near, far),
            };
            if (ReversedZ)
            {
                _ProjectionMatrix.M33 = -near / (far - near);
                _ProjectionMatrix.M43 = far * near / (far - near);
            }
        }
        /// <summary>
        /// 设置视口
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="minDepth"></param>
        /// <param name="maxDepth"></param>
        public void SetViewport(float x, float y, float width, float height, float minDepth, float maxDepth) => Viewport = new ViewportRect(x, y, width, height, minDepth, maxDepth);
        /// <summary>
        /// 设置裁剪区域
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public void SetScissor(float x, float y, float width, float height)
        {
            var viewPort = Viewport;
            if (width > viewPort.Width || width < 0) width = viewPort.Width;
            if (height > viewPort.Height || height < 0) height = viewPort.Height;
            Scissor = new Area((int)x, (int)y, (uint)width, (uint)height);
        }

        /// <summary>
        /// 通过鼠标移动差值旋转摄像机
        /// </summary>
        /// <param name="xDelta">鼠标X轴差值</param>
        /// <param name="yDelta">鼠标Y轴差值</param>
        /// <param name="rad"></param>
        /// <returns></returns>
        public Matrix4x4 RotationFromMouse(float xDelta, float yDelta, float rad) => Rotation(new Vector3(0, yDelta * rad, xDelta * rad));
        /// <summary>
        /// 根据鼠标差值以及指定点旋转摄影机
        /// </summary>
        /// <param name="position"></param>
        /// <param name="xDelta"></param>
        /// <param name="yDelta"></param>
        /// <param name="rad"></param>
        /// <returns></returns>
        public Matrix4x4 EncircleFromMouse(Vector3 position, float xDelta, float yDelta, float rad) => Encircle(position, new Vector3(0, yDelta * rad, xDelta * rad));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="position"></param>
        /// <param name="rollPitchYaw"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Matrix4x4 Encircle(Vector3 position, Vector3 rollPitchYaw)
        {
            var length = (EyePosition - position).Length();
            var e = _EulerAngles;
            var l = e + rollPitchYaw;
            if (PitchLock) l.Y = Math.Clamp(l.Y, MinPitch, MaxPitch);
            _EulerAngles = l;
            var q = l.ToInertiaZYXQuaternion();
            var n = Vector3.Transform(CameraZ, q);
            var u = Vector3.Transform(CameraY, q);
            UpdateCamera(n, u);
            EyePosition = n * length;
            return ViewMatrix;
        }
        /// <summary>
        /// 摄影机绕某一点环绕
        /// </summary>
        /// <param name="position"></param>
        /// <param name="axis"></param>
        /// <param name="rad"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Matrix4x4 Encircle(Vector3 position, Vector3 axis, float rad)
        {
            Quaternion q = Quaternion.CreateFromAxisAngle(Vector3.Normalize(axis), rad);
            var yawPitchRoll = q.ToInertiaZYXEulerAngles();
            return Encircle(position, yawPitchRoll);
        }

        /// <summary>
        /// 旋转摄影机
        /// </summary>
        /// <param name="rollPitchYaw"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Matrix4x4 Rotation(Vector3 rollPitchYaw)
        {
            var e = _EulerAngles;
            var l = e + rollPitchYaw;
            if (PitchLock) l.Y = Math.Clamp(l.Y, MinPitch, MaxPitch);
            _EulerAngles = l;
            var q = l.ToInertiaZYXQuaternion();
            var n = Vector3.Transform(CameraZ, q);
            var u = Vector3.Transform(CameraY, q);
            UpdateCamera(n, u);
            return ViewMatrix;
        }
        /// <summary>
        /// 旋转摄影机
        /// </summary>
        /// <param name="axis">旋转轴</param>
        /// <param name="rad">角度</param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Matrix4x4 Rotation(Vector3 axis, float rad)
        {
            Quaternion q = Quaternion.CreateFromAxisAngle(Vector3.Normalize(axis), rad);
            var d = q.ToInertiaZYXEulerAngles();
            return Rotation(d);
        }

        /// <summary>
        /// 绕X轴环绕某指定点
        /// </summary>
        /// <param name="position"></param>
        /// <param name="rad"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Matrix4x4 EncircleForX(Vector3 position, float rad) => Encircle(position, CameraX, rad);
        /// <summary>
        /// 绕Y轴环绕某指定点
        /// </summary>
        /// <param name="position"></param>
        /// <param name="rad"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Matrix4x4 EncircleForY(Vector3 position, float rad) => Encircle(position, CameraY, rad);
        /// <summary>
        /// 绕Z轴环绕某指定点
        /// </summary>
        /// <param name="position"></param>
        /// <param name="rad"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Matrix4x4 EncircleForZ(Vector3 position, float rad) => Encircle(position, CameraZ, rad);

        /// <summary>
        /// 将摄像机X轴旋转指定角度
        /// </summary>
        /// <param name="rad"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Matrix4x4 RotationForX(float rad) => Rotation(CameraX, rad);
        /// <summary>
        /// 将摄像机绕Y轴旋转指定角度
        /// </summary>
        /// <param name="rad"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Matrix4x4 RotationForY(float rad) => Rotation(CameraY, rad);
        /// <summary>
        /// 将摄像机Z轴旋转指定角度
        /// </summary>
        /// <param name="rad"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Matrix4x4 RotationForZ(float rad) => Rotation(CameraZ, rad);

        /// <summary>
        /// 通过鼠标移动差值平移摄像机
        /// </summary>
        /// <param name="xDelta"></param>
        /// <param name="yDelta"></param>
        /// <returns></returns>
        public Matrix4x4 TranslationFromMouse(float xDelta, float yDelta) => Translation(xDelta, yDelta, 0);
        /// <summary>
        /// 平移摄像机
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Matrix4x4 Translation(float x, float y, float z)
        {
            Vector3 v = new Vector3(x, y, z);
            Matrix4x4.Invert(_BasicViewMatrix, out Matrix4x4 view);
            var vector = Vector3.Transform(v, view);
            EyePosition += vector;
            return ViewMatrix;
        }

        /// <summary>
        /// 平移摄影机至
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Matrix4x4 TranslationTo(float x, float y, float z)
        {
            _EyePosition = new Vector3(x, y, z);
            return ViewMatrix;
        }

        /// <summary>
        /// 平行移动摄影机
        /// </summary>
        /// <param name="x"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public Matrix4x4 ParallelTranslation(float x, float z)
        {
            var v = new Vector3(x, 0, z);
            var n = Vector3.Normalize(new Vector3(N.X, 0, N.Z));
            var u = CameraY;
            var r = Vector3.Cross(u, n);
            var mat = new Matrix4x4(r.X, r.Y, r.Z, 0,
                u.X, u.Y, u.Z, 0,
                n.X, n.Y, n.Z, 0,
                0, 0, 0, 1.0f);
            v = Vector3.Transform(v, mat);
            _EyePosition += v;
            return ViewMatrix;
        }

        /// <summary>
        /// 将屏幕坐标投影到世界坐标
        /// </summary>
        /// <param name="winPos"></param>
        /// <param name="zPlane"></param>
        /// <returns></returns>
        public Vector3 ScreenToWorldPoint(Vector2 winPos, float zPlane = 1.0f)
        {
            zPlane = MathF.Min(zPlane, 1.0f);
            zPlane = MathF.Max(zPlane, 0.0f);
            float ndcX = winPos.X * 2.0f / Viewport.Width - 1.0f;
            float ndcY = 1.0f - (winPos.Y * 2.0f / Viewport.Height);
            Vector3 ndc = new Vector3(ndcX, ndcY, zPlane);
            Matrix4x4 inProj = Matrix4x4.Invert(ProjectionMatrix, out Matrix4x4 p) ? p : Matrix4x4.Identity;
            Vector4 clip = Vector4.Transform(new Vector4(ndc, 1.0f), inProj);
            Matrix4x4 inView = Matrix4x4.Invert(ViewMatrix, out Matrix4x4 v) ? v : Matrix4x4.Identity;
            Vector4 world = Vector4.Transform(clip, inView);
            if (world.W != 0.0f)
            {
                world = new Vector4(world.X / world.W, world.Y / world.W, world.Z / world.W, 1.0f);
            }
            var origin = new Vector3(world.X, world.Y, world.Z);
            return new Vector3(origin.X, origin.Y, world.Z);
        }
        /// <summary>
        /// 生成射线
        /// </summary>
        /// <param name="winPos"></param>
        /// <returns></returns>
        public Ray GenerateRay(Vector2 winPos)
        {
            var z = ReversedZ ? 1.0f : 0.0f;
            float ndcX = winPos.X * 2.0f / Viewport.Width - 1.0f;
            float ndcY = 1.0f - (winPos.Y * 2.0f / Viewport.Height);
            Vector3 ndc = new Vector3(ndcX, ndcY, z);
            Matrix4x4 inProj = Matrix4x4.Invert(ProjectionMatrix, out Matrix4x4 p) ? p : Matrix4x4.Identity;
            Vector4 clip = Vector4.Transform(new Vector4(ndc, 1.0f), inProj);
            Matrix4x4 inView = Matrix4x4.Invert(ViewMatrix, out Matrix4x4 v) ? v : Matrix4x4.Identity;
            Vector4 world = Vector4.Transform(clip, inView);
            if(world.W != 0.0f)
            {
                world = new Vector4(world.X / world.W, world.Y / world.W, world.Z / world.W, 1.0f);
            }
            Vector3 origin = new Vector3(world.X, world.Y, world.Z);
            Vector3 direction = Vector3.Normalize(origin - _EyePosition);
            return new Ray(origin, direction);
        }
        /// <summary>
        /// 射线拾取
        /// </summary>
        /// <param name="ray"></param>
        /// <param name="bound"></param>
        /// <returns></returns>
        public bool RayPicking(Ray ray, AABB bound)
        {
            Vector3 direction = ray.Direction;
            float dirX = direction.X != 0.0f ? 1.0f / direction.X : 0.0f;
            float dirY = direction.Y != 0.0f ? 1.0f / direction.Y : 0.0f;
            float dirZ = direction.Z != 0.0f ? 1.0f / direction.Z : 0.0f;
            Vector3 inDir = new Vector3(dirX, dirY, dirZ);
            Vector3 origin = ray.Origin;
            Vector3 min = bound.Min();
            Vector3 max = bound.Max();
            Vector3 iMin = (min - origin) * inDir;
            Vector3 iMax = (max - origin) * inDir;
            if (direction.X < 0.0f) (iMax.X, iMin.X) = (iMin.X, iMax.X);
            if (direction.Y < 0.0f) (iMax.Y, iMin.Y) = (iMin.Y, iMax.Y);
            if (direction.Z < 0.0f) (iMax.Z, iMin.Z) = (iMin.Z, iMax.Z);
            float n = MathF.Max(iMin.X, MathF.Max(iMin.Y, iMin.Z));
            float f = MathF.Min(iMax.X, MathF.Min(iMax.Y, iMax.Z));
            return f >= n && n > 0;
        }
        /// <summary>
        /// 射线拾取
        /// </summary>
        /// <param name="winPos"></param>
        /// <param name="bounds"></param>
        /// <returns></returns>
        public bool RayPicking(Vector2 winPos, AABB bounds)
        {
            Ray ray = GenerateRay(winPos);
            return RayPicking(ray, bounds);
        }

        /// <summary>
        /// 缩放摄影机
        /// </summary>
        /// <param name="offset"></param>
        /// <returns></returns>
        public Matrix4x4 Scroll(float offset)
        {
            var v = new Vector3(0, 0, -offset);
            var n = N;
            var u = CameraY;
            var r = Vector3.Cross(u, n);
            var mat = new Matrix4x4(r.X, r.Y, r.Z, 0,
                u.X, u.Y, u.Z, 0,
                n.X, n.Y, n.Z, 0,
                0, 0, 0, 1.0f);
            v = Vector3.Transform(v, mat);
            _EyePosition += v;
            return ViewMatrix;
        }

        /// <summary>
        /// 清理摄影机
        /// </summary>
        internal void Clear()
        {
        }

        /// <summary>
        /// 更新摄影机
        /// </summary>
        /// <param name="d"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void UpdateCamera(Vector3 d)
        {
            var t = _TargetUp;
            var r = Vector3.Cross(t, d);
            if (r == Vector3.Zero) throw new ArgumentException($"Illegal vector, the target direction vector {d} is parallel with up vector {t}");

            d = Vector3.Normalize(d);
            r = Vector3.Normalize(r);
            N = d;
            U = r;
            var u = Vector3.Cross(d, r);
            V = u;

            _BasicViewMatrix = new Matrix4x4(r.X, u.X, d.X, 0.0f,
                r.Y, u.Y, d.Y, 0.0f,
                r.Z, u.Z, d.Z, 0.0f,
                0.0f, 0.0f, 0.0f, 1.0f);
        }
        /// <summary>
        /// 更新摄影机
        /// </summary>
        /// <param name="d"></param>
        /// <param name="u"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void UpdateCamera(Vector3 d, Vector3 u)
        {
            N = d;
            U = u;
            var r = Vector3.Cross(u, d);
            V = r;
            _BasicViewMatrix = new Matrix4x4(r.X, u.X, d.X, 0.0f,
                r.Y, u.Y, d.Y, 0.0f,
                r.Z, u.Z, d.Z, 0.0f,
                0.0f, 0.0f, 0.0f, 1.0f);
        }
        /// <summary>
        /// 初始化摄影机
        /// </summary>
        /// <param name="p"></param>
        /// <param name="target"></param>
        /// <param name="up"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void InitCamera(Vector3 p, Vector3 target, Vector3 up)
        {
            _EyePosition = p;
            _TargetUp = up;
            var n = p - target;
            if (n == Vector3.Zero) n = CameraZ;
            UpdateCamera(n);
            _EulerAngles = _BasicViewMatrix.ToObjectZYXEulerAngles();
            _Quaternion = _EulerAngles.ToObjectZYXQuaternion();
        }

    }
}
