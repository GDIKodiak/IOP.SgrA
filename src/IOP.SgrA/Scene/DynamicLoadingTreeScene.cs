﻿using IOP.Extension.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 动态加载树场景
    /// </summary>
    public class DynamicLoadingTreeScene : Scene
    {
#nullable disable
        /// <summary>
        /// 管理器
        /// </summary>
        [Autowired]
        protected IGraphicsManager Manager { get; set; }
        /// <summary>
        /// 根对象
        /// </summary>
        protected RenderObject Root { get; set; }
#nullable enable

        /// <summary>
        /// 执行渲染
        /// </summary>
        /// <param name="fIndex"></param>
        public override void Rendering(uint fIndex = 0)
        {
            base.Rendering(fIndex);
        }
        /// <summary>
        /// 初始化
        /// </summary>
        public override void Initialization()
        {
            base.Initialization();
            Root = Manager.CreateRenderObject<RenderObject>("ROOT");
        }
    }
}
