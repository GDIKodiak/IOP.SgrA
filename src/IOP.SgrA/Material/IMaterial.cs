﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA
{
    /// <summary>
    /// 通用材质接口
    /// </summary>
    public interface IMaterial : IRenderComponent
    {
        /// <summary>
        /// 材质所属缓冲纹理
        /// </summary>
        ITexture BufferTexture { get; }
        /// <summary>
        /// 更新至GPU缓冲
        /// </summary>
        void UpdateToBuffer();
    }
}
