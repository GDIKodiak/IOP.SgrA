﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace IOP.SgrA
{
    /// <summary>
    /// 简单PBR材质
    /// </summary>
    public class SimplePBRMaterial : IMaterial
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; protected internal set; }
        /// <summary>
        /// 基础色
        /// </summary>
        public Vector3 Albedo { get; set; }
        /// <summary>
        /// 金属度
        /// </summary>
        public float Metallic { get; set; }
        /// <summary>
        /// 粗糙度
        /// </summary>
        public float Roughness { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ITexture BufferTexture { get; protected set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="bufferTexture"></param>
        public SimplePBRMaterial(string name, ITexture bufferTexture)
        {
            Name = name;
            BufferTexture = bufferTexture;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newObj"></param>
        public void CloneToNewRender(IRenderObject newObj)
        {
            if (newObj != null && !newObj.IsContainsComponent(Name))
            {
                var tex = BufferTexture.DeepCopy();
                SimplePBRMaterial material = new SimplePBRMaterial(Name, tex) 
                { 
                    Metallic = Metallic,
                    Roughness = Roughness,
                    Albedo = Albedo
                };
                newObj.AddComponent(material);
                newObj.AddComponent(tex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderObject"></param>
        public void OnAttach(IRenderObject renderObject) { }
        /// <summary>
        /// 
        /// </summary>
        public void Destroy()
        {
            BufferTexture?.Destroy();
        }

        /// <summary>
        /// 
        /// </summary>
        public void UpdateToBuffer()
        {
            if (BufferTexture == null) return;
            SimplePBR str = new SimplePBR(Albedo, Metallic, Roughness);
            BufferTexture.UpdateTextureMemoryData(str);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 32)]
    public struct SimplePBR
    {
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public Vector4 Albedo;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(16)]
        public Vector4 MRA;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="albedo"></param>
        /// <param name="metallic"></param>
        /// <param name="roughness"></param>
        public SimplePBR(Vector3 albedo, float metallic, float roughness)
        {
            Albedo = new Vector4(albedo, 0.0f);
            MRA = new Vector4(metallic, roughness, 1.0f, 0.0f);
        }
    }
}
