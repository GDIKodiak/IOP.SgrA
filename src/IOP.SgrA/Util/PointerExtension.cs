﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 指针对象扩展
    /// </summary>
    public static class PointerExtension
    {
        /// <summary>
        /// 转换为被固定的字节数组
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static PinnedObjectArray<byte[]> ToPinnedBytes(this string[] str)
        {
            if (str == null || !str.Any()) return PinnedObjectArray<byte[]>.Null();
            byte[][] bytes = new byte[str.Length][];
            for(int i = 0; i < str.Length; i++)
            {
                if (string.IsNullOrEmpty(str[i])) bytes[i] = new byte[0];
                else bytes[i] = Encoding.UTF8.GetBytes(str[i]);
            }
            return PinnedObjectArray<byte[]>.Create(bytes);
        }
    }
}
