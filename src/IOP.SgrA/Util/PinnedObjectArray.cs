﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata;
using System.Runtime.InteropServices;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 位置固定的对象数组
    /// </summary>
    public struct PinnedObjectArray<T>
    {
        /// <summary>
        /// 数组长度
        /// </summary>
        public int Length => Handles == null ? 0 : Handles.Length;
        /// <summary>
        /// 是否为空
        /// </summary>
        public bool IsNull => Handles.Length <= 0;

        private IntPtr[] Pointers { get; set; }
        private GCHandle[] Handles { get; set; }
        private GCHandle ArrayHandle { get; set; }
        /// <summary>
        /// 创建
        /// </summary>
        /// <param name="objects"></param>
        /// <returns></returns>
        public static PinnedObjectArray<T> Create(T[] objects)
        {
            if (objects == null || objects.Length <= 0) return Null();
            PinnedObjectArray<T> array = new PinnedObjectArray<T>();
            var handles = new GCHandle[objects.Length];
            var points = new IntPtr[objects.Length];
            array.Handles = handles;
            array.Pointers = points;
            for(int i = 0; i < handles.Length; i++)
            {
                T local = objects[i];
                if (local == null)
                {
                    handles[i] = new GCHandle();
                    points[i] = IntPtr.Zero;
                }
                else
                {
                    var h  = GCHandle.Alloc(local, GCHandleType.Pinned);
                    handles[i] = h;
                    points[i] = h.AddrOfPinnedObject();
                }
            }
            var aHandle = GCHandle.Alloc(points, GCHandleType.Pinned);
            array.ArrayHandle = aHandle;
            return array;
        }
        /// <summary>
        /// 转换为二级指针
        /// </summary>
        /// <returns></returns>
        public unsafe byte** ToPointerArray()
        {           
            if (!ArrayHandle.IsAllocated) return null;
            else return (byte**)ArrayHandle.AddrOfPinnedObject();
        }
        /// <summary>
        /// 获取数组指定下标处指针
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public IntPtr GetPointer(int index)
        {
            if (index < 0 || index > Handles.Length) return IntPtr.Zero;
            else return Pointers[index];
        }
        /// <summary>
        /// 释放
        /// </summary>
        public void Free()
        {
            if (Handles == null) return;
            else
            {
                for(int i = 0; i < Handles.Length; i++)
                {
                    var local = Handles[i];
                    if (local.IsAllocated && local.Target != null) local.Free();
                }
                if (ArrayHandle.IsAllocated) ArrayHandle.Free();
            }
        }
        /// <summary>
        /// 空对象
        /// </summary>
        /// <returns></returns>
        public static PinnedObjectArray<T> Null()
        {
            return new PinnedObjectArray<T>() { Handles = Array.Empty<GCHandle>(), Pointers = Array.Empty<IntPtr>() };
        }
    }
}
