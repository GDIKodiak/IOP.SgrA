﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 位置固定的对象
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public struct PinnedObject<T>
    {
        /// <summary>
        /// 源类型
        /// </summary>
        public SourceType SourceType { get; private set; }
        /// <summary>
        /// GC对象
        /// </summary>
        private GCHandle Handle;
        /// <summary>
        /// 转换为指针
        /// </summary>
        /// <returns></returns>
        public unsafe byte* ToPointer()
        {
            if (!Handle.IsAllocated) return null;
            else if (Handle.Target == null) return (byte*)GCHandle.ToIntPtr(Handle);
            else return (byte*)Handle.AddrOfPinnedObject();
        }
        /// <summary>
        /// 转换为指针
        /// </summary>
        /// <returns></returns>
        public IntPtr ToIntPtr()
        {
            if (!Handle.IsAllocated) return IntPtr.Zero;
            else if (Handle.Target == null) return GCHandle.ToIntPtr(Handle);
            else return Handle.AddrOfPinnedObject();
        }
        /// <summary>
        /// 释放对象
        /// </summary>
        public void Free()
        {
            if (Handle.IsAllocated && SourceType != SourceType.POINTER) Handle.Free();
        }
        /// <summary>
        /// 是否为空
        /// </summary>
        public bool IsNull => !Handle.IsAllocated;

        /// <summary>
        /// 创建对象
        /// </summary>
        /// <param name="object"></param>
        /// <returns></returns>
        public static PinnedObject<T> Create(T @object)
        {
            if (@object == null) return Null();
            PinnedObject <T> obj = new PinnedObject<T>();
            obj.Handle = GCHandle.Alloc(@object, GCHandleType.Pinned);
            obj.SourceType = SourceType.OBJECT;
            return obj;
        }
        /// <summary>
        /// 创建对象
        /// </summary>
        /// <param name="ptr"></param>
        /// <returns></returns>
        public static PinnedObject<T> Create(IntPtr ptr)
        {
            if (ptr == IntPtr.Zero) return Null();
            PinnedObject<T> obj = new PinnedObject<T>();
            obj.Handle = GCHandle.FromIntPtr(ptr);
            obj.SourceType = SourceType.POINTER;
            return obj;
        }
        /// <summary>
        /// 空对象
        /// </summary>
        /// <returns></returns>
        public static PinnedObject<T> Null()
        {
            return new PinnedObject<T>() { SourceType = SourceType.NULL };
        }
    }

    /// <summary>
    /// 源类型
    /// </summary>
    public enum SourceType
    {
        /// <summary>
        /// 空对象
        /// </summary>
        NULL,
        /// <summary>
        /// 托管对象
        /// </summary>
        OBJECT,
        /// <summary>
        /// 指针
        /// </summary>
        POINTER
    }
}
