﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 常量缓冲
    /// </summary>
    public class ConstantBuffer : IRenderComponent, IDisposable
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 常量区域
        /// </summary>
        public ulong Range { get; private set; } = 0;
        /// <summary>
        /// 缓冲
        /// </summary>
        public byte[] Buffer { get; private set; } = Array.Empty<byte>();
        /// <summary>
        /// 
        /// </summary>
        protected bool IsDispose = false;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="size"></param>
        public ConstantBuffer(ulong size)
        {
            byte[] buffer = new byte[size];
            Buffer = buffer;
            Range = (ulong)buffer.LongLength;
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="data"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public ConstantBuffer(byte[] data)
        {
            Buffer = data ?? throw new ArgumentNullException(nameof(data));
            Range = (ulong)data.LongLength;
        }
        /// <summary>
        /// 获取缓冲
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public Span<byte> GetBuffer(uint offset, uint length)
        {
            byte[] old = Buffer;
            if (offset > Range)
            {
                byte[] buffer = UpdateBuffer(offset);
                return buffer.AsSpan().Slice((int)offset, (int)length);
            }
            else
            {
                return old.AsSpan().Slice((int)offset, (int)length);
            }
        }

        /// <summary>
        /// 更新常量缓冲
        /// </summary>
        /// <param name="data"></param>
        /// <param name="offset"></param>
        public void Update(Span<byte> data, uint offset)
        {
            if (data == null) return;
            ulong length = (ulong)data.Length + offset;
            if(length > Range)
            {
                byte[] buffer = UpdateBuffer(length);
                data.CopyTo(buffer.AsSpan()[(int)offset..]);
            }
            else
            {
                byte[] buffer = Buffer;
                data.CopyTo(buffer.AsSpan()[(int)offset..]);
            }
        }
        /// <summary>
        /// 更新常量缓冲
        /// </summary>
        /// <param name="data"></param>
        /// <param name="size"></param>
        /// <param name="offset"></param>
        public unsafe void Update(byte* data, uint size, uint offset)
        {
            if (data == null) return;
            ulong length = (ulong)size + offset;
            if (length > Range)
            {
                byte[] buffer = UpdateBuffer(length);
                fixed(byte* b = buffer)
                {
                    byte* t = b + offset;
                    System.Buffer.MemoryCopy(data, t, size, size);
                }
            }
            else
            {
                byte[] buffer = Buffer;
                fixed (byte* b = buffer)
                {
                    byte* t = b + offset;
                    System.Buffer.MemoryCopy(data, t, size, size);
                }
            }
        }
        /// <summary>
        /// 销毁常量缓冲
        /// </summary>
        public void Dispose()
        {
            if (!IsDispose)
            {
                IsDispose = true;
                if (Buffer != null)
                {
                    var b = Buffer;
                    ArrayPool<byte>.Shared.Return(b);
                    Buffer = null;
                }
            }
        }
        /// <summary>
        /// 拷贝至新的渲染对象
        /// </summary>
        /// <param name="newObj"></param>
        /// <exception cref="NotImplementedException"></exception>
        public void CloneToNewRender(IRenderObject newObj)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderObject"></param>
        public void OnAttach(IRenderObject renderObject) { }
        /// <summary>
        /// 摧毁渲染组件
        /// </summary>
        public void Destroy()
        {
            Dispose();
        }

        /// <summary>
        /// 更新缓冲
        /// </summary>
        /// <param name="newSize"></param>
        /// <returns></returns>
        private byte[] UpdateBuffer(ulong newSize)
        {
            byte[] newBuffer = new byte[newSize];
            Buffer = newBuffer;
            Range = (ulong)newBuffer.LongLength;
            return newBuffer;
        }
    }
}
