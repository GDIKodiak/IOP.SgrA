﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 着色器类型
    /// </summary>
    public enum ShaderTypes
    {
        /// <summary>
        /// 
        /// </summary>
        None,
        /// <summary>
        /// 片元着色器
        /// </summary>
        FragmentShader,
        /// <summary>
        /// 顶点着色器
        /// </summary>
        VertexShader,
        /// <summary>
        /// 几何着色器
        /// </summary>
        GeometryShader,
        /// <summary>
        /// 曲面细分求值着色器
        /// </summary>
        TessEvaluationShader,
        /// <summary>
        /// 曲面细分控制着色器
        /// </summary>
        TessControlShader,
        /// <summary>
        /// 计算着色器
        /// </summary>
        ComputeShader
    }
}
