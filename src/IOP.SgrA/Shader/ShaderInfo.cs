﻿using System;
using System.Threading.Tasks;

namespace IOP.SgrA
{
    /// <summary>
    /// 着色器信息
    /// </summary>
    public abstract class ShaderInfo :IDisposable
    {
        /// <summary>
        /// 着色器类型
        /// </summary>
        public ShaderTypes ShaderType { get; }
        /// <summary>
        /// 着色器ID
        /// </summary>
        public int ShaderId { get; protected set; } = 0;
        /// <summary>
        /// 着色器名
        /// </summary>
        public string ShaderName { get; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="type">着色器类型</param>
        /// <param name="name"></param>
        public ShaderInfo(ShaderTypes type, string name)
        {
            ShaderType = type;
            ShaderName = name;
        }

        /// <summary>
        /// 加载着色器
        /// </summary>
        public abstract Task LoadShader();
        /// <summary>
        /// 编译着色器
        /// </summary>
        public abstract void CompileShader();
        /// <summary>
        /// 销毁资源
        /// </summary>
        public abstract void Dispose();
    }
}
