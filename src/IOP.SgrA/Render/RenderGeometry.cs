﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA
{
    /// <summary>
    /// 渲染几何集成数据
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 64)]
    public struct RenderGeometry
    {
        /// <summary>
        /// 坐标
        /// </summary>
        [FieldOffset(0)]
        public Vector3 Position;
        /// <summary>
        /// 旋转
        /// </summary>
        [FieldOffset(12)]
        public Quaternion Roation;
        /// <summary>
        /// 缩放
        /// </summary>
        [FieldOffset(28)]
        public Vector3 Scaling;
        /// <summary>
        /// 几何中心
        /// </summary>
        [FieldOffset(40)]
        public Vector3 Center;
        /// <summary>
        /// 几何范围
        /// </summary>
        [FieldOffset(52)]
        public Vector3 Extent;
    }
}
