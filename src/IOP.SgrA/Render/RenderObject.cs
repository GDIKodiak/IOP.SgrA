﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Linq;
using System.Numerics;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 渲染对象
    /// </summary>
    public partial class RenderObject : IRenderObject
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; protected internal set; }
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; protected internal set; }
        /// <summary>
        /// 当前渲染对象LOD级别
        /// </summary>
        public int LODLevel { get; set; } = 0;

        /// <summary>
        /// 组件集合
        /// </summary>
        protected IDictionary<string, IRenderComponent> Components { get; set; } = new Dictionary<string, IRenderComponent>();
        /// <summary>
        /// 子渲染对象
        /// </summary>
        protected IList<IRenderObject> Children { get; set; } = new List<IRenderObject>();
        /// <summary>
        /// 父渲染对象
        /// </summary>
        protected IRenderObject Parent { get; set; } = null;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="name"></param>
        /// <param name="id"></param>
        public RenderObject(string name, int id)
        {
            Name = name;
            Id = id;
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        public RenderObject() { }

        /// <summary>
        /// 添加组件
        /// </summary>
        /// <param name="component"></param>
        public virtual void AddComponent(IRenderComponent component)
        {
            if (string.IsNullOrEmpty(component.Name)) throw new ArgumentException("Target component's name is null");
            if (Components.ContainsKey(component.Name)) throw new InvalidOperationException($"component with name {component.Name} was already existence in this renderObject");
            if(component is IRenderGroup)
            {
                if (Components.ContainsKey("RenderGroup")) throw new InvalidOperationException("RenderObject only have unique render group");
                else
                {
                    Components.Add("RenderGroup", component);
                    component.OnAttach(this);
                }
            }
            else
            {
                Components.Add(component.Name, component);
                component.OnAttach(this);
            }
        }
        /// <summary>
        /// 添加或者更新组件
        /// </summary>
        /// <param name="component"></param>
        /// <param name="func"></param>
        public void AddOrUpdateComponent(IRenderComponent component, Func<string, IRenderComponent, IRenderComponent> func)
        {
            if (component == null || string.IsNullOrEmpty(component.Name)) return;
            string name = component.Name;
            if (component is IRenderGroup) name = "RenderGroup";
            if (Components.TryGetValue(name, out IRenderComponent old))
            {
                if (func == null)
                {
                    Components[name] = component;
                    component.OnAttach(this);
                }
                else
                {
                    var newComponent = func(name, old);
                    Components[name] = newComponent;
                    newComponent.OnAttach(this);
                }
            }
            else AddComponent(component);
        }
        /// <summary>
        /// 添加多个组件
        /// </summary>
        /// <param name="component"></param>
        public virtual void AddComponents(params IRenderComponent[] component)
        {
            if (component == null || component.Length <= 0) return;
            foreach(var item in component)
            {
                AddComponent(item);
            }
        }
        /// <summary>
        /// 获取组件
        /// </summary>
        /// <typeparam name="TComponent"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public virtual TComponent GetComponent<TComponent>(string name)
            where TComponent : IRenderComponent
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));
            if (Components.TryGetValue(name, out IRenderComponent component))
            {
                if (component is TComponent tc) return tc;
                else throw new InvalidCastException($"Cannot cast component to {nameof(TComponent)} with name {name}");
            }
            else throw new KeyNotFoundException($"Cannot found component with name {name}");
        }
        /// <summary>
        /// 获取多个组件
        /// </summary>
        /// <typeparam name="TComponent"></typeparam>
        /// <returns></returns>
        public virtual IEnumerable<TComponent> GetComponents<TComponent>()
            where TComponent : IRenderComponent
        {
            foreach(var item in Components)
            {
                if (item.Value is TComponent tc)
                    yield return tc;
            }
        }
        /// <summary>
        /// 更新
        /// </summary>
        public virtual void Update() { }
        /// <summary>
        /// 尝试获取组件
        /// </summary>
        /// <typeparam name="TComponent"></typeparam>
        /// <param name="name"></param>
        /// <param name="component"></param>
        /// <returns></returns>
        public virtual bool TryGetComponent<TComponent>(string name, out TComponent component) where TComponent : IRenderComponent
        {
            component = default;
            if (string.IsNullOrEmpty(name)) return false;
            if (Components.TryGetValue(name, out IRenderComponent c))
            {
                if (c is TComponent tc)
                {
                    component = tc;
                    return true;
                }
                else return false;
            }
            else return false;
        }
        /// <summary>
        /// 尝试获取渲染组
        /// </summary>
        /// <param name="renderGroup"></param>
        /// <returns></returns>
        public virtual bool TryGetRenderGroup(out IRenderGroup renderGroup) => TryGetComponent("RenderGroup", out renderGroup);
        /// <summary>
        /// 移除组件
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public virtual bool RemoveComponent(string name) => Components.Remove(name);
        /// <summary>
        /// 是否包含某个组件
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public virtual bool IsContainsComponent(string name) => !string.IsNullOrEmpty(name) && Components.ContainsKey(name);
        /// <summary>
        /// 获取变换
        /// </summary>
        /// <returns></returns>
        public Transform GetTransform()
        {
            var trans = GetComponent<Transform>(nameof(Transform));
            if (trans == null)
            {
                trans = new Transform(Id);
                AddComponent(trans);
            }
            return trans;
        }
        /// <summary>
        /// 获取对象轴对齐包围盒
        /// </summary>
        /// <returns></returns>
        public virtual Bounds GetBounds()
        {
            Transform transform = GetTransform();
            if (Components.TryGetValue(Bounds.BOXTYPE, out IRenderComponent c))
            {
                if (c is not Bounds box)
                {
                    box = new(Id, transform.Position, Vector3.One);
                    AddOrUpdateComponent(box, (key, value) => box);
                    return box;
                }
                else return box;
            }
            else
            {
                Bounds box = new(Id, transform.Position, Vector3.One);
                AddOrUpdateComponent(box, (key, value) => box);
                return box;
            }
        }
        /// <summary>
        /// 设置渲染对象包围盒
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        public virtual void SetBounds(Vector3 min, Vector3 max)
        {
            Bounds box = new(Id, min.X, min.Y, min.Z, max.X, max.Y, max.Z);
            AddOrUpdateComponent(box, (key, value) => box);
        }

        /// <summary>
        /// 获取父渲染对象
        /// </summary>
        /// <typeparam name="TRenderObject"></typeparam>
        /// <returns></returns>
        public virtual TRenderObject GetParent<TRenderObject>()
            where TRenderObject : class, IRenderObject
        {
            if (Parent == null) return default;
            else return Parent as TRenderObject;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IRenderObject GetParent()
        {
            if (Parent == null) return default;
            return Parent;
        }
        /// <summary>
        /// 添加子渲染对象
        /// </summary>
        /// <param name="child"></param>
        public virtual void AddChildren<TRenderObject>(TRenderObject child)
            where TRenderObject : class, IRenderObject
        {
            AddChildren(child as IRenderObject);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="child"></param>
        public virtual void AddChildren(IRenderObject child)
        {
            if (child == null) return;
            if(child is RenderObject obj) obj.Parent = this;
            Children.Add(child);
        }
        /// <summary>
        /// 遍历所有子渲染对象
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<IRenderObject> GetChildrens()
        {
            foreach(var item in Children)
            {
                yield return item;
            }
        }
        /// <summary>
        /// 移除子渲染对象
        /// </summary>
        /// <param name="child"></param>
        /// <returns></returns>
        public virtual bool RemoveChildren(IRenderObject child)
        {
            if (child == null) return false;
            if (child is RenderObject obj) obj.Parent = null;
            return Children.Remove(child);
        }
        /// <summary>
        /// 深度拷贝
        /// </summary>
        /// <returns></returns>
        public virtual IRenderObject DeepCopy(IGraphicsManager graphicsManager, string newName = null)
        {
            newName = string.IsNullOrEmpty(newName) ? $"{Name}-{Constant.DEEPCOPY}-{graphicsManager.GetNewId()}" : newName;
            var obj = graphicsManager.CreateRenderObject<RenderObject>(newName);
            List<IAnimation> animations = new List<IAnimation>();
            foreach(var item in Components)
            {
                IRenderComponent component = item.Value;
                if (component is IAnimation animation) animations.Add(animation);
                else
                {
                    component.CloneToNewRender(obj);
                }
            }
            foreach(var a in animations)
            {
                a.CloneToNewRender(obj);
            }
            foreach(var c in Children)
            {
                var dc = c.DeepCopy(graphicsManager);
                obj.AddChildren(dc);
            }
            return obj;
        }
        /// <summary>
        /// 摧毁渲染对象
        /// </summary>
        public virtual void Destroy(IGraphicsManager graphicsManager)
        {
            foreach(var item in Children)
            {
                item?.Destroy(graphicsManager);
            }
            Parent = null;
            foreach(var item in Components)
            {
                item.Value.Destroy();
            }
            Components.Clear();
            graphicsManager?.RemoveRenderObject(Name);
        }
    }
}
