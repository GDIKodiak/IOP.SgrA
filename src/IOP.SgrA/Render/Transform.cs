﻿using System.Numerics;

namespace IOP.SgrA
{
    /// <summary>
    /// 变换
    /// </summary>
    public class Transform : IRenderComponent
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name => nameof(Transform);
        /// <summary>
        /// 集成数据索引下标
        /// </summary>
        public int Index { get; protected internal set; }
        /// <summary>
        /// 坐标
        /// </summary>
        public Vector3 Position 
        {
            get => RenderObject.GetGeometryMetadata(Index).Position;
            set
            {
                ref RenderGeometry local = ref RenderObject.GetGeometryMetadata(Index);
                local.Position = value;
            }
        }
        /// <summary>
        /// 旋转
        /// </summary>
        public Quaternion Rotation 
        { 
            get => RenderObject.GetGeometryMetadata(Index).Roation;
            set
            {
                ref RenderGeometry local = ref RenderObject.GetGeometryMetadata(Index);
                local.Roation = value;
            }
        }
        /// <summary>
        /// 缩放
        /// </summary>
        public Vector3 Scaling 
        { 
            get => RenderObject.GetGeometryMetadata(Index).Scaling;
            set
            {
                ref RenderGeometry local = ref RenderObject.GetGeometryMetadata(Index);
                local.Scaling = value;
            }
        }
        /// <summary>
        /// 创建变换矩阵
        /// </summary>
        /// <returns></returns>
        public Matrix4x4 CreateMatrix()
        {
            return Matrix4x4.CreateScale(Scaling) * 
                Matrix4x4.CreateFromQuaternion(Rotation) * 
                Matrix4x4.CreateTranslation(Position);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="p"></param>
        /// <param name="r"></param>
        /// <param name="s"></param>
        public Transform(int index, Vector3 p, Quaternion r, Vector3 s)
        {
            Index = index;
            Position = p;
            Rotation = r;
            Scaling = s;
        }
        /// <summary>
        /// 
        /// </summary>
        public Transform(int index) 
        {
            Index = index;
            Position = Vector3.Zero;
            Rotation = Quaternion.Identity;
            Scaling = Vector3.One;
        }

        /// <summary>
        /// 拷贝至新的渲染对象
        /// </summary>
        /// <param name="newObj"></param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CloneToNewRender(IRenderObject newObj)
        {
            var t = newObj.GetTransform();
            t.Position = Position;
            t.Rotation = Rotation;
            t.Scaling = Scaling;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderObject"></param>
        public void OnAttach(IRenderObject renderObject) { }
        /// <summary>
        /// 摧毁渲染组件
        /// </summary>
        public void Destroy()
        {
        }
    }
}
