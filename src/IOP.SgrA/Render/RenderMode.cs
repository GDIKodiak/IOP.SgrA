﻿namespace IOP.SgrA
{
    /// <summary>
    /// 网格渲染模式
    /// </summary>
    public enum MeshRenderMode
    {
        /// <summary>
        /// 基于索引的渲染方式
        /// </summary>
        DrawElement,
        /// <summary>
        /// 基于数组的渲染方式
        /// </summary>
        DrawArray
    }
}
