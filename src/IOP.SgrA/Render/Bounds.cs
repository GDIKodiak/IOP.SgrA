﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 轴向对其包围盒
    /// </summary>
    public class Bounds : IRenderComponent, IBoundingBox
    {
        /// <summary>
        /// 
        /// </summary>
        public const string BOXTYPE = "AABB";
        /// <summary>
        /// 集成数据下标（渲染对象ID）
        /// </summary>
        public int Index { get; protected internal set; }
        /// <summary>
        /// 
        /// </summary>
        public string Name => BOXTYPE;
        /// <summary>
        /// 
        /// </summary>
        public float MinX { get => Center.X - Extents.X; }
        /// <summary>
        /// 
        /// </summary>
        public float MinY { get => Center.Y - Extents.Y; }
        /// <summary>
        /// 
        /// </summary>
        public float MinZ { get => Center.Z - Extents.Z; }
        /// <summary>
        /// 
        /// </summary>
        public float MaxX { get => Center.X + Extents.X; }
        /// <summary>
        /// 
        /// </summary>
        public float MaxY { get => Center.Y + Extents.Y; }
        /// <summary>
        /// 
        /// </summary>
        public float MaxZ { get => Center.Z + Extents.Z; }
        /// <summary>
        /// 中心坐标
        /// </summary>
        public Vector3 Center 
        {
            get => RenderObject.GetGeometryMetadata(Index).Center;
            private set
            {
                ref var local = ref RenderObject.GetGeometryMetadata(Index);
                local.Center = value;
            }
        }
        /// <summary>
        /// 范围
        /// </summary>
        public Vector3 Extents 
        {
            get => RenderObject.GetGeometryMetadata(Index).Extent;
            private set
            {
                ref var local = ref RenderObject.GetGeometryMetadata(Index);
                local.Extent = value;
            } 
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="center"></param>
        /// <param name="extents"></param>
        public Bounds(int index, Vector3 center, Vector3 extents)
        {
            Index = index;
            Center = center;
            Extents = extents;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="minX"></param>
        /// <param name="minY"></param>
        /// <param name="minZ"></param>
        /// <param name="maxX"></param>
        /// <param name="maxY"></param>
        /// <param name="maxZ"></param>
        public Bounds(int index, float minX, float minY, float minZ,
            float maxX, float maxY, float maxZ)
        {
            Index = index;
            Vector3 pMain = new Vector3(minX, minY, minZ);
            Vector3 pMax = new Vector3(maxX, maxY, maxZ);
            Vector3 center = (pMain + pMax) * 0.5f;
            Extents = pMax - center;
            Center = center;
        }
        /// <summary>
        /// 转换为AABB
        /// </summary>
        /// <returns></returns>
        public Bounds ToAABB() => this;
        /// <summary>
        /// 
        /// </summary>
        public void Destroy() { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newObj"></param>
        public void CloneToNewRender(IRenderObject newObj)
        {
            var box = newObj.GetBounds();
            box.Center = Center;
            box.Extents = Extents;
            newObj.AddOrUpdateComponent(box, (key, value) => box);
        }
        /// <summary>
        /// 某特定区域是否完全在此包围盒内
        /// </summary>
        /// <param name="center"></param>
        /// <param name="extents"></param>
        /// <returns></returns>
        public bool FullContains(Vector3 center, Vector3 extents)
        {
            Vector3 pMin = center - extents;
            Vector3 pMax = center + extents;
            if (pMin.X < MinX || pMin.Y < MinY || pMin.Z < MinZ ||
                pMax.X > MaxX || pMax.Y > MaxY || pMax.Z > MaxZ)
                return false;
            else return true;
        }
        /// <summary>
        /// 此包围盒是否完全存在于某一区域内
        /// </summary>
        /// <param name="center"></param>
        /// <param name="extents"></param>
        /// <returns></returns>
        public bool FullContainsIn(Vector3 center, Vector3 extents)
        {
            Vector3 pMin = center - extents;
            Vector3 pMax = center + extents;
            return MinX >= pMin.X && MinY >= pMin.Y && MinZ >= pMin.Z &&
                MaxX <= pMax.X && MaxY <= pMax.Y && MaxZ <= pMax.Z;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderObject"></param>
        public void OnAttach(IRenderObject renderObject)
        {
        }
        /// <summary>
        /// 获取经过模型矩阵变换的轴对齐包围盒
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns></returns>
        public AABB Transform(Matrix4x4 matrix)
        {
            float w = matrix.M44;
            Vector3 xa = new Vector3(matrix.M11, matrix.M12, matrix.M13) * MinX;
            Vector3 xb = new Vector3(matrix.M11, matrix.M12, matrix.M13) * MaxX;

            Vector3 ya = new Vector3(matrix.M21, matrix.M22, matrix.M23) * MinY;
            Vector3 yb = new Vector3(matrix.M21, matrix.M22, matrix.M23) * MaxY;

            Vector3 za = new Vector3(matrix.M31, matrix.M32, matrix.M33) * MinZ;
            Vector3 zb = new Vector3(matrix.M31, matrix.M32, matrix.M33) * MaxZ;

            Vector3 pmin = Vector3.Min(xa, xb) +
                Vector3.Min(ya, yb) +
                Vector3.Min(za, zb) +
                new Vector3(matrix.M41, matrix.M42, matrix.M43) / w;
            Vector3 pmax = Vector3.Max(xa, xb) +
                (Vector3.Max(ya, yb) +
                Vector3.Max(za, zb) +
                new Vector3(matrix.M41, matrix.M42, matrix.M43) / w);

            Vector3 center = (pmin + pmax) * 0.5f;
            Vector3 extens = pmax - center;
            return new AABB(center, extens);
        }
    }
}
