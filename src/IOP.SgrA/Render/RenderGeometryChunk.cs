﻿using IOP.SgrA.ECS;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA
{
    /// <summary>
    /// 渲染对象几何数据块
    /// </summary>
    public class RenderGeometryChunk
    {
        /// <summary>
        /// 块大小
        /// </summary>
        public const int CHUNKSIZE = 16 * 1024;
        /// <summary>
        /// 块持有的实体上限
        /// </summary>
        public static int Length => CHUNKSIZE / StructSize;
        /// <summary>
        /// 几何结构体大小
        /// </summary>
        public static int StructSize => Marshal.SizeOf<RenderGeometry>();
        /// <summary>
        /// 块下标
        /// </summary>
        public int ChunkIndex { get; internal set; }
        /// <summary>
        /// 内存
        /// </summary>
        public Memory<byte> Memory { get; private set; }
        /// <summary>
        /// 下一段
        /// </summary>
        public RenderGeometryChunk Next { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        internal IMemoryOwner<byte> _Owner = null;

        /// <summary>
        /// 
        /// </summary>
        public RenderGeometryChunk()
        {
            SetBuffer();
        }

        /// <summary>
        /// 下一段内存
        /// </summary>
        /// <param name="chunk"></param>
        public void SetNext(RenderGeometryChunk chunk)
        {
            Next = chunk;
            chunk.ChunkIndex = ChunkIndex + 1;
            chunk = this;
            while (chunk.Next != null)
            {
                chunk = chunk.Next;
            }
        }
        /// <summary>
        /// 释放内存
        /// </summary>
        public void Free()
        {
            var local = this;
            local.ChunkIndex = 0;
            Memory = default;
            _Owner.Dispose();
            while (local.Next != null)
            {
                local = local.Next;
                local.Free();
                local.Next = null;
            }
        }
        /// <summary>
        /// 获取几何数据
        /// </summary>
        /// <param name="localIndex"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public ref RenderGeometry GetGeometry(int localIndex)
        {
            if (localIndex >= Length) throw new ArgumentOutOfRangeException(nameof(localIndex));
            Span<byte> t = Memory.Span.Slice(localIndex * StructSize, StructSize);
            ref RenderGeometry rg = ref MemoryMarshal.AsRef<RenderGeometry>(t);
            return ref rg;
        }

        /// <summary>
        /// 
        /// </summary>
        internal void SetBuffer()
        {
            _Owner = MemoryPool<byte>.Shared.Rent(CHUNKSIZE);
            Memory = _Owner.Memory;
        }
    }
}
