﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 
    /// </summary>
    public enum GroupType
    {
        /// <summary>
        /// 主要
        /// </summary>
        Primary,
        /// <summary>
        /// 次要
        /// </summary>
        Secondary
    }
}
