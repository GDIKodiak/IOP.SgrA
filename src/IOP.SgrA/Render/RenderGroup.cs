﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Buffers;
using System.Linq;
using System.Diagnostics.Metrics;
using System.Threading;

namespace IOP.SgrA
{
    /// <summary>
    /// 渲染对象组
    /// </summary>
    public abstract class RenderGroup<TContext, TPipeline, TGroup> : IRenderGroup
        where TContext : Context
    {
        /// <summary>
        /// 裁剪区域
        /// </summary>
        public virtual Area Scissor { get => Camera != null ? Camera.Scissor : default; }
        /// <summary>
        /// 渲染窗口
        /// </summary>
        public virtual ViewportRect Viewport { get => Camera != null ? Camera.Viewport : default; }
        /// <summary>
        /// 渲染组类型
        /// </summary>
        public abstract GroupType GroupType { get; }
        /// <summary>
        /// 组渲染模式
        /// </summary>
        public virtual GroupRenderMode RenderMode { get; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; }
        /// <summary>
        /// 优先级
        /// </summary>
        public int Priority 
        {
            get => _Priority;
            set
            {
                var min = (int)RenderMode;
                var max = (int)RenderMode + 1000;
                int r = Math.Clamp(value, min, max);
                _Priority = r;
            }
        }
        /// <summary>
        /// 子项渲染管线构建者
        /// </summary>
        public IProductionLineBuilder<TContext> ItemProductionLineBuilder { get; protected set; }
        /// <summary>
        /// 组渲染管线构建者
        /// </summary>
        public IProductionLineBuilder<TGroup> GroupProductionLineBuilder { get; protected set; }
        /// <summary>
        /// 子项渲染管线
        /// </summary>
        public RenderingProductionLineDelegate<TContext> ItemProductionLine { get; protected set; } = (context) => { };
        /// <summary>
        /// 组渲染管线
        /// </summary>
        public RenderingProductionLineDelegate<TGroup> GroupProductionLine { get; protected set; } = (group) => { };
        /// <summary>
        /// 初始化动作
        /// </summary>
        protected Action<TGroup> InitializationAction { get; set; } = (group) => { };
        /// <summary>
        /// 摄影机
        /// </summary>
        public Camera Camera { get; set; } = new Camera();
        /// <summary>
        /// 着色器
        /// </summary>
        public TPipeline Pipeline { get; protected set; }
        /// <summary>
        /// 当前帧缓冲
        /// </summary>
        public uint CurrentFrame { get; protected set; } = 0;
        /// <summary>
        /// 服务提供者
        /// </summary>
        public readonly IServiceProvider ServiceProvider;
        /// <summary>
        /// 日志
        /// </summary>
        public readonly ILogger<TGroup> Logger;
        /// <summary>
        /// 自定义属性
        /// </summary>
        public readonly Dictionary<string, ValueType> CustomProperies = new Dictionary<string, ValueType>();
        /// <summary>
        /// 静态的渲染对象
        /// </summary>
        protected readonly ConcurrentBag<IRenderObject> StaticRenderObjects = new ConcurrentBag<IRenderObject>();
        /// <summary>
        /// 渲染上下文集合
        /// </summary>
        protected readonly ConcurrentQueue<Context> Contexts = new ConcurrentQueue<Context>();
        /// <summary>
        /// 子渲染组列表
        /// </summary>
        protected readonly ConcurrentBag<TGroup> Childrens = new ConcurrentBag<TGroup>();
        /// <summary>
        /// 父渲染组
        /// </summary>
        protected TGroup Parent { get; set; } = default;
        /// <summary>
        /// 是否停止
        /// </summary>
        protected bool IsStop = false;
        /// <summary>
        /// 
        /// </summary>
        protected int _Priority = 0;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="name"></param>
        /// <param name="serviceProvider"></param>
        /// <param name="shader"></param>
        /// <param name="mode"></param>
        public RenderGroup(string name, IServiceProvider serviceProvider, TPipeline shader, GroupRenderMode mode)
        {
            if (serviceProvider == null) throw new ArgumentNullException(nameof(serviceProvider));
            if (string.IsNullOrEmpty(name)) name = Guid.NewGuid().ToString("N");
            Name = name;
            ServiceProvider = serviceProvider;
            GroupProductionLineBuilder = new ProductionLineBuilder<TGroup>($"{name}_group", serviceProvider);
            ItemProductionLineBuilder = new ProductionLineBuilder<TContext>(name, serviceProvider);
            if(shader != null) Pipeline = shader;
            var f = ServiceProvider.GetRequiredService<ILoggerFactory>();
            Logger = f.CreateLogger<TGroup>();
            RenderMode = mode;
            Priority = (int)mode;
        }
        /// <summary>
        /// 设置摄影机
        /// </summary>
        /// <param name="camera"></param>
        public virtual void SetCamera(Camera camera) => Camera = camera;
        /// <summary>
        /// 此渲染组是否可用
        /// </summary>
        /// <returns></returns>
        public virtual bool IsEnable() => IsStop;
        /// <summary>
        /// 手动停用渲染组
        /// </summary>
        /// <returns></returns>
        public virtual void Disable() => IsStop = true;
        /// <summary>
        /// 手动启动渲染组
        /// </summary>
        public virtual void Enable() => IsStop = false;
        /// <summary>
        /// 组渲染
        /// </summary>
        public abstract void GroupRendering();
        /// <summary>
        /// 组渲染
        /// </summary>
        /// <param name="frameIndex"></param>
        public abstract void GroupRendering(uint frameIndex);
        /// <summary>
        /// 初始化
        /// </summary>
        public abstract void Initialization();
        /// <summary>
        /// 创建初始化动作
        /// </summary>
        /// <param name="action"></param>
        public virtual void CreateInitializationAction(Action<TGroup> action)
        {
            if (action == null) throw new ArgumentNullException(nameof(action));
            InitializationAction = action;
        }
        /// <summary>
        /// 设置视口
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="minDepth"></param>
        /// <param name="maxDepth"></param>
        public abstract void SetViewPort(float x, float y, float width, float height, float minDepth, float maxDepth);
        /// <summary>
        /// 创建组渲染动作
        /// </summary>
        /// <param name="groupProdBuilder"></param>
        public virtual void CreateGroupRenderingAction(Action<IProductionLineBuilder<TGroup>> groupProdBuilder)
        {
            GroupProductionLineBuilder.Clear();
            groupProdBuilder?.Invoke(GroupProductionLineBuilder);
            GroupProductionLine = GroupProductionLineBuilder.Build();
        }
        /// <summary>
        /// 创建子项渲染动作
        /// </summary>
        /// <param name="itemProdBuilder"></param>
        public virtual void CreateItemRenderingAction(Action<IProductionLineBuilder<TContext>> itemProdBuilder)
        {
            ItemProductionLineBuilder.Clear();
            itemProdBuilder?.Invoke(ItemProductionLineBuilder);
            ItemProductionLine = ItemProductionLineBuilder.Build();
        }
        /// <summary>
        /// 推送数据至纹理
        /// </summary>
        /// <typeparam name="TStruct"></typeparam>
        /// <param name="binding"></param>
        /// <param name="data"></param>
        /// <param name="setIndex"></param>
        public abstract void PushDataToTexture<TStruct>(TStruct data, uint setIndex, uint binding) where TStruct : struct;
        /// <summary>
        /// 推送数据至纹理
        /// </summary>
        /// <param name="data"></param>
        /// <param name="setIndex"></param>
        /// <param name="binding"></param>
        public abstract void PushDataToTexture(Span<byte> data, uint setIndex, uint binding);
        /// <summary>
        /// 推送上下文
        /// </summary>
        /// <param name="context"></param>
        public abstract void PushContext(Context context);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderObject"></param>
        public abstract void PushContext(IRenderObject renderObject);
        /// <summary>
        /// 绑定上下文
        /// </summary>
        /// <param name="contexts"></param>
        public abstract void BindingContext(Context[] contexts);
        /// <summary>
        /// 绑定上下文
        /// </summary>
        /// <param name="contexts"></param>
        /// <param name="renderObject"></param>
        /// <param name="deepCopy"></param>
        public abstract void BindingContext(Context[] contexts, IRenderObject renderObject, bool deepCopy = false);
        /// <summary>
        /// 解除上下文绑定
        /// </summary>
        /// <param name="context"></param>
        public abstract void UnbindContext(Context context);
        /// <summary>
        /// 绑定静态渲染对象
        /// </summary>
        /// <param name="renderObject"></param>
        public abstract void BindingStaticRenderObject(IRenderObject renderObject);

        /// <summary>
        /// 添加子渲染组
        /// </summary>
        /// <param name="child"></param>
        public virtual void AddChildren(TGroup child)
        {
            if (Childrens.Contains(child)) return;
            Childrens.Add(child);
        }

        /// <summary>
        /// 获取上下文
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<TContext> GetContexts()
        {
            while(Contexts.TryDequeue(out Context context))
            {
                yield return context as TContext;
            }
        }
        /// <summary>
        /// 获取静态对象
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<IRenderObject> GetStaticObjects()
        {
            foreach(var item in StaticRenderObjects)
            {
                yield return item;
            }
        }
        /// <summary>
        /// 清理上下文
        /// </summary>
        public virtual void ClearContexts() => Contexts.Clear();
        /// <summary>
        /// 获取所有子渲染组
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<TGroup> GetChildrens()
        {
            foreach (var item in Childrens) yield return item;
        }
        /// <summary>
        /// 获取自定义属性
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public virtual T GetCustomPropery<T>(string name)
            where T : struct
        {
            if (CustomProperies.TryGetValue(name, out ValueType value))
            {
                return (T)value;
            }
            else
            {
                CustomProperies.Add(name, default(T));
                return default;
            }
        }
        /// <summary>
        /// 设置自定义属性
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <param name="data"></param>
        public virtual void SetCustomPropery<T>(string name, T data)
            where T : struct
        {
            if (CustomProperies.ContainsKey(name))
            {
                CustomProperies[name] = data;
            }
            else CustomProperies.TryAdd(name, data);
        }
        /// <summary>
        /// 销毁资源
        /// </summary>
        public abstract void Dispose();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderObject"></param>
        public abstract void OnAttach(IRenderObject renderObject);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newObj"></param>

        public virtual void CloneToNewRender(IRenderObject newObj) => newObj.AddComponent(this);
        /// <summary>
        /// 
        /// </summary>
        public virtual void Destroy() { }
    }
}
