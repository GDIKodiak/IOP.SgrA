﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA
{
    /// <summary>
    /// 全局渲染配置项
    /// </summary>
    public class GlobalRenderConfigs : INotifyPropertyChanged
    {
        private SampleCount _SampleCount = SampleCount.Sample1X;
        private Matrix4x4 _WorldMatrix = Matrix4x4.Identity;

        /// <summary>
        /// 多重采样次数
        /// </summary>
        public SampleCount Multisampling 
        {
            get => _SampleCount;
            set
            {
                if (_SampleCount != value)
                {
                    _SampleCount = value;
                    OnPropertyChanged();
                }
            }
        }
        /// <summary>
        /// 世界坐标系置换矩阵
        /// </summary>
        public Matrix4x4 WorldMatrix
        {
            get => _WorldMatrix;
            set
            {
                if(_WorldMatrix != value)
                {
                    _WorldMatrix = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        public void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    /// <summary>
    /// 采样系数
    /// </summary>
    public enum SampleCount
    {
        /// <summary>
        /// 
        /// </summary>
        Sample1X = 1,
        /// <summary>
        /// 
        /// </summary>
        Sample2X = 2,
        /// <summary>
        /// 
        /// </summary>
        Sample4X = 4,
        /// <summary>
        /// 
        /// </summary>
        Sample8X = 8,
        /// <summary>
        /// 
        /// </summary>
        Sample16X = 16,
        /// <summary>
        /// 
        /// </summary>
        Sample32X = 32,
        /// <summary>
        /// 
        /// </summary>
        Sample64X = 64
    }
}
