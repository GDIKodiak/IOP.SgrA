﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 本地自适应缓存数据
    /// </summary>
    public class LocalUniformBufferData
    {
        /// <summary>
        /// 数据
        /// </summary>
        public Memory<byte> Data { get; set; }
        /// <summary>
        /// 是否需要更新
        /// </summary>
        public bool NeedUpdate { get; set; }
    }
}
