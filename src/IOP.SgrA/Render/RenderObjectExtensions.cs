﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 渲染对象扩展
    /// </summary>
    public static class RenderObjectExtensions
    {
        /// <summary>
        /// 创建动画
        /// </summary>
        /// <typeparam name="TRenderObject"></typeparam>
        /// <typeparam name="TAnimation"></typeparam>
        /// <param name="object"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static TRenderObject CreateAnimation<TRenderObject, TAnimation>(this TRenderObject @object, string name)
            where TRenderObject : IRenderObject
            where TAnimation : Animation
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException("name with component cannot be null");
            TAnimation animation = Activator.CreateInstance<TAnimation>();
            animation.Name = name;
            @object.AddComponent(animation);
            animation.OnAttach(@object);
            return @object;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TRenderObject"></typeparam>
        /// <typeparam name="TParticle"></typeparam>
        /// <param name="object"></param>
        /// <param name="contextManager"></param>
        /// <param name="renderObject"></param>
        /// <param name="renderGroup"></param>
        /// <returns></returns>
        public static TRenderObject CreateParticleGenerator<TRenderObject, TParticle>(this TRenderObject @object, 
            IContextManager contextManager, IRenderObject renderObject, IRenderGroup renderGroup)
            where TRenderObject : IRenderObject
            where TParticle : ParticleGenerator
        {
            object particle = Activator.CreateInstance(typeof(TParticle), new object[] { contextManager, renderObject, renderGroup });
            TParticle p = particle as TParticle;
            @object.AddComponent(p);
            p.OnAttach(@object);
            return @object;
        }
    }
}
