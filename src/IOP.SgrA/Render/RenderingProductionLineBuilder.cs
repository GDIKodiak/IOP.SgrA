﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IOP.SgrA
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TContext"></typeparam>
    public class RenderingProductionLineBuilder<TContext> : IRenderingProductionLineBuilder<TContext>
    {
        /// <summary>
        /// 服务
        /// </summary>
        public IServiceProvider Services { get; private set; }

        private readonly ILogger<RenderingProductionLineBuilder<TContext>> Logger;

        /// <summary>
        /// 中间件列表
        /// </summary>
        private readonly IList<Func<RenderingProductionLineDelegate<TContext>, RenderingProductionLineDelegate<TContext>>> _Middlewares =
            new List<Func<RenderingProductionLineDelegate<TContext>, RenderingProductionLineDelegate<TContext>>>();

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="provider"></param>
        public RenderingProductionLineBuilder(IServiceProvider provider)
        {
            Services = provider ?? throw new ArgumentNullException(nameof(IServiceProvider));
            Logger = provider.GetRequiredService<ILogger<RenderingProductionLineBuilder<TContext>>>();
        }

        /// <summary>
        /// 构建
        /// </summary>
        /// <returns></returns>
        public RenderingProductionLineDelegate<TContext> Build()
        {
            RenderingProductionLineDelegate<TContext> productline = next =>
            {
            };
            foreach (var middleware in _Middlewares.Reverse())
            {
                productline = middleware(productline);
            }

            return productline;
        }

        /// <summary>
        /// 添加中间件至末尾
        /// </summary>
        /// <param name="endPoint"></param>
        /// <returns></returns>
        public IRenderingProductionLineBuilder<TContext> Run(RenderingProductionLineDelegate<TContext> endPoint)
        {
            endPoint = endPoint ?? throw new ArgumentNullException(nameof(endPoint));
            return Use(next => endPoint);
        }

        /// <summary>
        /// 添加中间件
        /// </summary>
        /// <param name="middleware"></param>
        /// <returns></returns>
        public IRenderingProductionLineBuilder<TContext> Use(Func<RenderingProductionLineDelegate<TContext>, RenderingProductionLineDelegate<TContext>> middleware)
        {
            _Middlewares.Add(middleware);
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TMiddleware"></typeparam>
        /// <returns></returns>
        public IRenderingProductionLineBuilder<TContext> UseMiddleware<TMiddleware>()
            where TMiddleware : class, IRenderingMiddleware<TContext>
        {
            try
            {
                var middleware = ActivatorUtilities.CreateInstance<TMiddleware>(Services);
                return Use(next =>
                {
                    return context =>
                    {
                        middleware.InvokeAsync(context, next);
                    };
                });
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message + "\r\n" + e.StackTrace);
                return this;
            }
        }
    }
}
