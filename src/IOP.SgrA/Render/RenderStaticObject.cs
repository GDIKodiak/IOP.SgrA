﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IOP.SgrA
{
    /// <summary>
    /// 
    /// </summary>
    public partial class RenderObject
    {
        /// <summary>
        /// 
        /// </summary>
        internal protected static int _ID;
        /// <summary>
        /// 块列表
        /// </summary>
        internal protected static readonly List<RenderGeometryChunk> GeometryChunks = new();
        /// <summary>
        /// 被回收的下标
        /// </summary>
        internal protected static readonly ConcurrentQueue<int> RecycledIndexes = new();
        /// <summary>
        /// 
        /// </summary>
        internal protected static object StaticSyncRoot = new();
        /// <summary>
        /// 
        /// </summary>
        static RenderObject()
        {
            RenderGeometryChunk first = new();
            GeometryChunks.Add(first);
        }

        /// <summary>
        /// 创建渲染对象
        /// </summary>
        /// <typeparam name="TRender"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public static TRender CreateRenderObject<TRender>(string name)
            where TRender : RenderObject, new()
        {
            int id = CreateId();
            if(string.IsNullOrEmpty(name)) name = id.ToString();
            CreateRenderMetadata(id);
            TRender render = new()
            {
                Name = name,
                Id = id
            };
            render.AddComponent(new Transform(render.Id));
            render.AddComponent(new Bounds(render.Id, Vector3.Zero, Vector3.One));
            return render;
        }
        /// <summary>
        /// 获取几何元数据
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public static ref RenderGeometry GetGeometryMetadata(int id)
        {
            var chunkId = id / RenderGeometryChunk.Length;
            if (chunkId >= GeometryChunks.Count || chunkId < 0) throw new ArgumentOutOfRangeException(nameof(id));
            return ref GeometryChunks[chunkId].GetGeometry(id % RenderGeometryChunk.Length);
        }

        /// <summary>
        /// 创建一个ID
        /// </summary>
        /// <returns></returns>
        internal protected static int CreateId()
        {
            int r;
            if (RecycledIndexes.TryDequeue(out int re))
            {
                r = re;
            }
            else r = Interlocked.Increment(ref _ID) & int.MaxValue;
            return r;
        }
        /// <summary>
        /// 创建渲染对象元数据
        /// </summary>
        internal protected static void CreateRenderMetadata(int id)
        {
            CreateGeometryMetadata(id);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        internal protected static void CreateGeometryMetadata(int id)
        {
            var geometryChunkIndex = id / RenderGeometryChunk.Length;
            if (geometryChunkIndex >= GeometryChunks.Count)
            {
                lock (StaticSyncRoot)
                {
                    var last = GeometryChunks[^1];
                    RenderGeometryChunk chunk = new();
                    last.SetNext(chunk);
                    GeometryChunks.Add(chunk);
                }
            }
        }
    }
}
