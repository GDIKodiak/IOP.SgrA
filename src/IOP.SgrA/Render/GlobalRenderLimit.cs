﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA
{
    /// <summary>
    /// 全局渲染器限制
    /// </summary>
    public class GlobalRenderLimit
    {
        /// <summary>
        /// 最大采样次数
        /// </summary>
        public SampleCount MaxCampleCount { get; set; }
    }
}
