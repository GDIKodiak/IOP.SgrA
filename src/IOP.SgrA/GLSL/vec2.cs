﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    /// <summary>
    /// 
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 8, Pack = 4)]
    public struct vec2
    {
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public float x;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(4)]
        public float y;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public float r;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(4)]
        public float g;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public vec2(float x, float y)
        {
            this.x = x;
            this.y = y;
            r = x;
            g = y;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public vec2(float v)
        {
            x = y = r = g = v;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public vec2(ivec2 v)
        {
            x = y = r = g = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public vec2(uvec2 v)
        {
            x = y = r = g = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public vec2(dvec2 v)
        {
            x = y = r = g = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public vec2(bvec2 v)
        {
            x = y = r = g = 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static vec2 operator ++(vec2 left) => new vec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static vec2 operator --(vec2 left) => new vec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec2 operator +(vec2 left, vec2 right) => new vec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec2 operator -(vec2 left, vec2 right) => new vec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec2 operator *(vec2 left, vec2 right) => new vec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec2 operator *(vec2 left, mat2x2 right) => new vec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec2 operator *(vec2 left, mat2 right) => new vec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec3 operator *(vec2 left, mat3x2 right) => new vec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec4 operator *(vec2 left, mat4x2 right) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec2 operator /(vec2 left, vec2 right) => new vec2();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec2 operator +(float left, vec2 right) => new vec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec2 operator -(vec2 left, float right) => new vec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec2 operator -(float left, vec2 right) => new vec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static vec2 operator -(vec2 left) => new vec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec2 operator *(vec2 left, float right) => new vec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec2 operator *(float left, vec2 right) => new vec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec2 operator /(vec2 left, float right) => new vec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec2 operator /(uint left, vec2 right) => new vec2();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec2 xx { get => new(); }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec2 xy { get => new(); }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec2 yx { get => new(); }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec2 yy { get => new(); }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec2 rr { get => new(); }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec2 rg { get => new(); }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec2 gr { get => new(); }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec2 gg { get => new(); }
    }
}
