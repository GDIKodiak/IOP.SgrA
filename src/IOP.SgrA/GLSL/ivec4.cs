﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    /// <summary>
    /// 
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 16, Pack = 4)]
    public struct ivec4
    {
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public int x;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(4)]
        public int y;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(8)]
        public int z;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(12)]
        public int w;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public int r;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(4)]
        public int g;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(8)]
        public int b;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(12)]
        public int a;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="w"></param>
        public ivec4(int x, int y, int z, int w)
        {
            this.x = x; this.y = y; this.z = z; this.w = w;
            r = x; g = y; b = z; a = w;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vec"></param>
        /// <param name="w"></param>
        public ivec4(ivec3 vec, int w)
        {
            this.x = vec.x; this.y = vec.y; this.z = vec.z;
            this.w = w;
            r = x; g = y; b = z; a = w;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public ivec4(int v)
        {
            x = y = z = w = r = g = b = a = v;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public ivec4(vec4 v)
        {
            x = y = z = w = r = g = b = a = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public ivec4(uvec4 v)
        {
            x = y = z = w = r = g = b = a = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public ivec4(dvec4 v)
        {
            x = y = z = w = r = g = b = a = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public ivec4(bvec4 v)
        {
            x = y = z = w = r = g = b = a = 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static ivec4 operator ++(ivec4 left) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static ivec4 operator --(ivec4 left) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec4 operator +(ivec4 left, ivec4 right) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec4 operator -(ivec4 left, ivec4 right) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec4 operator *(ivec4 left, ivec4 right) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec2 operator *(ivec4 left, mat2x4 right) => new vec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="rgith"></param>
        /// <returns></returns>
        public static vec3 operator *(ivec4 left, mat3x4 rgith) => new vec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec4 operator *(ivec4 left, mat4 right) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec4 operator *(ivec4 left, mat4x4 right) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec4 operator /(ivec4 left, ivec4 right) => new ivec4();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec4 operator +(int left, ivec4 right) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec4 operator -(ivec4 left, int right) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec4 operator -(int left, ivec4 right) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static ivec4 operator -(ivec4 left) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec4 operator *(ivec4 left, int right) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec4 operator *(int left, ivec4 right) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec4 operator /(ivec4 left, int right) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec4 operator /(int left, ivec4 right) => new ivec4();

        #region ivec2
        /// <summary>
        ///
        /// </summary>
        public ivec2 xx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 xy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 xz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 xw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 yx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 yy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 yz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 yw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 zx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 zy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 zz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 zw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 wx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 wy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 wz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 ww { get => new(); set { } }

        /// <summary>
        ///
        /// </summary>
        public ivec2 rr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 rg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 rb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 ra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 gr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 gg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 gb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 ga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 br { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 bg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 bb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 ba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 ar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 ag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 ab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 aa { get => new(); set { } }
        #endregion

        #region ivec3
        /// <summary>
        ///
        /// </summary>
        public ivec3 xxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 xxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 xxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 xxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 xyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 xyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 xyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 xyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 xzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 xzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 xzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 xzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 xwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 xwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 xwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 xww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 yxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 yxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 yxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 yxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 yyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 yyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 yyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 yyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 yzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 yzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 yzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 yzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 ywx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 ywy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 ywz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 yww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 zxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 zxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 zxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 zxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 zyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 zyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 zyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 zyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 zzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 zzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 zzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 zzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 zwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 zwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 zwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 zww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 wxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 wxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 wxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 wxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 wyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 wyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 wyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 wyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 wzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 wzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 wzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 wzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 wwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 wwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 wwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 www { get => new(); set { } }

        /// <summary>
        ///
        /// </summary>
        public ivec3 rrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 rrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 rrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 rra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 rgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 rgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 rgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 rga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 rbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 rbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 rbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 rba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 rar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 rag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 rab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 raa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 grr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 grg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 grb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 gra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 ggr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 ggg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 ggb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 gga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 gbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 gbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 gbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 gba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 gar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 gag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 gab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 gaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 brr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 brg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 brb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 bra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 bgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 bgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 bgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 bga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 bbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 bbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 bbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 bba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 bar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 bag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 bab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 baa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 arr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 arg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 arb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 ara { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 agr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 agg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 agb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 aga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 abr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 abg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 abb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 aba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 aar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 aag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 aab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 aaa { get => new(); set { } }
        #endregion

        #region ivec4
        /// <summary>
        ///
        /// </summary>
        public ivec4 xxxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xxxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xxxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xxxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xxyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xxyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xxyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xxyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xxzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xxzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xxzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xxzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xxwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xxwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xxwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xxww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xyxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xyxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xyxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xyxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xyyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xyyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xyyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xyyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xyzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xyzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xyzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xyzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xywx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xywy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xywz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xyww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xzxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xzxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xzxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xzxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xzyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xzyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xzyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xzyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xzzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xzzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xzzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xzzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xzwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xzwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xzwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xzww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xwxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xwxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xwxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xwxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xwyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xwyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xwyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xwyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xwzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xwzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xwzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xwzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xwwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xwwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xwwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 xwww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yxxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yxxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yxxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yxxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yxyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yxyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yxyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yxyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yxzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yxzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yxzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yxzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yxwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yxwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yxwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yxww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yyxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yyxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yyxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yyxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yyyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yyyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yyyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yyyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yyzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yyzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yyzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yyzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yywx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yywy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yywz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yyww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yzxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yzxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yzxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yzxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yzyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yzyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yzyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yzyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yzzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yzzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yzzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yzzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yzwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yzwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yzwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 yzww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ywxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ywxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ywxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ywxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ywyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ywyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ywyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ywyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ywzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ywzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ywzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ywzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ywwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ywwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ywwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ywww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zxxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zxxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zxxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zxxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zxyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zxyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zxyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zxyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zxzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zxzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zxzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zxzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zxwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zxwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zxwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zxww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zyxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zyxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zyxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zyxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zyyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zyyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zyyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zyyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zyzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zyzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zyzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zyzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zywx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zywy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zywz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zyww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zzxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zzxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zzxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zzxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zzyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zzyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zzyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zzyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zzzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zzzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zzzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zzzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zzwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zzwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zzwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zzww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zwxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zwxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zwxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zwxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zwyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zwyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zwyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zwyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zwzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zwzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zwzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zwzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zwwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zwwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zwwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 zwww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wxxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wxxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wxxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wxxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wxyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wxyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wxyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wxyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wxzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wxzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wxzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wxzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wxwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wxwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wxwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wxww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wyxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wyxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wyxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wyxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wyyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wyyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wyyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wyyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wyzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wyzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wyzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wyzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wywx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wywy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wywz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wyww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wzxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wzxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wzxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wzxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wzyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wzyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wzyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wzyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wzzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wzzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wzzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wzzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wzwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wzwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wzwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wzww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wwxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wwxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wwxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wwxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wwyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wwyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wwyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wwyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wwzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wwzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wwzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wwzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wwwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wwwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wwwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 wwww { get => new(); set { } }

        /// <summary>
        ///
        /// </summary>
        public ivec4 rrrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rrrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rrrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rrra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rrgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rrgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rrgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rrga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rrbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rrbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rrbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rrba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rrar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rrag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rrab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rraa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rgrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rgrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rgrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rgra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rggr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rggg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rggb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rgga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rgbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rgbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rgbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rgba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rgar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rgag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rgab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rgaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rbrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rbrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rbrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rbra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rbgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rbgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rbgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rbga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rbbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rbbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rbbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rbba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rbar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rbag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rbab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rbaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rarr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rarg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rarb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rara { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ragr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ragg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ragb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 raga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rabr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rabg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 rabb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 raba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 raar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 raag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 raab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 raaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 grrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 grrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 grrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 grra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 grgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 grgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 grgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 grga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 grbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 grbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 grbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 grba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 grar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 grag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 grab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 graa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ggrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ggrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ggrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ggra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gggr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gggg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gggb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ggga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ggbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ggbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ggbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ggba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ggar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ggag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ggab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 ggaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gbrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gbrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gbrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gbra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gbgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gbgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gbgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gbga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gbbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gbbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gbbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gbba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gbar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gbag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gbab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gbaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 garr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 garg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 garb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gara { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gagr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gagg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gagb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gaga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gabr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gabg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gabb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gaba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gaar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gaag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gaab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 gaaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 brrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 brrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 brrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 brra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 brgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 brgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 brgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 brga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 brbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 brbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 brbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 brba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 brar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 brag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 brab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 braa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bgrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bgrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bgrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bgra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bggr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bggg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bggb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bgga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bgbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bgbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bgbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bgba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bgar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bgag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bgab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bgaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bbrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bbrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bbrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bbra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bbgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bbgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bbgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bbga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bbbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bbbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bbbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bbba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bbar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bbag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bbab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bbaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 barr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 barg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 barb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bara { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bagr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bagg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 bagb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 baga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 babr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 babg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 babb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 baba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 baar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 baag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 baab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 baaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 arrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 arrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 arrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 arra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 argr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 argg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 argb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 arga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 arbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 arbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 arbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 arba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 arar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 arag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 arab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 araa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 agrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 agrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 agrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 agra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 aggr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 aggg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 aggb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 agga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 agbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 agbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 agbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 agba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 agar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 agag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 agab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 agaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 abrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 abrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 abrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 abra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 abgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 abgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 abgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 abga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 abbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 abbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 abbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 abba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 abar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 abag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 abab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 abaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 aarr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 aarg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 aarb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 aara { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 aagr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 aagg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 aagb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 aaga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 aabr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 aabg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 aabb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 aaba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 aaar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 aaag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 aaab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec4 aaaa { get => new(); set { } }
        #endregion
    }
}
