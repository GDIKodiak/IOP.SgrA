﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    public partial class GLSLShaderBase
    {
        #region determinant
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public float determinant(mat2 m) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public float determinant(mat3 m) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public float determinant(mat4 m) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public double determinant(dmat2 m) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public double determinant(dmat3 m) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public double determinant(dmat4 m) => 0;
        #endregion
        #region inverse
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public mat2 inverse(mat2 m) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public mat3 inverse(mat3 m) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public mat4 inverse(mat4 m) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public dmat2 inverse(dmat2 m) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public dmat3 inverse(dmat3 m) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public dmat4 inverse(dmat4 m) => new();
        #endregion
        #region matrixCompMult
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public mat2 matrixCompMult(mat2 x, mat2 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public mat3 matrixCompMult(mat3 x, mat3 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public mat4 matrixCompMult(mat4 x, mat4 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public dmat2 matrixCompMult(dmat2 x, dmat2 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public dmat3 matrixCompMult(dmat3 x, dmat3 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public dmat4 matrixCompMult(dmat4 x, dmat4 y) => new();
        #endregion
        #region outerProduct
        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        public mat2 outerProduct(vec2 c, vec2 r) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        public mat3 outerProduct(vec3 c, vec3 r) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        public mat4 outerProduct(vec4 c, vec4 r) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        public mat2x3 outerProduct(vec3 c, vec2 r) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        public mat3x2 outerProduct(vec2 c, vec3 r) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        public mat2x4 outerProduct(vec4 c, vec2 r) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        public mat4x2 outerProduct(vec2 c, vec4 r) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        public mat3x4 outerProduct(vec4 c, vec3 r) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        public mat4x3 outerProduct(vec3 c, vec4 r) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        public dmat2 outerProduct(dvec2 c, dvec2 r) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        public dmat3 outerProduct(dvec3 c, dvec3 r) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        public dmat4 outerProduct(dvec4 c, dvec4 r) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        public dmat2x3 outerProduct(dvec3 c, dvec2 r) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        public dmat3x2 outerProduct(dvec2 c, dvec3 r) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        public dmat2x4 outerProduct(dvec4 c, dvec2 r) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        public dmat4x2 outerProduct(dvec2 c, dvec4 r) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        public dmat3x4 outerProduct(dvec4 c, dvec3 r) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        public dmat4x3 outerProduct(dvec3 c, dvec4 r) => new();
        #endregion
        #region transpose
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public mat2 transpose(mat2 m) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public mat3 transpose(mat3 m) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public mat4 transpose(mat4 m) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public mat2x3 transpose(mat3x2 m) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public mat2x4 transpose(mat4x2 m) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public mat3x2 transpose(mat2x3 m) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public mat3x4 transpose(mat4x3 m) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public mat4x2 transpose(mat2x4 m) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public mat4x3 transpose(mat3x4 m) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public dmat2 transpose(dmat2 m) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public dmat3 transpose(dmat3 m) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public dmat4 transpose(dmat4 m) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public dmat2x3 transpose(dmat3x2 m) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public dmat2x4 transpose(dmat4x2 m) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public dmat3x2 transpose(dmat2x3 m) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public dmat3x4 transpose(dmat4x3 m) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public dmat4x2 transpose(dmat2x4 m) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public dmat4x3 transpose(dmat3x4 m) => new();
        #endregion
    }
}
