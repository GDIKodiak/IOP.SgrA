﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    /// <summary>
    /// 
    /// </summary>
    public struct dmat4x4
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public dvec4 this[int column]
        {
            get => new dvec4();
            set { }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="column"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        public double this[int column, int row]
        {
            get => 0;
            set { }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static dmat4x4 operator ++(dmat4x4 left) => new dmat4x4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static dmat4x4 operator --(dmat4x4 left) => new dmat4x4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dmat4x4 operator +(dmat4x4 left, dmat4x4 right) => new dmat4x4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dmat4x4 operator -(dmat4x4 left, dmat4x4 right) => new dmat4x4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dmat4x4 operator *(dmat4x4 left, dmat4x4 right) => new dmat4x4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec4 operator *(dmat4x4 left, dvec4 right) => new dvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dmat2x4 operator *(dmat4x4 left, dmat2x4 right) => new dmat2x4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dmat3x4 operator *(dmat4x4 left, dmat3x4 right) => new dmat3x4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dmat4x4 operator *(dmat4x4 left, double right) => new dmat4x4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dmat4x4 operator /(dmat4x4 left, dmat4x4 right) => new dmat4x4();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mat"></param>
        public static implicit operator dmat4(dmat4x4 mat) => new dmat4();
    }
}
