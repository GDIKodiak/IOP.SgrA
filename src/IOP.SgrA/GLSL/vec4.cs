﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    /// <summary>
    /// 
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 16, Pack = 4)]
    public struct vec4
    {
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public float x;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(4)]
        public float y;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(8)]
        public float z;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(12)]
        public float w;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public float r;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(4)]
        public float g;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(8)]
        public float b;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(12)]
        public float a;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="w"></param>
        public vec4(float x, float y, float z, float w)
        {
            this.x = x; this.y = y; this.z = z; this.w = w;
            r = x; g = y; b = z; a = w;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vec"></param>
        /// <param name="w"></param>
        public vec4(vec3 vec, float w)
        {
            this.x = vec.x; this.y = vec.y; this.z = vec.z;
            this.w = w;
            r = x; g = y; b = z; a = w;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xy"></param>
        /// <param name="z"></param>
        /// <param name="w"></param>
        public vec4(vec2 xy, float z, float w)
        {
            this.x = xy.x; this.y = xy.y;
            this.z = z; this.w = w;
            r = x; g = y; b = z; a = w;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xy"></param>
        /// <param name="zw"></param>
        public vec4(vec2 xy, vec2 zw)
        {
            this.x = xy.x;
            this.y = xy.y;
            this.z = zw.x;
            this.w = zw.y;
            r = x; g = y; b = z; a = w;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public vec4(float v)
        {
            x = y = z = w = r = g = b = a = v;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public vec4(ivec4 v)
        {
            x = y = z = w = r = g = b = a = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public vec4(uvec4 v)
        {
            x = y = z = w = r = g = b = a = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public vec4(dvec4 v)
        {
            x = y = z = w = r = g = b = a = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public vec4(bvec4 v)
        {
            x = y = z = w = r = g = b = a = 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static vec4 operator ++(vec4 left) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static vec4 operator --(vec4 left) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec4 operator +(vec4 left, vec4 right) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec4 operator -(vec4 left, vec4 right) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec4 operator *(vec4 left, vec4 right) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec2 operator *(vec4 left, mat2x4 right) => new vec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="rgith"></param>
        /// <returns></returns>
        public static vec3 operator *(vec4 left, mat3x4 rgith) => new vec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec4 operator *(vec4 left, mat4 right) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec4 operator *(vec4 left, mat4x4 right) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec4 operator /(vec4 left, vec4 right) => new vec4();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec4 operator +(float left, vec4 right) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec4 operator -(vec4 left, float right) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec4 operator -(float left, vec4 right) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static vec4 operator -(vec4 left) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec4 operator *(vec4 left, float right) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec4 operator *(float left, vec4 right) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec4 operator /(vec4 left, float right) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec4 operator /(uint left, vec4 right) => new vec4();

        #region vec2
        /// <summary>
        /// 
        /// </summary>
        public vec2 xx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 xy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 xz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 xw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 yx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 yy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 yz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 yw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 zx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 zy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 zz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 zw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 wx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 wy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 wz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 ww { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 rr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 rg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 rb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 ra { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 gr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 gg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 gb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 ga { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 br { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 bg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 bb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 ba { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 ar { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 ag { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 ab { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 aa { get => new(); set { } }
        #endregion

        #region vec3
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 xxx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 xxy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 xxz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 xxw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 xyx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 xyy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 xyz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 xyw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 xzx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 xzy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 xzz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 xzw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 xwx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 xwy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 xwz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 xww { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 yxx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 yxy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 yxz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 yxw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 yyx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 yyy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 yyz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 yyw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 yzx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 yzy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 yzz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 yzw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 ywx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 ywy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 ywz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 yww { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 zxx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 zxy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 zxz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 zxw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 zyx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 zyy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 zyz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 zyw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 zzx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 zzy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 zzz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 zzw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 zwx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 zwy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 zwz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 zww { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 wxx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 wxy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 wxz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 wxw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 wyx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 wyy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 wyz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 wyw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 wzx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 wzy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 wzz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 wzw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 wwx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 wwy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 wwz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 www { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 rrr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 rrg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 rrb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 rra { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 rgr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 rgg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 rgb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 rga { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 rbr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 rbg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 rbb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 rba { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 rar { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 rag { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 rab { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 raa { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 grr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 grg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 grb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 gra { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 ggr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 ggg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 ggb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 gga { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 gbr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 gbg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 gbb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 gba { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 gar { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 gag { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 gab { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 gaa { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 brr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 brg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 brb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 bra { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 bgr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 bgg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 bgb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 bga { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 bbr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 bbg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 bbb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 bba { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 bar { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 bag { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 bab { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 baa { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 arr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 arg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 arb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 ara { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 agr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 agg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 agb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 aga { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 abr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 abg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 abb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 aba { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 aar { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 aag { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 aab { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 aaa { get => new(); set { } }
        #endregion

        #region vec4
        /// <summary>
        /// 
        /// </summary>
        public vec4 xxxx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xxxy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xxxz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xxxw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xxyx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xxyy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xxyz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xxyw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xxzx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xxzy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xxzz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xxzw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xxwx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xxwy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xxwz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xxww { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xyxx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xyxy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xyxz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xyxw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xyyx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xyyy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xyyz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xyyw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xyzx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xyzy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xyzz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xyzw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xywx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xywy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xywz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xyww { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xzxx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xzxy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xzxz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xzxw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xzyx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xzyy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xzyz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xzyw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xzzx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xzzy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xzzz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xzzw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xzwx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xzwy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xzwz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xzww { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xwxx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xwxy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xwxz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xwxw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xwyx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xwyy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xwyz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xwyw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xwzx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xwzy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xwzz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xwzw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xwwx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xwwy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xwwz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 xwww { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yxxx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yxxy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yxxz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yxxw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yxyx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yxyy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yxyz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yxyw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yxzx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yxzy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yxzz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yxzw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yxwx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yxwy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yxwz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yxww { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yyxx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yyxy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yyxz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yyxw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yyyx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yyyy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yyyz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yyyw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yyzx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yyzy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yyzz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yyzw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yywx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yywy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yywz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yyww { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yzxx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yzxy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yzxz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yzxw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yzyx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yzyy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yzyz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yzyw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yzzx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yzzy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yzzz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yzzw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yzwx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yzwy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yzwz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 yzww { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ywxx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ywxy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ywxz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ywxw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ywyx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ywyy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ywyz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ywyw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ywzx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ywzy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ywzz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ywzw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ywwx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ywwy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ywwz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ywww { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zxxx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zxxy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zxxz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zxxw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zxyx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zxyy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zxyz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zxyw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zxzx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zxzy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zxzz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zxzw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zxwx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zxwy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zxwz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zxww { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zyxx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zyxy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zyxz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zyxw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zyyx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zyyy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zyyz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zyyw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zyzx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zyzy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zyzz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zyzw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zywx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zywy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zywz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zyww { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zzxx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zzxy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zzxz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zzxw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zzyx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zzyy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zzyz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zzyw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zzzx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zzzy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zzzz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zzzw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zzwx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zzwy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zzwz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zzww { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zwxx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zwxy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zwxz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zwxw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zwyx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zwyy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zwyz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zwyw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zwzx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zwzy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zwzz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zwzw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zwwx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zwwy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zwwz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 zwww { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wxxx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wxxy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wxxz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wxxw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wxyx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wxyy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wxyz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wxyw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wxzx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wxzy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wxzz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wxzw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wxwx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wxwy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wxwz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wxww { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wyxx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wyxy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wyxz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wyxw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wyyx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wyyy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wyyz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wyyw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wyzx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wyzy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wyzz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wyzw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wywx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wywy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wywz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wyww { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wzxx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wzxy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wzxz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wzxw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wzyx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wzyy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wzyz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wzyw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wzzx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wzzy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wzzz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wzzw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wzwx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wzwy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wzwz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wzww { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wwxx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wwxy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wwxz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wwxw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wwyx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wwyy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wwyz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wwyw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wwzx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wwzy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wwzz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wwzw { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wwwx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wwwy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wwwz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 wwww { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rrrr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rrrg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rrrb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rrra { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rrgr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rrgg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rrgb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rrga { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rrbr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rrbg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rrbb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rrba { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rrar { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rrag { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rrab { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rraa { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rgrr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rgrg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rgrb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rgra { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rggr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rggg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rggb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rgga { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rgbr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rgbg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rgbb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rgba { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rgar { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rgag { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rgab { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rgaa { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rbrr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rbrg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rbrb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rbra { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rbgr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rbgg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rbgb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rbga { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rbbr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rbbg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rbbb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rbba { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rbar { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rbag { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rbab { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rbaa { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rarr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rarg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rarb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rara { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ragr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ragg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ragb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 raga { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rabr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rabg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 rabb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 raba { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 raar { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 raag { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 raab { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 raaa { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 grrr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 grrg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 grrb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 grra { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 grgr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 grgg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 grgb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 grga { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 grbr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 grbg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 grbb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 grba { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 grar { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 grag { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 grab { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 graa { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ggrr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ggrg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ggrb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ggra { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gggr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gggg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gggb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ggga { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ggbr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ggbg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ggbb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ggba { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ggar { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ggag { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ggab { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 ggaa { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gbrr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gbrg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gbrb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gbra { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gbgr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gbgg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gbgb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gbga { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gbbr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gbbg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gbbb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gbba { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gbar { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gbag { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gbab { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gbaa { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 garr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 garg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 garb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gara { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gagr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gagg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gagb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gaga { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gabr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gabg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gabb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gaba { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gaar { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gaag { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gaab { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gaaa { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 brrr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 brrg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 brrb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 brra { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 brgr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 brgg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 brgb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 brga { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 brbr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 brbg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 brbb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 brba { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 brar { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 brag { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 brab { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 braa { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bgrr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bgrg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bgrb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bgra { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bggr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bggg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bggb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bgga { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bgbr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bgbg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bgbb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bgba { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bgar { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bgag { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bgab { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bgaa { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bbrr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bbrg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bbrb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bbra { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bbgr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bbgg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bbgb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bbga { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bbbr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bbbg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bbbb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bbba { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bbar { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bbag { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bbab { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bbaa { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 barr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 barg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 barb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bara { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bagr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bagg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 bagb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 baga { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 babr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 babg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 babb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 baba { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 baar { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 baag { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 baab { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 baaa { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 arrr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 arrg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 arrb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 arra { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 argr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 argg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 argb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 arga { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 arbr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 arbg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 arbb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 arba { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 arar { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 arag { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 arab { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 araa { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 agrr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 agrg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 agrb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 agra { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 aggr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 aggg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 aggb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 agga { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 agbr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 agbg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 agbb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 agba { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 agar { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 agag { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 agab { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 agaa { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 abrr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 abrg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 abrb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 abra { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 abgr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 abgg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 abgb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 abga { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 abbr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 abbg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 abbb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 abba { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 abar { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 abag { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 abab { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 abaa { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 aarr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 aarg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 aarb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 aara { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 aagr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 aagg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 aagb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 aaga { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 aabr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 aabg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 aabb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 aaba { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 aaar { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 aaag { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 aaab { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec4 aaaa { get => new(); set { } }
        #endregion
    }
}
