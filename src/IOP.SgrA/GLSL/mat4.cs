﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    /// <summary>
    /// 
    /// </summary>
    public struct mat4
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public vec4 this[int column]
        {
            get => new vec4();
            set { }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="column"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        public float this[int column, int row]
        {
            get => 0;
            set { }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mat"></param>
        public mat4(mat3 mat)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static mat4 operator ++(mat4 left) => new mat4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static mat4 operator --(mat4 left) => new mat4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static mat4 operator +(mat4 left, mat4 right) => new mat4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static mat4 operator -(mat4 left, mat4 right) => new mat4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static mat4 operator *(mat4 left, mat4 right) => new mat4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec4 operator *(mat4 left, vec4 right) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static mat2x4 operator *(mat4 left, mat2x4 right) => new mat2x4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static mat3x4 operator *(mat4 left, mat3x4 right) => new mat3x4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static mat4 operator *(mat4 left, float right) => new mat4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static mat4 operator *(mat4 left, int right) => new mat4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static mat4 operator *(mat4 left, uint right) => new mat4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static mat4 operator /(mat4 left, mat4 right) => new mat4();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mat"></param>
        public static implicit operator mat4x4(mat4 mat) => new mat4();
    }
}
