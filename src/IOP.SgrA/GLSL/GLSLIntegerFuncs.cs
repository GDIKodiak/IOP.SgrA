﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    public partial class GLSLShaderBase
    {
        #region bitCount
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public int bitCount(int value) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public ivec2 bitCount(ivec2 value) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public ivec3 bitCount(ivec3 value) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public ivec4 bitCount(ivec4 value) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public uint bitCount(uint value) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public uvec2 bitCount(uvec2 value) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public uvec3 bitCount(uvec3 value) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public uvec4 bitCount(uvec4 value) => new();
        #endregion
        #region bitfieldExtract
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="offset"></param>
        /// <param name="bits"></param>
        /// <returns></returns>
        public int bitfieldExtract(int value, int offset, int bits) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="offset"></param>
        /// <param name="bits"></param>
        /// <returns></returns>
        public ivec2 bitfieldExtract(ivec2 value, int offset, int bits) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="offset"></param>
        /// <param name="bits"></param>
        /// <returns></returns>
        public ivec3 bitfieldExtract(ivec3 value, int offset, int bits) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="offset"></param>
        /// <param name="bits"></param>
        /// <returns></returns>
        public ivec4 bitfieldExtract(ivec4 value, int offset, int bits) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="offset"></param>
        /// <param name="bits"></param>
        /// <returns></returns>
        public uint bitfieldExtract(uint value, int offset, int bits) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="offset"></param>
        /// <param name="bits"></param>
        /// <returns></returns>
        public uvec2 bitfieldExtract(uvec2 value, int offset, int bits) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="offset"></param>
        /// <param name="bits"></param>
        /// <returns></returns>
        public uvec3 bitfieldExtract(uvec3 value, int offset, int bits) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="offset"></param>
        /// <param name="bits"></param>
        /// <returns></returns>
        public uvec4 bitfieldExtract(uvec4 value, int offset, int bits) => new();
        #endregion
        #region bitfieldInsert
        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <param name="insert"></param>
        /// <param name="offset"></param>
        /// <param name="bits"></param>
        /// <returns></returns>
        public int bitfieldInsert(int b, int insert, int offset, int bits) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <param name="insert"></param>
        /// <param name="offset"></param>
        /// <param name="bits"></param>
        /// <returns></returns>
        public ivec2 bitfieldInsert(ivec2 b, ivec2 insert, int offset, int bits) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <param name="insert"></param>
        /// <param name="offset"></param>
        /// <param name="bits"></param>
        /// <returns></returns>
        public ivec3 bitfieldInsert(ivec3 b, ivec3 insert, int offset, int bits) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <param name="insert"></param>
        /// <param name="offset"></param>
        /// <param name="bits"></param>
        /// <returns></returns>
        public ivec4 bitfieldInsert(ivec4 b, ivec4 insert, int offset, int bits) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <param name="insert"></param>
        /// <param name="offset"></param>
        /// <param name="bits"></param>
        /// <returns></returns>
        public uint bitfieldInsert(uint b, uint insert, int offset, int bits) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <param name="insert"></param>
        /// <param name="offset"></param>
        /// <param name="bits"></param>
        /// <returns></returns>
        public uvec2 bitfieldInsert(uvec2 b, uvec2 insert, int offset, int bits) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <param name="insert"></param>
        /// <param name="offset"></param>
        /// <param name="bits"></param>
        /// <returns></returns>
        public uvec3 bitfieldInsert(uvec3 b, uvec3 insert, int offset, int bits) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <param name="insert"></param>
        /// <param name="offset"></param>
        /// <param name="bits"></param>
        /// <returns></returns>
        public uvec4 bitfieldInsert(uvec4 b, uvec4 insert, int offset, int bits) => new();
        #endregion
        #region bitfieldReverse
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public int bitfieldReverse(int value) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public ivec2 bitfieldReverse(ivec2 value) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public ivec3 bitfieldReverse(ivec3 value) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public ivec4 bitfieldReverse(ivec4 value) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public uint bitfieldReverse(uint value) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public uvec2 bitfieldReverse(uvec2 value) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public uvec3 bitfieldReverse(uvec3 value) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public uvec4 bitfieldReverse(uvec4 value) => new();
        #endregion
        #region findLSB
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public int findLSB(int value) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public ivec2 findLSB(ivec2 value) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public ivec3 findLSB(ivec3 value) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public ivec4 findLSB(ivec4 value) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public uint findLSB(uint value) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public uvec2 findLSB(uvec2 value) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public uvec3 findLSB(uvec3 value) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public uvec4 findLSB(uvec4 value) => new();
        #endregion
        #region findMSB
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public int findMSB(int value) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public ivec2 findMSB(ivec2 value) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public ivec3 findMSB(ivec3 value) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public ivec4 findMSB(ivec4 value) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public uint findMSB(uint value) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public uvec2 findMSB(uvec2 value) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public uvec3 findMSB(uvec3 value) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public uvec4 findMSB(uvec4 value) => new();
        #endregion
        #region uaddCarry
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="carry"></param>
        /// <returns></returns>
        public uint uaddCarry(uint x, uint y, uint carry) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="carry"></param>
        /// <returns></returns>
        public uvec2 uaddCarry(uvec2 x, uvec2 y, uvec2 carry) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="carry"></param>
        /// <returns></returns>
        public uvec3 uaddCarry(uvec3 x, uvec3 y, uvec3 carry) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="carry"></param>
        /// <returns></returns>
        public uvec4 uaddCarry(uvec4 x, uvec4 y, uvec4 carry) => new();
        #endregion
        #region umulExtended
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="msb"></param>
        /// <param name="lsb"></param>
        public void umulExtended(uint x, uint y, uint msb, uint lsb) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="msb"></param>
        /// <param name="lsb"></param>
        public void umulExtended(uvec2 x, uvec2 y, uvec2 msb, uvec2 lsb) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="msb"></param>
        /// <param name="lsb"></param>
        public void umulExtended(uvec3 x, uvec3 y, uvec3 msb, uvec3 lsb) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="msb"></param>
        /// <param name="lsb"></param>
        public void umulExtended(uvec4 x, uvec4 y, uvec4 msb, uvec4 lsb) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="msb"></param>
        /// <param name="lsb"></param>
        public void umulExtended(int x, int y, int msb, int lsb) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="msb"></param>
        /// <param name="lsb"></param>
        public void umulExtended(ivec2 x, ivec2 y, ivec2 msb, ivec2 lsb) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="msb"></param>
        /// <param name="lsb"></param>
        public void umulExtended(ivec3 x, ivec3 y, ivec3 msb, ivec3 lsb) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="msb"></param>
        /// <param name="lsb"></param>
        public void umulExtended(ivec4 x, ivec4 y, ivec4 msb, ivec4 lsb) { }
        #endregion
        #region usubBorrow
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="borrow"></param>
        /// <returns></returns>
        public uint usubBorrow(uint x, uint y, uint borrow) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="borrow"></param>
        /// <returns></returns>
        public uvec2 usubBorrow(uvec2 x, uvec2 y, uvec2 borrow) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="borrow"></param>
        /// <returns></returns>
        public uvec3 usubBorrow(uvec3 x, uvec3 y, uvec3 borrow) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="borrow"></param>
        /// <returns></returns>
        public uvec4 usubBorrow(uvec4 x, uvec4 y, uvec4 borrow) => new();
        #endregion
    }
}
