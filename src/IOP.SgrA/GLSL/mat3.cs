﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    /// <summary>
    /// 
    /// </summary>
    public struct mat3
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public vec3 this[int column]
        {
            get => new vec3();
            set { }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="column"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        public float this[int column, int row]
        {
            get => 0;
            set { }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mat"></param>
        public mat3(mat4 mat)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static mat3 operator ++(mat3 left) => new mat3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static mat3 operator --(mat3 left) => new mat3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static mat3 operator +(mat3 left, mat3 right) => new mat3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static mat3 operator -(mat3 left, mat3 right) => new mat3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static mat3 operator *(mat3 left, mat3 right) => new mat3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec3 operator *(mat3 left, vec3 right) => new vec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static mat2x3 operator *(mat3 left, mat2x3 right) => new mat2x3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static mat4x3 operator *(mat3 left, mat4x3 right) => new mat4x3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static mat3 operator *(mat3 left, float right) => new mat3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static mat3 operator *(mat3 left, int right) => new mat3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static mat3 operator *(mat3 left, uint right) => new mat3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static mat3 operator /(mat3 left, mat3 right) => new mat3();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        public static implicit operator mat3x3(mat3 left) => new mat3x3();
    }
}
