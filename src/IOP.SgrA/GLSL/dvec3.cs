﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    /// <summary>
    /// 
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 24, Pack = 8)]
    public struct dvec3
    {
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public double x;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(8)]
        public double y;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(16)]
        public double z;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public double r;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(8)]
        public double g;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(16)]
        public double b;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public dvec3(double x, double y, double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            r = x; g = y; b = z;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public dvec3(double v)
        {
            x = y = z = r = g = b = v;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public dvec3(vec3 v)
        {
            x = y = z = r = g = b = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public dvec3(uvec3 v)
        {
            x = y = z = r = g = b = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public dvec3(ivec3 v)
        {
            x = y = z = r = g = b = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public dvec3(bvec3 v)
        {
            x = y = z = r = g = b = 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static dvec3 operator ++(dvec3 left) => new dvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static dvec3 operator --(dvec3 left) => new dvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec3 operator +(dvec3 left, dvec3 right) => new dvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec3 operator -(dvec3 left, dvec3 right) => new dvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec3 operator *(dvec3 left, dvec3 right) => new dvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec3 operator /(dvec3 left, dvec3 right) => new dvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec3 operator +(dvec3 left, double right) => new dvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec3 operator +(double left, dvec3 right) => new dvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec3 operator -(dvec3 left, double right) => new dvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec3 operator -(double left, dvec3 right) => new dvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static dvec3 operator -(dvec3 left) => new dvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec3 operator *(dvec3 left, double right) => new dvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec3 operator *(double left, dvec3 right) => new dvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec3 operator /(dvec3 left, double right) => new dvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec3 operator /(double left, dvec3 right) => new dvec3();

        #region dvec2
        /// <summary>
        ///
        /// </summary>
        public dvec2 xx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 xy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 xz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 yx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 yy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 yz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 zx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 zy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 zz { get => new(); set { } }

        /// <summary>
        ///
        /// </summary>
        public dvec2 rr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 rg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 rb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 gr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 gg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 gb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 br { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 bg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 bb { get => new(); set { } }
        #endregion

        #region dvec3
        /// <summary>
        ///
        /// </summary>
        public dvec3 xxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 xxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 xxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 xyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 xyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 xyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 xzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 xzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 xzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 yxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 yxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 yxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 yyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 yyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 yyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 yzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 yzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 yzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 zxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 zxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 zxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 zyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 zyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 zyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 zzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 zzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 zzz { get => new(); set { } }

        /// <summary>
        ///
        /// </summary>
        public dvec3 rrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 rrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 rrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 rgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 rgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 rgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 rbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 rbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 rbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 grr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 grg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 grb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 ggr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 ggg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 ggb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 gbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 gbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 gbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 brr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 brg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 brb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 bgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 bgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 bgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 bbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 bbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 bbb { get => new(); set { } }
        #endregion
    }
}
