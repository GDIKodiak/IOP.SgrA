﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    /// <summary>
    /// 
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 16, Pack = 4)]
    public struct uvec4
    {
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public uint x;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(4)]
        public uint y;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(8)]
        public uint z;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(12)]
        public uint w;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public uint r;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(4)]
        public uint g;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(8)]
        public uint b;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(12)]
        public uint a;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="w"></param>
        public uvec4(uint x, uint y, uint z, uint w)
        {
            this.x = x; this.y = y; this.z = z; this.w = w;
            r = x; g = y; b = z; a = w;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public uvec4(uint v)
        {
            x = y = z = w = r = g = b = a = v;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vec"></param>
        /// <param name="w"></param>
        public uvec4(uvec3 vec, uint w)
        {
            this.x = vec.x; this.y = vec.y; this.z = vec.z;
            this.w = w;
            r = x; g = y; b = z; a = w;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public uvec4(ivec4 v)
        {
            x = y = z = w = r = g = b = a = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public uvec4(vec4 v)
        {
            x = y = z = w = r = g = b = a = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public uvec4(dvec4 v)
        {
            x = y = z = w = r = g = b = a = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public uvec4(bvec4 v)
        {
            x = y = z = w = r = g = b = a = 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static uvec4 operator ++(uvec4 left) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static uvec4 operator --(uvec4 left) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec4 operator +(uvec4 left, uvec4 right) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec4 operator -(uvec4 left, uvec4 right) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec4 operator *(uvec4 left, uvec4 right) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec2 operator *(uvec4 left, mat2x4 right) => new uvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="rgith"></param>
        /// <returns></returns>
        public static uvec3 operator *(uvec4 left, mat3x4 rgith) => new uvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec4 operator *(uvec4 left, mat4 right) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec4 operator *(uvec4 left, mat4x4 right) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec4 operator /(uvec4 left, uvec4 right) => new uvec4();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec4 operator +(uint left, uvec4 right) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec4 operator -(uvec4 left, uint right) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec4 operator -(uint left, uvec4 right) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static uvec4 operator -(uvec4 left) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec4 operator *(uvec4 left, uint right) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec4 operator *(uint left, uvec4 right) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec4 operator /(uvec4 left, uint right) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec4 operator /(uint left, uvec4 right) => new uvec4();

        #region uvec2
        /// <summary>
        ///
        /// </summary>
        public uvec2 xx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 xy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 xz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 xw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 yx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 yy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 yz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 yw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 zx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 zy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 zz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 zw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 wx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 wy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 wz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 ww { get => new(); set { } }

        /// <summary>
        ///
        /// </summary>
        public uvec2 rr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 rg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 rb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 ra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 gr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 gg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 gb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 ga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 br { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 bg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 bb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 ba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 ar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 ag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 ab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 aa { get => new(); set { } }
        #endregion

        #region uvec3
        /// <summary>
        ///
        /// </summary>
        public uvec3 xxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 xxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 xxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 xxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 xyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 xyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 xyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 xyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 xzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 xzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 xzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 xzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 xwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 xwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 xwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 xww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 yxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 yxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 yxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 yxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 yyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 yyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 yyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 yyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 yzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 yzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 yzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 yzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 ywx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 ywy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 ywz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 yww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 zxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 zxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 zxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 zxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 zyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 zyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 zyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 zyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 zzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 zzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 zzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 zzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 zwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 zwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 zwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 zww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 wxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 wxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 wxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 wxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 wyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 wyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 wyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 wyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 wzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 wzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 wzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 wzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 wwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 wwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 wwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 www { get => new(); set { } }

        /// <summary>
        ///
        /// </summary>
        public uvec3 rrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 rrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 rrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 rra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 rgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 rgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 rgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 rga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 rbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 rbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 rbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 rba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 rar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 rag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 rab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 raa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 grr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 grg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 grb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 gra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 ggr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 ggg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 ggb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 gga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 gbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 gbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 gbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 gba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 gar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 gag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 gab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 gaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 brr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 brg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 brb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 bra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 bgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 bgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 bgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 bga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 bbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 bbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 bbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 bba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 bar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 bag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 bab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 baa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 arr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 arg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 arb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 ara { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 agr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 agg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 agb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 aga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 abr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 abg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 abb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 aba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 aar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 aag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 aab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 aaa { get => new(); set { } }
        #endregion

        #region uvec4
        /// <summary>
        ///
        /// </summary>
        public uvec4 xxxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xxxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xxxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xxxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xxyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xxyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xxyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xxyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xxzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xxzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xxzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xxzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xxwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xxwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xxwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xxww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xyxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xyxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xyxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xyxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xyyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xyyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xyyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xyyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xyzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xyzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xyzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xyzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xywx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xywy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xywz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xyww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xzxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xzxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xzxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xzxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xzyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xzyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xzyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xzyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xzzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xzzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xzzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xzzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xzwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xzwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xzwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xzww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xwxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xwxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xwxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xwxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xwyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xwyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xwyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xwyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xwzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xwzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xwzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xwzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xwwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xwwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xwwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 xwww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yxxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yxxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yxxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yxxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yxyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yxyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yxyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yxyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yxzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yxzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yxzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yxzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yxwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yxwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yxwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yxww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yyxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yyxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yyxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yyxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yyyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yyyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yyyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yyyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yyzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yyzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yyzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yyzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yywx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yywy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yywz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yyww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yzxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yzxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yzxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yzxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yzyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yzyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yzyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yzyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yzzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yzzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yzzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yzzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yzwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yzwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yzwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 yzww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ywxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ywxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ywxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ywxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ywyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ywyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ywyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ywyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ywzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ywzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ywzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ywzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ywwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ywwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ywwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ywww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zxxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zxxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zxxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zxxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zxyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zxyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zxyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zxyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zxzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zxzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zxzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zxzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zxwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zxwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zxwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zxww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zyxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zyxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zyxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zyxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zyyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zyyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zyyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zyyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zyzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zyzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zyzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zyzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zywx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zywy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zywz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zyww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zzxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zzxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zzxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zzxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zzyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zzyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zzyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zzyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zzzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zzzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zzzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zzzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zzwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zzwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zzwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zzww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zwxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zwxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zwxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zwxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zwyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zwyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zwyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zwyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zwzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zwzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zwzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zwzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zwwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zwwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zwwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 zwww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wxxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wxxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wxxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wxxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wxyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wxyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wxyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wxyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wxzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wxzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wxzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wxzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wxwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wxwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wxwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wxww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wyxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wyxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wyxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wyxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wyyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wyyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wyyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wyyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wyzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wyzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wyzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wyzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wywx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wywy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wywz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wyww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wzxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wzxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wzxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wzxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wzyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wzyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wzyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wzyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wzzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wzzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wzzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wzzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wzwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wzwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wzwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wzww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wwxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wwxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wwxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wwxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wwyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wwyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wwyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wwyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wwzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wwzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wwzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wwzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wwwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wwwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wwwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 wwww { get => new(); set { } }

        /// <summary>
        ///
        /// </summary>
        public uvec4 rrrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rrrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rrrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rrra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rrgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rrgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rrgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rrga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rrbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rrbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rrbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rrba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rrar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rrag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rrab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rraa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rgrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rgrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rgrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rgra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rggr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rggg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rggb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rgga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rgbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rgbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rgbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rgba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rgar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rgag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rgab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rgaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rbrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rbrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rbrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rbra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rbgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rbgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rbgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rbga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rbbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rbbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rbbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rbba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rbar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rbag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rbab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rbaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rarr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rarg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rarb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rara { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ragr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ragg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ragb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 raga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rabr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rabg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 rabb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 raba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 raar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 raag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 raab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 raaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 grrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 grrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 grrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 grra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 grgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 grgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 grgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 grga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 grbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 grbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 grbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 grba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 grar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 grag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 grab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 graa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ggrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ggrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ggrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ggra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gggr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gggg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gggb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ggga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ggbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ggbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ggbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ggba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ggar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ggag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ggab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 ggaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gbrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gbrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gbrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gbra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gbgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gbgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gbgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gbga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gbbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gbbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gbbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gbba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gbar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gbag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gbab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gbaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 garr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 garg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 garb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gara { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gagr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gagg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gagb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gaga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gabr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gabg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gabb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gaba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gaar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gaag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gaab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 gaaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 brrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 brrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 brrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 brra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 brgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 brgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 brgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 brga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 brbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 brbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 brbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 brba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 brar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 brag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 brab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 braa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bgrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bgrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bgrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bgra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bggr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bggg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bggb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bgga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bgbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bgbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bgbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bgba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bgar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bgag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bgab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bgaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bbrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bbrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bbrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bbra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bbgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bbgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bbgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bbga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bbbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bbbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bbbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bbba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bbar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bbag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bbab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bbaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 barr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 barg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 barb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bara { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bagr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bagg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 bagb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 baga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 babr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 babg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 babb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 baba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 baar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 baag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 baab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 baaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 arrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 arrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 arrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 arra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 argr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 argg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 argb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 arga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 arbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 arbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 arbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 arba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 arar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 arag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 arab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 araa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 agrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 agrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 agrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 agra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 aggr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 aggg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 aggb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 agga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 agbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 agbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 agbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 agba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 agar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 agag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 agab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 agaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 abrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 abrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 abrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 abra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 abgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 abgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 abgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 abga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 abbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 abbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 abbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 abba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 abar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 abag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 abab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 abaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 aarr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 aarg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 aarb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 aara { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 aagr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 aagg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 aagb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 aaga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 aabr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 aabg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 aabb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 aaba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 aaar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 aaag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 aaab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec4 aaaa { get => new(); set { } }
        #endregion
    }
}
