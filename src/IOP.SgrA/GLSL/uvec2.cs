﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    /// <summary>
    /// 
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 8, Pack = 4)]
    public struct uvec2
    {
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public uint x;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(4)]
        public uint y;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public uint r;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(4)]
        public uint g;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public uvec2(uint x, uint y)
        {
            this.x = x;
            this.y = y;
            r = x;
            g = y;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public uvec2(uint v)
        {
            x = y = r = g = v;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public uvec2(ivec2 v)
        {
            x = y = r = g = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public uvec2(vec2 v)
        {
            x = y = r = g = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public uvec2(dvec2 v)
        {
            x = y = r = g = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public uvec2(bvec2 v)
        {
            x = y = r = g = 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static uvec2 operator ++(uvec2 left) => new uvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static uvec2 operator --(uvec2 left) => new uvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec2 operator +(uvec2 left, uvec2 right) => new uvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec2 operator -(uvec2 left, uvec2 right) => new uvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec2 operator *(uvec2 left, uvec2 right) => new uvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec2 operator *(uvec2 left, mat2x2 right) => new uvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec2 operator *(uvec2 left, mat2 right) => new uvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec3 operator *(uvec2 left, mat3x2 right) => new uvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec4 operator *(uvec2 left, mat4x2 right) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec2 operator /(uvec2 left, uvec2 right) => new uvec2();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec2 operator +(uint left, uvec2 right) => new uvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec2 operator -(uvec2 left, uint right) => new uvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec2 operator -(uint left, uvec2 right) => new uvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static uvec2 operator -(uvec2 left) => new uvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec2 operator *(uvec2 left, uint right) => new uvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec2 operator *(uint left, uvec2 right) => new uvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec2 operator /(uvec2 left, uint right) => new uvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec2 operator /(uint left, uvec2 right) => new uvec2();

        /// <summary>
        ///
        /// </summary>
        public uvec2 xx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 xy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 yx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 yy { get => new(); set { } }

        /// <summary>
        ///
        /// </summary>
        public uvec2 rr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 rg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 gr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 gg { get => new(); set { } }
    }
}
