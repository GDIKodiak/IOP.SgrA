﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    /// <summary>
    /// 
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 2, Pack = 1)]
    public struct bvec2
    {
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public bool x;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(1)]
        public bool y;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public bool r;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(1)]
        public bool g;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public bvec2(bool x, bool y)
        {
            this.x = x;
            this.y = y;
            r = x;
            g = y;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public bvec2(bool v)
        {
            x = y = r = g = v;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public bvec2(ivec2 v)
        {
            x = y = r = g = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public bvec2(uvec2 v)
        {
            x = y = r = g = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public bvec2(dvec2 v)
        {
            x = y = r = g = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public bvec2(vec2 v)
        {
            x = y = r = g = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static bvec2 operator ++(bvec2 left) => new bvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static bvec2 operator --(bvec2 left) => new bvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bvec2 operator +(bvec2 left, bvec2 right) => new bvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bvec2 operator -(bvec2 left, bvec2 right) => new bvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bvec2 operator *(bvec2 left, bvec2 right) => new bvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bvec2 operator /(bvec2 left, bvec2 right) => new bvec2();

        /// <summary>
        ///
        /// </summary>
        public bvec2 xx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 xy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 yx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 yy { get => new(); set { } }

        /// <summary>
        ///
        /// </summary>
        public bvec2 rr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 rg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 gr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 gg { get => new(); set { } }
    }
}
