﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    /// <summary>
    /// 
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 12, Pack = 4)]
    public struct ivec3
    {
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public int x;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(4)]
        public int y;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(8)]
        public int z;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public int r;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(4)]
        public int g;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(8)]
        public int b;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public ivec3(int x, int y, int z)
        {
            this.x = x; this.y = y; this.z = z;
            r = x; g = y; b = z;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public ivec3(int v)
        {
            x = y = z = r = g = b = v;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public ivec3(vec3 v)
        {
            x = y = z = r = g = b = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public ivec3(uvec3 v)
        {
            x = y = z = r = g = b = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public ivec3(dvec3 v)
        {
            x = y = z = r = g = b = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public ivec3(bvec3 v)
        {
            x = y = z = r = g = b = 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static ivec3 operator ++(ivec3 left) => new ivec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static ivec3 operator --(ivec3 left) => new ivec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec3 operator +(ivec3 left, ivec3 right) => new ivec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec3 operator -(ivec3 left, ivec3 right) => new ivec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec3 operator *(ivec3 left, ivec3 right) => new ivec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec2 operator *(ivec3 left, mat2x3 right) => new vec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec2 operator *(ivec3 left, mat3x3 right) => new vec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec3 operator *(ivec3 left, mat3 right) => new vec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec4 operator *(ivec3 left, mat4x3 right) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec3 operator /(ivec3 left, ivec3 right) => new ivec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec3 operator +(ivec3 left, int right) => new ivec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec3 operator +(int left, ivec3 right) => new ivec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec3 operator -(ivec3 left, int right) => new ivec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec3 operator -(int left, ivec3 right) => new ivec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static ivec3 operator -(ivec3 left) => new ivec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec3 operator *(ivec3 left, int right) => new ivec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec3 operator *(int left, ivec3 right) => new ivec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec3 operator /(ivec3 left, int right) => new ivec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec3 operator /(int left, ivec3 right) => new ivec3();

        #region ivec3
        /// <summary>
        ///
        /// </summary>
        public ivec2 xx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 xy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 xz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 yx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 yy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 yz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 zx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 zy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 zz { get => new(); set { } }

        /// <summary>
        ///
        /// </summary>
        public ivec2 rr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 rg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 rb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 gr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 gg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 gb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 br { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 bg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 bb { get => new(); set { } }
        #endregion

        #region ivec3
        /// <summary>
        ///
        /// </summary>
        public ivec3 xxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 xxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 xxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 xyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 xyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 xyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 xzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 xzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 xzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 yxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 yxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 yxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 yyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 yyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 yyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 yzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 yzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 yzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 zxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 zxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 zxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 zyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 zyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 zyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 zzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 zzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 zzz { get => new(); set { } }

        /// <summary>
        ///
        /// </summary>
        public ivec3 rrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 rrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 rrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 rgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 rgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 rgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 rbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 rbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 rbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 grr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 grg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 grb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 ggr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 ggg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 ggb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 gbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 gbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 gbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 brr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 brg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 brb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 bgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 bgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 bgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 bbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 bbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec3 bbb { get => new(); set { } }
        #endregion
    }
}
