﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    /// <summary>
    /// 
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 4, Pack = 1)]
    public struct bvec3
    {
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public bool x;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(1)]
        public bool y;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(2)]
        public bool z;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public bool r;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(1)]
        public bool g;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(2)]
        public bool b;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public bvec3(bool x, bool y, bool z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            r = x; g = y; b = z;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public bvec3(bool v)
        {
            x = y = z = r = g = b = v;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public bvec3(vec3 v)
        {
            x = y = z = r = g = b = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public bvec3(uvec3 v)
        {
            x = y = z = r = g = b = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public bvec3(dvec3 v)
        {
            x = y = z = r = g = b = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public bvec3(ivec3 v)
        {
            x = y = z = r = g = b = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static bvec3 operator ++(bvec3 left) => new bvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static bvec3 operator --(bvec3 left) => new bvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bvec3 operator +(bvec3 left, bvec3 right) => new bvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bvec3 operator -(bvec3 left, bvec3 right) => new bvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bvec3 operator *(bvec3 left, bvec3 right) => new bvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bvec3 operator /(bvec3 left, bvec3 right) => new bvec3();

        #region bvec2
        /// <summary>
        ///
        /// </summary>
        public bvec2 xx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 xy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 xz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 yx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 yy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 yz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 zx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 zy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 zz { get => new(); set { } }

        /// <summary>
        ///
        /// </summary>
        public bvec2 rr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 rg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 rb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 gr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 gg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 gb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 br { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 bg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 bb { get => new(); set { } }
        #endregion

        #region bvec3
        /// <summary>
        ///
        /// </summary>
        public bvec3 xxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 xxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 xxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 xyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 xyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 xyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 xzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 xzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 xzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 yxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 yxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 yxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 yyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 yyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 yyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 yzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 yzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 yzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 zxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 zxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 zxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 zyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 zyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 zyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 zzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 zzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 zzz { get => new(); set { } }

        /// <summary>
        ///
        /// </summary>
        public bvec3 rrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 rrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 rrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 rgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 rgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 rgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 rbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 rbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 rbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 grr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 grg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 grb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 ggr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 ggg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 ggb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 gbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 gbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 gbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 brr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 brg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 brb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 bgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 bgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 bgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 bbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 bbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 bbb { get => new(); set { } }
        #endregion
    }
}
