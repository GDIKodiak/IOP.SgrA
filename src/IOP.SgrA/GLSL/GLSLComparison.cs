﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    public partial class GLSLShaderBase
    {
        #region all-any
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public bool all(bvec2 x) => false;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public bool all(bvec3 x) => false;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public bool all(bvec4 x) => false;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public bool any(bvec2 x) => false;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public bool any(bvec3 x) => false;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public bool any(bvec4 x) => false;
        #endregion
        #region greaterThan
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec2 greaterThan(vec2 x, vec2 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec3 greaterThan(vec3 x, vec3 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec4 greaterThan(vec4 x, vec4 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec2 greaterThan(ivec2 x, ivec2 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec3 greaterThan(ivec3 x, ivec3 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec4 greaterThan(ivec4 x, ivec4 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec2 greaterThan(uvec2 x, uvec2 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec3 greaterThan(uvec3 x, uvec3 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec4 greaterThan(uvec4 x, uvec4 y) => new();
        #endregion
        #region greaterThanEqual
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec2 greaterThanEqual(vec2 x, vec2 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec3 greaterThanEqual(vec3 x, vec3 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec4 greaterThanEqual(vec4 x, vec4 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec2 greaterThanEqual(ivec2 x, ivec2 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec3 greaterThanEqual(ivec3 x, ivec3 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec4 greaterThanEqual(ivec4 x, ivec4 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec2 greaterThanEqual(uvec2 x, uvec2 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec3 greaterThanEqual(uvec3 x, uvec3 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec4 greaterThanEqual(uvec4 x, uvec4 y) => new();
        #endregion
        #region lessThan
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec2 lessThan(vec2 x, vec2 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec3 lessThan(vec3 x, vec3 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec4 lessThan(vec4 x, vec4 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec2 lessThan(ivec2 x, ivec2 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec3 lessThan(ivec3 x, ivec3 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec4 lessThan(ivec4 x, ivec4 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec2 lessThan(uvec2 x, uvec2 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec3 lessThan(uvec3 x, uvec3 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec4 lessThan(uvec4 x, uvec4 y) => new();
        #endregion
        #region lessThanEqual
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec2 lessThanEqual(vec2 x, vec2 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec3 lessThanEqual(vec3 x, vec3 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec4 lessThanEqual(vec4 x, vec4 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec2 lessThanEqual(ivec2 x, ivec2 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec3 lessThanEqual(ivec3 x, ivec3 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec4 lessThanEqual(ivec4 x, ivec4 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec2 lessThanEqual(uvec2 x, uvec2 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec3 lessThanEqual(uvec3 x, uvec3 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec4 lessThanEqual(uvec4 x, uvec4 y) => new();
        #endregion
        #region not
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public bvec2 not(bvec2 x) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public bvec3 not(bvec3 x) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public bvec4 not(bvec4 x) => new();
        #endregion
    }
}
