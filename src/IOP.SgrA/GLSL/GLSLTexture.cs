﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    public partial class GLSLShaderBase
    {
        #region textureSize
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public int textureSize(sampler1D sampler, int lod) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public int textureSize(isampler1D sampler, int lod) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public int textureSize(usampler1D sampler, int lod) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec2 textureSize(sampler1DArray sampler, int lod) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec2 textureSize(isampler1DArray sampler, int lod) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec2 textureSize(usampler1DArray sampler, int lod) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec2 textureSize(sampler2D sampler, int lod) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec2 textureSize(isampler2D sampler, int lod) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec2 textureSize(usampler2D sampler, int lod) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec3 textureSize(sampler2DArray sampler, int lod) => new ivec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec3 textureSize(isampler2DArray sampler, int lod) => new ivec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec3 textureSize(usampler2DArray sampler, int lod) => new ivec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public ivec2 textureSize(sampler2DMS sampler) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public ivec2 textureSize(isampler2DMS sampler) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public ivec2 textureSize(usampler2DMS sampler) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public ivec3 textureSize(sampler2DMSArray sampler) => new ivec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public ivec3 textureSize(isampler2DMSArray sampler) => new ivec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public ivec3 textureSize(usampler2DMSArray sampler) => new ivec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public ivec2 textureSize(sampler2DRect sampler) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public ivec2 textureSize(isampler2DRect sampler) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public ivec2 textureSize(usampler2DRect sampler) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec3 textureSize(sampler3D sampler, int lod) => new ivec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec3 textureSize(isampler3D sampler, int lod) => new ivec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec3 textureSize(usampler3D sampler, int lod) => new ivec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureSize(samplerBuffer sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureSize(isamplerBuffer sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureSize(usamplerBuffer sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec2 textureSize(samplerCube sampler, int lod) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec2 textureSize(isamplerCube sampler, int lod) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec2 textureSize(usamplerCube sampler, int lod) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec3 textureSize(samplerCubeArray sampler, int lod) => new ivec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec3 textureSize(isamplerCubeArray sampler, int lod) => new ivec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec3 textureSize(usamplerCubeArray sampler, int lod) => new ivec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public int textureSize(sampler1DShadow sampler, int lod) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec2 textureSize(sampler2DShadow sampler, int lod) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec2 textureSize(samplerCubeShadow sampler, int lod) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public ivec2 textureSize(sampler2DRectShadow sampler) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec2 textureSize(sampler1DArrayShadow sampler, int lod) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec3 textureSize(sampler2DArrayShadow sampler, int lod) => new ivec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec3 textureSize(samplerCubeArrayShadow sampler, int lod) => new ivec3();
        #endregion
        #region texture
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public vec4 texture(sampler1D sampler, float p) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public vec4 texture(sampler1D sampler, float p, float bias) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public ivec4 texture(isampler1D sampler, float p) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public ivec4 texture(isampler1D sampler, float p, float bias) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public uvec4 texture(usampler1D sampler, float p) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public uvec4 texture(usampler1D sampler, float p, float bias) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public vec4 texture(sampler2D sampler, vec2 p) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public vec4 texture(sampler2D sampler, vec2 p, float bias) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public ivec4 texture(isampler2D sampler, vec2 p) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public ivec4 texture(isampler2D sampler, vec2 p, float bias) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public uvec4 texture(usampler2D sampler, vec2 p) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public uvec4 texture(usampler2D sampler, vec2 p, float bias) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public vec4 texture(sampler3D sampler, vec3 p) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public vec4 texture(sampler3D sampler, vec3 p, float bias) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public ivec4 texture(isampler3D sampler, vec3 p) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public ivec4 texture(isampler3D sampler, vec3 p, float bias) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public uvec4 texture(usampler3D sampler, vec3 p) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public uvec4 texture(usampler3D sampler, vec3 p, float bias) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public vec4 texture(samplerCube sampler, vec3 p) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public vec4 texture(samplerCube sampler, vec3 p, float bias) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public ivec4 texture(isamplerCube sampler, vec3 p) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public ivec4 texture(isamplerCube sampler, vec3 p, float bias) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public uvec4 texture(usamplerCube sampler, vec3 p) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public uvec4 texture(usamplerCube sampler, vec3 p, float bias) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public float texture(sampler1DShadow sampler, vec3 p) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public float texture(sampler1DShadow sampler, vec3 p, float bias) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public float texture(sampler2DShadow sampler, vec3 p) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public float texture(sampler2DShadow sampler, vec3 p, float bias) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public float texture(samplerCubeShadow sampler, vec3 p) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public float texture(samplerCubeShadow sampler, vec3 p, float bias) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public vec4 texture(sampler1DArray sampler, vec2 p) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public vec4 texture(sampler1DArray sampler, vec2 p, float bias) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public ivec4 texture(isampler1DArray sampler, vec2 p) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public ivec4 texture(isampler1DArray sampler, vec2 p, float bias) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public uvec4 texture(usampler1DArray sampler, vec2 p) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public uvec4 texture(usampler1DArray sampler, vec2 p, float bias) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public vec4 texture(sampler2DArray sampler, vec3 p) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public vec4 texture(sampler2DArray sampler, vec3 p, float bias) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public ivec4 texture(isampler2DArray sampler, vec3 p) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public ivec4 texture(isampler2DArray sampler, vec3 p, float bias) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public uvec4 texture(usampler2DArray sampler, vec3 p) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public uvec4 texture(usampler2DArray sampler, vec3 p, float bias) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public vec4 texture(samplerCubeArray sampler, vec4 p) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public vec4 texture(samplerCubeArray sampler, vec4 p, float bias) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public ivec4 texture(isamplerCubeArray sampler, vec4 p) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public ivec4 texture(isamplerCubeArray sampler, vec4 p, float bias) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public uvec4 texture(usamplerCubeArray sampler, vec4 p) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public uvec4 texture(usamplerCubeArray sampler, vec4 p, float bias) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public float texture(sampler1DArrayShadow sampler, vec3 p) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public float texture(sampler1DArrayShadow sampler, vec3 p, float bias) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public float texture(sampler2DArrayShadow sampler, vec4 p) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public float texture(sampler2DArrayShadow sampler, vec4 p, float bias) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec4 texture(sampler2DRect sampler, vec2 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public ivec4 texture(isampler2DRect sampler, vec2 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public uvec4 texture(usampler2DRect sampler, vec2 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public float texture(sampler2DRectShadow sampler, vec3 p) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public float texture(sampler2DRectShadow sampler, vec3 p, float bias) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="compare"></param>
        /// <returns></returns>
        public float texture(samplerCubeArrayShadow sampler, vec3 p, float compare) => 0;
        #endregion
        #region interpolateAtCentroid
        /// <summary>
        /// 
        /// </summary>
        /// <param name="interpolant"></param>
        /// <returns></returns>
        public float interpolateAtCentroid(float interpolant) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="interpolant"></param>
        /// <returns></returns>
        public vec2 interpolateAtCentroid(vec2 interpolant) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="interpolant"></param>
        /// <returns></returns>
        public vec3 interpolateAtCentroid(vec3 interpolant) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="interpolant"></param>
        /// <returns></returns>
        public vec4 interpolateAtCentroid(vec4 interpolant) => new();
        #endregion
        #region interpolateAtOffset
        /// <summary>
        /// 
        /// </summary>
        /// <param name="interpolant"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public float interpolateAtOffset(float interpolant, vec2 offset) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="interpolant"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec2 interpolateAtOffset(vec2 interpolant, vec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="interpolant"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec3 interpolateAtOffset(vec3 interpolant, vec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="interpolant"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 interpolateAtOffset(vec4 interpolant, vec2 offset) => new();
        #endregion
        #region interpolateAtSample
        /// <summary>
        /// 
        /// </summary>
        /// <param name="interpolant"></param>
        /// <param name="sample"></param>
        /// <returns></returns>
        public float interpolateAtSample(float interpolant, int sample) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="interpolant"></param>
        /// <param name="sample"></param>
        /// <returns></returns>
        public vec2 interpolateAtSample(vec2 interpolant, int sample) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="interpolant"></param>
        /// <param name="sample"></param>
        /// <returns></returns>
        public vec3 interpolateAtSample(vec3 interpolant, int sample) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="interpolant"></param>
        /// <param name="sample"></param>
        /// <returns></returns>
        public vec4 interpolateAtSample(vec4 interpolant, int sample) => new();
        #endregion
        #region texelFetch
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public vec4 texelFetch(sampler1D sampler, int P, int lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec4 texelFetch(isampler1D sampler, int P, int lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="usampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public uvec4 texelFetch(usampler1D usampler, int P, int lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public vec4 texelFetch(sampler2D sampler, ivec2 P, int lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec4 texelFetch(isampler2D sampler, ivec2 P, int lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="usampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public uvec4 texelFetch(usampler2D usampler, ivec2 P, int lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public vec4 texelFetch(sampler3D sampler, ivec3 P, int lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec4 texelFetch(isampler3D sampler, ivec3 P, int lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="usampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public uvec4 texelFetch(usampler3D usampler, ivec3 P, int lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec4 texelFetch(sampler2DRect sampler, ivec2 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public ivec4 texelFetch(isampler2DRect sampler, ivec2 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="usampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public uvec4 texelFetch(usampler2DRect usampler, ivec2 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public vec4 texelFetch(sampler1DArray sampler, ivec2 P, int lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec4 texelFetch(isampler1DArray sampler, ivec2 P, int lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="usampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public uvec4 texelFetch(usampler1DArray usampler, ivec2 P, int lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public vec4 texelFetch(sampler2DArray sampler, ivec3 P, int lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec4 texelFetch(isampler2DArray sampler, ivec3 P, int lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="usampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public uvec4 texelFetch(usampler2DArray usampler, ivec3 P, int lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec4 texelFetch(samplerBuffer sampler, int P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public ivec4 texelFetch(isamplerBuffer sampler, int P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="usampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public uvec4 texelFetch(usamplerBuffer usampler, int P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <returns></returns>
        public vec4 texelFetch(sampler2DMS sampler, ivec2 P, sample sample) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <returns></returns>
        public ivec4 texelFetch(isampler2DMS sampler, ivec2 P, sample sample) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="usampler"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <returns></returns>
        public uvec4 texelFetch(usampler2DMS usampler, ivec2 P, sample sample) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <returns></returns>
        public vec4 texelFetch(sampler2DMSArray sampler, ivec3 P, sample sample) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <returns></returns>
        public ivec4 texelFetch(isampler2DMSArray sampler, ivec3 P, sample sample) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="usampler"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <returns></returns>
        public uvec4 texelFetch(usampler2DMSArray usampler, ivec3 P, sample sample) => new();
        #endregion
        #region texelFetchOffset
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 texelFetchOffset(sampler1D sampler, int P, int lod, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 texelFetchOffset(isampler1D sampler, int P, int lod, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="usampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 texelFetchOffset(usampler1D usampler, int P, int lod, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 texelFetchOffset(sampler2D sampler, ivec2 P, int lod, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 texelFetchOffset(isampler2D sampler, ivec2 P, int lod, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="usampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 texelFetchOffset(usampler2D usampler, ivec2 P, int lod, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 texelFetchOffset(sampler3D sampler, ivec3 P, int lod, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 texelFetchOffset(isampler3D sampler, ivec3 P, int lod, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="usampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 texelFetchOffset(usampler3D usampler, ivec3 P, int lod, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 texelFetchOffset(sampler2DRect sampler, ivec2 P, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 texelFetchOffset(isampler2DRect sampler, ivec2 P, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="usampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 texelFetchOffset(usampler2DRect usampler, ivec2 P, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 texelFetchOffset(sampler1DArray sampler, ivec2 P, int lod, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 texelFetchOffset(isampler1DArray sampler, ivec2 P, int lod, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="usampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 texelFetchOffset(usampler1DArray usampler, ivec2 P, int lod, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 texelFetchOffset(sampler2DArray sampler, ivec3 P, int lod, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 texelFetchOffset(isampler2DArray sampler, ivec3 P, int lod, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="usampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 texelFetchOffset(usampler2DArray usampler, ivec3 P, int lod, int offset) => new();
        #endregion
        #region textureGather
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec4 textureGather(sampler2D sampler, vec2 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <returns></returns>
        public vec4 textureGather(sampler2D sampler, vec2 P, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public ivec4 textureGather(isampler2D sampler, vec2 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <returns></returns>
        public ivec4 textureGather(isampler2D sampler, vec2 P, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public uvec4 textureGather(usampler2D sampler, vec2 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <returns></returns>
        public uvec4 textureGather(usampler2D sampler, vec2 P, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec4 textureGather(sampler2DArray sampler, vec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <returns></returns>
        public vec4 textureGather(sampler2DArray sampler, vec3 P, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public ivec4 textureGather(isampler2DArray sampler, vec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <returns></returns>
        public ivec4 textureGather(isampler2DArray sampler, vec3 P, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public uvec4 textureGather(usampler2DArray sampler, vec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <returns></returns>
        public uvec4 textureGather(usampler2DArray sampler, vec3 P, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec4 textureGather(samplerCube sampler, vec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <returns></returns>
        public vec4 textureGather(samplerCube sampler, vec3 P, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public ivec4 textureGather(isamplerCube sampler, vec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <returns></returns>
        public ivec4 textureGather(isamplerCube sampler, vec3 P, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public uvec4 textureGather(usamplerCube sampler, vec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <returns></returns>
        public uvec4 textureGather(usamplerCube sampler, vec3 P, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec4 textureGather(samplerCubeArray sampler, vec4 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <returns></returns>
        public vec4 textureGather(samplerCubeArray sampler, vec4 P, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public ivec4 textureGather(isamplerCubeArray sampler, vec4 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <returns></returns>
        public ivec4 textureGather(isamplerCubeArray sampler, vec4 P, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public uvec4 textureGather(usamplerCubeArray sampler, vec4 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <returns></returns>
        public uvec4 textureGather(usamplerCubeArray sampler, vec4 P, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec4 textureGather(sampler2DRect sampler, vec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <returns></returns>
        public vec4 textureGather(sampler2DRect sampler, vec3 P, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public ivec4 textureGather(isampler2DRect sampler, vec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <returns></returns>
        public ivec4 textureGather(isampler2DRect sampler, vec3 P, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public uvec4 textureGather(usampler2DRect sampler, vec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <returns></returns>
        public uvec4 textureGather(usampler2DRect sampler, vec3 P, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="refZ"></param>
        /// <returns></returns>
        public vec4 textureGather(sampler2DShadow sampler, vec3 P, float refZ) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="refZ"></param>
        /// <returns></returns>
        public vec4 textureGather(sampler2DArrayShadow sampler, vec3 P, float refZ) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="refZ"></param>
        /// <returns></returns>
        public vec4 textureGather(samplerCubeShadow sampler, vec3 P, float refZ) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="refZ"></param>
        /// <returns></returns>
        public vec4 textureGather(samplerCubeArrayShadow sampler, vec4 P, float refZ) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="refZ"></param>
        /// <returns></returns>
        public vec4 textureGather(sampler2DRectShadow sampler, vec3 P, float refZ) => new();
        #endregion
        #region textureGatherOffset
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureGatherOffset(sampler2D sampler, vec2 P, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureGatherOffset(sampler2D sampler, vec2 P, ivec2 offset, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureGatherOffset(isampler2D sampler, vec2 P, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureGatherOffset(isampler2D sampler, vec2 P, ivec2 offset, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureGatherOffset(usampler2D sampler, vec2 P, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureGatherOffset(usampler2D sampler, vec2 P, ivec2 offset, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureGatherOffset(sampler2DArray sampler, vec3 P, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureGatherOffset(sampler2DArray sampler, vec3 P, ivec2 offset, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureGatherOffset(isampler2DArray sampler, vec3 P, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureGatherOffset(isampler2DArray sampler, vec3 P, ivec2 offset, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureGatherOffset(usampler2DArray sampler, vec3 P, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureGatherOffset(usampler2DArray sampler, vec3 P, ivec2 offset, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureGatherOffset(sampler2DRect sampler, vec3 P, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureGatherOffset(sampler2DRect sampler, vec3 P, ivec2 offset, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureGatherOffset(isampler2DRect sampler, vec3 P, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureGatherOffset(isampler2DRect sampler, vec3 P, ivec2 offset, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureGatherOffset(usampler2DRect sampler, vec3 P, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureGatherOffset(usampler2DRect sampler, vec3 P, ivec2 offset, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="refZ"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureGatherOffset(sampler2DShadow sampler, vec3 P, float refZ, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="refZ"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureGatherOffset(sampler2DArrayShadow sampler, vec3 P, float refZ, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="refZ"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureGatherOffset(sampler2DRectShadow sampler, vec3 P, float refZ, ivec2 offset) => new();
        #endregion
        #region textureGatherOffsets
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureGatherOffsets(sampler2D sampler, vec2 P, ivec2[] offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureGatherOffsets(sampler2D sampler, vec2 P, ivec2[] offset, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureGatherOffsets(isampler2D sampler, vec2 P, ivec2[] offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureGatherOffsets(isampler2D sampler, vec2 P, ivec2[] offset, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureGatherOffsets(usampler2D sampler, vec2 P, ivec2[] offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureGatherOffsets(usampler2D sampler, vec2 P, ivec2[] offset, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureGatherOffsets(sampler2DArray sampler, vec3 P, ivec2[] offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureGatherOffsets(sampler2DArray sampler, vec3 P, ivec2[] offset, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureGatherOffsets(isampler2DArray sampler, vec3 P, ivec2[] offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureGatherOffsets(isampler2DArray sampler, vec3 P, ivec2[] offset, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureGatherOffsets(usampler2DArray sampler, vec3 P, ivec2[] offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureGatherOffsets(usampler2DArray sampler, vec3 P, ivec2[] offset, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureGatherOffsets(sampler2DRect sampler, vec3 P, ivec2[] offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureGatherOffsets(sampler2DRect sampler, vec3 P, ivec2[] offset, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureGatherOffsets(isampler2DRect sampler, vec3 P, ivec2[] offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureGatherOffsets(isampler2DRect sampler, vec3 P, ivec2[] offset, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureGatherOffsets(usampler2DRect sampler, vec3 P, ivec2[] offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="comp"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureGatherOffsets(usampler2DRect sampler, vec3 P, ivec2[] offset, int comp) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="refZ"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureGatherOffsets(sampler2DShadow sampler, vec3 P, float refZ, ivec2[] offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="refZ"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureGatherOffsets(sampler2DArrayShadow sampler, vec3 P, float refZ, ivec2[] offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="refZ"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureGatherOffsets(sampler2DRectShadow sampler, vec3 P, float refZ, ivec2[] offset) => new();
        #endregion
        #region textureGrad
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <returns></returns>
        public vec4 textureGrad(sampler1D sampler, float P, float dPdx, float dPdy) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <returns></returns>
        public ivec4 textureGrad(isampler1D sampler, float P, float dPdx, float dPdy) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <returns></returns>
        public uvec4 textureGrad(usampler1D sampler, float P, float dPdx, float dPdy) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <returns></returns>
        public vec4 textureGrad(sampler2D sampler, vec2 P, vec2 dPdx, vec2 dPdy) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <returns></returns>
        public ivec4 textureGrad(isampler2D sampler, vec2 P, vec2 dPdx, vec2 dPdy) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <returns></returns>
        public uvec4 textureGrad(usampler2D sampler, vec2 P, vec2 dPdx, vec2 dPdy) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <returns></returns>
        public vec4 textureGrad(sampler3D sampler, vec3 P, vec3 dPdx, vec3 dPdy) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <returns></returns>
        public ivec4 textureGrad(isampler3D sampler, vec3 P, vec3 dPdx, vec3 dPdy) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <returns></returns>
        public uvec4 textureGrad(usampler3D sampler, vec3 P, vec3 dPdx, vec3 dPdy) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <returns></returns>
        public vec4 textureGrad(samplerCube sampler, vec3 P, vec3 dPdx, vec3 dPdy) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <returns></returns>
        public ivec4 textureGrad(isamplerCube sampler, vec3 P, vec3 dPdx, vec3 dPdy) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <returns></returns>
        public uvec4 textureGrad(usamplerCube sampler, vec3 P, vec3 dPdx, vec3 dPdy) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <returns></returns>
        public vec4 textureGrad(sampler2DRect sampler, vec2 P, vec2 dPdx, vec2 dPdy) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <returns></returns>
        public ivec4 textureGrad(isampler2DRect sampler, vec2 P, vec2 dPdx, vec2 dPdy) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <returns></returns>
        public uvec4 textureGrad(usampler2DRect sampler, vec2 P, vec2 dPdx, vec2 dPdy) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <returns></returns>
        public float textureGrad(sampler1DShadow sampler, vec3 P, float dPdx, float dPdy) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <returns></returns>
        public float textureGrad(sampler2DShadow sampler, vec3 P, vec2 dPdx, vec2 dPdy) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <returns></returns>
        public vec4 textureGrad(sampler1DArray sampler, vec2 P, float dPdx, float dPdy) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <returns></returns>
        public ivec4 textureGrad(isampler1DArray sampler, vec2 P, float dPdx, float dPdy) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <returns></returns>
        public uvec4 textureGrad(usampler1DArray sampler, vec2 P, float dPdx, float dPdy) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <returns></returns>
        public vec4 textureGrad(sampler2DArray sampler, vec3 P, vec2 dPdx, vec2 dPdy) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <returns></returns>
        public ivec4 textureGrad(isampler2DArray sampler, vec3 P, vec2 dPdx, vec2 dPdy) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <returns></returns>
        public uvec4 textureGrad(usampler2DArray sampler, vec3 P, vec2 dPdx, vec2 dPdy) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <returns></returns>
        public float textureGrad(sampler1DArrayShadow sampler, vec3 P, float dPdx, float dPdy) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <returns></returns>
        public vec4 textureGrad(samplerCubeArray sampler, vec4 P, vec3 dPdx, vec3 dPdy) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <returns></returns>
        public ivec4 textureGrad(isamplerCubeArray sampler, vec4 P, vec3 dPdx, vec3 dPdy) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <returns></returns>
        public uvec4 textureGrad(usamplerCubeArray sampler, vec4 P, vec3 dPdx, vec3 dPdy) => new uvec4();
        #endregion
        #region textureGradOffset
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureGradOffset(sampler1D sampler, float P, float dPdx, float dPdy, int offset) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureGradOffset(isampler1D sampler, float P, float dPdx, float dPdy, int offset) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureGradOffset(usampler1D sampler, float P, float dPdx, float dPdy, int offset) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureGradOffset(sampler2D sampler, vec2 P, vec2 dPdx, vec2 dPdy, ivec2 offset) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureGradOffset(isampler2D sampler, vec2 P, vec2 dPdx, vec2 dPdy, ivec2 offset) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureGradOffset(usampler2D sampler, vec2 P, vec2 dPdx, vec2 dPdy, ivec2 offset) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureGradOffset(sampler3D sampler, vec3 P, vec3 dPdx, vec3 dPdy, ivec3 offset) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureGradOffset(isampler3D sampler, vec3 P, vec3 dPdx, vec3 dPdy, ivec3 offset) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureGradOffset(usampler3D sampler, vec3 P, vec3 dPdx, vec3 dPdy, ivec3 offset) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureGradOffset(sampler2DRect sampler, vec2 P, vec2 dPdx, vec2 dPdy, ivec2 offset) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureGradOffset(isampler2DRect sampler, vec2 P, vec2 dPdx, vec2 dPdy, ivec2 offset) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureGradOffset(usampler2DRect sampler, vec2 P, vec2 dPdx, vec2 dPdy, ivec2 offset) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public float textureGradOffset(sampler1DShadow sampler, vec3 P, float dPdx, float dPdy, int offset) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public float textureGradOffset(sampler2DShadow sampler, vec3 P, vec2 dPdx, vec2 dPdy, ivec2 offset) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureGradOffset(sampler1DArray sampler, vec2 P, float dPdx, float dPdy, int offset) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureGradOffset(isampler1DArray sampler, vec2 P, float dPdx, float dPdy, int offset) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureGradOffset(usampler1DArray sampler, vec2 P, float dPdx, float dPdy, int offset) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureGradOffset(sampler2DArray sampler, vec3 P, vec2 dPdx, vec2 dPdy, ivec2 offset) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureGradOffset(isampler2DArray sampler, vec3 P, vec2 dPdx, vec2 dPdy, ivec2 offset) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureGradOffset(usampler2DArray sampler, vec3 P, vec2 dPdx, vec2 dPdy, ivec2 offset) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public float textureGradOffset(sampler1DArrayShadow sampler, vec3 P, float dPdx, float dPdy, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="dPdx"></param>
        /// <param name="dPdy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public float textureGradOffset(sampler2DArrayShadow sampler, vec3 P, float dPdx, float dPdy, ivec2 offset) => new();
        #endregion
        #region textureLod
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public vec4 textureLod(sampler1D sampler, float P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec4 textureLod(isampler1D sampler, float P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public uvec4 textureLod(usampler1D sampler, float P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public vec4 textureLod(sampler2D sampler, vec2 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec4 textureLod(isampler2D sampler, vec2 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public uvec4 textureLod(usampler2D sampler, vec2 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public vec4 textureLod(sampler3D sampler, vec3 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec4 textureLod(isampler3D sampler, vec3 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public uvec4 textureLod(usampler3D sampler, vec3 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public vec4 textureLod(samplerCube sampler, vec3 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec4 textureLod(isamplerCube sampler, vec3 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public uvec4 textureLod(usamplerCube sampler, vec3 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public float textureLod(sampler1DShadow sampler, vec3 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public float textureLod(sampler2DShadow sampler, vec4 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public vec4 textureLod(sampler1DArray sampler, vec2 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec4 textureLod(isampler1DArray sampler, vec2 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public uvec4 textureLod(usampler1DArray sampler, vec2 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public vec4 textureLod(sampler2DArray sampler, vec3 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec4 textureLod(isampler2DArray sampler, vec3 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public uvec4 textureLod(usampler2DArray sampler, vec3 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public float textureLod(sampler1DArrayShadow sampler, vec3 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public vec4 textureLod(samplerCubeArray sampler, vec4 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec4 textureLod(isamplerCubeArray sampler, vec4 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public uvec4 textureLod(usamplerCubeArray sampler, vec4 P, float lod) => new();

        #endregion
        #region textureLodOffset
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureLodOffset(sampler1D sampler, float P, float lod, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureLodOffset(isampler1D sampler, float P, float lod, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureLodOffset(usampler1D sampler, float P, float lod, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureLodOffset(sampler2D sampler, vec2 P, float lod, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureLodOffset(isampler2D sampler, vec2 P, float lod, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureLodOffset(usampler2D sampler, vec2 P, float lod, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureLodOffset(sampler3D sampler, vec3 P, float lod, ivec3 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureLodOffset(isampler3D sampler, vec3 P, float lod, ivec3 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureLodOffset(usampler3D sampler, vec3 P, float lod, ivec3 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public float textureLodOffset(sampler1DShadow sampler, vec3 P, float lod, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public float textureLodOffset(sampler2DShadow sampler, vec4 P, float lod, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureLodOffset(sampler1DArray sampler, vec2 P, float lod, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureLodOffset(isampler1DArray sampler, vec2 P, float lod, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureLodOffset(usampler1DArray sampler, vec2 P, float lod, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureLodOffset(sampler2DArray sampler, vec3 P, float lod, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureLodOffset(isampler2DArray sampler, vec3 P, float lod, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureLodOffset(usampler2DArray sampler, vec3 P, float lod, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public float textureLodOffset(sampler1DArrayShadow sampler, vec3 P, float lod, int offset) => new();
        #endregion
        #region textureOffset
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureOffset(sampler1D sampler, float p, int offset) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureOffset(sampler1D sampler, float p, int offset, float bias) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureOffset(isampler1D sampler, float p, int offset) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureOffset(isampler1D sampler, float p, int offset, float bias) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureOffset(usampler1D sampler, float p, int offset) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureOffset(usampler1D sampler, float p, int offset, float bias) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureOffset(sampler2D sampler, vec2 p, ivec2 offset) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureOffset(sampler2D sampler, vec2 p, ivec2 offset, float bias) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureOffset(isampler2D sampler, vec2 p, ivec2 offset) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureOffset(isampler2D sampler, vec2 p, ivec2 offset, float bias) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureOffset(usampler2D sampler, vec2 p, ivec2 offset) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureOffset(usampler2D sampler, vec2 p, ivec2 offset, float bias) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureOffset(sampler3D sampler, vec3 p, ivec3 offset) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureOffset(sampler3D sampler, vec3 p, ivec3 offset, float bias) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureOffset(isampler3D sampler, vec3 p, ivec3 offset) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureOffset(isampler3D sampler, vec3 p, ivec3 offset, float bias) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureOffset(usampler3D sampler, vec3 p, ivec3 offset) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureOffset(usampler3D sampler, vec3 p, ivec3 offset, float bias) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureOffset(sampler2DRect sampler, vec2 P, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureOffset(isampler2DRect sampler, vec2 P, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureOffset(usampler2DRect sampler, vec2 P, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public float textureOffset(sampler2DRectShadow sampler, vec3 p, ivec2 offset) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public float textureOffset(sampler1DShadow sampler, vec3 p, int offset) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public float textureOffset(sampler1DShadow sampler, vec3 p, int offset, float bias) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public float textureOffset(sampler2DShadow sampler, vec3 p, ivec2 offset) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public float textureOffset(sampler2DShadow sampler, vec3 p, ivec2 offset, float bias) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureOffset(sampler1DArray sampler, vec2 p, int offset) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureOffset(sampler1DArray sampler, vec2 p, int offset, float bias) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureOffset(isampler1DArray sampler, vec2 p, int offset) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="offset"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public ivec4 textureOffset(isampler1DArray sampler, vec2 p, int offset, float bias) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureOffset(usampler1DArray sampler, vec2 p, int offset) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureOffset(usampler1DArray sampler, vec2 p, int offset, float bias) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureOffset(sampler2DArray sampler, vec3 p, ivec2 offset) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureOffset(sampler2DArray sampler, vec3 p, ivec2 offset, float bias) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureOffset(isampler2DArray sampler, vec3 p, ivec2 offset) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureOffset(isampler2DArray sampler, vec3 p, ivec2 offset, float bias) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureOffset(usampler2DArray sampler, vec3 p, ivec2 offset) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureOffset(usampler2DArray sampler, vec3 p, ivec2 offset, float bias) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public float textureOffset(sampler1DArrayShadow sampler, vec3 p, int offset) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="p"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public float textureOffset(sampler2DArrayShadow sampler, vec4 p, vec2 offset) => 0;
        #endregion
        #region textureProj
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec4 textureProj(sampler1D sampler, vec2 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public vec4 textureProj(sampler1D sampler, vec2 P, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public ivec4 textureProj(isampler1D sampler, vec2 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public ivec4 textureProj(isampler1D sampler, vec2 P, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public uvec4 textureProj(usampler1D sampler, vec2 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public uvec4 textureProj(usampler1D sampler, vec2 P, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec4 textureProj(sampler1D sampler, vec4 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public vec4 textureProj(sampler1D sampler, vec4 P, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public ivec4 textureProj(isampler1D sampler, vec4 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public ivec4 textureProj(isampler1D sampler, vec4 P, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public uvec4 textureProj(usampler1D sampler, vec4 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public uvec4 textureProj(usampler1D sampler, vec4 P, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec4 textureProj(sampler2D sampler, vec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public vec4 textureProj(sampler2D sampler, vec3 P, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public ivec4 textureProj(isampler2D sampler, vec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public ivec4 textureProj(isampler2D sampler, vec3 P, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public uvec4 textureProj(usampler2D sampler, vec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public uvec4 textureProj(usampler2D sampler, vec3 P, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec4 textureProj(sampler2D sampler, vec4 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public vec4 textureProj(sampler2D sampler, vec4 P, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public ivec4 textureProj(isampler2D sampler, vec4 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public ivec4 textureProj(isampler2D sampler, vec4 P, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public uvec4 textureProj(usampler2D sampler, vec4 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public uvec4 textureProj(usampler2D sampler, vec4 P, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec4 textureProj(sampler3D sampler, vec4 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public vec4 textureProj(sampler3D sampler, vec4 P, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public ivec4 textureProj(isampler3D sampler, vec4 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public ivec4 textureProj(isampler3D sampler, vec4 P, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public uvec4 textureProj(usampler3D sampler, vec4 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public uvec4 textureProj(usampler3D sampler, vec4 P, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public float textureProj(sampler1DShadow sampler, vec4 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public float textureProj(sampler1DShadow sampler, vec4 P, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public float textureProj(sampler2DShadow sampler, vec4 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <returns></returns>
        public float textureProj(sampler2DShadow sampler, vec4 P, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec4 textureProj(sampler2DRect sampler, vec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public ivec4 textureProj(isampler2DRect sampler, vec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public uvec4 textureProj(usampler2DRect sampler, vec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec4 textureProj(sampler2DRect sampler, vec4 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public ivec4 textureProj(isampler2DRect sampler, vec4 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public uvec4 textureProj(usampler2DRect sampler, vec4 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public float textureProj(sampler2DRectShadow sampler, vec4 P) => new();
        #endregion
        #region textureProjGrad
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <returns></returns>
        public vec4 textureProjGrad(sampler1D sampler, vec2 P, float pDx, float pDy) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <returns></returns>
        public ivec4 textureProjGrad(isampler1D sampler, vec2 P, float pDx, float pDy) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <returns></returns>
        public uvec4 textureProjGrad(usampler1D sampler, vec2 P, float pDx, float pDy) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <returns></returns>
        public vec4 textureProjGrad(sampler1D sampler, vec4 P, float pDx, float pDy) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <returns></returns>
        public ivec4 textureProjGrad(isampler1D sampler, vec4 P, float pDx, float pDy) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <returns></returns>
        public uvec4 textureProjGrad(usampler1D sampler, vec4 P, float pDx, float pDy) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <returns></returns>
        public vec4 textureProjGrad(sampler2D sampler, vec3 P, vec2 pDx, vec2 pDy) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <returns></returns>
        public ivec4 textureProjGrad(isampler2D sampler, vec3 P, vec2 pDx, vec2 pDy) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <returns></returns>
        public uvec4 textureProjGrad(usampler2D sampler, vec3 P, vec2 pDx, vec2 pDy) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <returns></returns>
        public vec4 textureProjGrad(sampler2D sampler, vec4 P, vec2 pDx, vec2 pDy) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <returns></returns>
        public ivec4 textureProjGrad(isampler2D sampler, vec4 P, vec2 pDx, vec2 pDy) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <returns></returns>
        public uvec4 textureProjGrad(usampler2D sampler, vec4 P, vec2 pDx, vec2 pDy) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <returns></returns>
        public vec4 textureProjGrad(sampler3D sampler, vec4 P, vec3 pDx, vec3 pDy) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <returns></returns>
        public ivec4 textureProjGrad(isampler3D sampler, vec4 P, vec3 pDx, vec3 pDy) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <returns></returns>
        public uvec4 textureProjGrad(usampler3D sampler, vec4 P, vec3 pDx, vec3 pDy) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <returns></returns>
        public float textureProjGrad(sampler1DShadow sampler, vec4 P, float pDx, float pDy) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <returns></returns>
        public float textureProjGrad(sampler2DShadow sampler, vec4 P, vec2 pDx, vec2 pDy) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <returns></returns>
        public vec4 textureProjGrad(sampler2DRect sampler, vec3 P, vec2 pDx, vec2 pDy) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <returns></returns>
        public ivec4 textureProjGrad(isampler2DRect sampler, vec3 P, vec2 pDx, vec2 pDy) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <returns></returns>
        public uvec4 textureProjGrad(usampler2DRect sampler, vec3 P, vec2 pDx, vec2 pDy) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <returns></returns>
        public vec4 textureProjGrad(sampler2DRect sampler, vec4 P, vec2 pDx, vec2 pDy) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <returns></returns>
        public ivec4 textureProjGrad(isampler2DRect sampler, vec4 P, vec2 pDx, vec2 pDy) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <returns></returns>
        public uvec4 textureProjGrad(usampler2DRect sampler, vec4 P, vec2 pDx, vec2 pDy) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <returns></returns>
        public float textureProjGrad(sampler2DRectShadow sampler, vec4 P, vec2 pDx, vec2 pDy) => new();
        #endregion
        #region textureProjGradOffset
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureProjGradOffset(sampler1D sampler, vec2 P, float pDx, float pDy, int offset) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureProjGradOffset(isampler1D sampler, vec2 P, float pDx, float pDy, int offset) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureProjGradOffset(usampler1D sampler, vec2 P, float pDx, float pDy, int offset) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureProjGradOffset(sampler1D sampler, vec4 P, float pDx, float pDy, int offset) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureProjGradOffset(isampler1D sampler, vec4 P, float pDx, float pDy, int offset) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureProjGradOffset(usampler1D sampler, vec4 P, float pDx, float pDy, int offset) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureProjGradOffset(sampler2D sampler, vec3 P, vec2 pDx, vec2 pDy, ivec2 offset) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureProjGradOffset(isampler2D sampler, vec3 P, vec2 pDx, vec2 pDy, ivec2 offset) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureProjGradOffset(usampler2D sampler, vec3 P, vec2 pDx, vec2 pDy, ivec2 offset) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureProjGradOffset(sampler2D sampler, vec4 P, vec2 pDx, vec2 pDy, ivec2 offset) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureProjGradOffset(isampler2D sampler, vec4 P, vec2 pDx, vec2 pDy, ivec2 offset) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureProjGradOffset(usampler2D sampler, vec4 P, vec2 pDx, vec2 pDy, ivec2 offset) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureProjGradOffset(sampler3D sampler, vec4 P, vec3 pDx, vec3 pDy, ivec3 offset) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureProjGradOffset(isampler3D sampler, vec4 P, vec3 pDx, vec3 pDy, ivec3 offset) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureProjGradOffset(usampler3D sampler, vec4 P, vec3 pDx, vec3 pDy, ivec3 offset) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public float textureProjGradOffset(sampler1DShadow sampler, vec4 P, float pDx, float pDy, int offset) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public float textureProjGradOffset(sampler2DShadow sampler, vec4 P, vec2 pDx, vec2 pDy, ivec2 offset) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureProjGradOffset(sampler2DRect sampler, vec3 P, vec2 pDx, vec2 pDy, ivec2 offset) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureProjGradOffset(isampler2DRect sampler, vec3 P, vec2 pDx, vec2 pDy, ivec2 offset) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureProjGradOffset(usampler2DRect sampler, vec3 P, vec2 pDx, vec2 pDy, ivec2 offset) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureProjGradOffset(sampler2DRect sampler, vec4 P, vec2 pDx, vec2 pDy, ivec2 offset) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureProjGradOffset(isampler2DRect sampler, vec4 P, vec2 pDx, vec2 pDy, ivec2 offset) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureProjGradOffset(usampler2DRect sampler, vec4 P, vec2 pDx, vec2 pDy, ivec2 offset) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public float textureProjGradOffset(sampler2DRectShadow sampler, vec4 P, vec2 pDx, vec2 pDy, ivec2 offset) => new();
        #endregion
        #region textureProjLod
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public vec4 textureProjLod(sampler1D sampler, vec2 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec4 textureProjLod(isampler1D sampler, vec2 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public uvec4 textureProjLod(usampler1D sampler, vec2 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public vec4 textureProjLod(sampler1D sampler, vec4 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec4 textureProjLod(isampler1D sampler, vec4 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public uvec4 textureProjLod(usampler1D sampler, vec4 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public vec4 textureProjLod(sampler2D sampler, vec3 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec4 textureProjLod(isampler2D sampler, vec3 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public uvec4 textureProjLod(usampler2D sampler, vec3 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public vec4 textureProjLod(sampler2D sampler, vec4 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec4 textureProjLod(isampler2D sampler, vec4 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public uvec4 textureProjLod(usampler2D sampler, vec4 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public vec4 textureProjLod(sampler3D sampler, vec4 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public ivec4 textureProjLod(isampler3D sampler, vec4 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public uvec4 textureProjLod(usampler3D sampler, vec4 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public float textureProjLod(sampler1DShadow sampler, vec4 P, float lod) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <returns></returns>
        public float textureProjLod(sampler2DShadow sampler, vec4 P, float lod) => new();
        #endregion
        #region textureProjLodOffset
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureProjLodOffset(sampler1D sampler, vec2 P, float lod, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureProjLodOffset(isampler1D sampler, vec2 P, float lod, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureProjLodOffset(usampler1D sampler, vec2 P, float lod, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureProjLodOffset(sampler1D sampler, vec4 P, float lod, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureProjLodOffset(isampler1D sampler, vec4 P, float lod, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureProjLodOffset(usampler1D sampler, vec4 P, float lod, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureProjLodOffset(sampler2D sampler, vec3 P, float lod, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureProjLodOffset(isampler2D sampler, vec3 P, float lod, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureProjLodOffset(usampler2D sampler, vec3 P, float lod, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureProjLodOffset(sampler2D sampler, vec4 P, float lod, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureProjLodOffset(isampler2D sampler, vec4 P, float lod, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureProjLodOffset(usampler2D sampler, vec4 P, float lod, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureProjLodOffset(sampler3D sampler, vec4 P, float lod, ivec3 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureProjLodOffset(isampler3D sampler, vec4 P, float lod, ivec3 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureProjLodOffset(usampler3D sampler, vec4 P, float lod, ivec3 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public float textureProjLodOffset(sampler1DShadow sampler, vec4 P, float lod, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="lod"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public float textureProjLodOffset(sampler2DShadow sampler, vec4 P, float lod, ivec2 offset) => new();
        #endregion
        #region textureProjOffset
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureProjOffset(sampler1D sampler, vec2 P, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureProjOffset(sampler1D sampler, vec2 P, int offset, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureProjOffset(isampler1D sampler, vec2 P, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureProjOffset(isampler1D sampler, vec2 P, int offset, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureProjOffset(usampler1D sampler, vec2 P, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureProjOffset(usampler1D sampler, vec2 P, int offset, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureProjOffset(sampler1D sampler, vec4 P, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureProjOffset(sampler1D sampler, vec4 P, int offset, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureProjOffset(isampler1D sampler, vec4 P, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureProjOffset(isampler1D sampler, vec4 P, int offset, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureProjOffset(usampler1D sampler, vec4 P, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureProjOffset(usampler1D sampler, vec4 P, int offset, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureProjOffset(sampler2D sampler, vec3 P, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureProjOffset(sampler2D sampler, vec3 P, ivec2 offset, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureProjOffset(isampler2D sampler, ivec2 offset, vec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureProjOffset(isampler2D sampler, ivec2 offset, vec3 P, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureProjOffset(usampler2D sampler, vec3 P, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureProjOffset(usampler2D sampler, vec3 P, ivec2 offset, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureProjOffset(sampler2D sampler, vec4 P, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureProjOffset(sampler2D sampler, vec4 P, ivec2 offset, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureProjOffset(isampler2D sampler, vec4 P, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureProjOffset(isampler2D sampler, vec4 P, ivec2 offset, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureProjOffset(usampler2D sampler, vec4 P, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureProjOffset(usampler2D sampler, vec4 P, ivec2 offset, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureProjOffset(sampler3D sampler, vec4 P, ivec3 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureProjOffset(sampler3D sampler, vec4 P, ivec3 offset, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureProjOffset(isampler3D sampler, vec4 P, ivec3 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureProjOffset(isampler3D sampler, vec4 P, ivec3 offset, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureProjOffset(usampler3D sampler, vec4 P, ivec3 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureProjOffset(usampler3D sampler, vec4 P, ivec3 offset, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public float textureProjOffset(sampler1DShadow sampler, vec4 P, int offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public float textureProjOffset(sampler1DShadow sampler, vec4 P, int offset, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public float textureProjOffset(sampler2DShadow sampler, vec4 P, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="bias"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public float textureProjOffset(sampler2DShadow sampler, vec4 P, ivec2 offset, float bias) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureProjOffset(sampler2DRect sampler, vec3 P, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureProjOffset(isampler2DRect sampler, vec3 P, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureProjOffset(usampler2DRect sampler, vec3 P, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public vec4 textureProjOffset(sampler2DRect sampler, vec4 P, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ivec4 textureProjOffset(isampler2DRect sampler, vec4 P, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public uvec4 textureProjOffset(usampler2DRect sampler, vec4 P, ivec2 offset) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public float textureProjOffset(sampler2DRectShadow sampler, vec4 P, ivec2 offset) => new();
        #endregion
        #region textureQueryLevels
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureQueryLevels(sampler1D sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureQueryLevels(isampler1D sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureQueryLevels(usampler1D sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureQueryLevels(sampler2D sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureQueryLevels(isampler2D sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureQueryLevels(usampler2D sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureQueryLevels(sampler3D sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureQueryLevels(isampler3D sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureQueryLevels(usampler3D sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureQueryLevels(samplerCube sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureQueryLevels(isamplerCube sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureQueryLevels(usamplerCube sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureQueryLevels(sampler1DArray sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureQueryLevels(isampler1DArray sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureQueryLevels(usampler1DArray sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureQueryLevels(sampler2DArray sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureQueryLevels(isampler2DArray sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureQueryLevels(usampler2DArray sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureQueryLevels(samplerCubeArray sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureQueryLevels(isamplerCubeArray sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureQueryLevels(usamplerCubeArray sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureQueryLevels(sampler1DShadow sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureQueryLevels(sampler2DShadow sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureQueryLevels(samplerCubeShadow sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureQueryLevels(sampler1DArrayShadow sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureQueryLevels(sampler2DArrayShadow sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureQueryLevels(samplerCubeArrayShadow sampler) => 0;
        #endregion
        #region textureQueryLod
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec2 textureQueryLod(sampler1D sampler, float P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec2 textureQueryLod(isampler1D sampler, float P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec2 textureQueryLod(usampler1D sampler, float P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec2 textureQueryLod(sampler2D sampler, vec2 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec2 textureQueryLod(isampler2D sampler, vec2 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec2 textureQueryLod(usampler2D sampler, vec2 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec2 textureQueryLod(sampler3D sampler, vec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec2 textureQueryLod(isampler3D sampler, vec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec2 textureQueryLod(usampler3D sampler, vec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec2 textureQueryLod(samplerCube sampler, vec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec2 textureQueryLod(isamplerCube sampler, vec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec2 textureQueryLod(usamplerCube sampler, vec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec2 textureQueryLod(sampler1DArray sampler, float P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec2 textureQueryLod(isampler1DArray sampler, float P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec2 textureQueryLod(usampler1DArray sampler, float P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec2 textureQueryLod(sampler2DArray sampler, vec2 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec2 textureQueryLod(isampler2DArray sampler, vec2 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec2 textureQueryLod(usampler2DArray sampler, vec2 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec2 textureQueryLod(samplerCubeArray sampler, vec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec2 textureQueryLod(isamplerCubeArray sampler, vec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec2 textureQueryLod(usamplerCubeArray sampler, vec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec2 textureQueryLod(sampler1DShadow sampler, float P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec2 textureQueryLod(sampler2DShadow sampler, vec2 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec2 textureQueryLod(samplerCubeShadow sampler, vec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec2 textureQueryLod(sampler1DArrayShadow sampler, float P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec2 textureQueryLod(sampler2DArrayShadow sampler, vec2 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec2 textureQueryLod(samplerCubeArrayShadow sampler, vec3 P) => new();
        #endregion
        #region textureSamples
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureSamples(sampler2DMS sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureSamples(isampler2DMS sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureSamples(usampler2DMS sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureSamples(sampler2DMSArray sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureSamples(isampler2DMSArray sampler) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public int textureSamples(usampler2DMSArray sampler) => 0;
        #endregion
    }
}
