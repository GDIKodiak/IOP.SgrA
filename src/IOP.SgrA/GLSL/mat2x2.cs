﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    /// <summary>
    /// 
    /// </summary>
    public struct mat2x2
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public vec2 this[int column]
        {
            get => new vec2();
            set { }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="column"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        public float this[int column, int row]
        {
            get => 0;
            set { }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static mat2x2 operator ++(mat2x2 left) => new mat2x2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static mat2x2 operator --(mat2x2 left) => new mat2x2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static mat2x2 operator +(mat2x2 left, mat2x2 right) => new mat2x2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static mat2x2 operator -(mat2x2 left, mat2x2 right) => new mat2x2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static mat2x2 operator *(mat2x2 left, mat2x2 right) => new mat2x2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec2 operator *(mat2x2 left, vec2 right) => new vec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static mat3x2 operator *(mat2x2 left, mat3x2 right) => new mat3x2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static mat4x2 operator *(mat2x2 left, mat4x2 right) => new mat4x2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static mat2x2 operator *(mat2x2 left, float right) => new mat2x2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static mat2x2 operator *(mat2x2 left, int right) => new mat2x2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static mat2x2 operator *(mat2x2 left, uint right) => new mat2x2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static mat2x2 operator /(mat2x2 left, mat2x2 right) => new mat2x2();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        public static implicit operator mat2(mat2x2 left) => new mat2();
    }
}
