﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    /// <summary>
    /// 
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 4, Pack = 1)]
    public struct bvec4
    {
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public bool x;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(1)]
        public bool y;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(2)]
        public bool z;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(3)]
        public bool w;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public bool r;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(1)]
        public bool g;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(2)]
        public bool b;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(3)]
        public bool a;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="w"></param>
        public bvec4(bool x, bool y, bool z, bool w)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
            r = x; g = y; b = z; a = w;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vec"></param>
        /// <param name="w"></param>
        public bvec4(bvec3 vec, bool w)
        {
            this.x = vec.x; this.y = vec.y; this.z = vec.z;
            this.w = w;
            r = x; g = y; b = z; a = w;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public bvec4(bool v)
        {
            x = y = z = w = r = g = b = a = v;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public bvec4(ivec4 v)
        {
            x = y = z = w = r = g = b = a = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public bvec4(uvec4 v)
        {
            x = y = z = w = r = g = b = a = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public bvec4(dvec4 v)
        {
            x = y = z = w = r = g = b = a = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public bvec4(vec4 v)
        {
            x = y = z = w = r = g = b = a = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static bvec4 operator ++(bvec4 left) => new bvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static bvec4 operator --(bvec4 left) => new bvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bvec4 operator +(bvec4 left, bvec4 right) => new bvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bvec4 operator -(bvec4 left, bvec4 right) => new bvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bvec4 operator *(bvec4 left, bvec4 right) => new bvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bvec4 operator /(bvec4 left, bvec4 right) => new bvec4();

        #region bvec2
        /// <summary>
        ///
        /// </summary>
        public bvec2 xx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 xy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 xz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 xw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 yx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 yy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 yz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 yw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 zx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 zy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 zz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 zw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 wx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 wy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 wz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 ww { get => new(); set { } }

        /// <summary>
        ///
        /// </summary>
        public bvec2 rr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 rg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 rb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 ra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 gr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 gg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 gb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 ga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 br { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 bg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 bb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 ba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 ar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 ag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 ab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec2 aa { get => new(); set { } }
        #endregion

        #region bvec3
        /// <summary>
        ///
        /// </summary>
        public bvec3 xxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 xxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 xxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 xxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 xyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 xyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 xyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 xyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 xzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 xzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 xzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 xzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 xwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 xwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 xwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 xww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 yxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 yxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 yxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 yxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 yyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 yyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 yyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 yyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 yzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 yzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 yzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 yzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 ywx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 ywy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 ywz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 yww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 zxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 zxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 zxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 zxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 zyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 zyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 zyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 zyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 zzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 zzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 zzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 zzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 zwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 zwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 zwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 zww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 wxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 wxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 wxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 wxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 wyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 wyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 wyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 wyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 wzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 wzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 wzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 wzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 wwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 wwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 wwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 www { get => new(); set { } }

        /// <summary>
        ///
        /// </summary>
        public bvec3 rrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 rrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 rrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 rra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 rgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 rgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 rgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 rga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 rbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 rbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 rbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 rba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 rar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 rag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 rab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 raa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 grr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 grg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 grb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 gra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 ggr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 ggg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 ggb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 gga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 gbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 gbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 gbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 gba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 gar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 gag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 gab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 gaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 brr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 brg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 brb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 bra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 bgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 bgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 bgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 bga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 bbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 bbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 bbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 bba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 bar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 bag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 bab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 baa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 arr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 arg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 arb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 ara { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 agr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 agg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 agb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 aga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 abr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 abg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 abb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 aba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 aar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 aag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 aab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec3 aaa { get => new(); set { } }
        #endregion

        #region bvec4
        /// <summary>
        ///
        /// </summary>
        public bvec4 xxxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xxxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xxxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xxxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xxyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xxyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xxyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xxyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xxzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xxzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xxzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xxzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xxwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xxwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xxwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xxww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xyxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xyxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xyxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xyxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xyyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xyyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xyyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xyyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xyzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xyzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xyzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xyzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xywx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xywy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xywz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xyww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xzxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xzxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xzxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xzxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xzyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xzyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xzyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xzyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xzzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xzzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xzzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xzzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xzwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xzwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xzwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xzww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xwxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xwxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xwxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xwxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xwyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xwyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xwyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xwyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xwzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xwzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xwzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xwzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xwwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xwwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xwwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 xwww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yxxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yxxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yxxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yxxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yxyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yxyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yxyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yxyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yxzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yxzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yxzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yxzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yxwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yxwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yxwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yxww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yyxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yyxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yyxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yyxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yyyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yyyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yyyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yyyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yyzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yyzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yyzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yyzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yywx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yywy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yywz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yyww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yzxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yzxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yzxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yzxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yzyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yzyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yzyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yzyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yzzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yzzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yzzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yzzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yzwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yzwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yzwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 yzww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ywxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ywxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ywxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ywxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ywyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ywyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ywyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ywyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ywzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ywzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ywzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ywzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ywwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ywwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ywwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ywww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zxxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zxxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zxxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zxxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zxyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zxyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zxyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zxyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zxzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zxzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zxzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zxzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zxwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zxwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zxwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zxww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zyxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zyxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zyxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zyxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zyyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zyyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zyyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zyyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zyzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zyzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zyzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zyzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zywx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zywy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zywz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zyww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zzxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zzxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zzxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zzxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zzyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zzyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zzyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zzyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zzzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zzzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zzzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zzzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zzwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zzwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zzwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zzww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zwxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zwxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zwxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zwxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zwyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zwyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zwyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zwyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zwzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zwzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zwzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zwzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zwwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zwwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zwwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 zwww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wxxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wxxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wxxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wxxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wxyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wxyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wxyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wxyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wxzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wxzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wxzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wxzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wxwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wxwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wxwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wxww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wyxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wyxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wyxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wyxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wyyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wyyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wyyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wyyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wyzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wyzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wyzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wyzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wywx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wywy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wywz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wyww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wzxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wzxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wzxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wzxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wzyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wzyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wzyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wzyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wzzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wzzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wzzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wzzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wzwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wzwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wzwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wzww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wwxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wwxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wwxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wwxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wwyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wwyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wwyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wwyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wwzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wwzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wwzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wwzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wwwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wwwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wwwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 wwww { get => new(); set { } }

        /// <summary>
        ///
        /// </summary>
        public bvec4 rrrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rrrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rrrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rrra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rrgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rrgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rrgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rrga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rrbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rrbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rrbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rrba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rrar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rrag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rrab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rraa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rgrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rgrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rgrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rgra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rggr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rggg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rggb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rgga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rgbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rgbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rgbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rgba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rgar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rgag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rgab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rgaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rbrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rbrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rbrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rbra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rbgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rbgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rbgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rbga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rbbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rbbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rbbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rbba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rbar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rbag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rbab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rbaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rarr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rarg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rarb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rara { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ragr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ragg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ragb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 raga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rabr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rabg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 rabb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 raba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 raar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 raag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 raab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 raaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 grrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 grrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 grrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 grra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 grgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 grgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 grgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 grga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 grbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 grbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 grbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 grba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 grar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 grag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 grab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 graa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ggrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ggrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ggrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ggra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gggr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gggg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gggb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ggga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ggbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ggbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ggbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ggba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ggar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ggag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ggab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 ggaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gbrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gbrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gbrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gbra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gbgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gbgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gbgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gbga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gbbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gbbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gbbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gbba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gbar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gbag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gbab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gbaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 garr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 garg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 garb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gara { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gagr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gagg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gagb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gaga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gabr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gabg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gabb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gaba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gaar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gaag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gaab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 gaaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 brrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 brrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 brrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 brra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 brgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 brgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 brgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 brga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 brbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 brbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 brbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 brba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 brar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 brag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 brab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 braa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bgrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bgrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bgrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bgra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bggr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bggg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bggb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bgga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bgbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bgbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bgbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bgba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bgar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bgag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bgab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bgaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bbrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bbrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bbrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bbra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bbgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bbgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bbgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bbga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bbbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bbbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bbbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bbba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bbar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bbag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bbab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bbaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 barr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 barg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 barb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bara { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bagr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bagg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 bagb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 baga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 babr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 babg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 babb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 baba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 baar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 baag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 baab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 baaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 arrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 arrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 arrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 arra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 argr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 argg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 argb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 arga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 arbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 arbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 arbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 arba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 arar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 arag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 arab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 araa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 agrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 agrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 agrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 agra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 aggr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 aggg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 aggb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 agga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 agbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 agbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 agbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 agba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 agar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 agag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 agab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 agaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 abrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 abrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 abrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 abra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 abgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 abgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 abgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 abga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 abbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 abbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 abbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 abba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 abar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 abag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 abab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 abaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 aarr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 aarg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 aarb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 aara { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 aagr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 aagg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 aagb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 aaga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 aabr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 aabg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 aabb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 aaba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 aaar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 aaag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 aaab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public bvec4 aaaa { get => new(); set { } }
        #endregion
    }
}
