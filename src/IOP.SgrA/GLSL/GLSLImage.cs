﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    public partial class GLSLShaderBase
    {
        #region imageAtomicAdd
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(image1D image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(iimage1D image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(uimage1D image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(image2D image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(iimage2D image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(uimage2D image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(image3D image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(iimage3D image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(uimage3D image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(image2DRect image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(iimage2DRect image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(uimage2DRect image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(imageCube image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(iimageCube image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(uimageCube image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(imageBuffer image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(iimageBuffer image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(uimageBuffer image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(image1DArray image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(iimage1DArray image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(uimage1DArray image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(image2DArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(iimage2DArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(uimage2DArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(imageCubeArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(iimageCubeArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(uimageCubeArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(image2DMS image, ivec2 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(iimage2DMS image, ivec2 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(uimage2DMS image, ivec2 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(image2DMSArray image, ivec3 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(iimage2DMSArray image, ivec3 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAdd(uimage2DMSArray image, ivec3 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(image1D image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(iimage1D image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(uimage1D image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(image2D image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(iimage2D image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(uimage2D image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(image3D image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(iimage3D image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(uimage3D image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(image2DRect image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(iimage2DRect image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(uimage2DRect image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(imageCube image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(iimageCube image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(uimageCube image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(imageBuffer image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(iimageBuffer image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(uimageBuffer image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(image1DArray image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(iimage1DArray image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(uimage1DArray image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(image2DArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(iimage2DArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(uimage2DArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(imageCubeArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(iimageCubeArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(uimageCubeArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(image2DMS image, ivec2 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(iimage2DMS image, ivec2 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(uimage2DMS image, ivec2 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(image2DMSArray image, ivec3 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(iimage2DMSArray image, ivec3 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAdd(uimage2DMSArray image, ivec3 P, int sample, int data) => 0;
        #endregion
        #region imageAtomicAnd
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(image1D image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(iimage1D image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(uimage1D image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(image2D image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(iimage2D image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(uimage2D image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(image3D image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(iimage3D image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(uimage3D image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(image2DRect image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(iimage2DRect image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(uimage2DRect image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(imageCube image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(iimageCube image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(uimageCube image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(imageBuffer image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(iimageBuffer image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(uimageBuffer image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(image1DArray image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(iimage1DArray image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(uimage1DArray image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(image2DArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(iimage2DArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(uimage2DArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(imageCubeArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(iimageCubeArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(uimageCubeArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(image2DMS image, ivec2 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(iimage2DMS image, ivec2 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(uimage2DMS image, ivec2 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(image2DMSArray image, ivec3 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(iimage2DMSArray image, ivec3 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicAnd(uimage2DMSArray image, ivec3 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(image1D image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(iimage1D image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(uimage1D image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(image2D image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(iimage2D image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(uimage2D image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(image3D image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(iimage3D image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(uimage3D image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(image2DRect image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(iimage2DRect image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(uimage2DRect image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(imageCube image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(iimageCube image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(uimageCube image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(imageBuffer image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(iimageBuffer image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(uimageBuffer image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(image1DArray image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(iimage1DArray image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(uimage1DArray image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(image2DArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(iimage2DArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(uimage2DArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(imageCubeArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(iimageCubeArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(uimageCubeArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(image2DMS image, ivec2 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(iimage2DMS image, ivec2 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(uimage2DMS image, ivec2 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(image2DMSArray image, ivec3 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(iimage2DMSArray image, ivec3 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicAnd(uimage2DMSArray image, ivec3 P, int sample, int data) => 0;
        #endregion
        #region imageAtomicCompSwap
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(image1D image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(iimage1D image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(uimage1D image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(image2D image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(iimage2D image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(uimage2D image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(image3D image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(iimage3D image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(uimage3D image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(image2DRect image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(iimage2DRect image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(uimage2DRect image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(imageCube image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(iimageCube image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(uimageCube image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(imageBuffer image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(iimageBuffer image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(uimageBuffer image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(image1DArray image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(iimage1DArray image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(uimage1DArray image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(image2DArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(iimage2DArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(uimage2DArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(imageCubeArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(iimageCubeArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(uimageCubeArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(image2DMS image, ivec2 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(iimage2DMS image, ivec2 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(uimage2DMS image, ivec2 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(image2DMSArray image, ivec3 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(iimage2DMSArray image, ivec3 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicCompSwap(uimage2DMSArray image, ivec3 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(image1D image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(iimage1D image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(uimage1D image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(image2D image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(iimage2D image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(uimage2D image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(image3D image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(iimage3D image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(uimage3D image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(image2DRect image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(iimage2DRect image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(uimage2DRect image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(imageCube image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(iimageCube image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(uimageCube image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(imageBuffer image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(iimageBuffer image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(uimageBuffer image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(image1DArray image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(iimage1DArray image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(uimage1DArray image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(image2DArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(iimage2DArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(uimage2DArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(imageCubeArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(iimageCubeArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(uimageCubeArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(image2DMS image, ivec2 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(iimage2DMS image, ivec2 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(uimage2DMS image, ivec2 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(image2DMSArray image, ivec3 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(iimage2DMSArray image, ivec3 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicCompSwap(uimage2DMSArray image, ivec3 P, int sample, int data) => 0;
        #endregion
        #region imageAtomicExchange
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(image1D image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(iimage1D image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(uimage1D image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(image2D image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(iimage2D image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(uimage2D image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(image3D image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(iimage3D image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(uimage3D image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(image2DRect image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(iimage2DRect image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(uimage2DRect image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(imageCube image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(iimageCube image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(uimageCube image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(imageBuffer image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(iimageBuffer image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(uimageBuffer image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(image1DArray image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(iimage1DArray image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(uimage1DArray image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(image2DArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(iimage2DArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(uimage2DArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(imageCubeArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(iimageCubeArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(uimageCubeArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(image2DMS image, ivec2 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(iimage2DMS image, ivec2 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(uimage2DMS image, ivec2 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(image2DMSArray image, ivec3 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(iimage2DMSArray image, ivec3 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicExchange(uimage2DMSArray image, ivec3 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(image1D image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(iimage1D image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(uimage1D image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(image2D image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(iimage2D image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(uimage2D image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(image3D image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(iimage3D image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(uimage3D image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(image2DRect image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(iimage2DRect image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(uimage2DRect image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(imageCube image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(iimageCube image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(uimageCube image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(imageBuffer image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(iimageBuffer image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(uimageBuffer image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(image1DArray image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(iimage1DArray image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(uimage1DArray image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(image2DArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(iimage2DArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(uimage2DArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(imageCubeArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(iimageCubeArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(uimageCubeArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(image2DMS image, ivec2 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(iimage2DMS image, ivec2 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(uimage2DMS image, ivec2 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(image2DMSArray image, ivec3 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(iimage2DMSArray image, ivec3 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(uimage2DMSArray image, ivec3 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(image1D image, int P, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(iimage1D image, int P, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(uimage1D image, int P, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(image2D image, ivec2 P, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(iimage2D image, ivec2 P, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(uimage2D image, ivec2 P, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(image3D image, ivec3 P, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(iimage3D image, ivec3 P, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(uimage3D image, ivec3 P, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(image2DRect image, ivec2 P, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(iimage2DRect image, ivec2 P, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(uimage2DRect image, ivec2 P, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(imageCube image, ivec3 P, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(iimageCube image, ivec3 P, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(uimageCube image, ivec3 P, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(imageBuffer image, int P, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(iimageBuffer image, int P, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(uimageBuffer image, int P, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(image1DArray image, ivec2 P, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(iimage1DArray image, ivec2 P, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(uimage1DArray image, ivec2 P, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(image2DArray image, ivec3 P, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(iimage2DArray image, ivec3 P, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(uimage2DArray image, ivec3 P, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(imageCubeArray image, ivec3 P, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(iimageCubeArray image, ivec3 P, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(uimageCubeArray image, ivec3 P, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(image2DMS image, ivec2 P, int sample, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(iimage2DMS image, ivec2 P, int sample, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(uimage2DMS image, ivec2 P, int sample, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(image2DMSArray image, ivec3 P, int sample, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(iimage2DMSArray image, ivec3 P, int sample, float data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicExchange(uimage2DMSArray image, ivec3 P, int sample, float data) => 0;
        #endregion
        #region imageAtomicMax
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(image1D image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(iimage1D image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(uimage1D image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(image2D image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(iimage2D image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(uimage2D image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(image3D image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(iimage3D image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(uimage3D image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(image2DRect image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(iimage2DRect image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(uimage2DRect image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(imageCube image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(iimageCube image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(uimageCube image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(imageBuffer image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(iimageBuffer image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(uimageBuffer image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(image1DArray image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(iimage1DArray image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(uimage1DArray image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(image2DArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(iimage2DArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(uimage2DArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(imageCubeArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(iimageCubeArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(uimageCubeArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(image2DMS image, ivec2 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(iimage2DMS image, ivec2 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(uimage2DMS image, ivec2 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(image2DMSArray image, ivec3 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(iimage2DMSArray image, ivec3 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMax(uimage2DMSArray image, ivec3 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(image1D image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(iimage1D image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(uimage1D image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(image2D image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(iimage2D image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(uimage2D image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(image3D image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(iimage3D image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(uimage3D image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(image2DRect image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(iimage2DRect image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(uimage2DRect image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(imageCube image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(iimageCube image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(uimageCube image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(imageBuffer image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(iimageBuffer image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(uimageBuffer image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(image1DArray image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(iimage1DArray image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(uimage1DArray image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(image2DArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(iimage2DArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(uimage2DArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(imageCubeArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(iimageCubeArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(uimageCubeArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(image2DMS image, ivec2 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(iimage2DMS image, ivec2 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(uimage2DMS image, ivec2 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(image2DMSArray image, ivec3 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(iimage2DMSArray image, ivec3 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMax(uimage2DMSArray image, ivec3 P, int sample, int data) => 0;
        #endregion
        #region imageAtomicMin
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(image1D image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(iimage1D image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(uimage1D image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(image2D image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(iimage2D image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(uimage2D image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(image3D image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(iimage3D image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(uimage3D image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(image2DRect image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(iimage2DRect image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(uimage2DRect image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(imageCube image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(iimageCube image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(uimageCube image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(imageBuffer image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(iimageBuffer image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(uimageBuffer image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(image1DArray image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(iimage1DArray image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(uimage1DArray image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(image2DArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(iimage2DArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(uimage2DArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(imageCubeArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(iimageCubeArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(uimageCubeArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(image2DMS image, ivec2 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(iimage2DMS image, ivec2 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(uimage2DMS image, ivec2 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(image2DMSArray image, ivec3 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(iimage2DMSArray image, ivec3 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicMin(uimage2DMSArray image, ivec3 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(image1D image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(iimage1D image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(uimage1D image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(image2D image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(iimage2D image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(uimage2D image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(image3D image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(iimage3D image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(uimage3D image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(image2DRect image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(iimage2DRect image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(uimage2DRect image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(imageCube image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(iimageCube image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(uimageCube image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(imageBuffer image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(iimageBuffer image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(uimageBuffer image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(image1DArray image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(iimage1DArray image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(uimage1DArray image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(image2DArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(iimage2DArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(uimage2DArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(imageCubeArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(iimageCubeArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(uimageCubeArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(image2DMS image, ivec2 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(iimage2DMS image, ivec2 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(uimage2DMS image, ivec2 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(image2DMSArray image, ivec3 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(iimage2DMSArray image, ivec3 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicMin(uimage2DMSArray image, ivec3 P, int sample, int data) => 0;
        #endregion
        #region imageAtomicOr
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(image1D image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(iimage1D image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(uimage1D image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(image2D image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(iimage2D image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(uimage2D image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(image3D image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(iimage3D image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(uimage3D image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(image2DRect image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(iimage2DRect image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(uimage2DRect image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(imageCube image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(iimageCube image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(uimageCube image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(imageBuffer image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(iimageBuffer image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(uimageBuffer image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(image1DArray image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(iimage1DArray image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(uimage1DArray image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(image2DArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(iimage2DArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(uimage2DArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(imageCubeArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(iimageCubeArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(uimageCubeArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(image2DMS image, ivec2 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(iimage2DMS image, ivec2 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(uimage2DMS image, ivec2 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(image2DMSArray image, ivec3 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(iimage2DMSArray image, ivec3 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicOr(uimage2DMSArray image, ivec3 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(image1D image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(iimage1D image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(uimage1D image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(image2D image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(iimage2D image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(uimage2D image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(image3D image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(iimage3D image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(uimage3D image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(image2DRect image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(iimage2DRect image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(uimage2DRect image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(imageCube image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(iimageCube image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(uimageCube image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(imageBuffer image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(iimageBuffer image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(uimageBuffer image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(image1DArray image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(iimage1DArray image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(uimage1DArray image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(image2DArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(iimage2DArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(uimage2DArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(imageCubeArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(iimageCubeArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(uimageCubeArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(image2DMS image, ivec2 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(iimage2DMS image, ivec2 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(uimage2DMS image, ivec2 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(image2DMSArray image, ivec3 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(iimage2DMSArray image, ivec3 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicOr(uimage2DMSArray image, ivec3 P, int sample, int data) => 0;
        #endregion
        #region imageAtomicXor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(image1D image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(iimage1D image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(uimage1D image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(image2D image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(iimage2D image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(uimage2D image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(image3D image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(iimage3D image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(uimage3D image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(image2DRect image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(iimage2DRect image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(uimage2DRect image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(imageCube image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(iimageCube image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(uimageCube image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(imageBuffer image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(iimageBuffer image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(uimageBuffer image, int P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(image1DArray image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(iimage1DArray image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(uimage1DArray image, ivec2 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(image2DArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(iimage2DArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(uimage2DArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(imageCubeArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(iimageCubeArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(uimageCubeArray image, ivec3 P, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(image2DMS image, ivec2 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(iimage2DMS image, ivec2 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(uimage2DMS image, ivec2 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(image2DMSArray image, ivec3 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(iimage2DMSArray image, ivec3 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint imageAtomicXor(uimage2DMSArray image, ivec3 P, int sample, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(image1D image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(iimage1D image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(uimage1D image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(image2D image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(iimage2D image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(uimage2D image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(image3D image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(iimage3D image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(uimage3D image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(image2DRect image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(iimage2DRect image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(uimage2DRect image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(imageCube image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(iimageCube image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(uimageCube image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(imageBuffer image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(iimageBuffer image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(uimageBuffer image, int P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(image1DArray image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(iimage1DArray image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(uimage1DArray image, ivec2 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(image2DArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(iimage2DArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(uimage2DArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(imageCubeArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(iimageCubeArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(uimageCubeArray image, ivec3 P, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(image2DMS image, ivec2 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(iimage2DMS image, ivec2 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(uimage2DMS image, ivec2 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(image2DMSArray image, ivec3 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(iimage2DMSArray image, ivec3 P, int sample, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int imageAtomicXor(uimage2DMSArray image, ivec3 P, int sample, int data) => 0;
        #endregion
        #region imageLoad
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec4 imageLoad(image1D image, int P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public ivec4 imageLoad(iimage1D image, int P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public uvec4 imageLoad(uimage1D image, int P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec4 imageLoad(image2D image, ivec2 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public ivec4 imageLoad(iimage2D image, ivec2 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public uvec4 imageLoad(uimage2D image, ivec2 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec4 imageLoad(image3D image, ivec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public ivec4 imageLoad(iimage3D image, ivec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public uvec4 imageLoad(uimage3D image, ivec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec4 imageLoad(image2DRect image, ivec2 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public ivec4 imageLoad(iimage2DRect image, ivec2 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public uvec4 imageLoad(uimage2DRect image, ivec2 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec4 imageLoad(imageCube image, ivec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public ivec4 imageLoad(iimageCube image, ivec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public uvec4 imageLoad(uimageCube image, ivec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec4 imageLoad(imageBuffer image, int P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public ivec4 imageLoad(iimageBuffer image, int P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public uvec4 imageLoad(uimageBuffer image, int P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec4 imageLoad(image1DArray image, ivec2 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public ivec4 imageLoad(iimage1DArray image, ivec2 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public uvec4 imageLoad(uimage1DArray image, ivec2 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec4 imageLoad(image2DArray image, ivec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public ivec4 imageLoad(iimage2DArray image, ivec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public uvec4 imageLoad(uimage2DArray image, ivec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public vec4 imageLoad(imageCubeArray image, ivec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public ivec4 imageLoad(iimageCubeArray image, ivec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public uvec4 imageLoad(uimageCubeArray image, ivec3 P) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <returns></returns>
        public vec4 imageLoad(image2DMS image, ivec2 P, int sample) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <returns></returns>
        public ivec4 imageLoad(iimage2DMS image, ivec2 P, int sample) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <returns></returns>
        public uvec4 imageLoad(uimage2DMS image, ivec2 P, int sample) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <returns></returns>
        public vec4 imageLoad(image2DMSArray image, ivec3 P, int sample) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <returns></returns>
        public ivec4 imageLoad(iimage2DMSArray image, ivec3 P, int sample) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <returns></returns>
        public uvec4 imageLoad(uimage2DMSArray image, ivec3 P, int sample) => new();
        #endregion
        #region imageSamples
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public int imageSamples(image2DMS image) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public int imageSamples(iimage2DMS image) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public int imageSamples(uimage2DMS image) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public int imageSamples(image2DMSArray image) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public int imageSamples(iimage2DMSArray image) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public int imageSamples(uimage2DMSArray image) => 0;
        #endregion
        #region imageSize
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public int imageSize(image1D image) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public int imageSize(iimage1D image) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public int imageSize(uimage1D image) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public ivec2 imageSize(image2D image) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public ivec2 imageSize(iimage2D image) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public ivec2 imageSize(uimage2D image) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public ivec3 imageSize(image3D image) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public ivec3 imageSize(iimage3D image) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public ivec3 imageSize(uimage3D image) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public ivec2 imageSize(imageCube image) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public ivec2 imageSize(iimageCube image) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public ivec2 imageSize(uimageCube image) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public ivec3 imageSize(imageCubeArray image) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public ivec3 imageSize(iimageCubeArray image) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public ivec3 imageSize(uimageCubeArray image) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public ivec2 imageSize(image2DRect image) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public ivec2 imageSize(iimage2DRect image) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public ivec2 imageSize(uimage2DRect image) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public ivec2 imageSize(image1DArray image) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public ivec2 imageSize(iimage1DArray image) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public ivec2 imageSize(uimage1DArray image) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public ivec3 imageSize(image2DArray image) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public ivec3 imageSize(iimage2DArray image) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public ivec3 imageSize(uimage2DArray image) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public int imageSize(imageBuffer image) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public int imageSize(iimageBuffer image) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public int imageSize(uimageBuffer image) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public ivec2 imageSize(image2DMS image) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public ivec2 imageSize(iimage2DMS image) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public ivec2 imageSize(uimage2DMS image) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public ivec3 imageSize(image2DMSArray image) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public ivec3 imageSize(iimage2DMSArray image) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public ivec3 imageSize(uimage2DMSArray image) => new();
        #endregion
        #region imageStore
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        public void imageStore(image1D image, int P, vec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        public void imageStore(iimage1D image, int P, ivec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        public void imageStore(uimage1D image, int P, uvec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        public void imageStore(image2D image, ivec2 P, vec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        public void imageStore(iimage2D image, ivec2 P, ivec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        public void imageStore(uimage2D image, ivec2 P, uvec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        public void imageStore(image3D image, ivec3 P, vec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        public void imageStore(iimage3D image, ivec3 P, ivec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        public void imageStore(uimage3D image, ivec3 P, uvec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        public void imageStore(image2DRect image, ivec2 P, vec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        public void imageStore(iimage2DRect image, ivec2 P, ivec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        public void imageStore(uimage2DRect image, ivec2 P, uvec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        public void imageStore(imageCube image, ivec3 P, vec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        public void imageStore(iimageCube image, ivec3 P, ivec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        public void imageStore(uimageCube image, ivec3 P, uvec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        public void imageStore(imageBuffer image, int P, vec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        public void imageStore(iimageBuffer image, int P, ivec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        public void imageStore(uimageBuffer image, int P, uvec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        public void imageStore(image1DArray image, ivec2 P, vec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        public void imageStore(iimage1DArray image, ivec2 P, ivec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        public void imageStore(uimage1DArray image, ivec2 P, uvec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        public void imageStore(image2DArray image, ivec3 P, vec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        public void imageStore(iimage2DArray image, ivec3 P, ivec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        public void imageStore(uimage2DArray image, ivec3 P, uvec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        public void imageStore(imageCubeArray image, ivec3 P, vec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        public void imageStore(iimageCubeArray image, ivec3 P, ivec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="data"></param>
        public void imageStore(uimageCubeArray image, ivec3 P, uvec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        public void imageStore(image2DMS image, ivec2 P, int sample, vec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        public void imageStore(iimage2DMS image, ivec2 P, int sample, ivec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        public void imageStore(uimage2DMS image, ivec2 P, int sample, uvec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        public void imageStore(image2DMSArray image, ivec3 P, int sample, vec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        public void imageStore(iimage2DMSArray image, ivec3 P, int sample, ivec4 data) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="P"></param>
        /// <param name="sample"></param>
        /// <param name="data"></param>
        public void imageStore(uimage2DMSArray image, ivec3 P, int sample, uvec4 data) { }
        #endregion
    }
}
