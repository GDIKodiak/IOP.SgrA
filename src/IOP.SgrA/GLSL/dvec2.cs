﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    /// <summary>
    /// 
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 16, Pack = 8)]
    public struct dvec2
    {
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public double x;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(8)]
        public double y;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public double r;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(8)]
        public double g;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public dvec2(double x, double y)
        {
            this.x = x;
            this.y = y;
            r = x;
            g = y;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public dvec2(double v)
        {
            x = y = r = g = v;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public dvec2(ivec2 v)
        {
            x = y = r = g = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public dvec2(uvec2 v)
        {
            x = y = r = g = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public dvec2(vec2 v)
        {
            x = y = r = g = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public dvec2(bvec2 v)
        {
            x = y = r = g = 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static dvec2 operator ++(dvec2 left) => new dvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static dvec2 operator --(dvec2 left) => new dvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec2 operator +(dvec2 left, dvec2 right) => new dvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec2 operator -(dvec2 left, dvec2 right) => new dvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec2 operator *(dvec2 left, dvec2 right) => new dvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec2 operator /(dvec2 left, dvec2 right) => new dvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec2 operator +(dvec2 left, double right) => new dvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec2 operator +(double left, dvec2 right) => new dvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec2 operator -(dvec2 left, double right) => new dvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec2 operator -(double left, dvec2 right) => new dvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static dvec2 operator -(dvec2 left) => new dvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec2 operator *(dvec2 left, double right) => new dvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec2 operator *(double left, dvec2 right) => new dvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec2 operator /(dvec2 left, double right) => new dvec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec2 operator /(double left, dvec2 right) => new dvec2();

        #region dvec2
        /// <summary>
        ///
        /// </summary>
        public dvec2 xx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 xy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 yx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 yy { get => new(); set { } }

        /// <summary>
        ///
        /// </summary>
        public dvec2 rr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 rg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 gr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 gg { get => new(); set { } }
        #endregion
    }
}
