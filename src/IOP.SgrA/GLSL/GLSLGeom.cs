﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    /// <summary>
    /// GLSL几何着色器
    /// </summary>
    public abstract class GLSLGeom : GLSLShaderBase, IGeomShader
    {
        /// <summary>
        /// 
        /// </summary>
        public abstract void main();

        /// <summary>
        /// 
        /// </summary>
        public int gl_PrimitiveID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public vec4 gl_Position {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public float gl_PointSize {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public float[] gl_ClipDistance {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public float[] gl_CullDistance {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public gl_PerVertex[] gl_in { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int gl_Layer {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int gl_PrimitiveIDIn {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int gl_ViewportIndex {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        public virtual void EmitStreamVertex(int stream) { }
        /// <summary>
        /// 
        /// </summary>
        public virtual void EmitVertex() { }
        /// <summary>
        /// 
        /// </summary>
        public virtual void EndPrimitive() { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        public virtual void EndStreamPrimitive(int stream) { }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual string GetCode() => string.Empty;
    }
}
