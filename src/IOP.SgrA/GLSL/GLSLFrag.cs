﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    /// <summary>
    /// GLSL片元着色器
    /// </summary>
    public abstract class GLSLFrag : GLSLShaderBase, IFragShader
    {
        /// <summary>
        /// 
        /// </summary>
        public virtual vec4 gl_FragCoord { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual float gl_FragDepth {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual vec2 gl_PointCoord {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual bool gl_FrontFacing {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual bool gl_HelperInvocation {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual int gl_Layer {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual int gl_NumSamples {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual int gl_PrimitiveID {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual int gl_SampleID {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual int[] gl_SampleMask {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual int[] gl_SampleMaskIn {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual vec2 gl_SamplePosition {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual int gl_ViewportIndex {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual void discard() { }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual string GetCode() => string.Empty;

        /// <summary>
        /// 
        /// </summary>
        public abstract void main();

        #region subpass
        /// <summary>
        /// 
        /// </summary>
        /// <param name="subpass"></param>
        /// <returns></returns>
        public virtual vec4 subpassLoad(subpassInput subpass) => new();
        #endregion
        #region fwidth
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public float fwidth(float p) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public vec2 fwidth(vec2 p) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public vec3 fwidth(vec3 p) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public vec4 fwidth(vec4 p) => new();
        #endregion
        #region fwidthCoarse
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public float fwidthCoarse(float p) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public vec2 fwidthCoarse(vec2 p) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public vec3 fwidthCoarse(vec3 p) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public vec4 fwidthCoarse(vec4 p) => new();
        #endregion
        #region fwidthFine
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public float fwidthFine(float p) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public vec2 fwidthFine(vec2 p) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public vec3 fwidthFine(vec3 p) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public vec4 fwidthFine(vec4 p) => new();
        #endregion
    }
}
