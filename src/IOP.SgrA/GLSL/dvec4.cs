﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    /// <summary>
    /// 
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 32, Pack = 8)]
    public struct dvec4
    {
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public double x;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(8)]
        public double y;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(16)]
        public double z;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(24)]
        public double w;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public double r;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(8)]
        public double g;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(16)]
        public double b;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(24)]
        public double a;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="w"></param>
        public dvec4(double x, double y, double z, double w)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
            r = x; g = y; b = z; a = w;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public dvec4(double v)
        {
            x = y = z = w = r = g = b = a = v;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vec"></param>
        /// <param name="w"></param>
        public dvec4(dvec3 vec, double w)
        {
            this.x = vec.x; this.y = vec.y; this.z = vec.z;
            this.w = w;
            r = x; g = y; b = z; a = w;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public dvec4(ivec4 v)
        {
            x = y = z = w = r = g = b = a = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public dvec4(uvec4 v)
        {
            x = y = z = w = r = g = b = a = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public dvec4(vec4 v)
        {
            x = y = z = w = r = g = b = a = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public dvec4(bvec4 v)
        {
            x = y = z = w = r = g = b = a = 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static dvec4 operator ++(dvec4 left) => new dvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static dvec4 operator --(dvec4 left) => new dvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec4 operator +(dvec4 left, dvec4 right) => new dvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec4 operator -(dvec4 left, dvec4 right) => new dvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec4 operator *(dvec4 left, dvec4 right) => new dvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec4 operator /(dvec4 left, dvec4 right) => new dvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec4 operator +(dvec4 left, double right) => new dvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec4 operator +(double left, dvec4 right) => new dvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec4 operator -(dvec4 left, double right) => new dvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec4 operator -(double left, dvec4 right) => new dvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static dvec4 operator -(dvec4 left) => new dvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec4 operator *(dvec4 left, double right) => new dvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec4 operator *(double left, dvec4 right) => new dvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec4 operator /(dvec4 left, double right) => new dvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec4 operator /(double left, dvec4 right) => new dvec4();

        #region dvec2
        /// <summary>
        ///
        /// </summary>
        public dvec2 xx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 xy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 xz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 xw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 yx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 yy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 yz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 yw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 zx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 zy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 zz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 zw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 wx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 wy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 wz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 ww { get => new(); set { } }

        /// <summary>
        ///
        /// </summary>
        public dvec2 rr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 rg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 rb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 ra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 gr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 gg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 gb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 ga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 br { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 bg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 bb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 ba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 ar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 ag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 ab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec2 aa { get => new(); set { } }
        #endregion

        #region dvec3
        /// <summary>
        ///
        /// </summary>
        public dvec3 xxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 xxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 xxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 xxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 xyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 xyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 xyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 xyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 xzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 xzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 xzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 xzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 xwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 xwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 xwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 xww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 yxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 yxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 yxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 yxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 yyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 yyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 yyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 yyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 yzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 yzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 yzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 yzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 ywx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 ywy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 ywz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 yww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 zxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 zxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 zxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 zxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 zyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 zyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 zyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 zyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 zzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 zzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 zzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 zzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 zwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 zwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 zwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 zww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 wxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 wxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 wxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 wxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 wyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 wyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 wyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 wyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 wzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 wzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 wzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 wzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 wwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 wwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 wwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 www { get => new(); set { } }

        /// <summary>
        ///
        /// </summary>
        public dvec3 rrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 rrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 rrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 rra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 rgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 rgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 rgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 rga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 rbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 rbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 rbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 rba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 rar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 rag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 rab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 raa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 grr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 grg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 grb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 gra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 ggr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 ggg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 ggb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 gga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 gbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 gbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 gbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 gba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 gar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 gag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 gab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 gaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 brr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 brg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 brb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 bra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 bgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 bgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 bgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 bga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 bbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 bbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 bbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 bba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 bar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 bag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 bab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 baa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 arr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 arg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 arb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 ara { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 agr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 agg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 agb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 aga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 abr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 abg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 abb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 aba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 aar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 aag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 aab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec3 aaa { get => new(); set { } }
        #endregion

        #region dvec4
        /// <summary>
        ///
        /// </summary>
        public dvec4 xxxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xxxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xxxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xxxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xxyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xxyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xxyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xxyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xxzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xxzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xxzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xxzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xxwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xxwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xxwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xxww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xyxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xyxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xyxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xyxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xyyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xyyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xyyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xyyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xyzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xyzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xyzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xyzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xywx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xywy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xywz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xyww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xzxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xzxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xzxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xzxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xzyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xzyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xzyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xzyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xzzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xzzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xzzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xzzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xzwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xzwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xzwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xzww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xwxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xwxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xwxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xwxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xwyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xwyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xwyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xwyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xwzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xwzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xwzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xwzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xwwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xwwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xwwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 xwww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yxxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yxxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yxxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yxxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yxyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yxyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yxyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yxyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yxzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yxzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yxzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yxzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yxwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yxwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yxwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yxww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yyxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yyxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yyxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yyxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yyyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yyyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yyyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yyyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yyzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yyzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yyzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yyzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yywx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yywy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yywz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yyww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yzxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yzxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yzxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yzxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yzyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yzyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yzyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yzyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yzzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yzzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yzzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yzzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yzwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yzwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yzwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 yzww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ywxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ywxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ywxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ywxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ywyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ywyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ywyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ywyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ywzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ywzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ywzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ywzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ywwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ywwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ywwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ywww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zxxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zxxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zxxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zxxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zxyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zxyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zxyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zxyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zxzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zxzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zxzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zxzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zxwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zxwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zxwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zxww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zyxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zyxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zyxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zyxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zyyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zyyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zyyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zyyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zyzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zyzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zyzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zyzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zywx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zywy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zywz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zyww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zzxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zzxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zzxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zzxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zzyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zzyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zzyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zzyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zzzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zzzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zzzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zzzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zzwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zzwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zzwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zzww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zwxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zwxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zwxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zwxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zwyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zwyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zwyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zwyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zwzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zwzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zwzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zwzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zwwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zwwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zwwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 zwww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wxxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wxxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wxxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wxxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wxyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wxyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wxyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wxyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wxzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wxzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wxzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wxzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wxwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wxwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wxwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wxww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wyxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wyxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wyxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wyxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wyyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wyyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wyyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wyyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wyzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wyzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wyzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wyzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wywx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wywy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wywz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wyww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wzxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wzxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wzxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wzxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wzyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wzyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wzyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wzyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wzzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wzzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wzzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wzzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wzwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wzwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wzwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wzww { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wwxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wwxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wwxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wwxw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wwyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wwyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wwyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wwyw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wwzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wwzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wwzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wwzw { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wwwx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wwwy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wwwz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 wwww { get => new(); set { } }

        /// <summary>
        ///
        /// </summary>
        public dvec4 rrrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rrrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rrrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rrra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rrgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rrgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rrgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rrga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rrbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rrbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rrbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rrba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rrar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rrag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rrab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rraa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rgrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rgrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rgrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rgra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rggr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rggg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rggb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rgga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rgbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rgbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rgbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rgba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rgar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rgag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rgab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rgaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rbrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rbrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rbrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rbra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rbgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rbgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rbgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rbga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rbbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rbbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rbbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rbba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rbar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rbag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rbab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rbaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rarr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rarg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rarb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rara { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ragr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ragg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ragb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 raga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rabr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rabg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 rabb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 raba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 raar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 raag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 raab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 raaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 grrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 grrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 grrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 grra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 grgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 grgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 grgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 grga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 grbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 grbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 grbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 grba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 grar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 grag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 grab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 graa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ggrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ggrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ggrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ggra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gggr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gggg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gggb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ggga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ggbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ggbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ggbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ggba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ggar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ggag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ggab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 ggaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gbrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gbrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gbrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gbra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gbgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gbgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gbgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gbga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gbbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gbbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gbbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gbba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gbar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gbag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gbab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gbaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 garr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 garg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 garb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gara { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gagr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gagg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gagb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gaga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gabr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gabg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gabb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gaba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gaar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gaag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gaab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 gaaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 brrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 brrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 brrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 brra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 brgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 brgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 brgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 brga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 brbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 brbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 brbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 brba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 brar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 brag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 brab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 braa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bgrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bgrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bgrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bgra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bggr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bggg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bggb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bgga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bgbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bgbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bgbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bgba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bgar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bgag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bgab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bgaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bbrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bbrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bbrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bbra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bbgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bbgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bbgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bbga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bbbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bbbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bbbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bbba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bbar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bbag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bbab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bbaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 barr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 barg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 barb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bara { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bagr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bagg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 bagb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 baga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 babr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 babg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 babb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 baba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 baar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 baag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 baab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 baaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 arrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 arrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 arrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 arra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 argr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 argg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 argb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 arga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 arbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 arbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 arbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 arba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 arar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 arag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 arab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 araa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 agrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 agrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 agrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 agra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 aggr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 aggg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 aggb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 agga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 agbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 agbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 agbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 agba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 agar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 agag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 agab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 agaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 abrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 abrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 abrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 abra { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 abgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 abgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 abgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 abga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 abbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 abbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 abbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 abba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 abar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 abag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 abab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 abaa { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 aarr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 aarg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 aarb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 aara { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 aagr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 aagg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 aagb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 aaga { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 aabr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 aabg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 aabb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 aaba { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 aaar { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 aaag { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 aaab { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public dvec4 aaaa { get => new(); set { } }
        #endregion
    }
}
