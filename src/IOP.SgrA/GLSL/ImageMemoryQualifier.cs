﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    /// <summary>
    /// 
    /// </summary>
    public enum ImageMemoryQualifier
    {
        /// <summary>
        /// 
        /// </summary>
        Coherent,
        /// <summary>
        /// 
        /// </summary>
        Volatile,
        /// <summary>
        /// 
        /// </summary>
        Restrict,
        /// <summary>
        /// 
        /// </summary>
        Readonly,
        /// <summary>
        /// 
        /// </summary>
        Writeonly
    }
}
