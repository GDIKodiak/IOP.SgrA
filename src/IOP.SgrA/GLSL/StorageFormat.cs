﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    /// <summary>
    /// 
    /// </summary>
    public enum StorageFormat
    {
        /// <summary>
        /// 
        /// </summary>
        rgba32f,
        /// <summary>
        /// 
        /// </summary>
        rgba16f,
        /// <summary>
        /// 
        /// </summary>
        rg32f,
        /// <summary>
        /// 
        /// </summary>
        rg16f,
        /// <summary>
        /// 
        /// </summary>
        r11f_g11f_b10f,
        /// <summary>
        /// 
        /// </summary>
        r32f,
        /// <summary>
        /// 
        /// </summary>
        r16f,
        /// <summary>
        /// 
        /// </summary>
        rgba16,
        /// <summary>
        /// 
        /// </summary>
        rgb10_a2,
        /// <summary>
        /// 
        /// </summary>
        rgba8,
        /// <summary>
        /// 
        /// </summary>
        rg16,
        /// <summary>
        /// 
        /// </summary>
        rg8,
        /// <summary>
        /// 
        /// </summary>
        r16,
        /// <summary>
        /// 
        /// </summary>
        r8,
        /// <summary>
        /// 
        /// </summary>
        rgba16_snorm,
        /// <summary>
        /// 
        /// </summary>
        rgba8_snorm,
        /// <summary>
        /// 
        /// </summary>
        rg16_snorm,
        /// <summary>
        /// 
        /// </summary>
        rg8_snorm,
        /// <summary>
        /// 
        /// </summary>
        r16_snorm,
        /// <summary>
        /// 
        /// </summary>
        r8_snorm,
        /// <summary>
        /// 
        /// </summary>
        rgba32i,
        /// <summary>
        /// 
        /// </summary>
        rgba16i,
        /// <summary>
        /// 
        /// </summary>
        rgba8i,
        /// <summary>
        /// 
        /// </summary>
        rg32i,
        /// <summary>
        /// 
        /// </summary>
        rg16i,
        /// <summary>
        /// 
        /// </summary>
        rg8i,
        /// <summary>
        /// 
        /// </summary>
        r32i,
        /// <summary>
        /// 
        /// </summary>
        r16i,
        /// <summary>
        /// 
        /// </summary>
        r8i,
        /// <summary>
        /// 
        /// </summary>
        rgba32ui,
        /// <summary>
        /// 
        /// </summary>
        rgba16ui,
        /// <summary>
        /// 
        /// </summary>
        rgb10_a2ui,
        /// <summary>
        /// 
        /// </summary>
        rgba8ui,
        /// <summary>
        /// 
        /// </summary>
        rg32ui,
        /// <summary>
        /// 
        /// </summary>
        rg16ui,
        /// <summary>
        /// 
        /// </summary>
        rg8ui,
        /// <summary>
        /// 
        /// </summary>
        r32ui,
        /// <summary>
        /// 
        /// </summary>
        r16ui,
        /// <summary>
        /// 
        /// </summary>
        r8ui
    }
}
