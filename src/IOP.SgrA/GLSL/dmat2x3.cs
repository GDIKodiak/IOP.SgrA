﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    /// <summary>
    /// 
    /// </summary>
    public struct dmat2x3
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public dvec3 this[int column]
        {
            get => new dvec3();
            set { }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="column"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        public double this[int column, int row]
        {
            get => 0;
            set { }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static dmat2x3 operator ++(dmat2x3 left) => new dmat2x3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static dmat2x3 operator --(dmat2x3 left) => new dmat2x3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dmat2x3 operator +(dmat2x3 left, dmat2x3 right) => new dmat2x3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dmat2x3 operator -(dmat2x3 left, dmat2x3 right) => new dmat2x3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dmat2x3 operator *(dmat2x3 left, dmat2x3 right) => new dmat2x3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dvec3 operator *(dmat2x3 left, dvec2 right) => new dvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dmat2x3 operator *(dmat2x3 left, dmat2 right) => new dmat2x3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dmat3 operator *(dmat2x3 left, dmat3x2 right) => new dmat3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dmat4x3 operator *(dmat2x3 left, dmat4x2 right) => new dmat4x3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dmat2x3 operator *(dmat2x3 left, double right) => new dmat2x3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static dmat2x3 operator /(dmat2x3 left, dmat2x3 right) => new dmat2x3();
    }
}
