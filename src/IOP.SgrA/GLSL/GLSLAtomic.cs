﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    public partial class GLSLShaderBase
    {
        #region atomicAdd
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mem"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int atomicAdd(int mem, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mem"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint atomicAdd(uint mem, int data) => 0;
        #endregion
        #region atomicAnd
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mem"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int atomicAnd(int mem, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mem"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint atomicAnd(uint mem, int data) => 0;
        #endregion
        #region atomicCompSwap
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mem"></param>
        /// <param name="compare"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int atomicCompSwap(int mem, uint compare, uint data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mem"></param>
        /// <param name="compare"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint atomicCompSwap(uint mem, uint compare, uint data) => 0;
        #endregion
        #region atomicCounter
        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public uint atomicCounter(atomic_uint c) => 0;
        #endregion
        #region atomicCounterDecrement
        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public uint atomicCounterDecrement(atomic_uint c) => 0;
        #endregion
        #region atomicCounterIncrement
        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public uint atomicCounterIncrement(atomic_uint c) => 0;
        #endregion
        #region atomicExchange
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mem"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int atomicExchange(int mem, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mem"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint atomicExchange(uint mem, uint data) => 0;
        #endregion
        #region atomicMax
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mem"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int atomicMax(int mem, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mem"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint atomicMax(int mem, uint data) => 0;
        #endregion
        #region atomicMin
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mem"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int atomicMin(int mem, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mem"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint atomicMin(int mem, uint data) => 0;
        #endregion
        #region atomicOr
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mem"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int atomicOr(int mem, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mem"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint atomicOr(int mem, uint data) => 0;
        #endregion
        #region atomicXor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mem"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int atomicXor(int mem, int data) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mem"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public uint atomicXor(int mem, uint data) => 0;
        #endregion
        /// <summary>
        /// 
        /// </summary>
        public void memoryBarrier() { }
        /// <summary>
        /// 
        /// </summary>
        public void memoryBarrierBuffer() { }
        /// <summary>
        /// 
        /// </summary>
        public void memoryBarrierImage() { }

    }
}
