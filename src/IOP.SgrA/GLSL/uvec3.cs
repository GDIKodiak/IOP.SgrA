﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    /// <summary>
    /// 
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 12, Pack = 4)]
    public struct uvec3
    {
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public uint x;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(4)]
        public uint y;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(8)]
        public uint z;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public uint r;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(4)]
        public uint g;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(8)]
        public uint b;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public uvec3(uint x, uint y, uint z)
        {
            this.x = x; this.y = y; this.z = z;
            r = x; g = y; b = z;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public uvec3(uint v)
        {
            x = y = z = r = g = b = v;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public uvec3(vec3 v)
        {
            x = y = z = r = g = b = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public uvec3(ivec3 v)
        {
            x = y = z = r = g = b = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public uvec3(dvec3 v)
        {
            x = y = z = r = g = b = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public uvec3(bvec3 v)
        {
            x = y = z = r = g = b = 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static uvec3 operator ++(uvec3 left) => new uvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static uvec3 operator --(uvec3 left) => new uvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec3 operator +(uvec3 left, uvec3 right) => new uvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec3 operator -(uvec3 left, uvec3 right) => new uvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec3 operator *(uvec3 left, uvec3 right) => new uvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec2 operator *(uvec3 left, mat2x3 right) => new vec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec2 operator *(uvec3 left, mat3x3 right) => new vec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec3 operator *(uvec3 left, mat3 right) => new vec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec4 operator *(uvec3 left, mat4x3 right) => new uvec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec3 operator /(uvec3 left, uvec3 right) => new uvec3();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec3 operator +(uint left, uvec3 right) => new uvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec3 operator -(uvec3 left, uint right) => new uvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec3 operator -(uint left, uvec3 right) => new uvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static uvec3 operator -(uvec3 left) => new uvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec3 operator *(uvec3 left, uint right) => new uvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec3 operator *(uint left, uvec3 right) => new uvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec3 operator /(uvec3 left, uint right) => new uvec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static uvec3 operator /(uint left, uvec3 right) => new uvec3();

        #region uvec2
        /// <summary>
        ///
        /// </summary>
        public uvec2 xx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 xy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 xz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 yx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 yy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 yz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 zx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 zy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 zz { get => new(); set { } }

        /// <summary>
        ///
        /// </summary>
        public uvec2 rr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 rg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 rb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 gr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 gg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 gb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 br { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 bg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec2 bb { get => new(); set { } }
        #endregion

        #region uvec3
        /// <summary>
        ///
        /// </summary>
        public uvec3 xxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 xxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 xxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 xyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 xyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 xyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 xzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 xzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 xzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 yxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 yxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 yxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 yyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 yyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 yyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 yzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 yzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 yzz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 zxx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 zxy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 zxz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 zyx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 zyy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 zyz { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 zzx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 zzy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 zzz { get => new(); set { } }

        /// <summary>
        ///
        /// </summary>
        public uvec3 rrr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 rrg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 rrb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 rgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 rgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 rgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 rbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 rbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 rbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 grr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 grg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 grb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 ggr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 ggg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 ggb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 gbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 gbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 gbb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 brr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 brg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 brb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 bgr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 bgg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 bgb { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 bbr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 bbg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public uvec3 bbb { get => new(); set { } }
        #endregion
    }
}
