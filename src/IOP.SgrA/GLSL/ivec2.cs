﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    /// <summary>
    /// 
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 8, Pack = 4)]
    public struct ivec2
    {
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public int x;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(4)]
        public int y;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public int r;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(4)]
        public int g;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public ivec2(int x, int y)
        {
            this.x = x;
            this.y = y;
            r = x;
            g = y;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public ivec2(int v)
        {
            x = y = r = g = v;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public ivec2(vec2 v)
        {
            x = y = r = g = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public ivec2(uvec2 v)
        {
            x = y = r = g = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public ivec2(dvec2 v)
        {
            x = y = r = g = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public ivec2(bvec2 v)
        {
            x = y = r = g = 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static ivec2 operator ++(ivec2 left) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static ivec2 operator --(ivec2 left) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec2 operator +(ivec2 left, ivec2 right) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec2 operator -(ivec2 left, ivec2 right) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec2 operator *(ivec2 left, ivec2 right) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec2 operator *(ivec2 left, mat2x2 right) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec2 operator *(ivec2 left, mat2 right) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec3 operator *(ivec2 left, mat3x2 right) => new ivec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec4 operator *(ivec2 left, mat4x2 right) => new ivec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec2 operator /(ivec2 left, ivec2 right) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec2 operator +(ivec2 left, int right) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec2 operator +(int left, ivec2 right) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec2 operator -(ivec2 left, int right) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec2 operator -(int left, ivec2 right) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static ivec2 operator -(ivec2 left) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec2 operator *(ivec2 left, int right) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec2 operator *(int left, ivec2 right) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec2 operator /(ivec2 left, int right) => new ivec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static ivec2 operator /(int left, ivec2 right) => new ivec2();

        /// <summary>
        ///
        /// </summary>
        public ivec2 xx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 xy { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 yx { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 yy { get => new(); set { } }

        /// <summary>
        ///
        /// </summary>
        public ivec2 rr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 rg { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 gr { get => new(); set { } }
        /// <summary>
        ///
        /// </summary>
        public ivec2 gg { get => new(); set { } }
    }
}
