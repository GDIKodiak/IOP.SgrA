﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    public partial class GLSLShaderBase
    {
        #region cross
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public vec3 cross(vec3 x, vec3 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public dvec3 cross(dvec3 x, dvec3 y) => new();
        #endregion
        #region distance
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p0"></param>
        /// <param name="p1"></param>
        /// <returns></returns>
        public float distance(float p0, float p1) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p0"></param>
        /// <param name="p1"></param>
        /// <returns></returns>
        public float distance(vec2 p0, vec2 p1) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p0"></param>
        /// <param name="p1"></param>
        /// <returns></returns>
        public float distance(vec3 p0, vec3 p1) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p0"></param>
        /// <param name="p1"></param>
        /// <returns></returns>
        public float distance(vec4 p0, vec4 p1) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p0"></param>
        /// <param name="p1"></param>
        /// <returns></returns>
        public double distance(double p0, double p1) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p0"></param>
        /// <param name="p1"></param>
        /// <returns></returns>
        public double distance(dvec2 p0, dvec2 p1) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p0"></param>
        /// <param name="p1"></param>
        /// <returns></returns>
        public double distance(dvec3 p0, dvec3 p1) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p0"></param>
        /// <param name="p1"></param>
        /// <returns></returns>
        public double distance(dvec4 p0, dvec4 p1) => 0;
        #endregion
        #region dot
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p0"></param>
        /// <param name="p1"></param>
        /// <returns></returns>
        public float dot(float p0, float p1) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p0"></param>
        /// <param name="p1"></param>
        /// <returns></returns>
        public float dot(vec2 p0, vec2 p1) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p0"></param>
        /// <param name="p1"></param>
        /// <returns></returns>
        public float dot(vec3 p0, vec3 p1) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p0"></param>
        /// <param name="p1"></param>
        /// <returns></returns>
        public float dot(vec4 p0, vec4 p1) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p0"></param>
        /// <param name="p1"></param>
        /// <returns></returns>
        public double dot(double p0, double p1) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p0"></param>
        /// <param name="p1"></param>
        /// <returns></returns>
        public double dot(dvec2 p0, dvec2 p1) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p0"></param>
        /// <param name="p1"></param>
        /// <returns></returns>
        public double dot(dvec3 p0, dvec3 p1) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p0"></param>
        /// <param name="p1"></param>
        /// <returns></returns>
        public double dot(dvec4 p0, dvec4 p1) => 0;
        #endregion
        #region equal
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec2 equal(vec2 x, vec2 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec3 equal(vec3 x, vec3 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec4 equal(vec4 x, vec4 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec2 equal(ivec2 x, ivec2 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec3 equal(ivec3 x, ivec3 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec4 equal(ivec4 x, ivec4 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec2 equal(uvec2 x, uvec2 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec3 equal(uvec3 x, uvec3 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec4 equal(uvec4 x, uvec4 y) => new();
        #endregion
        #region faceforward
        /// <summary>
        /// 
        /// </summary>
        /// <param name="N"></param>
        /// <param name="I"></param>
        /// <param name="Nref"></param>
        /// <returns></returns>
        public float faceforward(float N, float I, float Nref) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="N"></param>
        /// <param name="I"></param>
        /// <param name="Nref"></param>
        /// <returns></returns>
        public vec2 faceforward(vec2 N, vec2 I, vec2 Nref) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="N"></param>
        /// <param name="I"></param>
        /// <param name="Nref"></param>
        /// <returns></returns>
        public vec3 faceforward(vec3 N, vec3 I, vec3 Nref) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="N"></param>
        /// <param name="I"></param>
        /// <param name="Nref"></param>
        /// <returns></returns>
        public vec4 faceforward(vec4 N, vec4 I, vec4 Nref) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="N"></param>
        /// <param name="I"></param>
        /// <param name="Nref"></param>
        /// <returns></returns>
        public double faceforward(double N, double I, double Nref) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="N"></param>
        /// <param name="I"></param>
        /// <param name="Nref"></param>
        /// <returns></returns>
        public dvec2 faceforward(dvec2 N, dvec2 I, dvec2 Nref) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="N"></param>
        /// <param name="I"></param>
        /// <param name="Nref"></param>
        /// <returns></returns>
        public dvec3 faceforward(dvec3 N, dvec3 I, dvec3 Nref) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="N"></param>
        /// <param name="I"></param>
        /// <param name="Nref"></param>
        /// <returns></returns>
        public dvec4 faceforward(dvec4 N, dvec4 I, dvec4 Nref) => new();
        #endregion
        #region length
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public float length(float x) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public float length(vec2 x) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public float length(vec3 x) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public float length(vec4 x) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public double length(double x) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public double length(dvec2 x) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public double length(dvec3 x) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public double length(dvec4 x) => new();
        #endregion
        #region normalize
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public float normalize(float x) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public vec2 normalize(vec2 x) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public vec3 normalize(vec3 x) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public vec4 normalize(vec4 x) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public double normalize(double x) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public dvec2 normalize(dvec2 x) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public dvec3 normalize(dvec3 x) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public dvec4 normalize(dvec4 x) => new();
        #endregion
        #region notEqual
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec2 notEqual(vec2 x, vec2 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec3 notEqual(vec3 x, vec3 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec4 notEqual(vec4 x, vec4 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec2 notEqual(ivec2 x, ivec2 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec3 notEqual(ivec3 x, ivec3 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec4 notEqual(ivec4 x, ivec4 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec2 notEqual(uvec2 x, uvec2 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec3 notEqual(uvec3 x, uvec3 y) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bvec4 notEqual(uvec4 x, uvec4 y) => new();
        #endregion
        #region reflect
        /// <summary>
        /// 
        /// </summary>
        /// <param name="I"></param>
        /// <param name="N"></param>
        /// <returns></returns>
        public float reflect(float I, float N) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="I"></param>
        /// <param name="N"></param>
        /// <returns></returns>
        public vec2 reflect(vec2 I, vec2 N) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="I"></param>
        /// <param name="N"></param>
        /// <returns></returns>
        public vec3 reflect(vec3 I, vec3 N) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="I"></param>
        /// <param name="N"></param>
        /// <returns></returns>
        public vec4 reflect(vec4 I, vec4 N) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="I"></param>
        /// <param name="N"></param>
        /// <returns></returns>
        public double reflect(double I, double N) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="I"></param>
        /// <param name="N"></param>
        /// <returns></returns>
        public dvec2 reflect(dvec2 I, dvec2 N) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="I"></param>
        /// <param name="N"></param>
        /// <returns></returns>
        public dvec3 reflect(dvec3 I, dvec3 N) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="I"></param>
        /// <param name="N"></param>
        /// <returns></returns>
        public dvec4 reflect(dvec4 I, dvec4 N) => new();
        #endregion
        #region refract
        /// <summary>
        /// 
        /// </summary>
        /// <param name="I"></param>
        /// <param name="N"></param>
        /// <param name="eta"></param>
        /// <returns></returns>
        public float refract(float I, float N, float eta) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="I"></param>
        /// <param name="N"></param>
        /// <param name="eta"></param>
        /// <returns></returns>
        public vec2 refract(vec2 I, vec2 N, float eta) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="I"></param>
        /// <param name="N"></param>
        /// <param name="eta"></param>
        /// <returns></returns>
        public vec3 refract(vec3 I, vec3 N, float eta) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="I"></param>
        /// <param name="N"></param>
        /// <param name="eta"></param>
        /// <returns></returns>
        public vec4 refract(vec4 I, vec4 N, float eta) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="I"></param>
        /// <param name="N"></param>
        /// <param name="eta"></param>
        /// <returns></returns>
        public double refract(double I, double N, float eta) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="I"></param>
        /// <param name="N"></param>
        /// <param name="eta"></param>
        /// <returns></returns>
        public dvec2 refract(dvec2 I, dvec2 N, float eta) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="I"></param>
        /// <param name="N"></param>
        /// <param name="eta"></param>
        /// <returns></returns>
        public dvec3 refract(dvec3 I, dvec3 N, float eta) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="I"></param>
        /// <param name="N"></param>
        /// <param name="eta"></param>
        /// <returns></returns>
        public dvec4 refract(dvec4 I, dvec4 N, float eta) => new();
        #endregion
    }
}
