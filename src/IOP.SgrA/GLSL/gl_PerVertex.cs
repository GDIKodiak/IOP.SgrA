﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    /// <summary>
    /// 
    /// </summary>
    public struct gl_PerVertex
    {
        /// <summary>
        /// 
        /// </summary>
        public vec4 gl_Position;
        /// <summary>
        /// 
        /// </summary>
        public float gl_PointSize;
        /// <summary>
        /// 
        /// </summary>
        public float[] gl_ClipDistance;
    }
}
