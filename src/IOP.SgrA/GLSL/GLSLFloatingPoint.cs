﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    public partial class GLSLShaderBase
    {
        #region Floating-Point
        #region floatBitsToInt
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public int floatBitsToInt(float x) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public ivec2 floatBitsToInt(vec2 x) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public ivec3 floatBitsToInt(vec3 x) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public ivec4 floatBitsToInt(vec4 x) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public uint floatBitsToUint(float x) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public uvec2 floatBitsToUint(vec2 x) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public uvec3 floatBitsToUint(vec3 x) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public uvec4 floatBitsToUint(vec4 x) => new();
        #endregion
        #region frexp
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="exp"></param>
        /// <returns></returns>
        public float frexp(float x, int exp)
        {
            exp = 0;
            return 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="exp"></param>
        /// <returns></returns>
        public vec2 frexp(vec2 x, ivec2 exp)
        {
            exp = new();
            return new();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="exp"></param>
        /// <returns></returns>
        public vec3 frexp(vec3 x, ivec3 exp)
        {
            exp = new();
            return new();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="exp"></param>
        /// <returns></returns>
        public vec4 frexp(vec4 x, ivec4 exp)
        {
            exp = new();
            return new();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="exp"></param>
        /// <returns></returns>
        public double frexp(double x, int exp)
        {
            exp = 0;
            return 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="exp"></param>
        /// <returns></returns>
        public dvec2 frexp(dvec2 x, ivec2 exp)
        {
            exp = new();
            return new();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="exp"></param>
        /// <returns></returns>
        public dvec3 frexp(dvec3 x, ivec3 exp)
        {
            exp = new();
            return new();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="exp"></param>
        /// <returns></returns>
        public dvec4 frexp(dvec4 x, ivec4 exp)
        {
            exp = new();
            return new();
        }
        #endregion
        #region intBitsToFloat
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public float intBitsToFloat(int x) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public vec2 intBitsToFloat(ivec2 x) => new vec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public vec3 intBitsToFloat(ivec3 x) => new vec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public vec4 intBitsToFloat(ivec4 x) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public float uintBitsToFloat(uint x) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public vec2 uintBitsToFloat(uvec2 x) => new vec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public vec3 uintBitsToFloat(uvec3 x) => new vec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public vec4 uintBitsToFloat(uvec4 x) => new vec4();
        #endregion
        #region ldexp
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="exp"></param>
        /// <returns></returns>
        public float ldexp(float x, int exp)
        {
            exp = 0;
            return 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="exp"></param>
        /// <returns></returns>
        public vec2 ldexp(vec2 x, ivec2 exp)
        {
            exp = new();
            return new();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="exp"></param>
        /// <returns></returns>
        public vec3 ldexp(vec3 x, ivec3 exp)
        {
            exp = new();
            return new();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="exp"></param>
        /// <returns></returns>
        public vec4 ldexp(vec4 x, ivec4 exp)
        {
            exp = new();
            return new();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="exp"></param>
        /// <returns></returns>
        public double ldexp(double x, int exp)
        {
            exp = 0;
            return 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="exp"></param>
        /// <returns></returns>
        public dvec2 ldexp(dvec2 x, ivec2 exp)
        {
            exp = new();
            return new();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="exp"></param>
        /// <returns></returns>
        public dvec3 ldexp(dvec3 x, ivec3 exp)
        {
            exp = new();
            return new();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="exp"></param>
        /// <returns></returns>
        public dvec4 ldexp(dvec4 x, ivec4 exp)
        {
            exp = new();
            return new();
        }
        #endregion
        #region pack
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public double packDouble2x32(uvec2 v) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public uint packHalf2x16(vec2 v) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public uint packUnorm2x16(vec2 v) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public uint packSnorm2x16(vec2 v) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public uint packUnorm4x8(vec4 v) => 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public uint packSnorm4x8(vec4 v) => 0;
        #endregion
        #region unpack
        /// <summary>
        /// 
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        public uvec2 unpackDouble2x32(double d) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public vec2 unpackHalf2x16(uint p) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public vec2 unpackUnorm2x16(uint p) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public vec2 unpackSnorm2x16(uint p) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public vec4 unpackUnorm4x8(uint p) => new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public vec4 unpackSnorm4x8(uint p) => new();
        #endregion
        #endregion
    }
}
