﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    /// <summary>
    /// 
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 12, Pack = 4)]
    public struct vec3
    {
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public float x;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(4)]
        public float y;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(8)]
        public float z;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public float r;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(4)]
        public float g;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(8)]
        public float b;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public vec3(float x, float y, float z)
        {
            this.x = x; this.y = y; this.z = z;
            r = x; g = y; b = z;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public vec3(float v)
        {
            x = y = z = r = g = b = v;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vec"></param>
        /// <param name="z"></param>
        public vec3(vec2 vec, float z)
        {
            x = vec.x; y = vec.y;
            this.z = z;
            r = x; g = y; b = z;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public vec3(ivec3 v)
        {
            x = y = z = r = g = b = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public vec3(uvec3 v)
        {
            x = y = z = r = g = b = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public vec3(dvec3 v)
        {
            x = y = z = r = g = b = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public vec3(bvec3 v)
        {
            x = y = z = r = g = b = 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static vec3 operator ++(vec3 left) => new vec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static vec3 operator --(vec3 left) => new vec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec3 operator +(vec3 left, vec3 right) => new vec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec3 operator -(vec3 left, vec3 right) => new vec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec3 operator *(vec3 left, vec3 right) => new vec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec2 operator *(vec3 left, mat2x3 right) => new vec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec2 operator *(vec3 left, mat3x3 right) => new vec2();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec3 operator *(vec3 left, mat3 right) => new vec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec4 operator *(vec3 left, mat4x3 right) => new vec4();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec3 operator /(vec3 left, vec3 right) => new vec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec3 operator +(vec3 left, float right) => new vec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec3 operator +(float left, vec3 right) => new vec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec3 operator -(vec3 left, float right) => new vec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec3 operator -(float left, vec3 right) => new vec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <returns></returns>
        public static vec3 operator -(vec3 left) => new vec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec3 operator *(vec3 left, float right) => new vec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec3 operator *(float left, vec3 right) => new vec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec3 operator /(vec3 left, float right) => new vec3();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static vec3 operator /(float left, vec3 right) => new vec3();

        /// <summary>
        /// 
        /// </summary>
        public vec2 xx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 xy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 xz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 yx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 yy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 yz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 zx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 zy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 zz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 rr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 rg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 rb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 gr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 gg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 gb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 br { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 bg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec2 bb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 xxx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 xxy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 xxz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec3 xyx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec3 xyy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 xyz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec3 xzx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 xzy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        public vec3 xzz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 yxx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 yxy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 yxz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 yyx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 yyy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 yyz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 yzx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 yzy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 yzz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 zxx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 zxy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 zxz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 zyx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 zyy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 zyz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 zzx { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 zzy { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 zzz { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 rrr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 rrg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 rrb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 rgr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 rgg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 rgb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 rbr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 rbg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 rbb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 grr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 grg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 grb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 ggr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 ggg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 ggb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 gbr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 gbg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 gbb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 brr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 brg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 brb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 bgr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 bgg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 bgb { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 bbr { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 bbg { get => new(); set { } }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public vec3 bbb { get => new(); set { } }
    }
}
