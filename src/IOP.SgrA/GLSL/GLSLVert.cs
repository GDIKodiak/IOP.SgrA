﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.GLSL
{
    /// <summary>
    /// GLSL顶点着色器
    /// </summary>
    public abstract class GLSLVert : GLSLShaderBase, IVertShader
    {
        /// <summary>
        /// 
        /// </summary>
        public virtual vec4 gl_Position { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual float gl_PointSize {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual float[] gl_ClipDistance {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual float[] gl_CullDistance { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual int gl_InstanceIndex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual int gl_VertexID {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual int gl_VertexIndex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual string GetCode() => string.Empty;
        /// <summary>
        /// 
        /// </summary>
        public abstract void main();
    }
}
