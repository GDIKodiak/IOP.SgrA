using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Threading;
using CodeWF.EventBus;
using IOP.Extension.DependencyInjection;
using IOP.Halo;
using IOP.Halo.EventBus;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ReactiveUI;
using System;
using System.Diagnostics;
using System.Reactive;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Views
{
    public partial class MainContainerView : ContainerWindow
    {
        [Autowired]
        protected IFrameworkContainer Container { get; set; }
        [Autowired]
        protected ILogger Logger { get; set; }

        protected IEventBus EventBus { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public MainContainerView()
        {
            InitializeComponent();
            MenuDescriptor files = new("文件") { IsEnable = true };
            MenuDescriptor open = new("打开") { IsEnable = true };
            MenuDescriptor openFile = new("文件") { IsEnable = true, Command = ReactiveCommand.Create(OpenFile) };
            files.AddToMenu(open);
            open.AddToMenu(openFile);

            MenuDescriptor testA = new("TestA") { IsEnable = true };
            testA.AddToMenu(new MenuDescriptor("TestAA") { IsEnable = true });
            testA.AddToMenu("TestAA", new MenuDescriptor("TestAAA") { IsEnable = true });
            testA.AddToMenu(new MenuDescriptor("TestAB") { IsEnable = true, Command = ReactiveCommand.Create(Test) });
            testA.AddToMenu(new MenuDescriptor("TestAC") { IsEnable = true });
            MenuDescriptor testB = new("TestB") { IsEnable = true };
            testB.AddToMenu(new MenuDescriptor("TestBA") { IsEnable = true });
            testB.AddToMenu(new MenuDescriptor("TestBB") { IsEnable = false });
            MenuDescriptor testC = new("TestC") { IsEnable = false, Command = ReactiveCommand.Create(Test) };
            TitleBarMenus.AddToTopMenu(files);
            TitleBarMenus.AddToTopMenu(testA);
            TitleBarMenus.AddToTopMenu(testB);
            TitleBarMenus.AddToTopMenu(testC);
        }

        private void Test()
        {
            Serilog.Log.Logger.Information("Test");
        }

        private async void OpenFile()
        {
            FileExplorer dialog = new FileExplorer() { Height = 475, Width = 750 };
            dialog.ShowInTaskbar = false;
            var r = await dialog.ShowDialog<bool>(this);
            if(r == true)
            {
                var path = dialog.SelectedFilePath;
                _ = Container.ExecuteFileLoadingFuncs(path).ContinueWith((t) =>
                {
                    if (t.IsFaulted)
                    {
                        Logger?.Log(LogLevel.Error, t.Exception, "");
                    }
                });
            }
        }

        protected override void OnOpened(EventArgs e)
        {
            base.OnOpened(e);
        }

        protected override void OnLoaded(RoutedEventArgs e)
        {
            base.OnLoaded(e);
            EventBus = Container.Services.GetRequiredService<IEventBus>();
            EventBus.Subscribe<DialogWindowArgs>(NewDialog);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
        }

        private void NewDialog(DialogWindowArgs args)
        {
            Dispatcher.UIThread.Invoke(() =>
            {
                if (args == null || args.Content == null) return;
                DialogWindow window = new DialogWindow();
                window.Content = args.Content;
                window.Height = args.Height;
                window.Width = args.Width;
                window.Focusable = false;
                window.Show(this);
            });
        }
    }
}
