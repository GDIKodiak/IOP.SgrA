﻿using IOP.Extension.DependencyInjection;
using IOP.Halo;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace IOP.SgrA.Editor.ViewModels
{
    public class MainContainerViewModel : ReactiveObject
    {
#nullable disable
        [Autowired]
        public IFrameworkContainer Container { get; set; }
#nullable enable

        public MainContainerViewModel()
        {

        }
    }
}
