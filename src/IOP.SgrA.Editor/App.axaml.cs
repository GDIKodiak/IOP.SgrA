using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Controls.Presenters;
using Avalonia.Markup.Xaml;
using Avalonia.Platform;
using IOP.Halo;
using IOP.SgrA.Editor.ViewModels;
using IOP.SgrA.Editor.Views;
using IOP.SgrA.Editor.Plugin;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using Serilog;
using IOP.SgrA.Editor.VulkanEngine;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.ComponentModel;
using IOP.SgrA.PythonEnvironment;
using System.IO;
using Python.Runtime;

namespace IOP.SgrA.Editor
{
    public partial class App : HaloApplication
    {
#nullable disable
        /// <summary>
        /// 
        /// </summary>
        protected Serilog.ILogger Logger { get; set; }
#nullable enable

        public override void Initialize()
        {
            base.Initialize();
            AvaloniaXamlLoader.Load(this);
        }

        public override void OnFrameworkInitializationCompleted()
        {           
            base.OnFrameworkInitializationCompleted();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="descriptors"></param>
        public override void RegistServices(IServiceCollection descriptors)
        {
            base.RegistServices(descriptors);
            Log.Logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .WriteTo.Debug()
                .CreateLogger();
            Logger = Log.Logger;
            descriptors.AddLogging((builder) => 
            {
                builder.AddSerilog(dispose: true);
            });
            descriptors.AddVulkanEngine();
            descriptors.AddPluginService();
        }

        /// <summary>
        /// 构建加载页
        /// </summary>
        /// <returns></returns>
        public override LoaderControl? CreateLoadingPage()
        {
            LoaderViewModel vm = new();
            LoaderView view = CreateAutowiredInstance<LoaderView>();
            view.Width = 622;
            view.Height = 350;
            view.DataContext = vm;
            return view;
        }
        /// <summary>
        /// 加载任务
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public override async Task LoadingTask(LoaderControl control)
        {
            control.ChangeMessage("Load init");
            control.ChangeProgress(10);
            control.ChangeMessage("Load Python environment");
            //var pyPath = Path.Combine(AppContext.BaseDirectory, "Python");
            //PythonSetup.PythonRootPath = pyPath;
            //ILoggerFactory loggerFactory = Container.GetRequiredService<ILoggerFactory>();
            //await PythonSetup.Setup(loggerFactory);
            control.ChangeMessage("Load Python environment finish");
            control.ChangeProgress(20);
            control.ChangeMessage("Load Plugins init");
            var p = await LoadPlugins();
            control.ChangeMessage("Load Plugins finish");

            control.ChangeMessage("Load RenderEngine");
            control.ChangeProgress(50);
            await LoadRenderEngine();
            control.ChangeMessage("Load RenderEngine finish");
            control.ChangeProgress(80);

            control.ChangeMessage("Attach Plugins");
            await AttachPlugins(p);
            control.ChangeMessage("Attach Plugins finish");
            control.ChangeProgress(100);

            control.ChangeMessage("Load finish");
        }

        /// <summary>
        /// 构建容器页
        /// </summary>
        /// <returns></returns>
        public override ContainerWindow CreateContainerWindow()
        {
            return CreateAutowiredInstance<MainContainerView>();
        }
        /// <summary>
        /// 创建主窗口
        /// </summary>
        /// <param name="container"></param>
        /// <returns></returns>
        public override Window CreateMainWindow(ContainerWindow container)
        {
            var vm = CreateAutowiredInstance<MainContainerViewModel>();
            container.DataContext = vm;
            container.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            container.ExtendClientAreaChromeHints = ExtendClientAreaChromeHints.NoChrome;
            container.SystemDecorations = SystemDecorations.BorderOnly;
            container.WindowState = WindowState.FullScreen;
            return container;
        }
        /// <summary>
        /// 当退出时
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public override void OnExit(object? sender, ShutdownRequestedEventArgs e)
        {
            base.OnExit(sender, e);
            //PythonSetup.CurrentEnvironment.Finish();
            //PythonEngine.Shutdown();
            var service = ServiceProvider.GetService<IPluginService>();
            if(service != null)
            {
                foreach(var p in service.EnumerationPluginInfo(PluginState.Available))
                {
                    p.Plugin?.Dispose();
                }
            }
            var loader = ServiceProvider.GetService<IEngineLoader>();
            loader?.DisposeEngine();
        }

        /// <summary>
        /// 加载渲染引擎
        /// </summary>
        /// <returns></returns>
        protected ValueTask LoadRenderEngine()
        {
            return new ValueTask(Task.Factory.StartNew(async () => 
            {
                var loader = ServiceProvider.GetRequiredService<IEngineLoader>();
#if DEBUG
                await loader.LoadEngineDebug();
#else
                await loader.LoadEngine();
#endif
            }));
        }

        /// <summary>
        /// 加载插件
        /// </summary>
        /// <returns></returns>
        protected async ValueTask<List<PluginInfo>> LoadPlugins()
        {
            List<PluginInfo> r = new();
            var container = Container;
            var service = container.GetRequiredService<IPluginService>();
            var plugins = service.ForeachPlugins();
            if (plugins != null)
            {
                r = await RegisterPlugins(plugins);
            }
            return r;
        }
        /// <summary>
        /// 注册插件
        /// </summary>
        /// <returns></returns>
        protected async ValueTask<List<PluginInfo>> RegisterPlugins(PluginInfo[] infos)
        {
            List<PluginInfo> loaded = new();
            foreach(var item in infos)
            {
                if (!item.EnableWhenHostStarted || item.State != PluginState.LoadedSuccess || item.Plugin == null) continue;
                var p = item.Plugin;
                try
                {
                    await p.Initialization();
                    loaded.Add(item);
                    item.ChangeState(PluginState.InitializeSuccess);
                }
                catch (Exception e)
                {
                    Logger.Error($"Initialization plugins {item.FullName} failed with exception {e.Message}");
                    item.Failed(PluginState.InitializeFailed, e);
                }
            }
            List<PluginInfo> attach = new();
            Container.ConfigureServices((service) =>
            {
                foreach(var item in loaded)
                {
                    try
                    {
                        item.Plugin.RegistService(service);
                        attach.Add(item);
                    }
                    catch (Exception e)
                    {
                        Logger.Error($"Regist Service failed with plugins {item.GetType().FullName} : {e.Message}");
                        item.Failed(PluginState.AttachFailed, e);
                    }
                }
            });
            return attach;
        }
        /// <summary>
        /// 将插件附加至上下文
        /// </summary>
        /// <param name="infos"></param>
        /// <returns></returns>
        protected async ValueTask AttachPlugins(List<PluginInfo> attach)
        {
            if (attach == null) return;
            foreach (var item in attach)
            {
                try
                {
                    if (item.State != PluginState.InitializeSuccess) continue;
                    await item.Plugin.OnAttachedToContext(Container);
                    item.ChangeState(PluginState.Available);
                }
                catch (Exception e)
                {
                    Logger.Error($"Attached plugins {item.FullName} failed with exception {e.Message}");
                    item.Failed(PluginState.AttachFailed, e);
                }
            }
        }
    }
}
