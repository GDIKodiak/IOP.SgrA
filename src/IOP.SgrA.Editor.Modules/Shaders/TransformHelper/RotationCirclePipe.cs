﻿using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;

namespace IOP.SgrA.Editor.Modules
{
    public partial class RotationCirclePipe : ScriptedVulkanPipeline
    {
        public RotationCircleVert Vert { get; set; }
        public RotationCircleFrag Frag { get; set; }

        public override PipelineInputAssemblyStateCreateOption CreateInputAssemblyState()
        {
            return new PipelineInputAssemblyStateCreateOption()
            {
                PrimitiveRestartEnable = false,
                Topology = PrimitiveTopology.TriangleList
            };
        }
        public override PipelineRasterizationStateCreateOption CreateRasterizationState()
        {
            return new PipelineRasterizationStateCreateOption()
            {
                PolygonMode = PolygonMode.Fill,
                CullMode = CullModeFlags.None,
                FrontFace = FrontFace.CounterClockwise,
                DepthClampEnable = false,
                RasterizerDiscardEnable = false,
                DepthBiasEnable = false,
                DepthBiasConstantFactor = -0.2f,
                DepthBiasClamp = -0.3f,
                DepthBiasSlopeFactor = -0.1f,
                LineWidth = 1.0f
            };
        }
        public override PipelineDepthStencilStateCreateOption CreateDepthStencilState()
        {
            return new PipelineDepthStencilStateCreateOption()
            {
                Back = new StencilOpState()
                {
                    FailOp = StencilOp.Keep,
                    PassOp = StencilOp.Keep,
                    CompareOp = CompareOp.Always,
                    CompareMask = 0,
                    Reference = 0,
                    DepthFailOp = StencilOp.Keep,
                    WriteMask = 0
                },
                Front = new StencilOpState()
                {
                    FailOp = StencilOp.Keep,
                    PassOp = StencilOp.Keep,
                    CompareOp = CompareOp.Always,
                    CompareMask = 0,
                    Reference = 0,
                    DepthFailOp = StencilOp.Keep,
                    WriteMask = 0
                },
                DepthWriteEnable = true,
                DepthTestEnable = true,
                DepthCompareOp = CompareOp.Greater,
                DepthBoundsTestEnable = false,
                MinDepthBounds = 0,
                MaxDepthBounds = 0,
                StencilTestEnable = false
            };
        }
    }
}
