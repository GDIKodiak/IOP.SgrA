﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Modules
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class RotationCircleFrag : GLSLFrag
    {
        [DynamicUniformBuffer(0, 0, DescriptorSetTarget.RenderObject)]
        public CircleVals Vals { get; set; }

        [ShaderInput(0)]
        public vec3 InTexcoord { get; set; }
        [ShaderInput(1)]
        public vec3 WorldPos { get; set; }
        [ShaderInput(2)]
        public vec4 Center { get; set; }

        [FragOutput(0, false)]
        public vec4 OutColor { get; set; }
        [FragOutput(1, false)]
        public float Depth {  get; set; }

        private float EPSILON = 0.01f;
        private int MAXSTEP = 50;
        private float PI = 3.1415926f;

        public override void main()
        {
            vec3 ro = Vals.CameraPos.xyz;
            vec3 dir = normalize(WorldPos - ro);
            vec3 center = Vals.Center.xyz;
            float maxDis = length(WorldPos - ro) * 2.0f;
            vec3 t = Vals.T;
            if(Vals.TypeEnterAndSelected.z == 1)
            {
                RayMarchingSelected(ro, dir, t, center, maxDis);
            }
            else
            {
                RayMarching(ro, dir, t.xy, center, maxDis);
            }
            Depth = 1.0f;
        }

        private float SphereSDF(vec3 p, float r)
        {
            return length(p) - r;
        }
        private float TorusSDFAxisX(vec3 p, vec2 t)
        {
            vec2 q = new vec2(length(p.yz) - t.x, p.x);
            return length(q) - t.y;
        }
        private float TorusSDFAxisY(vec3 p, vec2 t)
        {
            vec2 q = new vec2(length(p.xz) - t.x, p.y);
            return length(q) - t.y;
        }
        private float TorusSDFAxisZ(vec3 p, vec2 t)
        {
            vec2 q = new vec2(length(p.xy) - t.x, p.z);
            return length(q) - t.y;
        }
        private bool TorusSDFSelectedX(vec3 p, vec3 t, [Out]float dist, [Out] bool isInFan)
        {
            isInFan = false;
            float d = length(p.yz);
            float dist1 = length(new vec2(d - t.x, p.x)) - t.y;
            vec2 cylinder = abs(new vec2(d, p.x)) - new vec2(t.x, t.y);
            float dist2 = min(max(cylinder.x, cylinder.y), 0.0f) + length(max(cylinder, 0.0f));
            dist = min(dist1, dist2);
            vec2 centerP = ToNDCPos(Center);
            vec2 curP = ToNDCPos(Vals.MVP * new vec4(p, 1.0f));
            vec2 dd = fwidth(curP);
            vec2 diff = centerP - curP;
            vec2 dir = normalize(diff);
            vec2 dir2 = normalize(centerP - ToNDCPos(Vals.StartPosEndPos.xy));
            vec2 dir3 = normalize(centerP - ToNDCPos(Vals.StartPosEndPos.zw));

            if (t.z < -PI * 2.0f || t.z > PI * 2.0f) isInFan = false;
            else
            {
                float inclination = acos(dot(dir2, dir3));
                vec2 halfVec = normalize(dir2 + dir3);
                float hi = dot(halfVec, dir);
                float rangeCos = cos(inclination * 0.5f);
                isInFan = (t.z > -PI && t.z < PI) ? hi < rangeCos : hi > rangeCos;
            }

            float pDistence = abs(diff.x * dir2.y - diff.y * dir2.x);
            float pDistence2 = abs(diff.x * dir3.y - diff.y * dir3.x);
            float c = dot(dir, dir2);
            float c2 = dot(dir, dir3);
            return (d >= t.x - t.y) || (pDistence <= 1.0f * length(dd) && c >= 0.0f)
                || (pDistence2 <= 1.0f * length(dd) && c2 >= 0.0f);
        }
        private bool TorusSDFSelectedY(vec3 p, vec3 t, [Out] float dist, [Out] bool isInFan)
        {
            isInFan = false;
            float d = length(p.xz);
            float dist1 = length(new vec2(d - t.x, p.y)) - t.y;
            vec2 cylinder = abs(new vec2(d, p.y)) - new vec2(t.x, t.y);
            float dist2 = min(max(cylinder.x, cylinder.y), 0.0f) + length(max(cylinder, 0.0f));
            dist = min(dist1, dist2);
            vec2 centerP = ToNDCPos(Center);
            vec2 curP = ToNDCPos(Vals.MVP * new vec4(p, 1.0f));
            vec2 dd = fwidth(curP);
            vec2 diff = centerP - curP;
            vec2 dir = normalize(diff);
            vec2 dir2 = normalize(centerP - ToNDCPos(Vals.StartPosEndPos.xy));
            vec2 dir3 = normalize(centerP - ToNDCPos(Vals.StartPosEndPos.zw));

            if (t.z < -PI * 2.0f || t.z > PI * 2.0f) isInFan = false;
            else
            {
                float inclination = acos(dot(dir2, dir3));
                vec2 halfVec = normalize(dir2 + dir3);
                float hi = dot(halfVec, dir);
                float rangeCos = cos(inclination * 0.5f);
                isInFan = (t.z > -PI && t.z < PI) ? hi < rangeCos : hi > rangeCos;
            }

            float pDistence = abs(diff.x * dir2.y - diff.y * dir2.x);
            float pDistence2 = abs(diff.x * dir3.y - diff.y * dir3.x);
            float c = dot(dir, dir2);
            float c2 = dot(dir, dir3);
            return (d >= t.x - t.y) || (pDistence <= 1.0f * length(dd) && c >= 0.0f)
                || (pDistence2 <= 1.0f * length(dd) && c2 >= 0.0f);
        }
        private bool TorusSDFSelectedZ(vec3 p, vec3 t, [Out] float dist, [Out] bool isInFan)
        {
            isInFan = false;
            float d = length(p.xy);
            float dist1 = length(new vec2(d - t.x, p.z)) - t.y;
            vec2 cylinder = abs(new vec2(d, p.z)) - new vec2(t.x, t.y);
            float dist2 = min(max(cylinder.x, cylinder.y), 0.0f) + length(max(cylinder, 0.0f));
            dist = min(dist1, dist2);
            vec2 centerP = ToNDCPos(Center);
            vec2 curP = ToNDCPos(Vals.MVP * new vec4(p, 1.0f));
            vec2 dd = fwidth(curP);
            vec2 diff = centerP - curP;
            vec2 dir = normalize(diff);
            vec2 dir2 = normalize(centerP - ToNDCPos(Vals.StartPosEndPos.xy));
            vec2 dir3 = normalize(centerP - ToNDCPos(Vals.StartPosEndPos.zw));

            if (t.z < -PI * 2.0f || t.z > PI * 2.0f) isInFan = false;
            else
            {
                float inclination = acos(dot(dir2, dir3));
                vec2 halfVec = normalize(dir2 + dir3);
                float hi = dot(halfVec, dir);
                float rangeCos = cos(inclination * 0.5f);
                isInFan = (t.z > -PI && t.z < PI) ? hi < rangeCos : hi > rangeCos;
            }

            float pDistence = abs(diff.x * dir2.y - diff.y * dir2.x);
            float pDistence2 = abs(diff.x * dir3.y - diff.y * dir3.x);
            float c = dot(dir, dir2);
            float c2 = dot(dir, dir3);
            return (d >= t.x - t.y) || (pDistence <= 1.0f * length(dd) && c >= 0.0f)
                || (pDistence2 <= 1.0f * length(dd) && c2 >= 0.0f);
        }

        private bool RayMarching(vec3 ro, vec3 rd, vec2 t, vec3 center, float maxDis)
        {
            float depth = 0.0f;
            vec3 oPos = new vec3(0.0f);
            bool result = false;
            float dist = 0.0f;
            vec2 h = Vals.TypeEnterAndSelected.xy;
            for(int i = 0; i < MAXSTEP; i++)
            {
                vec3 pos = ro + depth * rd;
                if (h.x == 0.0f) dist = SphereSDF(pos, t.x);
                else if (h.x == 1.0f) dist = TorusSDFAxisX(pos, t);
                else if (h.x == 2.0f) dist = TorusSDFAxisY(pos, t);
                else if (h.x == 3.0f) dist = TorusSDFAxisZ(pos, t);
                else result = false;
                depth += dist;
                if (dist <= EPSILON)
                {
                    oPos = pos;
                    vec3 cDir = normalize(center - oPos);
                    float c = dot(cDir, rd);
                    if (h.x == 0.0f) result = true;
                    else if (h.x == 1.0f) result = c >= -0.25f;
                    else if (h.x == 2.0f) result = c >= -0.2f;
                    else if (h.x == 3.0f) result = c >= -0.25f;
                    else result = true;
                    break;
                }
                else if (depth > maxDis) break;
            }
            if (result)
            {
                if (h.x == 0.0f)
                {
                    float a = Vals.TypeEnterAndSelected.y == 0.0f ? 0.0f : 0.1f;
                    OutColor = new vec4(Vals.Color.xyz, a);
                }
                else
                {
                    float a = Vals.TypeEnterAndSelected.y == 0.0f ? 0.8f : 1.0f;
                    OutColor = new vec4(Vals.Color.xyz * a, 1.0f);
                }
            }
            else discard();
            return result;
        }

        private bool RayMarchingSelected(vec3 ro, vec3 rd, vec3 t, vec3 center, float maxDis)
        {
            float depth = 0.0f;
            vec3 oPos = new vec3(0.0f);
            bool result = false;
            float dist = 0.0f;
            float type = Vals.TypeEnterAndSelected.x;
            bool isOuter = false;
            bool isInFan = false;
            for (int i = 0; i < MAXSTEP; i++)
            {
                vec3 pos = ro + depth * rd;
                if (type == 1.0f) isOuter = TorusSDFSelectedX(pos, t, dist, isInFan);
                else if (type == 2.0f) isOuter = TorusSDFSelectedY(pos, t, dist, isInFan);
                else if (type == 3.0f) isOuter = TorusSDFSelectedZ(pos, t, dist, isInFan);
                else
                {
                    result = false;
                    break;
                }
                depth += dist;
                if (dist <= EPSILON)
                {
                    oPos = pos;
                    result = true;
                }
                else if (depth > maxDis) break;
            }
            if (result)
            {
                if (isInFan && !isOuter) discard();
                else OutColor = isOuter ? new vec4(Vals.Color.xyz, 1.0f) : new vec4(0.3f);
            }
            else discard();
            return result;
        }

        private vec2 ToNDCPos(vec4 clipPos)
        {
            clipPos = new vec4(clipPos.xyz / clipPos.w, 1.0f);
            return clipPos.xy;
        }
        private vec2 ToNDCPos(vec2 winPos)
        {
            float ndcX = winPos.x * 2.0f / Vals.ScreenLength.x - 1.0f;
            float ndcY = 1.0f - (winPos.y * 2.0f / Vals.ScreenLength.y);
            return new vec2(ndcX, ndcY);
        }
    }
}
