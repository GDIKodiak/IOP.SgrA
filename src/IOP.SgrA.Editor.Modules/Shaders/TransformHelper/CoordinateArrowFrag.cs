﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Modules
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class CoordinateArrowFrag : GLSLFrag
    {
        [DynamicUniformBuffer(0, 0, DescriptorSetTarget.RenderObject)]
        public ArrowVals Vals { get; set; }

        [ShaderInput(0)]
        public vec3 GPosition { get; set; }
        [ShaderInput(1)]
        public vec3 GColor { get; set; }

        [FragOutput(0, false)]
        public vec4 OutColor { get; set; }
        [FragOutput(1, false)]
        public float OutDepth { get; set; }

        public override void main()
        {
            vec2 state = Vals.IsPointerEnterAndSelected;
            if (state.x == 1) OutColor = new vec4(GColor, 1.0f);
            else OutColor = new vec4(GColor * 0.8f, 1.0f);
            OutDepth = 1.0f;
        }
    }
}
