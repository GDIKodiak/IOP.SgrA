﻿using Avalonia.Controls;
using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Modules
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class RotationCircleVert : GLSLVert
    {
        [DynamicUniformBuffer(0, 0, DescriptorSetTarget.RenderObject)]
        public CircleVals Vals { get; set; }

        [VertInput(0, Format.R32G32B32Sfloat)]
        public vec3 Pos { get; set; }
        [VertInput(1, Format.R32G32B32Sfloat)]
        public vec3 InTexcoord { get; set; }

        [ShaderOutput(0)]
        public vec3 OutTexcoord { get; set; }
        [ShaderOutput(1)]
        public vec3 WorldPos {  get; set; }
        [ShaderOutput(2)]
        public vec4 Center { get; set; }

        public override void main()
        {
            vec4 pos = new vec4(0.0f);
            vec4 worldPos = new vec4(0.0f);
            pos = Vals.MVP * new vec4(Pos, 1.0f);
            worldPos = Vals.MM * new vec4(Pos, 1.0f);
            WorldPos = worldPos.xyz;
            OutTexcoord = InTexcoord;
            Center = Vals.MVP * new vec4(Vals.Center.xyz, 1.0f);
            gl_Position = pos;
        }
    }
}
