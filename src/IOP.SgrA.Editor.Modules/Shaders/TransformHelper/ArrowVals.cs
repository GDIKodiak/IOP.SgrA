﻿using IOP.SgrA.GLSL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Modules
{
    public struct ArrowVals
    {
        public mat4 MVP;
        public mat4 MM;
        public vec2 IsPointerEnterAndSelected;
    }
}
