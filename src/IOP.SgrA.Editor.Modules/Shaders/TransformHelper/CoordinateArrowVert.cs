﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Modules
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class CoordinateArrowVert : GLSLVert
    {
        [DynamicUniformBuffer(0, 0, DescriptorSetTarget.RenderObject)]
        public ArrowVals Vals { get; set; }

        [VertInput(0, Format.R32G32B32Sfloat)]
        public vec3 Pos { get; set; }
        [VertInput(1, Format.R32G32B32Sfloat)]
        public vec3 Color { get; set; }

        [ShaderOutput(0)]
        public vec3 OutPos { get; set; }
        [ShaderOutput(1)]
        public vec3 OutColor { get; set; }

        public override void main()
        {
            OutPos = (Vals.MM * new vec4(Pos, 1)).xyz;
            OutColor = Color;
            vec4 outPos = Vals.MVP * new vec4(Pos, 1.0f);
            gl_Position = outPos;
        }
    }
}
