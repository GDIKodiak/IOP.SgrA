﻿using IOP.SgrA.GLSL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Modules
{
    public struct CircleVals
    {
        public mat4 MVP;
        public mat4 MM;
        public vec4 Color;
        public vec4 CameraPos;
        public vec4 Center;
        public vec4 StartPosEndPos;
        public vec2 ScreenLength;
        /// <summary>
        /// 环面t参数
        /// t.x：圆心到环中轴线半径
        /// t.y：环切面半径
        /// t.z：累计旋转角度Theta
        /// </summary>
        public vec3 T;
        public vec3 TypeEnterAndSelected;
    }
}
