﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Modules
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class WeightedOITLastFrag : GLSLFrag
    {
        [InputAttachment(0, 0, 0)]
        public subpassInput Accumulation {  get; set; }
        [InputAttachment(0, 1, 1)]
        public subpassInput Reveal {  get; set; }
        [InputAttachment(0, 2, 2)]
        public subpassInput RevealDepth {  get; set; }
        [FragOutput(0, true, ColorBlendOp = BlendOp.Add, AlphaBlendOp = BlendOp.Add,
            SrcColorBlendFactor = BlendFactor.One, SrcAlphaBlendFactor = BlendFactor.SrcAlpha,
            DstColorBlendFactor = BlendFactor.SrcColor, DstAlphaBlendFactor = BlendFactor.OneMinusSrcAlpha)]
        public vec4 OutColor { get; set; }
        [FragOutput(1, false)]
        public float OutDepth {  get; set; }

        float EPSILON = 0.00001f;
        
        private float max3(vec3 v)
        {
            return max(max(v.x, v.y), v.z);
        }

        public override void main()
        {
            float revealage = subpassLoad(Reveal).r;
            float revealDepth = subpassLoad(RevealDepth).r;

            vec4 accumulation = subpassLoad(Accumulation);

            if(revealage >= 1.0f - EPSILON)
            {
                OutColor = new vec4(accumulation.rgb, 1.0f);
            }
            else
            {
                if (isinf(max3(abs(accumulation.rgb))))
                    accumulation.rgb = new vec3(accumulation.a);

                vec3 average_color = accumulation.rgb / max(accumulation.a, EPSILON);
                OutColor = new vec4(average_color, revealage);
            }
            OutDepth = revealDepth;
        }
    }
}
