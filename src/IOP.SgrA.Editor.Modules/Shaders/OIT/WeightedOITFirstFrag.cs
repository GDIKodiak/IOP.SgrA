﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Modules
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class WeightedOITFirstFrag : GLSLFrag
    {
        [InputAttachment(0, 0, 0)]
        public subpassInput InColor { get; set; }
        [InputAttachment(0, 1, 1)]
        public subpassInput InDepth { get; set; }

        [ShaderInput(0)]
        public vec2 ScreenPos { get; set; }
        [FragOutput(0, true, ColorBlendOp = BlendOp.Add, AlphaBlendOp = BlendOp.Add,
            SrcColorBlendFactor = BlendFactor.One, SrcAlphaBlendFactor = BlendFactor.One,
            DstColorBlendFactor = BlendFactor.One, DstAlphaBlendFactor = BlendFactor.One)]
        public vec4 OutColor { get; set; }
        [FragOutput(1, false, ColorBlendOp = BlendOp.Add, AlphaBlendOp = BlendOp.Add,
            SrcColorBlendFactor = BlendFactor.Zero, SrcAlphaBlendFactor = BlendFactor.Zero,
            DstColorBlendFactor = BlendFactor.OneMinusSrcColor, DstAlphaBlendFactor = BlendFactor.OneMinusSrcColor)]
        public float Reveal { get; set; }
        [FragOutput(2, false)]
        public float RevealDepth {  get; set; }

        public override void main()
        {
            vec4 col = subpassLoad(InColor);
            var d = subpassLoad(InDepth);

            float weight = 0.0f;
            if (col.a >= 0.99995f) weight = 1.0f;
            else
            {
                weight = clamp(pow(min(1.0f, col.a * 10.0f) + 0.01f, 3.0f) * 1e8f * 
                    pow(d.r * 0.9f, 3.0f), 1e-2f, 3e3f);
            }
            vec4 accum = new vec4(col.rgb * col.a, col.a) * weight;
            OutColor = accum;
            Reveal = col.a;
            RevealDepth = d.r;
        }
    }
}
