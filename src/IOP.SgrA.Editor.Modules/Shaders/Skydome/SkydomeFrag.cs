﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Modules
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class SkydomeFrag : GLSLFrag
    {
        [UniformBuffer(0, 0, DescriptorSetTarget.Pipeline)]
        public SkyParam skyColors { get; set; }
        [StorageBuffer(1, 0, DescriptorSetTarget.Pipeline, Layout = BufferLayout.std430)]
        public ParallelLBuffer parallelLBuffer { get; set; }
        [StorageImage(2, 0, StorageFormat.rgba16, DescriptorSetTarget.Pipeline)]
        public imageCube iblImage { get; set; }
        [UniformBuffer(3, 0, DescriptorSetTarget.Pipeline)]
        public ConstantVals vals { get; set; }
        [PushConstant(8, 0)]
        public PushConstans pushConsts { get; set; }

        [ShaderInput(0)]
        public vec3 pos { get; set; }
        [ShaderInput(1)]
        public vec3 uv { get; set; }
        [ShaderInput(2)]
        public vec3 normal { get; set; }
        [ShaderInput(3)]
        public vec4 inCurPos { get; set; }
        [FragOutput(0, false)]
        public vec4 outColor { get; set; }
        [FragOutput(1, false)]
        public float outputDepth {  get; set; }

        const float PI = 3.1415926535897932384626433832795f;
        const uint ViewSamples = 8;
        const uint LightSamples = 4;
        const float I_sun = 10.0f;
        const float R_e = 6360e3f;
        const float R_a = 6420e3f;
        vec3 beta_R = new vec3(5.8e-6f, 13.5e-6f, 33.1e-6f);
        const float beta_M = 2e-5f;
        const float H_R = 7994;
        const float H_M = 1200;
        const float Mie_G = -0.76f;
        vec3 C = new vec3(0, -R_e, 0);

        vec2 totalDepthRM;
        vec3 I_R, I_M;
        vec3 sundir;

        public vec2 DensitiesRM(vec3 p)
        {
            float h = max(0.0f, length(p - C) - R_e); // calculate height from Earth surface
            return new vec2(exp(-h / 8e3f), exp(-h / 12e2f));
        }
        public float Escape(vec3 p, vec3 d, float R)
        {
            vec3 v = p - C;
            float b = dot(v, d);
            float det = b * b - dot(v, v) + R * R;
            if (det < 0.0f) return -1.0f;
            det = sqrt(det);
            float t1 = -b - det, t2 = -b + det;
            return (t1 >= 0.0f) ? t1 : t2;
        }
        public vec2 scatterDepthInt(vec3 o, vec3 d, float L, float steps)
        {
            // Accumulator
            vec2 depthRMs = new vec2(0.0f);

            // Set L to be step distance and pre-multiply d with it
            L /= steps; d *= L;

            // Go from point P to A
            for (float i = 0.0f; i < steps; ++i)
                // Simply accumulate densities
                depthRMs += DensitiesRM(o + d * i);

            return depthRMs * L;
        }
        public void scatterIn(vec3 o, vec3 d, float L, float steps)
        {

            // Set L to be step distance and pre-multiply d with it
            L /= steps; d *= L;

            // Go from point O to B
            for (float i = 0.0f; i < steps; ++i)
            {

                // Calculate position of point P_i
                vec3 p = o + d * i;

                // Calculate densities
                vec2 dRM = DensitiesRM(p) * L;

                // Accumulate T(P_i -> O) with the new P_i
                totalDepthRM += dRM;

                // Calculate sum of optical depths. totalDepthRM is T(P_i -> O)
                // scatterDepthInt calculates integral part for T(A -> P_i)
                // So depthRMSum becomes sum of both optical depths
                vec2 depthRMsum = totalDepthRM + scatterDepthInt(p, sundir, Escape(p, sundir, R_a), LightSamples);

                // Calculate e^(T(A -> P_i) + T(P_i -> O)
                vec3 A = exp(-beta_R * depthRMsum.x - new vec3(beta_M) * 1.1f * depthRMsum.y);

                // Accumulate I_R and I_M
                I_R += A * dRM.x;
                I_M += A * dRM.y;
            }
        }
        public vec3 scatter(vec3 o, vec3 d, float L, vec3 Lo)
        {

            // Zero T(P -> O) accumulator
            totalDepthRM = new vec2(0.0f);

            // Zero I_M and I_R
            I_R = I_M = new vec3(0.0f);

            // Compute T(P -> O) and I_M and I_R
            scatterIn(o, d, L, ViewSamples);

            // mu = cos(alpha)
            float mu = dot(d, sundir);

            // Calculate Lo extinction
            return Lo * exp(-beta_R * totalDepthRM.x - new vec3(beta_M) * 1.1f * totalDepthRM.y)
                + I_sun * (1.0f + mu * mu) * (I_R * beta_R * 0.0597f + I_M *
                new vec3(beta_M) * .0196f / pow(1.58f - 1.52f * mu, 1.5f));
        }
        public vec3 scatteringColor(vec3 dir)
        {
            vec3 cameraPos = vals.cameraPos.xyz;
            vec3 fsPostion = pos * R_e;
            vec3 center = new vec3(0.0f, -R_e, 0.0f);
            vec3 viewDir = normalize(fsPostion - cameraPos);
            vec3 eyePos = cameraPos;
            vec3 col = new vec3(0f);
            float L = Escape(eyePos, viewDir, R_a);
            col = scatter(eyePos, viewDir, L, col);
            return col;
        }
        public vec4 getSkyColor(vec3 dir)
        {
            vec3 posWS = normalize(pos);
            vec3 lDir = (dir + 1) * 0.5f;
            float sun = distance(posWS, dir);
            sun = 1 - smoothstep(0.7f, 1, sun * 30f);
            if (sun > 0 && lDir.y > 0.4)
            {
                return new vec4(1.0f, 1.0f, 1.0f, 0.0f);
            }
            else
            {
                if (lDir.y > 0.4995 && lDir.y < 1.005)
                {
                    vec4 daySkyColor = mix(mix(
                        skyColors.morningColor, skyColors.noonColor, clamp(smoothstep(0f, 0.9f, lDir.z) * 3f, 0.0f, 1.0f)),
                        skyColors.nightfallColor, smoothstep(0f, 0.3f, smoothstep(0.9f, 1, lDir.z) * 0.3f))
                        * step(0.5f, lDir.y);
                    vec4 dayHorizonColor = mix(mix(mix(
                        skyColors.risingsunHorizon2, skyColors.morningColor, clamp(smoothstep(0f, 0.2f, lDir.z), 0.0f, 1.0f) * 2f),
                        skyColors.noonHorizon, smoothstep(0f, 0.5f, smoothstep(0.2f, 0.9f, lDir.z) * 0.5f)),
                        skyColors.nightfallHorizon, smoothstep(0.9f, 1.0f, lDir.z))
                        * step(0.5f, lDir.y);
                    vec4 dayMixColor = mix(dayHorizonColor, daySkyColor,
                        smoothstep(0f, 0.5f + 0.4f * smoothstep(0.9f, 1f, lDir.z), posWS.y));
                    return new vec4(dayMixColor.xyz, 0.0f);
                }
                else
                {
                    vec4 nightSkyColor = mix(skyColors.morningColor,
                        mix(skyColors.nightColor, skyColors.nightfallColor, smoothstep(0.5f, 1f, lDir.z)), clamp(smoothstep(0f, 0.08f, lDir.z), 0.0f, 1.0f))
                        * step(-0.5f, -lDir.y);
                    vec4 nightHorizonColor = mix(skyColors.risingsunHorizon2, mix(skyColors.risingsunHorizon1,
                        mix(skyColors.nightHorizon, skyColors.nightfallHorizon, smoothstep(0f, 0.1f, smoothstep(0.7f, 1f, lDir.z) * 0.1f)),
                        clamp(smoothstep(0.1f, 0.7f, lDir.z) * 9900f, 0.0f, 1.0f)),
                        clamp(smoothstep(0.0f, 0.1f, lDir.z) * 5, 0.0f, 1.0f))
                        * step(-0.5f, -lDir.y);
                    float posWSY = 0.5f + 0.4f * smoothstep(0.9f, 1, lDir.z);
                    float curve = (cos(9.9f * PI * lDir.z) - 1) * 0.2f;
                    if (lDir.z < 0) curve = 0;
                    else if (lDir.z > 0.202) curve = 0;
                    posWSY += curve;
                    vec4 nightMixColor = mix(nightHorizonColor, nightSkyColor, smoothstep(0, posWSY, posWS.y));
                    return new vec4(nightMixColor.xyz, 0.0f);
                }
            }
        }
        public vec4 getSimpleSkyColor()
        {
            vec3 posWS = normalize(pos);
            float ny = (posWS.y + 1) * 0.5f;
            return mix(new vec4(0.1843f, 0.1843f, 0.1843f, 1.0f), new vec4(0.1843f, 0.1843f, 0.1843f, 1.0f), 1.0f);
        }

        public override void main()
        {
            var color = getSimpleSkyColor();
            outputDepth = 0.0000001f;
            //outputDepth = 0.0f;
            outColor = color;
        }
    }

    public struct SkyParam
    {
        public vec4 morningColor;
        public vec4 noonColor;
        public vec4 nightfallColor;
        public vec4 nightColor;
        public vec4 risingsunHorizon1;
        public vec4 risingsunHorizon2;
        public vec4 morningHorizon;
        public vec4 noonHorizon;
        public vec4 nightfallHorizon;
        public vec4 nightHorizon;
    }
    public struct ParallelLight
    {
        public vec4 direction;
        public vec4 color;
    };
    public struct ParallelLBuffer
    {
        public ParallelLight[] parallelLights;
    }
    public struct PushConstans
    {
        public uint bakeMode;
        public uint cubeIndex;
    }
}
