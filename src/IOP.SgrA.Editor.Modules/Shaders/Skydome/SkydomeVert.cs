﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Modules
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class SkydomeVert : GLSLVert
    {
        [UniformBuffer(3, 0, DescriptorSetTarget.Pipeline)]
        public ConstantVals vals { get; set; }
        [VertInput(0, Format.R32G32B32Sfloat)]
        public vec3 pos { get; set; }
        [VertInput(1, Format.R32G32B32Sfloat)]
        public vec3 normal { get; set; }
        [VertInput(2, Format.R32G32B32Sfloat)]
        public vec3 uv { get; set; }
        [ShaderOutput(0)]
        public vec3 localPos { get; set; }
        [ShaderOutput(1)]
        public vec3 outUV { get; set; }
        [ShaderOutput(2)]
        public vec3 reveNormal { get; set; }
        [ShaderOutput(3)]
        public vec4 outCurrPos { get; set; }

        public override void main()
        {
            localPos = pos;
            outUV = uv;
            vec4 currPos = vals.projection * vals.view * new vec4(pos, 1.0f);
            reveNormal = -1 * normal;
            gl_Position = currPos.xyww;
            outCurrPos = currPos;
        }
    }

    public struct ConstantVals
    {
        public mat4 projection;
        public mat4 view;
        public mat4 preView;
        public mat4 jitterProject;
        public vec4 cameraPos;
    }
}
