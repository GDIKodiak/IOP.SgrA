﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Modules
{
    public partial class PBRLightVert : GLSLVert
    {
        [ShaderOutput(0)]
        public vec3 uv { get; set; }

        public override void main()
        {
            if (gl_VertexIndex == 0)
            {
                uv = new vec3(0.0f, 1.0f, 0.0f);
                gl_Position = new vec4(-1.0f, -1.0f, 0.0f, 1.0f);
            }
            else if (gl_VertexIndex == 1)
            {
                uv = new vec3(1.0f, 1.0f, 0.0f);
                gl_Position = new vec4(1.0f, -1.0f, 0.0f, 1.0f);
            }
            else if (gl_VertexIndex == 2)
            {
                uv = new vec3(0.0f, 0.0f, 0.0f);
                gl_Position = new vec4(-1.0f, 1.0f, 0.0f, 1.0f);
            }
            else if (gl_VertexIndex == 3)
            {
                uv = new vec3(1.0f, 0.0f, 0.0f);
                gl_Position = new vec4(1.0f, 1.0f, 0.0f, 1.0f);
            }
        }
    }
}
