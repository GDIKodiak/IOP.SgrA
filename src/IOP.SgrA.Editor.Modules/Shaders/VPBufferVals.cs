﻿using IOP.SgrA.GLSL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Modules
{
    /// <summary>
    /// View Proj Buffer
    /// </summary>
    public struct VPBufferVals
    {
        public mat4 view;
        public mat4 proj;
        public mat4 preView;
        public float Near;
        public float Far;
        public int reverseZ;
    }
}
