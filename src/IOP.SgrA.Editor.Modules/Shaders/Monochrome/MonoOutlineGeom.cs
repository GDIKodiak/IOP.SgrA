﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Modules
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    [GeometryVertexInput(GeometryInputType.TrianglesAdjacency)]
    [GeometryVertexOutput(GeometryOutputType.LineStrip, 15)]
    public partial class MonoOutlineGeom : GLSLGeom
    {
        [UniformBuffer(0, 0, DescriptorSetTarget.RenderObject)]
        public MonoParameter Parameters { get; set; }
        [GeometryInput(0)]
        public vec3[] VNormal { get; set; }
        [GeometryInput(1)]
        public vec3[] VPosition { get; set; }

        [ShaderOutput(0)]
        public vec3 GNormal { get; set; }
        [ShaderOutput(1)]
        public vec3 GPosition { get; set; }
        [ShaderOutput(2)]
        public vec4 LPosition {  get; set; }
        [ShaderOutput(3)]
        public vec3 GDir { get; set; }

        public float PctExtend = 0.0f;

        /// <summary>
        /// 是否为正面
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        public bool IsFrontFacing(vec3 a, vec3 b, vec3 c)
        {
            return ((a.x * b.y - b.x * a.y) +
                (b.x * c.y - c.x * b.y)
                + (c.x * a.y - a.x * c.y)) > 0;
        }
        /// <summary>
        /// 插入描边线
        /// </summary>
        /// <param name="e0"></param>
        /// <param name="e1"></param>
        public void EmitEdgeQuad(int e0, int e1)
        {
            vec3 vp0 = VPosition[e0];
            vec3 vp1 = VPosition[e1];
            vec4 gp0 = gl_in[e0].gl_Position;
            vec4 gp1 = gl_in[e1].gl_Position;
            vec3 dir = normalize(gp1.xyz / gp1.w - gp0.xyz / gp0.w);
            // Emit the quad
            vec4 newP0 = new vec4(gp0.xy, gp0.z, gp0.w);
            GPosition = new vec3(vp0.xy, vp0.z);
            GNormal = VNormal[e0];
            LPosition = new vec4(gp0.xyz / gp0.w, 1.0f);
            GDir = dir;
            gl_Position = newP0;
            EmitVertex();
            vec4 newP2 = new vec4(gp1.xy, gp1.z, gp1.w);
            GPosition = new vec3(vp1.xy, vp1.z);
            GNormal = VNormal[e1];
            LPosition = new vec4(gp1.xyz / gp1.w, 1.0f);
            GDir = dir;
            gl_Position = newP2;
            EmitVertex();
            EndPrimitive();
        }

        public override void main()
        {
            vec3 p0 = gl_in[0].gl_Position.xyz / gl_in[0].gl_Position.w;
            vec3 p1 = gl_in[1].gl_Position.xyz / gl_in[1].gl_Position.w;
            vec3 p2 = gl_in[2].gl_Position.xyz / gl_in[2].gl_Position.w;
            vec3 p3 = gl_in[3].gl_Position.xyz / gl_in[3].gl_Position.w;
            vec3 p4 = gl_in[4].gl_Position.xyz / gl_in[4].gl_Position.w;
            vec3 p5 = gl_in[5].gl_Position.xyz / gl_in[5].gl_Position.w;
            if (IsFrontFacing(p0, p2, p4))
            {
                if (!IsFrontFacing(p0, p1, p2))
                    EmitEdgeQuad(0, 2);
                if (!IsFrontFacing(p2, p3, p4))
                    EmitEdgeQuad(2, 4);
                if (!IsFrontFacing(p4, p5, p0))
                    EmitEdgeQuad(4, 0);
            }
            //else if(instance != 0)
            //{
            //    // Output the original triangle
            //    GIsEdge = 0;
            //    // This triangle is not part of an edge.
            //    GNormal = VNormal[0];
            //    GPosition = VPosition[0];
            //    gl_Position = gl_in[0].gl_Position;
            //    EmitVertex();
            //    GNormal = VNormal[2];
            //    GPosition = VPosition[2];
            //    gl_Position = gl_in[2].gl_Position;
            //    EmitVertex();
            //    GNormal = VNormal[4];
            //    GPosition = VPosition[4];
            //    gl_Position = gl_in[4].gl_Position;
            //    EmitVertex();
            //    EndPrimitive();
            //}
        }
    }
}
