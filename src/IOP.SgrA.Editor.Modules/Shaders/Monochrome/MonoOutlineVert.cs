﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;

namespace IOP.SgrA.Editor.Modules
{
    [VertexInputBinding(0, 36)]
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class MonoOutlineVert : GLSLVert
    {
        [PushConstant(128, 0)]
        public BufferVals vals { get; set; }

        [VertInput(0, Format.R32G32B32Sfloat)]
        public vec3 pos { get; set; }
        [VertInput(1, Format.R32G32B32Sfloat)]
        public vec3 normal { get; set; }
        [ShaderOutput(0)]
        public vec3 n { get; set; }
        [ShaderOutput(1)]
        public vec3 outPos { get; set; }

        vec3 normalFromObjectToWorld(mat4 uMatrix, vec3 normal, vec3 postion)
        {
            vec3 target = postion + normal;
            vec3 newNormal = (uMatrix * new vec4(target, 1)).xyz - (uMatrix * new vec4(postion, 1)).xyz;
            newNormal = normalize(newNormal);
            return newNormal;
        }

        public override void main()
        {
            n = normalFromObjectToWorld(vals.mm, normal, pos);
            vec3 viewNormal = new vec3(n.xy, -0.5f);
            viewNormal = normalize(viewNormal);
            outPos = (vals.mm * new vec4(pos, 1)).xyz;
            outPos = outPos + viewNormal * 8.0f;
            gl_Position = vals.mvp * new vec4(pos, 1.0f);
        }
    }

    public struct MonoMaterial
    {
        public vec4 albedo;
        public vec4 mra;
    }
    public struct BufferVals
    {
        public mat4 mvp;
        public mat4 mm;
    }

    public struct MonoParameter
    {
        public vec3 CameraDir;
        public int ShowOutline;
        public float OutlineWidth;
        public float Width;
        public float Height;
    }
}
