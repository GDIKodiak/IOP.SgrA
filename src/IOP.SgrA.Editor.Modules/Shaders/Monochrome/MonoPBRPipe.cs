﻿using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;

namespace IOP.SgrA.Editor.Modules
{
    public partial class MonoPBRPipe : ScriptedVulkanPipeline
    {
        public MonoPBRVert Vert {  get; set; }

        public MonoPBRFrag Frag { get; set; }

        public override PipelineInputAssemblyStateCreateOption CreateInputAssemblyState()
        {
            return new PipelineInputAssemblyStateCreateOption()
            {
                PrimitiveRestartEnable = false,
                Topology = PrimitiveTopology.TriangleListWithAdjacency
            };
        }

        public override PipelineRasterizationStateCreateOption CreateRasterizationState()
        {
            return new PipelineRasterizationStateCreateOption()
            {
                PolygonMode = PolygonMode.Fill,
                CullMode = CullModeFlags.BackBit,
                FrontFace = FrontFace.CounterClockwise,
                DepthClampEnable = false,
                RasterizerDiscardEnable = false,
                DepthBiasEnable = true,
                DepthBiasConstantFactor = -1.0f,
                DepthBiasClamp = -3.0f,
                DepthBiasSlopeFactor = -2.0f,
                LineWidth = 1.0f
            };
        }

        public override PipelineDepthStencilStateCreateOption CreateDepthStencilState()
        {
            return new PipelineDepthStencilStateCreateOption()
            {
                Back = new StencilOpState()
                {
                    FailOp = StencilOp.Replace,
                    PassOp = StencilOp.Replace,
                    CompareOp = CompareOp.Always,
                    CompareMask = 0xFF,
                    Reference = 1,
                    DepthFailOp = StencilOp.Replace,
                    WriteMask = 0
                },
                Front = new StencilOpState()
                {
                    FailOp = StencilOp.Replace,
                    PassOp = StencilOp.Replace,
                    CompareOp = CompareOp.Always,
                    CompareMask = 0xFF,
                    Reference = 1,
                    DepthFailOp = StencilOp.Replace,
                    WriteMask = 0
                },
                DepthWriteEnable = true,
                DepthTestEnable = true,
                DepthCompareOp = CompareOp.Greater,
                DepthBoundsTestEnable = false,
                MinDepthBounds = 0,
                MaxDepthBounds = 0,
                StencilTestEnable = true
            };
        }
    }
}
