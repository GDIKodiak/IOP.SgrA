﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;

namespace IOP.SgrA.Editor.Modules
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class MonoOutlineFrag : GLSLFrag
    {
        [UniformBuffer(0, 0, DescriptorSetTarget.RenderObject)]
        public MonoParameter Parameters {  get; set; }
        [ShaderInput(0)]
        public vec3 GNormal { get; set; }
        [ShaderInput(1)]
        public vec3 GPosition { get; set; }
        [ShaderInput(2)]
        public vec4 LPosition {  get; set; }
        [ShaderInput(3)]
        public vec3 GDir { get; set; }

        [FragOutput(0, false)]
        public vec4 outAlbedo { get; set; }
        [FragOutput(1, false)]
        public vec4 outMRA { get; set; }
        [FragOutput(2, false)]
        public vec4 outNormal { get; set; }
        [FragOutput(3, false)]
        public vec4 outPos { get; set; }
        public override void main()
        {
            outAlbedo = new vec4(0.0f, 0.0f, 0.0f, 1.0f);
            outPos = new vec4(GPosition.xyz, gl_FragCoord.z);
            outMRA = new vec4(1.0f);
            outNormal = new vec4(GNormal.xyz, 1.0f);
        }
    }
}
