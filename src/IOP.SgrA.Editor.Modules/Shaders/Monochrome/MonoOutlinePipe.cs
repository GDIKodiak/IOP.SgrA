﻿using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Modules
{
    public partial class MonoOutlinePipe : ScriptedVulkanPipeline
    {
        public MonoOutlineVert Vert { get; set; }
        public MonoOutlineGeom Geom { get; set; }
        public MonoOutlineFrag Frag { get; set; }

        public override PipelineInputAssemblyStateCreateOption CreateInputAssemblyState()
        {
            return new PipelineInputAssemblyStateCreateOption()
            {
                PrimitiveRestartEnable = false,
                Topology = PrimitiveTopology.TriangleListWithAdjacency
            };
        }

        public override PipelineRasterizationStateCreateOption CreateRasterizationState()
        {
            return new PipelineRasterizationStateCreateOption()
            {
                PolygonMode = PolygonMode.Fill,
                CullMode = CullModeFlags.BackBit,
                FrontFace = FrontFace.CounterClockwise,
                DepthClampEnable = false,
                RasterizerDiscardEnable = false,
                DepthBiasEnable = true,
                DepthBiasConstantFactor = 1.5f,
                DepthBiasClamp = 4.0f,
                DepthBiasSlopeFactor = 3.0f,
                LineWidth = 2f
            };
        }

        public override PipelineDepthStencilStateCreateOption CreateDepthStencilState()
        {
            return new PipelineDepthStencilStateCreateOption()
            {
                Back = new StencilOpState()
                {
                    FailOp = StencilOp.Keep,
                    PassOp = StencilOp.Keep,
                    CompareOp = CompareOp.Greater,
                    CompareMask = 0xFF,
                    Reference = 2,
                    DepthFailOp = StencilOp.Keep,
                    WriteMask = 0
                },
                Front = new StencilOpState()
                {
                    FailOp = StencilOp.Keep,
                    PassOp = StencilOp.Keep,
                    CompareOp = CompareOp.Greater,
                    CompareMask = 0xFF,
                    Reference = 2,
                    DepthFailOp = StencilOp.Keep,
                    WriteMask = 0
                },
                DepthWriteEnable = true,
                DepthTestEnable = true,
                DepthCompareOp = CompareOp.GreaterOrEqual,
                DepthBoundsTestEnable = false,
                MinDepthBounds = 0,
                MaxDepthBounds = 0,
                StencilTestEnable = true
            };
        }

        public override PipelineDynamicStateCreateOption CreateDynamicState()
        {
            return new PipelineDynamicStateCreateOption()
            {
                DynamicStates =
                [
                    DynamicState.Viewport,
                    DynamicState.Scissor
                ]
            };
        }
    }
}
