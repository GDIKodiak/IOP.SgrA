﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Core.Native;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Modules
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class EditorGridFrag : GLSLFrag
    {
        [UniformBuffer(0, 0, DescriptorSetTarget.Pipeline)]
        public VPBufferVals Vals { get; set; }

        [ShaderInput(0)]
        public vec2 ScreenPos {  get; set; }

        [FragOutput(0, false, ColorBlendOp = BlendOp.Add, AlphaBlendOp = BlendOp.Add,
            SrcColorBlendFactor = BlendFactor.SrcAlpha, SrcAlphaBlendFactor = BlendFactor.SrcAlpha,
            DstColorBlendFactor = BlendFactor.OneMinusSrcAlpha, DstAlphaBlendFactor = BlendFactor.OneMinusSrcAlpha)]
        public vec4 OutColor { get; set; }
        [FragOutput(1, false)]
        public float Depth { get; set; }

        public float ComputeDepth(vec4 pos)
        {
            return pos.z / pos.w;
        }
        public float ComputeLinearDepth(vec4 pos)
        {
            float u_near = Vals.Near;
            float u_far = Vals.Far;
            int reverse = Vals.reverseZ;
            float clip_space_depth = pos.z / pos.w;
            float linearDepth;
            if (reverse == 1) linearDepth = (u_far * u_near) / ((u_far - u_near) * clip_space_depth + u_near);
            else linearDepth = (- u_far * u_near) / ((u_far - u_near) * clip_space_depth - u_far);
            linearDepth /= u_far;
            return linearDepth;
        }
        public vec3 UnprojectPoint(float x, float y, float z)
        {
            var u_viewInvMat = inverse(Vals.view);
            var u_projInvMat = inverse(Vals.proj);
            vec4 unprojectedPoint = u_viewInvMat * u_projInvMat * new vec4(x, y, z, 1.0f);
            return unprojectedPoint.xyz / unprojectedPoint.w;
        }

        public vec4 Grid(vec3 fragPos3D, float gridWidth, float lineWidth, bool drawAxis)
        {
            float wx = fragPos3D.x;
            float wz = fragPos3D.z;
            float ddx = fwidth(wx);
            float ddz = fwidth(wz);
            float x0 = abs(fract(wx / gridWidth - 0.5f) - 0.5f) / ddx * gridWidth;
            float z0 = abs(fract(wz / gridWidth - 0.5f) - 0.5f) / ddz * gridWidth;
            float v0 = clamp(min(x0, z0), 0.0f, lineWidth);
            v0 = 1.0f - smoothstep(0.0f, lineWidth, v0);
            vec4 color = new vec4(0.2745f, 0.2745f, 0.2745f, v0);
            if (drawAxis)
            {
                float mmx = smoothstep(-lineWidth, 0.0f, wz / ddz - 0.5f) - smoothstep(0.0f, lineWidth, wz / ddz - 0.5f);
                float mmz = smoothstep(-lineWidth, 0.0f, wx / ddx - 0.5f) - smoothstep(0.0f, lineWidth, wx / ddx - 0.5f);
                vec4 axis = new vec4(1.0f, 0.0f, 0.0f, 1.0f) * mmx + new vec4(0.2039f, 0.5294f, 0.9255f, 1.0f) * mmz;
                color = axis + (1.0f - axis.a) * color;
            }
            return color;
        }

        public override void main()
        {
            vec3 nearPoint = UnprojectPoint(ScreenPos.x, ScreenPos.y, 0.0f);
            vec3 farPoint = UnprojectPoint(ScreenPos.x, ScreenPos.y, 1.0f);
            float ty = -nearPoint.y / (farPoint.y - nearPoint.y);
            vec3 fragPos3D = nearPoint + ty * (farPoint - nearPoint);
            vec4 clipPos = Vals.proj * Vals.view * new vec4(fragPos3D.xyz, 1.0f);

            float depth = ComputeDepth(clipPos);
            float linearDepth = ComputeLinearDepth(clipPos);
            float fading = max(0.0f, (0.5f - linearDepth));
            var colorOne = Grid(fragPos3D, 20, 1.0f, true);
            colorOne.a *= fading;
            var colorTwo = Grid(fragPos3D, 100, 2.0f, true);
            colorTwo.a *= fading;
            var outputColor = new vec4(colorOne.rgb * colorTwo.a + colorTwo.rgb * (1.0f - colorTwo.a), colorOne.a + colorTwo.a);
            OutColor = outputColor;
            Depth = depth;

            gl_FragDepth = depth;
        }
    }
}
