﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Modules
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class EditorGridVert : GLSLVert
    {
        [UniformBuffer(0, 0, DescriptorSetTarget.Pipeline)]
        public VPBufferVals Vals { get; set; }

        [ShaderOutput(0)]
        public vec2 ScreenPos { get; set; }

        public override void main()
        {
            vec4 pos = new vec4(0.0f);
            if (gl_VertexIndex == 0)
            {
                pos = new vec4(-1.0f, -1.0f, 0.0f, 1.0f);
                ScreenPos = new vec2(-1.0f, -1.0f);
            }
            else if(gl_VertexIndex == 1)
            {
                pos = new vec4(1.0f, -1.0f, 0.0f, 1.0f);
                ScreenPos = new vec2(1.0f, -1.0f);
            }
            else if( gl_VertexIndex == 2)
            {
                pos = new vec4(-1.0f, 1.0f, 0.0f, 1.0f);
                ScreenPos = new vec2(-1.0f, 1.0f);
            }
            else if(gl_VertexIndex == 3)
            {
                pos = new vec4(1.0f, 1.0f, 0.0f, 1.0f);
                ScreenPos = new vec2(1.0f, 1.0f);
            }
            gl_Position = pos;
        }
    }
}
