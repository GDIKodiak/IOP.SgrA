﻿using IOP.SgrA.ECS;
using IOP.SgrA.Modeling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Modules.ECS
{
    public class SceneObjectSystem : ComponentSystem
    {
        public Quaternion ModelCoordBias { get; } = Quaternion.CreateFromAxisAngle(Vector3.UnitX, -90f * MathF.PI / 180.0f);
        private float angle = 270;

        public override void Initialization()
        {
            Vector3 v = Vector3.Transform(Vector3.UnitZ, 
                Quaternion.CreateFromAxisAngle(Vector3.UnitX, angle.ToRadians()));
            Owner.LightEnvironment.ChangeParallelLight("ParallelLight", v);
        }

        public override void Update(TimeSpan lastStamp)
        {
            ContextManager.Foreach<MonochromeGroup>((group) =>
            {
                var context = ContextManager.GetContext(group.Entity);
                var c = group.Monochrome.Color;
                var render = context.GetContextRenderObject();
                var t = render.GetTransform();
                var b = render.GetComponents<IMesh>().First();
                var bb = render.GetBounds();
                var m = render.GetComponents<IMaterial>().FirstOrDefault();
                m?.UpdateToBuffer();
                context.PushToRenderGroup();
                foreach(var child in render.GetChildrens())
                {
                    if(child is MonoOutlineRender)
                    {
                        var tChild = child.GetTransform();
                        tChild.Position = t.Position;
                        tChild.Rotation = t.Rotation;
                        tChild.Scaling = t.Scaling;
                        if (child.TryGetRenderGroup(out var renderGroup))
                        {
                            renderGroup.PushContext(child);
                        }
                    }
                }
            });
        }
    }
}
