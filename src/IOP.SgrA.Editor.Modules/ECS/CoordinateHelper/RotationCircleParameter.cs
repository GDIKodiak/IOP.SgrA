﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Modules
{
    public class RotationCircleParameter : IRenderComponent
    {
        public string Name => nameof(RotationCircleParameter);

        /// <summary>
        /// 是否被选中
        /// </summary>
        public bool IsSelected { get; set; }
        /// <summary>
        /// 光标是否进入
        /// </summary>
        public bool IsPointerEnter { get; set; }
        public Vector3 Color { get; set; }
        public Vector3 Center { get; set; }
        public Vector2 PointerPos { get; set; }
        /// <summary>
        /// 圆环面参数
        /// t.x：圆心到环中轴线半径
        /// t.y：环切面半径
        /// </summary>
        public Vector2 T {  get; set; }
        /// <summary>
        /// 开始旋转时起始WinPos
        /// </summary>
        public Vector2 StartPos { get; set; }
        /// <summary>
        /// 上一帧WinPos
        /// </summary>
        public Vector2 LastPos { get; set; }
        /// <summary>
        /// 缩放系数
        /// </summary>
        public float Scale {  get; set; }
        /// <summary>
        /// 累计旋转角度
        /// </summary>
        public float Theta { get; set; }
        /// <summary>
        /// 组件类型
        /// </summary>
        public RotationHelperType HelperType { get; set; }

        public void CloneToNewRender(IRenderObject newObj)
        {
            var para = new ArrowParameter();
            newObj.AddComponent(para);
        }

        public void Destroy()
        {
        }

        public void OnAttach(IRenderObject renderObject)
        {
        }
    }

    public enum RotationHelperType : int
    {
        Sphere = 0,
        TorusX = 1,
        TorusY = 2,
        TorusZ = 3
    }
}
