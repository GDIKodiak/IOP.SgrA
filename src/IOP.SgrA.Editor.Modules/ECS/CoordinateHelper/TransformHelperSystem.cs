﻿using IOP.SgrA.ECS;
using IOP.SgrA.Input;
using System.Diagnostics;
using System.Numerics;

namespace IOP.SgrA.Editor.Modules.ECS
{
    public class TransformHelperSystem : ComponentSystem
    {

        protected Camera EditorCamera { get; set; }
        protected IGraphicsManager GraphicsManager { get; set; }

        /// <summary>
        /// 指示器所在位置
        /// </summary>
        protected Vector3 HelperPosition { get; set; }
        /// <summary>
        /// 显示箭头指示器
        /// </summary>
        protected bool ShowArrowHelper { get; set; }
        /// <summary>
        /// 显示旋转帮助器
        /// </summary>
        protected bool ShowRotationHelper { get; set; } = true;

        protected const int MAXSDFSTEP = 50;

        protected Entity SelectedHelper { get; private set; } = Entity.Zero;
        protected bool PressRayMachingLost { get; set; } = true;

        public TransformHelperSystem(IGraphicsManager graphicsManager)
        {
            GraphicsManager = graphicsManager;
        }

        public override void Initialization()
        {
            GraphicsManager.TryGetCamera("MainCamera", out Camera camera);
            EditorCamera = camera;
        }
        public override void Update(TimeSpan lastStamp)
        {
            if (EditorCamera == null) return;
            var inputPosition = Input.GetMousePosition();
            var inputState = Input.GetMouseButtonState(MouseButton.ButtonLeft);
            var ray = EditorCamera.GenerateRay(inputPosition);
            var pos = HelperPosition;
            var diff = new Vector4(pos - EditorCamera.EyePosition, 1.0f);
            diff = Vector4.Transform(diff, EditorCamera.ViewMatrix);
            float scale = MathF.Abs(diff.Z / EditorCamera.Far);
            if (SelectedHelper != Entity.Zero && SelectedHelper != Entity.NULL && inputState.Action == InputAction.Press)
            {
                if (ShowArrowHelper)
                {
                    var context = ContextManager.GetContext(SelectedHelper);
                    var render = context.GetContextRenderObject();
                    TranslationRender(context, scale);
                    context.PushToRenderGroup();
                }
                else if (ShowRotationHelper)
                {
                    var context = ContextManager.GetContext(SelectedHelper);
                    var render = context.GetContextRenderObject();
                    TranslationRender(context, scale);
                    var para = render.GetComponent<RotationCircleParameter>(nameof(RotationCircleParameter));
                    para.Scale = scale;
                    CalculateTheta(para, inputPosition);
                    para.PointerPos = inputPosition;
                    context.PushToRenderGroup();
                }
                return;
            }
            if (inputState.Action == InputAction.Release)
            {
                PressRayMachingLost = true;
                SelectedHelper = Entity.Zero;
            }
            if (ShowArrowHelper) TranslationHelperHandle(scale, ray, inputState);
            if (ShowRotationHelper) RotationHelperHandle(scale, ray, inputPosition, inputState);
        }

        
        protected bool RayMachingRoationHelper(Ray ray, Vector2 tPara, Vector3 center, RotationHelperType type)
        {
            float depth = 0.0f;
            bool result = false;
            for (int i = 0; i < MAXSDFSTEP; i++)
            {
                Vector3 pos = ray.Origin + ray.Direction * depth;
                float dist;
                switch (type)
                {
                    case RotationHelperType.Sphere:
                        dist = SphereSDF(pos, tPara.X);
                        break;
                    case RotationHelperType.TorusX:
                        dist = TorusSDFAxisX(pos, tPara);
                        break;
                    case RotationHelperType.TorusY:
                        dist = TorusSDFAxisY(pos, tPara);
                        break;
                    case RotationHelperType.TorusZ:
                        dist = TorusSDFAxisZ(pos, tPara);
                        break;
                    default:
                        return false;
                }
                depth += dist;
                if (dist < 0.01f)
                {
                    Vector3 centerDir = Vector3.Normalize(center - pos);
                    float c = Vector3.Dot(centerDir, ray.Direction);
                    result = type switch
                    {
                        RotationHelperType.TorusX => c >= -0.15f,
                        RotationHelperType.TorusY => c >= -0.10f,
                        RotationHelperType.TorusZ => c >= -0.15f,
                        _ => true,
                    };
                    break;
                }
                else if (dist > 2000) break;
            }
            return result;
        }
        protected float SphereSDF(Vector3 p, float r)
        {
            return p.Length() - r;
        }
        protected float TorusSDFAxisX(Vector3 pos, Vector2 tPara)
        {
            Vector2 q = new Vector2(new Vector2(pos.Y, pos.Z).Length() - tPara.X, pos.X);
            return q.Length() - tPara.Y;
        }
        protected float TorusSDFAxisY(Vector3 pos, Vector2 tPara)
        {
            Vector2 q = new Vector2(new Vector2(pos.X, pos.Z).Length() - tPara.X, pos.Y);
            return q.Length() - tPara.Y;
        }
        protected float TorusSDFAxisZ(Vector3 pos, Vector2 tPara)
        {
            Vector2 q = new Vector2(new Vector2(pos.X, pos.Y).Length() - tPara.X, pos.Z);
            return q.Length() - tPara.Y;
        }
        protected void TranslationRender(Context context, float scale)
        {
            var render = context.GetContextRenderObject();
            var trans = render.GetTransform();

            var modelMatrix = Matrix4x4.CreateTranslation(trans.Position) * 
                Matrix4x4.CreateFromQuaternion(trans.Rotation) * Matrix4x4.CreateScale(trans.Scaling * scale);

            ref MVPMatrix mvp = ref context.GetMVPMatrix();
            mvp.ModelMatrix = modelMatrix;
        }
        protected void RotationHelperHandle(float scale, Ray ray, Vector2 pointerPos, MouseButtonState inputState)
        {
            bool selectedRotation = false;
            ContextManager.Foreach<CircleGroup>((group) =>
            {
                var context = ContextManager.GetContext(group.Entity);
                var render = context.GetContextRenderObject();
                TranslationRender(context, scale);

                var para = render.GetComponent<RotationCircleParameter>(nameof(RotationCircleParameter));
                para.Scale = scale;

                if (!selectedRotation && SelectedHelper != Entity.NULL)
                {
                    if (RayMachingRoationHelper(ray, new Vector2(para.T.X * scale, (para.T.Y + 5.0f) * scale), Vector3.Zero, para.HelperType))
                    {
                        para.IsPointerEnter = true;
                        selectedRotation = true;
                        if (inputState.Action == InputAction.Press)
                        {
                            SelectedHelper = group.Entity;
                            PressRayMachingLost = false;
                            para.IsSelected = true;
                            para.PointerPos = pointerPos;
                            para.StartPos = pointerPos;
                            para.LastPos = pointerPos;
                            para.Theta = 0;
                        }
                    }
                    else
                    {
                        para.IsPointerEnter = false;
                        para.IsSelected = false;
                    }
                }
                else
                {
                    para.IsPointerEnter = false;
                    para.IsSelected = false;
                }
                context.PushToRenderGroup();
            });
            if (inputState.Action == InputAction.Press && PressRayMachingLost)
            {
                SelectedHelper = Entity.NULL;
            }
        }
        protected void TranslationHelperHandle(float scale, Ray ray, MouseButtonState inputState)
        {
            bool selectedArrow = false;
            ContextManager.Foreach<ArrowGroup>((group) =>
            {
                var context = ContextManager.GetContext(group.Entity);
                var render = context.GetContextRenderObject();
                TranslationRender(context, scale);
                var modelMatrix = context.GetMVPMatrix().ModelMatrix;

                var para = render.GetComponent<ArrowParameter>(nameof(ArrowParameter));
                if (!selectedArrow && SelectedHelper != Entity.NULL)
                {
                    var bounds = render.GetBounds();
                    var aabb = bounds.Transform(modelMatrix);
                    if (EditorCamera.RayPicking(ray, aabb))
                    {
                        para.IsPointerEnter = true;
                        selectedArrow = true;
                        if (inputState.Action == InputAction.Press)
                        {
                            SelectedHelper = group.Entity;
                            PressRayMachingLost = false;
                        }
                    }
                    else para.IsPointerEnter = false;
                }
                else para.IsPointerEnter = false;
                context.PushToRenderGroup();
            });
            if (inputState.Action == InputAction.Press && PressRayMachingLost)
            {
                SelectedHelper = Entity.NULL;
            }
        }

        #region RotationHelpler
        private Vector2 ToNDCPos(Vector2 winPos)
        {
            float ndcX = winPos.X * 2.0f / Owner.RenderArea.Width - 1.0f;
            float ndcY = 1.0f - (winPos.Y * 2.0f / Owner.RenderArea.Height);
            return new Vector2(ndcX, ndcY);
        }
        private Vector2 ToNDCPos(Matrix4x4 mvp, Vector4 pos)
        {
            Vector4 clip = Vector4.Transform(pos, mvp);
            clip = new Vector4(new Vector3(clip.X, clip.Y, clip.Z) / clip.W, 1.0f);
            return new Vector2(clip.X, clip.Y);
        }
        private void CalculateTheta(RotationCircleParameter para, Vector2 currentPos)
        {
            Vector2 lastPos = para.LastPos;
            if (lastPos.X == currentPos.X && lastPos.Y == currentPos.Y) return;
            var model = Matrix4x4.CreateTranslation(para.Center);
            var mvp = model * EditorCamera.ViewMatrix * EditorCamera.ProjectionMatrix;
            Vector4 tCenter = new Vector4(para.Center, 1.0f);
            Vector2 nCenter = ToNDCPos(mvp, tCenter);
            Vector2 last = ToNDCPos(para.LastPos);
            Vector2 curr = ToNDCPos(currentPos);
            last = Vector2.Normalize(nCenter - last);
            curr = Vector2.Normalize(nCenter - curr);
            float theta = MathF.Acos(Vector2.Dot(last, curr));
            if(!float.IsNaN(theta))
            {
                float sign = last.X * curr.Y - last.Y * curr.X;
                theta = sign < 0 ? -theta : theta;
                para.Theta += theta;
            }
            Debug.WriteLine($"{para.Theta}");
            para.LastPos = currentPos;
        }
        #endregion
    }

    public struct ArrowGroup
    {
        public Entity Entity;
        public CoordinateArrowComponent ArrowComponent;
    }

    public struct CircleGroup
    {
        public Entity Entity;
        public RotationCircleComponent CircleComponent;
    }
}
