﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Modules.ECS
{
    public class TranslateRender : RenderObject
    {

        public override void Update()
        {
            var trans = GetTransform();
            foreach (var child in Children)
            {
                var cTrans = child.GetTransform();
                cTrans.Position = trans.Position;
                cTrans.Rotation = trans.Rotation;
                if(child.TryGetRenderGroup(out IRenderGroup group))
                {
                    group.PushContext(child);
                }
            }
        }

        /// <summary>
        /// 检测是否有对象被选中
        /// </summary>
        /// <param name="camera"></param>
        /// <param name="selected"></param>
        /// <returns></returns>
        public bool SelectedCheck(Camera camera, Scene scene, IInputStateController input, out IRenderObject selected)
        {
            var inputPosition = input.GetMousePosition();
            var ray = camera.GenerateRay(inputPosition);
            selected = null;
            bool found = false;
            foreach (var child in Children)
            {
                if (child.TryGetComponent<ArrowParameter>(nameof(ArrowParameter), out var para)) 
                {
                    var cTrans = child.GetTransform();
                    Matrix4x4 model = cTrans.CreateMatrix() * scene.WorldMatrix;
                    var bounds = child.GetBounds();
                    var aabb = bounds.Transform(model);
                    if (camera.RayPicking(ray, aabb))
                    {
                        para.IsPointerEnter = true;
                        selected = child;
                        found = true;
                    }
                    else
                    {
                        para.IsPointerEnter = false;
                    }
                }
            }
            return found;
        }
    }
}
