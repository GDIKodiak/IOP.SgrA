﻿using IOP.SgrA.ECS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Modules
{
    public struct RotationCircleComponent : IComponentData
    {
        public Vector3 Axis;
    }
}
