﻿using IOP.SgrA.ECS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Modules.ECS
{
    public struct CoordinateArrowComponent : IComponentData
    {
        public Vector3 Axis;
    }
}
