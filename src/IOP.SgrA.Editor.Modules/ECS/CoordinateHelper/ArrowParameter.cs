﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Modules
{
    /// <summary>
    /// 
    /// </summary>
    public class ArrowParameter : IRenderComponent
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name => nameof(ArrowParameter);
        /// <summary>
        /// 
        /// </summary>
        public bool IsPointerEnter {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsSelected {  get; set; }
        /// <summary>
        /// 轴向
        /// </summary>
        public Vector3 Axis { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newObj"></param>
        public void CloneToNewRender(IRenderObject newObj)
        {
            var para = new ArrowParameter();
            newObj.AddComponent(para);
        }
        /// <summary>
        /// 
        /// </summary>
        public void Destroy()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderObject"></param>
        public void OnAttach(IRenderObject renderObject)
        {
        }
    }
}
