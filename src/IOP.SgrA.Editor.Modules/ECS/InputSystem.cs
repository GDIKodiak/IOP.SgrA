﻿using IOP.Extension.DependencyInjection;
using IOP.SgrA.ECS;
using IOP.SgrA.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Modules
{
    /// <summary>
    /// 
    /// </summary>
    public class InputSystem : SyncComponentSystem
    {
        /// <summary>
        /// 
        /// </summary>
        public override int Priority => 0;
        /// <summary>
        /// 
        /// </summary>
        public override SyncExecuteTrigger ExecuteTrigger => SyncExecuteTrigger.BeforeExecuteSystem;
        /// <summary>
        /// 
        /// </summary>
        [Autowired]
        protected IGraphicsManager Manager { get; set; }
        /// <summary>
        /// 
        /// </summary>
        protected Camera Camera { get; set; }

        public override void Initialization()
        {
            Camera camera = null;
            Manager?.TryGetCamera("MainCamera", out camera);
            if (camera != null) Camera = camera;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="lastStamp"></param>
        public override void Update(TimeSpan lastStamp)
        {
            if (Camera == null) return;
            if (Input.GetMouseButtonState(MouseButton.ButtonRight).Action == InputAction.Press)
            {
                var delta = Input.GetMousePositionDelta();
                if (delta.X != 0 || delta.Y != 0)
                {
                    Camera.EncircleFromMouse(Vector3.Zero, delta.X, delta.Y, (float)(0.5 * Math.PI / 180.0));
                    //MessageBus.Current.SendMessage(new CameraEventArgs(Camera.EyePosition));
                }
            }
            int w = Input.GetMouseWheelDelta();
            if (w != 0)
            {
                Camera.Scroll(w * 10f);
                EventBus.Publish("OnCameraScroll", new CameraScrollEventArgs(w * 10f), EventTrigger.SystemFrameBefore);
            }
        }
    }
}
