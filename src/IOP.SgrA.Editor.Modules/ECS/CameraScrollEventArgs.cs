﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Modules
{
    public class CameraScrollEventArgs
    {
        public float Scroll {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="scroll"></param>
        public CameraScrollEventArgs(float scroll)
        {
            Scroll = scroll;
        }
    }
}
