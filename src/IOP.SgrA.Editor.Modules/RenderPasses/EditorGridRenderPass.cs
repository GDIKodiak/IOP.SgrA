﻿using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Modules
{
    [RenderPass("", RenderPassMode.Normal)]
    [Subpass(0, PipelineBindPoint.Graphics)]
    public partial class EditorGridRenderPass : ScriptedVulkanRenderPass
    {
        [Attachment(ImageLayout.General, Format.R16G16B16A16Sfloat, StoreOp = AttachmentStoreOp.Store)]
        [SubpassAttachment(0, SubpassAttachmentType.ColorAttachment, ImageLayout.ColorAttachmentOptimal)]
        [ImageConfig(Usage = ImageUsageFlags.ColorAttachmentBit | ImageUsageFlags.SampledBit, ImageType = ImageType.Type2D, ViewType = ImageViewType.Type2D)]
        [AttachmentClearValue(0.0f, 0.0f, 0.0f, 0.0f)]
        public RenderPassAttachment Output { get; set; }

        [Attachment(ImageLayout.DepthStencilAttachmentOptimal, Format.D16Unorm, StoreOp = AttachmentStoreOp.Store)]
        [SubpassAttachment(0, SubpassAttachmentType.DepthStencilAttachment, ImageLayout.DepthStencilAttachmentOptimal)]
        [ImageConfig(Usage = ImageUsageFlags.DepthStencilAttachmentBit, SubresourceRangeAspect = ImageAspectFlags.DepthBit, 
            ImageType = ImageType.Type2D, ViewType = ImageViewType.Type2D)]
        [AttachmentClearValue(0.0f, 0)]
        public RenderPassAttachment Depth { get; set; }

        [Attachment(ImageLayout.General, Format.R16Sfloat, StoreOp = AttachmentStoreOp.Store)]
        [SubpassAttachment(0, SubpassAttachmentType.ColorAttachment, ImageLayout.ColorAttachmentOptimal)]
        [ImageConfig(Usage = ImageUsageFlags.ColorAttachmentBit | ImageUsageFlags.SampledBit, SubresourceRangeAspect = ImageAspectFlags.ColorBit,
            ImageType = ImageType.Type2D, ViewType = ImageViewType.Type2D)]
        [AttachmentClearValue(0f, 0f, 0f, 0f)]
        public RenderPassAttachment OutputDepth { get; set; }

        [SubpassDependency(SrcSubpass = 0, DstSubpass = uint.MaxValue, 
            SrcStageMask = PipelineStageFlags.ColorAttachmentOutputBit, DstStageMask = PipelineStageFlags.BottomOfPipeBit,
            SrcAccessMask = AccessFlags.ColorAttachmentWriteBit,
            DstAccessMask = 0,
            DependencyFlags = DependencyFlags.ByRegionBit)]
        public PassDependency Dependency1 { get; set; }
    }
}
