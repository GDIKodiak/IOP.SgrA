﻿using CodeWF.EventBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Modules.Bus
{
    public class NewCoordinateHelperEventArgs : Command
    {
        /// <summary>
        /// 
        /// </summary>
        public IRenderObject Owner { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public CoordinateHelperType HelperType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="helperType"></param>
        public NewCoordinateHelperEventArgs(IRenderObject owner, CoordinateHelperType helperType)
        {
            Owner = owner;
            HelperType = helperType;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public enum CoordinateHelperType
    {
        TranslateHelper,
        RotationHelper,
        ScalingHelper
    }
}
