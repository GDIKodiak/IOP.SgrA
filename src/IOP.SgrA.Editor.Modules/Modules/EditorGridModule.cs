﻿using IOP.SgrA.Editor.VulkanEngine;
using IOP.SgrA.SilkNet.Vulkan;
using Microsoft.Extensions.Logging;
using Silk.NET.Vulkan;
using System;
using System.Buffers;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Modules
{
    [ModulePriority(ModulePriority.RenderGroup)]
    [ParentModule(typeof(OITModule))]
    public class EditorGridModule : VulkanModule, ISceneModule
    {
        /// <summary>
        /// 
        /// </summary>
        public Scene Scene { get; set; }


        private readonly ILogger<EditorGridModule> Logger;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        public EditorGridModule(ILogger<EditorGridModule> logger)
        {
            Logger = logger;
        }

        protected override Task Load(VulkanGraphicsManager manager)
        {
            try
            {
                var lDevice = manager.VulkanDevice;
                var pipeCache = lDevice.PipelineCache;
                var basePath = Path.GetDirectoryName(GetType().Assembly.Location);
                if (Parent is not OITModule oitModule) throw new NullReferenceException($"Cannot found Parent module {nameof(PBRModule)}");
                var group = oitModule.OITGroup ?? throw new NullReferenceException("Please Create PBRGroup first");
                var r = manager.TryGetCamera("MainCamera", out Camera camera);
                if (!r) throw new NullReferenceException("Cannot get camera with name MainCamera");
                var area = Scene.RenderArea;
                var sceneGroup = Scene.RenderGroup as PrimaryVulkanRenderGroup;

                var editorGroup = CreateMainEditorGridGroup(manager, sceneGroup, Scene, pipeCache, camera);
            }
            catch (Exception e)
            {
                Logger?.LogError(e, "");
            }
            return Task.CompletedTask;
        }

        protected override Task Unload(VulkanGraphicsManager manager)
        {
            return Task.CompletedTask;
        }

        private PrimaryVulkanRenderGroup CreateMainEditorGridGroup(VulkanGraphicsManager manager,
            PrimaryVulkanRenderGroup parentMain, Scene scene,
            PipelineCache cache, Camera camera)
        {
            var lDevice = manager.VulkanDevice;
            var area = scene.RenderArea;
            var pass = lDevice.CreateScriptedRenderPass<EditorGridRenderPass>(area);
            var pCommand = lDevice.CreateCommandPool((device, option) =>
            {
                option.Flags = CommandPoolCreateFlags.ResetCommandBufferBit;
                option.QueueFamilyIndex = device.WorkQueues[0].FamilyIndex;
            }).CreatePrimaryCommandBuffer();
            var linePipe = manager.BuildScriptedShaderAndPipeline<EditorGridLinePipe>("EditorGridLinePipe", pass, cache, Scene.RenderArea)
                .BuildUniformBuffer(0, 0, 204, SharingMode.Exclusive);
            var semaphore = lDevice.CreateSemaphore();
            var editorGridGroup = manager.CreatePrimaryRenderGroup("", linePipe, GroupRenderMode.Transparent)
                .Binding(pCommand, lDevice, pass, [semaphore.RawHandle], [], []);
            editorGridGroup.CreateGroupRenderingAction((builder) => builder.Run((group) => EditorGridMain(group)));
            editorGridGroup.SetCamera(camera);

            var image1 = pass.Framebuffer[0].Attachments[0];
            var image2 = pass.Framebuffer[0].Attachments[2];
            manager.CreateEngineAreaRenderingObject(Scene, editorGridGroup, "EditorGridRenderArea", image1, image2);
            return editorGridGroup;
        }

        private Matrix4x4 PreViewMatrix = Matrix4x4.Identity;
        private int FrameIndex = 0;
        private bool first = true;

        private void EditorGridMain(PrimaryVulkanRenderGroup group)
        {
            try
            {
                var queue = group.WorkQueues[0].Queue;
                var cmdBuffer = group.PrimaryCommandBuffer;
                var semaphores = group.Semaphores;
                var pass = group.RenderPass;
                var lDevice = group.LogicDevice;
                var camera = group.Camera;
                var pipeline = group.Pipeline;

                VulkanFrameBuffer framebuffer = group.RenderPass.GetFramebuffer(0);

                cmdBuffer.Reset();
                cmdBuffer.Begin();
                cmdBuffer.BeginRenderPass(pass, framebuffer, pass.BeginOption, SubpassContents.Inline);
                cmdBuffer.BindPipeline(PipelineBindPoint.Graphics, pipeline);
                var viewPort = camera.Viewport;
                cmdBuffer.SetViewport(new Viewport(viewPort.X, viewPort.Y, viewPort.Width, viewPort.Height, viewPort.MinDepth, viewPort.MaxDepth));
                cmdBuffer.SetScissor(new Rect2D(null, new Extent2D((uint)viewPort.Width, (uint)viewPort.Height)));

                int size = Marshal.SizeOf<Matrix4x4>();
                byte[] bytes = ArrayPool<byte>.Shared.Rent(size * 3 + 12);
                Span<byte> data = bytes; data = data[..(size * 3 + 12)];
                var view = camera.ViewMatrix;
                var proj = camera.ProjectionMatrix;
                if (first)
                {
                    PreViewMatrix = view;
                    first = !first;
                }
                var preView = PreViewMatrix;
                Vector2 local = new Vector2(camera.Near, camera.Far);
                int reverse = camera.ReversedZ ? 1 : 0;
                view.ToBytes(ref data, 0);
                proj.ToBytes(ref data, size);
                preView.ToBytes(ref data, size * 2);
                MemoryMarshal.Write(data[(size * 3)..], local);
                MemoryMarshal.Write(data[(size * 3 + 8)..], reverse);

                var tex = group.Pipeline.GetPipelineTexture(0, 0);
                tex.UpdateTextureMemoryData(data, 0);
                cmdBuffer.BindDescriptorSets(PipelineBindPoint.Graphics, pipeline.PipelineLayout, 0, null, tex.DescriptorSet);
                cmdBuffer.Draw(4, 1, 0, 0);
                cmdBuffer.EndRenderPass();
                cmdBuffer.End();
                var submitInfo = new SubmitOption
                {
                    WaitDstStageMask = null,
                    WaitSemaphores = null,
                    Buffers = [cmdBuffer.RawHandle],
                    SignalSemaphores = [semaphores[0]]
                };
                lDevice.Submit(queue, new Fence(0), submitInfo);
                ArrayPool<byte>.Shared.Return(bytes);
                PreViewMatrix = view;
            }
            catch (Exception e)
            {
                group.Disable();
                group.Logger?.LogError(e, e.Message);
            }
        }
    }
}
