﻿using CodeWF.EventBus;
using IOP.SgrA.Editor.Modules.ECS;
using IOP.SgrA.Editor.VulkanEngine.Bus;
using IOP.SgrA.SilkNet.Vulkan;
using Microsoft.Extensions.Logging;
using Silk.NET.Vulkan;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Modules
{
    [ModulePriority(ModulePriority.RenderGroup)]
    [ParentModule(typeof(PBRModule))]
    public class MonochromeModule : VulkanModule, ISceneModule
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly ILogger<MonochromeModule> Logger;
        /// <summary>
        /// 
        /// </summary>
        private readonly IEventBus EventBus;
        public Scene Scene { get; set; }

        public IRenderGroup RenderGroup { get; private set; }

        public IRenderGroup MonoOutlineGroup { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        public MonochromeModule(ILogger<MonochromeModule> logger, IEventBus eventBus)
        {
            Logger = logger;
            EventBus = eventBus;
            eventBus.Subscribe<SceneAddEventArgs>(NewMonoObject);
            eventBus.Subscribe<SceneAddManyEventArgs>(NewMonoObjects);
        }

        protected override Task Load(VulkanGraphicsManager manager)
        {
            var lDevice = manager.VulkanDevice;
            var pipeCache = lDevice.PipelineCache;
            var basePath = Path.GetDirectoryName(GetType().Assembly.Location);
            if (Parent is not PBRModule pbrModule) throw new NullReferenceException($"Cannot found Parent module {nameof(PBRModule)}");
            var group = pbrModule.PBRGroup ?? throw new NullReferenceException("Please Create PBRGroup first");
            try
            {
                var r = manager.TryGetCamera("MainCamera", out Camera camera);
                if (!r) throw new NullReferenceException("Cannot get camera with name MainCamera");
                var area = Scene.RenderArea;
                CreateMonochromeRenderGroup(manager, group, pipeCache, lDevice, camera);
            }
            catch (Exception e)
            {
                Logger?.LogError(e, "");
            }
            return Task.CompletedTask;
        }

        protected override Task Unload(VulkanGraphicsManager manager)
        {
            return Task.CompletedTask;
        }

        private void CreateMonochromeRenderGroup(VulkanGraphicsManager manager, 
            PrimaryVulkanRenderGroup mainGroup, PipelineCache cache, VulkanDevice device, Camera camera)
        {
            var monoOutlinePipe = manager.BuildScriptedShaderAndPipeline<MonoOutlinePipe>(manager.NewStringToken(),
                mainGroup.RenderPass, cache, Scene.RenderArea);
            var monoPBRPipe = manager.BuildScriptedShaderAndPipeline<MonoPBRPipe>(manager.NewStringToken(),
                mainGroup.RenderPass, cache, Scene.RenderArea);
            var sCommand = device.CreateCommandPool((device, option) =>
            {
                option.Flags = CommandPoolCreateFlags.ResetCommandBufferBit;
                option.QueueFamilyIndex = device.WorkQueues[0].FamilyIndex;
            }).CreateSecondaryCommandBuffer();
            var sGroup = manager.CreateSecondaryVulkanRenderGroup("MonochromeOutlineGroup", monoOutlinePipe) 
                .Binding(mainGroup, sCommand, Array.Empty<VulkanSemaphore>(), Array.Empty<VulkanFence>());
            sGroup.SetCamera(camera);
            sGroup.CreateGroupRenderingAction((builder) => builder.Run((group) => MonochromeOutlineMain(group)));

            var bGroup = manager.CreateSecondaryVulkanRenderGroup("MonochromeGroup", monoPBRPipe).Binding(sGroup);
            bGroup.SetCamera(camera);
            bGroup.CreateGroupRenderingAction((builder) => builder.Run((group) => MonochromeObjMain(group)));

            RenderGroup = bGroup;
            MonoOutlineGroup = sGroup;
        }

        private void MonochromeOutlineMain(SecondaryVulkanRenderGroup group)
        {
            var camera = group.Camera;
            var view = camera.ViewMatrix;
            var project = camera.ProjectionMatrix;
            var cmd = group.SecondaryCommandBuffer;
            CommandBufferInheritanceInfo info = new()
            {
                Framebuffer = group.Framebuffer.RawHandle,
                OcclusionQueryEnable = false,
                RenderPass = group.RenderPass.RawHandle,
                Subpass = 0
            };
            cmd.Reset();
            cmd.Begin(CommandBufferUsageFlags.OneTimeSubmitBit |
                CommandBufferUsageFlags.RenderPassContinueBit, info);

            foreach (var child in group.GetChildrens())
            {
                child.GroupRendering();
            }

            var viewPort = camera.Viewport;
            cmd.BindPipeline(PipelineBindPoint.Graphics, group.Pipeline);
            cmd.SetViewport(new Viewport(viewPort.X, viewPort.Y, viewPort.Width, viewPort.Height, viewPort.MinDepth, viewPort.MaxDepth));
            cmd.SetScissor(new Rect2D(null, new Extent2D((uint)viewPort.Width, (uint)viewPort.Height)));

            foreach (var context in group.GetContexts())
            {
                context.SetViewMatrix(in view);
                context.SetProjectionMatrix(in project);
                context.SetCamera(camera);
                var render = context.GetContextRenderObject();
                var pipeline = context.VulkanPipeline;
                int size = Marshal.SizeOf<Matrix4x4>();
                ref MVPMatrix local = ref context.GetMVPMatrix();
                var t = render.GetTransform();
                local.ModelMatrix = t.CreateMatrix() * Scene.WorldMatrix;
                Matrix4x4 mat = local.GetFinalMatrix();
                byte[] data = ArrayPool<byte>.Shared.Rent(size * 2);
                Span<byte> span = data;
                span = span[..(size * 2)];
                mat.ToBytes(ref span, 0);
                local.ModelMatrix.ToBytes(ref span, size);

                var vro = render.GetComponents<VRO>().First();
                var tex = render.GetComponents<VulkanTexture>().FirstOrDefault(x => x.Binding == 0);
                Span<float> uniform = [camera.N.X, camera.N.Y, camera.N.Z, 1.0f, 2.0f, viewPort.Width, viewPort.Height];
                tex.UpdateTextureMemoryData(MemoryMarshal.AsBytes(uniform));

                cmd.BindDescriptorSets(PipelineBindPoint.Graphics, pipeline.PipelineLayout, 0, null, [tex.GetDescriptorSet()]);
                cmd.PushConstants(pipeline.PipelineLayout, ShaderStageFlags.VertexBit, 0, span);
                cmd.BindVertexBuffers(0, [0], vro.VerticesBufferInfo.Buffer);
                cmd.BindIndexBuffer(vro.IndexesBufferInfo.Buffer, 0);
                cmd.DrawIndexed(vro.IndexesCount, 1, 0, 0, 0);

                ArrayPool<byte>.Shared.Return(data);
            }

            cmd.End();
        }

        private void MonochromeObjMain(SecondaryVulkanRenderGroup group)
        {
            var camera = group.Camera;
            var view = camera.ViewMatrix;
            var project = camera.ProjectionMatrix;
            var cmd = group.SecondaryCommandBuffer;
            var viewPort = camera.Viewport;

            cmd.BindPipeline(PipelineBindPoint.Graphics, group.Pipeline);
            cmd.SetViewport(new Viewport(viewPort.X, viewPort.Y, viewPort.Width, viewPort.Height, viewPort.MinDepth, viewPort.MaxDepth));
            cmd.SetScissor(new Rect2D(null, new Extent2D((uint)viewPort.Width, (uint)viewPort.Height)));
            foreach (var context in group.GetContexts())
            {
                context.SetViewMatrix(in view);
                context.SetProjectionMatrix(in project);
                context.SetCamera(camera);
                var render = context.GetContextRenderObject();
                var pipeline = context.VulkanPipeline;
                int size = Marshal.SizeOf<Matrix4x4>();
                ref MVPMatrix local = ref context.GetMVPMatrix();
                var t = render.GetTransform();
                local.ModelMatrix = t.CreateMatrix() * Scene.WorldMatrix;
                Matrix4x4 mat = local.GetFinalMatrix();
                byte[] data = ArrayPool<byte>.Shared.Rent(size * 2);
                Span<byte> span = data;
                span = span[..(size * 2)];
                mat.ToBytes(ref span, 0);
                local.ModelMatrix.ToBytes(ref span, size);
                var vro = render.GetComponents<VRO>().First();
                var tex = render.GetComponents<VulkanTexture>().FirstOrDefault(x => x.Binding == 1);
                Span<float> uniform = [camera.N.X, camera.N.Y, camera.N.Z, 1.0f, 2.0f, viewPort.Width, viewPort.Height];
                tex.UpdateTextureMemoryData(MemoryMarshal.AsBytes(uniform));

                cmd.BindDescriptorSets(PipelineBindPoint.Graphics, pipeline.PipelineLayout, 0, null, [tex.GetDescriptorSet()]);
                cmd.PushConstants(pipeline.PipelineLayout, ShaderStageFlags.VertexBit, 0, span);
                cmd.BindVertexBuffers(0, [0], vro.VerticesBufferInfo.Buffer);
                cmd.BindIndexBuffer(vro.IndexesBufferInfo.Buffer, 0);
                cmd.DrawIndexed(vro.IndexesCount, 1, 0, 0, 0);

                ArrayPool<byte>.Shared.Return(data);
            }
        }

        private void NewMonoObject(SceneAddEventArgs args)
        {
            if (args == null || args.RenderObject == null) return;
            try
            {
                AddOutlineRenderObject(args.RenderObject);
                VulkanManager.CreateContext(Scene, 1, "Monochrome", RenderGroup.Name, args.RenderObject);
            }
            catch (Exception e)
            {
                Logger?.LogError(e, "");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        private void NewMonoObjects(SceneAddManyEventArgs args)
        {
            if (args == null) return;
            try
            {
                foreach(var obj in args.Objects)
                {
                    AddOutlineRenderObject(obj);
                    VulkanManager.CreateContext(Scene, 1, "Monochrome", RenderGroup.Name, obj);
                }
            }
            catch (Exception e)
            {
                Logger?.LogError(e, "");
            }
        }

        private void AddOutlineRenderObject(IRenderObject parent)
        {
            if(parent == null) return;
            var graphicsManager = VulkanManager;
            var c1 = graphicsManager.CreateUniformBuffer(64, 0, 0, 1, 0);
            var mesh = parent.GetComponents<VRO>().FirstOrDefault();
            if(mesh != null)
            {
                var outlineRender = graphicsManager.CreateRenderObject<MonoOutlineRender>(graphicsManager.NewStringToken(), mesh, c1);
                outlineRender.SetBounds(mesh.MinVector, mesh.MinVector);
                outlineRender.AddComponent(MonoOutlineGroup);
                parent.AddChildren(outlineRender);
            }
        }
    }
}
