﻿using IOP.SgrA.Editor.VulkanEngine;
using IOP.SgrA.SilkNet.Vulkan;
using Microsoft.Extensions.Logging;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Semaphore = Silk.NET.Vulkan.Semaphore;

namespace IOP.SgrA.Editor.Modules
{
    [ModulePriority(ModulePriority.RenderPass)]
    public class PBRModule : VulkanModule, ISceneModule
    {
        private readonly ILogger<PBRModule> Logger;

        public Scene Scene { get; set; }

        public PrimaryVulkanRenderGroup PBRGroup { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        public PBRModule(ILogger<PBRModule> logger)
        {
            Logger = logger;
        }

        protected override Task Load(VulkanGraphicsManager manager)
        {
            var lDevice = manager.VulkanDevice;
            var pipeCache = lDevice.PipelineCache;
            var basePath = Path.GetDirectoryName(GetType().Assembly.Location);
            var group = Scene.RenderGroup;
            try
            {
                if (group == null) throw new NullReferenceException(nameof(Scene.RenderGroup));
                if (group is not PrimaryVulkanRenderGroup mainGroup) throw new InvalidCastException($"scene render group is not {nameof(PrimaryVulkanRenderGroup)}, " +
                    $"invalid to get render pass");
                var r = manager.TryGetCamera("MainCamera", out Camera camera);
                if (!r) throw new NullReferenceException("Cannot get camera with name MainCamera");
                var pbrGroup = CreatePBRMainGroup(manager, lDevice, pipeCache, camera, mainGroup, Scene);
                PBRGroup = pbrGroup;
            }
            catch (Exception e)
            {
                Logger?.LogError(e, "");
            }
            return Task.CompletedTask;
        }

        protected override Task Unload(VulkanGraphicsManager manager)
        {
            return Task.CompletedTask;
        }

        private PrimaryVulkanRenderGroup CreatePBRMainGroup(VulkanGraphicsManager manager, 
            VulkanDevice lDevice, PipelineCache cache, Camera camera, 
            PrimaryVulkanRenderGroup sceneMain, Scene scene)
        {
            var pass = lDevice.CreateScriptedRenderPass<PBRRenderPass>(Scene.RenderArea);
            var lightPipe = manager.BuildScriptedShaderAndPipeline<PBRLightPipe>("PBRLightPipeline", pass, cache, Scene.RenderArea)
                .BuildUniformBuffer(1, 0, 56, SharingMode.Exclusive)
                .BindPointLightEnvironment(scene.LightEnvironment, 2, 0)
                .BindParallelLightEnvironment(scene.LightEnvironment, 3, 0);
            for (uint i = 0; i < 4; i++)
            {
                var input = manager.CreateTextureDataFromImage(manager.NewStringToken(), 
                    lDevice, pass.Framebuffer[0].Attachments[i], ImageLayout.ShaderReadOnlyOptimal, 
                    DescriptorType.InputAttachment, i, 0, 1);
                var tInput = manager.CreateTexture(manager.NewStringToken(), input, null, i, 0, 1, 0, DescriptorType.InputAttachment);
                lightPipe.AddPipelineTexture(0, i, tInput);
            }
            var pCommand = lDevice.CreateCommandPool((device, option) =>
            {
                option.Flags = CommandPoolCreateFlags.ResetCommandBufferBit;
                option.QueueFamilyIndex = device.WorkQueues[0].FamilyIndex;
            }).CreatePrimaryCommandBuffer();
            var semaphore = lDevice.CreateSemaphore();
            var mainGroup = manager.CreatePrimaryRenderGroup("PBRGroup", lightPipe)
                .Binding(pCommand, lDevice, pass, [semaphore.RawHandle], []);
            mainGroup.SetCamera(camera);
            mainGroup.CreateGroupRenderingAction((builder) => builder.Run((group) => PBRMain(group)));
            var image = pass.Framebuffer[0].Attachments[5];
            var image2 = pass.Framebuffer[0].Attachments[6];
            var render = manager.CreateEngineAreaRenderingObject(Scene, mainGroup, "PBRArea", image, image2);
            return mainGroup;
        }

        private void PBRMain(PrimaryVulkanRenderGroup group)
        {
            try
            {
                var queue = group.WorkQueues[0].Queue;
                var cmdBuffer = group.PrimaryCommandBuffer;
                var semaphores = group.Semaphores;
                var pass = group.RenderPass;
                var lDevice = group.LogicDevice;
                var env = Scene.LightEnvironment;
                var camera = group.Camera;
                var pipe = group.Pipeline;
                uint count = env.GetPointLightCount();
                uint coun2 = env.GetParallelLightCount();
                LightPipeBuffer buffer = new()
                {
                    Ambient = env.Ambient,
                    CamPos = new Vector4(camera.EyePosition, 1.0f),
                    CamDir = new Vector4(camera.N, 1.0f),
                    PointLCount = count,
                    DirectLCount = coun2,
                };
                var tex = pipe.GetPipelineTexture(1, 0);
                tex.UpdateTextureMemoryData(buffer);

                VulkanFrameBuffer framebuffer = group.RenderPass.GetFramebuffer(0);
                cmdBuffer.Reset();
                cmdBuffer.Begin();
                cmdBuffer.BeginRenderPass(pass, framebuffer, pass.BeginOption, SubpassContents.SecondaryCommandBuffers);
                group.SecondaryGroupRendering(pass, framebuffer);
                cmdBuffer.ExecuteCommands(group.GetUpdateSecondaryBuffers());
                cmdBuffer.NextSubpass(SubpassContents.Inline);
                cmdBuffer.BindPipeline(PipelineBindPoint.Graphics, pipe);
                var viewPort = camera.Viewport;
                cmdBuffer.SetViewport(new Viewport(viewPort.X, viewPort.Y, viewPort.Width, viewPort.Height, viewPort.MinDepth, viewPort.MaxDepth));
                cmdBuffer.SetScissor(new Rect2D(null, new Extent2D((uint)viewPort.Width, (uint)viewPort.Height)));
                cmdBuffer.BindDescriptorSets(PipelineBindPoint.Graphics,
                    pipe.PipelineLayout, 0, Array.Empty<uint>(), pipe.GetPipelineDescriptorSet(0),
                    tex.GetDescriptorSet(), pipe.GetPipelineDescriptorSet(2), pipe.GetPipelineDescriptorSet(3));
                cmdBuffer.Draw(4, 1, 0, 0);
                cmdBuffer.EndRenderPass();
                cmdBuffer.End();

                var submitInfo = new SubmitOption
                {
                    WaitDstStageMask = [],
                    WaitSemaphores = [],
                    Buffers = [cmdBuffer.RawHandle],
                    SignalSemaphores = [semaphores[0]]
                };
                lDevice.Submit(queue, new Fence(0), submitInfo);
            }
            catch (Exception e)
            {
                group.Disable();
                group.Logger?.LogError(e, e.Message);
            }
        }
    }

    struct LightPipeBuffer
    {
        public Vector4 CamPos;
        public Vector4 CamDir;
        public Vector4 Ambient;
        public uint PointLCount;
        public uint DirectLCount;
    }
}
