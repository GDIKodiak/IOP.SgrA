﻿using CodeWF.EventBus;
using IOP.SgrA.Editor.Modules.Bus;
using IOP.SgrA.Editor.Modules.ECS;
using IOP.SgrA.Editor.VulkanEngine;
using IOP.SgrA.Modeling;
using IOP.SgrA.SilkNet.Vulkan;
using Microsoft.Extensions.Logging;
using Silk.NET.Vulkan;
using System;
using System.Buffers;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Modules
{
    /// <summary>
    /// 坐标系工具模块
    /// </summary>
    [ModulePriority(ModulePriority.RenderGroup)]
    public class CoordinateHelperModule : VulkanModule, ISceneModule
    {
        private readonly ILogger<CoordinateHelperModule> Logger;

        /// <summary>
        /// 
        /// </summary>
        public Scene Scene { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public IRenderGroup CoordinateArrowGroup { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public IRenderGroup RotationCircleGroup { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public IEventBus EventBus { get; set; }

        protected VulkanDynamicBufferAlloter ArrowDynamicUniformAlloter {  get; set; } 
        protected VulkanDynamicBufferAlloter CricleDynamicUniformAlloter { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        public CoordinateHelperModule(ILogger<CoordinateHelperModule> logger, IEventBus eventBus)
        {
            Logger = logger;
            EventBus = eventBus;
            eventBus.Subscribe<NewCoordinateHelperEventArgs>(NewHelper);
        }

        protected override Task Load(VulkanGraphicsManager manager)
        {
            try
            {
                var device = manager.VulkanDevice;
                var pipeCache = device.PipelineCache;
                var r = manager.TryGetCamera("MainCamera", out Camera camera);
                if (!r) throw new NullReferenceException("Cannot get camera with name MainCamera");
                var area = Scene.RenderArea;
                var pass = device.CreateScriptedRenderPass<CoordinateHelperPass>(area);
                var group = CreateCoordinateArrowGroup(manager, device, pass, camera, area);
                CreateRotationCircleGroup(manager, group, device, pass, camera, area);
            }
            catch (Exception e)
            {
                Logger?.LogError(e, "");
            }
            return Task.CompletedTask;
        }

        protected override Task Unload(VulkanGraphicsManager manager)
        {
            return Task.CompletedTask;
        }

        #region 坐标系箭头
        private PrimaryVulkanRenderGroup CreateCoordinateArrowGroup(VulkanGraphicsManager manager, 
            VulkanDevice lDevice, VulkanRenderPass pass, Camera camera, Area area)
        {
            var pipeline = manager.BuildScriptedShaderAndPipeline<CoordinateArrowPipe>(manager.NewStringToken(), pass, lDevice.PipelineCache, area);
            var dynamicUniform = manager.CreateDynamicBufferAlloter(pipeline, 136, 10, DescriptorType.UniformBufferDynamic, 0, 0);
            ArrowDynamicUniformAlloter = dynamicUniform;
            var pCommand = lDevice.CreateCommandPool((device, option) =>
            {
                option.Flags = CommandPoolCreateFlags.ResetCommandBufferBit;
                option.QueueFamilyIndex = device.WorkQueues[0].FamilyIndex;
            }).CreatePrimaryCommandBuffer();
            var semaphore = lDevice.CreateSemaphore();
            var sGroup = manager.CreatePrimaryRenderGroup(manager.NewStringToken(), pipeline, GroupRenderMode.Transparent + 1)
                .Binding(pCommand, lDevice, pass, [semaphore.RawHandle], []);
            sGroup.SetCamera(camera);
            sGroup.CreateGroupRenderingAction((builder) => builder.Run((group) => ArrowObjMain(group)));
            CoordinateArrowGroup = sGroup;
            var image = pass.Framebuffer[0].Attachments[0];
            var image2 = pass.Framebuffer[0].Attachments[2];
            var render = manager.CreateEngineAreaRenderingObject(Scene, sGroup, "CoordinateHelperArea", image, image2);
            CreateCoordinateArrowRender(manager, sGroup);
            return sGroup;
        }
        private void CreateCoordinateArrowRender(VulkanGraphicsManager manager, PrimaryVulkanRenderGroup sGroup)
        {
            var attr = ContextManager.CreateArchetype(new CoordinateArrowComponent());
            uint vCount = 0;
            Vector3 min; Vector3 max;

            var arrowX = CoordinateArrow.CreateCoordinateArrow(new Vector3(0.9372f, 0.2627f, 0.3451f), Vector3.UnitX, out vCount, out min, out max);
            var vroX = manager.CreateMesh<Vector3>(manager.NewStringToken(), [.. arrowX], vCount, min, max);
            var bufferX = ArrowDynamicUniformAlloter.MallocBlock();
            var renderX = manager.CreateRenderObject<RenderObject>(manager.NewStringToken(), vroX, bufferX, new ArrowParameter());
            renderX.GetTransform().Scaling = new Vector3(1.0f, 1.0f, 1.0f);
            renderX.SetBounds(min, max);

            var arrowY = CoordinateArrow.CreateCoordinateArrow(new Vector3(0.5059f, 0.8f, 0.1647f), Vector3.UnitY, out vCount, out min, out max);
            var vroY = manager.CreateMesh<Vector3>(manager.NewStringToken(), [.. arrowY], vCount, min, max);
            var bufferY = ArrowDynamicUniformAlloter.MallocBlock();
            var renderY = manager.CreateRenderObject<RenderObject>(manager.NewStringToken(), vroY, bufferY, new ArrowParameter());
            renderY.GetTransform().Scaling = new Vector3(1.0f, 1.0f, 1.0f);
            renderY.SetBounds(min, max);

            var arrowZ = CoordinateArrow.CreateCoordinateArrow(new Vector3(0.2039f, 0.5294f, 0.9255f), Vector3.UnitZ, out vCount, out min, out max);
            var vroZ = manager.CreateMesh<Vector3>(manager.NewStringToken(), [.. arrowZ], vCount, min, max);
            var bufferZ = ArrowDynamicUniformAlloter.MallocBlock();
            var renderZ = manager.CreateRenderObject<RenderObject>(manager.NewStringToken(), vroZ, bufferZ, new ArrowParameter());
            renderZ.GetTransform().Scaling = new Vector3(1.0f, 1.0f, 1.0f);
            renderZ.SetBounds(min, max);

            ContextManager.CreateContexts(1, attr, sGroup, renderX);
            ContextManager.CreateContexts(1, attr, sGroup, renderY);
            ContextManager.CreateContexts(1, attr, sGroup, renderZ);
        }
        private void ArrowObjMain(PrimaryVulkanRenderGroup group)
        {
            var queue = group.WorkQueues[0].Queue;
            var semaphores = group.Semaphores;
            var camera = group.Camera;
            var view = camera.ViewMatrix;
            var viewPort = camera.Viewport;
            var project = camera.ProjectionMatrix;
            var cmd = group.PrimaryCommandBuffer;
            var pipeline = group.Pipeline;
            var lDevice = group.LogicDevice;
            var pass = group.RenderPass;


            cmd.Reset();
            cmd.Begin(CommandBufferUsageFlags.OneTimeSubmitBit);
            cmd.BeginRenderPass(pass, pass.Framebuffer[0], pass.BeginOption, SubpassContents.Inline);
            cmd.BindPipeline(PipelineBindPoint.Graphics, group.Pipeline);
            cmd.SetViewport(new Viewport(viewPort.X, viewPort.Y, viewPort.Width, viewPort.Height, viewPort.MinDepth, viewPort.MaxDepth));
            cmd.SetScissor(new Rect2D(null, new Extent2D((uint)viewPort.Width, (uint)viewPort.Height)));

            int size = 136;
            byte[] data = ArrayPool<byte>.Shared.Rent(size);
            Span<byte> span = data;
            span = span[..size];
            foreach (var context in group.GetContexts())
            {
                var render = context.GetContextRenderObject();
                ref MVPMatrix mvp = ref context.GetMVPMatrix();
                Matrix4x4 model = render.GetTransform().CreateMatrix() * Scene.WorldMatrix;
                Matrix4x4 final = model * view * project;
                final.ToBytes(ref span, 0);
                model.ToBytes(ref span, 64);
                var vro = render.GetComponents<VRO>().First();
                var para = render.GetComponent<ArrowParameter>(nameof(ArrowParameter));
                int isSelected = para.IsSelected ? 1 : 0;
                int isPointerEnter = para.IsPointerEnter ? 1 : 0;
                Vector2 state = new Vector2(isPointerEnter, isSelected);
                MemoryMarshal.Write(span.Slice(128, 8), in state);
                var uniform = render.GetComponents<VulkanDynamicBufferBlock>().First();
                uniform.UpdateBlockData(span);
                cmd.BindDescriptorSets(PipelineBindPoint.Graphics, pipeline.PipelineLayout, 0, [uniform.BlockOffset], [uniform.DescriptorSet]);
                cmd.BindVertexBuffers(0, [0], vro.VerticesBufferInfo.Buffer);
                cmd.Draw(vro.VecticesCount, 1, 0, 0);
            }

            foreach(var child in group.GetChildrens())
            {
                child.GroupRendering();
            }
            ArrayPool<byte>.Shared.Return(data);
            cmd.EndRenderPass();
            cmd.End();

            var submitInfo = new SubmitOption
            {
                WaitDstStageMask = [],
                WaitSemaphores = [],
                Buffers = [cmd.RawHandle],
                SignalSemaphores = [semaphores[0]]
            };
            lDevice.Submit(queue, new Fence(0), submitInfo);
        }
        #endregion

        #region 旋转轴圆
        private PrimaryVulkanRenderGroup CreateRotationCircleGroup(VulkanGraphicsManager manager, PrimaryVulkanRenderGroup mainGroup,
            VulkanDevice lDevice, VulkanRenderPass pass, Camera camera, Area area)
        {
            var pipeline = manager.BuildScriptedShaderAndPipeline<RotationCirclePipe>(manager.NewStringToken(), pass, lDevice.PipelineCache, area);
            var dynamicUniform = manager.CreateDynamicBufferAlloter(pipeline, 240, 10, DescriptorType.UniformBufferDynamic, 0, 0);
            CricleDynamicUniformAlloter = dynamicUniform;
            var sGroup = manager.CreatePrimaryRenderGroup(manager.NewStringToken(), pipeline).Binding(mainGroup);
            sGroup.SetCamera(camera);
            sGroup.CreateGroupRenderingAction((builder) => builder.Run((group) => RotationCircleMain(group)));
            RotationCircleGroup = sGroup;

            CreateRotationCircleRender(manager, sGroup);
            return sGroup;
        }
        protected void CreateRotationCircleRender(VulkanGraphicsManager manager, PrimaryVulkanRenderGroup sGroup)
        {
            var attr = ContextManager.CreateArchetype(new RotationCircleComponent());
            Vector3 min; Vector3 max;

            var sphere = Sphere.CreateSphere(84, 5, out uint vCount, out min, out max);
            var vroSphere = manager.CreateMesh<Vector3>(manager.NewStringToken(), [.. sphere], vCount, min, max);
            var bufferS = CricleDynamicUniformAlloter.MallocBlock();
            var renderS = manager.CreateRenderObject<RenderObject>(manager.NewStringToken(), vroSphere, bufferS,
                new RotationCircleParameter()
                {
                    Color = new Vector3(0.8f, 0.8f, 0.8f),
                    T = new Vector2(84, 0),
                    HelperType = RotationHelperType.Sphere
                });
            renderS.SetBounds(min, max);

            var circleX = Box.CreateBoxWithTexcoord3(4, 90, 90, Vector3.UnitX, 
                out vCount, out min, out max);
            var vroX = manager.CreateMesh<Vector3>(manager.NewStringToken(), [.. circleX], vCount, min, max);
            var bufferX = CricleDynamicUniformAlloter.MallocBlock();
            var renderX = manager.CreateRenderObject<RenderObject>(manager.NewStringToken(), vroX, bufferX, 
                new RotationCircleParameter() 
                {
                    Color = new Vector3(0.9372f, 0.2627f, 0.3451f),
                    T = new Vector2(88, 2),
                    HelperType = RotationHelperType.TorusX
                });
            renderX.SetBounds(min, max);

            var circleY = Box.CreateBoxWithTexcoord3(90, 4, 90, Vector3.UnitX, 
                out vCount, out min, out max);
            var vroY = manager.CreateMesh<Vector3>(manager.NewStringToken(), [.. circleY], vCount, min, max);
            var bufferY = CricleDynamicUniformAlloter.MallocBlock();
            var renderY = manager.CreateRenderObject<RenderObject>(manager.NewStringToken(), vroY, bufferY, 
                new RotationCircleParameter()
                {
                    Color = new Vector3(0.5059f, 0.8f, 0.1647f),
                    T = new Vector2(88, 2),
                    HelperType = RotationHelperType.TorusY
                });
            renderY.SetBounds(min, max);

            var circleZ = Box.CreateBoxWithTexcoord3(90, 90, 4, Vector3.UnitX, 
                out vCount, out min, out max);
            var vroZ = manager.CreateMesh<Vector3>(manager.NewStringToken(), [.. circleZ], vCount, min, max);
            var bufferZ = CricleDynamicUniformAlloter.MallocBlock();
            var renderZ = manager.CreateRenderObject<RenderObject>(manager.NewStringToken(), vroZ, bufferZ,
                new RotationCircleParameter()
                {
                    Color = new Vector3(0.2039f, 0.5294f, 0.9255f),
                    T = new Vector2(88, 2),
                    HelperType = RotationHelperType.TorusZ
                });

            ContextManager.CreateContexts(1, attr, sGroup, renderZ);
            ContextManager.CreateContexts(1, attr, sGroup, renderY);
            ContextManager.CreateContexts(1, attr, sGroup, renderX);
            ContextManager.CreateContexts(1, attr, sGroup, renderS);
        }
        private void RotationCircleMain(PrimaryVulkanRenderGroup group)
        {
            var cmd = group.PrimaryCommandBuffer;
            var pipeline = group.Pipeline;
            var camera = group.Camera;
            var view = camera.ViewMatrix;
            var viewPort = camera.Viewport;
            var project = camera.ProjectionMatrix;

            cmd.BindPipeline(PipelineBindPoint.Graphics, group.Pipeline);
            cmd.SetViewport(new Viewport(viewPort.X, viewPort.Y, viewPort.Width, viewPort.Height, viewPort.MinDepth, viewPort.MaxDepth));
            cmd.SetScissor(new Rect2D(null, new Extent2D((uint)viewPort.Width, (uint)viewPort.Height)));

            int size = 240;
            byte[] data = ArrayPool<byte>.Shared.Rent(size);
            Span<byte> span = data;
            span = span[..size];
            foreach (var context in group.GetContexts())
            {
                var render = context.GetContextRenderObject();
                var para = render.GetComponent<RotationCircleParameter>(nameof(RotationCircleParameter));
                var color = para.Color; var pointer = para.PointerPos; var startPos = para.StartPos;
                var cameraP = camera.EyePosition;
                var scale = para.Scale; var center = para.Center;
                var aspect = new Vector2(viewPort.Width, viewPort.Height);
                float isPointerEnter = para.IsPointerEnter ? 1 : 0;
                float isSelected = para.IsSelected ? 1 : 0;
                var t = new Vector3(para.T * scale, para.Theta); 
                var h = new Vector3((int)para.HelperType, isPointerEnter, isSelected);
                ref MVPMatrix mvp = ref context.GetMVPMatrix();
                Matrix4x4 model = mvp.ModelMatrix;
                Matrix4x4 final = model * view * project;
                final.ToBytes(ref span, 0);
                model.ToBytes(ref span, 64);
                MemoryMarshal.Write(span.Slice(128, 16), in color);
                MemoryMarshal.Write(span.Slice(144, 16), in cameraP);
                MemoryMarshal.Write(span.Slice(160, 16), in center);
                MemoryMarshal.Write(span.Slice(176, 8), in pointer);
                MemoryMarshal.Write(span.Slice(184, 8), in startPos);
                MemoryMarshal.Write(span.Slice(192, 8), in aspect);
                MemoryMarshal.Write(span.Slice(208, 12), in t);
                MemoryMarshal.Write(span.Slice(224, 12), in h);

                var vro = render.GetComponents<VRO>().First();
                var uniform = render.GetComponents<VulkanDynamicBufferBlock>().First();
                uniform.UpdateBlockData(span);
                cmd.BindDescriptorSets(PipelineBindPoint.Graphics, pipeline.PipelineLayout, 0, [uniform.BlockOffset], [uniform.DescriptorSet]);
                cmd.BindVertexBuffers(0, [0], vro.VerticesBufferInfo.Buffer);
                cmd.Draw(vro.VecticesCount, 1, 0, 0);
            }
            ArrayPool<byte>.Shared.Return(data);
        }
        #endregion

        private void NewHelper(NewCoordinateHelperEventArgs args)
        {
            if (args == null || args.Owner == null) return;
            var manager = VulkanManager;
            if(args.HelperType == CoordinateHelperType.TranslateHelper)
            {
                var arrowX = CoordinateArrow.CreateCoordinateArrow(new Vector3(0.9372f, 0.2627f, 0.3451f), Vector3.UnitX, out uint vCount, out Vector3 min, out Vector3 max);
                var vroX = manager.CreateMesh<Vector3>(manager.NewStringToken(), [.. arrowX], vCount, min, max);
                var bufferX = ArrowDynamicUniformAlloter.MallocBlock();
                var renderX = manager.CreateRenderObject<RenderObject>(manager.NewStringToken(), vroX, bufferX, new ArrowParameter() { Axis = Vector3.UnitX });
                renderX.GetTransform().Scaling = new Vector3(1.0f, 1.0f, 1.0f);
                renderX.SetBounds(min, max);
                renderX.AddComponent(CoordinateArrowGroup);

                var arrowY = CoordinateArrow.CreateCoordinateArrow(new Vector3(0.5059f, 0.8f, 0.1647f), Vector3.UnitY, out vCount, out min, out max);
                var vroY = manager.CreateMesh<Vector3>(manager.NewStringToken(), [.. arrowY], vCount, min, max);
                var bufferY = ArrowDynamicUniformAlloter.MallocBlock();
                var renderY = manager.CreateRenderObject<RenderObject>(manager.NewStringToken(), vroY, bufferY, new ArrowParameter() { Axis = Vector3.UnitY });
                renderY.GetTransform().Scaling = new Vector3(1.0f, 1.0f, 1.0f);
                renderY.SetBounds(min, max);
                renderY.AddComponent(CoordinateArrowGroup);

                var arrowZ = CoordinateArrow.CreateCoordinateArrow(new Vector3(0.2039f, 0.5294f, 0.9255f), Vector3.UnitZ, out vCount, out min, out max);
                var vroZ = manager.CreateMesh<Vector3>(manager.NewStringToken(), [.. arrowZ], vCount, min, max);
                var bufferZ = ArrowDynamicUniformAlloter.MallocBlock();
                var renderZ = manager.CreateRenderObject<RenderObject>(manager.NewStringToken(), vroZ, bufferZ, new ArrowParameter() { Axis = Vector3.UnitZ });
                renderZ.GetTransform().Scaling = new Vector3(1.0f, 1.0f, 1.0f);
                renderZ.SetBounds(min, max);
                renderZ.AddComponent(CoordinateArrowGroup);

                var root = manager.CreateRenderObject<TranslateRender>(manager.NewStringToken());
                root.AddChildren(renderX);
                root.AddChildren(renderY);
                root.AddChildren(renderZ);

                args.Owner.AddChildren(root);
            }
        }
    }
}
