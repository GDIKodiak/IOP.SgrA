﻿using IOP.SgrA.Editor.VulkanEngine;
using IOP.SgrA.SilkNet.Vulkan;
using Microsoft.Extensions.Logging;
using Silk.NET.Vulkan;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Semaphore = Silk.NET.Vulkan.Semaphore;

namespace IOP.SgrA.Editor.Modules
{
    [ModulePriority(ModulePriority.ISEA)]
    public class EnvironmentModule : VulkanModule, ISceneModule
    {
        public Scene Scene { get; set; }

        private const uint IBLWidth = 512;
        private readonly ILogger<EnvironmentModule> Logger;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        public EnvironmentModule(ILogger<EnvironmentModule> logger)
        {
            Logger = logger;
        }

        protected override Task Load(VulkanGraphicsManager manager)
        {
            var lDevice = manager.VulkanDevice;
            var pipeCache = lDevice.PipelineCache;
            var basePath = Path.GetDirectoryName(GetType().Assembly.Location);
            var group = Scene.RenderGroup;
            try
            {
                if (group == null) throw new NullReferenceException(nameof(Scene.RenderGroup));
                if (group is not PrimaryVulkanRenderGroup mainGroup) throw new InvalidCastException($"scene render group is not {nameof(PrimaryVulkanRenderGroup)}, " +
                    $"invalid to get render pass");
                var r = manager.TryGetCamera("MainCamera", out Camera camera);
                if (!r) throw new NullReferenceException("Cannot get camera with name MainCamera");
                var area = Scene.RenderArea;
                var vro = manager.CreateEmptyMesh<VRO>("SkyDomeMESH");
                vro.CreateUnitSphere(10, lDevice, SharingMode.Exclusive);
                var skyDome = manager.CreateRenderObject<RenderObject>("SkyDome", vro);

                var skydomeCube = manager.CreateLocalImageTextureData("IBLTEXTUREDATA", lDevice, new ImageAndImageViewCreateOption()
                {
                    ImageCreateOption = new ImageCreateOption()
                    {
                        ArrayLayers = 6,
                        Format = Format.R16G16B16A16Sfloat,
                        ImageType = ImageType.Type2D,
                        InitialLayout = ImageLayout.General,
                        MipLevels = 1,
                        Samples = SampleCountFlags.Count1Bit,
                        SharingMode = SharingMode.Exclusive,
                        Tiling = ImageTiling.Optimal,
                        Usage = ImageUsageFlags.StorageBit | ImageUsageFlags.SampledBit,
                        Flags = ImageCreateFlags.CreateCubeCompatibleBit
                    },
                    ImageViewCreateOption = new ImageViewCreateOption()
                    {
                        Components = new ComponentMapping(ComponentSwizzle.R, ComponentSwizzle.G, ComponentSwizzle.B, ComponentSwizzle.A),
                        Format = Format.R16G16B16A16Sfloat,
                        SubresourceRange = new ImageSubresourceRange(ImageAspectFlags.ColorBit, 0, 1, 0, 6),
                        ViewType = ImageViewType.TypeCube
                    }
                }, 
                new Extent3D(IBLWidth, IBLWidth, 1), FormatFeatureFlags.StorageImageBit | FormatFeatureFlags.SampledImageBit);
                var pass = CreateEnvironmentPass(manager, Scene.RenderArea);
                CreateSkyDomeMainGroup(manager, pipeCache, pass, camera, skyDome, skydomeCube, mainGroup, Scene);
            }
            catch (Exception e)
            {
                Logger?.LogError(e, "");
            }
            return Task.CompletedTask;
        }

        /// <summary>
        /// 创建天空穹渲染通道
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="area"></param>
        /// <returns></returns>
        private VulkanRenderPass CreateEnvironmentPass(VulkanGraphicsManager manager, Area area)
        {
            var lDevice = manager.VulkanDevice ?? throw new NullReferenceException("Please create Vulkan deivce first");
            var pass = lDevice.CreateScriptedRenderPass<EnvironmentPass>(area);
            return pass;
        }

        protected override Task Unload(VulkanGraphicsManager manager)
        {
            return Task.CompletedTask;
        }

        #region Skydome
        private Matrix4x4 PreViewMatrix = Matrix4x4.Identity;
        private int FrameIndex = 0;

        private PrimaryVulkanRenderGroup CreateSkyDomeMainGroup(VulkanGraphicsManager manager, PipelineCache cache, 
            VulkanRenderPass envPass, Camera camera, IRenderObject sphere, VulkanImageTextureData skydomeCube, 
            PrimaryVulkanRenderGroup sceneMain, Scene scene)
        {
            var lDevice = manager.VulkanDevice;
            var area = scene.RenderArea;
            var skyDomeTex1 = manager.CreateTexture(manager.NewStringToken(), skydomeCube, null, 0, 0, 1, 2, DescriptorType.StorageImage);
            var skyDomeTex2 = manager.CreateTexture(manager.NewStringToken(), skydomeCube, null, 0, 0, 1, 2, DescriptorType.StorageImage);

            var skyDomepipe = manager.BuildScriptedShaderAndPipeline<SkydomePipe>("SkyDome", envPass, cache, area)
                .BuildUniformBuffer(0, 0, 160, SharingMode.Exclusive)
                .BindParallelLightEnvironment(scene.LightEnvironment, 1, 0)
                .BindPipelineTexture(skyDomeTex1, 2, 0)
                .BuildUniformBuffer(3, 0, 272, SharingMode.Exclusive);
            var pCommand = lDevice.CreateCommandPool((device, option) =>
            {
                option.Flags = CommandPoolCreateFlags.ResetCommandBufferBit;
                option.QueueFamilyIndex = device.WorkQueues[0].FamilyIndex;
            }).CreatePrimaryCommandBuffer();
            var semaphore = lDevice.CreateSemaphore();
            var mainGroup = manager.CreatePrimaryRenderGroup("SkyDomeGroup", skyDomepipe, GroupRenderMode.Background)
                .Binding(pCommand, lDevice, envPass, [semaphore.RawHandle], []);
            mainGroup.CreateGroupRenderingAction((builder) => builder.Run((group) => SkyDomeMain(group)));
            mainGroup.SetCamera(camera);

            Span<Vector4> skyColos = new Vector4[] {
                new (0.530f, 0.808f, 0.921f, 0.0f), //天蓝
                new (0.530f, 0.808f, 0.980f, 0.0f), //亮天蓝
                new (0.502f, 0.0f, 0.502f, 0.0f), //紫色
                new (0f, 0f, 0.345f, 0.0f), //暗蓝
                new (0.976f, 0.569f, 0.662f, 0.0f), //薄红
                new (1.0f, 1.0f, 0.878f, 0.0f), //亮黄
                new (0.878f, 1.0f, 1.0f, 0.0f), //浅蓝
                new (0.941f, 1.0f, 1.0f, 0.0f), //蔚蓝
                new (1.0f, 0.647f, 0.0f, 0.0f), //橙色
                new (0.255f, 0.411f, 0.882f, 0.0f) //宝蓝
            };
            Span<byte> bytes = MemoryMarshal.AsBytes(skyColos);
            var skyTex = skyDomepipe.GetPipelineTexture(0, 0);
            skyTex.UpdateTextureMemoryData(bytes);
            mainGroup.BindingStaticRenderObject(sphere);

            var image = envPass.Framebuffer[0].Attachments[0];
            var image2 = envPass.Framebuffer[0].Attachments[2];
            var render = manager.CreateEngineAreaRenderingObject(Scene, mainGroup, "SkyDomeArea", image, image2);
            return mainGroup;
        }
        private void SkyDomeMain(PrimaryVulkanRenderGroup group)
        {
            int size = Marshal.SizeOf<Matrix4x4>();
            int vSize = Marshal.SizeOf<Vector4>();
            int tSize = size * 4;
            byte[] data = ArrayPool<byte>.Shared.Rent(tSize + vSize);
            try
            {
                var queue = group.WorkQueues[0].Queue;
                var cmdBuffer = group.PrimaryCommandBuffer;
                var semaphores = group.Semaphores;
                var pass = group.RenderPass;
                var lDevice = group.LogicDevice;
                var camera = group.Camera;
                var pipe = group.Pipeline;

                VulkanFrameBuffer framebuffer = group.RenderPass.GetFramebuffer(0);
                cmdBuffer.Reset();
                cmdBuffer.Begin();

                foreach (var child in group.GetChildrens())
                {
                    child.GroupRendering();
                }

                cmdBuffer.BeginRenderPass(pass, framebuffer, pass.BeginOption, SubpassContents.Inline);
                cmdBuffer.BindPipeline(PipelineBindPoint.Graphics, pipe);
                var viewPort = camera.Viewport;
                cmdBuffer.SetViewport(new Viewport(viewPort.X, viewPort.Y, viewPort.Width, viewPort.Height, viewPort.MinDepth, viewPort.MaxDepth));
                cmdBuffer.SetScissor(new Rect2D(null, new Extent2D((uint)viewPort.Width, (uint)viewPort.Height)));

                Matrix4x4 jProjection = camera.ProjectionMatrix;
                Matrix4x4 projection = camera.ProjectionMatrix;
                Matrix4x4 view = camera.BasicViewMatrix;
                jProjection.HaltonJitter((int)viewPort.Width, (int)viewPort.Height, FrameIndex);
                DescriptorSet[] sets =
                [
                    pipe.GetPipelineDescriptorSet(0),
                    pipe.GetPipelineDescriptorSet(1),
                    pipe.GetPipelineDescriptorSet(2),
                    pipe.GetPipelineDescriptorSet(3),
                ];
                Span<uint> local = [0, 0];

                foreach (var item in group.GetStaticObjects())
                {
                    var cPos = new Vector4(camera.EyePosition * 0.01f, 0.0f);
                    Vector4 pos = cPos;
                    Matrix4x4 p = projection;
                    Matrix4x4 v = view;
                    Matrix4x4 preV = PreViewMatrix;
                    Span<byte> span = data;
                    span = span[..(tSize + vSize)];
                    p.ToBytes(ref span, 0);
                    v.ToBytes(ref span, size);
                    preV.ToBytes(ref span, size * 2);
                    jProjection.ToBytes(ref span, size * 3);
                    pos.ToBytes(ref span, size * 4);
                    var tex = pipe.GetPipelineTexture(3, 0);
                    tex.UpdateTextureMemoryData(span);
                    var vro = item.GetComponents<VRO>().First();
                    cmdBuffer.BindDescriptorSets(PipelineBindPoint.Graphics, pipe.PipelineLayout, 0, null, sets);
                    cmdBuffer.BindVertexBuffers(0, [0], vro.VerticesBufferInfo.Buffer);
                    cmdBuffer.PushConstants(pipe.PipelineLayout, ShaderStageFlags.FragmentBit, 0, MemoryMarshal.AsBytes(local));
                    cmdBuffer.Draw(vro.VecticesCount, 1, 0, 0);
                }
                PreViewMatrix = view;
                cmdBuffer.EndRenderPass();
                cmdBuffer.End();
                var submitInfo = new SubmitOption
                {
                    WaitDstStageMask = null,
                    WaitSemaphores = null,
                    Buffers = [cmdBuffer.RawHandle],
                    SignalSemaphores = [semaphores[0]]
                };
                lDevice.Submit(queue, new Fence(0), submitInfo);
                FrameIndex++;
                FrameIndex %= 8;
            }
            catch (Exception e)
            {
                group.Disable();
                group.Logger?.LogError(e, e.Message);
            }
            finally
            {
                ArrayPool<byte>.Shared.Return(data);
            }
        }
        #endregion
    }
}
