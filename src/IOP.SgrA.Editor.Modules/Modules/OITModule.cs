﻿using IOP.SgrA.Editor.VulkanEngine;
using IOP.SgrA.SilkNet.Vulkan;
using Microsoft.Extensions.Logging;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.IO.Pipelines;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace IOP.SgrA.Editor.Modules
{
    [ModulePriority(ModulePriority.RenderPass)]
    public class OITModule : VulkanModule, ISceneModule
    {
        private readonly ILogger<OITModule> Logger;

        public PrimaryVulkanRenderGroup OITGroup { get; private set; }

        public Scene Scene { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        public OITModule(ILogger<OITModule> logger)
        {
            Logger = logger;
        }

        protected override Task Load(VulkanGraphicsManager manager)
        {
            var lDevice = manager.VulkanDevice;
            var pipeCache = lDevice.PipelineCache;
            var basePath = Path.GetDirectoryName(GetType().Assembly.Location);
            var group = Scene.RenderGroup ?? throw new NullReferenceException(nameof(Scene.RenderGroup));
            if (group is not PrimaryVulkanRenderGroup mainGroup) throw new InvalidCastException($"scene render group is not {nameof(PrimaryVulkanRenderGroup)}, " +
                $"invalid to get render pass");
            var r = manager.TryGetCamera("MainCamera", out Camera camera);
            if (!r) throw new NullReferenceException("Cannot get camera with name MainCamera");
            try
            {
                var oitGroup = CreateOITMainGroup(manager, lDevice, pipeCache, camera, mainGroup, Scene);
                OITGroup = oitGroup;
            }
            catch (Exception e)
            {
                Logger?.LogError(e, "");
            }
            return Task.CompletedTask;
        }

        protected override Task Unload(VulkanGraphicsManager manager)
        {
            return Task.CompletedTask;
        }

        protected VulkanPipeline WeightedFirstPipe;
        protected VulkanPipeline WeightedLastPipe;
        private PrimaryVulkanRenderGroup CreateOITMainGroup(VulkanGraphicsManager manager,
            VulkanDevice lDevice, PipelineCache cache, Camera camera,
            PrimaryVulkanRenderGroup sceneMain, Scene scene)
        {

            var pass = lDevice.CreateScriptedRenderPass<OITRenderPass>(Scene.RenderArea);
            var oitFirstPipe = manager.BuildScriptedShaderAndPipeline<WeightedOITFirstPipe>("", pass, cache, scene.RenderArea);
            var oitLastPipe = manager.BuildScriptedShaderAndPipeline<WeightedOITLastPipe>("", pass, cache, scene.RenderArea);
            WeightedFirstPipe = oitFirstPipe;
            WeightedLastPipe = oitLastPipe;

            TransitionRenderPassAttachmentLayout(pass, lDevice);

            for(uint i = 0; i < 2; i++)
            {
                var firstInput = manager.CreateTextureDataFromImage(manager.NewStringToken(), 
                    lDevice, pass.Framebuffer[0].Attachments[i], ImageLayout.ShaderReadOnlyOptimal, 
                    DescriptorType.InputAttachment, i, 0, 1);
                var firstInputTex = manager.CreateTexture(manager.NewStringToken(), firstInput, null, i, 0, 1, 0, DescriptorType.InputAttachment);
                oitFirstPipe.AddPipelineTexture(0, i, firstInputTex);
            }

            for(uint i = 0; i < 3; i++)
            {
                var lastInput = manager.CreateTextureDataFromImage(manager.NewStringToken(),
                    lDevice, pass.Framebuffer[0].Attachments[i + 2], ImageLayout.ShaderReadOnlyOptimal,
                    DescriptorType.InputAttachment, i, 0, 1);
                var lastInputTex = manager.CreateTexture(manager.NewStringToken(), lastInput, null, i, 0, 1, 0, DescriptorType.InputAttachment);
                oitLastPipe.AddPipelineTexture(0, i, lastInputTex);
            }

            var pCommand = lDevice.CreateCommandPool((device, option) =>
            {
                option.Flags = CommandPoolCreateFlags.ResetCommandBufferBit;
                option.QueueFamilyIndex = device.WorkQueues[0].FamilyIndex;
            }).CreatePrimaryCommandBuffer();
            var semaphore = lDevice.CreateSemaphore();
            var mainGroup = manager.CreatePrimaryRenderGroup("", null, GroupRenderMode.Transparent)
                .Binding(pCommand, lDevice, pass, [semaphore.RawHandle], []);
            mainGroup.CreateGroupRenderingAction((builder) => builder.Run((group) => OITMain(group)));
            mainGroup.SetCamera(camera);
            var image = pass.Framebuffer[0].Attachments[5];
            var image2 = pass.Framebuffer[0].Attachments[6];
            var render = manager.CreateEngineAreaRenderingObject(Scene, mainGroup, "OITArea", image, image2);
            return mainGroup;
        }

        private void OITMain(PrimaryVulkanRenderGroup group)
        {
            try
            {
                var queue = group.WorkQueues[0].Queue;
                var cmdBuffer = group.PrimaryCommandBuffer;
                var semaphores = group.Semaphores;
                var pass = group.RenderPass;
                var lDevice = group.LogicDevice;
                var camera = group.Camera;
                var viewPort = camera.Viewport;

                VulkanFrameBuffer framebuffer = group.RenderPass.GetFramebuffer(0);
                cmdBuffer.Reset();
                cmdBuffer.Begin();
                cmdBuffer.BeginRenderPass(pass, framebuffer, pass.BeginOption, SubpassContents.SecondaryCommandBuffers);
                group.SecondaryGroupRendering(pass, framebuffer);
                cmdBuffer.ExecuteCommands(group.GetUpdateSecondaryBuffers());

                cmdBuffer.NextSubpass(SubpassContents.Inline);
                cmdBuffer.BindPipeline(PipelineBindPoint.Graphics, WeightedFirstPipe);
                cmdBuffer.SetViewport(new Viewport(viewPort.X, viewPort.Y, viewPort.Width, viewPort.Height, viewPort.MinDepth, viewPort.MaxDepth));
                cmdBuffer.SetScissor(new Rect2D(null, new Extent2D((uint)viewPort.Width, (uint)viewPort.Height)));
                cmdBuffer.BindDescriptorSets(PipelineBindPoint.Graphics,
                    WeightedFirstPipe.PipelineLayout, 0, [], WeightedFirstPipe.GetPipelineDescriptorSet(0));
                cmdBuffer.Draw(4, 1, 0, 0);

                cmdBuffer.NextSubpass(SubpassContents.Inline);
                cmdBuffer.BindPipeline(PipelineBindPoint.Graphics, WeightedLastPipe);
                cmdBuffer.SetViewport(new Viewport(viewPort.X, viewPort.Y, viewPort.Width, viewPort.Height, viewPort.MinDepth, viewPort.MaxDepth));
                cmdBuffer.SetScissor(new Rect2D(null, new Extent2D((uint)viewPort.Width, (uint)viewPort.Height)));
                cmdBuffer.BindDescriptorSets(PipelineBindPoint.Graphics,
                    WeightedLastPipe.PipelineLayout, 0, [], WeightedLastPipe.GetPipelineDescriptorSet(0));
                cmdBuffer.Draw(4, 1, 0, 0);
                cmdBuffer.EndRenderPass();
                cmdBuffer.End();

                var submitInfo = new SubmitOption
                {
                    WaitDstStageMask = [],
                    WaitSemaphores = [],
                    Buffers = [cmdBuffer.RawHandle],
                    SignalSemaphores = [semaphores[0]]
                };
                lDevice.Submit(queue, new Fence(0), submitInfo);

            }
            catch (Exception e)
            {
                group.Disable();
                group.Logger?.LogError(e, e.Message);
            }
        }

        private void TransitionRenderPassAttachmentLayout(VulkanRenderPass pass, VulkanDevice lDevice)
        {
            var queue = lDevice.WorkQueues.Where(x => x.Type.HasFlag(QueueType.Graphics)).FirstOrDefault()
                ?? throw new NullReferenceException("Cannot found work queue with type graphics");
            var fence = lDevice.CreateFence();
            var sourceRange = new ImageSubresourceRange()
            {
                AspectMask = ImageAspectFlags.ColorBit,
                BaseArrayLayer = 0,
                BaseMipLevel = 0,
                LayerCount = 1,
                LevelCount = 1
            };
            var localCommand = lDevice.ResourcesCommandPool.CreatePrimaryCommandBuffer();
            localCommand.Begin(CommandBufferUsageFlags.OneTimeSubmitBit);
            localCommand.TransitionImageLayout(pass.Framebuffer[0].Attachments[0].VulkanImage, 
                ImageLayout.Undefined, ImageLayout.ColorAttachmentOptimal, sourceRange);
            localCommand.TransitionImageLayout(pass.Framebuffer[0].Attachments[1].VulkanImage,
                ImageLayout.Undefined, ImageLayout.ColorAttachmentOptimal, sourceRange);
            localCommand.End();
            localCommand.SubmitSingleCommand(lDevice, queue.Queue, fence.RawHandle);
            lDevice.DestroyFence(fence);
        }
    }
}
