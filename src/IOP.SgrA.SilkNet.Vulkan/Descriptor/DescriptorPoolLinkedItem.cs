﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class DescriptorPoolLinkedItem
    {
        /// <summary>
        /// 描述集下标
        /// </summary>
        public uint SetIndex { get; internal set; }
        /// <summary>
        /// 类型
        /// </summary>
        public DynamicDescriptorPoolFlags Type { get; internal set; }
        /// <summary>
        /// 描述符池
        /// </summary>
        public DescriptorPool DescriptorPool { get; internal set; }
        /// <summary>
        /// 已经使用数
        /// </summary>
        public uint Used { get; internal set; }
        /// <summary>
        /// 最大数量
        /// </summary>
        public uint MaxCount { get; internal set; }
    }
}
