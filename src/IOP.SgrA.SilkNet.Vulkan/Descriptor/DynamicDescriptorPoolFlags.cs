﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 动态描述符集池标志
    /// </summary>
    [Flags]
    public enum DynamicDescriptorPoolFlags : ulong
    {
        /// <summary>
        /// 
        /// </summary>
        None = 0x0000,
        /// <summary>
        /// 
        /// </summary>
        CombinedImageSampler = 0x0001,
        /// <summary>
        /// 
        /// </summary>
        SampledImage = 0x0002,
        /// <summary>
        /// 
        /// </summary>
        StorageImage = 0x0004,
        /// <summary>
        /// 
        /// </summary>
        UniformTexelBuffer = 0x0008,
        /// <summary>
        /// 
        /// </summary>
        StorageTexelBuffer = 0x0010,
        /// <summary>
        /// 
        /// </summary>
        UniformBuffer = 0x0020,
        /// <summary>
        /// 
        /// </summary>
        StorageBuffer = 0x0040,
        /// <summary>
        /// 
        /// </summary>
        UniformBufferDynamic = 0x0080,
        /// <summary>
        /// 
        /// </summary>
        StorageBufferDynamic = 0x0100,
        /// <summary>
        /// 
        /// </summary>
        InputAttachment = 0x0200,
        /// <summary>
        /// 
        /// </summary>
        Sampler = 0x0400,
    }
}
