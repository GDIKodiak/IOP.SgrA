﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Image = Silk.NET.Vulkan.Image;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public static class VulkanManagerTextureExtension
    {
        /// <summary>
        /// 创建本地读写纹理数据
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="name"></param>
        /// <param name="logicDevice"></param>
        /// <param name="createOption"></param>
        /// <param name="flags"></param>
        /// <param name="extent"></param>
        /// <returns></returns>
        public static VulkanImageTextureData CreateLocalImageTextureData(this VulkanGraphicsManager manager, string name, 
            VulkanDevice logicDevice, ImageAndImageViewCreateOption createOption, Extent3D extent, FormatFeatureFlags flags)
        {
            if(createOption == null) throw new ArgumentNullException(nameof(createOption));
            var texture = manager.CreateEmptyTextureData<LocalImagelVulkanTextureData>(name);
            var imageOption = createOption.ImageCreateOption ?? throw new NullReferenceException($"{nameof(ImageAndImageViewCreateOption)}.ImageCreateOption");
            BuildLocalTextureImageData(texture, createOption, extent, flags, imageOption.InitialLayout, logicDevice);
            return texture;
        }
        /// <summary>
        /// 创建本地读写纹理数据
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="name"></param>
        /// <param name="logicDevice"></param>
        /// <param name="createAction"></param>
        /// <param name="flags"></param>
        /// <param name="extent"></param>
        /// <returns></returns>
        public static VulkanImageTextureData CreateLocalImageTextureData(this VulkanGraphicsManager manager, 
            string name, VulkanDevice logicDevice, Action<ImageAndImageViewCreateOption> createAction, 
            Extent3D extent, FormatFeatureFlags flags)
        {
            if (createAction == null) throw new ArgumentNullException(nameof(createAction));
            ImageAndImageViewCreateOption option = new ImageAndImageViewCreateOption();
            createAction(option);
            return CreateLocalImageTextureData(manager, name, logicDevice, option, extent, flags);
        }
        /// <summary>
        /// 从JSON字符串创建创建本地读写纹理数据
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="name"></param>
        /// <param name="logicDevice"></param>
        /// <param name="configJson"></param>
        /// <param name="flags"></param>
        /// <param name="extent"></param>
        /// <returns></returns>
        public static VulkanImageTextureData CreateLocalImageTextureData(this VulkanGraphicsManager manager, string name, 
            VulkanDevice logicDevice, string configJson, Extent3D extent, FormatFeatureFlags flags)
        {
            ImageAndImageViewCreateOption option = JsonSerializer.Deserialize<ImageAndImageViewCreateOption>(configJson);
            return CreateLocalImageTextureData(manager, name, logicDevice, option, extent, flags);
        }
        /// <summary>
        /// 从现有图像创建纹理数据
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="name"></param>
        /// <param name="logicDevice"></param>
        /// <param name="descriptorType"></param>
        /// <param name="binding"></param>
        /// <param name="arrayElement"></param>
        /// <param name="descriptorCount"></param>
        /// <param name="imageView"></param>
        /// <param name="imageLayout"></param>
        /// <returns></returns>
        public static VulkanImageTextureData CreateTextureDataFromImage(this VulkanGraphicsManager manager, string name, 
            VulkanDevice logicDevice, VulkanImageView imageView, ImageLayout imageLayout,
            DescriptorType descriptorType, uint binding = 0, uint arrayElement = 0, uint descriptorCount = 1)
        {
            if (imageView == null) throw new ArgumentNullException(nameof(imageView));
            var texture = manager.CreateEmptyTextureData<LocalImagelVulkanTextureData>(name);
            BuildTextureImageDataFromExisting(texture, imageView, imageLayout, descriptorType, logicDevice, binding, arrayElement, descriptorCount);
            return texture;
        }

        /// <summary>
        /// 创建一致性缓冲数据
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="name"></param>
        /// <param name="size"></param>
        /// <param name="mode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <returns></returns>
        public static VulkanBufferTextureData CreateUniformBufferData(this VulkanGraphicsManager manager, string name, ulong size,
            SharingMode mode = SharingMode.Exclusive, uint[] queueFamilyIndices = null)
        {
            return CreateBufferData(manager, name, size, 
                BufferUsageFlags.UniformBufferBit, DescriptorType.UniformBuffer, mode, queueFamilyIndices);
        }
        /// <summary>
        /// 创建动态一致性缓冲数据
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="name"></param>
        /// <param name="blockSize"></param>
        /// <param name="blockCapacity"></param>
        /// <param name="mode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <returns></returns>
        public static VulkanDynamicBufferData CreateDynamicUniformBufferData(this VulkanGraphicsManager manager, string name, 
            uint blockSize, uint blockCapacity,
            SharingMode mode = SharingMode.Exclusive, uint[] queueFamilyIndices = null)
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));
            var lDevice = manager.VulkanDevice;
            ulong alignment = lDevice.PhysicalDevice.GetDeviceProperties().Limits.MinUniformBufferOffsetAlignment;
            uint s = blockSize % (uint)alignment;
            blockSize = s == 0 ? blockSize : (s + 1) * (uint)alignment;
            ulong total = blockSize * blockCapacity;
            BufferCreateOption option = new BufferCreateOption();
            option.QueueFamilyIndices = queueFamilyIndices;
            option.SharingMode = mode;
            option.Usage = BufferUsageFlags.UniformBufferBit;
            option.Size = total;
            option.DescriptorType = DescriptorType.UniformBufferDynamic;
            var textureData = manager.CreateEmptyTextureData<VulkanDynamicBufferData>(name);
            BuildBufferData(textureData, manager, option, MemoryPropertyFlags.HostCachedBit | MemoryPropertyFlags.HostVisibleBit,
                total, lDevice);
            textureData.Alignment = (uint)alignment;
            textureData.BlockCapacity = blockCapacity;
            textureData.BlockSize = blockSize;
            return textureData;
        }
        /// <summary>
        /// 创建可读写缓冲数据
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="name"></param>
        /// <param name="size"></param>
        /// <param name="mode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <returns></returns>
        public static VulkanBufferTextureData CreateStorageBufferData(this VulkanGraphicsManager manager, string name, ulong size,
            SharingMode mode = SharingMode.Exclusive, uint[] queueFamilyIndices = null)
        {
            return CreateBufferData(manager, name, size, 
                BufferUsageFlags.StorageBufferBit, DescriptorType.StorageBuffer, mode, queueFamilyIndices);
        }
        /// <summary>
        /// 创建动态可读写缓冲数据
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="name"></param>
        /// <param name="blockSize"></param>
        /// <param name="blockCapacity"></param>
        /// <param name="mode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <returns></returns>
        public static VulkanDynamicBufferData CreateDynamicStorageBufferData(this VulkanGraphicsManager manager, string name, 
            uint blockSize, uint blockCapacity,
            SharingMode mode = SharingMode.Exclusive, uint[] queueFamilyIndices = null)
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));
            var lDevice = manager.VulkanDevice;
            ulong alignment = lDevice.PhysicalDevice.GetDeviceProperties().Limits.MinStorageBufferOffsetAlignment;
            uint s = blockSize % (uint)alignment;
            blockSize = s == 0 ? blockSize : (s + 1) * (uint)alignment;
            ulong total = blockSize * blockCapacity;
            BufferCreateOption option = new BufferCreateOption();
            option.QueueFamilyIndices = queueFamilyIndices;
            option.SharingMode = mode;
            option.Usage = BufferUsageFlags.StorageBufferBit;
            option.Size = total;
            option.DescriptorType = DescriptorType.StorageBufferDynamic;
            var textureData = manager.CreateEmptyTextureData<VulkanDynamicBufferData>(name);
            BuildBufferData(textureData, manager, option, MemoryPropertyFlags.HostCachedBit | MemoryPropertyFlags.HostVisibleBit,
                total, lDevice);
            textureData.Alignment = (uint)alignment;
            textureData.BlockSize = blockSize;
            textureData.BlockCapacity = blockCapacity;
            return textureData;
        }
        /// <summary>
        /// 创建可读写结构化缓冲数据
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="name"></param>
        /// <param name="format"></param>
        /// <param name="size"></param>
        /// <param name="mode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <returns></returns>
        public static VulkanBufferTextureData CreateStorageTexelBufferData(this VulkanGraphicsManager manager, string name, Format format, ulong size,
            SharingMode mode = SharingMode.Exclusive, uint[] queueFamilyIndices = null)
        {
            return CreateTexelBufferData(manager, name, format, size,
                BufferUsageFlags.StorageTexelBufferBit, DescriptorType.StorageTexelBuffer,
                mode, queueFamilyIndices);
        }
        /// <summary>
        /// 创建自适应结构化缓冲数据
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="name"></param>
        /// <param name="format"></param>
        /// <param name="size"></param>
        /// <param name="mode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <returns></returns>
        public static VulkanBufferTextureData CreateUniformTexelBufferData(this VulkanGraphicsManager manager, string name, Format format, ulong size,
            SharingMode mode = SharingMode.Exclusive, uint[] queueFamilyIndices = null)
        {
            return CreateTexelBufferData(manager, name, format, size,
                BufferUsageFlags.UniformTexelBufferBit, DescriptorType.UniformTexelBuffer,
                mode, queueFamilyIndices);
        }
        /// <summary>
        /// 创建仅显卡可读写结构化缓冲数据
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="name"></param>
        /// <param name="format"></param>
        /// <param name="size"></param>
        /// <param name="mode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <returns></returns>
        public static VulkanBufferTextureData CreateLocalStorageTexelBufferData(this VulkanGraphicsManager manager, string name, Format format, ulong size,
            SharingMode mode = SharingMode.Exclusive, uint[] queueFamilyIndices = null)
        {
            return CreateLocalTexelBufferData(manager, name, format, size,
                BufferUsageFlags.StorageTexelBufferBit, DescriptorType.StorageTexelBuffer,
                mode, queueFamilyIndices);
        }
        /// <summary>
        /// 创建结构化缓冲数据
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="name"></param>
        /// <param name="format"></param>
        /// <param name="size"></param>
        /// <param name="usage"></param>
        /// <param name="descriptorType"></param>
        /// <param name="mode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <returns></returns>
        public static VulkanBufferTextureData CreateTexelBufferData(this VulkanGraphicsManager manager, string name, Format format, ulong size,
            BufferUsageFlags usage, DescriptorType descriptorType,
            SharingMode mode = SharingMode.Exclusive, uint[] queueFamilyIndices = null)
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));
            var lDevice = manager.VulkanDevice;
            BufferCreateOption option = new BufferCreateOption();
            option.QueueFamilyIndices = queueFamilyIndices;
            option.SharingMode = mode;
            option.Usage = usage;
            option.Size = size;
            option.Format = format;
            option.DescriptorType = descriptorType;
            var textureData = manager.CreateEmptyTextureData<VulkanBufferTextureData>(name);
            BuildTexelBufferData(textureData, manager, option, format,
                MemoryPropertyFlags.HostCachedBit | MemoryPropertyFlags.HostVisibleBit, size, lDevice);
            return textureData;
        }
        /// <summary>
        /// 创建仅显卡使用的结构化缓冲数据
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="name"></param>
        /// <param name="format"></param>
        /// <param name="size"></param>
        /// <param name="usage"></param>
        /// <param name="descriptorType"></param>
        /// <param name="mode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static VulkanBufferTextureData CreateLocalTexelBufferData(this VulkanGraphicsManager manager, string name, Format format, ulong size,
            BufferUsageFlags usage, DescriptorType descriptorType,
            SharingMode mode = SharingMode.Exclusive, uint[] queueFamilyIndices = null)
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));
            var lDevice = manager.VulkanDevice;
            BufferCreateOption option = new BufferCreateOption();
            option.QueueFamilyIndices = queueFamilyIndices;
            option.SharingMode = mode;
            option.Usage = usage;
            option.Size = size;
            option.Format = format;
            option.DescriptorType = descriptorType;
            var textureData = manager.CreateEmptyTextureData<LocalBufferVulkanTextureData>(name);
            BuildTexelBufferData(textureData, manager, option, format,
                MemoryPropertyFlags.DeviceLocalBit, size, lDevice);
            return textureData;
        }
        /// <summary>
        /// 创建缓冲数据
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="name"></param>
        /// <param name="size"></param>
        /// <param name="descriptorType"></param>
        /// <param name="usage"></param>
        /// <param name="mode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <returns></returns>
        public static VulkanBufferTextureData CreateBufferData(this VulkanGraphicsManager manager, string name, ulong size, 
            BufferUsageFlags usage, DescriptorType descriptorType,
            SharingMode mode = SharingMode.Exclusive, uint[] queueFamilyIndices = null)
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));
            var lDevice = manager.VulkanDevice;
            BufferCreateOption option = new BufferCreateOption();
            option.QueueFamilyIndices = queueFamilyIndices;
            option.SharingMode = mode;
            option.Usage = usage;
            option.Size = size;
            option.DescriptorType = descriptorType;
            var textureData = manager.CreateEmptyTextureData<VulkanBufferTextureData>(name);
            BuildBufferData(textureData, manager, option, MemoryPropertyFlags.HostCachedBit | MemoryPropertyFlags.HostVisibleBit,
                size, lDevice);
            return textureData;
        }
        /// <summary>
        /// 创建仅显卡使用的缓冲数据
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="name"></param>
        /// <param name="size"></param>
        /// <param name="usage"></param>
        /// <param name="descriptorType"></param>
        /// <param name="mode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static VulkanBufferTextureData CreateLocalBufferData(this VulkanGraphicsManager manager, string name, ulong size,
            BufferUsageFlags usage, DescriptorType descriptorType,
            SharingMode mode = SharingMode.Exclusive, uint[] queueFamilyIndices = null)
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));
            var lDevice = manager.VulkanDevice;
            BufferCreateOption option = new BufferCreateOption();
            option.QueueFamilyIndices = queueFamilyIndices;
            option.SharingMode = mode;
            option.Usage = usage;
            option.Size = size;
            option.DescriptorType = descriptorType;
            var textureData = manager.CreateEmptyTextureData<LocalBufferVulkanTextureData>(name);
            BuildBufferData(textureData, manager, option, MemoryPropertyFlags.DeviceLocalBit, size, lDevice);
            return textureData;
        }

        /// <summary>
        /// 创建仅GPU读写的可存储纹理
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="name"></param>
        /// <param name="format"></param>
        /// <param name="extent"></param>
        /// <param name="binding"></param>
        /// <param name="arrayElement"></param>
        /// <param name="descriptorCount"></param>
        /// <param name="setIndex"></param>
        /// <returns></returns>
        /// <exception cref="NullReferenceException"></exception>
        public static VulkanTexture CreateLocalStorageTexture(this VulkanGraphicsManager manager, string name,
            Format format, Extent3D extent, uint binding = 0, uint arrayElement = 0, uint descriptorCount = 1, uint setIndex = 0)
        {
            var lDevice = manager.VulkanDevice ?? throw new NullReferenceException("Please create logic device first");
            if (string.IsNullOrEmpty(name)) name = manager.NewStringToken();
            var texData = manager.CreateLocalImageTextureData(name, lDevice, new ImageAndImageViewCreateOption()
            {
                ImageCreateOption = new ImageCreateOption
                {
                    ArrayLayers = 1,
                    Extent = extent,
                    Format = format,
                    ImageType = ImageType.Type2D,
                    InitialLayout = ImageLayout.General,
                    MipLevels = 1,
                    Samples = SampleCountFlags.Count1Bit,
                    SharingMode = SharingMode.Exclusive,
                    Tiling = ImageTiling.Optimal,
                    Usage = ImageUsageFlags.StorageBit
                },
                ImageViewCreateOption = new ImageViewCreateOption()
                {
                    Components = new ComponentMapping(ComponentSwizzle.R, ComponentSwizzle.G, ComponentSwizzle.B, ComponentSwizzle.A),
                    Format = format,
                    SubresourceRange = new ImageSubresourceRange(ImageAspectFlags.ColorBit, 0, 1, 0, 1),
                    ViewType = ImageViewType.Type2D
                }
            }, extent, FormatFeatureFlags.StorageImageBit);
            var tex = manager.CreateTexture($"{name}-Texture", texData, null, binding, arrayElement, descriptorCount, setIndex, DescriptorType.StorageImage);
            return tex;
        }
        /// <summary>
        /// 创建一致性缓冲纹理
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="name"></param>
        /// <param name="size"></param>
        /// <param name="binding"></param>
        /// <param name="arrayElement"></param>
        /// <param name="descriptorCount"></param>
        /// <param name="setIndex"></param>
        /// <param name="mode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <returns></returns>
        public static VulkanTexture CreateUniformBufferTexture(this VulkanGraphicsManager manager, string name,
            ulong size, uint binding = 0, uint arrayElement = 0, uint descriptorCount = 1, uint setIndex = 0,
            SharingMode mode = SharingMode.Exclusive, uint[] queueFamilyIndices = null)
        {
            var textureData = CreateUniformBufferData(manager, name, size, mode, queueFamilyIndices);
            return manager.CreateTexture(textureData.Name, textureData, null, binding, 
                arrayElement, descriptorCount, setIndex, DescriptorType.UniformBuffer);
        }
        /// <summary>
        /// 创建动态一致性缓冲纹理
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="name"></param>
        /// <param name="blockSzie"></param>
        /// <param name="blockCapacity"></param>
        /// <param name="binding"></param>
        /// <param name="arrayElement"></param>
        /// <param name="descriptorCount"></param>
        /// <param name="setIndex"></param>
        /// <param name="mode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <returns></returns>
        public static VulkanDynamicBuffer CreateDynamicUniformBufferTexture(this VulkanGraphicsManager manager, string name,
            uint blockSzie, uint blockCapacity, uint binding = 0, uint arrayElement = 0, uint descriptorCount = 1, uint setIndex = 0,
            SharingMode mode = SharingMode.Exclusive, uint[] queueFamilyIndices = null)
        {
            var textureData = CreateDynamicUniformBufferData(manager, name, blockSzie, blockCapacity, mode, queueFamilyIndices);
            return manager.CreateDynamicBuffer(textureData.Name, textureData, binding, 
                arrayElement, descriptorCount, setIndex, DescriptorType.UniformBufferDynamic);
        }
        /// <summary>
        /// 创建可读可写缓冲纹理
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="name"></param>
        /// <param name="size"></param>
        /// <param name="binding"></param>
        /// <param name="arrayElement"></param>
        /// <param name="descriptorCount"></param>
        /// <param name="setIndex"></param>
        /// <param name="mode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <returns></returns>
        public static VulkanTexture CreateStorageBufferTexture(this VulkanGraphicsManager manager, string name, 
            ulong size, uint binding = 0, uint arrayElement = 0, uint descriptorCount = 1, uint setIndex = 0,
            SharingMode mode = SharingMode.Exclusive, uint[] queueFamilyIndices = null)
        {
            var textureData = CreateStorageBufferData(manager, name, size, mode, queueFamilyIndices);
            return manager.CreateTexture(textureData.Name, textureData, null, binding, arrayElement, 
                descriptorCount, setIndex, DescriptorType.StorageBuffer);
        }
        /// <summary>
        /// 创建动态可读写缓冲纹理
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="name"></param>
        /// <param name="blockSize"></param>
        /// <param name="blockCapacity"></param>
        /// <param name="binding"></param>
        /// <param name="arrayElement"></param>
        /// <param name="descriptorCount"></param>
        /// <param name="setIndex"></param>
        /// <param name="mode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <returns></returns>
        public static VulkanDynamicBuffer CreateDynamicStorageBufferTexture(this VulkanGraphicsManager manager, string name,
            uint blockSize, uint blockCapacity, uint binding = 0, uint arrayElement = 0, uint descriptorCount = 1, uint setIndex = 0,
            SharingMode mode = SharingMode.Exclusive, uint[] queueFamilyIndices = null)
        {
            var textureData = CreateDynamicStorageBufferData(manager, name, blockSize, blockCapacity, mode, queueFamilyIndices);
            return manager.CreateDynamicBuffer(textureData.Name, textureData, binding, arrayElement,
                descriptorCount, setIndex, DescriptorType.StorageBufferDynamic);
        }
        /// <summary>
        /// 创建仅显卡可存储的缓冲纹理
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="name"></param>
        /// <param name="format"></param>
        /// <param name="size"></param>
        /// <param name="binding"></param>
        /// <param name="arrayElement"></param>
        /// <param name="descriptorCount"></param>
        /// <param name="setIndex"></param>
        /// <param name="mode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <returns></returns>
        public static VulkanTexture CreateLocalStorageTexelBufferTexture(this VulkanGraphicsManager manager, string name, 
            Format format, ulong size, uint binding = 0, uint arrayElement = 0, uint descriptorCount = 1, uint setIndex = 0,
            SharingMode mode = SharingMode.Exclusive, uint[] queueFamilyIndices = null)
        {
            var textureData = CreateLocalStorageTexelBufferData(manager, name, format, size, mode, queueFamilyIndices);
            return manager.CreateTexture(textureData.Name, textureData, null, binding, arrayElement, 
                descriptorCount, setIndex, DescriptorType.StorageTexelBuffer);
        }
        /// <summary>
        /// 创建可存储的结构化缓冲纹理
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="name"></param>
        /// <param name="format"></param>
        /// <param name="size"></param>
        /// <param name="binding"></param>
        /// <param name="arrayElement"></param>
        /// <param name="descriptorCount"></param>
        /// <param name="setIndex"></param>
        /// <param name="mode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <returns></returns>
        public static VulkanTexture CreateStorageTexelBufferTexture(this VulkanGraphicsManager manager, string name,
            Format format, ulong size, uint binding = 0, uint arrayElement = 0, uint descriptorCount = 1, uint setIndex = 0,
            SharingMode mode = SharingMode.Exclusive, uint[] queueFamilyIndices = null)
        {
            var textureData = CreateStorageTexelBufferData(manager, name, format, size, mode, queueFamilyIndices);
            return manager.CreateTexture(textureData.Name, textureData, null, binding, arrayElement, 
                descriptorCount, setIndex, DescriptorType.StorageTexelBuffer);
        }
        /// <summary>
        /// 创建结构化的自适应缓冲纹理
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="name"></param>
        /// <param name="format"></param>
        /// <param name="size"></param>
        /// <param name="mode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <param name="binding"></param>
        /// <param name="arrayElement"></param>
        /// <param name="descriptorCount"></param>
        /// <param name="setIndex"></param>
        /// <returns></returns>
        public static VulkanTexture CreateUniformTexelBufferTexture(this VulkanGraphicsManager manager, string name,
            Format format, ulong size, uint binding = 0, uint arrayElement = 0, uint descriptorCount = 1, uint setIndex = 0,
            SharingMode mode = SharingMode.Exclusive, uint[] queueFamilyIndices = null)
        {
            var textureData = CreateUniformTexelBufferData(manager, name, format, size, mode, queueFamilyIndices);
            return manager.CreateTexture(textureData.Name, textureData, null, binding, arrayElement, 
                descriptorCount, setIndex, DescriptorType.UniformTexelBuffer);
        }

        /// <summary>
        /// 创建动态缓冲分配器
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="pipeline"></param>
        /// <param name="blockSize">单个块大小</param>
        /// <param name="blockCapacity">块容量</param>
        /// <param name="descriptorType"></param>
        /// <param name="binding"></param>
        /// <param name="arrayElement"></param>
        /// <param name="descriptorCount"></param>
        /// <param name="setIndex"></param>
        /// <param name="mode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        public static VulkanDynamicBufferAlloter CreateDynamicBufferAlloter(this VulkanGraphicsManager manager, VulkanPipeline pipeline,
            uint blockSize, uint blockCapacity, DescriptorType descriptorType, uint binding = 0, uint arrayElement = 0, uint descriptorCount = 1, uint setIndex = 0,
            SharingMode mode = SharingMode.Exclusive, uint[] queueFamilyIndices = null)
        {
            if (!(descriptorType == DescriptorType.UniformBufferDynamic || descriptorType == DescriptorType.StorageBufferDynamic))
                throw new InvalidOperationException("Create dynamic buffer must be use UniformBufferDynamic or StorageBufferDynamic flag");
            VulkanDynamicBufferAlloter alloter = new VulkanDynamicBufferAlloter(manager, pipeline, blockSize, blockCapacity, binding, descriptorCount,
                arrayElement, setIndex, descriptorType);
            return alloter;
        }

        /// <summary>
        /// 加载纹理数据
        /// </summary>
        /// <typeparam name="CTextureData"></typeparam>
        /// <param name="texture"></param>
        /// <param name="logicDevice"></param>
        /// <param name="path"></param>
        /// <param name="createOption"></param>
        /// <returns></returns>
        public static async Task<CTextureData> LoadTextureData<CTextureData>(this CTextureData texture,
            VulkanDevice logicDevice, string path, ImageAndImageViewCreateOption createOption)
            where CTextureData : VulkanImageTextureData
        {
            if (createOption == null) throw new ArgumentNullException(nameof(createOption));
            if (File.Exists(path))
            {
                await texture.Load(new FileInfo(path));
                var imageC = createOption.ImageCreateOption;
                imageC.Extent = new Extent3D(texture.Width, texture.Height, texture.Depth);
                if (imageC.Usage == 0) imageC.Usage = ImageUsageFlags.SampledBit | ImageUsageFlags.TransferSrcBit | ImageUsageFlags.TransferDstBit;
                BuildHostTextureImageData(texture, createOption, imageC.Extent, 
                    FormatFeatureFlags.SampledImageBit | FormatFeatureFlags.TransferSrcBit | FormatFeatureFlags.TransferDstBit,
                    ImageLayout.ShaderReadOnlyOptimal, logicDevice);
            }
            else throw new FileNotFoundException(path);
            return texture;
        }
        /// <summary>
        /// 加载纹理数据
        /// </summary>
        /// <typeparam name="CTextureData"></typeparam>
        /// <param name="texture"></param>
        /// <param name="logicDevice"></param>
        /// <param name="data"></param>
        /// <param name="createOption"></param>
        /// <returns></returns>
        public static async Task<CTextureData> LoadTextureData<CTextureData>(this CTextureData texture,
            VulkanDevice logicDevice, byte[] data, ImageAndImageViewCreateOption createOption)
            where CTextureData : VulkanImageTextureData
        {
            if (createOption == null) throw new ArgumentNullException(nameof(createOption));
            await texture.Load(data);
            var imageC = createOption.ImageCreateOption;
            imageC.Extent = new Extent3D(texture.Width, texture.Height, texture.Depth);
            if (imageC.Usage == 0) imageC.Usage = ImageUsageFlags.SampledBit | ImageUsageFlags.TransferSrcBit | ImageUsageFlags.TransferDstBit;
            BuildHostTextureImageData(texture, createOption, imageC.Extent, FormatFeatureFlags.SampledImageBit | FormatFeatureFlags.TransferSrcBit | FormatFeatureFlags.TransferDstBit,
                ImageLayout.ShaderReadOnlyOptimal, logicDevice);
            return texture;
        }
        /// <summary>
        /// 加载纹理数据
        /// </summary>
        /// <typeparam name="CTextureData"></typeparam>
        /// <param name="texture"></param>
        /// <param name="logicDevice"></param>
        /// <param name="data"></param>
        /// <param name="configJson"></param>
        /// <returns></returns>
        public static async Task<CTextureData> LoadTextureData<CTextureData>(this CTextureData texture,
            VulkanDevice logicDevice, byte[] data, string configJson)
            where CTextureData : VulkanImageTextureData
        {
            var imageOption = JsonSerializer.Deserialize<ImageAndImageViewCreateOption>(configJson);
            return await LoadTextureData(texture, logicDevice, data, imageOption);
        }
        /// <summary>
        /// 加载纹理数据
        /// </summary>
        /// <typeparam name="CTextureData"></typeparam>
        /// <param name="texture"></param>
        /// <param name="logicDevice"></param>
        /// <param name="path"></param>
        /// <param name="configJson"></param>
        /// <returns></returns>
        public static async Task<CTextureData> LoadTextureData<CTextureData>(this CTextureData texture,
            VulkanDevice logicDevice, string path, string configJson)
            where CTextureData : VulkanImageTextureData
        {
            var imageOption = JsonSerializer.Deserialize<ImageAndImageViewCreateOption>(configJson);
            return await LoadTextureData(texture, logicDevice, path, imageOption);
        }
        /// <summary>
        /// 加载纹理数据
        /// </summary>
        /// <typeparam name="CTextureData"></typeparam>
        /// <param name="texture"></param>
        /// <param name="logicDevice"></param>
        /// <param name="data"></param>
        /// <param name="createAction"></param>
        /// <returns></returns>
        public static async Task<CTextureData> LoadTextureData<CTextureData>(this CTextureData texture,
            VulkanDevice logicDevice, byte[] data,
            Action<ImageAndImageViewCreateOption> createAction)
            where CTextureData : VulkanImageTextureData
        {
            if (createAction == null) throw new ArgumentNullException(nameof(createAction));
            ImageAndImageViewCreateOption createOption = new ImageAndImageViewCreateOption();
            createAction(createOption);
            return await LoadTextureData(texture, logicDevice, data, createOption);
        }
        /// <summary>
        /// 加载纹理数据
        /// </summary>
        /// <typeparam name="CTextureData"></typeparam>
        /// <param name="logicDevice"></param>
        /// <param name="createAction"></param>
        /// <param name="path"></param>
        /// <param name="texture"></param>
        /// <returns></returns>
        public static async Task<CTextureData> LoadTextureData<CTextureData>(this CTextureData texture,
            VulkanDevice logicDevice, string path,
            Action<ImageAndImageViewCreateOption> createAction)
            where CTextureData : VulkanImageTextureData
        {
            if (createAction == null) throw new ArgumentNullException(nameof(createAction));
            ImageAndImageViewCreateOption createOption = new ImageAndImageViewCreateOption();
            createAction(createOption);
            return await LoadTextureData(texture, logicDevice, path, createOption);
        }
        /// <summary>
        /// 加载可读可写的纹理数据
        /// </summary>
        /// <typeparam name="CTextureData"></typeparam>
        /// <param name="texture"></param>
        /// <param name="logicDevice"></param>
        /// <param name="path"></param>
        /// <param name="createOption"></param>
        /// <returns></returns>
        public static async Task<CTextureData> LoadStorageTextureData<CTextureData>(this CTextureData texture,
            VulkanDevice logicDevice, string path, ImageAndImageViewCreateOption createOption)
            where CTextureData : VulkanImageTextureData
        {
            if (createOption == null) throw new ArgumentNullException(nameof(createOption));
            if (File.Exists(path))
            {
                await texture.Load(new FileInfo(path));
                createOption.DescriptorType = DescriptorType.StorageImage;
                var imageC = createOption.ImageCreateOption;
                var viewC = createOption.ImageViewCreateOption;
                imageC.MipLevels = 1;
                imageC.Extent = new Extent3D(texture.Width, texture.Height, texture.Depth);
                ImageSubresourceRange subresource = viewC.SubresourceRange;
                subresource.LevelCount = 1;
                viewC.SubresourceRange = subresource;
                if (imageC.Usage == 0) imageC.Usage = ImageUsageFlags.StorageBit | ImageUsageFlags.TransferSrcBit | ImageUsageFlags.TransferDstBit;
                BuildHostTextureImageData(texture, createOption, imageC.Extent, FormatFeatureFlags.StorageImageBit | FormatFeatureFlags.TransferSrcBit | FormatFeatureFlags.TransferDstBit,
                    ImageLayout.General, logicDevice);
            }
            else throw new FileNotFoundException(path);
            return texture;
        }
        /// <summary>
        /// 加载可读可写的纹理数据
        /// </summary>
        /// <typeparam name="CTextureData"></typeparam>
        /// <param name="texture"></param>
        /// <param name="logicDevice"></param>
        /// <param name="data"></param>
        /// <param name="createOption"></param>
        /// <returns></returns>
        public static async Task<CTextureData> LoadStorageTextureData<CTextureData>(this CTextureData texture,
            VulkanDevice logicDevice, byte[] data, ImageAndImageViewCreateOption createOption)
            where CTextureData : VulkanImageTextureData
        {
            if (createOption == null) throw new ArgumentNullException(nameof(createOption));
            await texture.Load(data);
            createOption.DescriptorType = DescriptorType.StorageImage;
            var imageC = createOption.ImageCreateOption;
            var viewC = createOption.ImageViewCreateOption;
            imageC.MipLevels = 1;
            imageC.Extent = new Extent3D(texture.Width, texture.Height, texture.Depth);
            ImageSubresourceRange subresource = viewC.SubresourceRange;
            subresource.LevelCount = 1;
            viewC.SubresourceRange = subresource;
            if (imageC.Usage == 0) imageC.Usage = ImageUsageFlags.StorageBit | ImageUsageFlags.TransferSrcBit | ImageUsageFlags.TransferDstBit;
            BuildHostTextureImageData(texture, createOption, imageC.Extent, FormatFeatureFlags.StorageImageBit | FormatFeatureFlags.TransferSrcBit | FormatFeatureFlags.TransferDstBit,
                ImageLayout.General, logicDevice);
            return texture;
        }
        /// <summary>
        /// 加载可读可写的纹理数据
        /// </summary>
        /// <typeparam name="CTextureData"></typeparam>
        /// <param name="texture"></param>
        /// <param name="logicDevice"></param>
        /// <param name="data"></param>
        /// <param name="configJson"></param>
        /// <returns></returns>
        public static async Task<CTextureData> LoadStorageTextureData<CTextureData>(this CTextureData texture,
            VulkanDevice logicDevice, byte[] data, string configJson)
            where CTextureData : VulkanImageTextureData
        {
            var imageOption = JsonSerializer.Deserialize<ImageAndImageViewCreateOption>(configJson);
            return await LoadStorageTextureData(texture, logicDevice, data, imageOption);
        }
        /// <summary>
        /// 加载可读可写的纹理数据
        /// </summary>
        /// <typeparam name="CTextureData"></typeparam>
        /// <param name="texture"></param>
        /// <param name="logicDevice"></param>
        /// <param name="path"></param>
        /// <param name="configJson"></param>
        /// <returns></returns>
        public static async Task<CTextureData> LoadStorageTextureData<CTextureData>(this CTextureData texture,
            VulkanDevice logicDevice, string path, string configJson)
            where CTextureData : VulkanImageTextureData
        {
            var imageOption = JsonSerializer.Deserialize<ImageAndImageViewCreateOption>(configJson);
            return await LoadStorageTextureData(texture, logicDevice, path, imageOption);
        }
        /// <summary>
        /// 加载可读可写的纹理数据
        /// </summary>
        /// <typeparam name="CTextureData"></typeparam>
        /// <param name="texture"></param>
        /// <param name="logicDevice"></param>
        /// <param name="data"></param>
        /// <param name="createAction"></param>
        /// <returns></returns>
        public static async Task<CTextureData> LoadStorageTextureData<CTextureData>(this CTextureData texture,
            VulkanDevice logicDevice, byte[] data, Action<ImageAndImageViewCreateOption> createAction)
            where CTextureData : VulkanImageTextureData
        {
            if (createAction == null) throw new ArgumentNullException(nameof(createAction));
            ImageAndImageViewCreateOption createOption = new ImageAndImageViewCreateOption();
            createAction(createOption);
            return await LoadStorageTextureData(texture, logicDevice, data, createOption);
        }
        /// <summary>
        /// 加载可读可写的纹理数据
        /// </summary>
        /// <typeparam name="CTextureData"></typeparam>
        /// <param name="texture"></param>
        /// <param name="logicDevice"></param>
        /// <param name="path"></param>
        /// <param name="createAction"></param>
        /// <returns></returns>
        public static async Task<CTextureData> LoadStorageTextureData<CTextureData>(this CTextureData texture,
            VulkanDevice logicDevice, string path, Action<ImageAndImageViewCreateOption> createAction)
            where CTextureData : VulkanImageTextureData
        {
            if (createAction == null) throw new ArgumentNullException(nameof(createAction));
            ImageAndImageViewCreateOption createOption = new ImageAndImageViewCreateOption();
            createAction(createOption);
            return await LoadStorageTextureData(texture, logicDevice, path, createOption);
        }

        /// <summary>
        /// 从JSON文件创建采样器
        /// </summary>
        /// <param name="name"></param>
        /// <param name="device"></param>
        /// <param name="json"></param>
        /// <returns></returns>
        public static VulkanSampler CreateSamplerFromJson(this VulkanDevice device, string name, string json)
        {
            var samplerOption = JsonSerializer.Deserialize<SamplerCreateOption>(json);
            return device.CreateSampler(name, samplerOption);
        }
        /// <summary>
        /// 创建采样器
        /// </summary>
        /// <param name="name"></param>
        /// <param name="action"></param>
        /// <param name="device"></param>
        /// <returns></returns>
        public static VulkanSampler CreateSampler(this VulkanDevice device, string name, 
            Action<SamplerCreateOption> action)
        {
            if (action == null) throw new ArgumentNullException(nameof(action));
            SamplerCreateOption createOption = new SamplerCreateOption();
            action(createOption);
            return device.CreateSampler(name, createOption);
        }

        /// <summary>
        /// 使用资源管线变更图片布局
        /// </summary>
        /// <param name="imageView"></param>
        /// <param name="logicDevice"></param>
        /// <param name="srcLayout"></param>
        /// <param name="dstLayout"></param>
        /// <returns></returns>
        public static VulkanImageView ConvertImageLayout(this VulkanImageView imageView,
            VulkanDevice logicDevice, ImageLayout srcLayout, ImageLayout dstLayout)
        {
            if (imageView == null || imageView.VulkanImage == null) throw new ArgumentNullException(nameof(imageView));
            var image = imageView.VulkanImage;
            var range = imageView.CreateOption.SubresourceRange;
            var queue = logicDevice.WorkQueues.Where(x => x.Type.HasFlag(QueueType.Graphics)).FirstOrDefault() ??
                throw new NullReferenceException("Cannot found work queue with type graphics");
            var cmd = logicDevice.ResourcesCommandPool.CreatePrimaryCommandBuffer();
            VulkanFence fence = logicDevice.CreateFence();
            cmd.Begin();
            cmd.TransitionImageLayout(imageView.VulkanImage, srcLayout, dstLayout, range);
            cmd.End();
            cmd.SubmitSingleCommand(logicDevice, queue.Queue, fence.RawHandle);
            logicDevice.DestroyFence(fence);
            return imageView;
        }

        /// <summary>
        /// 构建本地的图片纹理数据
        /// </summary>
        /// <param name="texture"></param>
        /// <param name="option"></param>
        /// <param name="extent"></param>
        /// <param name="flags"></param>
        /// <param name="imageLayout"></param>
        /// <param name="logicDevice"></param>
        private static void BuildLocalTextureImageData(VulkanImageTextureData texture, ImageAndImageViewCreateOption option,
            Extent3D extent, FormatFeatureFlags flags, ImageLayout imageLayout, VulkanDevice logicDevice)
        {
            if (option == null) throw new ArgumentNullException(nameof(option));
            texture.Height = extent.Height;
            texture.Width = extent.Width;
            var imageC = option.ImageCreateOption;
            var viewC = option.ImageViewCreateOption;
            VulkanImageView imageView = BuildImage(texture, option, logicDevice, extent, flags, MemoryPropertyFlags.DeviceLocalBit);
            var descriptor = new DescriptorImageInfo
            {
                ImageLayout = imageLayout,
                ImageView = imageView.RawHandle
            };
            var wSet = new WriteDescriptorSet
            {
                DescriptorType = option.DescriptorType,
                ImageInfo = new DescriptorImageInfo[] { descriptor },
            };
            texture.WriteDescriptorSet = wSet;
            var memory = imageView.VulkanImage.DeviceMemory;
            texture.ImageInfo = new VulkanImageInfo
            {
                DeviceMemory = memory,
                Image = imageView.VulkanImage,
                ImageView = imageView,
                ImageFormat = imageC.Format,
                ImageType = imageC.ImageType,
                MemoryType = memory.PropertyFlags,
                Size = texture.BytesSize,
                Usage = imageC.Usage,
                WriteDescriptorSet = wSet
            };
            texture.IsLoaded = true;
            var queue = logicDevice.WorkQueues.Where(x=> x.Type.HasFlag(QueueType.Graphics)).FirstOrDefault();
            if (queue == null) throw new NullReferenceException("Cannot found work queue with type graphics");
            var cmd = logicDevice.ResourcesCommandPool.CreatePrimaryCommandBuffer();
            VulkanFence fence = logicDevice.CreateFence();
            cmd.Begin();
            cmd.TransitionImageLayout(imageView.VulkanImage, ImageLayout.Undefined, imageLayout, viewC.SubresourceRange);
            cmd.End();
            cmd.SubmitSingleCommand(logicDevice, queue.Queue, fence.RawHandle);
            logicDevice.DestroyFence(fence);
        }
        /// <summary>
        /// 构建主机可访问纹理图像数据
        /// </summary>
        /// <param name="option"></param>
        /// <param name="texture"></param>
        /// <param name="extent"></param>
        /// <param name="flags"></param>
        /// <param name="imageLayout"></param>
        /// <param name="logicDevice"></param>
        private static void BuildHostTextureImageData(VulkanImageTextureData texture, ImageAndImageViewCreateOption option,
            Extent3D extent, FormatFeatureFlags flags, ImageLayout imageLayout, VulkanDevice logicDevice)
        {
            var imageC = option.ImageCreateOption;
            var viewC = option.ImageViewCreateOption;
            VulkanImageView imageView = BuildImage(texture, option, logicDevice, extent, flags);
            ImageView view = imageView.RawHandle;
            VulkanImage image = imageView.VulkanImage;
            int tSize = (int)texture.BytesSize;
            int size = (int)image.DeviceMemory.Requirements.Size;
            tSize = tSize > size ? size : tSize;
            BufferCreateOption bOption = new BufferCreateOption();
            bOption.QueueFamilyIndices = imageC.QueueFamilyIndices;
            bOption.SharingMode = imageC.SharingMode;
            bOption.Usage = BufferUsageFlags.TransferSrcBit;
            bOption.Size = (ulong)size;
            var buffer = logicDevice.CreateBuffer(bOption, (ulong)size,
                MemoryPropertyFlags.HostVisibleBit | MemoryPropertyFlags.HostCoherentBit);
            IntPtr p = texture.Buffer;
            buffer.UpdateMemory(0, in p, (ulong)tSize);
            var descriptor = new DescriptorImageInfo
            {
                ImageLayout = imageLayout,
                ImageView = imageView.RawHandle
            };
            var wSet = new WriteDescriptorSet
            {
                DescriptorType = option.DescriptorType,
                ImageInfo = new DescriptorImageInfo[] { descriptor },
            };
            texture.WriteDescriptorSet = wSet;
            texture.ImageInfo = new VulkanImageInfo
            {
                DeviceMemory = image.DeviceMemory,
                Image = image,
                ImageView = imageView,
                ImageFormat = imageC.Format,
                ImageType = imageC.ImageType,
                MemoryType = image.DeviceMemory.PropertyFlags,
                Size = (ulong)size,
                Usage = imageC.Usage,
                WriteDescriptorSet = wSet
            };
            texture.IsLoaded = true;
            BufferImageCopy bufferImageCopy = new BufferImageCopy
            {
                ImageSubresource = new ImageSubresourceLayers(viewC.SubresourceRange.AspectMask,
                viewC.SubresourceRange.BaseMipLevel,
                viewC.SubresourceRange.BaseArrayLayer,
                viewC.SubresourceRange.LayerCount),
                ImageExtent = new Extent3D(texture.Width, texture.Height, texture.Depth),
                BufferOffset = 0
            };
            logicDevice.TransitionTilingOptimalImage(image, buffer, bufferImageCopy,
                viewC.SubresourceRange, texture.Width, texture.Height,
                imageC.MipLevels, option.Filter, imageLayout);
            logicDevice.DestroyBuffer(buffer);
            texture.Clear();
        }
        /// <summary>
        /// 从现有图像创建纹理图像数据
        /// </summary>
        /// <param name="texture"></param>
        /// <param name="imageView"></param>
        /// <param name="imageLayout"></param>
        /// <param name="descriptorType"></param>
        /// <param name="logicDevice"></param>
        /// <param name="binding"></param>
        /// <param name="arrayElement"></param>
        /// <param name="descriptorCount"></param>
        /// <param name="filter"></param>
        private static void BuildTextureImageDataFromExisting(VulkanImageTextureData texture, VulkanImageView imageView, ImageLayout imageLayout,
            DescriptorType descriptorType, VulkanDevice logicDevice,
            uint binding = 0, uint arrayElement = 0, uint descriptorCount = 1, Filter filter = Filter.Linear)
        {
            var view = imageView.RawHandle;
            var image = imageView.VulkanImage;
            if (view.Handle == 0) throw new NullReferenceException("No ImageView with target Image");
            var descriptor = new DescriptorImageInfo
            {
                ImageLayout = imageLayout,
                ImageView = view
            };
            var wSet = new WriteDescriptorSet
            {
                DescriptorType = descriptorType,
                ImageInfo = new DescriptorImageInfo[] { descriptor },
                DescriptorCount = descriptorCount,
                DestinationBinding = binding,
                DestinationArrayElement = arrayElement
            };
            texture.WriteDescriptorSet = wSet;
            texture.ImageInfo = new VulkanImageInfo
            {
                DeviceMemory = image.DeviceMemory,
                Image = image,
                ImageView = imageView,
                ImageFormat = image.CreateOption.Format,
                ImageType = image.CreateOption.ImageType,
                MemoryType = image.DeviceMemory.PropertyFlags,
                Size = texture.BytesSize,
                Usage = image.CreateOption.Usage,
                WriteDescriptorSet = wSet
            };
            texture.IsLoaded = true;
        }
        /// <summary>
        /// 创建缓冲数据
        /// </summary>
        /// <param name="textureData"></param>
        /// <param name="manager"></param>
        /// <param name="option"></param>
        /// <param name="deviceType"></param>
        /// <param name="size"></param>
        /// <param name="logicDevice"></param>
        /// <returns></returns>
        private static VulkanBufferTextureData BuildBufferData(VulkanBufferTextureData textureData, VulkanGraphicsManager manager, BufferCreateOption option,
            MemoryPropertyFlags deviceType, ulong size, VulkanDevice logicDevice)
        {
            var buffer = logicDevice.CreateBuffer(option, size, deviceType);
            var deivceMemory = buffer.DeviceMemory;
            var dInfo = new DescriptorBufferInfo { Buffer = buffer.RawHandle, Offset = 0, Range = size };
            var writeSet = new WriteDescriptorSet
            {
                BufferInfo = new DescriptorBufferInfo[] { dInfo },
                DescriptorType = option.DescriptorType
            };
            var info = new VulkanBufferInfo
            {
                Buffer = buffer,
                BufferUsageFlags = option.Usage,
                DeviceMemory = deivceMemory,
                MemoryType = deviceType,
                WriteDescriptorSet = writeSet,
                Size = size,
            };
            textureData.WriteDescriptorSet = writeSet;
            textureData.BufferInfo = info;
            textureData.BytesSize = size;
            return textureData;
        }
        /// <summary>
        /// 构建结构化缓冲数据
        /// </summary>
        /// <param name="textureData"></param>
        /// <param name="manager"></param>
        /// <param name="option"></param>
        /// <param name="format"></param>
        /// <param name="deviceType"></param>
        /// <param name="size"></param>
        /// <param name="logicDevice"></param>
        /// <returns></returns>
        private static VulkanBufferTextureData BuildTexelBufferData(VulkanBufferTextureData textureData, VulkanGraphicsManager manager, BufferCreateOption option,
            Format format, MemoryPropertyFlags deviceType, ulong size, VulkanDevice logicDevice)
        {
            var buffer = logicDevice.CreateBuffer(option, size, deviceType);
            var deivceMemory = buffer.DeviceMemory;
            var dInfo = new DescriptorBufferInfo { Buffer = buffer.RawHandle, Offset = 0, Range = size };
            var bufferView = logicDevice.CreateBufferView(buffer, format, 0, size);
            var writeSet = new WriteDescriptorSet
            {
                TexelBufferView = new BufferView[] { bufferView.RawHandle },
                DescriptorType = option.DescriptorType
            };
            var info = new VulkanBufferInfo
            {
                Buffer = buffer,
                BufferUsageFlags = option.Usage,
                DeviceMemory = deivceMemory,
                WriteDescriptorSet = writeSet,
                Size = size,
            };
            textureData.WriteDescriptorSet = writeSet;
            textureData.BufferInfo = info;
            textureData.BytesSize = size;
            return textureData;
        }

        /// <summary>
        /// 构建图片
        /// </summary>
        /// <param name="texture"></param>
        /// <param name="option"></param>
        /// <param name="logicDevice"></param>
        /// <param name="extent"></param>
        /// <param name="flags"></param>
        /// <param name="memoryFlags"></param>
        /// <returns></returns>
        private static VulkanImageView BuildImage(VulkanTextureData texture, ImageAndImageViewCreateOption option,
            VulkanDevice logicDevice, Extent3D extent, FormatFeatureFlags flags, MemoryPropertyFlags memoryFlags = 0)
        {
            var imageC = option.ImageCreateOption;
            var viewC = option.ImageViewCreateOption;
            viewC.Format = imageC.Format;
            var phy = logicDevice.PhysicalDevice;
            var prop = phy.GetFormatProperties(imageC.Format);
            if (prop.OptimalTilingFeatures.HasFlag(flags)) imageC.Tiling = ImageTiling.Optimal;
            else if (prop.LinearTilingFeatures.HasFlag(flags)) imageC.Tiling = ImageTiling.Linear;
            else
            {
                var prop2 = phy.GetFormatProperties2(imageC.Format);
                if (prop2.FormatProperties.OptimalTilingFeatures.HasFlag(flags)) imageC.Tiling = ImageTiling.Optimal;
                else if (prop2.FormatProperties.LinearTilingFeatures.HasFlag(flags)) imageC.Tiling = ImageTiling.Linear;
                throw new NotSupportedException($"Image Format {imageC.Format} is not supported");
            }
            imageC.Extent = extent;
            texture.Height = extent.Height; texture.Width = extent.Width; texture.Depth = extent.Depth;
            texture.ArrayLayers = imageC.ArrayLayers; uint level = 1;
            if (imageC.MipLevels > 1)
            {
                if (prop.LinearTilingFeatures.HasFlag(FormatFeatureFlags.BlitSrcBit))
                {
                    uint floor = (uint)(Math.Floor(Math.Log2(Math.Max(texture.Width, texture.Height))) + 1);
                    level = imageC.MipLevels > floor ? floor : imageC.MipLevels;
                }
            }
            imageC.MipLevels = level;
            ImageSubresourceRange subresource = viewC.SubresourceRange;
            subresource.LevelCount = level;
            viewC.SubresourceRange = subresource;
            var image = logicDevice.CreateImage(option, extent, null, memoryFlags);
            return image;
        }
    }
}
