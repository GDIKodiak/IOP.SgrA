﻿using Silk.NET.Vulkan;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class VulkanDynamicBuffer : VulkanTexture, IDynamicBufferTexture
    {
        /// <summary>
        /// 缓冲长度
        /// </summary>
        public ulong Size => BufferData != null ? BufferData.BytesSize : 0;

        /// <summary>
        /// 块大小
        /// </summary>
        public uint BlockSize => BufferData != null ? BufferData.BlockSize : 0;
        /// <summary>
        /// 缓冲块容量
        /// </summary>
        public uint BlockCapacity => BufferData != null ? BufferData.BlockCapacity : 0;

        internal VulkanDynamicBufferData BufferData;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="setIndex"></param>
        /// <param name="binding"></param>
        /// <param name="name"></param>
        /// <param name="graphicsManager"></param>
        /// <param name="descriptorCount"></param>
        /// <param name="arrayElement"></param>
        /// <param name="descriptorType"></param>
        public VulkanDynamicBuffer(VulkanGraphicsManager graphicsManager, string name, VulkanDynamicBufferData buffer, 
            uint binding = 0, uint descriptorCount = 1, uint arrayElement = 0,
            uint setIndex = 0, DescriptorType descriptorType = DescriptorType.UniformBufferDynamic)
            : base(name, null, buffer, binding, descriptorCount, arrayElement, setIndex, descriptorType)
        {
            if (buffer == null) throw new ArgumentNullException(nameof(buffer));
            DescriptorObjectType = ConvertDescriptorType(buffer.WriteDescriptorSet.DescriptorType);
            BufferData = buffer;
            var writeSet = BufferData.WriteDescriptorSet;
            writeSet.DescriptorCount = descriptorCount;
            writeSet.DestinationArrayElement = arrayElement;
            writeSet.DescriptorType = descriptorType;
            for(int i = 0; i < writeSet.BufferInfo.Length; i++)
            {
                var local = writeSet.BufferInfo[i];
                local.Range = buffer.BlockSize;
                writeSet.BufferInfo[i] = local;
            }
            var copy = CopyWriteDescriptor(writeSet);
            WriteDescriptorSet = copy;
            ArrayLayers = BufferData.ArrayLayers;
            Binding = binding;
            DescriptorCount = descriptorCount;
            ArrayElement = arrayElement;
            SetIndex = setIndex;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="updateData"></param>
        /// <exception cref="NullReferenceException"></exception>
        public override void UpdateTextureMemoryData(byte[] updateData)
        {
            if (BufferData == null) throw new NullReferenceException("No TextureData");
            BufferData.UpdateMemoryData(updateData);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="updateData"></param>
        /// <param name="startIndex"></param>
        /// <exception cref="NullReferenceException"></exception>
        public override void UpdateTextureMemoryData(byte[] updateData, int startIndex)
        {
            if (BufferData == null) throw new NullReferenceException("No TextureData");
            BufferData.UpdateMemoryData(updateData, startIndex);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="updateData"></param>
        /// <exception cref="NullReferenceException"></exception>
        public override void UpdateTextureMemoryData(Span<byte> updateData)
        {
            if (BufferData == null) throw new NullReferenceException("No TextureData");
            BufferData.UpdateMemoryData(updateData);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="updateData"></param>
        /// <param name="startIndex"></param>
        /// <exception cref="NullReferenceException"></exception>
        public override void UpdateTextureMemoryData(Span<byte> updateData, int startIndex)
        {
            if (BufferData == null) throw new NullReferenceException("No TextureData");
            BufferData.UpdateMemoryData(updateData, startIndex);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TStruct"></typeparam>
        /// <param name="struct"></param>
        public override void UpdateTextureMemoryData<TStruct>(TStruct @struct)
        {
            var size = Marshal.SizeOf<TStruct>();
            byte[] data = ArrayPool<byte>.Shared.Rent(size);
            Span<byte> local = data;
            local = local.Slice(0, size);
            try
            {
                MemoryMarshal.Write(local, ref @struct);
                UpdateTextureMemoryData(local);
            }
            finally
            {
                ArrayPool<byte>.Shared.Return(data);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TStruct"></typeparam>
        /// <param name="struct"></param>
        /// <param name="startIndex"></param>
        public override void UpdateTextureMemoryData<TStruct>(TStruct @struct, int startIndex)
        {
            var size = Marshal.SizeOf<TStruct>();
            byte[] data = ArrayPool<byte>.Shared.Rent(size);
            Span<byte> local = data;
            local = local[..size];
            try
            {
                MemoryMarshal.Write(local, ref @struct);
                UpdateTextureMemoryData(local, startIndex);
            }
            finally
            {
                ArrayPool<byte>.Shared.Return(data);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newObj"></param>
        public override void CloneToNewRender(IRenderObject newObj)
        {
            var newTex = new VulkanDynamicBuffer(GraphicsManager, Name, BufferData, Binding, DescriptorCount, ArrayElement, SetIndex,
                WriteDescriptorSet.DescriptorType);
            newObj.AddComponent(newTex);
        }
        /// <summary>
        /// 申请纹理块
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        /// <exception cref="NullReferenceException"></exception>
        public ITextureBlock MallocBlock()
        {
            if (BufferData == null) throw new NullReferenceException(nameof(BufferData));
            var block = BufferData.MallocBlock();
            block.DescriptorSet = DescriptorSet;
            return block;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="size"></param>
        /// <param name="block"></param>
        /// <returns></returns>
        /// <exception cref="NullReferenceException"></exception>
        public bool TryMallocBlock(out ITextureBlock block)
        {
            if (BufferData == null) throw new NullReferenceException(nameof(BufferData));
            bool result = BufferData.TryMallocBlock(out VulkanDynamicBufferBlock b);
            b.DescriptorSet = DescriptorSet;
            block = b;
            return result;
        }
        /// <summary>
        /// 回收资源块
        /// </summary>
        /// <param name="block"></param>
        public void Recovery(ITextureBlock block)
        {
            if (BufferData == null) throw new NullReferenceException(nameof(BufferData));
            if (block is not VulkanDynamicBufferBlock vBlock) return;
            BufferData.RecoveryBlock(vBlock);
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Destroy()
        {
            GraphicsManager?.DestroyTextureData(BufferData);
        }


        /// <summary>
        /// 深拷贝写入描述符集
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private WriteDescriptorSet CopyWriteDescriptor(WriteDescriptorSet source)
        {
            var bufferInfos = source.BufferInfo;
            var bufferView = source.TexelBufferView;
            var result = new WriteDescriptorSet()
            {
                DestinationArrayElement = source.DestinationArrayElement,
                DescriptorCount = source.DescriptorCount,
                DescriptorType = source.DescriptorType,
                DestinationBinding = source.DestinationBinding,
                DestinationSet = default
            };
            if (bufferInfos != null)
            {
                var copyBuffer = new DescriptorBufferInfo[bufferInfos.Length];
                for (int i = 0; i < bufferInfos.Length; i++)
                {
                    copyBuffer[i] = new DescriptorBufferInfo
                    {
                        Buffer = bufferInfos[i].Buffer,
                        Offset = bufferInfos[i].Offset,
                        Range = bufferInfos[i].Range
                    };
                }
                result.BufferInfo = copyBuffer;
            }
            result.TexelBufferView = bufferView;
            return result;
        }
    }
}
