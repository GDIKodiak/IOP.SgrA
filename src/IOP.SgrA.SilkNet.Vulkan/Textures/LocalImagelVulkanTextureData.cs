﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 本地纹理数据
    /// </summary>
    public class LocalImagelVulkanTextureData : VulkanImageTextureData
    {
        /// <summary>
        /// 禁用加载
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public override Task Load(byte[] data) => throw new InvalidOperationException("DeviceLocalTexture is not supported to load data");
        /// <summary>
        /// 禁用加载
        /// </summary>
        /// <param name="fileInfo"></param>
        /// <returns></returns>
        public override Task Load(FileInfo fileInfo) => throw new InvalidOperationException("DeviceLocalTexture is not supported to load data");
        /// <summary>
        /// 禁用加载
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public override Task Load(Stream stream) => throw new InvalidOperationException("DeviceLocalTexture is not supported to load data");
    }
}
