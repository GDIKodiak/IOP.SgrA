﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class Bn3dtexVulkanTextureData : VulkanImageTextureData
    {
        /// <summary>
        /// 
        /// </summary>
        public Bn3dtexHeader Header { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public override async Task Load(Stream stream)
        {
            int size = Marshal.SizeOf<Bn3dtexHeader>();
            Bn3dtexHeader header;
            byte[] data = ArrayPool<byte>.Shared.Rent(size);
            Memory<byte> local = new Memory<byte>(data).Slice(0, size);
            await stream.ReadAsync(local);
            header = MemoryMarshal.Read<Bn3dtexHeader>(local.Span);
            Header = header;
            BytesSize = header.Width * header.Height * header.Depth * 4;
            Width = header.Width;
            Height = header.Height;
            Depth = header.Depth;
            await base.Load(stream);
            ArrayPool<byte>.Shared.Return(data);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public override async Task Load(byte[] data)
        {
            int size = Marshal.SizeOf<Bn3dtexHeader>(); int index = 0;
            Bn3dtexHeader header;
            Memory<byte> d = data; Memory<byte> local = d.Slice(0, size);
            header = MemoryMarshal.Read<Bn3dtexHeader>(local.Span); index += size;
            Header = header;
            BytesSize = header.Width * header.Height * header.Depth * 4;
            Width = header.Width;
            Height = header.Height;
            Depth = header.Depth;
            await base.Load(d[index..].ToArray());
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public struct Bn3dtexHeader
    {
        /// <summary>
        /// 
        /// </summary>
        public uint Width;
        /// <summary>
        /// 
        /// </summary>
        public uint Height;
        /// <summary>
        /// 
        /// </summary>
        public uint Depth;
    }
}
