﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 图片纹理数据
    /// </summary>
    public class VulkanImageTextureData : VulkanTextureData
    {
        /// <summary>
        /// 图像信息
        /// </summary>
        public VulkanImageInfo ImageInfo { get; internal protected set; }
        /// <summary>
        /// 获取设备内存
        /// </summary>
        public override VulkanDeviceMemory GetDeviceMemory() => ImageInfo.DeviceMemory;
        /// <summary>
        /// 更新设备内存数据
        /// </summary>
        /// <param name="updateData"></param>
        public override void UpdateMemoryData(byte[] updateData)
        {
            if (updateData == null) throw new ArgumentNullException(nameof(updateData));
            if (!ImageInfo.Usage.HasFlag(ImageUsageFlags.StorageBit) || ImageInfo.MemoryType.HasFlag(MemoryPropertyFlags.DeviceLocalBit))
                throw new InvalidOperationException("Only Storage image could update memory data");
            ulong size = ImageInfo.Size;
            if ((ulong)updateData.LongLength > size) throw new IndexOutOfRangeException("Update data is too bigger than target memory");
            VulkanDeviceMemory memory = GetDeviceMemory() ?? throw new NullReferenceException("Target texture data could not have device memory");
            memory.UpdateMemory(updateData, 0);
        }
        /// <summary>
        /// 更新设备内存数据
        /// </summary>
        /// <param name="updateData"></param>
        /// <param name="startIndex"></param>
        public override void UpdateMemoryData(byte[] updateData, int startIndex)
        {
            if (updateData == null) throw new ArgumentNullException(nameof(updateData));
            if (!ImageInfo.Usage.HasFlag(ImageUsageFlags.StorageBit) || ImageInfo.MemoryType.HasFlag(MemoryPropertyFlags.DeviceLocalBit))
                throw new InvalidOperationException("Only Storage image could update memory data");
            ulong size = ImageInfo.Size;
            if ((ulong)(updateData.LongLength + startIndex) > size) throw new IndexOutOfRangeException("Update data is too bigger than target memory");
            VulkanDeviceMemory memory = GetDeviceMemory() ?? throw new NullReferenceException("Target texture data could not have device memory");
            memory.UpdateMemory(updateData, (ulong)startIndex);
        }
        /// <summary>
        /// 更新设备内存数据
        /// </summary>
        /// <param name="updateData"></param>
        public override void UpdateMemoryData(Span<byte> updateData) => UpdateMemoryData(updateData.ToArray());
        /// <summary>
        /// 更新设备内存数据
        /// </summary>
        /// <param name="updateData"></param>
        /// <param name="startIndex"></param>
        public override void UpdateMemoryData(Span<byte> updateData, int startIndex) => UpdateMemoryData(updateData.ToArray(), startIndex);
        /// <summary>
        /// 获取内存数据
        /// </summary>
        /// <returns></returns>
        public override Span<byte> GetMemoryData()
        {
            if (!ImageInfo.Usage.HasFlag(ImageUsageFlags.StorageBit) || ImageInfo.MemoryType.HasFlag(MemoryPropertyFlags.DeviceLocalBit))
                throw new InvalidOperationException("Only Storage image could get memory data");
            VulkanDeviceMemory memory = GetDeviceMemory() ?? throw new NullReferenceException("Target texture data could not have device memory");
            int size = (int)ImageInfo.Size;
            byte[] data = new byte[size];
            memory.CopyTo(data, 0, (ulong)size);
            return new Span<byte>(data);
        }
        /// <summary>
        /// 针对图片类型的纹理返回浅拷贝
        /// </summary>
        /// <param name="manager"></param>
        /// <returns></returns>
        public override VulkanTextureData Copy(VulkanGraphicsManager manager) => this;

        /// <summary>
        /// 销毁资源
        /// </summary>
        public override void Dispose()
        {
            ImageInfo.Image?.Dispose();
            ImageInfo = default;
            base.Dispose();
        }
    }
}
