﻿using Silk.NET.Vulkan;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipelines;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// Vulkan用纹理数据
    /// </summary>
    public class VulkanTextureData : ITextureData
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; internal protected set; }
        /// <summary>
        /// 宽度
        /// </summary>
        public uint Width { get; internal protected set; } = 1;
        /// <summary>
        /// 高度
        /// </summary>
        public uint Height { get; internal protected set; } = 1;
        /// <summary>
        /// 深度
        /// </summary>
        public uint Depth { get; internal protected set; } = 1;
        /// <summary>
        /// 纹理数组数量
        /// </summary>
        public uint ArrayLayers { get; internal protected set; } = 1;
        /// <summary>
        /// 缓冲
        /// </summary>
        public virtual IntPtr Buffer { get; protected set; } = IntPtr.Zero;
        /// <summary>
        /// 字节长度
        /// </summary>
        public ulong BytesSize { get; internal protected set; }
        /// <summary>
        /// 是否完成加载
        /// </summary>
        public bool IsLoaded { get; internal protected set; }
        /// <summary>
        /// 写入描述集
        /// </summary>
        public WriteDescriptorSet WriteDescriptorSet { get; internal protected set; }
        /// <summary>
        /// 
        /// </summary>
        protected Pipe Pipe { get; set; } = new Pipe();

        /// <summary>
        /// 
        /// </summary>
        protected int Step = 2048;
        /// <summary>
        /// 
        /// </summary>
        protected bool IsDispose = false;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="name"></param>
        public VulkanTextureData(string name) => Name = name;
        /// <summary>
        /// 构造函数
        /// </summary>
        public VulkanTextureData() { }

        /// <summary>
        /// 加载纹理
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public virtual Task Load(byte[] data)
        {
            if (IsLoaded) return Task.CompletedTask;
            Buffer = Marshal.AllocHGlobal(data.Length);
            BytesSize = (ulong)(data.Length);
            Marshal.Copy(data, 0, Buffer, data.Length);
            IsLoaded = true;
            return Task.CompletedTask;
        }
        /// <summary>
        /// 加载纹理
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public async virtual Task Load(Stream stream)
        {
            if (IsLoaded) return;
            if (BytesSize == 0) BytesSize = (ulong)(stream.Length - stream.Position);
            Buffer = Marshal.AllocHGlobal((IntPtr)(BytesSize));
            var f = FillPipeline(stream, Pipe.Writer);
            var r = ReadPipeline(Pipe.Reader);
            await Task.WhenAll(f, r);
            Pipe.Reset();
            IsLoaded = true;
        }
        /// <summary>
        /// 加载纹理
        /// </summary>
        /// <param name="fileInfo"></param>
        /// <returns></returns>
        public async virtual Task Load(FileInfo fileInfo)
        {
            if (IsLoaded) return;
            using FileStream fileStream = fileInfo.OpenRead();
            await Load(fileStream);
        }

        /// <summary>
        /// 更新内存数据
        /// </summary>
        /// <param name="updateData"></param>
        public virtual void UpdateMemoryData(byte[] updateData) { }
        /// <summary>
        /// 更新内存数据
        /// </summary>
        /// <param name="updateData"></param>
        public virtual void UpdateMemoryData(Span<byte> updateData) { }
        /// <summary>
        /// 更新内存数据
        /// </summary>
        /// <param name="updateData"></param>
        /// <param name="startIndex"></param>
        public virtual void UpdateMemoryData(Span<byte> updateData, int startIndex) { }
        /// <summary>
        /// 更新内存数据
        /// </summary>
        /// <param name="updateData"></param>
        /// <param name="startIndex"></param>
        public virtual void UpdateMemoryData(byte[] updateData, int startIndex) { }
        /// <summary>
        /// 获取设备内存
        /// </summary>
        /// <returns></returns>
        public virtual VulkanDeviceMemory GetDeviceMemory() { return default; }
        /// <summary>
        /// 获取内存数据
        /// </summary>
        /// <returns></returns>
        public virtual Span<byte> GetMemoryData() { return Span<byte>.Empty; }
        /// <summary>
        /// 拷贝
        /// </summary>
        /// <param name="manager"></param>
        /// <returns></returns>
        public virtual VulkanTextureData Copy(VulkanGraphicsManager manager) { return new VulkanTextureData(); }
        /// <summary>
        /// 当渲染对象被摧毁时
        /// </summary>
        /// <param name="device"></param>
        public virtual void OnRenderObjectDestroyed(VulkanDevice device)
        {

        }

        /// <summary>
        /// 释放资源
        /// </summary>
        public virtual void Dispose()
        {
            if (!IsDispose)
            {
                IsDispose = true;
                IsLoaded = false;
                if (Buffer != IntPtr.Zero)
                {
                    Marshal.FreeHGlobal(Buffer);
                    Buffer = IntPtr.Zero;
                }
            }
        }

        /// <summary>
        /// 清理缓存
        /// </summary>
        public void Clear()
        {
            if (!IsLoaded) return;
            IsLoaded = false;
            if (Buffer != IntPtr.Zero)
            {
                Marshal.FreeHGlobal(Buffer);
                Buffer = IntPtr.Zero;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="writer"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected async Task FillPipeline(Stream stream, PipeWriter writer)
        {
            try
            {
                while (true)
                {
                    Memory<byte> memory = writer.GetMemory(Step);
                    int bytesRead = await stream.ReadAsync(memory);
                    writer.Advance(bytesRead);
                    if (bytesRead == 0) break;
                    FlushResult result = await writer.FlushAsync();
                    if (result.IsCompleted) break;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                writer.Complete();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected async Task ReadPipeline(PipeReader reader)
        {
            try
            {
                IntPtr ptr = Buffer;
                ulong index = 0;
                ulong size = BytesSize;
                int diff;
                ulong old;
                byte[] local;
                while (true)
                {
                    ReadResult result = await reader.ReadAsync();
                    ReadOnlySequence<byte> buffer = result.Buffer;
                    local = buffer.ToArray();
                    old = index;
                    index += (ulong)local.Length;
                    if (index > size)
                    {
                        diff = (int)(size - old);
                        Marshal.Copy(local, 0, ptr, diff);
                        break;
                    }
                    diff = local.Length;
                    Marshal.Copy(local, 0, ptr, diff);
                    ptr = IntPtr.Add(ptr, diff);
                    reader.AdvanceTo(buffer.End);
                    if (result.IsCompleted) break;
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                reader.Complete();
            }
        }
    }
}
