﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class DdsVulkanTextureData : VulkanImageTextureData
    {
        /// <summary>
        /// 
        /// </summary>
        public const int RESERVED1COUNT = 44;
        /// <summary>
        /// 
        /// </summary>
        public const int HEADERSIZE = 128;
        /// <summary>
        /// 
        /// </summary>
        public const int DDPIXELFORMATSIZE = 32;
        /// <summary>
        /// 
        /// </summary>
        public const int DDCAPS2SIZE = 16;
        /// <summary>
        /// 
        /// </summary>
        public const int DXT10HEADERSIZE = 20;
        /// <summary>
        /// 
        /// </summary>
        public uint Magic { get; protected set; }
        /// <summary>
        /// 
        /// </summary>
        public DDS_HEADER Header { get; protected set; }
        /// <summary>
        /// 
        /// </summary>
        public DDS_Header_DXT10? DXT10Header { get; protected set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public override async Task Load(Stream stream)
        {
            byte[] data = ArrayPool<byte>.Shared.Rent(HEADERSIZE);
            Memory<byte> header = new Memory<byte>(data).Slice(0, HEADERSIZE);
            await stream.ReadAsync(header);
            Memory<byte> local = header;
            ReadHeader(local);
            if(BytesSize <= 0)
            {
                byte[] dxt10Data = ArrayPool<byte>.Shared.Rent(DXT10HEADERSIZE);
                Memory<byte> dxt10Header = new Memory<byte>(dxt10Data).Slice(0, DXT10HEADERSIZE);
                await stream.ReadAsync(dxt10Header);
                ReadDXT10Header(dxt10Header, Header.Height, Header.Width);
                ArrayPool<byte>.Shared.Return(dxt10Data);
            }
            ArrayPool<byte>.Shared.Return(data);
            await base.Load(stream);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public override async Task Load(byte[] data)
        {
            Memory<byte> local = data; int index = 0;
            Memory<byte> header = local.Slice(0, HEADERSIZE); index += HEADERSIZE;
            ReadHeader(header);
            if (BytesSize <= 0)
            {
                Memory<byte> dxt10Header = local.Slice(index, DXT10HEADERSIZE);
                ReadDXT10Header(dxt10Header, Header.Height, Header.Width); index += DXT10HEADERSIZE;
            }
            await base.Load(local[index..].ToArray());
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bytes"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void ReadHeader(Memory<byte> bytes)
        {
            int index = 0;
            DDS_HEADER formatHeader = new DDS_HEADER();
            Magic = MemoryMarshal.Read<uint>(bytes.Span.Slice(index, 4));
            index += 4;
            formatHeader.Size = MemoryMarshal.Read<uint>(bytes.Span.Slice(index, 4));
            index += 4;
            formatHeader.Flags = MemoryMarshal.Read<uint>(bytes.Span.Slice(index, 4));
            index += 4;
            formatHeader.Height = MemoryMarshal.Read<uint>(bytes.Span.Slice(index, 4));
            index += 4;
            formatHeader.Width = MemoryMarshal.Read<uint>(bytes.Span.Slice(index, 4));
            index += 4;
            formatHeader.PitchOrLinearSize = MemoryMarshal.Read<uint>(bytes.Span.Slice(index, 4));
            index += 4;
            formatHeader.Depth = MemoryMarshal.Read<uint>(bytes.Span.Slice(index, 4));
            index += 4;
            formatHeader.MipMapCount = MemoryMarshal.Read<uint>(bytes.Span.Slice(index, 4));
            index += 4 + RESERVED1COUNT;
            formatHeader.PixelFormat = MemoryMarshal.Read<DDPIXELFORMAT>(bytes.Span.Slice(index, DDPIXELFORMATSIZE));
            index += DDPIXELFORMATSIZE;
            formatHeader.Caps = MemoryMarshal.Read<DDCAPS2>(bytes.Span.Slice(index, DDCAPS2SIZE));
            Header = formatHeader;
            uint count = GetDataCount(formatHeader.PixelFormat, formatHeader.Width, formatHeader.Height);
            Height = formatHeader.Height;
            Width = formatHeader.Width;
            BytesSize = count;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="height"></param>
        /// <param name="width"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void ReadDXT10Header(Memory<byte> bytes, uint height, uint width)
        {
            int index = 0;
            DDS_Header_DXT10 header = new DDS_Header_DXT10();
            header.DxgiFormat = (DXGI_FORMAT)MemoryMarshal.Read<int>(bytes.Span.Slice(index, 4));
            index += 4;
            header.ResourceDimension = (D3D10_RESOURCE_DIMENSION)MemoryMarshal.Read<int>(bytes.Span.Slice(index, 4));
            index += 4;
            header.MiscFlag = MemoryMarshal.Read<uint>(bytes.Span.Slice(index, 4));
            index += 4;
            header.AraySize = MemoryMarshal.Read<uint>(bytes.Span.Slice(index, 4));
            index += 4;
            header.MiscFlags2 = MemoryMarshal.Read<uint>(bytes.Span.Slice(index, 4));
            uint count = GetDataCount(header.DxgiFormat, width, height, header.AraySize);
            BytesSize = count;
            DXT10Header = header;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private uint GetDataCount(DDPIXELFORMAT format, uint width, uint height)
        {
            uint r = 0;
            uint flags = format.Flags;
            if((flags & 0x4) != 0)
            {
                uint fourCC = format.FourCC;
                Span<uint> o = stackalloc uint[] { fourCC };
                Span<byte> l = MemoryMarshal.AsBytes(o);
                string type = Encoding.ASCII.GetString(l);
                switch (type)
                {
                    case "DXT1":
                        r = Math.Max(1, (width + 3) / 4) * Math.Max(1, (height + 3) / 4) * 8;
                        break;
                    case "DXT2":
                    case "DXT3":
                    case "DXT4":
                    case "DXT5":
                        r = Math.Max(1, (width + 3) / 4) * Math.Max(1, (height + 3) / 4) * 16;
                        break;
                    case "DX10":
                        r = 0;
                        break;
                    default:
                        throw new InvalidCastException("Unknow DXT Format");
                }
            }
            else if((flags & 0x40) != 0 || (flags & 0x20000) != 0 || (flags & 1) != 0)
            {
                r = ((format.RGBBitCount * width + 7) / 8) * height;
            }
            return r;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private uint GetDataCount(DXGI_FORMAT format, uint width, uint height, uint arrayLayers)
        {
            uint r;
            switch (format)
            {
                case DXGI_FORMAT.DXGI_FORMAT_BC1_UNORM:
                case DXGI_FORMAT.DXGI_FORMAT_BC1_UNORM_SRGB:
                case DXGI_FORMAT.DXGI_FORMAT_BC4_TYPELESS:
                case DXGI_FORMAT.DXGI_FORMAT_BC4_UNORM:
                case DXGI_FORMAT.DXGI_FORMAT_BC4_SNORM:
                    r = (Math.Max(1, (width + 3) / 4) * Math.Max(1, (height + 3) / 4) * 8) * arrayLayers;
                    break;
                case DXGI_FORMAT.DXGI_FORMAT_BC2_TYPELESS:
                case DXGI_FORMAT.DXGI_FORMAT_BC2_UNORM:
                case DXGI_FORMAT.DXGI_FORMAT_BC2_UNORM_SRGB:
                case DXGI_FORMAT.DXGI_FORMAT_BC3_TYPELESS:
                case DXGI_FORMAT.DXGI_FORMAT_BC3_UNORM:
                case DXGI_FORMAT.DXGI_FORMAT_BC3_UNORM_SRGB:
                case DXGI_FORMAT.DXGI_FORMAT_BC5_TYPELESS:
                case DXGI_FORMAT.DXGI_FORMAT_BC5_UNORM:
                case DXGI_FORMAT.DXGI_FORMAT_BC5_SNORM:
                case DXGI_FORMAT.DXGI_FORMAT_BC6H_TYPELESS:
                case DXGI_FORMAT.DXGI_FORMAT_BC6H_UF16:
                case DXGI_FORMAT.DXGI_FORMAT_BC6H_SF16:
                case DXGI_FORMAT.DXGI_FORMAT_BC7_TYPELESS:
                case DXGI_FORMAT.DXGI_FORMAT_BC7_UNORM:
                case DXGI_FORMAT.DXGI_FORMAT_BC7_UNORM_SRGB:
                    r = (Math.Max(1, (width + 3) / 4) * Math.Max(1, (height + 3) / 4) * 16) * arrayLayers;
                    break;
                case DXGI_FORMAT.DXGI_FORMAT_R8G8_B8G8_UNORM:
                case DXGI_FORMAT.DXGI_FORMAT_G8R8_G8B8_UNORM:
                case DXGI_FORMAT.DXGI_FORMAT_AYUV:
                case DXGI_FORMAT.DXGI_FORMAT_YUY2:
                    r = (((height + 1) >> 1) * (width + 1) >> 1 * 4) * arrayLayers;
                    break;
                default:
                    throw new NotSupportedException();
            }
            return r;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public struct DDS_HEADER
    {
        /// <summary>
        /// 
        /// </summary>
        public uint Size;
        /// <summary>
        /// 
        /// </summary>
        public uint Flags;
        /// <summary>
        /// 
        /// </summary>
        public uint Height;
        /// <summary>
        /// 
        /// </summary>
        public uint Width;
        /// <summary>
        /// 
        /// </summary>
        public uint PitchOrLinearSize;
        /// <summary>
        /// 
        /// </summary>
        public uint Depth;
        /// <summary>
        /// 
        /// </summary>
        public uint MipMapCount;
        /// <summary>
        /// 
        /// </summary>
        public DDPIXELFORMAT PixelFormat;
        /// <summary>
        /// 
        /// </summary>
        public DDCAPS2 Caps;
    }

    /// <summary>
    /// DXT10以上版本头
    /// </summary>
    public struct DDS_Header_DXT10
    {
        /// <summary>
        /// 
        /// </summary>
        public DXGI_FORMAT DxgiFormat;
        /// <summary>
        /// 
        /// </summary>
        public D3D10_RESOURCE_DIMENSION ResourceDimension;
        /// <summary>
        /// 
        /// </summary>
        public uint MiscFlag;
        /// <summary>
        /// 
        /// </summary>
        public uint AraySize;
        /// <summary>
        /// 
        /// </summary>
        public uint MiscFlags2;
    }

    /// <summary>
    /// 
    /// </summary>
    [StructLayout(LayoutKind.Explicit)]
    public struct DDPIXELFORMAT
    {
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public uint Size;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(4)]
        public uint Flags;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(8)]
        public uint FourCC;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(12)]
        public uint RGBBitCount;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(16)]
        public uint RBitMask;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(20)]
        public uint GBitMask;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(24)]
        public uint BBitMask;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(28)]
        public uint RGBAlphaBitMask;
        
    }

    /// <summary>
    /// 
    /// </summary>
    [StructLayout(LayoutKind.Explicit)]
    public struct DDCAPS2
    {
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(0)]
        public uint Caps1;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(4)]
        public uint Caps2;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(8)]
        public uint Caps3;
        /// <summary>
        /// 
        /// </summary>
        [FieldOffset(12)]
        public uint Caps4;
    }

    /// <summary>
    /// 格式
    /// </summary>
    public enum DXGI_FORMAT
    {
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_UNKNOWN,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R32G32B32A32_TYPELESS,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R32G32B32A32_FLOAT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R32G32B32A32_UINT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R32G32B32A32_SINT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R32G32B32_TYPELESS,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R32G32B32_FLOAT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R32G32B32_UINT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R32G32B32_SINT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R16G16B16A16_TYPELESS,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R16G16B16A16_FLOAT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R16G16B16A16_UNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R16G16B16A16_UINT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R16G16B16A16_SNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R16G16B16A16_SINT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R32G32_TYPELESS,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R32G32_FLOAT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R32G32_UINT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R32G32_SINT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R32G8X24_TYPELESS,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_D32_FLOAT_S8X24_UINT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_X32_TYPELESS_G8X24_UINT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R10G10B10A2_TYPELESS,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R10G10B10A2_UNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R10G10B10A2_UINT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R11G11B10_FLOAT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R8G8B8A8_TYPELESS,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R8G8B8A8_UNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R8G8B8A8_UNORM_SRGB,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R8G8B8A8_UINT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R8G8B8A8_SNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R8G8B8A8_SINT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R16G16_TYPELESS,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R16G16_FLOAT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R16G16_UNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R16G16_UINT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R16G16_SNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R16G16_SINT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R32_TYPELESS,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_D32_FLOAT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R32_FLOAT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R32_UINT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R32_SINT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R24G8_TYPELESS,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_D24_UNORM_S8_UINT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R24_UNORM_X8_TYPELESS,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_X24_TYPELESS_G8_UINT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R8G8_TYPELESS,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R8G8_UNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R8G8_UINT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R8G8_SNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R8G8_SINT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R16_TYPELESS,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R16_FLOAT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_D16_UNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R16_UNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R16_UINT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R16_SNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R16_SINT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R8_TYPELESS,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R8_UNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R8_UINT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R8_SNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R8_SINT,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_A8_UNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R1_UNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R9G9B9E5_SHAREDEXP,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R8G8_B8G8_UNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_G8R8_G8B8_UNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_BC1_TYPELESS,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_BC1_UNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_BC1_UNORM_SRGB,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_BC2_TYPELESS,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_BC2_UNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_BC2_UNORM_SRGB,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_BC3_TYPELESS,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_BC3_UNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_BC3_UNORM_SRGB,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_BC4_TYPELESS,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_BC4_UNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_BC4_SNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_BC5_TYPELESS,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_BC5_UNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_BC5_SNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_B5G6R5_UNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_B5G5R5A1_UNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_B8G8R8A8_UNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_B8G8R8X8_UNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_B8G8R8A8_TYPELESS,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_B8G8R8A8_UNORM_SRGB,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_B8G8R8X8_TYPELESS,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_B8G8R8X8_UNORM_SRGB,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_BC6H_TYPELESS,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_BC6H_UF16,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_BC6H_SF16,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_BC7_TYPELESS,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_BC7_UNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_BC7_UNORM_SRGB,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_AYUV,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_Y410,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_Y416,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_NV12,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_P010,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_P016,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_420_OPAQUE,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_YUY2,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_Y210,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_Y216,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_NV11,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_AI44,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_IA44,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_P8,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_A8P8,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_B4G4R4A4_UNORM,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_P208,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_V208,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_V408,
        /// <summary>
        /// 
        /// </summary>
        DXGI_FORMAT_FORCE_UINT
    }
    /// <summary>
    /// 资源类型
    /// </summary>
    public enum D3D10_RESOURCE_DIMENSION
    {
        /// <summary>
        /// 
        /// </summary>
        D3D10_RESOURCE_DIMENSION_UNKNOWN,
        /// <summary>
        /// 
        /// </summary>
        D3D10_RESOURCE_DIMENSION_BUFFER,
        /// <summary>
        /// 
        /// </summary>
        D3D10_RESOURCE_DIMENSION_TEXTURE1D,
        /// <summary>
        /// 
        /// </summary>
        D3D10_RESOURCE_DIMENSION_TEXTURE2D,
        /// <summary>
        /// 
        /// </summary>
        D3D10_RESOURCE_DIMENSION_TEXTURE3D
    }
}
