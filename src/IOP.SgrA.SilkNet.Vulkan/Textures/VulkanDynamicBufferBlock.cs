﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class VulkanDynamicBufferBlock : ITextureBlock
    {
        private VulkanDynamicBufferData _Owner;

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public uint BlockSize { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public uint BlockOffset {  get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public DescriptorSet DescriptorSet { get; internal set; }
        /// <summary>
        /// 
        /// </summary>
        public ITextureData Buffer => _Owner;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="blockSize"></param>
        /// <param name="blockOffset"></param>
        /// <param name="name"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public VulkanDynamicBufferBlock(VulkanDynamicBufferData owner, uint blockSize, uint blockOffset, string name)
        {
            if(string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));
            _Owner = owner ?? throw new ArgumentNullException(nameof(owner));
            BlockSize = blockSize;
            BlockOffset = blockOffset;
            Name = name;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newObj"></param>
        /// <exception cref="NotImplementedException"></exception>
        public void CloneToNewRender(IRenderObject newObj)
        {
            var newBlock = _Owner.MallocBlock();
            newObj.AddComponent(newBlock);
        }
        /// <summary>
        /// 
        /// </summary>
        public void Destroy() => _Owner.RecoveryBlock(this);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderObject"></param>
        public void OnAttach(IRenderObject renderObject) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="updateData"></param>
        public void UpdateBlockData(byte[] updateData)
        {
            int offset = (int)BlockOffset;
            Span<byte> span = updateData;
            uint min = Math.Min(BlockSize, (uint)span.Length);
            span = span[..(int)min];
            _Owner.UpdateMemoryData(span, offset);
        }
        /// <summary>
        /// 更新内存数据
        /// </summary>
        /// <param name="updateData"></param>
        public void UpdateBlockData(Span<byte> updateData)
        {
            int offset = (int)BlockOffset;
            _Owner.UpdateMemoryData(updateData, offset);
        }
        /// <summary>
        /// 更新内存数据
        /// </summary>
        /// <typeparam name="TStruct"></typeparam>
        /// <param name="struct"></param>
        public void UpdateBlockData<TStruct>(TStruct @struct) where TStruct : unmanaged
        {
            Span<byte> data = MemoryMarshal.AsBytes(stackalloc TStruct[] { @struct });
            int offset = (int)BlockOffset;
            _Owner.UpdateMemoryData(data, offset);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="updateData"></param>
        /// <param name="offset"></param>
        public void UpdateBlockData(byte[] updateData, uint offset)
        {
            Span<byte> span = updateData;
            UpdateBlockData(span, offset);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="updateData"></param>
        /// <param name="offset"></param>
        /// <exception cref="IndexOutOfRangeException"></exception>
        public void UpdateBlockData(Span<byte> updateData, uint offset)
        {
            uint end = offset + BlockOffset + (uint)updateData.Length;
            if (end > BlockSize) throw new IndexOutOfRangeException("update data has out of memory size");
            _Owner.UpdateMemoryData(updateData, (int)(offset + BlockOffset));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TStruct"></typeparam>
        /// <param name="struct"></param>
        /// <param name="offset"></param>
        public void UpdateBlockData<TStruct>(TStruct @struct, uint offset)
            where TStruct : unmanaged
        {
            Span<byte> data = MemoryMarshal.AsBytes(stackalloc TStruct[] { @struct });
            UpdateBlockData(data, offset);
        }
    }
}
