﻿using Silk.NET.Vulkan;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// Vulkan纹理
    /// </summary>
    public class VulkanTexture : ITexture
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; protected internal set; }
        /// <summary>
        /// 采样器
        /// </summary>
        public VulkanSampler Sampler { get; protected internal set; }
        /// <summary>
        /// 纹理数据
        /// </summary>
        public VulkanTextureData TextureData { get; protected internal set; }
        /// <summary>
        /// 描述符类型
        /// </summary>
        public DescriptorObjectType DescriptorObjectType { get; protected internal set; }
        /// <summary>
        /// 宽度
        /// </summary>
        public uint Width { get; protected internal set; }
        /// <summary>
        /// 长度
        /// </summary>
        public uint Height { get; protected internal set; }
        /// <summary>
        /// 纹理数组数量
        /// </summary>
        public uint ArrayLayers { get; protected internal set; }
        /// <summary>
        /// 描述符集绑定位置
        /// </summary>
        public uint Binding 
        { 
            get => WSets.DestinationBinding; 
            protected internal set 
            { 
                ref WriteDescriptorSet w = ref GetWriteDescriptorSet(); 
                w.DestinationBinding = value;  
            } 
        }
        /// <summary>
        /// 描述符数量
        /// </summary>
        public uint DescriptorCount { get; protected internal set; } = 1;
        /// <summary>
        /// 目标数组下标
        /// </summary>
        public uint ArrayElement { get; protected internal set; } = 0;
        /// <summary>
        /// 描述符集下标
        /// </summary>
        public uint SetIndex { get; protected internal set; } = 0;
        /// <summary>
        /// 描述集
        /// </summary>
        public DescriptorSet DescriptorSet { get; protected internal set; }
        /// <summary>
        /// 描述集
        /// </summary>
        public WriteDescriptorSet WriteDescriptorSet { get { return WSets; } protected internal set { WSets = value; } }
        /// <summary>
        /// 设备
        /// </summary>
        internal VulkanDevice Device { get; set; }
        /// <summary>
        /// 绘制管理器
        /// </summary>
        internal VulkanGraphicsManager GraphicsManager { get; set; }
        /// <summary>
        /// 获取写入描述集
        /// </summary>
        /// <returns></returns>
        internal ref WriteDescriptorSet GetWriteDescriptorSet() => ref WSets;

        private WriteDescriptorSet WSets;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="textureData"></param>
        /// <param name="name"></param>
        /// <param name="setIndex"></param>
        /// <param name="descriptorCount"></param>
        /// <param name="binding"></param>
        /// <param name="arrayElement"></param>
        /// <param name="descriptorType"></param>
        public VulkanTexture(string name, VulkanSampler sampler, VulkanTextureData textureData,
            uint binding = 0, uint descriptorCount = 1, uint arrayElement = 0, 
            uint setIndex = 0, DescriptorType descriptorType = DescriptorType.CombinedImageSampler)
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));
            TextureData = textureData ?? throw new ArgumentNullException(nameof(textureData));
            Sampler = sampler;
            Name = name;
            Height = textureData.Height;
            Width = textureData.Width;
            SetIndex = setIndex;
            DescriptorObjectType = ConvertDescriptorType(descriptorType);
            var writeSet = textureData.WriteDescriptorSet;
            writeSet.DescriptorCount = descriptorCount;
            writeSet.DestinationArrayElement = arrayElement;
            var copy = CopyWriteDescriptor(writeSet, sampler);
            copy.DescriptorType = descriptorType;
            WriteDescriptorSet = copy;
            ArrayLayers = textureData.ArrayLayers;
            Binding = binding;
            DescriptorCount = descriptorCount;
            ArrayElement = arrayElement;
        }

        /// <summary>
        /// 获取纹理描述集
        /// </summary>
        public DescriptorSet GetDescriptorSet() => WriteDescriptorSet.DestinationSet;

        /// <summary>
        /// 更新采样器
        /// </summary>
        /// <param name="sampler"></param>
        /// <returns></returns>
        public virtual bool UpdateSampler(ISampler sampler)
        {
            if (sampler == null) throw new ArgumentNullException(nameof(sampler));
            if (!(sampler is VulkanSampler vulkanSampler)) throw new InvalidCastException($"Target sampler {sampler.Name} is not vulkan sampler");
            ref var wSet = ref GetWriteDescriptorSet();
            var infos = wSet.ImageInfo;
            if (infos != null)
            {
                for (int i = 0; i < infos.Length; i++)
                    infos[i].Sampler = vulkanSampler.RawHandle;
            }
            UpdateDescriptorSets();
            return true;
        }
        /// <summary>
        /// 更新纹理数据
        /// </summary>
        /// <param name="textureData"></param>
        /// <returns></returns>
        public virtual bool UpdateTextureData(ITextureData textureData)
        {
            if (textureData == null) throw new ArgumentNullException(nameof(textureData));
            if (textureData is not VulkanTextureData vulkanTextureData) throw new InvalidCastException($"Target texture data {textureData.Name} is not vulkan texture data");
            TextureData = vulkanTextureData;
            DescriptorSet set = GetDescriptorSet();
            var wSet = vulkanTextureData.WriteDescriptorSet;
            wSet.DestinationBinding = Binding;
            wSet.DescriptorCount = DescriptorCount;
            wSet.DestinationArrayElement = ArrayElement;
            var copy = CopyWriteDescriptor(wSet, Sampler);
            copy.DestinationSet = set;
            WriteDescriptorSet = copy;
            UpdateDescriptorSets();
            return true;
        }
        /// <summary>
        /// 更新纹理内存数据
        /// </summary>
        /// <param name="updateData"></param>
        public virtual void UpdateTextureMemoryData(byte[] updateData)
        {
            if (TextureData == null) throw new NullReferenceException("No TextureData");
            TextureData.UpdateMemoryData(updateData);
            UpdateDescriptorSets();
        }
        /// <summary>
        /// 更新纹理内存数据
        /// </summary>
        /// <param name="updateData"></param>
        /// <param name="startIndex"></param>
        public virtual void UpdateTextureMemoryData(byte[] updateData, int startIndex)
        {
            if (TextureData == null) throw new NullReferenceException("No TextureData");
            TextureData.UpdateMemoryData(updateData, startIndex);
            UpdateDescriptorSets();
        }
        /// <summary>
        /// 更新纹理内存数据
        /// </summary>
        /// <param name="updateData"></param>
        public virtual void UpdateTextureMemoryData(Span<byte> updateData)
        {
            if (TextureData == null) throw new NullReferenceException("No TextureData");
            TextureData.UpdateMemoryData(updateData);
            UpdateDescriptorSets();
        }
        /// <summary>
        /// 更新纹理内存数据
        /// </summary>
        /// <param name="updateData"></param>
        /// <param name="startIndex"></param>
        public virtual void UpdateTextureMemoryData(Span<byte> updateData, int startIndex)
        {
            if (TextureData == null) throw new NullReferenceException("No TextureData");
            TextureData.UpdateMemoryData(updateData, startIndex);
            UpdateDescriptorSets();
        }
        /// <summary>
        /// 更新纹理内存数据
        /// </summary>
        /// <typeparam name="TStruct"></typeparam>
        /// <param name="struct"></param>
        public virtual void UpdateTextureMemoryData<TStruct>(TStruct @struct)
            where TStruct : unmanaged
        {
            var size = Marshal.SizeOf<TStruct>();
            byte[] data = ArrayPool<byte>.Shared.Rent(size);
            Span<byte> local = data;
            local = local.Slice(0, size);
            try
            {
                MemoryMarshal.Write(local, ref @struct);
                UpdateTextureMemoryData(local);
            }
            finally
            {
                ArrayPool<byte>.Shared.Return(data);
            }
        }
        /// <summary>
        /// 更新纹理内存数据
        /// </summary>
        /// <typeparam name="TStruct"></typeparam>
        /// <param name="struct"></param>
        /// <param name="startIndex"></param>
        public virtual void UpdateTextureMemoryData<TStruct>(TStruct @struct, int startIndex)
            where TStruct : unmanaged
        {
            var size = Marshal.SizeOf<TStruct>();
            byte[] data = ArrayPool<byte>.Shared.Rent(size);
            Span<byte> local = data;
            local = local[..size];
            try
            {
                MemoryMarshal.Write(local, ref @struct);
                UpdateTextureMemoryData(local, startIndex);
            }
            finally
            {
                ArrayPool<byte>.Shared.Return(data);
            }
        }
        /// <summary>
        /// 获取纹理内存数据
        /// </summary>
        /// <returns></returns>
        public virtual Span<byte> GetTextureMemoryData()
        {
            if (TextureData == null) throw new NullReferenceException("No TextureData");
            Span<byte> r = TextureData.GetMemoryData();
            return r;
        }
        /// <summary>
        /// 绑定描述集
        /// </summary>
        /// <param name="descriptorSet"></param>
        public virtual VulkanTexture BindDescriptorSet(DescriptorSet descriptorSet)
        {
            ref var wSets = ref GetWriteDescriptorSet();
            wSets.DestinationSet = descriptorSet;
            DescriptorSet = descriptorSet;
            return this;
        }
        /// <summary>
        /// 
        /// </summary>
        public void UpdateDescriptorSets()
        {
            var set = GetDescriptorSet();
            if (set.Handle == 0) return;
            WriteDescriptorSet[] options = ArrayPool<WriteDescriptorSet>.Shared.Rent(1);
            try
            {
                Span<WriteDescriptorSet> span = options;
                options[0] = WriteDescriptorSet;
                Device.UpdateDescriptorSets(span[..1], null);
            }
            finally
            {
                ArrayPool<WriteDescriptorSet>.Shared.Return(options);
            }
        }
        /// <summary>
        /// 拷贝至新的渲染渲染对象
        /// </summary>
        /// <param name="newObj"></param>
        public virtual void CloneToNewRender(IRenderObject newObj)
        {
            var newTex = DeepCopy();
            newObj.AddComponent(newTex);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderObject"></param>
        public virtual void OnAttach(IRenderObject renderObject) 
        { 
            if(renderObject.TryGetComponent("RenderGroup", out IRenderGroup g))
            {
                VulkanPipeline pipeline = null;
                if(g is PrimaryVulkanRenderGroup primary)
                {
                    pipeline = primary.Pipeline;
                }
                else if(g is SecondaryVulkanRenderGroup secondary)
                {
                    pipeline = secondary.Pipeline;
                }
                if(pipeline != null)
                {
                    var set = pipeline.CreateDescriptorSet(SetIndex);
                    BindDescriptorSet(set);
                }
            }
        }
        /// <summary>
        /// 深度拷贝
        /// </summary>
        /// <returns></returns>
        public ITexture DeepCopy()
        {
            if (GraphicsManager == null) throw new NullReferenceException("Cannot found GraphicsManager");
            var newTex = GraphicsManager.CreateTexture($"{Name}-{Constant.DEEPCOPY}-{GraphicsManager.GetNewId()},",
                TextureData.Copy(GraphicsManager), Sampler, Binding, ArrayElement, DescriptorCount, SetIndex, WSets.DescriptorType);
            return newTex;
        }
        /// <summary>
        /// 摧毁渲染组件
        /// </summary>
        public virtual void Destroy()
        {
            Sampler = null;
            TextureData = null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        protected DescriptorObjectType ConvertDescriptorType(DescriptorType type)
        {
            return type switch
            {
                DescriptorType.CombinedImageSampler => DescriptorObjectType.CombinedImageSampler,
                DescriptorType.StorageImage => DescriptorObjectType.StorageImage,
                DescriptorType.SampledImage => DescriptorObjectType.SampledImage,
                DescriptorType.InputAttachment => DescriptorObjectType.InputAttachment,
                DescriptorType.StorageBuffer => DescriptorObjectType.StorageBuffer,
                DescriptorType.StorageBufferDynamic => DescriptorObjectType.StorageBufferDynamic,
                DescriptorType.StorageTexelBuffer => DescriptorObjectType.StorageTexelBuffer,
                DescriptorType.UniformBuffer => DescriptorObjectType.UniformBuffer,
                DescriptorType.UniformBufferDynamic => DescriptorObjectType.UniformBufferDynamic,
                DescriptorType.UniformTexelBuffer => DescriptorObjectType.UniformTexelBuffer,
                _ => DescriptorObjectType.None
            };
        }

        /// <summary>
        /// 深拷贝写入描述符集
        /// </summary>
        /// <param name="source"></param>
        /// <param name="sampler"></param>
        /// <returns></returns>
        private WriteDescriptorSet CopyWriteDescriptor(WriteDescriptorSet source, VulkanSampler sampler)
        {
            var imageInfos = source.ImageInfo;
            var bufferInfos = source.BufferInfo;
            var bufferView = source.TexelBufferView;
            var result = new WriteDescriptorSet()
            {
                DestinationArrayElement = source.DestinationArrayElement,
                DescriptorCount = source.DescriptorCount,
                DescriptorType = source.DescriptorType,
                DestinationBinding = source.DestinationBinding,
                DestinationSet = default
            };
            if (imageInfos != null)
            {
                var copyImage = new DescriptorImageInfo[imageInfos.Length];
                for (int i = 0; i < imageInfos.Length; i++)
                {
                    copyImage[i] = new DescriptorImageInfo
                    {
                        ImageLayout = imageInfos[i].ImageLayout,
                        ImageView = imageInfos[i].ImageView
                    };
                    if (sampler != null) copyImage[i].Sampler = sampler.RawHandle;
                }
                result.ImageInfo = copyImage;
            }
            if (bufferInfos != null)
            {
                var copyBuffer = new DescriptorBufferInfo[bufferInfos.Length];
                for (int i = 0; i < bufferInfos.Length; i++)
                {
                    copyBuffer[i] = new DescriptorBufferInfo
                    {
                        Buffer = bufferInfos[i].Buffer,
                        Offset = bufferInfos[i].Offset,
                        Range = bufferInfos[i].Range
                    };
                }
                result.BufferInfo = copyBuffer;
            }
            result.TexelBufferView = bufferView;
            return result;
        }

    }
}
