﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class VulkanDynamicBufferAlloter
    {
        /// <summary>
        /// 
        /// </summary>
        protected VulkanGraphicsManager Manager;
        /// <summary>
        /// 
        /// </summary>
        protected VulkanPipeline Pipeline;
        /// <summary>
        /// 
        /// </summary>
        protected uint Binding;
        /// <summary>
        /// 
        /// </summary>
        protected uint DescriptorCount;
        /// <summary>
        /// 
        /// </summary>
        protected uint ArrayElement;
        /// <summary>
        /// 
        /// </summary>
        protected uint SetIndex = 0;
        /// <summary>
        /// 
        /// </summary>
        protected DescriptorType DescriptorType;
        /// <summary>
        /// 
        /// </summary>
        protected uint BlockSize;
        /// <summary>
        /// 块容量
        /// </summary>
        protected uint BlockCapacity;
        /// <summary>
        /// 
        /// </summary>
        protected List<VulkanDynamicBuffer> Buffers = new();
        /// <summary>
        /// 
        /// </summary>
        protected readonly object SyncRoot = new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="graphicsManager"></param>
        /// <param name="pipeline"></param>
        /// <param name="binding"></param>
        /// <param name="descriptorCount"></param>
        /// <param name="blockSize">块大小</param>
        /// <param name="blockCapacity">块容量</param>
        /// <param name="arrayElement"></param>
        /// <param name="setIndex"></param>
        /// <param name="descriptorType"></param>
        /// <exception cref="InvalidOperationException"></exception>
        public VulkanDynamicBufferAlloter(VulkanGraphicsManager graphicsManager, VulkanPipeline pipeline, uint blockSize, uint blockCapacity,
            uint binding = 0, uint descriptorCount = 1, uint arrayElement = 0,
            uint setIndex = 0, DescriptorType descriptorType = DescriptorType.UniformBufferDynamic)
        {
            if (!(descriptorType == DescriptorType.UniformBufferDynamic || descriptorType == DescriptorType.StorageBufferDynamic))
                throw new InvalidOperationException("Create dynamic buffer must be use UniformBufferDynamic or StorageBufferDynamic flag");
            var ldevice = graphicsManager.VulkanDevice;
            ulong align = 1;
            if (descriptorType == DescriptorType.UniformBufferDynamic)
                align = ldevice.PhysicalDevice.GetDeviceProperties().Limits.MinUniformBufferOffsetAlignment;
            else align = ldevice.PhysicalDevice.GetDeviceProperties().Limits.MinStorageBufferOffsetAlignment;
            uint s = blockSize / (uint)align;
            blockSize = (s + 1) * (uint)align;
            Manager = graphicsManager ?? throw new ArgumentNullException(nameof(graphicsManager));
            Pipeline = pipeline ?? throw new ArgumentNullException(nameof(pipeline));
            Binding = binding;
            DescriptorCount = descriptorCount;
            ArrayElement = arrayElement;
            SetIndex = setIndex;
            DescriptorType = descriptorType;
            BlockSize = blockSize;
            BlockCapacity = blockCapacity;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public ITextureBlock MallocBlock()
        {
            uint size = BlockSize;
            VulkanDynamicBuffer buffer;
            if (Buffers.Count <= 0)
            {
                lock (SyncRoot)
                {
                    if (Buffers.Count > 0) buffer = Buffers[0];
                    else
                    {
                        buffer = CreateNewBuffer();
                        Buffers.Add(buffer);
                    }
                    return buffer.MallocBlock();
                }
            }
            lock (SyncRoot)
            {
                int count = Buffers.Count;
                for (int i = 0; i < count; i++)
                {
                    var local = Buffers[i];
                    if(local.TryMallocBlock(out ITextureBlock block))
                    {
                        return block;
                    }
                }
                buffer = CreateNewBuffer();
                Buffers.Add(buffer);
                return buffer.MallocBlock();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private VulkanDynamicBuffer CreateNewBuffer()
        {
            VulkanDynamicBuffer buffer;
            if (DescriptorType == DescriptorType.UniformBufferDynamic)
            {
                buffer = Manager.CreateDynamicUniformBufferTexture(Manager.NewStringToken(), BlockSize, BlockCapacity,
                    Binding, ArrayElement, DescriptorCount, SetIndex);
            }
            else
            {
                buffer = Manager.CreateDynamicStorageBufferTexture(Manager.NewStringToken(), BlockSize, BlockCapacity,
                    Binding, ArrayElement, DescriptorCount, SetIndex);
            }
            buffer.BindDescriptorSet(Pipeline.CreateDescriptorSet(SetIndex));
            buffer.UpdateDescriptorSets();
            return buffer;
        }
    }
}
