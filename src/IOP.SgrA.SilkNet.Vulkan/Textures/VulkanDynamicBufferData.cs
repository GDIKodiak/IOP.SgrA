﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class VulkanDynamicBufferData : VulkanBufferTextureData
    {
        private readonly object SyncRoot = new object();
        private readonly LinkedList<DynamicBlockRecord> Recovery = new();

        /// <summary>
        /// 数据对齐要求
        /// </summary>
        public uint Alignment { get; internal set; }

        /// <summary>
        /// 块大小
        /// </summary>
        public uint BlockSize {  get; internal set; }
        /// <summary>
        /// 块容量
        /// </summary>
        public uint BlockCapacity {  get; internal set; }

        /// <summary>
        /// 申请块
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public VulkanDynamicBufferBlock MallocBlock()
        {
            if (TryMallocBlock(out VulkanDynamicBufferBlock block)) return block;
            else throw new IndexOutOfRangeException("Buffer's memory has not enough to alloc");
        }
        /// <summary>
        /// 尝试申请纹理块
        /// </summary>
        /// <param name="size"></param>
        /// <param name="block"></param>
        /// <returns></returns>
        public bool TryMallocBlock(out VulkanDynamicBufferBlock block)
        {
            ulong offset = 0;
            uint size = BlockSize;
            if (Recovery.Count > 0)
            {
                lock (SyncRoot)
                {
                    var curr = Recovery.First;
                    do
                    {
                        var v = curr.Value;
                        offset = v.Offset;
                        if (v.Size < size)
                        {
                            curr = curr.Next;
                            continue;
                        }
                        else
                        {
                            ulong diff = v.Size - size;
                            offset += size;
                            block = new(this, size, (uint)offset, Name);
                            curr.Value = new DynamicBlockRecord(offset, diff, null);
                            if (diff == 0) Recovery.Remove(curr);
                            return true;
                        }
                    }
                    while (curr != null);
                }
            }
            lock (SyncRoot)
            {
                if (offset >= BytesSize || (BytesSize - offset) < size) { block = null; return false; }
                block = new VulkanDynamicBufferBlock(this, size, (uint)offset, Name);
                ulong diff = BytesSize - offset - size;
                Recovery.AddLast(new DynamicBlockRecord(size, diff, null));
                return true;
            }
        }

        /// <summary>
        /// 回收块
        /// </summary>
        /// <param name="block"></param>
        public void RecoveryBlock(VulkanDynamicBufferBlock block)
        {
            if (block.Buffer != this) return;
            lock (SyncRoot)
            {
                ulong offset = block.BlockOffset;
                ulong size = block.BlockSize;
                if (Recovery.Count == 0)
                {
                    DynamicBlockRecord record = new DynamicBlockRecord(offset, size, null);
                    Recovery.AddLast(record);
                }
                var curr = Recovery.First;
                do
                {
                    var record = curr.Value;
                    var end = record.Offset + record.Size;
                    if(end >= block.BlockOffset)
                    {
                        curr.Value = new DynamicBlockRecord(record.Offset, record.Size + block.BlockSize - (end - block.BlockOffset), null);
                        break;
                    }
                    else
                    {
                        var next = curr.Next;
                        if(next != null)
                        {
                            var nextValue = next.Value;
                            var blockEnd = block.BlockOffset + block.BlockSize;
                            if(blockEnd >= nextValue.Offset && blockEnd <= nextValue.Offset + nextValue.Size)
                            {
                                next.Value = new DynamicBlockRecord(block.BlockOffset, block.BlockSize + nextValue.Size - (blockEnd - nextValue.Offset), null);
                                break;
                            }
                            else if(blockEnd > nextValue.Offset + nextValue.Size)
                            {
                                curr = curr.Next;
                                continue;
                            }
                            else
                            {
                                Recovery.AddAfter(curr, new DynamicBlockRecord(block.BlockOffset, block.BlockSize, null));
                                break;
                            }
                        }
                        else
                        {
                            Recovery.AddAfter(curr, new DynamicBlockRecord(block.BlockOffset, block.BlockSize, null));
                            break;
                        }
                    }
                } 
                while (curr != null);
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public struct DynamicBlockRecord
    {
        /// <summary>
        /// 
        /// </summary>
        public ulong Offset;
        /// <summary>
        /// 
        /// </summary>
        public ulong Size;
        /// <summary>
        /// 
        /// </summary>
        public VulkanDynamicBufferBlock Block;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="size"></param>
        /// <param name="block"></param>
        public DynamicBlockRecord(ulong offset, ulong size, VulkanDynamicBufferBlock block)
        {
            Offset = offset;
            Size = size;
            Block = block;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return (int)(Offset << 16 | Size);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals([NotNullWhen(true)] object obj)
        {
            if (obj is not DynamicBlockRecord record) return false;
            return Offset == record.Offset && Size == record.Size;
        }
    }
}
