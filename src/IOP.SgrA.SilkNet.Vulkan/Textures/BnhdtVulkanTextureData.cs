﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// Bnhdt Vulkan纹理数据
    /// </summary>
    public class BnhdtVulkanTextureData : VulkanImageTextureData
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public override async Task Load(Stream stream)
        {
            byte[] data = ArrayPool<byte>.Shared.Rent(4);
            Memory<byte> local = new Memory<byte>(data).Slice(0, 4);
            await stream.ReadAsync(local);
            uint width = MemoryMarshal.Read<uint>(local.Span);
            Width = width;
            await stream.ReadAsync(local);
            uint height = MemoryMarshal.Read<uint>(local.Span);
            Height = height;
            await base.Load(stream);
            ArrayPool<byte>.Shared.Return(data);
        }
        /// <summary>
        /// 加载
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public override async Task Load(byte[] data)
        {
            Memory<byte> local = data; int index = 0;
            uint width = MemoryMarshal.Read<uint>(local.Span[index..]);
            Width = width; index += sizeof(uint);
            uint height = MemoryMarshal.Read<uint>(local.Span[index..]);
            Height = height; index += sizeof(uint);
            Memory<byte> t = local[index..];
            await base.Load(t.ToArray());
        }
    }
}
