﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class BntexaVulkanTextureData : VulkanImageTextureData
    {
        /// <summary>
        /// 
        /// </summary>
        public BntexaHeader Header { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public async override Task Load(Stream stream)
        {
            int size = Marshal.SizeOf<Bn3dtexHeader>();
            BntexaHeader header;
            byte[] data = ArrayPool<byte>.Shared.Rent(size);
            Memory<byte> local = new Memory<byte>(data).Slice(0, size);
            await stream.ReadAsync(local);
            header = MemoryMarshal.Read<BntexaHeader>(local.Span);
            Header = header;
            BytesSize = header.Width * header.Height * header.Length * 4;
            Width = header.Width;
            Height = header.Height;
            await base.Load(stream);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async override Task Load(byte[] data)
        {
            int size = Marshal.SizeOf<Bn3dtexHeader>();
            BntexaHeader header;
            Memory<byte> d = data; int index = 0;
            Memory<byte> local = d.Slice(0, size);
            header = MemoryMarshal.Read<BntexaHeader>(local.Span); index += size;
            Header = header;
            BytesSize = header.Width * header.Height * header.Length * 4;
            Width = header.Width;
            Height = header.Height;
            await base.Load(d[index..].ToArray());
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public struct BntexaHeader
    {
        /// <summary>
        /// 
        /// </summary>
        public uint Width;
        /// <summary>
        /// 
        /// </summary>
        public uint Height;
        /// <summary>
        /// 
        /// </summary>
        public uint Length;
    }
}
