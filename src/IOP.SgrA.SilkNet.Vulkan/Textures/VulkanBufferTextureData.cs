﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 纹理缓冲数据
    /// </summary>
    public class VulkanBufferTextureData : VulkanTextureData
    {
        /// <summary>
        /// 缓冲信息
        /// </summary>
        public VulkanBufferInfo BufferInfo { get; internal protected set; }
        /// <summary>
        /// 获取缓冲
        /// </summary>
        /// <returns></returns>
        public VulkanBuffer GetBuffer() => BufferInfo.Buffer;
        /// <summary>
        /// 获取设备内存
        /// </summary>
        /// <returns></returns>
        public override VulkanDeviceMemory GetDeviceMemory() => BufferInfo.DeviceMemory;
        /// <summary>
        /// 更新缓冲内存数据
        /// </summary>
        /// <param name="updateData"></param>
        public override void UpdateMemoryData(byte[] updateData)
        {
            Span<byte> buffer = new(updateData);
            UpdateMemoryData(buffer);
        }
        /// <summary>
        /// 更新设备内存数据
        /// </summary>
        /// <param name="updateData"></param>
        /// <param name="startIndex"></param>
        public override void UpdateMemoryData(byte[] updateData, int startIndex)
        {
            Span<byte> buffer = new(updateData);
            UpdateMemoryData(buffer, startIndex);
        }
        /// <summary>
        /// 更新设备内存数据
        /// </summary>
        /// <param name="updateData"></param>
        public override void UpdateMemoryData(Span<byte> updateData)
        {
            if (BufferInfo.MemoryType.HasFlag(MemoryPropertyFlags.DeviceLocalBit))
                throw new InvalidOperationException("Only Host memory could update memory data");
            ulong size = BufferInfo.Size;
            ulong start = BufferInfo.Offset;
            ulong uD = (ulong)updateData.Length;
            if (size <= 0) return;
            if (uD > size) throw new IndexOutOfRangeException("Update data is too bigger than target memory");
            VulkanBuffer buffer = BufferInfo.Buffer ?? throw new NullReferenceException("Target texture data could not have buffer");
            buffer.UpdateMemory(start, updateData);
        }
        /// <summary>
        /// 更新设备内存数据
        /// </summary>
        /// <param name="updateData"></param>
        /// <param name="startIndex"></param>
        public override void UpdateMemoryData(Span<byte> updateData, int startIndex)
        {
            if (BufferInfo.MemoryType.HasFlag(MemoryPropertyFlags.DeviceLocalBit))
                throw new InvalidOperationException("Only Host memory could update memory data");
            ulong size = BufferInfo.Size;
            ulong start = BufferInfo.Offset + (ulong)startIndex;
            ulong uD = (ulong)updateData.Length;
            if (uD > size - (ulong)startIndex) throw new IndexOutOfRangeException("Update data is too bigger than target memory");
            VulkanBuffer buffer = BufferInfo.Buffer; if (buffer == null) throw new NullReferenceException("Target texture data could not have buffer");
            buffer.UpdateMemory(start, updateData);
        }
        /// <summary>
        /// 获取内存数据
        /// </summary>
        /// <returns></returns>
        public override Span<byte> GetMemoryData()
        {
            if (BufferInfo.MemoryType.HasFlag(MemoryPropertyFlags.DeviceLocalBit))
                throw new InvalidOperationException("Only Host memory could get memory data");
            VulkanDeviceMemory memory = GetDeviceMemory(); if (memory == null) throw new NullReferenceException("Target texture data could not have device memory");
            ulong size = BufferInfo.Size;
            ulong start = BufferInfo.Offset;
            byte[] data = new byte[size];
            memory.CopyTo(data, start, size);
            return new Span<byte>(data);
        }
        /// <summary>
        /// 针对缓冲类型的纹理使用深拷贝
        /// </summary>
        /// <param name="manager"></param>
        /// <returns></returns>
        public override VulkanTextureData Copy(VulkanGraphicsManager manager)
        {
            var newName = string.IsNullOrEmpty(Name) ? Guid.NewGuid().ToString("N") : Name;
            newName = $"{newName}-{Constant.DEEPCOPY}-{manager.GetNewId()}";
            VulkanBufferTextureData newBuffer;
            if (BufferInfo.MemoryType.HasFlag(MemoryPropertyFlags.DeviceLocalBit))
                newBuffer = manager.CreateLocalBufferData(newName, BufferInfo.Size, BufferInfo.BufferUsageFlags, BufferInfo.WriteDescriptorSet.DescriptorType);
            else
                newBuffer = manager.CreateBufferData(newName, BufferInfo.Size, BufferInfo.BufferUsageFlags, BufferInfo.WriteDescriptorSet.DescriptorType);
            return newBuffer;
        }
        /// <summary>
        /// 当渲染对象被摧毁时
        /// </summary>
        /// <param name="device"></param>
        public override void OnRenderObjectDestroyed(VulkanDevice device)
        {
            if(BufferInfo.Buffer != null)
            {
                device.DestroyBuffer(BufferInfo.Buffer);
                Dispose();
            }
        }

        /// <summary>
        /// 销毁资源
        /// </summary>
        public override void Dispose()
        {
            BufferInfo.Buffer?.Dispose();
            BufferInfo = default;
            base.Dispose();
        }
    }
}
