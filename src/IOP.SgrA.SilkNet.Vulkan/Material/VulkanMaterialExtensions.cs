﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public static class VulkanMaterialExtensions
    {
        /// <summary>
        /// 创建简单PBR材质
        /// </summary>
        /// <param name="graphicsManager"></param>
        /// <param name="albedo"></param>
        /// <param name="metallic"></param>
        /// <param name="roughness"></param>
        /// <param name="setIndex"></param>
        /// <param name="binding"></param>
        /// <param name="render"></param>
        /// <returns></returns>
        public static SimplePBRMaterial CreateSimplePBRMaterial(this IGraphicsManager graphicsManager, IRenderObject render,
            Vector3 albedo, float metallic, float roughness, uint setIndex, uint binding)
        {
            var tex = graphicsManager.CreateUniformBuffer(32, binding, 0, 1, setIndex) as VulkanTexture;
            SimplePBRMaterial material = new(graphicsManager.NewStringToken(), tex)
            {
                Albedo = albedo,
                Metallic = metallic,
                Roughness = roughness
            };
            material.UpdateToBuffer();
            render.AddComponent(material);
            render.AddComponent(tex);
            return material;
        }
    }
}
