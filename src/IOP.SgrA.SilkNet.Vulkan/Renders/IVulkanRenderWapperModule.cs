﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 渲染包装类模块
    /// </summary>
    public interface IVulkanRenderWapperModule
    {
        /// <summary>
        /// 包装
        /// </summary>
        VulkanRenderWapper RenderWapper { get; set; }
    }
}
