﻿using IOP.SgrA.ECS;
using Microsoft.Extensions.Logging;
using Silk.NET.Vulkan;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Semaphore = Silk.NET.Vulkan.Semaphore;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// Vulkan交换链渲染调度器
    /// </summary>
    public class VulkanSwapchainRenderDispatcher : IRenderDispatcher
    {
        /// <summary>
        /// 管理器
        /// </summary>
        public VulkanGraphicsManager Manager { get; set; }
        /// <summary>
        /// 输入
        /// </summary>
        public IInputStateController Input { get; set; }
        /// <summary>
        /// 上下文管理器
        /// </summary>
        public IContextManager ContextManager { get; set; }
        /// <summary>
        /// 事件总线
        /// </summary>
        public SystemEventBus EventBus { get; set; }
        /// <summary>
        /// 图片到达信号量
        /// </summary>
        public Semaphore ImageAvailableSemaphore { get; set; }
        /// <summary>
        /// 渲染结束信号量
        /// </summary>
        public Semaphore RenderFinishSemaphore { get; set; }
        /// <summary>
        /// 交换链
        /// </summary>
        public VulkanSwapchain Swapchain { get; set; }
        /// <summary>
        /// 渲染通道
        /// </summary>
        public VulkanRenderPass SwapchainRenderPass { get; set; }
        /// <summary>
        /// 呈现队列
        /// </summary>
        public WorkQueue PresentQueue { get; set; }
        /// <summary>
        /// 日志
        /// </summary>
        public readonly ILogger<VulkanSwapchainRenderDispatcher> Logger;
        /// <summary>
        /// 待切换组件
        /// </summary>
        protected readonly ConcurrentQueue<IRenderDispatcherComponent> SwitchComponents = new ConcurrentQueue<IRenderDispatcherComponent>();
        /// <summary>
        /// 
        /// </summary>
        protected readonly object SyncRoot = new object();
        /// <summary>
        /// 渲染组任务执行委托
        /// </summary>
        private Action RenderGroupTaskAction = new Action(() => { });
        private const int MaxWaring = 50;
        private volatile int IsStop = 0;
        private IRenderDispatcherComponent RunningComponent = null;
        private TimeSpan LastTimeStamp;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="logger"></param>
        public VulkanSwapchainRenderDispatcher(ILogger<VulkanSwapchainRenderDispatcher> logger)
        {
            Logger = logger;
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public void Initialization()
        {
            ContextManager.RefreshUpdateQueue();
            RenderGroupTaskAction = DefaultRenderGroupTask;
        }
        /// <summary>
        /// 获取事件总线
        /// </summary>
        /// <returns></returns>
        public ISystemEventBus GetSystemEventBus() => EventBus;
        /// <summary>
        /// 获取当前场景
        /// </summary>
        /// <returns></returns>
        public Scene GetCurrentScene() => RunningComponent as Scene;
        /// <summary>
        /// 发送时间戳
        /// </summary>
        /// <param name="timeSpan"></param>
        /// <exception cref="NotImplementedException"></exception>
        public void SendTimeStamp(TimeSpan timeSpan) => LastTimeStamp = timeSpan;

        /// <summary>
        /// 渲染
        /// </summary>
        public void Rendering() => RenderGroupTaskAction();

        /// <summary>
        /// 创建并推送组件
        /// </summary>
        /// <param name="component"></param>
        /// <returns></returns>
        public void PushDispatcherComponent(IRenderDispatcherComponent component)
        {
            if (RunningComponent == null)
            {
                SwitchComponents.Enqueue(component);
                lock (SyncRoot) { GetComponent(); }
            }
            else SwitchComponents.Enqueue(component);
        }

        /// <summary>
        /// 开始渲染
        /// </summary>
        public void Start() => Interlocked.Exchange(ref IsStop, 0);
        /// <summary>
        /// 停止渲染
        /// </summary>
        public void Stop() => Interlocked.Exchange(ref IsStop, 1);

        /// <summary>
        /// 重建渲染调度器
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public void RecreateRenderDispatcher(uint width, uint height)
        {
            VulkanDevice lDevice = Manager.VulkanDevice;
            Stop();
            lDevice.WaitIdle();
            VulkanSwapchain oldSwapChain = Swapchain;
            Extent2D extent = new Extent2D(width, height);
            var detail = oldSwapChain.Details;
            var indices = oldSwapChain.QueueFamilyIndex;
            if (!indices.Any()) throw new NullReferenceException("No queue indices could to recreate swapChain");
            var familes = indices.Select(x => x.FamilyIndex).ToArray();
            uint imageCount = detail.Capabilities.MinImageCount + 1;
            if (detail.Capabilities.MaxImageCount > 0 && imageCount > detail.Capabilities.MaxImageCount)
                imageCount = detail.Capabilities.MaxImageCount;
            unsafe
            {
                fixed (uint* p = familes)
                {
                    SwapchainCreateInfoKHR infoKHR = new SwapchainCreateInfoKHR();
                    infoKHR.SType = StructureType.SwapchainCreateInfoKhr;
                    infoKHR.Surface = oldSwapChain.Surface;
                    infoKHR.PresentMode = oldSwapChain.SwapChainPresentMode;
                    infoKHR.CompositeAlpha = CompositeAlphaFlagsKHR.OpaqueBitKhr;
                    infoKHR.PQueueFamilyIndices = p;
                    infoKHR.QueueFamilyIndexCount = 1;
                    infoKHR.ImageSharingMode = oldSwapChain.SharingMode;
                    infoKHR.ImageUsage = ImageUsageFlags.ColorAttachmentBit;
                    infoKHR.ImageExtent = extent;
                    infoKHR.ImageColorSpace = oldSwapChain.SwapChainFormat.ColorSpace;
                    infoKHR.ImageFormat = oldSwapChain.SwapChainFormat.Format;
                    infoKHR.MinImageCount = imageCount;
                    infoKHR.ImageArrayLayers = 1;
                    infoKHR.PreTransform = detail.Capabilities.CurrentTransform;
                    infoKHR.Clipped = true;
                    infoKHR.OldSwapchain = oldSwapChain.RawHandle;
                    var r = lDevice.SwapchainExtension.CreateSwapchain(lDevice.RawHandle, in infoKHR, null, out SwapchainKHR swapchain);
                    if (r != Result.Success) throw new VulkanOperationException("CreateSwapchain", r);
                    VulkanSwapchain vulkanSwapchain = new VulkanSwapchain(lDevice.NativeAPI, swapchain, lDevice.RawHandle, lDevice.Instance,
                        oldSwapChain.Surface);
                    vulkanSwapchain.Details = detail;
                    vulkanSwapchain.SwapChainExtent = extent;
                    vulkanSwapchain.SwapChainFormat = oldSwapChain.SwapChainFormat;
                    vulkanSwapchain.SwapChainPresentMode = oldSwapChain.SwapChainPresentMode;
                    vulkanSwapchain.SharingMode = infoKHR.ImageSharingMode;
                    vulkanSwapchain.QueueFamilyIndex = indices;
                    var config = oldSwapChain.Views.First().CreateOption;
                    vulkanSwapchain.CreateSwapchainImageViews((images) =>
                    {
                        List<VulkanImageView> views = new List<VulkanImageView>();
                        foreach (var item in images)
                        {
                            var v = item.CreateImageView(config);
                            views.Add(v);
                        }
                        return views;
                    });
                    var pass = SwapchainRenderPass;
                    Swapchain = vulkanSwapchain;
                    pass.RecreateSwapchainFrameBuffer(vulkanSwapchain, width, height);
                    pass.BeginOption.RenderArea = new Rect2D { Extent = new Extent2D(width, height), Offset = new Offset2D { X = 0, Y = 0 } };
                    oldSwapChain.Dispose();
                }
            }
            Start();
        }
        /// <summary>
        /// 摧毁调度器
        /// </summary>
        public void DestroyDispatcher()
        {
            var lDevice = Manager.VulkanDevice;
            lDevice.WaitIdle();
            Swapchain.Dispose();
        }

        /// <summary>
        /// 默认渲染组任务
        /// </summary>
        private void DefaultRenderGroupTask()
        {
            SpinWait wait = new SpinWait();
            int stop = IsStop;
            while (Interlocked.CompareExchange(ref IsStop, stop, stop) != stop)
            {
                wait.SpinOnce();
                stop = IsStop;
            }
            if (stop == 1) return;
            Fence nFence = new Fence(0);
            try
            {
                Span<Semaphore> rSemaphores = stackalloc Semaphore[] { RenderFinishSemaphore };
                var component = GetComponent();
                var queue = PresentQueue.Queue;
                if (component != null && component.IsEnabled)
                {
                    ContextManager.RefreshUpdateQueue();
                    var fIndex = Swapchain.AcquireNextImage(ulong.MaxValue, ImageAvailableSemaphore, nFence);
                    EventBus.UpdateEvent(EventTrigger.SystemFrameBefore);
                    component.ExecuteComponentSystem(LastTimeStamp);
                    EventBus.UpdateEvent(EventTrigger.SystemFrameAfter);
                    component.Rendering(fIndex);
                    Swapchain.Present(PresentQueue.Queue, rSemaphores, fIndex);
                }
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message + "\r\n" + e.StackTrace);
                Stop();
            }
        }

        /// <summary>
        /// 获取组件
        /// </summary>
        /// <returns></returns>
        private IRenderDispatcherComponent GetComponent()
        {
            if(SwitchComponents.TryDequeue(out IRenderDispatcherComponent component))
            {
                RunningComponent = component;
            }
            if (RunningComponent == null) return null;
            return RunningComponent;
        }
    }
}
