﻿using IOP.SgrA.ECS;
using Microsoft.Extensions.Logging;
using Silk.NET.Vulkan;
using System;
using System.Buffers;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 次要渲染组
    /// </summary>
    public class SecondaryVulkanRenderGroup : RenderGroup<VulkanContext, VulkanPipeline, SecondaryVulkanRenderGroup>
    {
        /// <summary>
        /// 渲染组类型
        /// </summary>
        public override GroupType GroupType => GroupType.Secondary;
        /// <summary>
        /// 渲染管理器
        /// </summary>
        public VulkanGraphicsManager GraphicsManager { get; internal set; }
        /// <summary>
        /// 逻辑设备
        /// </summary>
        public VulkanDevice LogicDevice { get; internal set; }
        /// <summary>
        /// 主要命令缓冲
        /// </summary>
        public VulkanCommandBuffer SecondaryCommandBuffer { get; internal set; }
        /// <summary>
        /// 信号量
        /// </summary>
        public List<VulkanSemaphore> Semaphores { get; internal set; } = new List<VulkanSemaphore>();
        /// <summary>
        /// 栅栏列表
        /// </summary>
        public List<VulkanFence> Fences { get; internal set; } = new List<VulkanFence>();
        /// <summary>
        /// 渲染通道
        /// </summary>
        public VulkanRenderPass RenderPass { get; internal set; }
        /// <summary>
        /// 帧缓冲
        /// </summary>
        public VulkanFrameBuffer Framebuffer { get; internal set; }
        /// <summary>
        /// 主渲染组
        /// </summary>
        public PrimaryVulkanRenderGroup Primary { get; internal set; }
        /// <summary>
        /// 
        /// </summary>
        protected internal bool IsBinding = false;
        /// <summary>
        /// 
        /// </summary>
        protected uint Counter = 0;
        /// <summary>
        /// 
        /// </summary>

        protected ConcurrentDictionary<uint, VulkanContext> ContextsPool = new();

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="name"></param>
        /// <param name="serviceProvider"></param>
        /// <param name="shader"></param>
        /// <param name="mode"></param>
        public SecondaryVulkanRenderGroup(string name, IServiceProvider serviceProvider, VulkanPipeline shader, GroupRenderMode mode = GroupRenderMode.Opaque)
            : base(name, serviceProvider, shader, mode)
        {
        }

        /// <summary>
        /// 初始化函数
        /// </summary>
        public override void Initialization()
        {
            InitializationAction(this);
            foreach (var item in Childrens)
            {
                item.Initialization();
            }
        }
        /// <summary>
        /// 是否启用
        /// </summary>
        /// <returns></returns>
        public override bool IsEnable() => IsBinding && !IsStop;
        /// <summary>
        /// 禁用
        /// </summary>
        public override void Disable()
        {
            SecondaryCommandBuffer.Reset();
            base.Disable();
        }
        /// <summary>
        /// 组渲染
        /// </summary>
        public override void GroupRendering()
        {
            GroupProductionLine(this);
            Interlocked.Exchange(ref Counter, 0);
        }
        /// <summary>
        /// 组渲染
        /// </summary>
        /// <param name="frameIndex"></param>
        public override void GroupRendering(uint frameIndex)
        {
            CurrentFrame = 0;
            GroupProductionLine(this);
            Interlocked.Exchange(ref Counter, 0);
        }
        /// <summary>
        /// 设置视口信息
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="minDepth"></param>
        /// <param name="maxDepth"></param>
        public override void SetViewPort(float x, float y, float width, float height, float minDepth, float maxDepth)
        {
            Camera.SetViewport(x, y, width, height, minDepth, maxDepth);
        }
        /// <summary>
        /// 推送数据至纹理
        /// </summary>
        /// <typeparam name="TStruct"></typeparam>
        /// <param name="data"></param>
        /// <param name="setIndex"></param>
        /// <param name="binding"></param>
        public override void PushDataToTexture<TStruct>(TStruct data, uint setIndex, uint binding)
            where TStruct : struct
        {
            if (Pipeline == null) return;
            if (Pipeline.TryGetPipelineTexture(setIndex, binding, out VulkanTexture texture))
            {
                var size = Marshal.SizeOf(data);
                if ((ulong)size > texture.TextureData.BytesSize) return;
                byte[] o = ArrayPool<byte>.Shared.Rent(size);
                Span<byte> d = o;
                MemoryMarshal.Write(d, ref data);
                Span<byte> f = d.Slice(0, size);
                texture.UpdateTextureMemoryData(f);
                ArrayPool<byte>.Shared.Return(o);
            }
        }
        /// <summary>
        /// 推送数据至纹理
        /// </summary>
        /// <param name="data"></param>
        /// <param name="setIndex"></param>
        /// <param name="binding"></param>
        public override void PushDataToTexture(Span<byte> data, uint setIndex, uint binding)
        {
            if (Pipeline == null) return;
            if (Pipeline.TryGetPipelineTexture(setIndex, binding, out VulkanTexture texture))
            {
                if ((ulong)data.Length > texture.TextureData.BytesSize)
                {
                    Logger.LogWarning("Push data to texture failed, input data is too large");
                    return;
                }
                texture.UpdateTextureMemoryData(data);
            }
        }

        /// <summary>
        /// 推送上下文至渲染组
        /// </summary>
        /// <param name="context"></param>
        public override void PushContext(Context context)
        {
            if (context is VulkanContext local)
            {
                local.VulkanPipeline = Pipeline;
                local.LogicDevice = Pipeline.Device;
                local.CommandBuffer = SecondaryCommandBuffer;
                Contexts.Enqueue(local);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderObject"></param>
        public override void PushContext(IRenderObject renderObject)
        {
            VulkanContext context = GetInternalContext(renderObject);
            Contexts.Enqueue(context);
        }
        /// <summary>
        /// 绑定上下文
        /// </summary>
        /// <param name="contexts"></param>
        public override void BindingContext(Context[] contexts)
        {
            foreach (var context in contexts)
            {
                if (context is VulkanContext local)
                {
                    local.VulkanPipeline = Pipeline;
                    local.LogicDevice = Pipeline.Device;
                    local.CommandBuffer = SecondaryCommandBuffer;
                    local.BindRenderGroup(this);
                }
            }
        }
        /// <summary>
        /// 绑定上下文
        /// </summary>
        /// <param name="contexts"></param>
        /// <param name="renderObject"></param>
        /// <param name="deepCopy"></param>
        public override void BindingContext(Context[] contexts, IRenderObject renderObject, bool deepCopy = false)
        {
            int index = 0;
            foreach (var context in contexts)
            {
                if (context is VulkanContext local)
                {
                    local.VulkanPipeline = Pipeline;
                    local.LogicDevice = Pipeline.Device;
                    local.CommandBuffer = SecondaryCommandBuffer;
                    local.BindRenderGroup(this);
                    IRenderObject obj;
                    if (deepCopy) obj = renderObject.DeepCopy(GraphicsManager);
                    else obj = renderObject;
                    local.BindRenderObject(obj);
                    BindDescriptorSetToObject(obj);
                    var constants = Pipeline.CreateConstantBuffers();
                    obj.AddComponents(constants);
                    index++;
                }
            }
        }
        /// <summary>
        /// 解除上下文绑定
        /// </summary>
        /// <param name="context"></param>
        public override void UnbindContext(Context context)
        {
            if (context != null)
            {
                var obj = context.GetContextRenderObject();
                if (obj != null) obj.Destroy(GraphicsManager);
            }
        }
        /// <summary>
        /// 绑定静态渲染对象
        /// </summary>
        /// <param name="renderObject"></param>
        public override void BindingStaticRenderObject(IRenderObject renderObject)
        {
            if (StaticRenderObjects.Contains(renderObject)) return;
            BindDescriptorSetToObject(renderObject);
            StaticRenderObjects.Add(renderObject);
        }

        /// <summary>
        /// 获取子渲染组
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<SecondaryVulkanRenderGroup> GetChildrens()
        {
            foreach (var item in Childrens)
            {
                item.Framebuffer = Framebuffer;
                item.RenderPass = RenderPass;
                yield return item;
            }
        }

        /// <summary>
        /// 销毁资源
        /// </summary>
        public override void Dispose()
        {
            GroupProductionLine = null;
            GroupProductionLineBuilder = null;
            ItemProductionLine = null;
            ItemProductionLineBuilder = null;
            Pipeline = null;
            Contexts.Clear();
            Parent = null;
            Camera = null;
            Fences.Clear();
            Semaphores.Clear();
            LogicDevice = null;
            SecondaryCommandBuffer = null;
            Primary = null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderObject"></param>
        public override void OnAttach(IRenderObject renderObject)
        {
            BindDescriptorSetToObject(renderObject);
        }

        /// <summary>
        /// 绑定描述符集至渲染对象
        /// </summary>
        /// <param name="obj"></param>
        protected void BindDescriptorSetToObject(IRenderObject obj)
        {
            if (Pipeline == null) throw new InvalidOperationException($"Render gourp {Name} is not contain pipeline to rending render object");
            List<WriteDescriptorSet> writes = new List<WriteDescriptorSet>();
            Dictionary<uint, DescriptorSet> books = new Dictionary<uint, DescriptorSet>();
            foreach (var item in obj.GetComponents<VulkanTexture>())
            {
                DescriptorSet sets;
                if (books.ContainsKey(item.SetIndex)) sets = books[item.SetIndex];
                else
                {
                    sets = Pipeline.CreateDescriptorSet(item.SetIndex);
                    books.Add(item.SetIndex, sets);
                }
                item.BindDescriptorSet(sets);
                writes.Add(item.WriteDescriptorSet);
            }
            if (writes.Count > 0) LogicDevice.UpdateDescriptorSets(writes.ToArray(), null);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderObject"></param>
        /// <returns></returns>
        protected VulkanContext GetInternalContext(IRenderObject renderObject)
        {
            uint counter = Counter;
            SpinWait wait = new();
            while (Interlocked.CompareExchange(ref Counter, counter + 1, counter) != counter)
            {
                wait.SpinOnce();
                counter = Counter;
            }
            counter++;
            VulkanContext context;
            if (counter >= ContextsPool.Count + 1)
            {
                context = new VulkanContext();
                ContextsPool.TryAdd(counter, context);
            }
            else if (!ContextsPool.TryGetValue(counter, out VulkanContext c))
            {
                context = new VulkanContext();
                ContextsPool.TryAdd(counter, context);
                return context;
            }
            else context = c;
            context.VulkanPipeline = Pipeline;
            context.LogicDevice = Pipeline.Device;
            context.CommandBuffer = SecondaryCommandBuffer;
            context.BindRenderGroup(this);
            context.BindRenderObject(renderObject);
            context.Entity = new Entity(-1);
            return context;
        }
    }
}
