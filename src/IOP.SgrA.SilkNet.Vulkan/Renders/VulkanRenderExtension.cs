﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public static class VulkanRenderExtension
    {
        /// <summary>
        /// 创建主要渲染组
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="name"></param>
        /// <param name="pipeline"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public static PrimaryVulkanRenderGroup CreatePrimaryRenderGroup(this VulkanGraphicsManager manager, 
            string name, VulkanPipeline pipeline, GroupRenderMode mode = GroupRenderMode.Opaque)
        {
            if (string.IsNullOrEmpty(name)) name = Guid.NewGuid().ToString("N");
            if (manager.IsContainsGroup(name)) throw new Exception($"The productionLine name {name} is already used");
            PrimaryVulkanRenderGroup renderGroup = new PrimaryVulkanRenderGroup(name, manager.ServiceProvider, pipeline, mode);
            renderGroup.GraphicsManager = manager;
            manager.AddRenderGroup(renderGroup);
            return renderGroup;
        }
        /// <summary>
        /// 将Vulkan相关对象绑定至渲染组并使渲染组可用
        /// </summary>
        /// <param name="renderGroup"></param>
        /// <param name="primaryCommandBuffer"></param>
        /// <param name="logicDevice"></param>
        /// <param name="semaphore"></param>
        /// <param name="fences"></param>
        /// <param name="renderPass"></param>
        /// <param name="events"></param>
        /// <returns></returns>
        public static PrimaryVulkanRenderGroup Binding(this PrimaryVulkanRenderGroup renderGroup,
            VulkanCommandBuffer primaryCommandBuffer, VulkanDevice logicDevice, VulkanRenderPass renderPass,
            IEnumerable<Semaphore> semaphore, IEnumerable<Fence> fences, IEnumerable<Event> events = null)
        {
            renderGroup.LogicDevice = logicDevice ?? throw new ArgumentNullException(nameof(logicDevice));
            renderGroup.PrimaryCommandBuffer = primaryCommandBuffer ?? throw new ArgumentNullException(nameof(primaryCommandBuffer));
            renderGroup.RenderPass = renderPass;
            renderGroup.WorkQueues = logicDevice.WorkQueues;
            if (semaphore != null && semaphore.Any()) renderGroup.Semaphores = semaphore.ToArray();
            if (fences != null && fences.Any()) renderGroup.Fences = fences.ToArray();
            if (events != null && events.Any()) renderGroup.Events = events.ToArray();
            renderGroup.IsBinding = true;
            return renderGroup;
        }
        /// <summary>
        /// 绑定至另一个主渲染组
        /// </summary>
        /// <param name="renderGroup"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static PrimaryVulkanRenderGroup Binding(this PrimaryVulkanRenderGroup renderGroup, PrimaryVulkanRenderGroup parent)
        {
            if (parent == null) throw new ArgumentNullException(nameof(parent));
            renderGroup.LogicDevice = parent.LogicDevice;
            renderGroup.PrimaryCommandBuffer = parent.PrimaryCommandBuffer;
            renderGroup.Semaphores = parent.Semaphores;
            renderGroup.Fences = parent.Fences;
            renderGroup.RenderPass = parent.RenderPass;
            parent.AddChildren(renderGroup);
            renderGroup.IsBinding = true;
            return renderGroup;
        }
        /// <summary>
        /// 绑定至另一个主渲染组，使用独立渲染通道
        /// </summary>
        /// <param name="renderGroup"></param>
        /// <param name="parent"></param>
        /// <param name="pass"></param>
        /// <returns></returns>
        public static PrimaryVulkanRenderGroup Binding(this PrimaryVulkanRenderGroup renderGroup, PrimaryVulkanRenderGroup parent, VulkanRenderPass pass)
        {
            if (parent == null) throw new ArgumentNullException(nameof(parent));
            renderGroup.LogicDevice = parent.LogicDevice;
            renderGroup.PrimaryCommandBuffer = parent.PrimaryCommandBuffer;
            renderGroup.Semaphores = parent.Semaphores;
            renderGroup.Fences = parent.Fences;
            renderGroup.RenderPass = pass;
            parent.AddChildren(renderGroup);
            renderGroup.IsBinding = true;
            return renderGroup;
        }

        /// <summary>
        /// 创建次要渲染组
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="name"></param>
        /// <param name="pipeline"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public static SecondaryVulkanRenderGroup CreateSecondaryVulkanRenderGroup(this VulkanGraphicsManager manager, 
            string name, VulkanPipeline pipeline, GroupRenderMode mode = GroupRenderMode.Opaque)
        {
            if (pipeline == null) throw new ArgumentNullException(nameof(pipeline));
            (var check, var message) = pipeline.IsBuildFinish();
            if (!check) throw new Exception($"The argument pipeline is cannot use, the reason {message}");
            if (string.IsNullOrEmpty(name)) name = Guid.NewGuid().ToString("N");
            if (manager.IsContainsGroup(name)) throw new Exception($"The productionLine name {name} is already used");
            SecondaryVulkanRenderGroup renderGroup = new SecondaryVulkanRenderGroup(name, manager.ServiceProvider, pipeline, mode);
            renderGroup.GraphicsManager = manager;
            manager.AddRenderGroup(renderGroup);
            return renderGroup;
        }
        /// <summary>
        /// 将次要渲染组绑定到主要渲染组上
        /// </summary>
        /// <param name="renderGroup"></param>
        /// <param name="primaryRenderGroup"></param>
        /// <param name="secondaryCommand"></param>
        /// <param name="semaphore"></param>
        /// <param name="fences"></param>
        /// <returns></returns>
        public static SecondaryVulkanRenderGroup Binding(this SecondaryVulkanRenderGroup renderGroup, PrimaryVulkanRenderGroup primaryRenderGroup, 
            VulkanCommandBuffer secondaryCommand, IEnumerable<VulkanSemaphore> semaphore, IEnumerable<VulkanFence> fences)
        {
            if (primaryRenderGroup == null) throw new ArgumentNullException(nameof(primaryRenderGroup));
            renderGroup.LogicDevice = primaryRenderGroup.LogicDevice;
            renderGroup.SecondaryCommandBuffer = secondaryCommand ?? throw new ArgumentNullException(nameof(secondaryCommand));
            primaryRenderGroup.AddSecondary(renderGroup);
            if (semaphore != null && semaphore.Any()) renderGroup.Semaphores.AddRange(semaphore);
            if (fences != null && fences.Any()) renderGroup.Fences.AddRange(fences);
            renderGroup.IsBinding = true;
            return renderGroup;
        }
        /// <summary>
        /// 绑定子渲染组至次要渲染组上
        /// </summary>
        /// <param name="renderGroup"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        public static SecondaryVulkanRenderGroup Binding(this SecondaryVulkanRenderGroup renderGroup, SecondaryVulkanRenderGroup parent)
        {
            if (parent == null) throw new ArgumentNullException(nameof(parent));
            renderGroup.LogicDevice = parent.LogicDevice;
            renderGroup.SecondaryCommandBuffer = parent.SecondaryCommandBuffer;
            renderGroup.Semaphores = parent.Semaphores;
            renderGroup.Fences = parent.Fences;
            parent.AddChildren(renderGroup);
            renderGroup.IsBinding = true;
            return renderGroup;
        }
        /// <summary>
        /// 创建点光源渲染对象
        /// </summary>
        /// <typeparam name="TRenderObject"></typeparam>
        /// <param name="manager"></param>
        /// <param name="name"></param>
        /// <param name="position"></param>
        /// <param name="strength"></param>
        /// <returns></returns>
        public static TRenderObject CreatePointLight<TRenderObject>(this VulkanGraphicsManager manager, string name, Vector3 position, Vector4 strength)
            where TRenderObject : RenderObject, new()
        {
            PointLight light = new PointLight(position, strength);
            var render = manager.CreateRenderObject<TRenderObject>(name, light);
            return render;
        }
        /// <summary>
        /// 创建平行光渲染对象
        /// </summary>
        /// <typeparam name="TRenderObject"></typeparam>
        /// <param name="manager"></param>
        /// <param name="name"></param>
        /// <param name="position"></param>
        /// <param name="strength"></param>
        /// <returns></returns>
        public static TRenderObject CreateParallelLight<TRenderObject>(this VulkanGraphicsManager manager, string name, Vector3 position, Vector4 strength)
            where TRenderObject : RenderObject, new()
        {
            ParallelLight light = new ParallelLight(position, strength);
            light.Name = name;
            var render = manager.CreateRenderObject<TRenderObject>(name, light);
            return render;
        }

        /// <summary>
        /// 获取渲染组渲染通道
        /// 渲染组应为主要渲染组
        /// </summary>
        /// <param name="renderGroup"></param>
        /// <returns></returns>
        public static VulkanRenderPass GetRenderPass(this IRenderGroup renderGroup)
        {
            if (renderGroup is not PrimaryVulkanRenderGroup vulkan)
                throw new InvalidCastException($"target render is not {nameof(PrimaryVulkanRenderGroup)}, " +
                    $"invalid to get render pass");
            return vulkan.RenderPass;
        }
    }
}
