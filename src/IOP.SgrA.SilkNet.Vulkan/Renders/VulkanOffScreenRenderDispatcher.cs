﻿using IOP.SgrA.ECS;
using Microsoft.Extensions.Logging;
using Silk.NET.Vulkan;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Semaphore = Silk.NET.Vulkan.Semaphore;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// Vulkan离屏渲染调度器
    /// </summary>
    public class VulkanOffScreenRenderDispatcher : IRenderDispatcher
    {
        /// <summary>
        /// 管理器
        /// </summary>
        public VulkanGraphicsManager Manager { get; set; }
        /// <summary>
        /// 输入
        /// </summary>
        public IInputStateController Input { get; set; }
        /// <summary>
        /// 上下文管理器
        /// </summary>
        public IContextManager ContextManager { get; set; }
        /// <summary>
        /// 事件总线
        /// </summary>
        public SystemEventBus EventBus { get; set; }
        /// <summary>
        /// 渲染结束信号量
        /// </summary>
        public Semaphore RenderFinishSemaphore { get; set; }
        /// <summary>
        /// 渲染通道
        /// </summary>
        public VulkanRenderPass OffScreenRenderPass { get; set; }
        /// <summary>
        /// 目标附件下标
        /// </summary>
        public uint TargetAttachmentIndex { get; set; }
        /// <summary>
        /// 输出缓冲
        /// </summary>
        public List<VulkanBuffer> OutputBuffer { get; set; }
        /// <summary>
        /// 工作队列
        /// </summary>
        public WorkQueue WorkQueue { get; set; }
        /// <summary>
        /// 当前调度器是否正在执行渲染
        /// </summary>
        public bool IsRendering
        {
            get
            {
                SpinWait wait = new SpinWait();
                int local = _IsRendering;
                while (Interlocked.CompareExchange(ref _IsRendering, local, local) != local)
                {
                    wait.SpinOnce();
                    local = _IsRendering;
                }
                return local == 1;
            }
            protected set
            {
                SpinWait wait = new SpinWait();
                int update = value ? 1 : 0;
                int local = _IsRendering;
                while(Interlocked.CompareExchange(ref _IsRendering, update, local) != local)
                {
                    wait.SpinOnce();
                    local = _IsRendering;
                }
            }
        }
        /// <summary>
        /// 呼叫停止执行渲染
        /// </summary>
        protected bool NeedStop 
        {
            get
            {
                SpinWait wait = new SpinWait();
                int local = _NeedStop;
                while (Interlocked.CompareExchange(ref _NeedStop, local, local) != local)
                {
                    wait.SpinOnce();
                    local = _NeedStop;
                }
                return local == 1;
            }
            set
            {
                SpinWait wait = new SpinWait();
                int update = value ? 1 : 0;
                int local = _NeedStop;
                while (Interlocked.CompareExchange(ref _NeedStop, update, local) != local)
                {
                    wait.SpinOnce();
                    local = _NeedStop;
                }
            }
        }

        /// <summary>
        /// 日志
        /// </summary>
        public readonly ILogger<VulkanOffScreenRenderDispatcher> Logger;
        /// <summary>
        /// 待切换组件
        /// </summary>
        protected readonly ConcurrentQueue<IRenderDispatcherComponent> SwitchComponents = new ConcurrentQueue<IRenderDispatcherComponent>();
        /// <summary>
        /// 渲染组任务执行委托
        /// </summary>
        protected Action RenderGroupTaskAction = new Action(() => { });
        /// <summary>
        /// 
        /// </summary>
        protected const int MaxWaring = 50;
        /// <summary>
        /// 
        /// </summary>
        protected IRenderDispatcherComponent RunningComponent = null;
        /// <summary>
        /// 
        /// </summary>
        protected int FrameLength = 0;
        /// <summary>
        /// 
        /// </summary>
        protected int CurrentFrame = 0;
        /// <summary>
        /// 
        /// </summary>
        protected int OutputFrame = 0;
        /// <summary>
        /// 
        /// </summary>
        protected VulkanDevice Device;
        /// <summary>
        /// 
        /// </summary>
        protected VulkanCommandBuffer OutputCommand;
        /// <summary>
        /// 
        /// </summary>
        protected VulkanCommandPool CommandPool;
        /// <summary>
        /// 
        /// </summary>
        protected VulkanFence Fence;
        /// <summary>
        /// 
        /// </summary>
        protected TimeSpan LastTimeStamp;
        /// <summary>
        /// 
        /// </summary>
        protected readonly object SyncRoot = new object();
        private volatile int _IsRendering = 0;
        private volatile int _NeedStop = 0;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="logger"></param>
        public VulkanOffScreenRenderDispatcher(ILogger<VulkanOffScreenRenderDispatcher> logger)
        {
            Logger = logger;
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public virtual void Initialization()
        {
            ContextManager.RefreshUpdateQueue();
            RenderGroupTaskAction = DefaultRenderGroupTask;
            if(OffScreenRenderPass != null)
            {
                var frameBuffer = OffScreenRenderPass.Framebuffer;
                FrameLength = frameBuffer != null ? frameBuffer.Count : 0;
                Device = OffScreenRenderPass.Device;
                var pool = Device.CreateCommandPool((device, option) =>
                {
                    option.Flags = CommandPoolCreateFlags.ResetCommandBufferBit;
                    option.QueueFamilyIndex = WorkQueue.FamilyIndex;
                });
                CommandPool = pool;
                OutputCommand = CommandPool.CreatePrimaryCommandBuffer();
                Fence = Device.CreateFence();
            }
        }
        /// <summary>
        /// 获取事件总线
        /// </summary>
        /// <returns></returns>
        public ISystemEventBus GetSystemEventBus() => EventBus;
        /// <summary>
        /// 获取当前场景
        /// </summary>
        /// <returns></returns>
        public Scene GetCurrentScene() => RunningComponent as Scene;
        /// <summary>
        /// 发送时间戳
        /// </summary>
        /// <param name="timeSpan"></param>
        /// <exception cref="NotImplementedException"></exception>
        public virtual void SendTimeStamp(TimeSpan timeSpan) => LastTimeStamp = timeSpan;
        /// <summary>
        /// 渲染
        /// </summary>
        public virtual void Rendering() => RenderGroupTaskAction();
        /// <summary>
        /// 创建并推送组件
        /// </summary>
        /// <param name="component"></param>
        /// <returns></returns>
        public virtual void PushDispatcherComponent(IRenderDispatcherComponent component)
        {
            if (RunningComponent == null)
            {
                SwitchComponents.Enqueue(component);
                lock (SyncRoot) { GetComponent(); }
            }
            else SwitchComponents.Enqueue(component);
        }
        /// <summary>
        /// 获取输出缓冲
        /// </summary>
        /// <returns></returns>
        public VulkanBuffer GetOutputBuffer() => OutputBuffer[OutputFrame];
        /// <summary>
        /// 
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public int GetOutputBuffer(out VulkanBuffer buffer)
        {
            buffer = OutputBuffer[OutputFrame];
            return OutputFrame;
        }

        /// <summary>
        /// 开始渲染
        /// </summary>
        public void Start()
        {
            NeedStop = false;
        }
        /// <summary>
        /// 停止渲染
        /// </summary>
        public void Stop()
        {
            NeedStop = true;
            SpinWait spinWait = new SpinWait();
            while (IsRendering) { spinWait.SpinOnce(); }
        }

        /// <summary>
        /// 重建渲染调度器
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public virtual void RecreateRenderDispatcher(uint width, uint height)
        {
            VulkanDevice lDevice = Manager.VulkanDevice;
            Stop();
            lDevice.WaitIdle();
            var pass = OffScreenRenderPass;
            pass.RecreateFrameBuffer(width, height);
            pass.BeginOption.RenderArea = new Rect2D { Extent = new Extent2D(width, height), Offset = new Offset2D { X = 0, Y = 0 } };
            var oldBuffer = OutputBuffer.ToArray();
            OutputBuffer.Clear();
            var frameBuffers = pass.Framebuffer;
            for (int i = 0; i < frameBuffers.Count; i++)
            {
                OutputBuffer.Add(CreateCopiedBuffer(lDevice, frameBuffers[i], TargetAttachmentIndex));
            }
            foreach (var o in oldBuffer) o.Dispose();
            Start();
        }
        /// <summary>
        /// 创建复制用缓冲
        /// </summary>
        /// <param name="device"></param>
        /// <param name="frameBuffer"></param>
        /// <param name="targetAttachIndex"></param>
        /// <returns></returns>
        public VulkanBuffer CreateCopiedBuffer(VulkanDevice device, VulkanFrameBuffer frameBuffer, uint targetAttachIndex)
        {
            var attachs = frameBuffer.Attachments;
            if (targetAttachIndex >= attachs.Length) throw new ArgumentOutOfRangeException(nameof(targetAttachIndex), "TargetIndex is greater than framebuffer attach's length");
            VulkanImageView view = attachs[targetAttachIndex];
            MemoryRequirements size = view.VulkanImage.GetMemoryRequirements();
            BufferCreateOption option = new BufferCreateOption()
            {
                Size = size.Size,
                SharingMode = SharingMode.Exclusive,
                Usage = BufferUsageFlags.TransferDstBit
            };
            var buffer = device.CreateBuffer(option, size.Size, MemoryPropertyFlags.HostVisibleBit | MemoryPropertyFlags.HostCachedBit);
            return buffer;
        }
        /// <summary>
        /// 摧毁渲染调度器
        /// </summary>
        public void DestroyDispatcher()
        {
            Device.WaitIdle();
            Stop();
            Device.WaitIdle();
        }

        /// <summary>
        /// 获取组件
        /// </summary>
        /// <returns></returns>
        protected IRenderDispatcherComponent GetComponent()
        {
            if (SwitchComponents.TryDequeue(out IRenderDispatcherComponent component))
            {
                RunningComponent = component;
            }
            if (RunningComponent == null) return null;
            return RunningComponent;
        }

        /// <summary>
        /// 默认渲染组任务
        /// </summary>
        private void DefaultRenderGroupTask()
        {
            if (OutputCommand == null || OutputCommand.RawHandle.Handle == IntPtr.Zero) return;
            try
            {
                var component = GetComponent();
                var queue = WorkQueue.Queue;
                if (component != null && component.IsEnabled)
                {
                    if (NeedStop) return;
                    IsRendering = true;
                    ContextManager.RefreshUpdateQueue();
                    EventBus.UpdateEvent(EventTrigger.SystemFrameBefore);
                    component.ExecuteComponentSystem(LastTimeStamp);
                    EventBus.UpdateEvent(EventTrigger.SystemFrameAfter);
                    int index = ++CurrentFrame % FrameLength;
                    Interlocked.Exchange(ref CurrentFrame, index);
                    OutputCommand.Reset();
                    component.Rendering((uint)index);
                    var frame = OffScreenRenderPass.GetFramebuffer((uint)index);
                    VulkanImageView target = frame.Attachments[TargetAttachmentIndex];
                    VulkanBuffer buffer = OutputBuffer[index];
                    VulkanImage image = target.VulkanImage;
                    var area = target.VulkanImage.CreateOption.Extent;
                    var size = buffer.CreateOption.Size;
                    var sourceRange = new ImageSubresourceRange()
                    {
                        AspectMask = ImageAspectFlags.ColorBit,
                        BaseArrayLayer = 0,
                        BaseMipLevel = 0,
                        LayerCount = 1,
                        LevelCount = 1
                    };
                    OutputCommand.Begin();
                    OutputCommand.CopyImageToBuffer(image, ImageLayout.TransferSrcOptimal, buffer,
                        new BufferImageCopy()
                        {
                            BufferOffset = 0,
                            BufferRowLength = 0,
                            BufferImageHeight = 0,
                            ImageExtent = area,
                            ImageOffset = new Offset3D(0, 0, 0),
                            ImageSubresource = new ImageSubresourceLayers()
                            {
                                AspectMask = ImageAspectFlags.ColorBit,
                                BaseArrayLayer = 0,
                                LayerCount = 1,
                                MipLevel = 0
                            }
                        });
                    OutputCommand.End();
                    SubmitOption option = new SubmitOption
                    {
                        Buffers = new CommandBuffer[] { OutputCommand.RawHandle },
                        SignalSemaphores = null,
                        WaitSemaphores = new Semaphore[] { RenderFinishSemaphore },
                        WaitDstStageMask = new PipelineStageFlags[] { PipelineStageFlags.ColorAttachmentOutputBit }
                    };
                    Device.Submit(WorkQueue.Queue, Fence.RawHandle, option);
                    Result r = Result.Timeout;
                    Span<Fence> f = stackalloc Fence[] { Fence.RawHandle };
                    do
                    {
                        r = Device.WaitForFences(f, true, 100000000);
                    } while (r == Result.Timeout);
                    Device.ResetFences(f);
                    Interlocked.Exchange(ref OutputFrame, index);
                }
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message + "\r\n" + e.StackTrace);
                Stop();
            }
            finally
            {
                IsRendering = false;
            }
        }
    }
}
