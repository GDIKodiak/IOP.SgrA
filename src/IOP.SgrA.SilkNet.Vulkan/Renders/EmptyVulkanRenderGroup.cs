﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class EmptyVulkanRenderGroup : RenderGroup<VulkanContext, VulkanPipeline, EmptyVulkanRenderGroup>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="serviceProvider"></param>
        /// <param name="shader"></param>
        /// <param name="mode"></param>
        public EmptyVulkanRenderGroup(string name, IServiceProvider serviceProvider, VulkanPipeline shader, GroupRenderMode mode) : 
            base(name, serviceProvider, shader, mode)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public override GroupType GroupType => GroupType.Primary;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="contexts"></param>
        public override void BindingContext(Context[] contexts)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="contexts"></param>
        /// <param name="renderObject"></param>
        /// <param name="deepCopy"></param>
        public override void BindingContext(Context[] contexts, IRenderObject renderObject, bool deepCopy = false)
        {
            foreach (var context in contexts)
            {
                context.BindRenderGroup(this);
                context.BindRenderObject(renderObject);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderObject"></param>
        public override void BindingStaticRenderObject(IRenderObject renderObject)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        public override void Dispose()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        public override void GroupRendering()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="frameIndex"></param>
        public override void GroupRendering(uint frameIndex)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        public override void Initialization()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderObject"></param>
        public override void OnAttach(IRenderObject renderObject)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public override void PushContext(Context context)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderObject"></param>
        public override void PushContext(IRenderObject renderObject)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TStruct"></typeparam>
        /// <param name="data"></param>
        /// <param name="setIndex"></param>
        /// <param name="binding"></param>
        public override void PushDataToTexture<TStruct>(TStruct data, uint setIndex, uint binding)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="setIndex"></param>
        /// <param name="binding"></param>
        public override void PushDataToTexture(Span<byte> data, uint setIndex, uint binding)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="minDepth"></param>
        /// <param name="maxDepth"></param>
        public override void SetViewPort(float x, float y, float width, float height, float minDepth, float maxDepth)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <exception cref="NotImplementedException"></exception>
        public override void UnbindContext(Context context)
        {
        }
    }
}
