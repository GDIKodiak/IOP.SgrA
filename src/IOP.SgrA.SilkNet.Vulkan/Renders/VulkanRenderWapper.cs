﻿using IOP.Extension.DependencyInjection;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// Vulkan渲染包装类
    /// </summary>
    public class VulkanRenderWapper
    {
        /// <summary>
        /// 标识符
        /// </summary>
        public string Identifier { get; protected internal set; }
        /// <summary>
        /// 渲染区域
        /// </summary>
        public virtual Area RenderArea { get; set; }
        /// <summary>
        /// 渲染调度器
        /// </summary>
        public IRenderDispatcher RenderDispatcher { get; set; }
        /// <summary>
        /// 服务提供者
        /// </summary>
        [Autowired]
        public IServiceProvider ServiceProvider { get; protected set; }
        /// <summary>
        /// 绘图管理器
        /// </summary>
        [Autowired]
        public IGraphicsManager GraphicsManager { get; protected set; }
        /// <summary>
        /// 模块服务
        /// </summary>
        [Autowired]
        protected IModuleService ModuleService { get; set; }
        /// <summary>
        /// 包装类模块
        /// </summary>
        protected readonly Dictionary<ModulePriority, List<IGraphicsModule>> WapperModules = new Dictionary<ModulePriority, List<IGraphicsModule>>()
        {
            { ModulePriority.Core, new List<IGraphicsModule>() },
            { ModulePriority.ISEA, new List<IGraphicsModule>() },
            { ModulePriority.RenderPass, new List<IGraphicsModule>() },
            { ModulePriority.RenderGroup, new List<IGraphicsModule>() },
            { ModulePriority.Assets, new List<IGraphicsModule>() },
            { ModulePriority.Priority5, new List<IGraphicsModule>() },
            { ModulePriority.Priority6, new List<IGraphicsModule>() },
            { ModulePriority.Priority7, new List<IGraphicsModule>() },
            { ModulePriority.None, new List<IGraphicsModule>() }
        };
        /// <summary>
        /// 
        /// </summary>
        protected readonly Dictionary<Type, IGraphicsModule> ModulesDic = new Dictionary<Type, IGraphicsModule>();
        /// <summary>
        /// 
        /// </summary>
        public VulkanRenderWapper()
        {
            Identifier = Guid.NewGuid().ToString("N");
        }
        /// <summary>
        /// 初始化
        /// </summary>
        public virtual void Initialization() { }
        /// <summary>
        /// 添加包装类模块
        /// </summary>
        /// <param name="types"></param>
        public virtual void AddWapperModules(params Type[] types)
        {
            ModuleInfo[] modules = ModuleService == null ? throw new NullReferenceException(nameof(ModuleService)) :
                ModuleService.CreateModules(ModulesDic, types);
            foreach (var item in modules)
            {
                var module = item.Module;
                if(module is IVulkanRenderWapperModule vModel)
                {
                    vModel.RenderWapper = this;
                    WapperModules[item.ModulePriority].Add(item.Module);
                    ModulesDic.TryAdd(item.Module.GetType(), item.Module);
                }
            }
        }

        /// <summary>
        /// 加载渲染模块
        /// </summary>
        /// <returns></returns>
        public virtual Task LoadRenderModules()
        {
            var _CountdownEvent = new CountdownEvent(64);
            foreach (var item in WapperModules)
            {
                if (item.Value.Any())
                {
                    var list = item.Value;
                    _CountdownEvent.Reset(list.Count);
                    for (int i = 0; i < list.Count; i++)
                    {
                        if (!list[i].IsLoaded)
                        {
                            list[i].ContextManager = RenderDispatcher.ContextManager;
                            _ = list[i].Load().ContinueWith((task) => _CountdownEvent.Signal());
                        }
                        else _CountdownEvent.Signal();
                    }
                    _CountdownEvent.Wait();
                }
            }
            return Task.CompletedTask;
        }

        /// <summary>
        /// 创建并切换场景
        /// </summary>
        /// <typeparam name="TScene"></typeparam>
        /// <param name="name"></param>
        /// <param name="renderGroup"></param>
        /// <param name="componentSystems"></param>
        /// <returns></returns>
        public virtual TScene CreateAndCutScene<TScene>(string name, IRenderGroup renderGroup, params Type[] componentSystems)
            where TScene : Scene
        {
            var disaptcher = RenderDispatcher;
            TScene scene = ServiceProvider.CreateAutowiredInstance<TScene>();
            scene.Name = name;
            scene.RenderGroup = renderGroup;
            scene.RenderArea = RenderArea;
            scene.ContextManager = disaptcher.ContextManager;
            scene.Input = disaptcher.Input;
            scene.EventBus = disaptcher.GetSystemEventBus();
            scene.AddComponentSystem(componentSystems);
            VulkanLightEnvironment light = new(scene, GraphicsManager);
            scene.LightEnvironment = light;
            scene.IsEnabled = true;
            disaptcher.PushDispatcherComponent(scene);
            return scene;
        }
        /// <summary>
        /// 获取当前场景
        /// </summary>
        public virtual Scene GetCurrnetScene => RenderDispatcher.GetCurrentScene();

        /// <summary>
        /// 获取渲染调度器
        /// </summary>
        /// <typeparam name="TDispatcher"></typeparam>
        /// <returns></returns>
        public virtual TDispatcher GetRenderDispatcher<TDispatcher>()
            where TDispatcher : class, IRenderDispatcher => RenderDispatcher as TDispatcher;
        /// <summary>
        /// 是否存在渲染曲面
        /// </summary>
        /// <returns></returns>
        public virtual bool HasVulkanSurface() => false;
        /// <summary>
        /// 获取呈现曲面
        /// </summary>
        public virtual SurfaceKHR GetVulkanSurface() => new SurfaceKHR(0);
    }
}
