﻿using Microsoft.Extensions.Logging;
using Silk.NET.Vulkan;
using System;
using System.Buffers;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static System.Reflection.Metadata.BlobBuilder;
using Semaphore = Silk.NET.Vulkan.Semaphore;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 主要渲染组
    /// </summary>
    public class PrimaryVulkanRenderGroup : RenderGroup<VulkanContext, VulkanPipeline, PrimaryVulkanRenderGroup>
    {
        /// <summary>
        /// 渲染组类型
        /// </summary>
        public override GroupType GroupType { get; } = GroupType.Primary;
        /// <summary>
        /// 渲染管理器
        /// </summary>
        public VulkanGraphicsManager GraphicsManager { get; internal set; }
        /// <summary>
        /// 逻辑设备
        /// </summary>
        public VulkanDevice LogicDevice { get; internal set; }
        /// <summary>
        /// 主要命令缓冲
        /// </summary>
        public VulkanCommandBuffer PrimaryCommandBuffer { get; internal set; }
        /// <summary>
        /// 渲染流程
        /// </summary>
        public VulkanRenderPass RenderPass { get; internal set; }
        /// <summary>
        /// 工作队列
        /// </summary>
        public WorkQueue[] WorkQueues { get; internal set; }
        /// <summary>
        /// 信号量
        /// </summary>
        public Semaphore[] Semaphores { get; internal set; } = Array.Empty<Semaphore>();
        /// <summary>
        /// 栅栏列表
        /// </summary>
        public Fence[] Fences { get; internal set; } = Array.Empty<Fence>();
        /// <summary>
        /// Vulkan事件
        /// </summary>
        public Event[] Events { get; internal set; } = Array.Empty<Event>();
        /// <summary>
        /// 需要触发的信号量
        /// </summary>
        public List<VulkanSemaphore> SignalSemaphores { get; internal set; } = new List<VulkanSemaphore>();
        /// <summary>
        /// 次要渲染组
        /// </summary>
        protected ConcurrentBag<SecondaryVulkanRenderGroup> Secondaries { get; } = new ConcurrentBag<SecondaryVulkanRenderGroup>();
        /// <summary>
        /// 超时时间
        /// </summary>
        protected int Timeout { get; set; } = 10000;
        /// <summary>
        /// 最大任务数量
        /// </summary>
        protected int MaxTaskCount { get; set; } = 64;
        /// <summary>
        /// 信号控制器
        /// </summary>
        protected CountdownEvent Countdown = new CountdownEvent(0);
        /// <summary>
        /// 次要渲染组集合
        /// </summary>
        protected ConcurrentBag<VulkanCommandBuffer> SecondaryCommandBuffers { get; } = new ConcurrentBag<VulkanCommandBuffer>();
        /// <summary>
        /// 
        /// </summary>
        protected internal bool IsBinding = false;
        /// <summary>
        /// 
        /// </summary>
        protected uint Counter = 0;
        /// <summary>
        /// 
        /// </summary>

        protected ConcurrentDictionary<uint, VulkanContext> ContextsPool = new();

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="name"></param>
        /// <param name="serviceProvider"></param>
        /// <param name="shader"></param>
        /// <param name="mode"></param>
        public PrimaryVulkanRenderGroup(string name, IServiceProvider serviceProvider, VulkanPipeline shader, GroupRenderMode mode = GroupRenderMode.Opaque)
            : base(name, serviceProvider, shader, mode)
        {
            Countdown = new CountdownEvent(MaxTaskCount);
        }
        /// <summary>
        /// 初始化函数
        /// </summary>
        public override void Initialization() 
        {
            InitializationAction(this);
            foreach(var item in Secondaries)
            {
                item.Initialization();
            }
        }
        /// <summary>
        /// 是否启用
        /// </summary>
        /// <returns></returns>
        public override bool IsEnable() => IsBinding && !IsStop;
        /// <summary>
        /// 组渲染
        /// </summary>
        public override void GroupRendering()
        {
            GroupProductionLine(this);
            Interlocked.Exchange(ref Counter, 0);
        }
        /// <summary>
        /// 组渲染
        /// </summary>
        /// <param name="frameIndex"></param>
        public override void GroupRendering(uint frameIndex)
        {
            CurrentFrame = frameIndex;
            GroupProductionLine(this);
            Interlocked.Exchange(ref Counter, 0);
        }
        /// <summary>
        /// 设置视口
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="minDepth"></param>
        /// <param name="maxDepth"></param>
        public override void SetViewPort(float x, float y, float width, float height, float minDepth, float maxDepth)
        {
            Camera?.SetViewport(x, y, width, height, minDepth, maxDepth);
        }
        /// <summary>
        /// 推送数据至缓冲
        /// </summary>
        /// <typeparam name="TStruct"></typeparam>
        /// <param name="data"></param>
        /// <param name="binding"></param>
        /// <param name="setIndex"></param>
        public override void PushDataToTexture<TStruct>(TStruct data, uint setIndex, uint binding)
            where TStruct : struct
        {
            if (Pipeline == null) return;
            if(Pipeline.TryGetPipelineTexture(setIndex, binding, out VulkanTexture texture))
            {
                var size = Marshal.SizeOf(data);
                if ((ulong)size > texture.TextureData.BytesSize)
                {
                    Logger.LogWarning("Push data to texture failed, input data is too large");
                    return;
                }
                byte[] o = ArrayPool<byte>.Shared.Rent(size);
                Span<byte> d = o;
                MemoryMarshal.Write(d, ref data);
                Span<byte> f = d.Slice(0, size);
                texture.UpdateTextureMemoryData(f);
                ArrayPool<byte>.Shared.Return(o);
            }
            else Logger.LogWarning($"No found texture with Index {setIndex} Binding {binding}");
        }
        /// <summary>
        /// 推送数据至纹理
        /// </summary>
        /// <param name="data"></param>
        /// <param name="setIndex"></param>
        /// <param name="binding"></param>
        public override void PushDataToTexture(Span<byte> data, uint setIndex, uint binding)
        {
            if (Pipeline == null) return;
            if (Pipeline.TryGetPipelineTexture(setIndex, binding, out VulkanTexture texture))
            {
                if ((ulong)data.Length > texture.TextureData.BytesSize)
                {
                    Logger.LogWarning("Push data to texture failed, input data is too large");
                    return;
                }
                texture.UpdateTextureMemoryData(data);
            }
            else Logger.LogWarning($"No found texture with Index {setIndex} Binding {binding}");
        }

        /// <summary>
        /// 推送上下文至渲染组
        /// </summary>
        /// <param name="context"></param>
        public override void PushContext(Context context)
        {
            Contexts.Enqueue(context);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderObject"></param>
        public override void PushContext(IRenderObject renderObject)
        {
            VulkanContext context = GetInternalContext(renderObject);
            Contexts.Enqueue(context);
        }
        /// <summary>
        /// 绑定上下文
        /// </summary>
        /// <param name="contexts"></param>
        public override void BindingContext(Context[] contexts)
        {
            foreach (var context in contexts)
            {
                if (context is VulkanContext local)
                {
                    local.VulkanPipeline = Pipeline;
                    local.LogicDevice = Pipeline.Device;
                    local.CommandBuffer = PrimaryCommandBuffer;
                    local.BindRenderGroup(this);
                }
            }
        }
        /// <summary>
        /// 绑定上下文
        /// </summary>
        /// <param name="contexts"></param>
        /// <param name="renderObject"></param>
        /// <param name="deepCopy"></param>
        public override void BindingContext(Context[] contexts, IRenderObject renderObject, bool deepCopy = false)
        {
            if (Pipeline == null) return;
            foreach (var context in contexts)
            {
                if (context is VulkanContext local)
                {
                    local.VulkanPipeline = Pipeline;
                    local.LogicDevice = Pipeline.Device;
                    local.CommandBuffer = PrimaryCommandBuffer;
                    local.BindRenderGroup(this);
                    IRenderObject obj;
                    if (deepCopy) obj = renderObject.DeepCopy(GraphicsManager);
                    else obj = renderObject;
                    local.BindRenderObject(obj);
                    BindDescriptorSetToObject(obj);
                    var constants = Pipeline.CreateConstantBuffers();
                    obj.AddComponents(constants);
                }
            }
        }
        /// <summary>
        /// 解除上下文绑定
        /// </summary>
        /// <param name="context"></param>
        public override void UnbindContext(Context context)
        {
            if(context != null)
            {
                var obj = context.GetContextRenderObject();
                if (obj != null) obj.Destroy(GraphicsManager);
            }
        }
        /// <summary>
        /// 绑定静态渲染对象
        /// </summary>
        /// <param name="renderObject"></param>
        public override void BindingStaticRenderObject(IRenderObject renderObject)
        {
            if (StaticRenderObjects.Contains(renderObject)) return;
            BindDescriptorSetToObject(renderObject);
            StaticRenderObjects.Add(renderObject);
        }

        /// <summary>
        /// 添加子渲染组
        /// </summary>
        /// <param name="child"></param>
        public override void AddChildren(PrimaryVulkanRenderGroup child)
        {
            base.AddChildren(child);
        }
        /// <summary>
        /// 获取次要渲染组
        /// </summary>
        /// <returns></returns>
        public SecondaryVulkanRenderGroup[] GetSecondaries() => Secondaries.ToArray();
        /// <summary>
        /// 添加次要渲染组
        /// </summary>
        /// <param name="childRenderGroup"></param>
        public void AddSecondary(SecondaryVulkanRenderGroup childRenderGroup)
        {
            if (childRenderGroup == null) throw new ArgumentNullException(nameof(childRenderGroup));
            childRenderGroup.Primary = this;
            Secondaries.Add(childRenderGroup);
        }
        /// <summary>
        /// 添加次要渲染组
        /// </summary>
        /// <param name="childRenderGroups"></param>
        public void AddSecondaries(IEnumerable<SecondaryVulkanRenderGroup> childRenderGroups)
        {
            if (childRenderGroups != null && childRenderGroups.Any())
            {
                foreach (var item in childRenderGroups)
                {
                    item.Primary = this;
                    Secondaries.Add(item);
                }
            }
        }
        /// <summary>
        /// 次要渲染组渲染
        /// </summary>
        /// <param name="renderPass"></param>
        /// <param name="frame"></param>
        public virtual void SecondaryGroupRendering(VulkanRenderPass renderPass, VulkanFrameBuffer frame)
        {
            if (Secondaries.Any())
            {
                Countdown.Reset(Secondaries.Count);
                SecondaryCommandBuffers.Clear();
                Parallel.ForEach(Secondaries, (child) =>
                {
                    if (child.IsEnable())
                    {
                        child.RenderPass = renderPass;
                        child.Framebuffer = frame;
                        try
                        {
                            child.GroupRendering();
                            SecondaryCommandBuffers.Add(child.SecondaryCommandBuffer);
                        }
                        catch (Exception e)
                        {
                            child.Logger.LogError(e.Message + "\r\n" + e.StackTrace);
                            child.Disable();
                        }
                    }
                    {
                        child.ClearContexts();
                        Countdown.Signal();
                    }
                });
                Countdown.Wait(Timeout);
            }
        }
        /// <summary>
        /// 获取需要更新的次级命令缓冲
        /// </summary>
        /// <returns></returns>
        public virtual VulkanCommandBuffer[] GetUpdateSecondaryBuffers() => SecondaryCommandBuffers.ToArray();

        /// <summary>
        /// 设置超时时间
        /// </summary>
        /// <param name="timeout"></param>
        public virtual void SetTimeout(int timeout)
        {
            if (timeout <= 0) timeout = int.MaxValue;
            Timeout = timeout;
        }
        /// <summary>
        /// 变更最大任务数
        /// </summary>
        /// <param name="count"></param>
        public virtual void ChangeMaxTaskCount(int count)
        {
            Countdown.Reset(count);
            MaxTaskCount = count;
        }
        /// <summary>
        /// 设置触发用信号量
        /// </summary>
        /// <param name="semaphores"></param>
        public PrimaryVulkanRenderGroup SetSignalSemaphores(IEnumerable<VulkanSemaphore> semaphores)
        {
            SignalSemaphores.AddRange(semaphores);
            return this;
        }
        /// <summary>
        /// 清理触发用信号量
        /// </summary>
        public PrimaryVulkanRenderGroup ClearSignalSemaphore()
        {
            SignalSemaphores.Clear();
            return this;
        }
        /// <summary>
        /// 禁用
        /// </summary>
        public override void Disable()
        {
            PrimaryCommandBuffer.Reset();
            base.Disable();
        }

        /// <summary>
        /// 销毁资源
        /// </summary>
        public override void Dispose()
        {
            GroupProductionLine = null;
            GroupProductionLineBuilder = null;
            ItemProductionLine = null;
            ItemProductionLineBuilder = null;
            Pipeline = null;
            Contexts.Clear();
            foreach (var i in Secondaries)
            {
                i.Dispose();
            }
            Childrens.Clear();
            Camera = null;
            Fences = null;
            Semaphores = null;
            WorkQueues = null;
            RenderPass = null;
            LogicDevice = null;
            Parent = null;
            SecondaryCommandBuffers.Clear();
            PrimaryCommandBuffer = null;
            Countdown.Dispose();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderObject"></param>
        public override void OnAttach(IRenderObject renderObject)
        {
            BindDescriptorSetToObject(renderObject);
        }

        /// <summary>
        /// 绑定描述符集至渲染对象
        /// </summary>
        /// <param name="obj"></param>
        protected void BindDescriptorSetToObject(IRenderObject obj)
        {
            if (Pipeline == null) throw new InvalidOperationException($"Render gourp {Name} is not contain pipeline to rending render object");
            List<WriteDescriptorSet> writes = new List<WriteDescriptorSet>();
            Dictionary<uint, DescriptorSet> books = new Dictionary<uint, DescriptorSet>();
            foreach (var item in obj.GetComponents<VulkanTexture>())
            {
                DescriptorSet sets;
                if (books.ContainsKey(item.SetIndex)) sets = books[item.SetIndex];
                else
                {
                    sets = Pipeline.CreateDescriptorSet(item.SetIndex);
                    books.Add(item.SetIndex, sets);
                }
                item.BindDescriptorSet(sets);
                writes.Add(item.WriteDescriptorSet);
            }
            if (writes.Count > 0) LogicDevice.UpdateDescriptorSets(writes.ToArray(), null);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderObject"></param>
        /// <returns></returns>
        protected VulkanContext GetInternalContext(IRenderObject renderObject)
        {
            uint counter = Counter;
            SpinWait wait = new();
            while (Interlocked.CompareExchange(ref Counter, counter + 1, counter) != counter)
            {
                wait.SpinOnce();
                counter = Counter;
            }
            counter++;
            VulkanContext context;
            if (counter >= ContextsPool.Count + 1)
            {
                context = new VulkanContext();
                ContextsPool.TryAdd(counter, context);
            }
            else if (!ContextsPool.TryGetValue(counter, out VulkanContext c))
            {
                context = new VulkanContext();
                ContextsPool.TryAdd(counter, context);
                return context;
            }
            else context = c;
            context.VulkanPipeline = Pipeline;
            context.LogicDevice = Pipeline.Device;
            context.CommandBuffer = PrimaryCommandBuffer;
            context.BindRenderGroup(this);
            context.BindRenderObject(renderObject);
            return context;
        }
    }
}
