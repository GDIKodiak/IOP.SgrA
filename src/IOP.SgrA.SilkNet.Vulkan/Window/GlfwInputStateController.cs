﻿using IOP.SgrA.Input;
using Silk.NET.GLFW;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class GlfwInputStateController : InputStateController
    {
        /// <summary>
        /// 
        /// </summary>
        private Glfw NativeApi { get; set; }
        /// <summary>
        /// 
        /// </summary>
        private IntPtr Window { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="glfw"></param>
        /// <param name="window"></param>
        public GlfwInputStateController(Glfw glfw, IntPtr window)
        {
            NativeApi = glfw;
            Window = window;
        }

        /// <summary>
        /// 隐藏光标
        /// </summary>
        /// <param name="enable"></param>
        public override void CursorDisabled(bool enable)
        {
            InputCommand.Enqueue(() =>
            {
                bool r = enable;
                unsafe
                {
                    if (r)
                    {
                        NativeApi.SetInputMode((WindowHandle*)Window, CursorStateAttribute.Cursor, CursorModeValue.CursorNormal);
                    }
                    else
                    {
                        NativeApi.SetInputMode((WindowHandle*)Window, CursorStateAttribute.Cursor, CursorModeValue.CursorDisabled);
                    }
                }
            });
        }
    }
}
