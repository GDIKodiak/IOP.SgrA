﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using IOP.Extension.DependencyInjection;
using IOP.SgrA.Input;
using Silk.NET.GLFW;
using Silk.NET.Vulkan;
using Microsoft.Extensions.Logging;
using System.Numerics;
using System.Threading;
using System.Linq;
using System.Runtime.CompilerServices;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class VulkanGlfwWindow : VulkanRenderWapper, IGenericWindow
    {
        /// <summary>
        /// 渲染区域
        /// </summary>
        public override Area RenderArea { get => _Area; set => _Area = value; }
        /// <summary>
        /// 底层API
        /// </summary>
        public readonly Glfw Native = Glfw.GetApi();
        /// <summary>
        /// 窗口句柄
        /// </summary>
        public IntPtr WindowHandle { get; protected internal set; } = IntPtr.Zero;
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; protected internal set; }
        /// <summary>
        /// 配置项
        /// </summary>
        public WindowOption Option 
        {
            get => _Option;
            protected internal set
            {
                if(value != null)
                {
                    _Option = value;
                    _Area = new Area(0, 0, value.Width, value.Height);
                }
            }
        }
        /// <summary>
        /// 宽纵比
        /// </summary>
        public float Aspect { get; private set; }
        /// <summary>
        /// 日志
        /// </summary>
        public ILogger<VulkanGlfwWindow> Logger { get; protected set; }
        /// <summary>
        /// 输入
        /// </summary>
        public InputStateController Input { get; protected set; }
        /// <summary>
        /// FPS工具类
        /// </summary>
        public FPSUtil FPS { get; protected set; }
        /// <summary>
        /// 鼠标移动事件
        /// </summary>
        public event EventHandler<MouseMoveEventArgs> MouseMove;
        /// <summary>
        /// 鼠标按钮触发事件
        /// </summary>
        public event EventHandler<MouseButtonEventArgs> MouseButton;
        /// <summary>
        /// 键盘事件
        /// </summary>
        public event EventHandler<KeyboardEventArgs> PressKey;
        /// <summary>
        /// 窗口大小变更事件
        /// </summary>
        public event EventHandler<EventArgs> Resize;
        /// <summary>
        /// 当加载结束时
        /// </summary>
        public event EventHandler<EventArgs> Loaded;
        /// <summary>
        /// 
        /// </summary>
        [Autowired]
        protected ILoggerFactory LoggerFactory { get; set; }
        /// <summary>
        /// 光标坐标
        /// </summary>
        protected Vector2 Position { get; set; }
        /// <summary>
        /// 表面
        /// </summary>
        protected internal SurfaceKHR Surface { get; set; }
        /// <summary>
        /// 当窗口大小发生变更时
        /// </summary>
        protected Action<VulkanGlfwWindow, uint, uint> Resized;
        /// <summary>
        /// 配置任务
        /// </summary>
        internal Func<Task> ConfigTask { get; set; }

        private GlfwCallbacks.MouseButtonCallback MouseHandle;
        private GlfwCallbacks.FramebufferSizeCallback FramebuffersizeFun;
        private GlfwCallbacks.KeyCallback KeyCallBack;
        private Area _Area;
        private WindowOption _Option = new WindowOption();

        /// <summary>
        /// 初始化
        /// </summary>
        public override void Initialization()
        {
            Native.WindowHint(WindowHintClientApi.ClientApi, ClientApi.NoApi);
            unsafe
            {
                WindowHandle* handle = Native.CreateWindow((int)Option.Width, (int)Option.Height, Option.Title, null, null);
                if (handle == null) throw new InvalidOperationException("Create Glfw window failed");
                MouseHandle = MouseButtonHandle;
                KeyCallBack = PressKeyHandle;
                FramebuffersizeFun = ResizeHandle;
                Native.SetMouseButtonCallback(handle, MouseHandle);
                Native.SetKeyCallback(handle, KeyCallBack);
                Native.SetFramebufferSizeCallback(handle, FramebuffersizeFun);
                IntPtr ptr = new IntPtr(handle);
                Input = new GlfwInputStateController(Native, ptr);
                WindowHandle = ptr;
                Aspect = (float)Option.Width / Option.Height;
                
            }
            Logger = LoggerFactory.CreateLogger<VulkanGlfwWindow>();
        }
        /// <summary>
        /// 运行窗口
        /// </summary>
        public void RunWindow() => RunWindow(120);
        /// <summary>
        /// 运行窗口
        /// </summary>
        /// <param name="updateRate"></param>
        public virtual void RunWindow(double updateRate = 120)
        {
            FPS = new FPSUtil(updateRate);
            var startTime = DateTime.Now;
            var endTime = startTime.AddSeconds(5);
            while (true)
            {
                bool r;
                unsafe { r = Native.WindowShouldClose((WindowHandle*)WindowHandle); }
                try
                {
                    if (!r)
                    {
                        FPS.Begin();
                        RenderDispatcher.Rendering();
                        Input.RefreshMouseStateFrame();
                        Input.RefreshKeyboardFrame();
                        Input.RefreshInputCommand();
                        Native.PollEvents();
                        GetCursorPosition();
                        if (Option.IntervalMode == ThreadIntervalMode.YieldMode) FPS.Yield();
                        else FPS.Sleep();
                        FPS.CallFPS();
                        RenderDispatcher.SendTimeStamp(FPS.GetLastStamp());
                        startTime = DateTime.Now;
                        if (startTime > endTime)
                        {
                            Logger.LogInformation($"FPS: {FPS.CurrentFPS}");
                            endTime = startTime.AddSeconds(5);
                        }
                    }
                    else 
                        break;
                }
                catch (Exception e)
                {
                    Logger.LogError(e.Message + "\r\n" + e.StackTrace);
                    break;
                }
            }
        }
        /// <summary>
        /// 是否拥有呈现曲面
        /// </summary>
        /// <returns></returns>
        public override bool HasVulkanSurface() => Surface.Handle != 0;
        /// <summary>
        /// 获取呈现曲面
        /// </summary>
        /// <returns></returns>
        public override SurfaceKHR GetVulkanSurface() => Surface;

        /// <summary>
        /// 运行配置任务
        /// </summary>
        /// <returns></returns>
        public Task RunConfigTask() => ConfigTask() ?? Task.CompletedTask;
        /// <summary>
        /// 注册窗口大小变更事件
        /// </summary>
        /// <param name="handle"></param>
        public virtual void ConfigResizeCallbackHandle(Action<VulkanGlfwWindow, uint, uint> handle) => Resized = handle;

        /// <summary>
        /// 当鼠标移动时
        /// </summary>
        /// <param name="args"></param>
        public virtual void OnMouseMove(MouseMoveEventArgs args) => MouseMove?.Invoke(this, args);
        /// <summary>
        /// 当鼠标发送按钮事件时
        /// </summary>
        /// <param name="args"></param>
        public virtual void OnMouseButton(MouseButtonEventArgs args) => MouseButton?.Invoke(this, args);
        /// <summary>
        /// 键盘事件
        /// </summary>
        /// <param name="args"></param>
        public virtual void OnPressKey(KeyboardEventArgs args) => PressKey?.Invoke(this, args);
        /// <summary>
        /// 当窗口发生变更时
        /// </summary>
        /// <param name="args"></param>
        public virtual void OnResize(EventArgs args) => Resize?.Invoke(this, args);
        /// <summary>
        /// 加载时
        /// </summary>
        /// <param name="args"></param>
        public void OnLoaded(EventArgs args)
        {
            Loaded?.Invoke(this, args);
        }
        /// <summary>
        /// 销毁窗口
        /// </summary>
        public void Dispose()
        {
            MouseHandle = null;
            FramebuffersizeFun = null;
            KeyCallBack = null;
            unsafe
            {
                GraphicsManager?.DestroyWindow(this);
                Native.DestroyWindow((WindowHandle*)WindowHandle);
            }
        }

        /// <summary>
        /// 获取实时鼠标坐标
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void GetCursorPosition()
        {
            Vector2 old = Position;
            double x = 0;
            double y = 0;
            unsafe { Native.GetCursorPos((WindowHandle*)WindowHandle, out x, out y); }
            Vector2 newP = new Vector2((float)x, (float)y);
            Vector2 diff = old - newP;
            if (diff.X != 0 || diff.Y != 0)
            {
                MouseMoveEventArgs args = new MouseMoveEventArgs((int)x, (int)y, (int)diff.X, (int)diff.Y);
                Position = newP;
                Input.MouseState.X = (int)x;
                Input.MouseState.Y = (int)y;
                Input.MouseState.XDelta = (int)diff.X;
                Input.MouseState.YDelta = (int)diff.Y;
                OnMouseMove(args);
            }
            else
            {
                Input.MouseState.XDelta = 0;
                Input.MouseState.YDelta = 0;
            }
        }
        /// <summary>
        /// 窗口大小变更事件
        /// </summary>
        /// <param name="window"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        private unsafe void ResizeHandle(WindowHandle* window, int width, int height)
        {
            Option.Width = (uint)width;
            Option.Height = (uint)height;
            _Area.Height = (uint)height;
            _Area.Width = (uint)width;
            RenderDispatcher.Stop();
            if (width == 0 || height == 0) return;
            Resized?.Invoke(this, (uint)width, (uint)height);
            OnResize(new EventArgs());
            RenderDispatcher.Start();
        }
        /// <summary>
        /// 鼠标点击事件
        /// </summary>
        /// <param name="window"></param>
        /// <param name="button"></param>
        /// <param name="action"></param>
        /// <param name="mods"></param>
        private unsafe void MouseButtonHandle(WindowHandle* window, Silk.NET.GLFW.MouseButton button, Silk.NET.GLFW.InputAction action, KeyModifiers mods)
        {
            Input.MouseButton sButton = (Input.MouseButton)button;
            Input.InputAction sAction = (Input.InputAction)action;
            InputMods sMods = (InputMods)mods;
            MouseButtonEventArgs args = new MouseButtonEventArgs((int)Position.X, (int)Position.Y, sButton, sAction, sMods);
            Input.MouseButtonStateChanged(new MouseStateFrame(DateTime.Now, sButton, sAction, sMods));
            OnMouseButton(args);
        }
        /// <summary>
        /// 键盘事件处理函数
        /// </summary>
        /// <param name="window"></param>
        /// <param name="key"></param>
        /// <param name="scancode"></param>
        /// <param name="action"></param>
        /// <param name="mods"></param>
        private unsafe void PressKeyHandle(WindowHandle* window, Keys key, int scancode, Silk.NET.GLFW.InputAction action, KeyModifiers mods)
        {
            PressKey pKey = (PressKey)key;
            Input.InputAction sAction = (Input.InputAction)action;
            InputMods sMods = (InputMods)mods;
            KeyboardEventArgs args = new KeyboardEventArgs(pKey, sAction, sMods);
            Input.KeyboardStateChanged(new PressKeyStateFrame(DateTime.Now, pKey, sAction, sMods));
            OnPressKey(args);
        }
    }
}
