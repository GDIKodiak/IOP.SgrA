﻿using Microsoft.Extensions.DependencyInjection;
using IOP.Extension.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Silk.NET.GLFW;
using Silk.NET.Core.Native;
using Silk.NET.Vulkan;
using System.Linq;
using IOP.SgrA.ECS;
using System.Reflection;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public static class VulkanManagerWindowExtension
    {
        /// <summary>
        /// 新建窗口
        /// </summary>
        /// <typeparam name="TWindow"></typeparam>
        /// <param name="vulkanManager"></param>
        /// <param name="name"></param>
        /// <param name="optionAction"></param>
        /// <returns></returns>
        public static TWindow CreateWindow<TWindow>(this VulkanGraphicsManager vulkanManager, string name, Action<WindowOption> optionAction)
            where TWindow : VulkanGlfwWindow
        {
            var options = new WindowOption();
            optionAction?.Invoke(options);
            var window = CreateWindow<TWindow>(vulkanManager, name, options);
            return window;
        }
        /// <summary>
        /// 新建窗口
        /// </summary>
        /// <typeparam name="TWindow"></typeparam>
        /// <param name="vulkanManager"></param>
        /// <param name="name"></param>
        /// <param name="option"></param>
        /// <returns></returns>
        public static TWindow CreateWindow<TWindow>(this VulkanGraphicsManager vulkanManager, string name, WindowOption option)
            where TWindow : VulkanGlfwWindow
        {
            return vulkanManager.CreateWindow<TWindow>(name, option);
        }

        /// <summary>
        /// 配置窗口
        /// </summary>
        /// <typeparam name="TWindow"></typeparam>
        /// <typeparam name="TConfig"></typeparam>
        /// <param name="window"></param>
        /// <returns></returns>
        public static TWindow ConfigWindow<TWindow, TConfig>(this TWindow window)
            where TWindow : VulkanGlfwWindow
            where TConfig : class, IWindowConfig<TWindow, VulkanGraphicsManager>
        {
            var manager = window.GraphicsManager;
            if (!(manager is VulkanGraphicsManager vManager)) throw new InvalidCastException("No Vulkan Environmental");
            TConfig config = manager.ServiceProvider.CreateAutowiredInstance<TConfig>();
            config.Initialization();
            window.ConfigTask = () => config.Config(window, vManager);
            return window;
        }

        /// <summary>
        /// 配置Vulkan渲染调度器
        /// </summary>
        /// <param name="workQueue"></param>
        /// <param name="swapchain"></param>
        /// <param name="imageAvailableSemaphore"></param>
        /// <param name="renderFinishSemaphore"></param>
        /// <param name="swapchainRenderPass"></param>
        /// <param name="window"></param>
        /// <returns></returns>
        public static TWindow CreateVulkanRenderDispatcher<TWindow>(this TWindow window, WorkQueue workQueue, 
            VulkanSwapchain swapchain, VulkanRenderPass swapchainRenderPass,
            VulkanSemaphore imageAvailableSemaphore, VulkanSemaphore renderFinishSemaphore)
            where TWindow : VulkanGlfwWindow
        {
            VulkanSwapchainRenderDispatcher dispatcher = window.ServiceProvider.CreateAutowiredInstance<VulkanSwapchainRenderDispatcher>();
            var context = window.ServiceProvider.CreateAutowiredInstance<VulkanContextManager>();
            var eventBus = window.ServiceProvider.CreateAutowiredInstance<SystemEventBus>();
            window.RenderDispatcher = dispatcher;
            dispatcher.Swapchain = swapchain;
            dispatcher.SwapchainRenderPass = swapchainRenderPass;
            dispatcher.PresentQueue = workQueue;
            dispatcher.ImageAvailableSemaphore = imageAvailableSemaphore.RawHandle;
            dispatcher.RenderFinishSemaphore = renderFinishSemaphore.RawHandle;
            dispatcher.Input = window.Input;
            dispatcher.ContextManager = window.GraphicsManager.GetContextManager();
            dispatcher.EventBus = eventBus;
            dispatcher.Manager = window.GraphicsManager as VulkanGraphicsManager;
            dispatcher.Initialization();
            return window;
        }

        /// <summary>
        /// 获取交换链渲染调度器交换链渲染通道
        /// </summary>
        /// <param name="dispatcher"></param>
        /// <returns></returns>
        public static VulkanRenderPass GetSwapchainRenderPass(this IRenderDispatcher dispatcher)
        {
            if (dispatcher is VulkanSwapchainRenderDispatcher vDispatcher)
            {
                return vDispatcher.SwapchainRenderPass;
            }
            else throw new NotSupportedException("Target RenderDispatcher is not a VulkanSwapchainRenderDispatcher");
        }
        /// <summary>
        /// 获取离屏渲染调度器渲染通道
        /// </summary>
        /// <param name="dispatcher"></param>
        /// <returns></returns>
        public static VulkanRenderPass GetOffScreenRenderPass(this IRenderDispatcher dispatcher)
        {
            if (dispatcher is VulkanOffScreenRenderDispatcher oDisaptcher)
            {
                return oDisaptcher.OffScreenRenderPass;
            }
            else throw new NotSupportedException("Target RenderDispatcher is not a VulkanOffScreenRenderDispatcher");
        }
        /// <summary>
        /// 获取渲染通道
        /// </summary>
        /// <param name="dispatcher"></param>
        /// <returns></returns>
        public static VulkanRenderPass GetRenderPass(this IRenderDispatcher dispatcher)
        {
            if (dispatcher is VulkanSwapchainRenderDispatcher vDispatcher)
            {
                return vDispatcher.SwapchainRenderPass;
            }
            else if (dispatcher is VulkanOffScreenRenderDispatcher oDisaptcher)
            {
                return oDisaptcher.OffScreenRenderPass;
            }
            else throw new NotSupportedException("Unknown RenderDispatcher");
        }

        /// <summary>
        /// 运行窗口
        /// </summary>
        /// <typeparam name="TWindow"></typeparam>
        /// <param name="vulkanManager"></param>
        /// <param name="window"></param>
        public static void RunWindow<TWindow>(this VulkanGraphicsManager vulkanManager, TWindow window)
            where TWindow : VulkanGlfwWindow
        {
            Task.Factory.StartNew(() =>
            {
                AutoResetEvent @event = new AutoResetEvent(false);
                try
                {
                    var instance = vulkanManager.Instance;
                    if (instance.RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Please create vulkan instance first");
                    window.Initialization();
                    var api = window.Native;
                    unsafe 
                    {
                        VkNonDispatchableHandle handle = new VkNonDispatchableHandle();
                        var r = api.CreateWindowSurface(instance.RawHandle.ToHandle(), (WindowHandle*)window.WindowHandle, null, &handle);
                        if ((ErrorCode)r != ErrorCode.NoError) throw new InvalidOperationException("Create window surface failed");
                        SurfaceKHR surface = handle.ToSurface();
                        window.Surface = surface;
                    }
                    Task.Run(async () =>
                    {
                        await window.RunConfigTask();
                        await window.LoadRenderModules();
                    }).ContinueWith((t) => @event.Set());
                    @event.WaitOne();
                    window.OnLoaded(new EventArgs());
                    window.RunWindow(window.Option.UpdateRate);
                }
                finally
                {
                    window.Dispose();
                    @event.Dispose();
                    vulkanManager.RenderWappers.TryRemove(window.Name, out _);
                }

            }, TaskCreationOptions.LongRunning);
        }

        /// <summary>
        /// 初始化所有组件系统
        /// </summary>
        /// <typeparam name="TScene"></typeparam>
        /// <param name="serviceProvider"></param>
        /// <param name="scene"></param>
        /// <param name="systems"></param>
        /// <param name="dispatcher"></param>
        private static void InitSystems<TScene>(IRenderDispatcher dispatcher, IServiceProvider serviceProvider, TScene scene, Type[] systems)
            where TScene : Scene
        {
            if (systems == null) return;
            var systemT = typeof(IComponentSystem);
            foreach (var item in systems)
            {
                if (item.GetInterfaces().Any(x => x == systemT))
                {
                    var system = serviceProvider.CreateAutowiredInstance(item) as IComponentSystem;
                    var eventBus = dispatcher.GetSystemEventBus();
                    if (system is ComponentSystem componentSystem)
                    {
                        componentSystem.Owner = scene;
                        componentSystem.ContextManager = dispatcher.ContextManager;
                        componentSystem.Input = dispatcher.Input;
                        componentSystem.EventBus = eventBus;
                        scene.ComponentSystems.Add(system);
                    }
                }
            }
        }
    }
}
