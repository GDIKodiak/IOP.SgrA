﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using Silk.NET.Vulkan;
using Silk.NET.Vulkan.Extensions.KHR;
using Microsoft.Extensions.Configuration;
using IOP.Extension.DependencyInjection;
using System.Threading.Tasks;
using System.Threading;
using System.Runtime.InteropServices;
using IOP.SgrA.ECS;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// VulkanManager
    /// </summary>
    public class VulkanGraphicsManager : IGraphicsManager
    {
        /// <summary>
        /// 
        /// </summary>
        public const uint VK_SUBPASS_EXTERNAL = uint.MaxValue;
        /// <summary>
        /// 
        /// </summary>
        public const uint VK_ATTACHMENT_UNUSED = uint.MaxValue;
        /// <summary>
        /// 空渲染组
        /// </summary>
        public const string EmptyGroup = "EmptyGroup";

        /// <summary>
        /// 显示API名
        /// </summary>
        public string GraphicsName => "Vulkan";
        /// <summary>
        /// 配置根名
        /// </summary>
        public string ConfigurationRootName { get; set; } = string.Empty;
        /// <summary>
        /// 服务提供者
        /// </summary>
        public IServiceProvider ServiceProvider => _ServiceProvider;
        /// <summary>
        /// 日志
        /// </summary>
        public ILogger<VulkanGraphicsManager> Logger { get; }
        /// <summary>
        /// 配置项
        /// </summary>
        public IConfiguration Configuration { get; }
        /// <summary>
        /// 全局渲染配置项
        /// </summary>
        public GlobalRenderConfigs RenderConfigs { get; } = new GlobalRenderConfigs();
        /// <summary>
        /// 上下文管理器
        /// </summary>
        public VulkanContextManager ContextManager { get; } = new VulkanContextManager();
        /// <summary>
        /// 方法库
        /// </summary>
        public Vk NativeAPI { get; protected internal set; } = Vk.GetApi();
        /// <summary>
        /// Vulkan实例
        /// </summary>
        public VulkanInstance Instance { get; protected internal set; }
        /// <summary>
        /// 物理设备列表
        /// </summary>
        public VulkanPhysicalDevice[] PhysicalDevices { get; protected internal set; }
        /// <summary>
        /// 逻辑设备
        /// </summary>
        public VulkanDevice VulkanDevice { get; protected internal set; }
        /// <summary>
        /// 获取一个空渲染组
        /// </summary>
        public IRenderGroup GetEmptyGroup { get => RenderGroups[EmptyGroup]; }

        /// <summary>
        /// 显示API服务
        /// </summary>
        protected readonly ConcurrentDictionary<Type, IGraphicsManagerService> Services = new ConcurrentDictionary<Type, IGraphicsManagerService>();
        /// <summary>
        /// 着色器信息字典
        /// </summary>
        protected readonly ConcurrentDictionary<string, ShaderInfo> ShaderInfos = new ConcurrentDictionary<string, ShaderInfo>();
        /// <summary>
        /// 纹理
        /// </summary>
        protected readonly ConcurrentDictionary<string, VulkanTexture> Textures = new ConcurrentDictionary<string, VulkanTexture>();
        /// <summary>
        /// 纹理数据
        /// </summary>
        protected readonly ConcurrentDictionary<string, VulkanTextureData> TextureDatas = new ConcurrentDictionary<string, VulkanTextureData>();
        /// <summary>
        /// 管线
        /// </summary>
        protected readonly ConcurrentDictionary<string, VulkanPipeline> Pipelines = new ConcurrentDictionary<string, VulkanPipeline>();
        /// <summary>
        /// 渲染组集合
        /// </summary>
        protected readonly ConcurrentDictionary<string, IRenderGroup> RenderGroups = new ConcurrentDictionary<string, IRenderGroup>();
        /// <summary>
        /// 渲染对象
        /// </summary>
        protected readonly ConcurrentDictionary<string, IRenderObject> RenderObjects = new ConcurrentDictionary<string, IRenderObject>();
        /// <summary>
        /// 文字
        /// </summary>
        protected readonly ConcurrentDictionary<string, IFont> Fonts = new ConcurrentDictionary<string, IFont>();
        /// <summary>
        /// 网格数据
        /// </summary>
        protected readonly ConcurrentDictionary<string, VRO> Meshes = new ConcurrentDictionary<string, VRO>();
        /// <summary>
        /// 摄影机
        /// </summary>
        protected readonly ConcurrentDictionary<string, Camera> Cameras = new ConcurrentDictionary<string, Camera>();
        /// <summary>
        /// 渲染包装对象字典
        /// </summary>
        internal protected readonly ConcurrentDictionary<string, VulkanRenderWapper> RenderWappers = new ConcurrentDictionary<string, VulkanRenderWapper>();
        /// <summary>
        /// 全局渲染器限制
        /// </summary>
        internal protected readonly GlobalRenderLimit RenderLimit = new();
        /// <summary>
        /// 是否销毁资源
        /// </summary>
        private volatile bool IsDispose = false;
        private IServiceProvider _ServiceProvider = null;
        private int Id = 0;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="serviceProvider"></param>
        public VulkanGraphicsManager(IServiceProvider serviceProvider)
        {
            _ServiceProvider = serviceProvider;
            var loggerF = serviceProvider.GetRequiredService<ILoggerFactory>();
            Logger = loggerF.CreateLogger<VulkanGraphicsManager>();
            Configuration = serviceProvider.GetRequiredService<IConfiguration>();
            RenderGroups.TryAdd(EmptyGroup, new EmptyVulkanRenderGroup(EmptyGroup, serviceProvider, null, GroupRenderMode.Core));
            RenderConfigs.PropertyChanged += RenderConfigs_PropertyChanged;
        }

        /// <summary>
        /// 创建窗口
        /// </summary>
        /// <typeparam name="TWindow"></typeparam>
        /// <param name="name"></param>
        /// <param name="option"></param>
        /// <returns></returns>
        public TWindow CreateWindow<TWindow>(string name, WindowOption option)
            where TWindow : class, IGenericWindow
        {
            if (string.IsNullOrEmpty(name)) name = Guid.NewGuid().ToString("N");
            if (RenderWappers.ContainsKey(name)) throw new ArgumentException($"The window named {name} has already used");
            var window = ServiceProvider.CreateAutowiredInstance<TWindow>();
            if (window is VulkanGlfwWindow glfw)
            {
                glfw.Option = option ?? throw new ArgumentNullException(nameof(option));
                glfw.Name = name;
                RenderWappers.AddOrUpdate(name, glfw, (key, value) => glfw);
                return window;
            }
            else throw new NotSupportedException("Not Supported window type");
        }
        /// <summary>
        /// 摧毁窗口
        /// </summary>
        /// <param name="window"></param>
        public void DestroyWindow(IGenericWindow window)
        {
            if(RenderWappers.TryRemove(window.Name, out VulkanRenderWapper wapper))
            {
                if(Instance.RawHandle.Handle != IntPtr.Zero)
                {
                    if (NativeAPI.TryGetInstanceExtension(Instance.RawHandle, out KhrSurface khrSurface))
                    {
                        unsafe
                        {
                            wapper.RenderDispatcher.DestroyDispatcher();
                            var surface = wapper.GetVulkanSurface();
                            if(surface.Handle != 0)
                            {
                                khrSurface.DestroySurface(Instance.RawHandle, surface, null);
                            }
                        }
                    }
                }
            }
        }
        /// <summary>
        /// 构建一个渲染包装器
        /// </summary>
        /// <typeparam name="TWapper"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public TWapper CreateRenderWapper<TWapper>(string name)
            where TWapper : VulkanRenderWapper
        {
            if (string.IsNullOrEmpty(name))
            {
                name = Guid.NewGuid().ToString("N");
                while (RenderWappers.ContainsKey(name))
                {
                    name = Guid.NewGuid().ToString("N");
                }
            }
            if (RenderWappers.ContainsKey(name)) throw new ArgumentException($"The window named {name} has already used");
            TWapper wapper = ServiceProvider.CreateAutowiredInstance<TWapper>();
            wapper.Identifier = name;
            RenderWappers.AddOrUpdate(name, wapper, (key, value) => wapper);
            return wapper;
        }
        /// <summary>
        /// 获取上下文管理器
        /// </summary>
        public IContextManager GetContextManager() => ContextManager;

        /// <summary>
        /// 加载着色器脚本
        /// </summary>
        /// <param name="shaders"></param>
        public void LoadShaders(ShaderInfo[] shaders)
        {
            List<Task> tasks = new List<Task>(shaders.Length);
            foreach (var item in shaders)
            {
                var t = Task.Run(() =>
                {
                    item.LoadShader();
                    ShaderInfos.AddOrUpdate(item.ShaderName, item, (key, value) => value);
                });
                tasks.Add(t);
            }
            Task.WaitAll(tasks.ToArray());
        }
        /// <summary>
        /// 加载着色器脚本
        /// </summary>
        /// <param name="shaders"></param>
        /// <returns></returns>
        public async Task LoadShadersAsync(ShaderInfo[] shaders)
        {
            List<Task> tasks = new List<Task>(shaders.Length);
            foreach (var item in shaders)
            {
                var t = Task.Run(() =>
                {
                    item.LoadShader();
                    ShaderInfos.AddOrUpdate(item.ShaderName, item, (key, value) => value);
                });
                tasks.Add(t);
            }
            await Task.WhenAll(tasks);
        }
        /// <summary>
        /// 删除着色器脚本
        /// </summary>
        /// <param name="shaders"></param>
        public void DeleteShaders(ShaderInfo[] shaders)
        {
            foreach (var item in shaders)
            {
                ShaderInfos.TryRemove(item.ShaderName, out ShaderInfo info);
                if (info != null) info.Dispose();
            }
        }

        /// <summary>
        /// 创建一个空的纹理数据
        /// </summary>
        /// <typeparam name="CTextureData"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public CTextureData CreateEmptyTextureData<CTextureData>(string name)
            where CTextureData : VulkanTextureData, new()
        {
            if (string.IsNullOrEmpty(name)) name = Guid.NewGuid().ToString("N");
            CTextureData texture = new CTextureData
            {
                Name = name
            };
            TextureDatas.AddOrUpdate(name, texture, (key, value) => texture);
            return texture;
        }
        /// <summary>
        /// 获取纹理数据
        /// </summary>
        /// <param name="name"></param>
        /// <param name="textureData"></param>
        /// <returns></returns>
        public bool TryGetTextureData(string name, out VulkanTextureData textureData) => TextureDatas.TryGetValue(name, out textureData);
        /// <summary>
        /// 创建纹理
        /// </summary>
        /// <param name="name"></param>
        /// <param name="textureData"></param>
        /// <param name="sampler"></param>
        /// <param name="binding"></param>
        /// <param name="arrayElement"></param>
        /// <param name="descriptorCount"></param>
        /// <param name="setIndex"></param>
        /// <param name="descriptorType"></param>
        /// <returns></returns>
        public VulkanTexture CreateTexture(string name, VulkanTextureData textureData,
            VulkanSampler sampler, uint binding = 0, uint arrayElement = 0, uint descriptorCount = 1, 
            uint setIndex = 0, DescriptorType descriptorType = DescriptorType.CombinedImageSampler)
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));
            if (Textures.ContainsKey(name)) throw new Exception($"The name: {name} has already used");
            if (textureData == null) throw new ArgumentNullException(nameof(textureData));
            if (descriptorType == DescriptorType.UniformBufferDynamic || descriptorType == DescriptorType.StorageBufferDynamic)
            {
                if (textureData is not VulkanDynamicBufferData data) throw new InvalidOperationException("Create dynamic buffer must be use VulkanDynamicBufferData");
                return CreateDynamicBuffer(name, data, binding, arrayElement, descriptorCount, setIndex, descriptorType);
            }
            var tex = new VulkanTexture(name, sampler, textureData, binding,
                descriptorCount, arrayElement, setIndex, descriptorType);
            tex.Device = VulkanDevice;
            tex.GraphicsManager = this;
            Textures.AddOrUpdate(name, tex, (key, value) => tex);
            return tex;
        }
        /// <summary>
        /// 创建动态缓冲
        /// </summary>
        /// <param name="name"></param>
        /// <param name="textureData"></param>
        /// <param name="binding"></param>
        /// <param name="arrayElement"></param>
        /// <param name="descriptorCount"></param>
        /// <param name="setIndex"></param>
        /// <param name="descriptorType"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="Exception"></exception>
        /// <exception cref="InvalidOperationException"></exception>
        public VulkanDynamicBuffer CreateDynamicBuffer(string name, VulkanDynamicBufferData textureData, 
            uint binding = 0, uint arrayElement = 0, uint descriptorCount = 1,
            uint setIndex = 0, DescriptorType descriptorType = DescriptorType.UniformBufferDynamic)
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));
            if (Textures.ContainsKey(name)) throw new Exception($"The name: {name} has already used");
            if (textureData == null) throw new ArgumentNullException(nameof(textureData));
            if (!(descriptorType == DescriptorType.UniformBufferDynamic || descriptorType == DescriptorType.StorageBufferDynamic))
                throw new InvalidOperationException("Create dynamic buffer must be use UniformBufferDynamic or StorageBufferDynamic flag");
            var tex = new VulkanDynamicBuffer(this, name, textureData, binding, descriptorCount, arrayElement, setIndex, descriptorType);
            tex.Device = VulkanDevice;
            tex.GraphicsManager = this;
            Textures.AddOrUpdate(name, tex, (key, value) => tex);
            return tex;
        }
        /// <summary>
        /// 获取纹理
        /// </summary>
        /// <param name="name"></param>
        /// <param name="texture"></param>
        /// <returns></returns>
        public bool TryGetTexture(string name, out VulkanTexture texture) => Textures.TryGetValue(name, out texture);
        /// <summary>
        /// 是否包含纹理
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool IsContainsTexture(string name) => !string.IsNullOrEmpty(name) && Textures.ContainsKey(name);
        /// <summary>
        /// 释放所有纹理数据
        /// </summary>
        public void FreeAllVulkanTextureDatas()
        {
            foreach (var item in TextureDatas)
            {
                item.Value.Dispose();
            }
            Textures.Clear();
        }
        /// <summary>
        /// 摧毁指定纹理数据
        /// </summary>
        /// <param name="textureData"></param>
        public void DestroyTextureData(VulkanTextureData textureData)
        {
            if(TextureDatas.TryRemove(textureData.Name, out var tex))
            {
                tex.Dispose();
            }
            else textureData.Dispose();
        }

        /// <summary>
        /// 创建新的管线
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public VulkanPipeline CreateEmptyPipeline(string name)
        {
            if(Pipelines.ContainsKey(name)) throw new Exception($"Name {name} with pipeline is already used");
            VulkanPipeline pipeline = new VulkanPipeline(name, GetNewId(), VulkanDevice, Configuration);
            pipeline.VulkanManager = this;
            Pipelines.AddOrUpdate(name, pipeline, (key, value) => pipeline);
            return pipeline;
        }

        /// <summary>
        /// 添加渲染组
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public bool AddRenderGroup(IRenderGroup group) => RenderGroups.TryAdd(group.Name, group);
        /// <summary>
        /// 是否包含渲染组
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool IsContainsGroup(string name) => RenderGroups.ContainsKey(name);
        /// <summary>
        /// 尝试获取渲染组
        /// </summary>
        /// <param name="name"></param>
        /// <param name="group"></param>
        /// <returns></returns>
        public bool TryGetRenderGroup(string name, out IRenderGroup group) => RenderGroups.TryGetValue(name, out group);

        /// <summary>
        /// 创建上下文
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="scene"></param>
        /// <param name="size"></param>
        /// <param name="archetypeName"></param>
        /// <param name="groupName"></param>
        /// <param name="renderObject"></param>
        /// <param name="initData"></param>
        /// <param name="deepCopy"></param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="NullReferenceException"></exception>
        public void CreateContext<TGroup>(Scene scene, int size, string archetypeName, string groupName, IRenderObject renderObject, TGroup initData, bool deepCopy = false)
            where TGroup : struct
        {
            if(scene == null) throw new ArgumentNullException(nameof(scene));
            if (string.IsNullOrEmpty(archetypeName)) archetypeName = string.Empty;
            if(string.IsNullOrEmpty(groupName)) groupName = string.Empty;
            var contextManager = scene.ContextManager;
            if (!TryGetRenderGroup(groupName, out IRenderGroup g)) throw new NullReferenceException($"Cannot got render group with name {groupName}");
            if (!contextManager.TryGetArchetype(archetypeName, out Archetype archetype)) throw new NullReferenceException($"Cannot got archetype with name {archetypeName}");
            contextManager.CreateContexts(size, archetype, g, renderObject, initData, deepCopy);
            scene.Insert(renderObject);
        }
        /// <summary>
        /// 创建上下文
        /// </summary>
        /// <param name="scene"></param>
        /// <param name="size"></param>
        /// <param name="archetypeName"></param>
        /// <param name="groupName"></param>
        /// <param name="renderObject"></param>
        /// <param name="deepCopy"></param>
        public void CreateContext(Scene scene, int size, string archetypeName, string groupName, IRenderObject renderObject, bool deepCopy = false)
        {
            if (scene == null) throw new ArgumentNullException(nameof(scene));
            if (string.IsNullOrEmpty(archetypeName)) archetypeName = string.Empty;
            if (string.IsNullOrEmpty(groupName)) groupName = string.Empty;
            var contextManager = scene.ContextManager;
            if (!TryGetRenderGroup(groupName, out IRenderGroup g)) throw new NullReferenceException($"Cannot got render group with name {groupName}");
            if (!contextManager.TryGetArchetype(archetypeName, out Archetype archetype)) throw new NullReferenceException($"Cannot got archetype with name {archetypeName}");
            contextManager.CreateContexts(size, archetype, g, renderObject, deepCopy);
            scene.Insert(renderObject);
        }

        /// <summary>
        /// 创建渲染对象
        /// </summary>
        /// <typeparam name="TRenderObject"></typeparam>
        /// <param name="name"></param>
        /// <param name="components"></param>
        /// <returns></returns>
        public TRenderObject CreateRenderObject<TRenderObject>(string name, params IRenderComponent[] components)
            where TRenderObject : RenderObject, new()
        {
            if (string.IsNullOrEmpty(name)) name = Guid.NewGuid().ToString("N");
            var id = GetNewId();
            if (RenderObjects.ContainsKey(name)) throw new ArgumentException($"RenderObject with name {name} was already exists");
            TRenderObject render = RenderObject.CreateRenderObject<TRenderObject>(name);
            RenderObjects.AddOrUpdate(name, render, (key, value) => value);
            if(components != null)
            {
                foreach (var item in components) render.AddComponent(item);
            }
            return render;
        }
        /// <summary>
        /// 尝试获取渲染对象
        /// </summary>
        /// <typeparam name="TRenderObject"></typeparam>
        /// <param name="name"></param>
        /// <param name="renderObject"></param>
        /// <returns></returns>
        public bool TryGetRenderObject<TRenderObject>(string name, out TRenderObject renderObject)
            where TRenderObject : RenderObject, new()
        {
            if(RenderObjects.TryGetValue(name, out IRenderObject render))
            {
                if(render is TRenderObject tRender)
                {
                    renderObject = tRender;
                    return true;
                }
            }
            renderObject = default;
            return false;
        }
        /// <summary>
        /// 移除渲染对象
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool RemoveRenderObject(string name) => RenderObjects.TryRemove(name, out _);

        /// <summary>
        /// 创建一个动画并附加至一个渲染对象
        /// </summary>
        /// <typeparam name="TRenderObject"></typeparam>
        /// <typeparam name="TAnimation"></typeparam>
        /// <param name="object"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public TRenderObject CreateAnimation<TRenderObject, TAnimation>(TRenderObject @object, string name)
            where TRenderObject : IRenderObject
            where TAnimation : Animation
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException("name with component cannot be null");
            TAnimation animation = Activator.CreateInstance<TAnimation>();
            animation.Name = name;
            @object.AddComponent(animation);
            animation.OnAttach(@object);
            return @object;
        }
        /// <summary>
        /// 创建一个粒子发生器并附加到一个渲染对象中
        /// </summary>
        /// <typeparam name="TRenderObject"></typeparam>
        /// <typeparam name="TParticle"></typeparam>
        /// <param name="object"></param>
        /// <param name="contextManager"></param>
        /// <param name="particleObject"></param>
        /// <param name="renderGroup"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public TRenderObject CreateParticleGenerator<TRenderObject, TParticle>(TRenderObject @object, string name, IContextManager contextManager, IRenderObject particleObject, IRenderGroup renderGroup)
            where TRenderObject : IRenderObject
            where TParticle : ParticleGenerator
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException("name with component cannot be null");
            object particle = Activator.CreateInstance(typeof(TParticle), new object[] { contextManager, particleObject, renderGroup });
            TParticle p = particle as TParticle;
            p.Name = name;
            @object.AddComponent(p);
            p.OnAttach(@object);
            return @object;
        }
        /// <summary>
        /// 创建一个粒子发生器并附加到一个空的渲染对象上
        /// </summary>
        /// <typeparam name="TRenderObject"></typeparam>
        /// <typeparam name="TParticle"></typeparam>
        /// <param name="contextManager"></param>
        /// <param name="particleObject"></param>
        /// <param name="renderGroup"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public TRenderObject CreateParticleGenerator<TRenderObject, TParticle>(string name, IContextManager contextManager, IRenderObject particleObject, IRenderGroup renderGroup)
            where TRenderObject : RenderObject, new()
            where TParticle : ParticleGenerator
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException("name with component cannot be null");
            object particle = Activator.CreateInstance(typeof(TParticle), new object[] { contextManager, particleObject, renderGroup });
            TParticle p = particle as TParticle;
            p.Name = name;
            var obj = CreateRenderObject<TRenderObject>(name);
            obj.AddComponent(p);
            p.OnAttach(obj);
            return obj;
        }

        /// <summary>
        /// 创建一个空的网格
        /// </summary>
        /// <typeparam name="TMesh"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public TMesh CreateEmptyMesh<TMesh>(string name)
            where TMesh : VRO, new()
        {
            if (string.IsNullOrEmpty(name)) name = Guid.NewGuid().ToString("N");
            var id = GetNewId();
            if (RenderObjects.ContainsKey(name)) name += id;
            TMesh mesh = new TMesh
            {
                Name = name,
                Id = id
            };
            Meshes.AddOrUpdate(name, mesh, (key, value) => mesh);
            return mesh;
        }
        /// <summary>
        /// 尝试获取节点
        /// </summary>
        /// <typeparam name="TMesh"></typeparam>
        /// <param name="name"></param>
        /// <param name="mesh"></param>
        /// <returns></returns>
        public bool TryGetMesh<TMesh>(string name, out TMesh mesh)
            where TMesh : VRO
        {
            if(Meshes.TryGetValue(name, out VRO v))
            {
                if (v is TMesh t)
                {
                    mesh = t;
                    return true;
                }
            }
            mesh = default;
            return false;
        }
        /// <summary>
        /// 创建网格组件
        /// </summary>
        /// <param name="name"></param>
        /// <param name="data"></param>
        /// <param name="vectexCount"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public IMesh CreateMesh(string name, Vector3[] data, uint vectexCount, Vector3 min, Vector3 max)
        {
            if(string.IsNullOrEmpty(name)) name = NewStringToken();
            VRO mesh = CreateEmptyMesh<VRO>(name);
            if(data != null && data.Length > 0)
            {
                Span<float> d = MemoryMarshal.Cast<Vector3, float>(data);
                mesh.CreateVecticesBuffer(d, vectexCount, VulkanDevice, SharingMode.Exclusive);
            }
            mesh.MinVector = min; mesh.MaxVector = max;
            return mesh;
        }
        /// <summary>
        /// 创建索引缓冲
        /// </summary>
        /// <param name="name"></param>
        /// <param name="data"></param>
        /// <param name="indexed"></param>
        /// <param name="vectexCount"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public IMesh CreateIndexedMesh(string name, Vector3[] data, uint[] indexed, uint vectexCount, Vector3 min, Vector3 max)
        {
            if (string.IsNullOrEmpty(name)) name = NewStringToken();
            VRO mesh = CreateEmptyMesh<VRO>(name);
            if (data != null && data.Length > 0)
            {
                Span<float> d = MemoryMarshal.Cast<Vector3, float>(data);
                mesh.CreateVecticesBuffer(d, vectexCount, VulkanDevice, SharingMode.Exclusive);
            }
            if(indexed != null && indexed.Length > 0)
            {
                mesh.CreateIndexesBuffer(indexed, VulkanDevice, SharingMode.Exclusive);
            }
            mesh.MinVector = min; mesh.MaxVector = max;
            return mesh;
        }
        /// <summary>
        /// 创建网格组件
        /// </summary>
        /// <typeparam name="TV"></typeparam>
        /// <param name="name"></param>
        /// <param name="data"></param>
        /// <param name="vertexCount"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public IMesh CreateMesh<TV>(string name, TV[] data, uint vertexCount, Vector3 min, Vector3 max)
            where TV : unmanaged
        {
            if (string.IsNullOrEmpty(name)) name = Guid.NewGuid().ToString("N");
            VRO mesh = CreateEmptyMesh<VRO>(name);
            if (data != null && data.Length > 0)
            {
                Span<float> d = MemoryMarshal.Cast<TV, float>(data);
                mesh.CreateVecticesBuffer(d, vertexCount, VulkanDevice, SharingMode.Exclusive);
                mesh.MinVector = min; mesh.MaxVector = max;
            }
            return mesh;
        }

        /// <summary>
        /// 创建一致性缓冲
        /// </summary>
        /// <param name="size"></param>
        /// <param name="binding"></param>
        /// <param name="arrayElement"></param>
        /// <param name="descriptorCount"></param>
        /// <param name="setIndex"></param>
        /// <returns></returns>
        public ITexture CreateUniformBuffer(ulong size, uint binding = 0, uint arrayElement = 0,
           uint descriptorCount = 1, uint setIndex = 0)
        {
            return this.CreateUniformBufferTexture(Guid.NewGuid().ToString("N"),
                size, binding, arrayElement, descriptorCount, setIndex);
        }

        /// <summary>
        /// 创建摄影机
        /// </summary>
        /// <param name="name"></param>
        /// <param name="position"></param>
        /// <param name="target"></param>
        /// <param name="up"></param>
        /// <returns></returns>
        public Camera CreateCamera(string name, Vector3 position, Vector3 target, Vector3 up)
        {
            if (Cameras.ContainsKey(name)) throw new InvalidOperationException($"the name {name} with camera has already used");
            Camera camera = new Camera(position, target, up);
            Cameras.AddOrUpdate(name, camera, (key, value) => camera);
            return camera;
        }
        /// <summary>
        /// 尝试获取摄影机
        /// </summary>
        /// <param name="name"></param>
        /// <param name="camera"></param>
        /// <returns></returns>
        public bool TryGetCamera(string name, out Camera camera) => Cameras.TryGetValue(name, out camera);
        /// <summary>
        /// 遍历摄影机
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Camera> GetCameras()
        {
            var cameras = Cameras.ToArray();
            foreach (var item in cameras) yield return item.Value;
        }

        /// <summary>
        /// 添加字体
        /// </summary>
        /// <param name="font"></param>
        public void AddFont(IFont font)
        {
            if (Fonts.ContainsKey(font.Name)) throw new ArgumentException($"Font with name {font.Name} is already used");
            Fonts.AddOrUpdate(font.Name, font, (key, value) => value);
        }
        /// <summary>
        /// 尝试获取字体
        /// </summary>
        /// <typeparam name="TFont"></typeparam>
        /// <param name="name"></param>
        /// <param name="font"></param>
        /// <returns></returns>
        public bool TryGetFont<TFont>(string name, out TFont font)
             where TFont : IFont
        {
            if(Fonts.TryGetValue(name, out IFont f))
            {
                if (f is TFont tf)
                {
                    font = tf;
                    return true;
                }
            }
            font = default;
            return false;
        }

        /// <summary>
        /// 获取显示API服务
        /// </summary>
        /// <typeparam name="TService"></typeparam>
        /// <returns></returns>
        public TService GetGraphicsManagerService<TService>() 
            where TService : IGraphicsManagerService
        {
            var type = typeof(TService);
            if(Services.TryGetValue(type, out IGraphicsManagerService service))
            {
                if (service is TService tService) return tService;
            }
            throw new InvalidCastException("Unknow Service");
        }

        /// <summary>
        /// 查询物理显卡属性限制
        /// </summary>
        public void QueryPhysicsPropertiesLimit(VulkanPhysicalDevice physical)
        {
            var p = physical.GetDeviceProperties();
            QueryFramebufferSampleCounts(p);
        }
        /// <summary>
        /// 创建全局渲染器配置项
        /// </summary>
        public void CreateDefaultGlobalRenderConfig()
        {
            SetDefaultFramebufferSampleCounts();
        }

        /// <summary>
        /// 返回新的Id
        /// </summary>
        /// <returns></returns>
        public int GetNewId()
        {
            return Interlocked.Increment(ref Id) & int.MaxValue;
        }
        /// <summary>
        /// 返回一个新的字符串令牌
        /// </summary>
        /// <returns></returns>
        public string NewStringToken() => $"{Guid.NewGuid():N}{GetNewId()}";
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public (bool, string) ManagerCheck()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 销毁资源
        /// </summary>
        public void Dispose()
        {
            if (IsDispose) return;
            IsDispose = true;
            foreach (var item in RenderWappers)
            {
                item.Value.RenderDispatcher?.Stop();
                item.Value.RenderDispatcher?.DestroyDispatcher();
            }
            RenderWappers.Clear();
            Textures.Clear();
            foreach (var item in Pipelines)
            {
                item.Value?.Dispose();
            }
            VulkanDevice.Dispose();
            Instance.Dispose();
        }


        private void RenderConfigs_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch(e.PropertyName)
            {
                case nameof(RenderConfigs.Multisampling):
                    var max = RenderLimit.MaxCampleCount;
                    var c = RenderConfigs.Multisampling;
                    if (c > max) RenderConfigs.Multisampling = max;
                    else if (c == SampleCount.Sample1X) RenderConfigs.Multisampling = SampleCount.Sample2X;
                    break;
                default:
                    break;
            }
        }

        private void QueryFramebufferSampleCounts(PhysicalDeviceProperties p)
        {
            var samplecount = p.Limits.FramebufferColorSampleCounts &
                p.Limits.FramebufferDepthSampleCounts;
            if ((samplecount & SampleCountFlags.Count64Bit) > 0) RenderLimit.MaxCampleCount = SampleCount.Sample64X;
            else if ((samplecount & SampleCountFlags.Count32Bit) > 0) RenderLimit.MaxCampleCount = SampleCount.Sample32X;
            else if ((samplecount & SampleCountFlags.Count16Bit) > 0) RenderLimit.MaxCampleCount = SampleCount.Sample16X;
            else if ((samplecount & SampleCountFlags.Count8Bit) > 0) RenderLimit.MaxCampleCount = SampleCount.Sample8X;
            else if ((samplecount & SampleCountFlags.Count4Bit) > 0) RenderLimit.MaxCampleCount = SampleCount.Sample4X;
            else if ((samplecount & SampleCountFlags.Count2Bit) > 0) RenderLimit.MaxCampleCount = SampleCount.Sample2X;
            else RenderLimit.MaxCampleCount = SampleCount.Sample1X;
        }
        private void SetDefaultFramebufferSampleCounts()
        {
            var max = RenderLimit.MaxCampleCount;
            if (max >= SampleCount.Sample2X) max = SampleCount.Sample2X;
            else max = RenderLimit.MaxCampleCount;
            RenderConfigs.Multisampling = max;
        }
    }
}
