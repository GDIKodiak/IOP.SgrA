﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class VulkanOperationException : Exception
    {
        /// <summary>
        /// 
        /// </summary>
        public string OperationName { get; }
        /// <summary>
        /// 
        /// </summary>
        public Result Result { get; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationName"></param>
        /// <param name="result"></param>
        public VulkanOperationException(string operationName, Result result)
            :base(CreateMessage(operationName, result))
        {
            OperationName = operationName;
            Result = result;
        }

        private static string CreateMessage(string operationName, Result result) => $"Running operation {operationName} failed, because of {result}";
    }
}
