﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 内存段
    /// </summary>
    public class VulkanMemorySegment
    {
        /// <summary>
        /// 段下标
        /// </summary>
        public int Index { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public VulkanDeviceMemory Memory { get; private set; }
        /// <summary>
        /// 总偏移
        /// </summary>
        public ulong Offset { get; private set; }
        /// <summary>
        /// 大小
        /// </summary>
        public ulong Size { get; private set; }
        /// <summary>
        /// 内存属性
        /// </summary>
        public MemoryPropertyFlags PropertyFlags { get => Memory.PropertyFlags; }
        /// <summary>
        /// 块字典
        /// </summary>
        public ConcurrentDictionary<long, VulkanMemoryBlock> Blocks { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        protected readonly object SyncRoot = new();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="memory"></param>
        /// <param name="offset"></param>
        /// <param name="size"></param>
        public VulkanMemorySegment(int index, VulkanDeviceMemory memory, ulong offset, ulong size)
        {
            Index = index;
            Memory = memory ?? throw new ArgumentNullException(nameof(memory));
            Offset = offset;
            Size = size;
            Blocks = new ConcurrentDictionary<long, VulkanMemoryBlock>();
            VulkanMemoryBlock d = new(this, VulkanMemoryManager.NewKey(), 0, size);
            Blocks.AddOrUpdate(d.Key, d, (key, value) => d);
        }
        /// <summary>
        /// 
        /// </summary>
        public VulkanMemorySegment(int index, ulong offset, ulong size) 
        {
            Index = index;
            Offset = offset;
            Size = size;
            Blocks = new ConcurrentDictionary<long, VulkanMemoryBlock>();
            VulkanMemoryBlock d = new(this, VulkanMemoryManager.NewKey(), offset, size);
            Blocks.AddOrUpdate(d.Key, d, (key, value) => d);
        }
        /// <summary>
        /// 更新内存数据
        /// </summary>
        /// <param name="data"></param>
        /// <param name="localOffset">本地段偏移</param>
        public void UpdateMemoryData(Span<byte> data, ulong localOffset)
        {
            if (PropertyFlags.HasFlag(MemoryPropertyFlags.DeviceLocalBit))
                throw new InvalidOperationException("Only Host memory could update memory data");
            ulong size = Size; ulong start = localOffset;
            if (start > Offset || (start + (ulong)data.Length) >= size) 
                throw new ArgumentOutOfRangeException($"parameter {nameof(localOffset)} is out of segment offset");
            Memory.UpdateMemory(data, start);
        }
        /// <summary>
        /// 更新内存数据
        /// </summary>
        /// <param name="data"></param>
        /// <param name="localOffset"></param>
        public void UpdateMemoryData(byte[] data, ulong localOffset)
        {
            Span<byte> buffer = data;
            UpdateMemoryData(buffer, localOffset);
        }
        /// <summary>
        /// 回收内存段
        /// </summary>
        public void Recovery(VulkanMemoryBlock block)
        {
            if (block.Owner != this || block == null) return;
            lock (SyncRoot)
            {
                block.Flags = -1;
                VulkanMemoryBlock m = block.Merge(out VulkanMemoryBlock prev, out VulkanMemoryBlock next);
                while (prev != null || next != null)
                {
                    if (prev != null)
                    {
                        Blocks.TryRemove(prev.Key, out _);
                        prev.Owner = null;
                    }
                    if (next != null)
                    {
                        Blocks.TryRemove(next.Key, out _);
                        next.Owner = null;
                    }
                    if (block.Key != m.Key)
                    {
                        Blocks.TryRemove(block.Key, out _);
                        block.Owner = null;
                        block = m;
                    }
                    m = block.Merge(out prev, out next);
                }
                Blocks.AddOrUpdate(m.Key, m, (key, value) => m);
            }
        }
        /// <summary>
        /// 尝试分配内存
        /// </summary>
        /// <param name="block"></param>
        /// <param name="requirements"></param>
        /// <returns></returns>
        public bool TryMalloc(out VulkanMemoryBlock block, MemoryRequirements requirements)
        {
            bool found = false; block = null;
            ulong size = requirements.Size; ulong mod = requirements.Size % requirements.Alignment;
            if(mod != 0)
            {
                size = ((requirements.Size / requirements.Alignment) + 1) * requirements.Alignment;
            }
            foreach (var item in Blocks.Values)
            {
                var start = item.Offset; ulong m = item.Offset % requirements.Alignment;
                var avaSize = item.Size; ulong zOffset = 0;
                if(m != 0)
                {
                    start = ((item.Offset / requirements.Alignment) + 1) * requirements.Alignment;
                    zOffset = start - item.Offset;
                    avaSize = item.Size - zOffset;
                }
                if (avaSize >= size && item.Flags < 0)
                {
                    found = true;
                    lock (SyncRoot)
                    {
                        var r = item.Division(zOffset, size);
                        Blocks.AddOrUpdate(r.Key, r, (key, value) => r);
                        block = r;
                        r.Flags = 1;
                    }
                    break;
                }
            }
            return found;
        }
    }
}
