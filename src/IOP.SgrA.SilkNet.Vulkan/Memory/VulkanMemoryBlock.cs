﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class VulkanMemoryBlock
    {
        /// <summary>
        /// 
        /// </summary>
        public long Key { get; private set; }
        /// <summary>
        /// 段偏移
        /// </summary>
        public ulong Offset { get; private set; }
        /// <summary>
        /// 大小
        /// </summary>
        public ulong Size { get; private set; }
        /// <summary>
        /// 所在段
        /// </summary>
        public VulkanMemorySegment Owner { get; protected internal set; }
        /// <summary>
        /// 标志位
        /// </summary>
        public int Flags { get; internal protected set; }
        /// <summary>
        /// 
        /// </summary>
        public VulkanMemoryBlock Next { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public VulkanMemoryBlock Prev { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="segment"></param>
        /// <param name="key"></param>
        /// <param name="offset"></param>
        /// <param name="size"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public VulkanMemoryBlock(VulkanMemorySegment segment, long key, ulong offset, ulong size)
        {
            Owner = segment ?? throw new ArgumentNullException(nameof(segment));
            if (offset < 0 || size <= 0) throw new ArgumentException("Invalid parameter");
            Key = key;
            Offset = offset;
            Size = size;
            Flags = -1;
        }

        /// <summary>
        /// 将一块内存切分为两块内存
        /// </summary>
        /// <param name="localOffset">块内局部偏移</param>
        /// <returns></returns>
        public VulkanMemoryBlock DivisionFromOffset(ulong localOffset)
        {
            ulong oldSize = Size;
            if (localOffset >= oldSize) throw new ArgumentOutOfRangeException("target block was not enough to division");
            if (Flags <= 0) throw new InvalidOperationException("Target memory was already used");
            if (localOffset == 0) return this;
            ulong newSize = oldSize - localOffset;
            VulkanMemoryBlock block = new(Owner, VulkanMemoryManager.NewKey(), Offset + localOffset, newSize);
            block.Prev = this;
            block.Next = Next;
            Next = block;
            Size = newSize;
            return block;
        }
        /// <summary>
        /// 将一块内存切分为两块内存
        /// 从当前块零点位置开始
        /// </summary>
        /// <param name="size">大小</param>
        /// <returns></returns>
        public VulkanMemoryBlock DivisionFromStart(ulong size)
        {
            ulong oldSize = Size;
            if (oldSize == size) return this;
            if (size > oldSize) throw new ArgumentOutOfRangeException(nameof(size), "target block was not enough to division");
            if (Flags >= 0) throw new InvalidOperationException("Target memory was already used");
            if (size == 0) throw new InvalidOperationException("Division size must be greater than 0");
            VulkanMemoryBlock block = new(Owner, VulkanMemoryManager.NewKey(), Offset, size);
            block.Prev = Prev;
            block.Next = this;
            Prev = block;
            Offset += size;
            Size = oldSize - size;
            return block;
        }
        /// <summary>
        /// 从指定位置和大小切分内存
        /// </summary>
        /// <param name="start"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public VulkanMemoryBlock Division(ulong start, ulong size)
        {
            if (size == 0) throw new InvalidOperationException("Division size must be greater than 0");
            if (Flags >= 0) throw new InvalidOperationException("Target memory was already used");
            ulong end = start + size; ulong oldSize = Size;
            if (end > oldSize) throw new ArgumentOutOfRangeException(nameof(size), "target block was not enough to division");
            if(start != 0) DivisionFromStart(start);
            return DivisionFromStart(size);
        }
        /// <summary>
        /// 合并当前内存块周边内存
        /// </summary>
        /// <param name="prev"></param>
        /// <param name="next"></param>
        /// <returns></returns>
        public VulkanMemoryBlock Merge(out VulkanMemoryBlock prev, out VulkanMemoryBlock next)
        {
            prev = Prev; next = Next;
            if (prev != null && prev.Flags >= 0) prev = null;
            if (next != null && next.Flags >= 0) next = null;
            if (prev == null && next == null) return this;
            ulong offset = Offset; ulong size = Size;
            VulkanMemoryBlock nP = prev; VulkanMemoryBlock nn = next;
            if(prev != null)
            {
                offset = prev.Offset;
                size += prev.Size;
                nP = prev.Prev;
            }
            if(next != null)
            {
                size += next.Size;
                nn = next.Next;
            }
            VulkanMemoryBlock result = new(Owner, VulkanMemoryManager.NewKey(), offset, size);
            result.Prev = nP; result.Next = nn;
            return result;
        }
    }
}
