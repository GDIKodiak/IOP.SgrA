﻿using Silk.NET.Vulkan;
using System;
using System.Buffers;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// Vulkan内存管理器
    /// </summary>
    public class VulkanMemoryManager
    {
        private static long _Key;
        private static readonly object _SyncRoot = new();
        private static VulkanMemoryManager _Current;

        /// <summary>
        /// 段列表
        /// </summary>
        protected readonly ConcurrentDictionary<int, List<VulkanMemorySegment>> Segents = new();
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static long NewKey() => Interlocked.Increment(ref _Key);
        /// <summary>
        /// 逻辑设备
        /// </summary>
        public VulkanDevice Device { get; private set; }
        /// <summary>
        /// 段最小字节数
        /// </summary>
        public ulong MinSize { get; set; } = 16384;
        /// <summary>
        /// 
        /// </summary>
        public static VulkanMemoryManager Current 
        {
            get
            {
                if (_Current == null) throw new NullReferenceException("No Memory Manager");
                return _Current;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        internal VulkanMemoryManager(VulkanDevice device)
        {
            if(_Current == null)
            {
                lock (_SyncRoot)
                {
                    if(_Current == null)
                    {
                        Device = device ?? throw new ArgumentNullException(nameof(device));
                        _Current = this;
                    }
                }
            }
        }
        /// <summary>
        /// 申请内存
        /// </summary>
        /// <param name="type"></param>
        /// <param name="requirements"></param>
        /// <returns></returns>
        public VulkanMemoryBlock Malloc(MemoryPropertyFlags type, MemoryRequirements requirements)
        {
            bool found = Device.MemoryTypeFromProperties(requirements.MemoryTypeBits, type, out int bits);
            if (found)
            {
                if (Segents.TryGetValue(bits, out var list))
                {
                    lock (_SyncRoot)
                    {
                        bool f = false;
                        foreach(var item in list)
                        {
                            f = item.TryMalloc(out VulkanMemoryBlock b, requirements);
                            if (f) return b;
                        }
                        ulong size = requirements.Size;
                        var last = list[^1];
                        VulkanMemorySegment segment = NewSeqment(type, requirements, list.Count, last.Offset + last.Size);
                        list.Add(segment);
                        f = segment.TryMalloc(out VulkanMemoryBlock block, requirements);
                        if (f) return block;
                        else throw new InvalidOperationException("malloc memory failed");
                    }
                }
                else
                {
                    lock (_SyncRoot)
                    {
                        ulong size = requirements.Size;
                        VulkanMemorySegment segment = NewSeqment(type, requirements, 0, 0);
                        list = new List<VulkanMemorySegment>() { segment };
                        Segents.AddOrUpdate(bits, list, (key, value) => list);
                        var r = segment.TryMalloc(out VulkanMemoryBlock block, requirements);
                        if (r) return block;
                        else throw new InvalidOperationException("malloc memory failed");
                    }
                }
            }
            else throw new NotSupportedException($"Target device is not have memory type for memory flags {type}");
        }
        /// <summary>
        /// 回收内存
        /// </summary>
        /// <param name="block"></param>
        public void Recovery(VulkanMemoryBlock block)
        {
            if (block == null || block.Owner == null) return;
            var seqment = block.Owner;
            seqment.Recovery(block);
        }
        /// <summary>
        /// 释放所有内存
        /// </summary>
        public void FreeAll()
        {
            if(Device == null) return;
            lock(_SyncRoot)
            {
                foreach(var item in Segents)
                {
                    var list = item.Value;
                    foreach(var l in list)
                    {
                        l.Memory.Dispose();
                    }
                    list.Clear();
                }
                Segents.Clear();
            }
        }

        /// <summary>
        /// 新的内存段
        /// </summary>
        /// <param name="type"></param>
        /// <param name="requirements"></param>
        /// <param name="index"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        private VulkanMemorySegment NewSeqment(MemoryPropertyFlags type, 
            MemoryRequirements requirements, int index, ulong offset)
        {
            ulong size = Math.Max(MinSize, requirements.Size);
            ulong mod = size % requirements.Alignment;
            if (mod != 0)
            {
                size = ((requirements.Size / requirements.Alignment) + 1) * requirements.Alignment;
            }
            requirements.Size = size;
            var memory = Device.CreateDeviceMemory(type, requirements);
            VulkanMemorySegment segment = new(index, memory, offset, requirements.Size);
            return segment;
        }

    }
}
