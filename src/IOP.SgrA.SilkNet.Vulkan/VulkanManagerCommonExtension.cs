﻿using Silk.NET.Vulkan;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.Json;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public static class VulkanManagerCommonExtension
    {
        /// <summary>
        /// 变更图像布局
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="old"></param>
        /// <param name="oldImageLayout"></param>
        /// <param name="newImageLayout"></param>
        /// <param name="range"></param>
        /// <returns></returns>
        public static VulkanCommandBuffer TransitionImageLayout(this VulkanCommandBuffer cmd, VulkanImage old,
            ImageLayout oldImageLayout, ImageLayout newImageLayout, ImageSubresourceRange range)
        {
            ImageMemoryBarrier imageMemoryBarrier = new ImageMemoryBarrier
            {
                SrcAccessMask = 0,
                DstAccessMask = 0,
                Image = old.RawHandle,
                OldLayout = oldImageLayout,
                NewLayout = newImageLayout,
                DstQueueFamilyIndex = 0,
                SrcQueueFamilyIndex = 0,
                SubresourceRange = range,
                SType = StructureType.ImageMemoryBarrier
            };
            PipelineStageFlags sourceStage;
            PipelineStageFlags destinationStage;
            (sourceStage, destinationStage) = SetPipelineStage(ref imageMemoryBarrier, oldImageLayout, newImageLayout);
            ReadOnlySpan<ImageMemoryBarrier> b = stackalloc ImageMemoryBarrier[] { imageMemoryBarrier };
            cmd.PipelineBarrier(sourceStage, destinationStage, Array.Empty<MemoryBarrier>(), Array.Empty<BufferMemoryBarrier>(), b);
            return cmd;
        }
        /// <summary>
        /// 变更图像布局2
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="old"></param>
        /// <param name="oldLayout"></param>
        /// <param name="newLayout"></param>
        /// <param name="ranges"></param>
        /// <returns></returns>
        public static VulkanCommandBuffer TransitionImageLayoutKHR2(this VulkanCommandBuffer cmd, VulkanImage old,
            ImageLayout oldLayout, ImageLayout newLayout, Span<ImageSubresourceRange> ranges)
        {
            DependencyInfo info = new DependencyInfo();
            info.SType = StructureType.DependencyInfoKhr;
            Span<ImageMemoryBarrier2> khrs = stackalloc ImageMemoryBarrier2[ranges.Length];
            for(int i = 0; i < ranges.Length; i++)
            {
                SetPipelineStageKHR2(ref khrs[i], old, oldLayout, newLayout);
                khrs[i].SubresourceRange= ranges[i];
            }
            unsafe
            {
                fixed(ImageMemoryBarrier2* p = &khrs.GetPinnableReference())
                {
                    info.PImageMemoryBarriers = p;
                    info.ImageMemoryBarrierCount = (uint)khrs.Length;
                    cmd.PipelineBarrier2KHR(info);
                }
            }
            return cmd;
        }
        /// <summary>
        /// 变更图像布局2
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="old"></param>
        /// <param name="oldLayout"></param>
        /// <param name="newLayout"></param>
        /// <param name="range"></param>
        /// <returns></returns>
        public static VulkanCommandBuffer TransitionImageLayoutKHR2(this VulkanCommandBuffer cmd, VulkanImage old,
            ImageLayout oldLayout, ImageLayout newLayout, in ImageSubresourceRange range)
        {
            Span<ImageSubresourceRange> ranges = stackalloc ImageSubresourceRange[] { range };
            return TransitionImageLayoutKHR2(cmd, old, oldLayout, newLayout, ranges);
        }

        /// <summary>
        /// 从Json中创建图像
        /// </summary>
        /// <param name="device"></param>
        /// <param name="configJson"></param>
        /// <param name="extent"></param>
        /// <param name="memory"></param>
        /// <returns></returns>
        public static VulkanImageView CreateImageFromJson(this VulkanDevice device, string configJson, Extent3D extent, MemoryPropertyFlags memory)
        {
            ImageAndImageViewCreateOption option = JsonSerializer.Deserialize<ImageAndImageViewCreateOption>(configJson);
            return device.CreateImage(option, extent, null, memory);
        }

        /// <summary>
        /// 单次提交
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="device"></param>
        /// <param name="queue"></param>
        /// <param name="fence"></param>
        /// <returns></returns>
        public static VulkanCommandBuffer SubmitSingleCommand(this VulkanCommandBuffer cmd, VulkanDevice device, Queue queue, Fence fence)
        {
            var p = new SubmitOption
            {
                Buffers = new CommandBuffer[] { cmd.RawHandle },
                SignalSemaphores = null,
                WaitDstStageMask = null,
                WaitSemaphores = null
            };
            device.Submit(queue, fence, p);
            Result r = Result.Timeout;
            Span<Fence> pf = stackalloc Fence[] { fence };
            do
            {
                r = device.WaitForFences(pf, true, 100000000);
            } while (r == Result.Timeout);
            device.ResetFences(pf);
            return cmd;
        }

        /// <summary>
        /// 瓦片格式布局转换
        /// </summary>
        /// <param name="logicDevice"></param>
        /// <param name="image"></param>
        /// <param name="buffer"></param>
        /// <param name="copy"></param>
        /// <param name="subresourceRange"></param>
        /// <param name="width"></param>
        /// <param name="heigth"></param>
        /// <param name="mipLevel"></param>
        /// <param name="filter"></param>
        /// <param name="finalLayout"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal static void TransitionTilingOptimalImage(this VulkanDevice logicDevice,
            VulkanImage image, VulkanBuffer buffer,
            BufferImageCopy copy, ImageSubresourceRange subresourceRange,
            uint width, uint heigth, uint mipLevel = 1, Filter filter = Filter.Linear, ImageLayout finalLayout = ImageLayout.ShaderReadOnlyOptimal)
        {
            var queue = logicDevice.WorkQueues.Where(x => x.Type.HasFlag(QueueType.Graphics)).FirstOrDefault();
            if (queue == null) throw new NullReferenceException("Cannot found work queue with type graphics");
            var commandPool = logicDevice.ResourcesCommandPool;
            VulkanFence fence = logicDevice.CreateFence();
            var cmd = commandPool.CreateCommandBuffer(CommandBufferLevel.Primary);
            cmd.Begin();
            TransitionImageLayout(cmd, image, ImageLayout.Undefined, ImageLayout.TransferDstOptimal, subresourceRange);
            cmd.CopyBufferToImage(buffer, image, ImageLayout.TransferDstOptimal, copy);
            if (mipLevel <= 1)
            {
                TransitionImageLayout(cmd, image, ImageLayout.TransferDstOptimal, finalLayout, subresourceRange);
            }
            if (mipLevel > 1)
            {
                GenerateMipmaps(cmd, image, (int)width, (int)heigth, mipLevel, filter);
            }
            cmd.End();
            cmd.SubmitSingleCommand(logicDevice, queue.Queue, fence.RawHandle);
            logicDevice.DestroyFence(fence);
        }
        /// <summary>
        /// 创建Mipmaps纹理
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="image"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="mipLevel"></param>
        /// <param name="filter"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal static void GenerateMipmaps(VulkanCommandBuffer cmd,
            VulkanImage image, int width, int height, uint mipLevel, Filter filter = Filter.Linear)
        {
            ImageMemoryBarrier barrier = new ImageMemoryBarrier
            {
                SrcAccessMask = 0,
                DstAccessMask = 0,
                Image = image.RawHandle,
                DstQueueFamilyIndex = 0,
                SrcQueueFamilyIndex = 0,
                SType = StructureType.ImageMemoryBarrier
            };
            ImageSubresourceRange subresourceRange = new ImageSubresourceRange(ImageAspectFlags.ColorBit, 0, 1, 0, 1);
            for (uint i = 1; i < mipLevel; i++)
            {
                subresourceRange.BaseMipLevel = i - 1;
                barrier.SubresourceRange = subresourceRange;
                barrier.OldLayout = ImageLayout.TransferDstOptimal;
                barrier.NewLayout = ImageLayout.TransferSrcOptimal;
                barrier.SrcAccessMask = AccessFlags.TransferWriteBit;
                barrier.DstAccessMask = AccessFlags.TransferReadBit;
                cmd.PipelineBarrier(PipelineStageFlags.TransferBit, PipelineStageFlags.TransferBit, null, null, new ImageMemoryBarrier[] { barrier });
                ImageBlit blit = new ImageBlit
                {
                    SrcSubresource = new ImageSubresourceLayers(ImageAspectFlags.ColorBit, i - 1, 0, 1),
                    SrcOffsets = new ImageBlit.SrcOffsetsBuffer { Element0 = new Offset3D(), Element1 = new Offset3D(width, height, 1) },
                    DstSubresource = new ImageSubresourceLayers(ImageAspectFlags.ColorBit, i, 0, 1),
                    DstOffsets = new ImageBlit.DstOffsetsBuffer() { Element0 = new Offset3D(), Element1 = new Offset3D(width > 1 ? width / 2 : 1, height > 1 ? height / 2 : 1, 1) }
                };
                cmd.BlitImage(image, ImageLayout.TransferSrcOptimal, image, ImageLayout.TransferDstOptimal, blit, filter);
                barrier.OldLayout = ImageLayout.TransferSrcOptimal;
                barrier.NewLayout = ImageLayout.ShaderReadOnlyOptimal;
                barrier.SrcAccessMask = AccessFlags.TransferReadBit;
                barrier.DstAccessMask = AccessFlags.ShaderReadBit;
                cmd.PipelineBarrier(PipelineStageFlags.TransferBit, PipelineStageFlags.FragmentShaderBit, null, null, new ImageMemoryBarrier[] { barrier });
                if (width > 1) width /= 2;
                if (height > 1) height /= 2;
            }
            subresourceRange.BaseMipLevel = mipLevel - 1;
            barrier.SubresourceRange = subresourceRange;
            barrier.OldLayout = ImageLayout.TransferDstOptimal;
            barrier.NewLayout = ImageLayout.ShaderReadOnlyOptimal;
            barrier.SrcAccessMask = AccessFlags.TransferReadBit;
            barrier.DstAccessMask = AccessFlags.ShaderReadBit;
            cmd.PipelineBarrier(PipelineStageFlags.TransferBit, PipelineStageFlags.FragmentShaderBit, null, null, new ImageMemoryBarrier[] { barrier });
        }
        /// <summary>
        /// 设置屏障管线阶段
        /// </summary>
        /// <param name="barrier"></param>
        /// <param name="oldImageLayout"></param>
        /// <param name="newImageLayout"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal static (PipelineStageFlags, PipelineStageFlags) SetPipelineStage(ref ImageMemoryBarrier barrier, ImageLayout oldImageLayout, ImageLayout newImageLayout)
        {
            PipelineStageFlags sourceStage;
            PipelineStageFlags destinationStage;

            if (oldImageLayout == ImageLayout.Undefined || oldImageLayout == ImageLayout.General)
            {
                barrier.SrcAccessMask = AccessFlags.None;
                sourceStage = PipelineStageFlags.TopOfPipeBit;
            }
            else if (oldImageLayout == ImageLayout.TransferDstOptimal)
            {
                barrier.SrcAccessMask = AccessFlags.TransferWriteBit;
                sourceStage = PipelineStageFlags.TransferBit;
            }
            else if (oldImageLayout == ImageLayout.TransferSrcOptimal)
            {
                barrier.SrcAccessMask = AccessFlags.TransferReadBit;
                sourceStage = PipelineStageFlags.TransferBit;
            }
            else if (oldImageLayout == ImageLayout.ShaderReadOnlyOptimal)
            {
                barrier.SrcAccessMask = AccessFlags.ShaderWriteBit;
                sourceStage = PipelineStageFlags.FragmentShaderBit;
            }
            else if (oldImageLayout == ImageLayout.DepthAttachmentOptimal || 
                oldImageLayout == ImageLayout.DepthAttachmentStencilReadOnlyOptimal || 
                oldImageLayout == ImageLayout.StencilAttachmentOptimal)
            {
                barrier.SrcAccessMask = AccessFlags.DepthStencilAttachmentReadBit;
                sourceStage = PipelineStageFlags.LateFragmentTestsBit;
            }
            else if (oldImageLayout == ImageLayout.ColorAttachmentOptimal)
            {
                barrier.SrcAccessMask = AccessFlags.ColorAttachmentReadBit | AccessFlags.ColorAttachmentWriteBit;
                sourceStage = PipelineStageFlags.ColorAttachmentOutputBit;
            }
            else throw new NotSupportedException("unsupported layout transition!");

            if (newImageLayout == ImageLayout.General)
            {
                barrier.DstAccessMask = AccessFlags.None;
                destinationStage = PipelineStageFlags.BottomOfPipeBit;
            }
            else if (newImageLayout == ImageLayout.TransferDstOptimal)
            {
                barrier.DstAccessMask = AccessFlags.TransferWriteBit;
                destinationStage = PipelineStageFlags.TransferBit;
            }
            else if (newImageLayout == ImageLayout.TransferSrcOptimal)
            {
                barrier.DstAccessMask = AccessFlags.TransferReadBit;
                destinationStage = PipelineStageFlags.TransferBit;
            }
            else if (newImageLayout == ImageLayout.ShaderReadOnlyOptimal)
            {
                barrier.DstAccessMask = AccessFlags.ShaderReadBit;
                destinationStage = PipelineStageFlags.VertexShaderBit;
            }
            else if (newImageLayout == ImageLayout.DepthAttachmentOptimal ||
                newImageLayout == ImageLayout.DepthAttachmentStencilReadOnlyOptimal ||
                newImageLayout == ImageLayout.StencilAttachmentOptimal)
            {
                barrier.DstAccessMask = AccessFlags.DepthStencilAttachmentWriteBit;
                destinationStage = PipelineStageFlags.EarlyFragmentTestsBit;
            }
            else if (newImageLayout == ImageLayout.ColorAttachmentOptimal)
            {
                barrier.DstAccessMask = AccessFlags.ColorAttachmentReadBit | AccessFlags.ColorAttachmentWriteBit;
                destinationStage = PipelineStageFlags.ColorAttachmentOutputBit;
            }
            else throw new NotSupportedException("unsupported layout transition!");
            return (sourceStage, destinationStage);
        }
        /// <summary>
        /// 设置屏障管线阶段2
        /// </summary>
        /// <param name="barrier"></param>
        /// <param name="image"></param>
        /// <param name="oldImageLayout"></param>
        /// <param name="newImageLayout"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal static void SetPipelineStageKHR2(ref ImageMemoryBarrier2 barrier, VulkanImage image, ImageLayout oldImageLayout, ImageLayout newImageLayout)
        {
            PipelineStageFlags2 sourceStage;
            PipelineStageFlags2 destinationStage;
            if (oldImageLayout == ImageLayout.Undefined && newImageLayout == ImageLayout.TransferDstOptimal)
            {
                barrier.DstAccessMask = AccessFlags2.TransferWriteBit;
                sourceStage = PipelineStageFlags2.TopOfPipeBit;
                destinationStage = PipelineStageFlags2.TransferBit;
            }
            else if (oldImageLayout == ImageLayout.Undefined && newImageLayout == ImageLayout.General)
            {
                barrier.DstAccessMask = AccessFlags2.ShaderReadBit | AccessFlags2.ShaderWriteBit;
                sourceStage = PipelineStageFlags2.TopOfPipeBit;
                destinationStage = PipelineStageFlags2.AllGraphicsBit;
            }
            else if (oldImageLayout == ImageLayout.General && newImageLayout == ImageLayout.TransferDstOptimal)
            {
                barrier.DstAccessMask = AccessFlags2.TransferReadBit;
                sourceStage = PipelineStageFlags2.AllGraphicsBit;
                destinationStage = PipelineStageFlags2.TransferBit;
            }
            else if (oldImageLayout == ImageLayout.TransferDstOptimal && newImageLayout == ImageLayout.General)
            {
                barrier.SrcAccessMask = AccessFlags2.TransferReadBit;
                barrier.DstAccessMask = AccessFlags2.ShaderReadBit | AccessFlags2.ShaderWriteBit;
                sourceStage = PipelineStageFlags2.TransferBit;
                destinationStage = PipelineStageFlags2.AllGraphicsBit;
            }
            else if (oldImageLayout == ImageLayout.TransferDstOptimal && newImageLayout == ImageLayout.ShaderReadOnlyOptimal)
            {
                barrier.SrcAccessMask = AccessFlags2.TransferWriteBit;
                barrier.DstAccessMask = AccessFlags2.ShaderReadBit;
                sourceStage = PipelineStageFlags2.TransferBit;
                destinationStage = PipelineStageFlags2.FragmentShaderBit;
            }
            else if (oldImageLayout == ImageLayout.TransferDstOptimal && newImageLayout == ImageLayout.TransferSrcOptimal)
            {
                barrier.SrcAccessMask = AccessFlags2.TransferWriteBit;
                barrier.DstAccessMask = AccessFlags2.TransferReadBit;
                sourceStage = PipelineStageFlags2.TransferBit;
                destinationStage = PipelineStageFlags2.TransferBit;
            }
            else if (oldImageLayout == ImageLayout.TransferSrcOptimal && newImageLayout == ImageLayout.TransferDstOptimal)
            {
                barrier.SrcAccessMask = AccessFlags2.TransferReadBit;
                barrier.DstAccessMask = AccessFlags2.TransferWriteBit;
                sourceStage = PipelineStageFlags2.TransferBit;
                destinationStage = PipelineStageFlags2.TransferBit;
            }
            else if (oldImageLayout == ImageLayout.TransferSrcOptimal && newImageLayout == ImageLayout.ShaderReadOnlyOptimal)
            {
                barrier.SrcAccessMask = AccessFlags2.TransferReadBit;
                barrier.DstAccessMask = AccessFlags2.ShaderReadBit;
                sourceStage = PipelineStageFlags2.TransferBit;
                destinationStage = PipelineStageFlags2.FragmentShaderBit;
            }
            else if (oldImageLayout == ImageLayout.DepthStencilAttachmentOptimal && newImageLayout == ImageLayout.TransferSrcOptimal)
            {
                barrier.SrcAccessMask = AccessFlags2.DepthStencilAttachmentReadBit;
                barrier.DstAccessMask = AccessFlags2.TransferWriteBit;
                sourceStage = PipelineStageFlags2.LateFragmentTestsBit;
                destinationStage = PipelineStageFlags2.TransferBit;
            }
            else if (oldImageLayout == ImageLayout.DepthAttachmentOptimal && newImageLayout == ImageLayout.TransferSrcOptimal)
            {
                barrier.SrcAccessMask = AccessFlags2.DepthStencilAttachmentReadBit;
                barrier.DstAccessMask = AccessFlags2.TransferWriteBit;
                sourceStage = PipelineStageFlags2.LateFragmentTestsBit;
                destinationStage = PipelineStageFlags2.TransferBit;
            }
            else if (oldImageLayout == ImageLayout.StencilAttachmentOptimal && newImageLayout == ImageLayout.TransferSrcOptimal)
            {
                barrier.SrcAccessMask = AccessFlags2.DepthStencilAttachmentReadBit;
                barrier.DstAccessMask = AccessFlags2.TransferWriteBit;
                sourceStage = PipelineStageFlags2.LateFragmentTestsBit;
                destinationStage = PipelineStageFlags2.TransferBit;
            }
            else if (oldImageLayout == ImageLayout.TransferSrcOptimal && newImageLayout == ImageLayout.DepthStencilAttachmentOptimal)
            {
                barrier.SrcAccessMask = AccessFlags2.TransferReadBit;
                barrier.DstAccessMask = AccessFlags2.DepthStencilAttachmentWriteBit;
                sourceStage = PipelineStageFlags2.TransferBit;
                destinationStage = PipelineStageFlags2.EarlyFragmentTestsBit;
            }
            else if (oldImageLayout == ImageLayout.TransferSrcOptimal && newImageLayout == ImageLayout.DepthAttachmentOptimal)
            {
                barrier.SrcAccessMask = AccessFlags2.TransferReadBit;
                barrier.DstAccessMask = AccessFlags2.DepthStencilAttachmentWriteBit;
                sourceStage = PipelineStageFlags2.TransferBit;
                destinationStage = PipelineStageFlags2.EarlyFragmentTestsBit;
            }
            else if (oldImageLayout == ImageLayout.TransferSrcOptimal && newImageLayout == ImageLayout.StencilAttachmentOptimal)
            {
                barrier.SrcAccessMask = AccessFlags2.TransferReadBit;
                barrier.DstAccessMask = AccessFlags2.DepthStencilAttachmentWriteBit;
                sourceStage = PipelineStageFlags2.TransferBit;
                destinationStage = PipelineStageFlags2.EarlyFragmentTestsBit;
            }
            else throw new NotSupportedException("unsupported layout transition!");
            barrier.SrcStageMask = sourceStage;
            barrier.DstStageMask = destinationStage;
            barrier.OldLayout= oldImageLayout;
            barrier.NewLayout= newImageLayout;
            barrier.Image = image.RawHandle;
            barrier.SType = StructureType.ImageMemoryBarrier2;
        }
    }
}
