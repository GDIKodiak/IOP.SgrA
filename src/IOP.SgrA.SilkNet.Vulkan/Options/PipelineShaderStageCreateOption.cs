﻿using IOP.Extension.Json;
using Microsoft.Extensions.Options;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class PipelineShaderStageCreateOption : IOptions<PipelineShaderStageCreateOption>
    {
        /// <summary>
        /// stage names a single pipeline stage. Bits which can be set include: + --
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumArrayConverter))]
        public ShaderStageFlags Stage { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public ShaderModule Module { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public SpecializationInfo? SpecializationInfo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public PipelineShaderStageCreateOption Value => this;
    }
}
