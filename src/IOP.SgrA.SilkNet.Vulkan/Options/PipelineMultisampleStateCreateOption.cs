﻿using IOP.Extension.Json;
using Microsoft.Extensions.Options;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class PipelineMultisampleStateCreateOption : IOptions<PipelineMultisampleStateCreateOption>
    {
        /// <summary>
        /// A SampleCountFlagBits specifying the number of samples per pixel used in rasterization.
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumArrayConverter))]
        public SampleCountFlags RasterizationSamples { get; set; }
        /// <summary>
        /// Specifies that fragment shading executes per-sample if VK_TRUE, or per-fragment
        /// if VK_FALSE, as described in Sample Shading.
        /// </summary>
        public bool SampleShadingEnable { get; set; }
        /// <summary>
        /// The minimum fraction of sample shading, as described in Sample Shading.
        /// </summary>
        public float MinSampleShading { get; set; }
        /// <summary>
        /// A bitmask of static coverage information that is ANDed with the coverage information
        /// generated during rasterization, as described in Sample Mask.
        /// </summary>
        public uint[] SampleMask { get; set; }
        /// <summary>
        /// alphaToCoverageEnable controls whether a temporary coverage value is generated
        /// based on the alpha component of the fragment's first color output as specified
        /// in the Multisample Coverage section.
        /// </summary>
        public bool AlphaToCoverageEnable { get; set; }
        /// <summary>
        /// alphaToOneEnable controls whether the alpha component of the fragment's first
        /// color output is replaced with one as described in Multisample Coverage.
        /// </summary>
        public bool AlphaToOneEnable { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public PipelineMultisampleStateCreateOption Value => this;
    }
}
