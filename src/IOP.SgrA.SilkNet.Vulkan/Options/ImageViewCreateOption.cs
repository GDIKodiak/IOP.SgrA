﻿using Microsoft.Extensions.Options;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 图像视图配置
    /// </summary>
    public class ImageViewCreateOption : IOptions<ImageViewCreateOption>
    {
        /// <summary>
        /// Reserved for future use.
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public ImageViewCreateFlags Flags { get; set; }
        /// <summary>
        /// A Image on which the view will be created.
        /// </summary>
        [JsonIgnore]
        public Image Image { get; set; }
        /// <summary>
        /// The type of the image view.
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public ImageViewType ViewType { get; set; }
        /// <summary>
        /// A Format describing the format and type used to interpret data elements in the
        /// image.
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public Format Format { get; set; }
        /// <summary>
        /// Specifies a remapping of color components (or of depth or stencil components
        /// after they have been converted into color components). See ComponentMapping.
        /// </summary>
        [JsonConverter(typeof(StructAndStructArrayConverter))]
        public ComponentMapping Components { get; set; }
        /// <summary>
        /// A ImageSubresourceRange selecting the set of mipmap levels and array layers to
        /// be accessible to the view.
        /// </summary>
        [JsonConverter(typeof(StructAndStructArrayConverter))]
        public ImageSubresourceRange SubresourceRange { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public ImageViewCreateOption Value => this;
    }
}
