﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// App配置
    /// </summary>
    public class ApplicationOption : IOptions<ApplicationOption>
    {
        /// <summary>
        /// A string containing the name of the application.
        /// </summary>
        public string ApplicationName { get; set; }
        /// <summary>
        /// The developer-supplied version number of the application.
        /// </summary>
        [JsonConverter(typeof(JsonVulkanVersionConverter))]
        public Versions ApplicationVersion { get; set; }
        /// <summary>
        /// Astring containing the name of the engine (if any) used to create the application.
        /// </summary>
        public string EngineName { get; set; }
        /// <summary>
        /// The developer-supplied version number of the engine used to create the application.
        /// </summary>
        [JsonConverter(typeof(JsonVulkanVersionConverter))]
        public Versions EngineVersion { get; set; }
        /// <summary>
        ///  The version of the Vulkan API against which the application expects to run. If
        ///  apiVersion is 0.0.0 the implementation must ignore it, otherwise if the implementation
        ///  does not support the requested apiVersion it must throw IncompatibleDriverException.
        ///  The patch version number specified in apiVersion is ignored when creating an
        ///  instance object. Only the major and minor versions of the instance must match
        ///  those requested in apiVersion.
        /// </summary>
        [JsonConverter(typeof(JsonVulkanVersionConverter))]
        public Versions ApiVersion { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public ApplicationOption Value => this;
    }
}
