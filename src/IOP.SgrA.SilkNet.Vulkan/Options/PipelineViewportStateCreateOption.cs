﻿using Microsoft.Extensions.Options;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class PipelineViewportStateCreateOption : IOptions<PipelineViewportStateCreateOption>
    {
        private Viewport[] _viewport = null;
        /// <summary>
        /// 反转视图坐标
        /// </summary>
        [JsonIgnore]
        public bool Reverse { get; set; } = true;
        /// <summary>
        /// An array of Viewport structures, defining the viewport transforms. If the viewport
        /// state is dynamic, this member is ignored.
        /// </summary>
        [JsonConverter(typeof(StructAndStructArrayConverter))]
        public Viewport[] Viewports 
        {
            get => _viewport;
            set
            {
                if(value == null || Reverse == false) _viewport = value;
                else
                {
                    _viewport = new Viewport[value.Length];
                    for (int i = 0; i < value.Length; i++)
                    {
                        var old = value[i];
                        var h = old.Height;
                        Viewport viewport = new(old.X, old.Y + h, old.Width, -h, 0.0f, 1.0f);
                        _viewport[i] = viewport;
                    }
                }
            }
        }
        /// <summary>
        /// An array of Rect2D structures which define the rectangular bounds of the scissor
        /// for the corresponding viewport. If the scissor state is dynamic, this member
        /// is ignored.
        /// </summary>
        [JsonConverter(typeof(StructAndStructArrayConverter))]
        public Rect2D[] Scissors { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="reverse"></param>
        public PipelineViewportStateCreateOption(bool reverse = true)
        {
            Reverse = reverse;
        }
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public PipelineViewportStateCreateOption Value => this;
    }
}
