﻿using Microsoft.Extensions.Options;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 描述符布局创建配置
    /// </summary>
    public class DescriptorSetLayoutCreateOption : IOptions<DescriptorSetLayoutCreateOption>
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// flags provides options for descriptor set layout creation, and is of type DescriptorSetLayoutCreateFlags.
        /// Bits which can be set include: + --
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public DescriptorSetLayoutCreateFlags Flags { get; set; } = 0;
        /// <summary>
        /// 
        /// </summary>
        public DescriptorSetLayoutBindingOption[] Bindings { get; set; }
        /// <summary>
        /// 描述符集目标
        /// Pipleine：描述符集由管线持有，全局生效
        /// RenderObject：描述符集由渲染对象持有，只针对当前渲染对象有效
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public DescriptorSetTarget DescriptorSetTarget { get; set; } = DescriptorSetTarget.Pipeline;
        /// <summary>
        /// 描述符集下标
        /// </summary>
        public uint SetIndex { get; set; } = 0;
        /// <summary>
        /// 最大描述集数量
        /// </summary>
        public uint MaxSets { get; set; } = 8;
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public DescriptorSetLayoutCreateOption Value => this;
    }

    /// <summary>
    /// 描述符布局绑定配置
    /// </summary>
    public class DescriptorSetLayoutBindingOption
    {
        /// <summary>
        /// 绑定位置
        /// </summary>
        public uint Binding { get; set; }
        /// <summary>
        /// 描述符类型
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public DescriptorType DescriptorType { get; set; }
        /// <summary>
        /// 描述符数量
        /// </summary>
        public uint DescriptorCount { get; set; }
        /// <summary>
        /// 绑定阶段
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public ShaderStageFlags StageFlags { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public Sampler[] PImmutableSamplers { get; set; }
    }

    /// <summary>
    /// 描述符目标
    /// </summary>
    public enum DescriptorSetTarget : int
    {
        /// <summary>
        /// 管线
        /// </summary>
        Pipeline = 0,
        /// <summary>
        /// 渲染对象
        /// </summary>
        RenderObject = 1
    }
}
