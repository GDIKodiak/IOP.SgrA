﻿using Microsoft.Extensions.Options;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class PipelineLayoutCreateOption : IOptions<PipelineLayoutCreateOption>
    {
        /// <summary>
        /// An array of DescriptorSetLayout objects.
        /// </summary>
        [JsonIgnore]
        public DescriptorSetLayout[] SetLayouts { get; set; }
        /// <summary>
        /// An array of PushConstantRange structures defining a set of push constant ranges
        /// for use in a single pipeline layout. In addition to descriptor set layouts, a
        /// pipeline layout also describes how many push constants can be accessed by each
        /// stage of the pipeline. + [NOTE] .Note ==== Push constants represent a high speed
        /// path to modify constant data in pipelines that is expected to outperform memory-backed
        /// resource updates. ====
        /// </summary>
        [JsonConverter(typeof(StructAndStructArrayConverter))]
        public PushConstantRange[] PushConstantRanges { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public PipelineLayoutCreateOption Value => this;
    }
}
