﻿using Microsoft.Extensions.Options;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class ImageAndImageViewCreateOption : IOptions<ImageAndImageViewCreateOption>
    {
        /// <summary>
        /// 图像创建配置
        /// </summary>
        public ImageCreateOption ImageCreateOption { get; set; }
        /// <summary>
        /// 图像视图创建配置
        /// </summary>
        public ImageViewCreateOption ImageViewCreateOption { get; set; }
        /// <summary>
        /// 描述符类型
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public DescriptorType DescriptorType { get; set; } = DescriptorType.CombinedImageSampler;
        /// <summary>
        /// 过滤器类型
        /// </summary>
        public Filter Filter { get; set; } = Filter.Linear;

        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public ImageAndImageViewCreateOption Value => this;
    }
}
