﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 工作队列
    /// </summary>
    public class WorkQueue
    {
        /// <summary>
        /// 类型
        /// </summary>
        public QueueType Type { get; }
        /// <summary>
        /// 队列家族下标
        /// </summary>
        public uint FamilyIndex { get; }
        /// <summary>
        /// 队列下标
        /// </summary>
        public uint QueueIndex { get; }
        /// <summary>
        /// 队列
        /// </summary>
        public Queue Queue { get; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="type"></param>
        /// <param name="queue"></param>
        /// <param name="familyIndex"></param>
        /// <param name="queueIndex"></param>
        public WorkQueue(QueueType type, uint familyIndex, uint queueIndex, Queue queue)
        {
            Type = type;
            FamilyIndex = familyIndex;
            QueueIndex = queueIndex;
            Queue = queue;
        }
    }
}
