﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// Vulkan图像信息
    /// </summary>
    public struct VulkanImageInfo
    {
        /// <summary>
        /// 图像格式
        /// </summary>
        public Format ImageFormat;
        /// <summary>
        /// 图像类型
        /// </summary>
        public ImageType ImageType;
        /// <summary>
        /// 图片用途
        /// </summary>
        public ImageUsageFlags Usage;
        /// <summary>
        /// 缓冲写入描述集
        /// </summary>
        public WriteDescriptorSet WriteDescriptorSet;
        /// <summary>
        /// 缓冲描述集
        /// </summary>
        public DescriptorSet DescriptorSet;
        /// <summary>
        /// 内存类型
        /// </summary>
        public MemoryPropertyFlags MemoryType;
        /// <summary>
        /// 图片设备内存
        /// </summary>
        public VulkanDeviceMemory DeviceMemory;
        /// <summary>
        /// 大小
        /// </summary>
        public ulong Size;
        /// <summary>
        /// 图像
        /// </summary>
        public VulkanImage Image;
        /// <summary>
        /// 图像视图
        /// </summary>
        public VulkanImageView ImageView;
    }
}
