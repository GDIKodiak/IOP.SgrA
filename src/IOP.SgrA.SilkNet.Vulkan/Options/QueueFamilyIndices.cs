﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 队列家族索引
    /// </summary>
    public struct QueueFamilyIndices
    {
        /// <summary>
        /// 家族类型
        /// </summary>
        public QueueType FamilyType;
        /// <summary>
        /// 家族下标
        /// </summary>
        public uint FamilyIndex;
    }

    /// <summary>
    /// 队列类型
    /// </summary>
    [Flags]
    public enum QueueType
    {
        /// <summary>
        /// 绘图队列
        /// </summary>
        Graphics = 1,
        /// <summary>
        /// 呈现队列
        /// </summary>
        Present = 2,
        /// <summary>
        /// 计算队列
        /// </summary>
        Compute = 4
    }
}
