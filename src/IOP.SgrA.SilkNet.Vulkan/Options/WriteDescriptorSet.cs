﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public struct WriteDescriptorSet
    {
        /// <summary>
        /// 
        /// </summary>
        public DescriptorSet DestinationSet { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public uint DestinationBinding { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public uint DestinationArrayElement { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public uint DescriptorCount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DescriptorType DescriptorType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DescriptorImageInfo[] ImageInfo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DescriptorBufferInfo[] BufferInfo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public BufferView[] TexelBufferView;
    }
}
