﻿using IOP.Extension.Json;
using Microsoft.Extensions.Options;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 图像创建配置
    /// </summary>
    public class ImageCreateOption : IOptions<ImageCreateOption>
    {
        /// <summary>
        ///  A bitmask describing additional parameters of the image. See ImageCreateFlagBits
        ///  below for a description of the supported bits.
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumArrayConverter))]
        public ImageCreateFlags Flags { get; set; }
        /// <summary>
        /// A ImageType specifying the basic dimensionality of the image, as described below.
        /// Layers in array textures do not count as a dimension for the purposes of the
        /// image type.
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public ImageType ImageType { get; set; }
        /// <summary>
        /// A Format describing the format and type of the data elements that will be contained
        /// in the image.
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public Format Format { get; set; }
        /// <summary>
        /// A Extent3D describing the number of data elements in each dimension of the base
        /// level.
        /// </summary>
        [JsonConverter(typeof(StructAndStructArrayConverter))]
        public Extent3D Extent { get; set; }
        /// <summary>
        /// mipLevels describes the number of levels of detail available for minified sampling
        /// of the image.
        /// </summary>
        public uint MipLevels { get; set; }
        /// <summary>
        /// The number of layers in the image.
        /// </summary>
        public uint ArrayLayers { get; set; }
        /// <summary>
        /// The number of sub-data element samples in the image as defined in SampleCountFlagBits.
        /// See Multisampling.
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumArrayConverter))]
        public SampleCountFlags Samples { get; set; }
        /// <summary>
        /// A ImageTiling specifying the tiling arrangement of the data elements in memory,
        /// as described below.
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public ImageTiling Tiling { get; set; } = ImageTiling.Optimal;
        /// <summary>
        /// A bitmask describing the intended usage of the image. See ImageUsageFlagBits
        /// below for a description of the supported bits.
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumArrayConverter))]
        public ImageUsageFlags Usage { get; set; }
        /// <summary>
        /// The sharing mode of the image when it will be accessed by multiple queue families,
        /// and must be one of the values described for SharingMode in the Resource Sharing
        /// section below.
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public SharingMode SharingMode { get; set; }
        /// <summary>
        /// A list of queue families that will access this image (ignored if sharingMode
        /// is not VK_SHARING_MODE_CONCURRENT).
        /// </summary>
        public uint[] QueueFamilyIndices { get; set; }
        /// <summary>
        /// initialLayout selects the initial ImageLayout state of all image subresources
        /// of the image. See Image Layouts. initialLayout must be VK_IMAGE_LAYOUT_UNDEFINED
        /// or VK_IMAGE_LAYOUT_PREINITIALIZED.
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public ImageLayout InitialLayout { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public ImageCreateOption Value => this;
    }
}
