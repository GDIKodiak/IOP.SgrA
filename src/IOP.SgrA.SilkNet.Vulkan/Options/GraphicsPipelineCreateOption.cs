﻿using Microsoft.Extensions.Options;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 图形管线创建配置
    /// </summary>
    public class GraphicsPipelineCreateOption : IOptions<GraphicsPipelineCreateOption>
    {
        /// <summary>
        /// A bitmask of PipelineCreateFlagBits controlling how the pipeline will be generated,
        /// as described below.
        /// </summary>
        public PipelineCreateFlags Flags { get; set; } = 0;
        /// <summary>
        /// An array of size stageCount structures of type PipelineShaderStageCreateInfo
        /// describing the set of the shader stages to be included in the graphics pipeline.
        /// </summary>
        public PipelineShaderStageCreateOption[] Stages { get; set; }
        /// <summary>
        /// An instance of the PipelineVertexInputStateCreateInfo structure.
        /// </summary>
        public PipelineVertexInputStateCreateOption VertexInputState { get; set; }
        /// <summary>
        /// An instance of the PipelineInputAssemblyStateCreateInfo structure which determines
        /// input assembly behavior, as described in Drawing Commands.
        /// </summary>
        public PipelineInputAssemblyStateCreateOption InputAssemblyState { get; set; }
        /// <summary>
        /// An instance of the PipelineRasterizationStateCreateInfo structure.
        /// </summary>
        public PipelineRasterizationStateCreateOption RasterizationState { get; set; }
        /// <summary>
        /// An instance of the PipelineTessellationStateCreateInfo structure, or Null if
        /// the pipeline does not include a tessellation control shader stage and tessellation
        /// evaluation shader stage.
        /// </summary>
        public PipelineTessellationStateCreateOption TessellationState { get; set; }
        /// <summary>
        /// An instance of the PipelineViewportStateCreateInfo structure, or Null if the
        /// pipeline has rasterization disabled.
        /// </summary>
        public PipelineViewportStateCreateOption ViewportState { get; set; }
        /// <summary>
        /// An instance of the PipelineColorBlendStateCreateInfo structure, or Null if the
        /// pipeline has rasterization disabled or if the subpass of the render pass the
        /// pipeline is created against does not use any color attachments.
        /// </summary>
        public PipelineColorBlendStateCreateOption ColorBlendState { get; set; }
        /// <summary>
        /// An instance of the PipelineDepthStencilStateCreateInfo structure, or Null if
        /// the pipeline has rasterization disabled or if the subpass of the render pass
        /// the pipeline is created against does not use a depth/stencil attachment.
        /// </summary>
        public PipelineDepthStencilStateCreateOption DepthStencilState { get; set; }
        /// <summary>
        /// A pointer to PipelineDynamicStateCreateInfo and is used to indicate which properties
        /// of the pipeline state object are dynamic and can be changed independently of
        /// the pipeline state. This can be Null, which means no state in the pipeline is
        /// considered dynamic.
        /// </summary>
        public PipelineDynamicStateCreateOption DynamicState { get; set; }
        /// <summary>
        /// An instance of the PipelineMultisampleStateCreateInfo, or Null if the pipeline
        /// has rasterization disabled.
        /// </summary>
        public PipelineMultisampleStateCreateOption MultisampleState { get; set; }
        /// <summary>
        /// The description of binding locations used by both the pipeline and descriptor
        /// sets used with the pipeline.
        /// </summary>
        [JsonIgnore]
        public PipelineLayout Layout { get; set; }
        /// <summary>
        /// A handle to a render pass object describing the environment in which the pipeline
        /// will be used; the pipeline must only be used with an instance of any render pass
        /// compatible with the one provided. See Render Pass Compatibility for more information.
        /// </summary>
        [JsonIgnore]
        public RenderPass RenderPass { get; set; }
        /// <summary>
        /// The index of the subpass in the render pass where this pipeline will be used.
        /// </summary>
        public uint Subpass { get; set; }
        /// <summary>
        /// A pipeline to derive from.
        /// </summary>
        [JsonIgnore]
        public Pipeline BasePipelineHandle { get; set; }
        /// <summary>
        /// An index into the pCreateInfos parameter to use as a pipeline to derive from.
        /// </summary>
        public int BasePipelineIndex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public GraphicsPipelineCreateOption Value => this;
    }
}
