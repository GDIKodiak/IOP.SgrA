﻿using Microsoft.Extensions.Options;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class DeviceCreateOption : IOptions<DeviceCreateOption>
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// An array of DeviceQueueCreateInfo structures describing the queues that are requested
        /// to be created along with the logical device. Refer to the Queue Creation section
        /// below for further details.
        /// </summary>
        public DeviceQueueCreateInfo[] QueueCreateInfos { get; set; }
        /// <summary>
        /// ppEnabledLayerNames is deprecated and ignored. See Device Layer Deprecation.
        /// </summary>
        public string[] EnabledLayerNames { get; set; }
        /// <summary>
        /// An array of enabledExtensionCount strings containing the names of extensions
        /// to enable for the created device. See the Extensions section for further details.
        /// </summary>
        public string[] EnabledExtensionNames { get; set; }
        /// <summary>
        /// Null or a PhysicalDeviceFeatures structure that contains boolean indicators of
        /// all the features to be enabled. Refer to the Features section for further details.
        /// </summary>
        public PhysicalDeviceFeatures? EnabledFeatures { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Features2 EnabledFeatures2 { get; set;}
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public DeviceCreateOption Value => this;
    }
}
