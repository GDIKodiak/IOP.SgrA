﻿using IOP.Extension.Json;
using Microsoft.Extensions.Options;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class PipelineDynamicStateCreateOption : IOptions<PipelineDynamicStateCreateOption>
    {
        /// <summary>
        /// An array of DynamicState enums which indicate which pieces of pipeline state
        /// will use the values from dynamic state commands rather than from the pipeline
        /// state creation info.
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumArrayConverter))]
        public DynamicState[] DynamicStates { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public PipelineDynamicStateCreateOption Value => this;
    }
}
