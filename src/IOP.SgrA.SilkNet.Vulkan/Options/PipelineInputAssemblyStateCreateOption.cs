﻿using Microsoft.Extensions.Options;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class PipelineInputAssemblyStateCreateOption : IOptions<PipelineInputAssemblyStateCreateOption>
    {
        /// <summary>
        /// A PrimitiveTopology defining the primitive topology, as described below.
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public PrimitiveTopology Topology { get; set; }
        /// <summary>
        /// primitiveRestartEnable controls whether a special vertex index value is treated
        /// as restarting the assembly of primitives. This enable only applies to indexed
        /// draws (flink:vkCmdDrawIndexed and flink:vkCmdDrawIndexedIndirect), and the special
        /// index value is either 0xFFFFFFFF when the indexType parameter of fname:vkCmdBindIndexBuffer
        /// is equal to VK_INDEX_TYPE_UINT32, or 0xFFFF when indexType is equal to VK_INDEX_TYPE_UINT16.
        /// Primitive restart is not allowed for "`list`" topologies.
        /// </summary>
        public bool PrimitiveRestartEnable { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public PipelineInputAssemblyStateCreateOption Value => this;
    }
}
