﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public struct SubmitOption
    {
        /// <summary>
        /// 
        /// </summary>
        public Semaphore[] WaitSemaphores { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Semaphore[] SignalSemaphores { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public CommandBuffer[] Buffers { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public PipelineStageFlags[] WaitDstStageMask { get; set; }
    }
}
