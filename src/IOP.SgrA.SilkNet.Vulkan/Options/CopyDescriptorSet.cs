﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public struct CopyDescriptorSet
    {
        /// <summary>
        /// 
        /// </summary>
        public DescriptorSet SourceSet;
        /// <summary>
        /// 
        /// </summary>
        public uint SourceBinding;
        /// <summary>
        /// 
        /// </summary>
        public uint SourceArrayElement;
        /// <summary>
        /// 
        /// </summary>
        public DescriptorSet DestinationSet;
        /// <summary>
        /// 
        /// </summary>
        public uint DestinationBinding;
        /// <summary>
        /// 
        /// </summary>
        public uint DestinationArrayElement;
        /// <summary>
        /// 
        /// </summary>
        public uint DescriptorCount;
    }
}
