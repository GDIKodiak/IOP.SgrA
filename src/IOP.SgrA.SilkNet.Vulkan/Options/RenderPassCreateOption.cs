﻿using Microsoft.Extensions.Options;
using Silk.NET.Core.Attributes;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 渲染通道创建
    /// </summary>
    public class RenderPassCreateOption : IOptions<RenderPassCreateOption>
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// An array of attachmentCount number of AttachmentDescription structures describing
        /// properties of the attachments, or Null if attachmentCount is zero.
        /// </summary>
        [JsonConverter(typeof(StructAndStructArrayConverter))]
        public AttachmentDescription[] Attachments { get; set; }

        /// <summary>
        /// An array of SubpassDescription structures describing properties of the subpasses.
        /// </summary>
        public SubpassDescriptionOption[] Subpasses { get; set; }

        /// <summary>
        /// An array of dependencyCount number of SubpassDependency structures describing
        /// dependencies between pairs of subpasses, or Null if dependencyCount is zero.
        /// </summary>
        [JsonConverter(typeof(StructAndStructArrayConverter))]
        public SubpassDependency[] Dependencies { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public RenderPassCreateOption Value => this;
    }

    /// <summary>
    /// 子通道配置
    /// </summary>
    public class SubpassDescriptionOption : IOptions<SubpassDescriptionOption>
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public PipelineBindPoint PipelineBindPoint { get; set; }
        /// <summary>
        /// 输入组件
        /// </summary>
        [JsonConverter(typeof(StructAndStructArrayConverter))]
        public AttachmentReference[] InputAttachments { get; set; }
        /// <summary>
        /// 颜色组件
        /// </summary>
        [JsonConverter(typeof(StructAndStructArrayConverter))]
        public AttachmentReference[] ColorAttachments { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonConverter(typeof(StructAndStructArrayConverter))]
        public AttachmentReference ResolveAttachments { get; set; }
        /// <summary>
        /// 深度缓冲组件
        /// </summary>
        [JsonConverter(typeof(StructAndStructArrayConverter))]
        public AttachmentReference DepthStencilAttachment { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public uint[] PreserveAttachments { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public SubpassDescriptionOption Value => this;
    }
}
