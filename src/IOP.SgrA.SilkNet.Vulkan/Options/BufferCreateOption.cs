﻿using Microsoft.Extensions.Options;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 缓存创建配置
    /// </summary>
    public class BufferCreateOption : IOptions<BufferCreateOption>
    {
        /// <summary>
        /// The size in bytes of the buffer to be created.
        /// </summary>
        public ulong Size { get; set; }
        /// <summary>
        /// A bitmask describing the allowed usages of the buffer.
        /// </summary>
        public BufferUsageFlags Usage { get; set; }
        /// <summary>
        /// The sharing mode of the buffer when it will be accessed by multiple queue families.
        /// </summary>
        public SharingMode SharingMode { get; set; } = SharingMode.Exclusive;
        /// <summary>
        /// A list of queue families that will access this buffer (ignored if sharingMode
        /// is not Concurrent).
        /// </summary>
        public uint[] QueueFamilyIndices { get; set; } = null;
        /// <summary>
        /// 缓冲格式
        /// </summary>
        public Format Format { get; set; }
        /// <summary>
        /// 描述符类型
        /// </summary>
        public DescriptorType DescriptorType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public BufferCreateOption Value => this;
    }
}
