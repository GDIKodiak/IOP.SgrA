﻿using Microsoft.Extensions.Options;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class DescriptorPoolCreateOption : IOptions<DescriptorPoolCreateOption>
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public DescriptorPoolCreateFlags Flags { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public uint MaxSets { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonConverter(typeof(StructAndStructArrayConverter))]
        public DescriptorPoolSize[] PoolSizes { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public DescriptorPoolCreateOption Value => this;
    }
}
