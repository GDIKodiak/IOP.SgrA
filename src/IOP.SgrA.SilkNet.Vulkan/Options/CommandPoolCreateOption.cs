﻿using Microsoft.Extensions.Options;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class CommandPoolCreateOption : IOptions<CommandPoolCreateOption>
    {
        /// <summary>
        /// A bitmask indicating usage behavior for the pool and command buffers allocated
        /// from it. Bits which can be set include: + --
        /// </summary>
        public CommandPoolCreateFlags Flags { get; set; }
        /// <summary>
        /// 队列家族下标
        /// </summary>
        public uint QueueFamilyIndex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public CommandPoolCreateOption Value => this;
    }
}
