﻿using Microsoft.Extensions.Options;
using Silk.NET.Vulkan;
using System.Text.Json.Serialization;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class PipelineColorBlendStateCreateOption : IOptions<PipelineColorBlendStateCreateOption>
    {
        /// <summary>
        /// logicOpEnable controls whether to apply Logical Operations.
        /// </summary>
        public bool LogicOpEnable { get; set; }
        /// <summary>
        /// logicOp selects which logical operation to apply.
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public LogicOp LogicOp { get; set; }
        /// <summary>
        /// pAttachments: is array of per target attachment states.
        /// </summary>
        [JsonConverter(typeof(StructAndStructArrayConverter))]
        public PipelineColorBlendAttachmentState[] Attachments { get; set; }
        /// <summary>
        /// An array of four values used as the R, G, B, and A components of the blend constant
        /// that are used in blending, depending on the blend factor.
        /// </summary>
        [JsonConverter(typeof(ValueTupleConverter))]
        public (float, float, float, float) BlendConstants { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public PipelineColorBlendStateCreateOption Value => this;
    }
}
