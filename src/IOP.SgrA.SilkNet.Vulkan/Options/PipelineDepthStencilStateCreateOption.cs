﻿using Microsoft.Extensions.Options;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class PipelineDepthStencilStateCreateOption : IOptions<PipelineDepthStencilStateCreateOption>
    {
        /// <summary>
        /// depthTestEnable controls whether depth testing is enabled.
        /// </summary>
        public bool DepthTestEnable { get; set; }
        /// <summary>
        /// depthWriteEnable controls whether depth writes are enabled.
        /// </summary>
        public bool DepthWriteEnable { get; set; }
        /// <summary>
        /// The comparison operator used in the depth test.
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public CompareOp DepthCompareOp { get; set; }
        /// <summary>
        /// depthBoundsTestEnable controls whether depth bounds testing is enabled.
        /// </summary>
        public bool DepthBoundsTestEnable { get; set; }
        /// <summary>
        /// stencilTestEnable controls whether stencil testing is enabled.
        /// </summary>
        public bool StencilTestEnable { get; set; }
        /// <summary>
        /// front and back control the parameters of the stencil test.
        /// </summary>
        [JsonConverter(typeof(StructAndStructArrayConverter))]
        public StencilOpState Front { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonConverter(typeof(StructAndStructArrayConverter))]
        public StencilOpState Back { get; set; }
        /// <summary>
        /// minDepthBounds and maxDepthBounds define the range of values used in the depth
        /// bounds test.
        /// </summary>
        public float MinDepthBounds { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public float MaxDepthBounds { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public PipelineDepthStencilStateCreateOption Value => this;
    }
}
