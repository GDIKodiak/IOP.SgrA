﻿using Microsoft.Extensions.Options;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 采样器创建信息
    /// </summary>
    public class SamplerCreateOption : IOptions<SamplerCreateOption>
    {
        /// <summary>
        /// The magnification filter to apply to lookups.
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public Filter MagFilter { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public Filter MinFilter { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public SamplerMipmapMode MipmapMode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public SamplerAddressMode AddressModeU { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public SamplerAddressMode AddressModeV { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public SamplerAddressMode AddressModeW { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public float MipLodBias { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool AnisotropyEnable { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public float MaxAnisotropy { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool CompareEnable { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public CompareOp CompareOp { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public float MinLod { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public float MaxLod { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public BorderColor BorderColor { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool UnnormalizedCoordinates { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public SamplerCreateOption Value => this;
    }
}
