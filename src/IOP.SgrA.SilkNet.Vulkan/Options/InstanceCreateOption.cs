﻿using Microsoft.Extensions.Options;
using System.Text.Json.Serialization;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class InstanceCreateOption : IOptions<InstanceCreateOption>
    {
        /// <summary>
        /// An array of enabledLayerCount strings containing the names of layers to enable
        /// for the created instance. See the Layers section for further details.
        /// </summary>
        public string[] EnabledLayerNames { get; set; }
        /// <summary>
        /// An array of enabledExtensionCount strings containing the names of extensions
        /// to enable.
        /// </summary>
        public string[] EnabledExtensionNames { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public InstanceCreateOption Value => this;
    }
}
