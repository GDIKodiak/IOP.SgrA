﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class RenderPassBeginOption
    {
        /// <summary>
        /// The render pass to begin an instance of.
        /// </summary>
        public RenderPass RenderPass { get; set; }
        /// <summary>
        /// The framebuffer containing the attachments that are used with the render pass.
        /// </summary>
        public Framebuffer Framebuffer { get; set; }
        /// <summary>
        /// The render area that is affected by the render pass instance, and is described
        /// in more detail below.
        /// </summary>
        public Rect2D RenderArea { get; set; }
        /// <summary>
        /// An array of ClearValue structures that contains clear values for each attachment,
        /// if the attachment uses a loadOp value of VK_ATTACHMENT_LOAD_OP_CLEAR or if the
        /// attachment has a depth/stencil format and uses a stencilLoadOp value of VK_ATTACHMENT_LOAD_OP_CLEAR.
        /// The array is indexed by attachment number. Only elements corresponding to cleared
        /// attachments are used. Other elements of pClearValues are ignored.
        /// </summary>
        public ClearValue[] ClearValues { get; set; }
    }
}
