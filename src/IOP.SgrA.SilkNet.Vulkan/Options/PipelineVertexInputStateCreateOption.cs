﻿using Microsoft.Extensions.Options;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class PipelineVertexInputStateCreateOption : IOptions<PipelineVertexInputStateCreateOption>
    {
        /// <summary>
        /// An array of VertexInputAttributeDescription structures.
        /// </summary>
        [JsonConverter(typeof(StructAndStructArrayConverter))]
        public VertexInputAttributeDescription[] VertexAttributeDescriptions { get; set; }
        /// <summary>
        /// An array of VertexInputBindingDescription structures.
        /// </summary>
        [JsonConverter(typeof(StructAndStructArrayConverter))]
        public VertexInputBindingDescription[] VertexBindingDescriptions { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public PipelineVertexInputStateCreateOption Value => this;
    }
}
