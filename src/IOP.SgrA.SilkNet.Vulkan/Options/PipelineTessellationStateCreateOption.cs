﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class PipelineTessellationStateCreateOption : IOptions<PipelineTessellationStateCreateOption>
    {
        /// <summary>
        /// patchControlPoints number of control points per patch.
        /// </summary>
        public uint PatchControlPoints { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public PipelineTessellationStateCreateOption Value => this;
    }
}
