﻿using Microsoft.Extensions.Options;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class PipelineRasterizationStateCreateOption : IOptions<PipelineRasterizationStateCreateOption>
    {
        /// <summary>
        /// depthClampEnable controls whether to clamp the fragment's depth values instead
        /// of clipping primitives to the z planes of the frustum, as described in Primitive
        /// Clipping.
        /// </summary>
        public bool DepthClampEnable { get; set; }
        /// <summary>
        /// rasterizerDiscardEnable controls whether primitives are discarded immediately
        /// before the rasterization stage.
        /// </summary>
        public bool RasterizerDiscardEnable { get; set; }
        /// <summary>
        /// The triangle rendering mode. See PolygonMode.
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public PolygonMode PolygonMode { get; set; }
        /// <summary>
        /// The triangle facing direction used for primitive culling. See CullModeFlagBits.
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public CullModeFlags CullMode { get; set; } = 0;
        /// <summary>
        /// The front-facing triangle orientation to be used for culling. See FrontFace.
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public FrontFace FrontFace { get; set; }
        /// <summary>
        /// depthBiasEnable controls whether to bias fragment depth values.
        /// </summary>
        public bool DepthBiasEnable { get; set; }
        /// <summary>
        /// A scalar factor controlling the constant depth value added to each fragment.
        /// </summary>
        public float DepthBiasConstantFactor { get; set; }
        /// <summary>
        /// The maximum (or minimum) depth bias of a fragment.
        /// </summary>
        public float DepthBiasClamp { get; set; }
        /// <summary>
        /// A scalar factor applied to a fragment's slope in depth bias calculations.
        /// </summary>
        public float DepthBiasSlopeFactor { get; set; }
        /// <summary>
        /// The width of rasterized line segments.
        /// </summary>
        public float LineWidth { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public PipelineRasterizationStateCreateOption Value => this;
    }
}
