﻿using Silk.NET.Vulkan;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 交换链呈现细节
    /// </summary>
    public class SwapChainSupportDetails
    {
        /// <summary>
        /// 
        /// </summary>
        public SurfaceCapabilitiesKHR Capabilities;
        /// <summary>
        /// 
        /// </summary>
        public SurfaceFormatKHR[] Formats;
        /// <summary>
        /// 
        /// </summary>
        public PresentModeKHR[] PresentModes;
    }
}
