﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// Vulkan缓冲信息
    /// </summary>
    public struct VulkanBufferInfo
    {
        /// <summary>
        /// 缓冲用途
        /// </summary>
        public BufferUsageFlags BufferUsageFlags;
        /// <summary>
        /// 缓冲写入描述集
        /// </summary>
        public WriteDescriptorSet WriteDescriptorSet;
        /// <summary>
        /// 缓冲描述集
        /// </summary>
        public DescriptorSet DescriptorSet;
        /// <summary>
        /// 内存偏移
        /// </summary>
        public ulong Offset;
        /// <summary>
        /// 大小
        /// </summary>
        public ulong Size;
        /// <summary>
        /// 缓冲
        /// </summary>
        public VulkanBuffer Buffer;
        /// <summary>
        /// 内存类型
        /// </summary>
        public MemoryPropertyFlags MemoryType;
        /// <summary>
        /// 缓冲设备内存
        /// </summary>
        public VulkanDeviceMemory DeviceMemory;
    }
}
