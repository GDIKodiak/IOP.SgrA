﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Silk.NET.GLFW;
using GLFW = Silk.NET.GLFW.Glfw;
using Silk.NET.Vulkan;
using System.Runtime.InteropServices;
using System.Reflection;
using Microsoft.Extensions.FileProviders;
using System.Linq;
using System.IO;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class VulkanHostService : IHostedService
    {
        /// <summary>
        /// 服务
        /// </summary>
        protected IServiceProvider ServiceProvider;
        /// <summary>
        /// 日志
        /// </summary>
        protected readonly ILogger<VulkanHostService> Logger;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <param name="logger"></param>
        public VulkanHostService(IServiceProvider serviceProvider, ILogger<VulkanHostService> logger)
        {
            ServiceProvider = serviceProvider;
            Logger = logger;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                {
                    var basePath = AppContext.BaseDirectory;
                    var filePath = Path.Combine(basePath, "libglfw.so.3.3");
                    if (!File.Exists(filePath))
                    {
                        var assembly = Assembly.GetAssembly(typeof(VulkanHostService));
                        var embeddedFileProvider = new EmbeddedFileProvider(assembly);
                        var files = embeddedFileProvider.GetDirectoryContents(string.Empty).ToList();
                        var lib = files.Where(x => x.Name == "lib.libglfw.so.3.3").FirstOrDefault();
                        if (lib != null)
                        {
                            using var sourceStream = lib.CreateReadStream();
                            using var targetStream = File.Create(filePath, (int)sourceStream.Length);
                            sourceStream.CopyTo(targetStream);
                            targetStream.Close();
                            sourceStream.Close();
                        }
                        else throw new DllNotFoundException("Cannot get bibglfw.so.3.3 in EmbeddedResources");
                    }
                }
                GLFW glfw = GLFW.GetApi();
                glfw.Init();
                var startups = ServiceProvider.GetServices<ISgrAStartup>();
                IHostEnvironment hostEnvironment = ServiceProvider.GetRequiredService<IHostEnvironment>();
                IGraphicsBuilder graphicsBuilder = ServiceProvider.GetRequiredService<IGraphicsBuilder>();
                foreach (var item in startups) item.Configure(graphicsBuilder, hostEnvironment);
                return Task.CompletedTask;
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message + "\r\n" + e.StackTrace);
            }
            return Task.CompletedTask;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
