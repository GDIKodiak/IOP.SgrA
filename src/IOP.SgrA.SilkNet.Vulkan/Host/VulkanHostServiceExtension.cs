﻿using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public static class VulkanHostServiceExtension
    {
        /// <summary>
        /// 添加Vulkan环境
        /// </summary>
        /// <param name="hostBuilder"></param>
        /// <returns></returns>
        public static ISgrAHostBuilder AddVulkanEnvironmental(this ISgrAHostBuilder hostBuilder)
        {
            hostBuilder.ConfigureServices((service) =>
            {
                service.TryAddSingleton<ISceneManager, SceneManager>();
                service.TryAddSingleton<IModuleService, VulkanModuleService>();
                service.TryAddSingleton<IGraphicsManager, VulkanGraphicsManager>();
                service.AddHostedService<VulkanHostService>();
            });
            return hostBuilder;
        }

        /// <summary>
        /// 添加Vulkan专用Json配置器
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static IConfigurationBuilder AddVulkanJsonFile(this IConfigurationBuilder builder, string path)
        {
            return builder.Add(new VulkanJsonConfigurationSource(path));
        }
    }
}
