﻿using Silk.NET.Shaderc;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class VulkanGLSLShaderInfo : VulkanShaderInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public string SourceCode { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="name"></param>
        /// <param name="sourceCode"></param>
        public VulkanGLSLShaderInfo(ShaderTypes type, string name, string sourceCode) 
            : base(type, name)
        {
            SourceCode = sourceCode;
        }

        /// <summary>
        /// 
        /// </summary>
        public override void CompileShader()
        {
            if (string.IsNullOrEmpty(SourceCode)) throw new NullReferenceException("No source code");
            CompileShaderCore();
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Dispose()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override Task LoadShader()
        {
            return Task.CompletedTask;
        }

        /// <summary>
        /// 
        /// </summary>
        protected unsafe void CompileShaderCore()
        {
            string name = ShaderType switch
            {
                ShaderTypes.VertexShader => "main.vert",
                ShaderTypes.FragmentShader => "main.frag",
                ShaderTypes.GeometryShader => "main.geom",
                _ => throw new NotSupportedException("Not supperted shader type")
            };
            ShaderKind kind = ShaderType switch
            {
                ShaderTypes.VertexShader => ShaderKind.VertexShader,
                ShaderTypes.FragmentShader => ShaderKind.FragmentShader,
                ShaderTypes.GeometryShader => ShaderKind.GeometryShader,
                _ => throw new NotSupportedException("Not supperted shader type")
            };
            Span<byte> buffer = Encoding.UTF8.GetBytes(SourceCode);
            Shaderc api = Shaderc.GetApi();
            Compiler* compiler = api.CompilerInitialize();
            CompileOptions* options = api.CompileOptionsInitialize();
            api.CompileOptionsSetSourceLanguage(options, SourceLanguage.Glsl);
            api.CompileOptionsSetTargetEnv(options, TargetEnv.Vulkan, (1u << 22) | (2 << 12)); //Vulkan Version1.2
            try
            {
                CompilationResult* result = api.CompileIntoSpv(compiler, in buffer[0], (nuint)buffer.Length, kind, name, "main", options);
                CompilationStatus status = api.ResultGetCompilationStatus(result);
                if (status != CompilationStatus.Success)
                {
                    var message = api.ResultGetErrorMessageS(result);
                    throw new Exception(message);
                }
                else
                {
                    byte* b = api.ResultGetBytes(result);
                    nuint length = api.ResultGetLength(result);
                    byte[] source = new byte[length];
                    fixed(byte* ptr = source)
                    {
                        System.Buffer.MemoryCopy(b, ptr, length, length);
                    }
                    Span<byte> trans = source;
                    Span<uint> target = MemoryMarshal.Cast<byte, uint>(trans);
                    OriginData = target.ToArray();
                    OriginFileLength = (uint)source.Length;
                }
                api.ResultRelease(result);
            }
            finally
            {
                api.CompilerRelease(compiler);
                api.CompileOptionsRelease(options);
            }
        }
    }
}
