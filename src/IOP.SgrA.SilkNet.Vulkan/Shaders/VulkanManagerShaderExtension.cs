﻿using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Microsoft.Extensions.Configuration;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public static class VulkanManagerShaderExtension
    {
        /// <summary>
        /// 构建自适应管线缓冲
        /// </summary>
        /// <param name="pipeline"></param>
        /// <param name="destinationBinding"></param>
        /// <param name="size"></param>
        /// <param name="sharingMode"></param>
        /// <param name="setIndex"></param>
        /// <param name="arrayElement"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <returns></returns>
        public static VulkanPipeline BuildUniformBuffer(this VulkanPipeline pipeline, uint setIndex, uint destinationBinding, ulong size, 
            SharingMode sharingMode, uint[] queueFamilyIndices = null, uint arrayElement = 0)
        {
            if (pipeline.DescriptorCreateInfo == null || pipeline.DescriptorCreateInfo.Length <= 0) throw new InvalidOperationException("No pipeline descriptor layout info, please create descriptor layout first");
            var dPipe = pipeline.DescriptorCreateInfo
                .Where(x => x.DescriptorSetTarget == DescriptorSetTarget.Pipeline && x.SetIndex == setIndex)
                .FirstOrDefault() ?? throw new InvalidOperationException($"Cannot found pipeline descriptor with setIndex {setIndex}");
            var texture = pipeline.VulkanManager.CreateUniformBufferTexture(Guid.NewGuid().ToString("N") + "UNIFORM", size,
                destinationBinding, arrayElement, 1, dPipe.SetIndex, sharingMode, queueFamilyIndices);
            texture.BindDescriptorSet(pipeline.GetPipelineDescriptorSet(setIndex));
            pipeline.AddPipelineTexture(setIndex, destinationBinding, texture);
            return pipeline;
        }
        /// <summary>
        /// 创建可读可写的缓冲纹理
        /// </summary>
        /// <param name="pipeline"></param>
        /// <param name="setIndex"></param>
        /// <param name="destinationBinding"></param>
        /// <param name="size"></param>
        /// <param name="sharingMode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <param name="arrayElement"></param>
        /// <returns></returns>
        public static VulkanPipeline BuildStorageBuffer(this VulkanPipeline pipeline, uint setIndex, uint destinationBinding, ulong size, 
            SharingMode sharingMode, uint[] queueFamilyIndices = null, uint arrayElement = 0)
        {
            var dPipe = pipeline.DescriptorCreateInfo
                .Where(x => x.DescriptorSetTarget == DescriptorSetTarget.Pipeline && x.SetIndex == setIndex)
                .FirstOrDefault() ?? throw new InvalidOperationException($"Cannot found pipeline descriptor with setIndex {setIndex}");
            var texture = pipeline.VulkanManager.CreateStorageBufferTexture(Guid.NewGuid().ToString("N") + "STORAGE", size,
                destinationBinding, arrayElement, 1, dPipe.SetIndex, sharingMode, queueFamilyIndices);
            texture.BindDescriptorSet(pipeline.GetPipelineDescriptorSet(setIndex));
            pipeline.AddPipelineTexture(setIndex, destinationBinding, texture);
            return pipeline;
        }
        /// <summary>
        /// 绑定管线用纹理
        /// </summary>
        /// <param name="pipeline"></param>
        /// <param name="texture"></param>
        /// <param name="setIndex"></param>
        /// <param name="destinationBinding"></param>
        /// <returns></returns>
        public static VulkanPipeline BindPipelineTexture(this VulkanPipeline pipeline, VulkanTexture texture, 
            uint setIndex, uint destinationBinding)
        {
            var dPipe = pipeline.DescriptorCreateInfo
                .Where(x => x.DescriptorSetTarget == DescriptorSetTarget.Pipeline && x.SetIndex == setIndex)
                .FirstOrDefault();
            if (dPipe == null) throw new InvalidOperationException($"Cannot found pipeline descriptor with setIndex {setIndex}");
            texture.BindDescriptorSet(pipeline.GetPipelineDescriptorSet(setIndex));
            pipeline.AddPipelineTexture(setIndex, destinationBinding, texture);
            return pipeline;
        }
        /// <summary>
        /// 绑定点光源环境
        /// </summary>
        /// <param name="pipeline"></param>
        /// <param name="environment"></param>
        /// <param name="destinationBinding"></param>
        /// <param name="setIndex"></param>
        /// <returns></returns>
        public static VulkanPipeline BindPointLightEnvironment(this VulkanPipeline pipeline, ILightEnvironment environment, 
            uint setIndex, uint destinationBinding)
        {
            if (environment == null || environment is not VulkanLightEnvironment vulkan) return pipeline;
            var dPipe = pipeline.DescriptorCreateInfo
                .Where(x => x.DescriptorSetTarget == DescriptorSetTarget.Pipeline && x.SetIndex == setIndex)
                .FirstOrDefault() ?? throw new InvalidOperationException($"Cannot found pipeline descriptor with setIndex {setIndex}");
            var manager = pipeline.VulkanManager;
            var texture = manager.CreateTexture(Guid.NewGuid().ToString("N") + "STORAGE", vulkan.GetCurrentPointLightBuffer(), null, destinationBinding,
                0, 1, setIndex, DescriptorType.StorageBuffer);
            texture.BindDescriptorSet(pipeline.GetPipelineDescriptorSet(setIndex));
            pipeline.AddPipelineTexture(setIndex, destinationBinding, texture);
            vulkan.OnPointLightSourceChanged += (data, env) =>
            {
                env.Owner.Post(() =>
                {
                    texture.UpdateTextureData(data);
                });
            };
            return pipeline;
        }
        /// <summary>
        /// 绑定平行光环境
        /// </summary>
        /// <param name="pipeline"></param>
        /// <param name="environment"></param>
        /// <param name="setIndex"></param>
        /// <param name="destinationBinding"></param>
        /// <returns></returns>
        public static VulkanPipeline BindParallelLightEnvironment(this VulkanPipeline pipeline, ILightEnvironment environment,
            uint setIndex, uint destinationBinding)
        {
            if (environment == null || environment is not VulkanLightEnvironment vulkan) return pipeline;
            var dPipe = pipeline.DescriptorCreateInfo
                .Where(x => x.DescriptorSetTarget == DescriptorSetTarget.Pipeline && x.SetIndex == setIndex)
                .FirstOrDefault() ?? throw new InvalidOperationException($"Cannot found pipeline descriptor with setIndex {setIndex}");
            var manager = pipeline.VulkanManager;
            var texture = manager.CreateTexture(Guid.NewGuid().ToString("N") + "STORAGE", vulkan.GetCurrentParallelLightBuffer(), null, destinationBinding,
                0, 1, setIndex, DescriptorType.StorageBuffer);
            texture.BindDescriptorSet(pipeline.GetPipelineDescriptorSet(setIndex));
            pipeline.AddPipelineTexture(setIndex, destinationBinding, texture);
            vulkan.OnParallelLightSourceChanged += (data, env) =>
            {
                env.Owner.Post(() =>
                {
                    texture.UpdateTextureData(data);
                });
            };
            return pipeline;
        }

        /// <summary>
        /// 构建管线布局
        /// </summary>
        /// <param name="pipeline"></param>
        /// <param name="options"></param>
        /// <param name="createOption"></param>
        /// <returns></returns>
        public static VulkanPipeline BuildPipelineLayout(this VulkanPipeline pipeline, DescriptorSetLayoutCreateOption[] options, PipelineLayoutCreateOption createOption)
        {
            pipeline.CreatePipelineSetLayout(options, createOption);
            return pipeline;
        }
        /// <summary>
        /// 从配置项构建管线布局
        /// </summary>
        /// <param name="pipeline"></param>
        /// <param name="pipeLayoutNode"></param>
        /// <param name="layout"></param>
        /// <returns></returns>
        public static VulkanPipeline BuildPipelineLayoutFromConfiguration(this VulkanPipeline pipeline, string pipeLayoutNode = nameof(DescriptorSetLayoutCreateOption),
            string layout = nameof(PipelineLayoutCreateOption))
        {
            var pipeJson = pipeline.Configuration.GetValue(pipeLayoutNode ?? string.Empty, string.Empty);
            var layoutJson = pipeline.Configuration.GetValue(layout ?? string.Empty, string.Empty);
            DescriptorSetLayoutCreateOption[] options = new DescriptorSetLayoutCreateOption[0];
            PipelineLayoutCreateOption createOption = null;
            if (!string.IsNullOrEmpty(pipeJson))
            {
                options = JsonSerializer.Deserialize<DescriptorSetLayoutCreateOption[]>(pipeJson);
            }
            if (!string.IsNullOrEmpty(layoutJson))
            {
                createOption = JsonSerializer.Deserialize<PipelineLayoutCreateOption>(layoutJson);
            }
            pipeline.CreatePipelineSetLayout(options, createOption);
            return pipeline;
        }
        /// <summary>
        /// 从Json创建管线布局
        /// </summary>
        /// <param name="pipeline"></param>
        /// <param name="layoutJson"></param>
        /// <param name="createJson"></param>
        /// <returns></returns>
        public static VulkanPipeline BuildPipelineLayoutFromJson(this VulkanPipeline pipeline, string layoutJson, string createJson)
        {
            DescriptorSetLayoutCreateOption[] options = new DescriptorSetLayoutCreateOption[0];
            PipelineLayoutCreateOption createOption = null;
            if (!string.IsNullOrEmpty(layoutJson))
            {
                options = JsonSerializer.Deserialize<DescriptorSetLayoutCreateOption[]>(layoutJson);
            }
            if (!string.IsNullOrEmpty(createJson))
            {
                createOption = JsonSerializer.Deserialize<PipelineLayoutCreateOption>(createJson);
            }
            pipeline.CreatePipelineSetLayout(options, createOption);
            return pipeline;
        }

        /// <summary>
        /// 构建着色器管线
        /// </summary>
        /// <param name="pipeline"></param>
        /// <param name="renderPass"></param>
        /// <param name="pipelineCache"></param>
        /// <param name="infos"></param>
        /// <param name="subPass"></param>
        /// <param name="inputState"></param>
        /// <param name="assemblyState"></param>
        /// <param name="restState"></param>
        /// <param name="viewportState"></param>
        /// <param name="blendState"></param>
        /// <param name="dStemcilState"></param>
        /// <param name="mState"></param>
        /// <param name="dState"></param>
        /// <param name="basePipeline"></param>
        /// <param name="basePipelineIndex"></param>
        /// <returns></returns>
        public static VulkanPipeline BuildGraphicsPipeline(this VulkanPipeline pipeline, VulkanRenderPass renderPass, PipelineCache pipelineCache, VulkanShaderInfo[] infos,
            uint subPass, Action<PipelineVertexInputStateCreateOption> inputState,
            Action<PipelineInputAssemblyStateCreateOption> assemblyState,
            Action<PipelineRasterizationStateCreateOption> restState,
            Action<PipelineViewportStateCreateOption> viewportState,
            Action<PipelineColorBlendStateCreateOption> blendState = null,
            Action<PipelineDepthStencilStateCreateOption> dStemcilState = null,
            Action<PipelineMultisampleStateCreateOption> mState = null,
            Action<PipelineDynamicStateCreateOption> dState = null,
            VulkanPipeline basePipeline = null,
            int basePipelineIndex = -1)
        {
            if (infos == null) throw new ArgumentNullException(nameof(infos));
            if (inputState == null) throw new ArgumentNullException(nameof(inputState));
            if (assemblyState == null) throw new ArgumentNullException(nameof(assemblyState));
            if (restState == null) throw new ArgumentNullException(nameof(restState));
            if (viewportState == null) throw new ArgumentNullException(nameof(viewportState));

            GraphicsPipelineCreateOption createOption = new GraphicsPipelineCreateOption();
            createOption.BasePipelineIndex = basePipelineIndex;
            createOption.Subpass = subPass;
            createOption.Stages = new PipelineShaderStageCreateOption[infos.Length];
            for(int i = 0; i < infos.Length; i++)
            {
                createOption.Stages[i] = new PipelineShaderStageCreateOption
                {
                    Name = infos[i].EntryPoint,
                    Stage = infos[i].GetVulkanShaderType()
                };
            }
            if (basePipeline != null)
            {
                createOption.BasePipelineHandle = basePipeline.Pipeline;
            }
            PipelineVertexInputStateCreateOption inputStateOption = new PipelineVertexInputStateCreateOption();
            inputState(inputStateOption);
            createOption.VertexInputState = inputStateOption;

            PipelineInputAssemblyStateCreateOption assemblyStateOption = new PipelineInputAssemblyStateCreateOption();
            assemblyState(assemblyStateOption);
            createOption.InputAssemblyState = assemblyStateOption;

            PipelineRasterizationStateCreateOption restStateOption = new PipelineRasterizationStateCreateOption();
            restState(restStateOption);
            createOption.RasterizationState = restStateOption;

            PipelineViewportStateCreateOption viewportStateOption = new PipelineViewportStateCreateOption();
            viewportState(viewportStateOption);
            createOption.ViewportState = viewportStateOption;

            if (blendState != null)
            {
                PipelineColorBlendStateCreateOption blendStateOption = new PipelineColorBlendStateCreateOption();
                blendState(blendStateOption);
                createOption.ColorBlendState = blendStateOption;
            }
            if (dStemcilState != null)
            {
                PipelineDepthStencilStateCreateOption dStencilStateOption = new PipelineDepthStencilStateCreateOption();
                dStemcilState(dStencilStateOption);
                createOption.DepthStencilState = dStencilStateOption;
            }
            if (mState != null)
            {
                PipelineMultisampleStateCreateOption mOption = new PipelineMultisampleStateCreateOption();
                mState(mOption);
                createOption.MultisampleState = mOption;
            }
            if (dState != null)
            {
                PipelineDynamicStateCreateOption dStateOption = new PipelineDynamicStateCreateOption();
                dState(dStateOption);
                createOption.DynamicState = dStateOption;
            }
            return pipeline.BuildGraphicsPipeline(renderPass, pipelineCache, infos, createOption);
        }
        /// <summary>
        /// 构建着色器管线
        /// </summary>
        /// <param name="pipeline"></param>
        /// <param name="renderPass"></param>
        /// <param name="pipelineCache"></param>
        /// <param name="infos"></param>
        /// <param name="createOption"></param>
        /// <returns></returns>
        public static VulkanPipeline BuildGraphicsPipeline(this VulkanPipeline pipeline, VulkanRenderPass renderPass, PipelineCache pipelineCache, 
            VulkanShaderInfo[] infos, GraphicsPipelineCreateOption createOption)
        {
            if (pipeline.PipelineLayout.Handle == 0) throw new NullReferenceException("The pipeline layout is null, please build PipelineLayout first");
            if (renderPass == null) throw new NullReferenceException("The render pass is null in this logic device, please create render pass first");
            if (pipelineCache.Handle == 0) throw new ArgumentNullException(nameof(pipelineCache));
            if (infos == null) throw new ArgumentNullException(nameof(infos));
            if (createOption == null) throw new ArgumentNullException(nameof(createOption));
            var device = pipeline.Device;
            var stages = createOption.Stages;
            for (int i = 0; i < infos.Length; i++)
            {
                var local = infos[i];
                if (local.OriginData == null || local.OriginFileLength == 0) throw new NullReferenceException($"Please load shader first, shader name :{local.ShaderName}");
                string name = stages[i].Name ?? "main";
                var module = device.CreateShaderModule(local.OriginData, local.OriginFileLength);
                stages[i].Module = module;
                stages[i].Name = name;
                stages[i].Stage = local.GetVulkanShaderType();
                pipeline.ShaderModules.Add(module);
            }
            if (stages.Length != infos.Length) throw new IndexOutOfRangeException("the stages's length is not same as shader's length");
            var pipe = device.CreateGraphicsPipeline(renderPass, pipelineCache, pipeline.PipelineLayout, createOption);
            pipeline.Pipeline = pipe;
            return pipeline;
        }
        /// <summary>
        /// 从配置项中构建图像管线
        /// </summary>
        /// <param name="pipeline"></param>
        /// <param name="pass"></param>
        /// <param name="nodeName"></param>
        /// <param name="infos"></param>
        /// <param name="pipelineCache"></param>
        /// <returns></returns>
        public static VulkanPipeline BuildGraphicsPipelineFromConfiguration(this VulkanPipeline pipeline, VulkanRenderPass pass, PipelineCache pipelineCache, VulkanShaderInfo[] infos, string nodeName = nameof(GraphicsPipelineCreateOption))
        {
            var appJson = pipeline.Configuration.GetValue<string>(nodeName);
            if (string.IsNullOrEmpty(appJson)) throw new ArgumentNullException($"Cannot get {nameof(GraphicsPipelineCreateOption)} form configuration, node name {nodeName}");
            GraphicsPipelineCreateOption createOption = JsonSerializer.Deserialize<GraphicsPipelineCreateOption>(appJson);
            return pipeline.BuildGraphicsPipeline(pass, pipelineCache, infos, createOption);
        }
        /// <summary>
        /// 从Json构建着色管线
        /// </summary>
        /// <param name="renderPass"></param>
        /// <param name="pipeline"></param>
        /// <param name="pipelineCache"></param>
        /// <param name="infos"></param>
        /// <param name="json"></param>
        /// <returns></returns>
        public static VulkanPipeline BuildGraphicsPipelineFromJson(this VulkanPipeline pipeline, VulkanRenderPass renderPass, PipelineCache pipelineCache, VulkanShaderInfo[] infos, string json)
        {
            GraphicsPipelineCreateOption createOption = JsonSerializer.Deserialize<GraphicsPipelineCreateOption>(json);
            return BuildGraphicsPipeline(pipeline, renderPass, pipelineCache, infos, createOption);
        }
        /// <summary>
        /// 构建脚本化图形管线
        /// </summary>
        /// <typeparam name="TPipeline"></typeparam>
        /// <param name="manager"></param>
        /// <param name="name"></param>
        /// <param name="renderPass"></param>
        /// <param name="pipelineCache"></param>
        /// <param name="infos"></param>
        /// <param name="area"></param>
        /// <param name="sample"></param>
        /// <returns></returns>
        public static VulkanPipeline BuildScriptedGraphicsPipeline<TPipeline>(this VulkanGraphicsManager manager, string name, VulkanRenderPass renderPass, PipelineCache pipelineCache,
            VulkanShaderInfo[] infos, Area area, SampleCount sample = SampleCount.Sample1X)
            where TPipeline : ScriptedVulkanPipeline, new()
        {
            TPipeline pipe = new TPipeline();
            if (string.IsNullOrEmpty(name)) name = manager.NewStringToken();
            var pipeline = manager.CreateEmptyPipeline(name);
            pipeline = pipeline.BuildPipelineLayout(pipe.BuildPipelineDescriptorSetLayoutOptions(), pipe.BuildPipelineLayoutOption());
            var option = pipe.BuildPipelineOption(area);
            option.MultisampleState.RasterizationSamples = sample.ToSampleFlags();
            pipeline = pipeline.BuildGraphicsPipeline(renderPass, pipelineCache, infos, option);
            return pipeline;
        }
        /// <summary>
        /// 构建脚本化着色器以及其管线
        /// </summary>
        /// <typeparam name="TPipeline"></typeparam>
        /// <param name="manager"></param>
        /// <param name="name"></param>
        /// <param name="renderPass"></param>
        /// <param name="pipelineCache"></param>
        /// <param name="area"></param>
        /// <param name="sample"></param>
        /// <returns></returns>
        public static VulkanPipeline BuildScriptedShaderAndPipeline<TPipeline>(this VulkanGraphicsManager manager, string name, VulkanRenderPass renderPass, 
            PipelineCache pipelineCache, Area area, SampleCount sample = SampleCount.Sample1X)
            where TPipeline : ScriptedVulkanPipeline, new()
        {
            TPipeline pipe = new TPipeline();
            var infos = pipe.CompileScriptedShaders();
            if (string.IsNullOrEmpty(name)) name = manager.NewStringToken();
            var pipeline = manager.CreateEmptyPipeline(name);
            pipeline = pipeline.BuildPipelineLayout(pipe.BuildPipelineDescriptorSetLayoutOptions(), pipe.BuildPipelineLayoutOption());
            var option = pipe.BuildPipelineOption(area);
            option.MultisampleState.RasterizationSamples = sample.ToSampleFlags();
            pipeline = pipeline.BuildGraphicsPipeline(renderPass, pipelineCache, infos, option);
            return pipeline;
        }

        /// <summary>
        /// 构建计算着色器管线
        /// </summary>
        /// <param name="pipeline"></param>
        /// <param name="pipelineCache"></param>
        /// <param name="info"></param>
        /// <returns></returns>
        public static VulkanPipeline BuildComputePipeline(this VulkanPipeline pipeline, PipelineCache pipelineCache, VulkanShaderInfo info)
        {
            if (pipeline.PipelineLayout.Handle == 0) throw new NullReferenceException("The pipeline layout is null, please build PipelineLayout first");
            if (pipelineCache.Handle == 0) throw new ArgumentNullException(nameof(pipelineCache));
            if (info == null) throw new ArgumentNullException(nameof(info));
            var device = pipeline.Device;
            var layout = pipeline.PipelineLayout;
            if (info.OriginData == null || info.OriginFileLength == 0) throw new NullReferenceException($"Please load shader first, shader name :{info.ShaderName}");
            var module = device.CreateShaderModule(info.OriginData, info.OriginFileLength);
            pipeline.ShaderModules.Add(module);
            var stageInfo = new PipelineShaderStageCreateOption
            {
                Module = module,
                Name = string.IsNullOrEmpty(info.EntryPoint) ? "main" : info.EntryPoint,
                Stage = ShaderStageFlags.ComputeBit
            };
            var pipe = device.CreateComputePipeline(pipelineCache, layout, stageInfo);
            pipeline.Pipeline = pipe;
            return pipeline;
        }
    }
}
