﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// Vulkan着色器信息
    /// </summary>
    public abstract class VulkanShaderInfo : ShaderInfo
    {
        /// <summary>
        /// 入口函数
        /// </summary>
        public string EntryPoint { get; set; } = "main";
        /// <summary>
        /// 原始数据
        /// </summary>
        public uint[] OriginData { get; protected set; }
        /// <summary>
        /// 原始文件大小
        /// </summary>
        public uint OriginFileLength { get; protected set; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="type"></param>
        /// <param name="name"></param>
        public VulkanShaderInfo(ShaderTypes type, string name)
            : base(type, name)
        {
        }
        /// <summary>
        /// 获取Vulkan着色器类型
        /// </summary>
        /// <returns></returns>
        public virtual ShaderStageFlags GetVulkanShaderType()
        {
            switch (ShaderType)
            {
                case ShaderTypes.ComputeShader:
                    return ShaderStageFlags.ComputeBit;
                case ShaderTypes.FragmentShader:
                    return ShaderStageFlags.FragmentBit;
                case ShaderTypes.GeometryShader:
                    return ShaderStageFlags.GeometryBit;
                case ShaderTypes.TessControlShader:
                    return ShaderStageFlags.TessellationControlBit;
                case ShaderTypes.TessEvaluationShader:
                    return ShaderStageFlags.TessellationEvaluationBit;
                case ShaderTypes.VertexShader:
                    return ShaderStageFlags.VertexBit;
                default:
                    return 0;
            }
        }
    }
}
