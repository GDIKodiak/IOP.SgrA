﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// VulkanSPV文件着色器信息
    /// </summary>
    public class VulkanSPVShaderInfo : VulkanShaderInfo
    {
        /// <summary>
        /// 文件
        /// </summary>
        public FileInfo FilePath { get; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="type"></param>
        /// <param name="name"></param>
        /// <param name="file"></param>
        public VulkanSPVShaderInfo(ShaderTypes type, string name, FileInfo file)
            : base(type, name)
        {
            FilePath = file;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="name"></param>
        /// <param name="bytes"></param>
        public VulkanSPVShaderInfo(ShaderTypes type, string name, byte[] bytes)
            : base(type, name)
        {
            var shaderData = new uint[(int)Math.Ceiling(bytes.Length / 4f)];
            Span<byte> b = bytes; Span<uint> c = MemoryMarshal.Cast<byte, uint>(b);
            Span<uint> t = shaderData; c.CopyTo(t);
            OriginData = shaderData;
            OriginFileLength = (uint)bytes.Length;
        }

        /// <summary>
        /// 编译着色器(无需编译)
        /// </summary>
        public override void CompileShader() { }

        /// <summary>
        /// 加载着色器
        /// </summary>
        public override async Task LoadShader()
        {
            if (!FilePath.Exists) throw new FileNotFoundException($"Cannot found file names {FilePath.FullName}");
            var bytes = await File.ReadAllBytesAsync(FilePath.FullName);
            var shaderData = new uint[(int)Math.Ceiling(bytes.Length / 4f)];
            Buffer.BlockCopy(bytes, 0, shaderData, 0, bytes.Length);
            OriginData = shaderData;
            OriginFileLength = (uint)bytes.Length;
        }

        /// <summary>
        /// 销毁资源
        /// </summary>
        public override void Dispose()
        {
            OriginData = null;
        }
    }
}
