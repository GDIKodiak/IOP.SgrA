﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class VulkanBytesShaderInfo : VulkanShaderInfo
    {
        /// <summary>
        /// 二进制数组
        /// </summary>
        public byte[] Sources { get; private set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="type"></param>
        /// <param name="name"></param>
        /// <param name="code"></param>
        public VulkanBytesShaderInfo(ShaderTypes type, string name, byte[] code)
            : base(type, name)
        {
            Sources = code;
        }

        /// <summary>
        /// 编译着色器
        /// </summary>
        public override void CompileShader()
        {
        }

        /// <summary>
        /// 销毁资源
        /// </summary>
        public override void Dispose()
        {
            Sources = null;
        }

        /// <summary>
        /// 加载着色器
        /// </summary>
        /// <returns></returns>
        public override Task LoadShader()
        {
            OriginFileLength = (uint)Sources.Length;
            ReadOnlySpan<byte> c = Sources;
            ReadOnlySpan<uint> data = MemoryMarshal.Cast<byte, uint>(c);
            OriginData = data.ToArray();
            return Task.CompletedTask;
        }
    }
}
