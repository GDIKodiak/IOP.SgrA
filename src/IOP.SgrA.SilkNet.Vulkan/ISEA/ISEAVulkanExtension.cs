﻿using IOP.ISEA;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public static class ISEAVulkanExtension
    {
        /// <summary>
        /// 创建渲染对象
        /// </summary>
        /// <typeparam name="TRenderObject"></typeparam>
        /// <param name="manager"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public static async Task<TRenderObject[]> CreateRenderObjectsFromISEA<TRenderObject>(this VulkanGraphicsManager manager, ISEADocument document)
            where TRenderObject : RenderObject, new()
        {
            RenderObjectNode[] nodes = document.GetNodes<RenderObjectNode>();
            TRenderObject[] objects = new TRenderObject[nodes.Length];
            for (int i = 0; i < nodes.Length; i++)
            {
                var obj = await manager.CreateRenderObjectFromISEA<TRenderObject>(nodes[i]);
                objects[i] = obj;
            }
            return objects;
        }
        /// <summary>
        /// 根据名称创建渲染对象
        /// </summary>
        /// <typeparam name="TRenderObject"></typeparam>
        /// <param name="manager"></param>
        /// <param name="document"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static async Task<TRenderObject> CreateRenderObjectsFromISEA<TRenderObject>(this VulkanGraphicsManager manager, ISEADocument document, string name)
            where TRenderObject : RenderObject, new()
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));
            RenderObjectNode node = document.GetNodeFromName<RenderObjectNode>(name);
            var obj = await manager.CreateRenderObjectFromISEA<TRenderObject>(node);
            return obj;
        }

        /// <summary>
        /// 创建渲染对象
        /// </summary>
        /// <typeparam name="TRenderObject"></typeparam>
        /// <param name="manager"></param>
        /// <param name="node"></param>
        /// <returns></returns>
        public static async Task<TRenderObject> CreateRenderObjectFromISEA<TRenderObject>(this VulkanGraphicsManager manager, RenderObjectNode node)
            where TRenderObject : RenderObject, new()
        {
            string name = string.IsNullOrEmpty(node.Name.Data) ? Guid.NewGuid().ToString("N") : node.Name.Data;
            List<IRenderComponent> components = new List<IRenderComponent>();
            Vector3 min = -Vector3.One;
            Vector3 max = Vector3.One;
            foreach (var component in node.Comonents)
            {
                switch (component)
                {
                    case MeshNode meshNode:
                        var vro = await manager.CreateVROFromISEA(meshNode);
                        min = vro.MinVector;
                        max = vro.MaxVector;
                        components.Add(vro);
                        break;
                    case TextureNode textureNode:
                        var texture = await manager.CreateVulkanTextureFromISEA(textureNode);
                        components.Add(texture);
                        break;
                    default:
                        break;
                }
            }
            TRenderObject obj = manager.CreateRenderObject<TRenderObject>(name, components.ToArray());
            var tramsform = obj.GetTransform();
            obj.SetBounds(min, max);
            tramsform.Position = node.Position;
            tramsform.Rotation = node.Rotation;
            tramsform.Scaling = node.Scale;
            if (node.ChildArrayLength > 0)
            {
                foreach(var item in node.Childrens)
                {
                    if(manager.TryGetRenderObject(item.Name.Data, out TRenderObject rChildren))
                    {
                        var copy = rChildren.DeepCopy(manager);
                        obj.AddChildren(copy);
                    }
                    else
                    {
                        var child = await CreateRenderObjectFromISEA<TRenderObject>(manager, item);
                        obj.AddChildren(child);
                    }
                }
            }
            return obj;
        }

        /// <summary>
        /// 创建网格
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="mesh"></param>
        /// <returns></returns>
        public static Task<VRO> CreateVROFromISEA(this VulkanGraphicsManager manager, MeshNode mesh)
        {
            var name = string.IsNullOrEmpty(mesh.Name.Data) ? Guid.NewGuid().ToString("N") : mesh.Name.Data;
            if (manager.TryGetMesh(name, out VRO old))
            {
                return Task.FromResult(old);
            }
            var vro = manager.CreateEmptyMesh<VRO>(name);
            if (mesh.ComponentSequenceLength > 1)
            {
                Span<byte> r = new byte[mesh.VerticesStride * mesh.VerticesCount];
                int used = 0;
                var components = mesh.VerticesComponents;
                var seq = mesh.VerticesComponentSequence;
                for (int i = 0; i < mesh.VerticesCount; i++)
                {
                    if (used >= r.Length) break;
                    foreach (var index in seq)
                    {
                        if (used >= r.Length) break;
                        var local = components[index];
                        int start = i * local.Stride;
                        int end = (i + 1) * local.Stride;
                        if (end > local.Data.ArrayLength) continue;
                        var copy = local.Data.Arrays.AsSpan()[start..end];
                        copy.CopyTo(r[used..]);
                        used += local.Stride;
                    }
                }
                vro = vro.CreateVecticesBuffer(r.ToArray(), (uint)mesh.VerticesCount, manager.VulkanDevice, SharingMode.Exclusive);
            }
            else
            {
                MeshData data = mesh.VerticesComponents[0];
                vro = vro.CreateVecticesBuffer(data.Data.Arrays, (uint)mesh.VerticesCount, manager.VulkanDevice, SharingMode.Exclusive);
            }
            if (mesh.MeshMode == MeshMode.Indexes)
            {
                MeshData indexes = mesh.Indexes;
                vro = vro.CreateIndexesBuffer(indexes.Data.Arrays, (uint)(indexes.Data.ArrayLength / indexes.Stride), manager.VulkanDevice, SharingMode.Exclusive);
            }
            if (mesh.IndirectCommandsLength > 0)
            {
                foreach (var item in mesh.DrawIndirectCommands)
                {
                    switch (item.Purpose)
                    {
                        case MeshDataPurpose.DRAWINDIRECT:
                            Span<byte> l = item.Data.Arrays;
                            Span<DrawIndirectData> d = MemoryMarshal.Cast<byte, DrawIndirectData>(l);
                            DrawIndirectCommand[] commands = new DrawIndirectCommand[d.Length];
                            for (int i = 0; i < d.Length; i++)
                            {
                                commands[i] = new DrawIndirectCommand(d[i].VertexCount, d[i].InstanceCount, d[i].FirstVertex, d[i].FirstInstance);
                            }
                            vro = vro.CreateOrUpdateDrawIndirectBuffer(commands, manager.VulkanDevice, SharingMode.Exclusive);
                            break;
                        case MeshDataPurpose.DRAWINDEXEDINDIRECT:
                            Span<byte> dl = item.Data.Arrays;
                            Span<DrawIndexedIndirectData> di = MemoryMarshal.Cast<byte, DrawIndexedIndirectData>(dl);
                            DrawIndexedIndirectCommand[] dCommands = new DrawIndexedIndirectCommand[di.Length];
                            for (int i = 0; i < di.Length; i++)
                            {
                                dCommands[i] = new DrawIndexedIndirectCommand(di[i].IndexCount, di[i].InstanceCount,
                                    di[i].FirstIndex, di[i].VertexOffset, di[i].FirstInstance);
                            }
                            vro = vro.CreateOrUpdateDrawIndexedIndirectBuffer(dCommands, manager.VulkanDevice, SharingMode.Exclusive);
                            break;
                        default:
                            break;
                    }
                }
            }
            vro.MinVector = mesh.MinVector;
            vro.MaxVector = mesh.MaxVector;
            return Task.FromResult(vro);
        }

        /// <summary>
        /// 从资产文件中创建纹理对象
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="node"></param>
        /// <returns></returns>
        public static async Task<VulkanTexture> CreateVulkanTextureFromISEA(this VulkanGraphicsManager manager, TextureNode node)
        {
            var image = node.Image;
            var name = string.IsNullOrEmpty(node.Name.Data) ? Guid.NewGuid().ToString("N") : node.Name.Data;
            if(manager.TryGetTexture(name, out VulkanTexture tex)) return tex;
            var samplerConfig = node.SamplerConfig;
            VulkanSampler sampler = null;
            var deivce = manager.VulkanDevice;
            switch (image.Extension.ToString())
            {
                case Constant.BNTEX:
                    var bntex = await CreateBntexTextureData(manager, node);
                    if (samplerConfig != null)
                    {
                        if (string.IsNullOrEmpty(samplerConfig.Name.Data)) samplerConfig.Name = Guid.NewGuid().ToString("N");
                        sampler = deivce.CreateSamplerFromJson(samplerConfig.Name, samplerConfig.Config.Data);
                    }
                    var texture = manager.CreateTexture(name, bntex, sampler, node.Binding, node.ArrayElement, 
                        node.DescriptorCount, node.SetIndex, bntex.WriteDescriptorSet.DescriptorType);
                    return texture;
                case Constant.DDS:
                    var dds = await CreateDdsTextureData(manager, node);
                    if (samplerConfig != null)
                    {
                        if (string.IsNullOrEmpty(samplerConfig.Name.Data)) samplerConfig.Name = Guid.NewGuid().ToString("N");
                        sampler = deivce.CreateSamplerFromJson(samplerConfig.Name, samplerConfig.Config.Data);
                    }
                    var ddsTexture = manager.CreateTexture(name, dds, sampler, node.Binding, node.ArrayElement,
                        node.DescriptorCount, node.SetIndex, dds.WriteDescriptorSet.DescriptorType);
                    return ddsTexture;
                case Constant.BN3DTEX:
                    var bn3d = await CreateBn3dtexTextureData(manager, node);
                    if (samplerConfig != null)
                    {
                        if (string.IsNullOrEmpty(samplerConfig.Name.Data)) samplerConfig.Name = Guid.NewGuid().ToString("N");
                        sampler = deivce.CreateSamplerFromJson(samplerConfig.Name, samplerConfig.Config.Data);
                    }
                    var bn3dTex = manager.CreateTexture(name, bn3d, sampler, node.Binding, node.ArrayElement, 
                        node.DescriptorCount, node.SetIndex, bn3d.WriteDescriptorSet.DescriptorType);
                    return bn3dTex;
                case Constant.BNHDT:
                    var bnhdt = await CreateBnhdttexTextureData(manager, node);
                    if (samplerConfig != null)
                    {
                        if (string.IsNullOrEmpty(samplerConfig.Name.Data)) samplerConfig.Name = Guid.NewGuid().ToString("N");
                        sampler = deivce.CreateSamplerFromJson(samplerConfig.Name, samplerConfig.Config.Data);
                    }
                    var bnhdtTex = manager.CreateTexture(name, bnhdt, sampler, node.Binding, node.ArrayElement, 
                        node.DescriptorCount, node.SetIndex, bnhdt.WriteDescriptorSet.DescriptorType);
                    return bnhdtTex;
                case Constant.BNTEXA:
                    var bntexa = await CreateBntexaTextureData(manager, node);
                    if (samplerConfig != null)
                    {
                        if (string.IsNullOrEmpty(samplerConfig.Name.Data)) samplerConfig.Name = Guid.NewGuid().ToString("N");
                        sampler = deivce.CreateSamplerFromJson(samplerConfig.Name, samplerConfig.Config.Data);
                    }
                    var bbntexaTex = manager.CreateTexture(name, bntexa, sampler, node.Binding, 
                        node.ArrayElement, node.DescriptorCount, node.SetIndex, bntexa.WriteDescriptorSet.DescriptorType);
                    return bbntexaTex;
                default:
                    throw new NotSupportedException("Unknow Image type");
            }
        }

        /// <summary>
        /// 创建纹理数据
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="node"></param>
        /// <returns></returns>
        private static async Task<BntexVulkanTextureData> CreateBntexTextureData(VulkanGraphicsManager manager, TextureNode node)
        {
            var image = node.Image;
            string name = string.IsNullOrEmpty(image.Name.Data) ? Guid.NewGuid().ToString("N") :
                image.Name.Data;
            var config = node.TexConfig.Config.Data;
            if (manager.TryGetTextureData(name, out VulkanTextureData oldData))
            {
                if (oldData is BntexVulkanTextureData tdata) return tdata;
                else throw new ArgumentException($"target name {name} is already used");
            }
            else
            {
                var tex = manager.CreateEmptyTextureData<BntexVulkanTextureData>(name);
                if (node.Usage == TextureUsage.CombinedImageSampler || node.Usage == TextureUsage.SampledImage) 
                    await tex.LoadTextureData(manager.VulkanDevice, image.ImageData.Arrays, config);
                else await tex.LoadStorageTextureData(manager.VulkanDevice, image.ImageData.Arrays, config);
                return tex;
            }
        }
        /// <summary>
        /// 创建纹理数据
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="node"></param>
        /// <returns></returns>
        private static async Task<Bn3dtexVulkanTextureData> CreateBn3dtexTextureData(VulkanGraphicsManager manager, TextureNode node)
        {
            var image = node.Image;
            string name = string.IsNullOrEmpty(image.Name.Data) ? Guid.NewGuid().ToString("N") :
                image.Name.Data;
            var config = node.TexConfig.Config.Data;
            if (manager.TryGetTextureData(name, out VulkanTextureData oldData))
            {
                if (oldData is Bn3dtexVulkanTextureData tdata) return tdata;
                else throw new ArgumentException($"target name {name} is already used");
            }
            else
            {
                var tex = manager.CreateEmptyTextureData<Bn3dtexVulkanTextureData>(name);
                if (node.Usage == TextureUsage.CombinedImageSampler || node.Usage == TextureUsage.SampledImage) 
                    await tex.LoadTextureData(manager.VulkanDevice, image.ImageData.Arrays, config);
                else await tex.LoadStorageTextureData(manager.VulkanDevice, image.ImageData.Arrays, config);
                return tex;
            }
        }
        /// <summary>
        /// 创建纹理数据
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="node"></param>
        /// <returns></returns>
        private static async Task<BntexaVulkanTextureData> CreateBntexaTextureData(VulkanGraphicsManager manager, TextureNode node)
        {
            var image = node.Image;
            string name = string.IsNullOrEmpty(image.Name.Data) ? Guid.NewGuid().ToString("N") :
                image.Name.Data;
            var config = node.TexConfig.Config.Data;
            if (manager.TryGetTextureData(name, out VulkanTextureData oldData))
            {
                if (oldData is BntexaVulkanTextureData tdata) return tdata;
                else throw new ArgumentException($"target name {name} is already used");
            }
            else
            {
                var tex = manager.CreateEmptyTextureData<BntexaVulkanTextureData>(name);
                if (node.Usage == TextureUsage.CombinedImageSampler || node.Usage == TextureUsage.SampledImage) 
                    await tex.LoadTextureData(manager.VulkanDevice, image.ImageData.Arrays, config);
                else await tex.LoadStorageTextureData(manager.VulkanDevice, image.ImageData.Arrays, config);
                return tex;
            }
        }
        /// <summary>
        /// 创建纹理数据
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="node"></param>
        /// <returns></returns>
        private static async Task<BnhdtVulkanTextureData> CreateBnhdttexTextureData(VulkanGraphicsManager manager, TextureNode node)
        {
            var image = node.Image;
            string name = string.IsNullOrEmpty(image.Name.Data) ? Guid.NewGuid().ToString("N") :
                image.Name.Data;
            var config = node.TexConfig.Config.Data;
            if (manager.TryGetTextureData(name, out VulkanTextureData oldData))
            {
                if (oldData is BnhdtVulkanTextureData tdata) return tdata;
                else throw new ArgumentException($"target name {name} is already used");
            }
            else
            {
                var data = manager.CreateEmptyTextureData<BnhdtVulkanTextureData>(name);
                if (node.Usage == TextureUsage.CombinedImageSampler || node.Usage == TextureUsage.SampledImage)
                    await data.LoadTextureData(manager.VulkanDevice, image.ImageData.Arrays, config);
                else
                    await data.LoadStorageTextureData(manager.VulkanDevice, image.ImageData.Arrays, config);
                return data;
            }
        }
        /// <summary>
        /// 创建纹理数据
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="node"></param>
        /// <returns></returns>
        private static async Task<DdsVulkanTextureData> CreateDdsTextureData(VulkanGraphicsManager manager, TextureNode node)
        {
            var image = node.Image;
            string name = string.IsNullOrEmpty(image.Name.Data) ? Guid.NewGuid().ToString("N") :
                image.Name.Data;
            var config = node.TexConfig.Config.Data;
            if (manager.TryGetTextureData(name, out VulkanTextureData oldData))
            {
                if (oldData is DdsVulkanTextureData tdata) return tdata;
                else throw new ArgumentException($"target name {name} is already used");
            }
            else
            {
                var tex = manager.CreateEmptyTextureData<DdsVulkanTextureData>(name);
                if (node.Usage == TextureUsage.CombinedImageSampler || node.Usage == TextureUsage.SampledImage) 
                    await tex.LoadTextureData(manager.VulkanDevice, image.ImageData.Arrays, config);
                else await tex.LoadStorageTextureData(manager.VulkanDevice, image.ImageData.Arrays, config);
                return tex;
            }
        }
    }
}
