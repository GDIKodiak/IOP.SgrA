﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using Silk.NET.Vulkan;
using Microsoft.Extensions.Configuration;
using System.Linq;
using Silk.NET.Core.Native;
using Silk.NET.Vulkan.Extensions.EXT;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using IOP.SgrA.SilkNet.Vulkan.Scripted;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public static partial class VulkanManagerExtension
    {
        /// <summary>
        /// 配置Vulkan对象
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="appInfo"></param>
        /// <param name="instanceInfo"></param>
        /// <returns></returns>
        public static VulkanGraphicsManager CreateVulkanInstance(this VulkanGraphicsManager manager, Action<ApplicationOption> appInfo, Action<InstanceCreateOption> instanceInfo)
        {
            if (appInfo == null) throw new ArgumentNullException(nameof(appInfo));
            if (instanceInfo == null) throw new ArgumentNullException(nameof(instanceInfo));
            ApplicationOption appOption = new ApplicationOption();
            appInfo(appOption);
            InstanceCreateOption instanceCreateOption = new InstanceCreateOption();
            instanceInfo(instanceCreateOption);
            return manager.CreateVulkanInstance(appOption, instanceCreateOption);
        }
        /// <summary>
        /// 配置Vulkan对象
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="appInfo"></param>
        /// <param name="instanceInfo"></param>
        /// <returns></returns>
        public static VulkanGraphicsManager CreateVulkanInstance(this VulkanGraphicsManager manager, ApplicationOption appInfo, InstanceCreateOption instanceInfo)
        {
            unsafe
            {
                var api = Vk.GetApi();
                var appName = PinnedObject<string>.Create(appInfo.ApplicationName);
                var engineName = PinnedObject<string>.Create(appInfo.EngineName);
                var ppExtension = instanceInfo.EnabledExtensionNames.ToPinnedBytes();
                var ppLayers = instanceInfo.EnabledLayerNames.ToPinnedBytes();
                ApplicationInfo application = new ApplicationInfo
                {
                    SType = StructureType.ApplicationInfo,
                    ApiVersion = appInfo.ApiVersion,
                    PApplicationName = appName.ToPointer(),
                    ApplicationVersion = appInfo.ApplicationVersion,
                    PEngineName = engineName.ToPointer(),
                    EngineVersion = appInfo.EngineVersion
                };
                InstanceCreateInfo createInfo = new InstanceCreateInfo
                {
                    SType = StructureType.InstanceCreateInfo,
                    PApplicationInfo = &application,
                    PpEnabledExtensionNames = ppExtension.ToPointerArray(),
                    EnabledExtensionCount = instanceInfo.EnabledExtensionNames == null ? 0 : (uint)instanceInfo.EnabledExtensionNames.Length,
                    PpEnabledLayerNames = ppLayers.ToPointerArray(),
                    EnabledLayerCount = instanceInfo.EnabledLayerNames == null ? 0 : (uint)instanceInfo.EnabledLayerNames.Length
                };
                var result = api.CreateInstance(in createInfo, null, out Instance instance);
                appName.Free(); engineName.Free(); ppExtension.Free(); ppLayers.Free();
                if (result != Result.Success) throw new VulkanOperationException($"Create {nameof(Instance)}", result);
                VulkanInstance vulkanInstance = new VulkanInstance(api, instance);
                manager.Instance = vulkanInstance;
                manager.NativeAPI = api;
                manager.PhysicalDevices = vulkanInstance.EnumeratePhysicalDevices();
                if (!manager.PhysicalDevices.Any()) throw new NullReferenceException("No Physical Device could be used");
                return manager;
            }
        }
        /// <summary>
        /// 从配置项中创建Vulkan实例
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="appNodeName"></param>
        /// <param name="instanceNodeName"></param>
        /// <returns></returns>
        public static VulkanGraphicsManager CreateVulkanInstanceFromConfiguration(this VulkanGraphicsManager manager, string appNodeName = nameof(ApplicationOption), string instanceNodeName = nameof(InstanceCreateOption))
        {
            var appJson = manager.Configuration.GetValue<string>(appNodeName);
            if (string.IsNullOrEmpty(appJson)) throw new ArgumentNullException($"Cannot get {nameof(ApplicationOption)} form configuration, node name {appNodeName}");
            ApplicationOption application = JsonSerializer.Deserialize<ApplicationOption>(appJson);

            var instanceJson = manager.Configuration.GetValue<string>(instanceNodeName);
            if (string.IsNullOrEmpty(instanceJson)) throw new ArgumentNullException($"Cannot get {nameof(InstanceCreateOption)} form configuration, node name {instanceNodeName}");
            InstanceCreateOption instanceCreate = JsonSerializer.Deserialize<InstanceCreateOption>(instanceJson);
            return manager.CreateVulkanInstance(application, instanceCreate);
        }

        /// <summary>
        /// 创建Debug上报回调
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="callback"></param>
        /// <param name="flags"></param>
        /// <returns></returns>
        public static VulkanGraphicsManager CreateDebugReportCallback(this VulkanGraphicsManager manager, DebugReportFunction callback, DebugReportFlagsEXT flags = DebugReportFlagsEXT.ErrorBitExt | DebugReportFlagsEXT.WarningBitExt)
        {
            var i = manager.Instance;
            if (i == null) throw new NullReferenceException("Please create VkInstance first before CreateDebugReportCallback");
            var callBack = i.CreateDebugReportCallback(callback, flags);
            return manager;
        }

        /// <summary>
        /// 创建逻辑设备
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="physicalDevice"></param>
        /// <param name="deviceinfo"></param>
        /// <param name="queueCreateFunc"></param>
        /// <returns></returns>
        public static VulkanDevice CreateVulkanDevice(this VulkanGraphicsManager manager, VulkanPhysicalDevice physicalDevice,
            Action<DeviceCreateOption> deviceinfo,
            Func<VulkanPhysicalDevice, IEnumerable<QueueFamilyIndices>> queueCreateFunc)
        {
            if (physicalDevice == null) throw new ArgumentNullException(nameof(physicalDevice));
            if (physicalDevice.RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invaid PhysicalDevice");
            if (deviceinfo == null) throw new ArgumentNullException(nameof(deviceinfo));
            if (queueCreateFunc == null) throw new ArgumentNullException(nameof(queueCreateFunc));
            DeviceCreateOption deviceCreateOption = new DeviceCreateOption();
            deviceinfo(deviceCreateOption);
            List<DeviceQueueCreateInfo> queueCreateInfos = new List<DeviceQueueCreateInfo>();
            IEnumerable<QueueFamilyIndices> indices = queueCreateFunc(physicalDevice);
            if (!indices.Any()) throw new NotSupportedException("QueueFamily Count must be greater than 1");
            unsafe
            {
                DeviceCreateInfo createInfo = new DeviceCreateInfo();
                float* f = stackalloc float[] { 1.0f };
                foreach (var item in indices)
                {
                    DeviceQueueCreateInfo info = new DeviceQueueCreateInfo
                    {
                        SType = StructureType.DeviceQueueCreateInfo,
                        QueueFamilyIndex = item.FamilyIndex,
                        QueueCount = 1,
                        PQueuePriorities = f
                    };
                    queueCreateInfos.Add(info);
                }
                var ppExtension = deviceCreateOption.EnabledExtensionNames.ToPinnedBytes();
                var ppLayers = deviceCreateOption.EnabledLayerNames.ToPinnedBytes();
                fixed (DeviceQueueCreateInfo* qP = queueCreateInfos.ToArray())
                {
                    createInfo.SType = StructureType.DeviceCreateInfo;
                    createInfo.PQueueCreateInfos = qP;
                    createInfo.QueueCreateInfoCount = (uint)queueCreateInfos.Count;
                    createInfo.PpEnabledExtensionNames = ppExtension.ToPointerArray();
                    createInfo.PpEnabledLayerNames = ppLayers.ToPointerArray();
                    createInfo.EnabledExtensionCount = (uint)ppExtension.Length;
                    createInfo.EnabledLayerCount = (uint)ppLayers.Length;
                    #region Features2
                    if (deviceCreateOption.EnabledFeatures2 != null)
                    {
                        var feature = deviceCreateOption.EnabledFeatures2;
                        feature = feature.GetRoot();
                        void* pointer = feature.GetPointer();
                        createInfo.PNext = pointer;
                    }
                    #endregion
                    var fP = deviceCreateOption.EnabledFeatures == null ? physicalDevice.GetFeatures() : deviceCreateOption.EnabledFeatures.Value;
                    createInfo.PEnabledFeatures = &fP;
                    var api = manager.NativeAPI;
                    var r = api.CreateDevice(physicalDevice.RawHandle, in createInfo, null, out Device device);
                    ppExtension.Free(); ppLayers.Free();
                    deviceCreateOption.EnabledFeatures2?.Free();
                    if (r != Result.Success) throw new VulkanOperationException("CreateDevice", r);
                    VulkanDevice vulkanDevice = new VulkanDevice(api, physicalDevice.Instance, device, physicalDevice);
                    vulkanDevice.FamilyIndices = indices.ToArray();
                    manager.VulkanDevice = vulkanDevice;
                    return vulkanDevice;
                }
            }
        }

        /// <summary>
        /// 创建默认配置的交换链
        /// </summary>
        /// <param name="logicDevice"></param>
        /// <param name="window"></param>
        /// <returns></returns>
        public static VulkanSwapchain CreateDefaultSwapChain(this VulkanDevice logicDevice, VulkanGlfwWindow window)
        {
            var option = window.Option;
            return logicDevice.CreateSwapChain(window,
                (formats) =>
                {
                    if (formats.Length == 1 && formats[0].Format == Format.Undefined)
                    {
                        return new SurfaceFormatKHR
                        {
                            Format = Format.B8G8R8A8Unorm,
                            ColorSpace = ColorSpaceKHR.PaceSrgbNonlinearKhr
                        };
                    }
                    foreach (var format in formats)
                    {
                        if (format.Format == Format.B8G8R8A8Unorm && format.ColorSpace == ColorSpaceKHR.PaceSrgbNonlinearKhr)
                            return format;
                    }
                    return formats[0];
                },
                (modes) => modes.Contains(PresentModeKHR.MailboxKhr) ? PresentModeKHR.MailboxKhr : PresentModeKHR.FifoKhr,
                (cap) =>
                {
                    if (cap.CurrentExtent.Width != uint.MaxValue) return cap.CurrentExtent;
                    else
                    {
                        return new Extent2D
                        {
                            Width = Math.Max(cap.MinImageExtent.Width, Math.Min(cap.MaxImageExtent.Width, option.Width)),
                            Height = Math.Max(cap.MinImageExtent.Height, Math.Min(cap.MaxImageExtent.Height, option.Height))
                        };
                    }
                });
        }
        /// <summary>
        /// 创建默认交换链视图
        /// </summary>
        /// <param name="swapchain"></param>
        /// <returns></returns>
        public static VulkanSwapchain CreateDefaultSwapChainImageViews(this VulkanSwapchain swapchain)
        {
            var images = swapchain.GetSwapchainImages();
            swapchain.CreateSwapchainImageViews((image) =>
            {
                VulkanImageView[] views = new VulkanImageView[images.Length];
                int index = 0;
                Extent2D extent2D = swapchain.SwapChainExtent;
                foreach(var item in images)
                {
                    item.CreateOption = new ImageCreateOption() { Extent = new Extent3D(extent2D.Width, extent2D.Height, 1) };
                    views[index] = item.CreateImageView(new ImageViewCreateOption()
                    {
                        Image = item.RawHandle,
                        Format = swapchain.SwapChainFormat.Format,
                        ViewType = ImageViewType.Type2D,
                        Components = new ComponentMapping(ComponentSwizzle.R, ComponentSwizzle.G, ComponentSwizzle.B, ComponentSwizzle.A),
                        SubresourceRange = new ImageSubresourceRange(ImageAspectFlags.ColorBit, 0, 1, 0, 1)
                    });
                    index++;
                }
                return views;
            });
            return swapchain;
        }

        /// <summary>
        /// 创建深度缓冲
        /// </summary>
        /// <param name="logicDevice"></param>
        /// <param name="imageCreateAction"></param>
        /// <param name="imageViewCreateAction"></param>
        /// <param name="memoryOffest"></param>
        /// <returns></returns>
        public static VulkanImageView CreateDepthBuffer(this VulkanDevice logicDevice,
            Func<VulkanPhysicalDevice, ImageCreateOption, bool> imageCreateAction,
            Action<ImageViewCreateOption> imageViewCreateAction, uint memoryOffest = 0)
        {
            if (imageCreateAction == null) throw new ArgumentNullException(nameof(imageCreateAction));
            if (imageViewCreateAction == null) throw new ArgumentNullException(nameof(imageViewCreateAction));
            var phy = logicDevice.PhysicalDevice;
            ImageCreateOption imageCreate = new ImageCreateOption();
            bool isSupport = imageCreateAction(phy, imageCreate);
            if (isSupport)
            {
                ImageAndImageViewCreateOption option = new ImageAndImageViewCreateOption();
                ImageViewCreateOption imageViewCreate = new ImageViewCreateOption();
                imageViewCreateAction(imageViewCreate);
                option.ImageCreateOption = imageCreate;
                option.ImageViewCreateOption = imageViewCreate;
                var buffer = logicDevice.CreateDepthBuffer(option);
                return buffer;
            }
            else throw new NotSupportedException($"Cannot found supported {nameof(ImageTiling)} to create depth buffer image, target format : {imageCreate.Format}");
        }
        /// <summary>
        /// 创建深度缓冲
        /// </summary>
        /// <param name="logicDevice"></param>
        /// <param name="createOption"></param>
        /// <returns></returns>
        public static VulkanImageView CreateDepthBuffer(this VulkanDevice logicDevice, ImageAndImageViewCreateOption createOption)
        {
            createOption.ImageCreateOption.Usage = ImageUsageFlags.DepthStencilAttachmentBit;
            var imageNode = logicDevice.CreateImage(createOption, createOption.ImageCreateOption.Extent);
            return imageNode;
        }
        /// <summary>
        /// 创建默认的深度缓冲
        /// </summary>
        /// <param name="logicDevice"></param>
        /// <param name="format"></param>
        /// <param name="height"></param>
        /// <param name="width"></param>
        /// <returns></returns>
        public static VulkanImageView CreateDefaultDepthBuffer(this VulkanDevice logicDevice, Format format, uint width, uint height)
        {
            VulkanImageView dImage = logicDevice.CreateDepthBuffer((device, option) =>
            {
                var props = device.GetFormatProperties(format);
                option.Format = format;
                option.ImageType = ImageType.Type2D;
                option.Extent = new Extent3D(width, height, 1);
                option.MipLevels = 1;
                option.ArrayLayers = 1;
                option.Samples = SampleCountFlags.Count1Bit;
                option.InitialLayout = ImageLayout.Undefined;
                option.SharingMode = SharingMode.Exclusive;
                if (props.LinearTilingFeatures.HasFlag(FormatFeatureFlags.DepthStencilAttachmentBit))
                {
                    option.Tiling = ImageTiling.Linear;
                    return true;
                }
                else if (props.OptimalTilingFeatures.HasFlag(FormatFeatureFlags.DepthStencilAttachmentBit))
                {
                    option.Tiling = ImageTiling.Optimal;
                    return true;
                }
                else return false;
            }, (option) =>
            {
                option.Format = format;
                option.Components = new ComponentMapping(ComponentSwizzle.R, ComponentSwizzle.G, ComponentSwizzle.B, ComponentSwizzle.A);
                option.SubresourceRange = new ImageSubresourceRange(ImageAspectFlags.DepthBit, 0, 1, 0, 1);
                option.ViewType = ImageViewType.Type2D;
            });
            return dImage;
        }
        /// <summary>
        /// 从配置项中创建深度缓冲
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="nodeName"></param>
        /// <returns></returns>
        public static VulkanImageView CreateDepthBufferFromConfiguration(this VulkanGraphicsManager manager, string nodeName = "DepthBufferCreateOption")
        {
            var appJson = manager.Configuration.GetValue<string>(nodeName);
            if (string.IsNullOrEmpty(appJson)) throw new ArgumentNullException($"Cannot get {nameof(ImageAndImageViewCreateOption)} form configuration, node name {nodeName}");
            ImageAndImageViewCreateOption createOption = JsonSerializer.Deserialize<ImageAndImageViewCreateOption>(appJson);
            if (createOption.ImageCreateOption == null) throw new NullReferenceException($"{nameof(ImageCreateOption)} is null");
            if (createOption.ImageViewCreateOption == null) throw new NullReferenceException($"{nameof(ImageViewCreateOption)} is null");
            var device = manager.VulkanDevice;
            var phyDevice = device.PhysicalDevice;
            var props = phyDevice.GetFormatProperties(createOption.ImageCreateOption.Format);
            if (props.LinearTilingFeatures.HasFlag(FormatFeatureFlags.DepthStencilAttachmentBit))
                createOption.ImageCreateOption.Tiling = ImageTiling.Linear;
            else if (props.OptimalTilingFeatures.HasFlag(FormatFeatureFlags.DepthStencilAttachmentBit))
                createOption.ImageCreateOption.Tiling = ImageTiling.Optimal;
            else throw new NotSupportedException($"Cannot found supported {nameof(ImageTiling)} to create depth buffer image, target format : {createOption.ImageCreateOption.Format}");
            createOption.ImageViewCreateOption.Format = createOption.ImageCreateOption.Format;
            var buffer = device.CreateDepthBuffer(createOption);
            return buffer;
        }
        /// <summary>
        /// 从Json字符串中创建深度缓冲
        /// </summary>
        /// <param name="logicDevice"></param>
        /// <param name="configJson"></param>
        /// <returns></returns>
        public static VulkanImageView CreateDepthBufferFromJson(this VulkanDevice logicDevice, string configJson)
        {
            ImageAndImageViewCreateOption createOption = JsonSerializer.Deserialize<ImageAndImageViewCreateOption>(configJson);
            if (createOption.ImageCreateOption == null) throw new NullReferenceException($"{nameof(ImageCreateOption)} is null");
            if (createOption.ImageViewCreateOption == null) throw new NullReferenceException($"{nameof(ImageViewCreateOption)} is null");
            var phyDevice = logicDevice.PhysicalDevice;
            var props = phyDevice.GetFormatProperties(createOption.ImageCreateOption.Format);
            if (props.LinearTilingFeatures.HasFlag(FormatFeatureFlags.DepthStencilAttachmentBit))
                createOption.ImageCreateOption.Tiling = ImageTiling.Linear;
            else if (props.OptimalTilingFeatures.HasFlag(FormatFeatureFlags.DepthStencilAttachmentBit))
                createOption.ImageCreateOption.Tiling = ImageTiling.Optimal;
            else throw new NotSupportedException($"Cannot found supported {nameof(ImageTiling)} to create depth buffer image, target format : {createOption.ImageCreateOption.Format}");
            createOption.ImageViewCreateOption.Format = createOption.ImageCreateOption.Format;
            var buffer = logicDevice.CreateDepthBuffer(createOption);
            return buffer;
        }
        /// <summary>
        /// 从Json字符串中创建深度缓冲
        /// </summary>
        /// <param name="logicDevice"></param>
        /// <param name="configJson"></param>
        /// <param name="extent"></param>
        /// <returns></returns>
        public static VulkanImageView CreateDepthBufferFromJson(this VulkanDevice logicDevice, string configJson, Extent3D extent)
        {
            ImageAndImageViewCreateOption createOption = JsonSerializer.Deserialize<ImageAndImageViewCreateOption>(configJson);
            if (createOption.ImageCreateOption == null) throw new NullReferenceException($"{nameof(ImageCreateOption)} is null");
            if (createOption.ImageViewCreateOption == null) throw new NullReferenceException($"{nameof(ImageViewCreateOption)} is null");
            var phyDevice = logicDevice.PhysicalDevice;
            var props = phyDevice.GetFormatProperties(createOption.ImageCreateOption.Format);
            if (props.LinearTilingFeatures.HasFlag(FormatFeatureFlags.DepthStencilAttachmentBit))
                createOption.ImageCreateOption.Tiling = ImageTiling.Linear;
            else if (props.OptimalTilingFeatures.HasFlag(FormatFeatureFlags.DepthStencilAttachmentBit))
                createOption.ImageCreateOption.Tiling = ImageTiling.Optimal;
            else throw new NotSupportedException($"Cannot found supported {nameof(ImageTiling)} to create depth buffer image, target format : {createOption.ImageCreateOption.Format}");
            createOption.ImageCreateOption.Extent = extent;
            createOption.ImageViewCreateOption.Format = createOption.ImageCreateOption.Format;
            var buffer = logicDevice.CreateDepthBuffer(createOption);
            return buffer;
        }

        /// <summary>
        /// 创建渲染通道
        /// </summary>
        /// <param name="logicDevice"></param>
        /// <param name="name"></param>
        /// <param name="attachmentFunc"></param>
        /// <param name="subpassFunc"></param>
        /// <param name="dependFunc"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public static VulkanRenderPass CreateRenderPass(this VulkanDevice logicDevice, string name,
            Func<VulkanDevice, AttachmentDescription[]> attachmentFunc,
            Func<VulkanDevice, SubpassDescriptionOption[]> subpassFunc,
            Func<VulkanDevice, SubpassDependency[]> dependFunc, RenderPassMode mode = RenderPassMode.Normal)
        {
            if (attachmentFunc == null) throw new ArgumentNullException(nameof(attachmentFunc));
            if (subpassFunc == null) throw new ArgumentNullException(nameof(subpassFunc));
            if (dependFunc == null) throw new ArgumentNullException(nameof(dependFunc));
            AttachmentDescription[] attachments = attachmentFunc(logicDevice);
            SubpassDescriptionOption[] subpasses = subpassFunc(logicDevice);
            SubpassDependency[] dependencies = dependFunc(logicDevice);
            RenderPassCreateOption createOption = new RenderPassCreateOption();
            createOption.Attachments = attachments;
            createOption.Subpasses = subpasses;
            createOption.Dependencies = dependencies;
            createOption.Name = name;
            return logicDevice.CreateRenderPass(name, createOption, mode);
        }
        /// <summary>
        /// 从配置项中创建渲染通道
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="logicDevice"></param>
        /// <param name="name"></param>
        /// <param name="formats"></param>
        /// <param name="mode"></param>
        /// <param name="nodeName"></param>
        /// <returns></returns>
        public static VulkanRenderPass CreateRenderPassFromConfiguration(this VulkanGraphicsManager manager, VulkanDevice logicDevice,
            string name, Format[] formats, RenderPassMode mode, string nodeName = nameof(RenderPassCreateOption))
        {
            var appJson = manager.Configuration.GetValue<string>(nodeName);
            if (string.IsNullOrEmpty(appJson)) throw new ArgumentNullException($"Cannot get {nameof(RenderPassCreateOption)} form configuration, node name {nodeName}");
            return logicDevice.CreateRenderPassFromJson(name, formats, mode, appJson);
        }
        /// <summary>
        /// 创建交换链渲染通道
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="logicDevice"></param>
        /// <param name="name"></param>
        /// <param name="swapchain"></param>
        /// <param name="extraFormats"></param>
        /// <param name="nodeName"></param>
        /// <returns></returns>
        public static VulkanRenderPass CreateSwapchainRenderPassFromConfiguration(this VulkanGraphicsManager manager, VulkanDevice logicDevice,
            string name, VulkanSwapchain swapchain, Format[] extraFormats, string nodeName = nameof(RenderPassCreateOption))
        {
            var appJson = manager.Configuration.GetValue<string>(nodeName);
            if (string.IsNullOrEmpty(appJson)) throw new ArgumentNullException($"Cannot get {nameof(RenderPassCreateOption)} form configuration, node name {nodeName}");
            return CreateSwapchainRenderPassFromJson(logicDevice, name, swapchain, extraFormats, appJson);
        }
        /// <summary>
        /// 从Json文件创建渲染通道
        /// </summary>
        /// <param name="logicDevice"></param>
        /// <param name="name"></param>
        /// <param name="formats"></param>
        /// <param name="mode"></param>
        /// <param name="configJson"></param>
        /// <returns></returns>
        public static VulkanRenderPass CreateRenderPassFromJson(this VulkanDevice logicDevice, 
            string name, Format[] formats, RenderPassMode mode, string configJson)
        {
            if (string.IsNullOrEmpty(configJson)) throw new ArgumentNullException($"Cannot get {nameof(RenderPassCreateOption)} form json");
            RenderPassCreateOption createOption = JsonSerializer.Deserialize<RenderPassCreateOption>(configJson);
            for (int i = 0; i < formats.Length; i++)
            {
                createOption.Attachments[i].Format = formats[i];
            }
            return logicDevice.CreateRenderPass(name, createOption, mode);
        }
        /// <summary>
        /// 从Json字符串中创建交换链渲染通道
        /// </summary>
        /// <param name="logicDevice"></param>
        /// <param name="name"></param>
        /// <param name="swapchain"></param>
        /// <param name="extraFormats"></param>
        /// <param name="json"></param>
        /// <returns></returns>
        public static VulkanRenderPass CreateSwapchainRenderPassFromJson(this VulkanDevice logicDevice,
            string name, VulkanSwapchain swapchain, Format[] extraFormats, string json)
        {
            if (string.IsNullOrEmpty(json)) throw new ArgumentNullException($"No config json");
            RenderPassCreateOption createOption = JsonSerializer.Deserialize<RenderPassCreateOption>(json);
            createOption.Attachments[0].Format = swapchain.SwapChainFormat.Format;
            for (int i = 0; i < extraFormats.Length; i++)
            {
                createOption.Attachments[i + 1].Format = extraFormats[i];
            }
            var pass = logicDevice.CreateRenderPass(name, createOption, RenderPassMode.Swapchain);
            return pass;
        }
        /// <summary>
        /// 创建脚本化渲染通道
        /// </summary>
        /// <typeparam name="TRenderPass"></typeparam>
        /// <param name="logicDevice"></param>
        /// <param name="area"></param>
        /// <param name="sample"></param>
        /// <returns></returns>
        public static VulkanRenderPass CreateScriptedRenderPass<TRenderPass>(this VulkanDevice logicDevice, Area area, SampleCount sample = SampleCount.Sample1X)
            where TRenderPass : ScriptedVulkanRenderPass, new()
        {
            TRenderPass pass = new TRenderPass();
            pass.GlobalSampleCount = sample;
            VulkanRenderPass result = pass.Build(logicDevice, area);
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TRenderPass"></typeparam>
        /// <param name="logicDevice"></param>
        /// <param name="swapchain"></param>
        /// <param name="area"></param>
        /// <param name="sampleCount"></param>
        /// <returns></returns>
        public static VulkanRenderPass CreateSwapchainScriptedRenderPass<TRenderPass>(this VulkanDevice logicDevice, VulkanSwapchain swapchain, 
            Area area, SampleCount sampleCount = SampleCount.Sample1X)
            where TRenderPass : ScriptedVulkanRenderPass, new()
        {
            TRenderPass pass = new TRenderPass();
            pass.GlobalSampleCount = sampleCount;
            VulkanRenderPass result = pass.Build(logicDevice, swapchain, area);
            return result;
        }

        /// <summary>
        /// 创建交换链帧缓冲
        /// 交换链颜色附件必须为第一个
        /// </summary>
        /// <param name="renderPass"></param>
        /// <param name="swapchain"></param>
        /// <param name="extraAttachments"></param>
        /// <param name="height"></param>
        /// <param name="width"></param>
        /// <param name="layers"></param>
        /// <returns></returns>
        public static VulkanRenderPass CreateSwapchainFrameBuffer(this VulkanRenderPass renderPass, VulkanSwapchain swapchain,
            VulkanImageView[] extraAttachments, uint height, uint width, uint layers = 1)
        {
            if (extraAttachments == null) throw new ArgumentNullException(nameof(extraAttachments));
            if (renderPass.Mode != RenderPassMode.Swapchain) throw new NotSupportedException("Target renderpass not supported swapchain");
            var sImages = swapchain.Views;
            if (sImages == null) throw new InvalidOperationException("Please create swapchain image view first");
            foreach (var item in sImages)
            {
                VulkanImageView[] attach = new VulkanImageView[extraAttachments.Length + 1];
                attach[0] = item;
                for (int i = 1; i < attach.Length; i++) attach[i] = extraAttachments[i - 1];
                renderPass.CreateFrameBuffer(attach, width, height, layers);
            }
            return renderPass;
        }
        /// <summary>
        /// 创建交换链帧缓冲
        /// 交换链颜色附件必须为第一个
        /// </summary>
        /// <param name="renderPass"></param>
        /// <param name="swapchain"></param>
        /// <param name="extraAttachments"></param>
        /// <param name="height"></param>
        /// <param name="width"></param>
        /// <param name="layers"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="NotSupportedException"></exception>
        /// <exception cref="InvalidOperationException"></exception>
        public static VulkanRenderPass CreateSwapchainFrameBuffer(this VulkanRenderPass renderPass, VulkanSwapchain swapchain,
            ImageAndImageViewCreateOption[] extraAttachments, uint height, uint width, uint layers = 1)
        {
            if (extraAttachments == null) throw new ArgumentNullException(nameof(extraAttachments));
            if (renderPass.Mode != RenderPassMode.Swapchain) throw new NotSupportedException("Target renderpass not supported swapchain");
            var sImages = swapchain.Views;
            var device = renderPass.Device;
            if (sImages == null) throw new InvalidOperationException("Please create swapchain image view first");
            Extent3D extent = new Extent3D(width, height, 1);
            foreach (var item in sImages)
            {
                VulkanImageView[] attach = new VulkanImageView[extraAttachments.Length + 1];
                attach[0] = item;
                for (int i = 1; i < attach.Length; i++)
                {
                    attach[i] = device.CreateImage(extraAttachments[i - 1], extent);
                }
                renderPass.CreateFrameBuffer(attach, width, height, layers);
            }
            return renderPass;
        }
        /// <summary>
        /// 重建交换链帧缓冲
        /// </summary>
        /// <param name="renderPass"></param>
        /// <param name="newSwapchain"></param>
        /// <param name="newWidth"></param>
        /// <param name="newHeight"></param>
        /// <param name="layers"></param>
        /// <returns></returns>
        public static VulkanRenderPass RecreateSwapchainFrameBuffer(this VulkanRenderPass renderPass, VulkanSwapchain newSwapchain,
            uint newWidth, uint newHeight, uint layers = 1)
        {
            if (renderPass.Mode != RenderPassMode.Swapchain) throw new NotSupportedException("Target Renderpass not supported swapchain");
            var sImages = newSwapchain.Views;
            var device = renderPass.Device;
            if (sImages == null) throw new InvalidOperationException("Please create swapchain image view first");
            var oldFramebuffer = renderPass.Framebuffer.ToArray();
            renderPass.ClearFrameBuffer();
            Dictionary<Guid, VulkanImageView> books = new Dictionary<Guid, VulkanImageView>();
            for(int i = 0; i < sImages.Length; i++)
            {
                var old = oldFramebuffer[i].Attachments;
                VulkanImageView[] newImages = new VulkanImageView[old.Length];
                newImages[0] = sImages[i];
                for(int j = 1; j < newImages.Length; j++)
                {
                    var l = old[j];
                    if (books.ContainsKey(l.Guid)) newImages[j] = books[l.Guid];
                    else
                    {
                        l = device.RecreateImage(l, new Extent3D(newWidth, newHeight, layers));
                        books.Add(l.Guid, l);
                        newImages[j] = l;
                    }
                }
                renderPass.CreateFrameBuffer(newImages, newWidth, newHeight, 1);
            }
            return renderPass;
        }
        /// <summary>
        /// 创建帧缓冲
        /// </summary>
        /// <param name="renderPass"></param>
        /// <param name="attachments"></param>
        /// <param name="layers"></param>
        /// <param name="height"></param>
        /// <param name="width"></param>
        /// <returns></returns>
        public static VulkanRenderPass CreateFrameBuffer(this VulkanRenderPass renderPass,
            ImageAndImageViewCreateOption[] attachments, uint height, uint width, uint layers = 1)
        {
            if (attachments == null) throw new ArgumentNullException(nameof(attachments));
            var device = renderPass.Device;
            Extent3D extent = new Extent3D(width, height, 1);
            VulkanImageView[] views = new VulkanImageView[attachments.Length];
            for (int j = 0; j < attachments.Length; j++)
            {
                views[j] = device.CreateImage(attachments[j], extent);
            }
            renderPass.CreateFrameBuffer(views, width, height, layers);
            return renderPass;
        }
        /// <summary>
        /// 创建离屏渲染帧缓冲
        /// </summary>
        /// <param name="renderPass"></param>
        /// <param name="attachments"></param>
        /// <param name="height"></param>
        /// <param name="width"></param>
        /// <param name="layers"></param>
        /// <param name="frameCount"></param>
        /// <returns></returns>
        public static VulkanRenderPass CreateOffScreenFrameBuffer(this VulkanRenderPass renderPass,
            ImageAndImageViewCreateOption[] attachments, uint height, uint width, uint layers, uint frameCount = 1)
        {
            if (attachments == null) throw new ArgumentNullException(nameof(attachments));
            if (renderPass.Mode == RenderPassMode.Swapchain) throw new NotSupportedException("Target renderpass is swapchain renderpass");
            var device = renderPass.Device;
            Extent3D extent = new Extent3D(width, height, 1);
            for(int i = 0; i < frameCount; i++)
            {
                VulkanImageView[] views = new VulkanImageView[attachments.Length];
                for(int j = 0; j < attachments.Length; j++)
                {
                    views[j] = device.CreateImage(attachments[j], extent);
                }
                renderPass.CreateFrameBuffer(views, width, height, layers);
            }
            return renderPass;
        }
        /// <summary>
        /// 从Json文件创建离屏渲染帧缓冲
        /// </summary>
        /// <param name="renderPass"></param>
        /// <param name="json"></param>
        /// <param name="height"></param>
        /// <param name="width"></param>
        /// <param name="layers"></param>
        /// <param name="frameCount"></param>
        /// <returns></returns>
        public static VulkanRenderPass CreateOffScreenFrameBufferFromJson(this VulkanRenderPass renderPass, string json,
            uint height, uint width, uint layers, uint frameCount = 1)
        {
            if (string.IsNullOrEmpty(json)) throw new ArgumentNullException(nameof(json));
            ImageAndImageViewCreateOption[] options = JsonSerializer.Deserialize<ImageAndImageViewCreateOption[]>(json);
            return CreateOffScreenFrameBuffer(renderPass, options, height, width, layers, frameCount);
        }
        /// <summary>
        /// 重建帧缓冲
        /// </summary>
        /// <param name="renderPass"></param>
        /// <param name="newHeight"></param>
        /// <param name="newWidth"></param>
        /// <param name="layers"></param>
        /// <returns></returns>
        public static VulkanRenderPass RecreateFrameBuffer(this VulkanRenderPass renderPass, uint newWidth, uint newHeight, uint layers = 1)
        {
            var device = renderPass.Device;
            var oldFramebuffer = renderPass.Framebuffer.ToArray();
            renderPass.ClearFrameBuffer();
            Dictionary<Guid, VulkanImageView> books = new Dictionary<Guid, VulkanImageView>();
            for (int i = 0; i < oldFramebuffer.Length; i++)
            {
                var old = oldFramebuffer[i].Attachments;
                VulkanImageView[] newImages = new VulkanImageView[old.Length];
                for (int j = 0; j < newImages.Length; j++)
                {
                    var l = old[j];
                    if (books.ContainsKey(l.Guid)) newImages[i] = l;
                    else
                    {
                        l = device.RecreateImage(l, new Extent3D(newWidth, newHeight, layers));
                        books.Add(l.Guid, l);
                        newImages[j] = l;
                    }
                }
                renderPass.CreateFrameBuffer(newImages, newWidth, newHeight, 1);
            }
            return renderPass;
        }

        /// <summary>
        /// 创建多个主要命令缓冲
        /// </summary>
        /// <param name="pool"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static VulkanCommandBuffer[] CreatePrimaryCommandBuffers(this VulkanCommandPool pool, uint count)
        {
            return pool.CreateCommandBuffers(CommandBufferLevel.Primary, count);
        }
        /// <summary>
        /// 创建主要命令缓冲
        /// </summary>
        /// <param name="pool"></param>
        /// <returns></returns>
        public static VulkanCommandBuffer CreatePrimaryCommandBuffer(this VulkanCommandPool pool)
        {
            return pool.CreateCommandBuffer(CommandBufferLevel.Primary);
        }
        /// <summary>
        /// 创建多个次要命令缓冲
        /// </summary>
        /// <param name="pool"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static VulkanCommandBuffer[] CreateSecondaryCommandBuffer(this VulkanCommandPool pool, uint count)
        {
            return pool.CreateCommandBuffers(CommandBufferLevel.Secondary, count);
        }
        /// <summary>
        /// 创建次要命令缓冲
        /// </summary>
        /// <param name="pool"></param>
        /// <returns></returns>
        public static VulkanCommandBuffer CreateSecondaryCommandBuffer(this VulkanCommandPool pool)
        {
            return pool.CreateCommandBuffer(CommandBufferLevel.Secondary);
        }

        /// <summary>
        /// 采样类型转换
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public static SampleCountFlags ToSampleFlags(this SampleCount count)
        {
            return count switch
            {
                SampleCount.Sample64X => SampleCountFlags.Count64Bit,
                SampleCount.Sample32X => SampleCountFlags.Count32Bit,
                SampleCount.Sample16X => SampleCountFlags.Count16Bit,
                SampleCount.Sample8X => SampleCountFlags.Count8Bit,
                SampleCount.Sample4X => SampleCountFlags.Count4Bit,
                SampleCount.Sample2X => SampleCountFlags.Count2Bit,
                _ => SampleCountFlags.Count1Bit,
            };
        }
    }
}
