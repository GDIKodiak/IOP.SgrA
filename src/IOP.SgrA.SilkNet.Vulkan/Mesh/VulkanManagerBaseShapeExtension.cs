﻿using IOP.SgrA.SilkNet.Vulkan.Mesh;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// Vulkan基础形状网格扩展
    /// </summary>
    public static partial class VulkanManagerMeshExtension
    {
        /// <summary>
        /// 创建一个标准单位圆
        /// </summary>
        /// <typeparam name="TMesh"></typeparam>
        /// <param name="vro"></param>
        /// <param name="angleSpan"></param>
        /// <param name="logicDevice"></param>
        /// <param name="sharingMode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <returns></returns>
        public static TMesh CreateUnitSphere<TMesh>(this TMesh vro, float angleSpan,
            VulkanDevice logicDevice, SharingMode sharingMode, uint[] queueFamilyIndices = null)
            where TMesh : VRO
        {
            Span<Vector3> data = Sphere.CreateUnitSphere(angleSpan, out uint v);
            vro.VecticesCount = v;
            var bytes = MemoryMarshal.AsBytes(data);
            vro.VerticesBufferInfo = CreateNewBuffer(vro, bytes, BufferUsageFlags.VertexBufferBit,
                logicDevice, sharingMode, queueFamilyIndices);
            return vro;
        }
    }
}
