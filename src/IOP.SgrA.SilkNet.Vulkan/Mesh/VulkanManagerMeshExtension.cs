﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public static partial class VulkanManagerMeshExtension
    {
        /// <summary>
        /// 创建顶点缓冲
        /// </summary>
        /// <typeparam name="TMesh"></typeparam>
        /// <param name="vro"></param>
        /// <param name="data"></param>
        /// <param name="vecticesCount"></param>
        /// <param name="logicDevice"></param>
        /// <param name="sharingMode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <returns></returns>
        public static TMesh CreateVecticesBuffer<TMesh>(this TMesh vro, float[] data, uint vecticesCount, 
            VulkanDevice logicDevice, SharingMode sharingMode, uint[] queueFamilyIndices = null)
            where TMesh : VRO
        {
            vro.VecticesCount = vecticesCount;
            var span = data.AsSpan();
            var bytes = MemoryMarshal.AsBytes(span);
            vro.VerticesBufferInfo = CreateNewBuffer(vro, bytes, BufferUsageFlags.VertexBufferBit, 
                logicDevice, sharingMode, queueFamilyIndices);
            return vro;
        }
        /// <summary>
        /// 创建顶点缓冲
        /// </summary>
        /// <typeparam name="TMesh"></typeparam>
        /// <param name="vro"></param>
        /// <param name="data"></param>
        /// <param name="vecticesCount"></param>
        /// <param name="logicDevice"></param>
        /// <param name="sharingMode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <returns></returns>
        public static TMesh CreateVecticesBuffer<TMesh>(this TMesh vro, Span<float> data, uint vecticesCount,
            VulkanDevice logicDevice, SharingMode sharingMode, uint[] queueFamilyIndices = null)
            where TMesh : VRO
        {
            vro.VecticesCount = vecticesCount;
            var bytes = MemoryMarshal.AsBytes(data);
            vro.VerticesBufferInfo = CreateNewBuffer(vro, bytes, BufferUsageFlags.VertexBufferBit,
                logicDevice, sharingMode, queueFamilyIndices);
            return vro;
        }
        /// <summary>
        /// 创建顶点缓冲
        /// </summary>
        /// <typeparam name="TMesh"></typeparam>
        /// <param name="vro"></param>
        /// <param name="data"></param>
        /// <param name="vecticesCount"></param>
        /// <param name="logicDevice"></param>
        /// <param name="sharingMode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <returns></returns>
        public static TMesh CreateVecticesBuffer<TMesh>(this TMesh vro, byte[] data, uint vecticesCount, VulkanDevice logicDevice, 
            SharingMode sharingMode, uint[] queueFamilyIndices = null)
            where TMesh : VRO
        {
            vro.VecticesCount = vecticesCount;
            var span = data.AsSpan();
            var bytes = MemoryMarshal.AsBytes(span);
            vro.VerticesBufferInfo = CreateNewBuffer(vro, bytes, BufferUsageFlags.VertexBufferBit,
                logicDevice, sharingMode, queueFamilyIndices);
            return vro;
        }
        /// <summary>
        /// 创建顶点缓冲
        /// </summary>
        /// <param name="vro"></param>
        /// <param name="ptr"></param>
        /// <param name="bytesSize"></param>
        /// <param name="vecticesCount"></param>
        /// <param name="logicDevice"></param>
        /// <param name="sharingMode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <returns></returns>
        public static TMesh CreateVecticesBuffer<TMesh>(this TMesh vro, in IntPtr ptr, ulong bytesSize, uint vecticesCount,
            VulkanDevice logicDevice, SharingMode sharingMode, uint[] queueFamilyIndices = null)
            where TMesh : VRO
        {
            vro.VecticesCount = vecticesCount;
            vro.VerticesBufferInfo = CreateNewBuffer(vro, in ptr, bytesSize, BufferUsageFlags.VertexBufferBit, logicDevice, sharingMode, queueFamilyIndices);
            return vro;
        }

        /// <summary>
        /// 创建索引缓冲
        /// </summary>
        /// <typeparam name="TMesh"></typeparam>
        /// <param name="vro"></param>
        /// <param name="indexes"></param>
        /// <param name="logicDevice"></param>
        /// <param name="sharingMode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <returns></returns>
        public static TMesh CreateIndexesBuffer<TMesh>(this TMesh vro, 
            uint[] indexes, VulkanDevice logicDevice, SharingMode sharingMode, uint[] queueFamilyIndices = null)
            where TMesh : VRO
        {
            vro.IndexesCount = (uint)(indexes.Length);
            var span = indexes.AsSpan();
            var bytes = MemoryMarshal.AsBytes(span);
            vro.IndexesBufferInfo = CreateNewBuffer(vro, bytes, BufferUsageFlags.IndexBufferBit, logicDevice, sharingMode, queueFamilyIndices);
            return vro;
        }
        /// <summary>
        /// 创建索引缓冲
        /// </summary>
        /// <typeparam name="TMesh"></typeparam>
        /// <param name="vro"></param>
        /// <param name="indexes"></param>
        /// <param name="indexesCount"></param>
        /// <param name="logicDevice"></param>
        /// <param name="sharingMode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <returns></returns>
        public static TMesh CreateIndexesBuffer<TMesh>(this TMesh vro, byte[] indexes, uint indexesCount,
            VulkanDevice logicDevice, SharingMode sharingMode, uint[] queueFamilyIndices = null)
            where TMesh : VRO
        {
            vro.IndexesCount = indexesCount;
            Span<byte> span = indexes;
            vro.IndexesBufferInfo = CreateNewBuffer(vro, span, BufferUsageFlags.IndexBufferBit, 
                logicDevice, sharingMode, queueFamilyIndices);
            return vro;
        }

        /// <summary>
        /// 创建间接绘制
        /// </summary>
        /// <typeparam name="TMesh"></typeparam>
        /// <param name="vro"></param>
        /// <param name="commands"></param>
        /// <param name="logicDevice"></param>
        /// <param name="sharingMode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <returns></returns>
        public static TMesh CreateOrUpdateDrawIndirectBuffer<TMesh>(this TMesh vro, DrawIndirectCommand[] commands,
            VulkanDevice logicDevice, SharingMode sharingMode, uint[] queueFamilyIndices = null)
            where TMesh : VRO
        {
            if (commands == null || commands.Length == 0) throw new ArgumentNullException(nameof(commands));
            var span = new Span<DrawIndirectCommand>(commands);
            var bytes = MemoryMarshal.AsBytes(span);
            if (vro.DrawIndirectBufferInfo.Buffer == null)
            {
                vro.DrawIndirectBufferInfo = CreateNewBuffer(vro, bytes, BufferUsageFlags.IndirectBufferBit, 
                    logicDevice, sharingMode, queueFamilyIndices);
            }
            else
            {
                var old = vro.DrawIndirectBufferInfo;
                if ((ulong)bytes.Length > old.Size)
                {
                    vro.DrawIndirectBufferInfo = CreateNewBuffer(vro, bytes, BufferUsageFlags.IndirectBufferBit, 
                        logicDevice, sharingMode, queueFamilyIndices);
                    logicDevice.DestroyBuffer(old.Buffer);
                }
                else
                {
                    old.Buffer.UpdateMemory(0, bytes);
                }
            }
            vro.DrawIndirectCommands = commands;
            return vro;
        }
        /// <summary>
        /// 创建间接索引绘制
        /// </summary>
        /// <param name="vro"></param>
        /// <param name="commands"></param>
        /// <param name="logicDevice"></param>
        /// <param name="sharingMode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <returns></returns>
        public static VRO CreateOrUpdateDrawIndexedIndirectBuffer(this VRO vro, DrawIndexedIndirectCommand[] commands,
            VulkanDevice logicDevice, SharingMode sharingMode, uint[] queueFamilyIndices = null)
        {
            if (commands == null || commands.Length == 0) throw new ArgumentNullException(nameof(commands));
            var span = new Span<DrawIndexedIndirectCommand>(commands);
            var bytes = MemoryMarshal.AsBytes(span);
            if (vro.DrawIndexesIndirectBufferInfo.Buffer == null)
            {
                vro.DrawIndexesIndirectBufferInfo = CreateNewBuffer(vro, bytes, BufferUsageFlags.IndirectBufferBit,
                    logicDevice, sharingMode, queueFamilyIndices);
            }
            else
            {
                var old = vro.DrawIndexesIndirectBufferInfo;
                if ((ulong)bytes.Length > old.Size)
                {
                    vro.DrawIndexesIndirectBufferInfo = CreateNewBuffer(vro, bytes, BufferUsageFlags.IndirectBufferBit,
                        logicDevice, sharingMode, queueFamilyIndices);
                    logicDevice.DestroyBuffer(old.Buffer);
                }
                else
                {
                    old.Buffer.UpdateMemory(0, bytes);
                }
            }
            vro.DrawIndexedIndirectCommands = commands;
            return vro;
        }

        /// <summary>
        /// 构建新的Buffer
        /// </summary>
        /// <param name="vro"></param>
        /// <param name="bytes"></param>
        /// <param name="usageFlags"></param>
        /// <param name="logicDevice"></param>
        /// <param name="sharingMode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static VulkanBufferInfo CreateNewBuffer(VRO vro, Span<byte> bytes, BufferUsageFlags usageFlags, 
            VulkanDevice logicDevice, SharingMode sharingMode, uint[] queueFamilyIndices = null)
        {
            BufferCreateOption option = new BufferCreateOption
            {
                QueueFamilyIndices = queueFamilyIndices,
                SharingMode = sharingMode,
                Usage = usageFlags,
                Size = (ulong)bytes.Length
            };
            var buffer = logicDevice.CreateBuffer(option, option.Size,
                MemoryPropertyFlags.HostVisibleBit | MemoryPropertyFlags.HostCachedBit);
            buffer.UpdateMemory(0, bytes);
            return new VulkanBufferInfo
            {
                Buffer = buffer,
                DeviceMemory = buffer.DeviceMemory,
                BufferUsageFlags = usageFlags,
                WriteDescriptorSet = new WriteDescriptorSet
                {
                    BufferInfo = new DescriptorBufferInfo[] 
                    {
                        new DescriptorBufferInfo 
                        { 
                            Buffer = buffer.RawHandle, 
                            Offset = 0, 
                            Range = buffer.DeviceMemory.Requirements.Size
                        }
                    },
                    DescriptorCount = 1,
                    DescriptorType = DescriptorType.InputAttachment
                },
                Size = option.Size
            };
        }
        /// <summary>
        /// 创建新的缓冲
        /// </summary>
        /// <param name="vro"></param>
        /// <param name="ptr"></param>
        /// <param name="bytesSize"></param>
        /// <param name="usageFlags"></param>
        /// <param name="logicDevice"></param>
        /// <param name="sharingMode"></param>
        /// <param name="queueFamilyIndices"></param>
        /// <returns></returns>
        private static VulkanBufferInfo CreateNewBuffer(VRO vro, in IntPtr ptr, ulong bytesSize, 
            BufferUsageFlags usageFlags, VulkanDevice logicDevice, SharingMode sharingMode, uint[] queueFamilyIndices = null)
        {
            BufferCreateOption option = new BufferCreateOption
            {
                QueueFamilyIndices = queueFamilyIndices,
                SharingMode = sharingMode,
                Usage = usageFlags,
                Size = bytesSize
            };
            var buffer = logicDevice.CreateBuffer(option, option.Size,
                MemoryPropertyFlags.HostVisibleBit | MemoryPropertyFlags.HostCachedBit);
            buffer.UpdateMemory(0, in ptr, bytesSize);
            return new VulkanBufferInfo
            {
                Buffer = buffer,
                DeviceMemory = buffer.DeviceMemory,
                BufferUsageFlags = usageFlags,
                WriteDescriptorSet = new WriteDescriptorSet
                {
                    BufferInfo = new DescriptorBufferInfo[]
                    {
                        new DescriptorBufferInfo
                        {
                            Buffer = buffer.RawHandle,
                            Offset = 0,
                            Range = buffer.DeviceMemory.Requirements.Size
                        }
                    },
                    DescriptorCount = 1,
                    DescriptorType = DescriptorType.InputAttachment
                },
                Size = option.Size
            };
        }
    }
}
