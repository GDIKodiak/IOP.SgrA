﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan.Mesh
{
    /// <summary>
    /// 
    /// </summary>
    public class Sphere
    {
        /// <summary>
        /// 构建单位圆
        /// </summary>
        /// <param name="angleSpan"></param>
        /// <param name="vertexesCount"></param>
        /// <returns></returns>
        public static Span<Vector3> CreateUnitSphere(float angleSpan, out uint vertexesCount)
        {
            const float r = 1.0f;
            List<Vector3> alVertix = new List<Vector3>();
            for (float vAngle = 90f; vAngle > -90f; vAngle -= angleSpan)
            {
                for (float hAngle = 360f; hAngle > 0; hAngle -= angleSpan)
                {
                    double xozLength = r * MathF.Cos(vAngle.ToRadians());
                    float x1 = (float)(xozLength * MathF.Cos(hAngle.ToRadians()));
                    float z1 = (float)(xozLength * MathF.Sin(hAngle.ToRadians()));
                    float y1 = (float)(r * MathF.Sin(vAngle.ToRadians()));
                    xozLength = r * MathF.Cos((vAngle - angleSpan).ToRadians());
                    float x2 = (float)(xozLength * MathF.Cos(hAngle.ToRadians()));
                    float z2 = (float)(xozLength * MathF.Sin(hAngle.ToRadians()));
                    float y2 = (float)(r * MathF.Sin((vAngle - angleSpan).ToRadians()));
                    xozLength = r * MathF.Cos((vAngle - angleSpan).ToRadians());
                    float x3 = (float)(xozLength * MathF.Cos((hAngle - angleSpan).ToRadians()));
                    float z3 = (float)(xozLength * MathF.Sin((hAngle - angleSpan).ToRadians()));
                    float y3 = (float)(r * MathF.Sin((vAngle - angleSpan).ToRadians()));
                    xozLength = r * MathF.Cos(vAngle.ToRadians());
                    float x4 = (float)(xozLength * MathF.Cos((hAngle - angleSpan).ToRadians()));
                    float z4 = (float)(xozLength * MathF.Sin((hAngle - angleSpan).ToRadians()));
                    float y4 = (float)(r * MathF.Sin(vAngle.ToRadians()));
                    alVertix.Add(new Vector3(x1, y1, z1));
                    alVertix.Add(new Vector3(x2, y2, z2));
                    alVertix.Add(new Vector3(x4, y4, z4));
                    alVertix.Add(new Vector3(x4, y4, z4));
                    alVertix.Add(new Vector3(x2, y2, z2));
                    alVertix.Add(new Vector3(x3, y3, z3));
                }
            }
            Span<Vector3> texCoor = GenerateCircleTexCoor((int)(360f / angleSpan), (int)(180f / angleSpan));
            vertexesCount = (uint)alVertix.Count;
            Span<Vector3> result = new Vector3[vertexesCount * 3];
            int index = 0;
            int texIndex = 0;
            for (int i = 0; i < vertexesCount; i++)
            {
                result[index++] = alVertix[i];
                result[index++] = alVertix[i];
                result[index++] = texCoor[texIndex++];
            }
            return result;
        }
        /// <summary>
        /// 构建圆贴图坐标
        /// </summary>
        /// <param name="bw"></param>
        /// <param name="bh"></param>
        /// <returns></returns>
        public static Span<Vector3> GenerateCircleTexCoor(int bw, int bh)
        {
            var result = new Vector3[bw * bh * 6];
            float sizew = 1.0f / bw;
            float sizeh = 1.0f / bh;
            int c = 0;
            for (int i = 0; i < bh; i++)
            {
                for (int j = 0; j < bw; j++)
                {
                    float s = j * sizew;
                    float t = i * sizeh;
                    result[c++] = new Vector3(s, t, 0);
                    result[c++] = new Vector3(s, t + sizeh, 0);
                    result[c++] = new Vector3(s + sizew, t, 0);
                    result[c++] = new Vector3(s + sizew, t, 0);
                    result[c++] = new Vector3(s, t + sizeh, 0);
                    result[c++] = new Vector3(s + sizew, t + sizeh, 0);
                }
            }
            return result;
        }
    }
}
