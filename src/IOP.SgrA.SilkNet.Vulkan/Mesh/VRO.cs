﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// Vulkan渲染对象
    /// </summary>
    public class VRO : IMesh, IDisposable
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 顶点数量
        /// </summary>
        public uint VecticesCount { get; set; }
        /// <summary>
        /// 索引数量
        /// </summary>
        public uint IndexesCount { get; set; }
        /// <summary>
        /// LOD级别
        /// </summary>
        public int LODLevel { get; set; }
        /// <summary>
        /// 最小值向量
        /// </summary>
        public Vector3 MinVector { get; set; }
        /// <summary>
        /// 最大值向量
        /// </summary>
        public Vector3 MaxVector { get; set; }
        /// <summary>
        /// 顶点缓冲信息
        /// </summary>
        public VulkanBufferInfo VerticesBufferInfo { get; set; }
        /// <summary>
        /// 索引缓冲信息
        /// </summary>
        public VulkanBufferInfo IndexesBufferInfo { get; set; }
        /// <summary>
        /// 间接绘制缓冲信息
        /// </summary>
        public VulkanBufferInfo DrawIndirectBufferInfo { get; set; }
        /// <summary>
        /// 索引间接绘制缓冲信息
        /// </summary>
        public VulkanBufferInfo DrawIndexesIndirectBufferInfo { get; set; }
        /// <summary>
        /// 间接绘制命令
        /// </summary>
        public DrawIndirectCommand[] DrawIndirectCommands { get; set; } = new DrawIndirectCommand[0];
        /// <summary>
        /// 索引间接绘制命令
        /// </summary>
        public DrawIndexedIndirectCommand[] DrawIndexedIndirectCommands { get; set; } = new DrawIndexedIndirectCommand[0];

        /// <summary>
        /// 
        /// </summary>
        public VRO()
        {

        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="name"></param>
        /// <param name="id"></param>
        public VRO(string name, int id)
        {
            Name = name;
            Id = id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="minVector"></param>
        /// <param name="maxVector"></param>
        /// <param name="offset"></param>
        /// <exception cref="IndexOutOfRangeException"></exception>
        public void UpdateMeshData(Span<byte> data, Vector3 minVector, Vector3 maxVector, uint offset = 0)
        {
            var buffer = VerticesBufferInfo.Buffer;
            ulong size = VerticesBufferInfo.Size;
            if ((ulong)(offset + data.Length) > size) throw new IndexOutOfRangeException(nameof(data));
            buffer.UpdateMemory(offset, data);
            MinVector = minVector;
            MaxVector = maxVector;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="minVector"></param>
        /// <param name="maxVector"></param>
        /// <param name="offset"></param>
        public void UpdateMeshData(byte[] data, Vector3 minVector, Vector3 maxVector, uint offset = 0)
        {
            UpdateMeshData(new Span<byte>(data), minVector, maxVector, offset);
        }

        /// <summary>
        /// 拷贝
        /// </summary>
        /// <returns></returns>
        public void CloneToNewRender(IRenderObject newObj) => newObj.AddComponent(this);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderObject"></param>
        public void OnAttach(IRenderObject renderObject) { }
        /// <summary>
        /// 摧毁组件
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        public void Destroy()
        {
        }

        /// <summary>
        /// 销毁资源
        /// </summary>
        public void Dispose()
        {
        }
    }
}
