﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public struct VulkanSubmitSemaphore
    {
        /// <summary>
        /// 信号量
        /// </summary>
        public Semaphore Semaphore;
        /// <summary>
        /// 
        /// </summary>
        public PipelineStageFlags Stage;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="semaphore"></param>
        /// <param name="stage"></param>
        public VulkanSubmitSemaphore(Semaphore semaphore, PipelineStageFlags stage)
        {
            Semaphore = semaphore;
            Stage = stage;
        }
    }
}
