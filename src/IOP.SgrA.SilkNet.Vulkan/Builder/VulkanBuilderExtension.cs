﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public static class VulkanBuilderExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildAction"></param>
        /// <param name="builder"></param>
        public static void BuildVulkan(this IGraphicsBuilder builder, Action<VulkanGraphicsManager> buildAction)
        {
            if (buildAction == null) throw new ArgumentNullException(nameof(buildAction));
            var graphicsManager = builder.Service.GetServices<IGraphicsManager>().Where(x => x.GraphicsName == "Vulkan");
            if (!graphicsManager.Any()) throw new NullReferenceException("Cannot found VulkanManager in Host ServiceProvider, Are you sure Add Vulkan Service?");
            var vulkan = graphicsManager.First() as VulkanGraphicsManager;
            buildAction.Invoke(vulkan);
        }
    }
}
