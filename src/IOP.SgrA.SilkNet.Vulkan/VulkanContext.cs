﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// Vulkan上下文
    /// </summary>
    public class VulkanContext : Context
    {
        /// <summary>
        /// Vulkan裁剪矩阵
        /// </summary>
        public readonly Matrix4x4 ClipMatrix = new Matrix4x4(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.5f, 0.0f, 0.0f, 0.0f, 0.5f, 1.0f);
        /// <summary>
        /// 命令缓冲
        /// </summary>
        public VulkanCommandBuffer CommandBuffer { get; internal set; }
        /// <summary>
        /// Vulkan管线
        /// </summary>
        public VulkanPipeline VulkanPipeline { get; internal set; }
        /// <summary>
        /// 逻辑设备
        /// </summary>
        public VulkanDevice LogicDevice { get; internal set; }
        /// <summary>
        /// 获取上下文所属渲染组
        /// </summary>
        /// <returns></returns>
        public override IRenderGroup GetContextRenderGroup() => RenderGroup;

        /// <summary>
        /// 获取上下文所属渲染对象
        /// </summary>
        /// <returns></returns>
        public override IRenderObject GetContextRenderObject() => RenderObject;

        /// <summary>
        /// 推送至渲染组
        /// </summary>
        public override void PushToRenderGroup()
        {
            if (RenderGroup == null) return;
            RenderGroup.PushContext(this);
        }


    }
}
