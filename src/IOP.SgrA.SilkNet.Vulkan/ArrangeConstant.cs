﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public static class ArrangeConstant
    {
        /// <summary>
        /// 交换链图片可用信号量
        /// </summary>
        public const string ImageAvailableSemaphore = "ImageAvailableSemaphore";
        /// <summary>
        /// 渲染结束信号量
        /// </summary>
        public const string RenderFinishSemaphore = "RenderFinishSemaphore";
    }
}
