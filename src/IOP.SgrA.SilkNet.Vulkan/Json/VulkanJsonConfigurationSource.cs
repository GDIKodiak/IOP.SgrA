﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// VulkanJson文件配置源
    /// </summary>
    public class VulkanJsonConfigurationSource : IConfigurationSource
    {
        /// <summary>
        /// 路径
        /// </summary>
        private string _Path { get; set; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="path"></param>
        public VulkanJsonConfigurationSource(string path)
        {
            _Path = path;
        }
        /// <summary>
        /// 构建配置源
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public IConfigurationProvider Build(IConfigurationBuilder builder)
        {
            var fileProvider = builder.GetFileProvider();
            var fullPath = fileProvider.GetFileInfo(_Path).PhysicalPath;
            return new VulkanJsonConfigurationProvider(fullPath);
        }
    }
}
