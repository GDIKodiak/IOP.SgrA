﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;
using System.Text.Json;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// VulkanVersion转换器
    /// </summary>
    public class JsonConverterVulkanVersion : JsonConverter<Versions>
    {
        /// <summary>
        /// 是否允许转换
        /// </summary>
        /// <param name="typeToConvert"></param>
        /// <returns></returns>
        public override bool CanConvert(Type typeToConvert)
        {
            return typeToConvert == typeof(Versions);
        }

        /// <summary>
        /// 读取
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="typeToConvert"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public override Versions Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            if (reader.TokenType != JsonTokenType.String)
            {
                throw new JsonException("Cannot convert to vulkan version struct, the json data must be string");
            }
            string data = reader.GetString();
            string[] versions = data.Split('.');
            int[] numbers = new int[3];
            for (int i = 0; i < versions.Length; i++)
            {
                if (i > 2) break;
                if (!int.TryParse(versions[i], out int r)) throw new JsonException("Cannot convert to vulkan version struct, string parse to number failed");
                else numbers[i] = r;
            }
            Versions v = new Versions((uint)numbers[0], (uint)numbers[1], (uint)numbers[2]);
            return v;
        }
        /// <summary>
        /// 写入
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="value"></param>
        /// <param name="options"></param>
        public override void Write(Utf8JsonWriter writer, Versions value, JsonSerializerOptions options)
        {
            string[] datas = new string[] { value.Major.ToString(), value.Minor.ToString(), value.Patch.ToString() };
            string r = string.Join('.', datas);
            writer.WriteStringValue(r);
        }
    }
}
