﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 元组转换器
    /// </summary>
    public class JsonConverterValueTuple<T> : JsonConverter<T>
    {
        /// <summary>
        /// 是否允许转换
        /// </summary>
        /// <param name="typeToConvert"></param>
        /// <returns></returns>
        public override bool CanConvert(Type typeToConvert)
        {
            return typeToConvert.GetInterfaces().Any(x => x == typeof(ITuple));
        }
        /// <summary>
        /// 读取
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="typeToConvert"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public override T Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            if (reader.TokenType != JsonTokenType.StartArray) throw new JsonException("The Value Tuple data must be a array in json");
            var types = typeToConvert.GetGenericArguments();
            var defineMethod = typeof(ValueTuple).GetMethods().Where(x => x.IsStatic && x.GetGenericArguments().Length == types.Length).FirstOrDefault();
            if (defineMethod == null) throw new JsonException("Cannot get ValueTuple create method");
            var createMethod = defineMethod.MakeGenericMethod(types);
            Expression[] expressions = new Expression[types.Length];
            int i = 0;
            while (true)
            {
                reader.Read();
                if (reader.TokenType == JsonTokenType.EndArray) break;

                var itemType = types[i];
                if (itemType == typeof(float)) expressions[i] = GetFloatExpression(ref reader, itemType);
                else throw new JsonException($"Not support type {itemType.Name}");
                if (i < types.Length) i++;
            }
            Expression call = Expression.Call(createMethod, expressions);
            Expression<Func<T>> lambda = Expression.Lambda<Func<T>>(call);
            Func<T> func = lambda.Compile();
            var result = func();
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="value"></param>
        /// <param name="options"></param>
        public override void Write(Utf8JsonWriter writer, T value, JsonSerializerOptions options)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 获取单精度浮点值
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="itemType"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private Expression GetFloatExpression(ref Utf8JsonReader reader, Type itemType)
        {
            if (reader.TokenType == JsonTokenType.Number)
            {
                var d = reader.GetSingle();
                ConstantExpression constant = Expression.Constant(d);
                return constant;
            }
            else if (reader.TokenType == JsonTokenType.String)
            {
                var d = reader.GetString();
                if (!float.TryParse(d, out float e)) throw new JsonException($"Cannot convert float to {itemType.Name} from json value {e}");
                ConstantExpression constant = Expression.Constant(e);
                return constant;
            }
            else throw new JsonException($"Cannot convert tuple item {itemType.Name} from this json, the json type must be number or string for float");
        }
    }
}
