﻿using Microsoft.Extensions.Configuration;
using System.IO;
using System.Text.Json;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// VulkanJson配置提供者
    /// </summary>
    public class VulkanJsonConfigurationProvider : ConfigurationProvider
    {
        /// <summary>
        /// 路径
        /// </summary>
        private string _Path { get; set; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="path"></param>
        public VulkanJsonConfigurationProvider(string path)
        {
            _Path = path;
        }
        /// <summary>
        /// 加载
        /// </summary>
        public override void Load()
        {
            if (!File.Exists(_Path)) throw new FileNotFoundException($"Cannot found file named {_Path}");
            string data = File.ReadAllText(_Path);
            using JsonDocument json = JsonDocument.Parse(data);
            JsonElement root = json.RootElement;
            foreach (JsonProperty node in root.EnumerateObject())
            {
                Set(node.Name, node.Value.ToString());
            }
        }
    }
}
