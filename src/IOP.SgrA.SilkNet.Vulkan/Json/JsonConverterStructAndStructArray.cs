﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.Json.Serialization;
using System.Text.Json;
using System.Linq;
using Silk.NET.Core;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 通用结构体以及结构体数组转换器
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class JsonConverterStructAndStructArray<T> : JsonConverter<T>
    {
        /// <summary>
        /// 是否允许转换
        /// </summary>
        /// <param name="typeToConvert"></param>
        /// <returns></returns>
        public override bool CanConvert(Type typeToConvert)
        {
            return typeToConvert == typeof(T);
        }
        /// <summary>
        /// 读取
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="typeToConvert"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public override T Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            if (typeToConvert.IsArray)
            {
                var arrayE = GetStructArrayExpression(ref reader, typeToConvert);
                var lambda = Expression.Lambda<Func<T>>(arrayE);
                var func = lambda.Compile();
                return func();
            }
            else
            {
                if (reader.TokenType != JsonTokenType.StartObject) throw new JsonException($"Cannot convert to a struct from this json value, the json value must be a object, the struct type: {typeToConvert.FullName}");
                var expression = GetStructExpression(ref reader, typeToConvert);
                var lambda = Expression.Lambda<Func<T>>(expression);
                var func = lambda.Compile();
                return func();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="value"></param>
        /// <param name="options"></param>
        public override void Write(Utf8JsonWriter writer, T value, JsonSerializerOptions options)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 获取结构体数组表达式
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="structArrayType"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private Expression GetStructArrayExpression(ref Utf8JsonReader reader, Type structArrayType)
        {
            if (reader.TokenType != JsonTokenType.StartArray) throw new JsonException("the target object is a array, but json value is not a array");
            string tName = structArrayType.FullName.Replace("[]", string.Empty);
            Type oType = structArrayType.Assembly.GetType(tName);
            List<Expression> expressions = new List<Expression>();
            while (true)
            {
                reader.Read();
                if (reader.TokenType == JsonTokenType.EndArray) break;
                if (reader.TokenType == JsonTokenType.StartObject)
                {
                    var e = GetStructExpression(ref reader, oType);
                    expressions.Add(e);
                }
                else throw new JsonException($"Cannot convert to a struct from this json value, the json value must be a object, the struct type: {oType.FullName}");
            }
            var arrayE = Expression.NewArrayInit(oType, expressions.ToArray());
            return arrayE;
        }

        /// <summary>
        /// 获取结构体表达式
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="structType"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private Expression GetStructExpression(ref Utf8JsonReader reader, Type structType)
        {
            if (reader.TokenType != JsonTokenType.StartObject)
            {
                throw new JsonException($"Cannot convert to a struct from this json value, the json value must be a object, the struct type: {structType.FullName}");
            }
            List<MemberAssignment> members = new List<MemberAssignment>();
            while (true)
            {
                reader.Read();
                if (reader.TokenType == JsonTokenType.EndObject) break;
                if (reader.TokenType == JsonTokenType.PropertyName)
                {
                    var d = reader.GetString();
                    var field = structType.GetMember(d).Where(x => x.MemberType == MemberTypes.Field || x.MemberType == MemberTypes.Property).FirstOrDefault();
                    if (field == null) throw new JsonException($"Cannot found field or property with name {d} in type {structType.FullName}");
                    var fType = field.MemberType == MemberTypes.Field ? (field as FieldInfo).FieldType : (field as PropertyInfo).PropertyType;
                    MemberAssignment mAssign;
                    if (fType.IsEnum) mAssign = Expression.Bind(field, GetEnumExpression(ref reader, fType));
                    else if (fType == typeof(bool)) mAssign = Expression.Bind(field, GetBoolExpression(ref reader, fType));
                    else if (fType == typeof(Bool32))
                    {
                        mAssign = Expression.Bind(field, GetBool32Expression(ref reader, fType));
                    }
                    else if (fType == typeof(int)) mAssign = Expression.Bind(field, GetIntExpression(ref reader, fType));
                    else if (fType == typeof(uint)) mAssign = Expression.Bind(field, GetUintExpression(ref reader, fType));
                    else if (fType == typeof(float)) mAssign = Expression.Bind(field, GetFloatExpression(ref reader, fType));
                    else if (fType.IsGenericType) mAssign = Expression.Bind(field, GetGenericExpression(ref reader, fType));
                    else if (fType.IsValueType)
                    {
                        reader.Read();
                        mAssign = Expression.Bind(field, GetStructExpression(ref reader, fType));
                    }
                    else if (fType.IsArray)
                    {
                        reader.Read();
                        mAssign = Expression.Bind(field, GetStructArrayExpression(ref reader, fType));
                    }
                    else throw new JsonException($"Not support type : {fType.FullName}");
                    members.Add(mAssign);
                }
            }
            Expression newStruct = Expression.MemberInit(Expression.New(structType), members.ToArray());
            return newStruct;
        }

        /// <summary>
        /// 当类型为泛型是返回的表达式
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="genericType"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private Expression GetGenericExpression(ref Utf8JsonReader reader, Type genericType)
        {
            var oTypes = genericType.GetGenericArguments();
            Expression valueExpression;
            if (oTypes.Length == 1)
            {
                var oType = oTypes[0];
                if (oType.IsEnum) valueExpression = GetEnumExpression(ref reader, oType);
                else if (oType == typeof(bool)) valueExpression = GetBoolExpression(ref reader, oType);
                else if (oType == typeof(int)) valueExpression = GetIntExpression(ref reader, oType);
                else if (oType == typeof(uint)) valueExpression = GetUintExpression(ref reader, oType);
                else if (oType == typeof(float)) valueExpression = GetFloatExpression(ref reader, oType);
                else if (oType.IsGenericType) valueExpression = GetGenericExpression(ref reader, oType);
                else if (oType.IsValueType)
                {
                    reader.Read();
                    valueExpression = GetStructExpression(ref reader, oType);
                }
                else throw new JsonException($"Not support type : {oType.FullName}");
            }
            else throw new JsonException("Not support to convert genericType where that's GenericArguments is greater than 1");
            var c = genericType.GetConstructor(oTypes);
            if (c == null) throw new JsonException($"Not support genericType {genericType}");
            NewExpression newExpression = Expression.New(c, valueExpression);
            return newExpression;
        }

        /// <summary>
        /// 获取枚举表达式树
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="fieldType"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private Expression GetEnumExpression(ref Utf8JsonReader reader, Type fieldType)
        {
            reader.Read();
            if (reader.TokenType == JsonTokenType.String)
            {
                var d = reader.GetString();
                if (!Enum.TryParse(fieldType, d, out object e)) throw new JsonException($"Cannot convert field {fieldType.Name} from json value {d}");
                ConstantExpression constant = Expression.Constant(e);
                return constant;
            }
            else if (reader.TokenType == JsonTokenType.StartArray)
            {
                string enumString = default;
                bool first = true;
                while (true)
                {
                    reader.Read();
                    if (reader.TokenType == JsonTokenType.EndArray) break;
                    if (reader.TokenType != JsonTokenType.String) throw new JsonException("The json array value type must be string for enum");
                    string enumValue = reader.GetString();
                    if (first) first = false;
                    else enumString += ",";
                    enumString += enumValue;
                }
                if (!Enum.TryParse(fieldType, enumString, out object e)) throw new JsonException($"Cannot convert enum {fieldType.Name} from json value {e}");
                ConstantExpression constant = Expression.Constant(e);
                return constant;
            }
            else throw new JsonException($"Cannot convert field to {fieldType.Name} from this json, the json type must be string array or string for enum");
        }
        /// <summary>
        /// 获取uint表达式树
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="fieldType"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private Expression GetUintExpression(ref Utf8JsonReader reader, Type fieldType)
        {
            reader.Read();
            if (reader.TokenType == JsonTokenType.Number)
            {
                var d = reader.GetUInt32();
                ConstantExpression constant = Expression.Constant(d);
                return constant;
            }
            else if (reader.TokenType == JsonTokenType.String)
            {
                var d = reader.GetString();
                if (!uint.TryParse(d, out uint e)) throw new JsonException($"Cannot convert uint to {fieldType.Name} from json value {e}");
                ConstantExpression constant = Expression.Constant(e);
                return constant;
            }
            else throw new JsonException($"Cannot convert field {fieldType.Name} from this json, the json type must be number or string for uint");
        }

        /// <summary>
        /// 获取int表达式
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="fieldType"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private Expression GetIntExpression(ref Utf8JsonReader reader, Type fieldType)
        {
            reader.Read();
            if (reader.TokenType == JsonTokenType.Number)
            {
                var d = reader.GetInt32();
                ConstantExpression constant = Expression.Constant(d);
                return constant;
            }
            else if (reader.TokenType == JsonTokenType.String)
            {
                var d = reader.GetString();
                if (!int.TryParse(d, out int e)) throw new JsonException($"Cannot convert uint to {fieldType.Name} from json value {e}");
                ConstantExpression constant = Expression.Constant(e);
                return constant;
            }
            else throw new JsonException($"Cannot convert field {fieldType.Name} from this json, the json type must be number or string for int");
        }

        /// <summary>
        /// 获取单精度浮点值
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="fieldType"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private Expression GetFloatExpression(ref Utf8JsonReader reader, Type fieldType)
        {
            reader.Read();
            if (reader.TokenType == JsonTokenType.Number)
            {
                var d = reader.GetSingle();
                ConstantExpression constant = Expression.Constant(d);
                return constant;
            }
            else if (reader.TokenType == JsonTokenType.String)
            {
                var d = reader.GetString();
                if (!float.TryParse(d, out float e)) throw new JsonException($"Cannot convert float to {fieldType.Name} from json value {e}");
                ConstantExpression constant = Expression.Constant(e);
                return constant;
            }
            else throw new JsonException($"Cannot convert field {fieldType.Name} from this json, the json type must be number or string for float");
        }

        /// <summary>
        /// 获取布尔值
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="fieldType"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private Expression GetBoolExpression(ref Utf8JsonReader reader, Type fieldType)
        {
            reader.Read();
            if (reader.TokenType == JsonTokenType.True || reader.TokenType == JsonTokenType.False)
            {
                var d = reader.GetBoolean();
                ConstantExpression constant = Expression.Constant(d);
                return constant;
            }
            else if (reader.TokenType == JsonTokenType.String)
            {
                var d = reader.GetString();
                if (!bool.TryParse(d, out bool e)) throw new JsonException($"Cannot convert bool to {fieldType.Name} from json value {e}");
                ConstantExpression constant = Expression.Constant(e);
                return constant;
            }
            else throw new JsonException($"Cannot convert field {fieldType.Name} from this json, the json type must be bool or string for bool");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="fieldType"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private Expression GetBool32Expression(ref Utf8JsonReader reader, Type fieldType)
        {
            reader.Read();
            bool b;
            if (reader.TokenType == JsonTokenType.True || reader.TokenType == JsonTokenType.False)
            {
                b = reader.GetBoolean();
            }
            else if (reader.TokenType == JsonTokenType.String)
            {
                var d = reader.GetString();
                if (!bool.TryParse(d, out bool e)) throw new JsonException($"Cannot convert bool to {fieldType.Name} from json value {e}");
                b = e;
            }
            else throw new JsonException($"Cannot convert field {fieldType.Name} from this json, the json type must be bool or string for bool");
            var cons = fieldType.GetConstructor(new Type[] { typeof(bool) });
            ConstantExpression constant = Expression.Constant(b);
            return Expression.New(cons, constant);
        }
    }
}
