﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public struct Versions
    {
        private readonly uint value;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="major"></param>
        /// <param name="minor"></param>
        /// <param name="patch"></param>
        public Versions(uint major, uint minor, uint patch)
        {
            value = major << 22 | minor << 12 | patch;
        }
        /// <summary>
        /// 
        /// </summary>
        public uint Major => value >> 22;
        /// <summary>
        /// 
        /// </summary>
        public uint Minor => (value >> 12) & 0x3ff;
        /// <summary>
        /// 
        /// </summary>
        public uint Patch => (value >> 22) & 0xfff;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="version"></param>
        public static implicit operator uint(Versions version)
        {
            return version.value;
        }
    }
}
