﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 栅格
    /// </summary>
    public class VulkanFence : IVulkanWapper<Fence>
    {
        /// <summary>
        /// 
        /// </summary>
        public uint Id { get; }
        /// <summary>
        /// 
        /// </summary>
        public Fence RawHandle { get; }
        /// <summary>
        /// 设备
        /// </summary>
        public Device Device { get; }

        private Vk NativeAPI = Vk.GetApi();

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="fence"></param>
        /// <param name="device"></param>
        /// <param name="api"></param>
        /// <param name="id"></param>
        public VulkanFence(Fence fence, Device device, Vk api, uint id)
        {
            RawHandle = fence;
            Device = device;
            NativeAPI = api;
            Id = id;
        }
        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            if(RawHandle.Handle != 0 && Device.Handle != IntPtr.Zero)
            {
                unsafe
                {
                    NativeAPI.DestroyFence(Device, RawHandle, null);
                }
            }
        }
    }
}
