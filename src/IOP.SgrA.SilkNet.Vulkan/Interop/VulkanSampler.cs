﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 采样器
    /// </summary>
    public class VulkanSampler : IVulkanWapper<Sampler>, ISampler
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; }
        /// <summary>
        /// 
        /// </summary>
        public Device Device { get; }
        /// <summary>
        /// 
        /// </summary>
        public Sampler RawHandle { get; }

        private Vk NativeAPI = Vk.GetApi();
        private volatile bool IsDispose;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="api"></param>
        /// <param name="device"></param>
        /// <param name="sampler"></param>
        /// <param name="name"></param>
        public VulkanSampler(Vk api, Device device, Sampler sampler, string name)
        {
            Name = name;
            Device = device;
            NativeAPI = api;
            RawHandle = sampler;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            if (IsDispose) return;
            IsDispose = true;
            if (RawHandle.Handle != 0 && Device.Handle != IntPtr.Zero)
            {
                unsafe
                {
                    NativeAPI.DestroySampler(Device, RawHandle, null);
                }
            }
        }
    }
}
