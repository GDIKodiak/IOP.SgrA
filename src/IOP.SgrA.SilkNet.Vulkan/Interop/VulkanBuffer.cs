﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using Buffer = Silk.NET.Vulkan.Buffer;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class VulkanBuffer : IVulkanWapper<Buffer>
    {
        /// <summary>
        /// 
        /// </summary>
        public Buffer RawHandle { get; }
        /// <summary>
        /// 
        /// </summary>
        public Device Device { get; }
        /// <summary>
        /// 所在内存偏移
        /// </summary>
        public ulong MemoryOffset { get; protected internal set; }
        /// <summary>
        /// 内存块
        /// </summary>
        public VulkanMemoryBlock Block { get; protected internal set; } = null;
        /// <summary>
        /// 设备内存
        /// </summary>
        public VulkanDeviceMemory DeviceMemory { get; protected internal set; } = null;
        /// <summary>
        /// 创建配置
        /// </summary>
        public BufferCreateOption CreateOption { get; protected internal set; } = null;
        /// <summary>
        /// Id
        /// </summary>
        public Guid Guid { get; protected internal set; } = Guid.Empty;
        /// <summary>
        /// 是否销毁
        /// </summary>
        public bool IsDispose { get => _IsDispose; }

        private Vk NativeAPI = Vk.GetApi();
        private volatile bool _IsDispose;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="api"></param>
        /// <param name="device"></param>
        /// <param name="buffer"></param>
        public VulkanBuffer(Vk api, Device device, Buffer buffer)
        {
            NativeAPI = api;
            Device = device;
            RawHandle = buffer;
        }

        /// <summary>
        /// 获取内存需求
        /// </summary>
        /// <returns></returns>
        public MemoryRequirements GetMemoryRequirements()
        {
            if (Device.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            if (RawHandle.Handle == 0) throw new InvalidOperationException("Invalid Image");
            NativeAPI.GetBufferMemoryRequirements(Device, RawHandle, out MemoryRequirements requirements);
            return requirements;
        }
        /// <summary>
        /// 尝试更新内存
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool TryUpdateMemory(ulong offset, byte[] data)
        {
            if (data.Length <= 0) return true;
            if (CouldUpdateMemory())
            {
                try
                {
                    DeviceMemory.UpdateMemory(data, MemoryOffset + offset);
                }
                catch
                {
                    return false;
                }
            }
            return false;
        }
        /// <summary>
        /// 更新内存
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="data"></param>
        public void UpdateMemory(ulong offset, byte[] data)
        {
            if (data.Length <= 0) return;
            Span<byte> local = data;
            UpdateMemory(offset, local);
        }
        /// <summary>
        /// 更新内存
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="data"></param>
        public void UpdateMemory(ulong offset, Span<byte> data)
        {
            if (data.Length <= 0) return;
            if (CouldUpdateMemory())
            {
                DeviceMemory?.UpdateMemory(data, MemoryOffset + offset);
            }
            else throw new NotSupportedException("Cannot update target memory");
        }
        /// <summary>
        /// 更新内存
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="ptr"></param>
        /// <param name="size"></param>
        public void UpdateMemory(ulong offset, in IntPtr ptr, ulong size)
        {
            if (ptr == IntPtr.Zero) return;
            if (CouldUpdateMemory())
            {
                DeviceMemory?.UpdateMemory(ptr, size, MemoryOffset + offset);
            }
            else throw new NotSupportedException("Cannot update target memory");
        }

        /// <summary>
        /// 是否允许更新内存
        /// </summary>
        /// <returns></returns>
        public bool CouldUpdateMemory()
        {
            if (DeviceMemory == null) return false;
            var type = DeviceMemory.PropertyFlags;
            if (type.HasFlag(MemoryPropertyFlags.HostCachedBit)) return true;
            else if (type.HasFlag(MemoryPropertyFlags.HostCoherentBit)) return true;
            else if (type.HasFlag(MemoryPropertyFlags.HostVisibleBit)) return true;
            else return false;
        }
        /// <summary>
        /// 绑定缓冲内存
        /// </summary>
        /// <param name="deviceMemory"></param>
        /// <param name="offset"></param>
        public void BindBufferMemory(VulkanDeviceMemory deviceMemory, uint offset)
        {
            if (RawHandle.Handle == 0 || Device.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Buffer");
            if (deviceMemory.RawHandle.Handle == 0) throw new InvalidOperationException("Invalid Memory");
            var r = NativeAPI.BindBufferMemory(Device, RawHandle, deviceMemory.RawHandle, offset);
            if (r != Result.Success) throw new VulkanOperationException("BindBufferMemory", r);
            DeviceMemory?.Dispose();
            DeviceMemory = deviceMemory;
            MemoryOffset = 0;
        }
        /// <summary>
        /// 绑定缓冲内存
        /// </summary>
        public void BindBufferMemory(MemoryPropertyFlags flags)
        {
            if (RawHandle.Handle == 0 || Device.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Buffer");
            if (Block != null) VulkanMemoryManager.Current.Recovery(Block);
            var block = VulkanMemoryManager.Current.Malloc(flags, GetMemoryRequirements());
            Block = block; var seq = block.Owner;
            var r = NativeAPI.BindBufferMemory(Device, RawHandle, seq.Memory.RawHandle, block.Offset);
            if (r != Result.Success) throw new VulkanOperationException("BindBufferMemory", r);
            DeviceMemory?.Dispose();
            DeviceMemory = seq.Memory;
            MemoryOffset = block.Offset;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            if (_IsDispose) return;
            _IsDispose = true;
            if(Block != null)
            {
                VulkanMemoryManager.Current.Recovery(Block);
                DeviceMemory = null;
            }
            else DeviceMemory?.Dispose();
            if (RawHandle.Handle != 0 && Device.Handle != IntPtr.Zero)
            {
                unsafe { NativeAPI.DestroyBuffer(Device, RawHandle, null); }
            }
        }
    }
}
