﻿using IOP.ISEA;
using Silk.NET.Core.Native;
using Silk.NET.Vulkan;
using Silk.NET.Vulkan.Extensions.KHR;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 逻辑设备
    /// </summary>
    public class VulkanDevice : IVulkanWapper<Device>
    {
        /// <summary>
        /// 实例
        /// </summary>
        public Instance Instance { get; }
        /// <summary>
        /// 
        /// </summary>
        public Device RawHandle { get; }
        /// <summary>
        /// 物理设备
        /// </summary>
        public VulkanPhysicalDevice PhysicalDevice { get; }
        /// <summary>
        /// 队列族
        /// </summary>
        public QueueFamilyIndices[] FamilyIndices { get; protected internal set; }
        /// <summary>
        /// 工作队列
        /// </summary>
        public WorkQueue[] WorkQueues { get; protected internal set; }
        /// <summary>
        /// 资源命令池
        /// </summary>
        public VulkanCommandPool ResourcesCommandPool { get; protected internal set; }
        /// <summary>
        /// 管线缓存
        /// </summary>
        public PipelineCache PipelineCache { get; protected internal set; }
        /// <summary>
        /// 交换链拓展
        /// </summary>
        public KhrSwapchain SwapchainExtension { get; protected internal set; }
        /// <summary>
        /// 底部API
        /// </summary>
        public Vk NativeAPI { get; protected internal set; } = Vk.GetApi();
        /// <summary>
        /// 渲染通道
        /// </summary>
        protected readonly ConcurrentDictionary<string, VulkanRenderPass> RenderPasses = new ConcurrentDictionary<string, VulkanRenderPass>();
        /// <summary>
        /// 图像
        /// </summary>
        protected readonly ConcurrentDictionary<Guid, VulkanImageView> Images = new ConcurrentDictionary<Guid, VulkanImageView>();
        /// <summary>
        /// 缓冲
        /// </summary>
        protected readonly ConcurrentDictionary<Guid, VulkanBuffer> Buffers = new ConcurrentDictionary<Guid, VulkanBuffer>();
        /// <summary>
        /// 缓冲视图
        /// </summary>
        protected readonly ConcurrentDictionary<Guid, VulkanBufferView> BufferViews = new ConcurrentDictionary<Guid, VulkanBufferView>();
        /// <summary>
        /// 信号量
        /// </summary>
        protected readonly ConcurrentDictionary<string, VulkanSemaphore> Semaphores = new ConcurrentDictionary<string, VulkanSemaphore>();
        /// <summary>
        /// 事件
        /// </summary>
        protected readonly ConcurrentDictionary<string, VulkanEvent> Events = new();
        /// <summary>
        /// 栅栏
        /// </summary>
        protected readonly ConcurrentDictionary<uint, VulkanFence> Fences = new ConcurrentDictionary<uint, VulkanFence>();
        /// <summary>
        /// 管线
        /// </summary>
        protected readonly ConcurrentDictionary<string, Pipeline> Pipelines = new ConcurrentDictionary<string, Pipeline>();
        /// <summary>
        /// 采样器
        /// </summary>
        protected readonly ConcurrentDictionary<string, VulkanSampler> Samplers = new ConcurrentDictionary<string, VulkanSampler>();
        /// <summary>
        /// 命令符池
        /// </summary>
        protected readonly ConcurrentBag<VulkanCommandPool> CommandPools = new ConcurrentBag<VulkanCommandPool>();
        /// <summary>
        /// 扩展
        /// </summary>
        protected readonly ConcurrentDictionary<string, NativeExtension<Vk>> Extensions = new ConcurrentDictionary<string, NativeExtension<Vk>>();
        private volatile bool IsDispose;
        private int Id = 0;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="api"></param>
        /// <param name="device"></param>
        /// <param name="physicalDevice"></param>
        /// <param name="instance"></param>
        public VulkanDevice(Vk api, Instance instance, Device device, VulkanPhysicalDevice physicalDevice)
        {
            Instance = instance;
            NativeAPI = api;
            RawHandle = device;
            PhysicalDevice = physicalDevice;
            VulkanMemoryManager _ = new(this);
        }
        /// <summary>
        /// 尝试获取设备扩展
        /// </summary>
        /// <typeparam name="TE"></typeparam>
        /// <param name="name"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        public bool TryGetDeviceExtensions<TE>(string name, out TE extension)
            where TE: NativeExtension<Vk>
        {
            bool r = false; extension = null;
            if (string.IsNullOrEmpty(name)) return r;
            if(Extensions.TryGetValue(name, out NativeExtension<Vk> value))
            {
                if(value is TE te)
                {
                    r = true; 
                    extension = te;
                    return r;
                }
            }
            if(NativeAPI.TryGetDeviceExtension<TE>(Instance, RawHandle, out TE v))
            {
                extension = v;
                Extensions.AddOrUpdate(name, extension, (key, value) => v);
                r = true; return r;
            }
            return r;
        }
        /// <summary>
        /// 初始化工作队列
        /// </summary>
        /// <param name="initFunc"></param>
        public void InitWorkQueue(Func<VulkanDevice, IEnumerable<QueueFamilyIndices>, IEnumerable<WorkQueue>> initFunc)
        {
            if (initFunc == null) throw new ArgumentNullException(nameof(initFunc));
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            var family = FamilyIndices; if (family == null || family.Length <= 0) throw new InvalidOperationException("No Enable QueueFamily");
            var queues = initFunc(this, family);
            if(queues != null) WorkQueues = queues.ToArray();
        }
        /// <summary>
        /// 获取队列
        /// </summary>
        /// <param name="queueFimilyIndex"></param>
        /// <param name="queueIndex"></param>
        /// <returns></returns>
        public Queue GetQueue(uint queueFimilyIndex, uint queueIndex = 0)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            NativeAPI.GetDeviceQueue(RawHandle, queueFimilyIndex, queueIndex, out Queue queue);
            return queue;
        }
        /// <summary>
        /// 创建管线缓存
        /// </summary>
        /// <returns></returns>
        public PipelineCache CreatePipelineCache()
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            unsafe
            {
                PipelineCacheCreateInfo info = new PipelineCacheCreateInfo(StructureType.PipelineCacheCreateInfo, null, null, null, null);
                var r = NativeAPI.CreatePipelineCache(RawHandle, in info, null, out PipelineCache pipelineCache);
                if (r != Result.Success) throw new VulkanOperationException("CreatePipelineCache", r);
                PipelineCache = pipelineCache;
                return pipelineCache;
            }
        }
        /// <summary>
        /// 创建交换链
        /// </summary>
        /// <param name="window"></param>
        /// <param name="chooseFormatFunc"></param>
        /// <param name="presentModeFunc"></param>
        /// <param name="extentFunc"></param>
        /// <param name="indicesFunc"></param>
        /// <returns></returns>
        public VulkanSwapchain CreateSwapChain(VulkanRenderWapper window, Func<SurfaceFormatKHR[], SurfaceFormatKHR> chooseFormatFunc,
            Func<PresentModeKHR[], PresentModeKHR> presentModeFunc,
            Func<SurfaceCapabilitiesKHR, Extent2D> extentFunc,
            Func<IEnumerable<QueueFamilyIndices>, QueueFamilyIndices> indicesFunc = null)
        {
            if (chooseFormatFunc == null) throw new ArgumentNullException(nameof(chooseFormatFunc));
            if (presentModeFunc == null) throw new ArgumentNullException(nameof(presentModeFunc));
            if (extentFunc == null) throw new ArgumentNullException(nameof(extentFunc));
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            KhrSwapchain extension = GetSwapchainExtension();
            var surface = window.GetVulkanSurface();
            var phy = PhysicalDevice;
            var detail = phy.QuerySwapChainSupport(surface);
            SurfaceFormatKHR format = chooseFormatFunc(detail.Formats);
            PresentModeKHR mode = presentModeFunc(detail.PresentModes);
            Extent2D extent = extentFunc(detail.Capabilities);
            QueueFamilyIndices familyIndices;
            if (indicesFunc != null) familyIndices = indicesFunc(FamilyIndices);
            else familyIndices = FamilyIndices[0];
            if (!phy.GetSurfaceSupport(familyIndices.FamilyIndex, surface))
                throw new NotSupportedException("Current QueueFamily is not supported to present");
            uint imageCount = detail.Capabilities.MinImageCount + 1;
            if (detail.Capabilities.MaxImageCount > 0 && imageCount > detail.Capabilities.MaxImageCount)
                imageCount = detail.Capabilities.MaxImageCount;

            unsafe
            {
                uint* p = stackalloc uint[] { familyIndices.FamilyIndex };
                SwapchainCreateInfoKHR infoKHR = new SwapchainCreateInfoKHR();
                infoKHR.SType = StructureType.SwapchainCreateInfoKhr;
                infoKHR.Surface = surface;
                infoKHR.PresentMode = mode;
                infoKHR.CompositeAlpha = CompositeAlphaFlagsKHR.OpaqueBitKhr;
                infoKHR.PQueueFamilyIndices = p;
                infoKHR.QueueFamilyIndexCount = 1;
                infoKHR.ImageSharingMode = familyIndices.FamilyType.HasFlag(QueueType.Present | QueueType.Graphics) ? SharingMode.Exclusive : SharingMode.Concurrent;
                infoKHR.ImageUsage = ImageUsageFlags.ColorAttachmentBit;
                infoKHR.ImageExtent = extent;
                infoKHR.ImageColorSpace = format.ColorSpace;
                infoKHR.ImageFormat = format.Format;
                infoKHR.MinImageCount = imageCount;
                infoKHR.ImageArrayLayers = 1;
                infoKHR.PreTransform = detail.Capabilities.CurrentTransform;
                infoKHR.Clipped = true;
                var r = SwapchainExtension.CreateSwapchain(RawHandle, in infoKHR, null, out SwapchainKHR swapchain);
                if (r != Result.Success) throw new VulkanOperationException("CreateSwapchain", r);
                VulkanSwapchain vulkanSwapchain = new VulkanSwapchain(NativeAPI, swapchain, RawHandle, Instance, surface);
                vulkanSwapchain.Details = detail;
                vulkanSwapchain.SwapChainExtent = extent;
                vulkanSwapchain.SwapChainFormat = format;
                vulkanSwapchain.SwapChainPresentMode = mode;
                vulkanSwapchain.SharingMode = infoKHR.ImageSharingMode;
                vulkanSwapchain.QueueFamilyIndex = new QueueFamilyIndices[] { familyIndices };
                return vulkanSwapchain;
            }
        }
        /// <summary>
        /// 创建图像
        /// </summary>
        /// <param name="option"></param>
        /// <param name="extent"></param>
        /// <param name="guid"></param>
        /// <param name="memoryFlags"></param>
        /// <returns></returns>
        public VulkanImageView CreateImage(ImageAndImageViewCreateOption option,
            Extent3D extent, Guid? guid = null, MemoryPropertyFlags memoryFlags = 0)
        {
            var imageOpt = option.ImageCreateOption;
            var imageViewOpt = option.ImageViewCreateOption;
            if (imageOpt == null || imageViewOpt == null) throw new ArgumentNullException(nameof(option));
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            if (WorkQueues == null || !WorkQueues.Any()) throw new InvalidOperationException("Please InitWorkQueue first");
            imageOpt.Extent = extent;
            MemoryPropertyFlags memoryProperty = memoryFlags == 0 ? GetMemoryType(imageOpt.Usage) : memoryFlags;
            unsafe
            {
                PinnedObject<uint[]> fimlies = PinnedObject<uint[]>.Create(imageOpt.QueueFamilyIndices);
                ImageCreateInfo info = new ImageCreateInfo()
                {
                    SType = StructureType.ImageCreateInfo,
                    ArrayLayers = imageOpt.ArrayLayers,
                    Extent = extent,
                    Format = imageOpt.Format,
                    ImageType = imageOpt.ImageType,
                    InitialLayout = ImageLayout.Undefined,
                    MipLevels = imageOpt.MipLevels,
                    PQueueFamilyIndices = (uint*)fimlies.ToPointer(),
                    QueueFamilyIndexCount = imageOpt.QueueFamilyIndices == null ? 0 : (uint)imageOpt.QueueFamilyIndices.Length,
                    Samples = imageOpt.Samples,
                    SharingMode = imageOpt.SharingMode,
                    Tiling = imageOpt.Tiling,
                    Usage = imageOpt.Usage,
                    Flags = imageOpt.Flags
                };
                var result = NativeAPI.CreateImage(RawHandle, in info, null, out Image image);
                fimlies.Free();
                if (result != Result.Success) throw new VulkanOperationException("CreateImage", result);
                VulkanImage vulkanImage = new VulkanImage(NativeAPI, RawHandle, image);
                vulkanImage.CreateOption = imageOpt;
                vulkanImage.BindImageMemory(memoryProperty);
                var imageView = vulkanImage.CreateImageView(imageViewOpt);
                Guid id;
                if (guid == null)
                {
                    do { id = Guid.NewGuid(); }
                    while (Images.ContainsKey(id));
                    imageView.Guid = id;
                    Images.TryAdd(id, imageView);
                }
                else
                {
                    id = guid.Value;
                    imageView.Guid = id;
                    Images.AddOrUpdate(id, imageView, (key, value) => { value.Dispose(); return imageView; });
                }
                return imageView;
            }
        }
        /// <summary>
        /// 重建图像
        /// </summary>
        /// <param name="newExtent"></param>
        /// <param name="old"></param>
        /// <returns></returns>
        public VulkanImageView RecreateImage(VulkanImageView old, Extent3D newExtent)
        {          
            var viewCreate = old.CreateOption;
            var oldImage = old.VulkanImage;
            if (viewCreate == null || oldImage == null) throw new NotSupportedException("Target ImageView is not suppored to recreate");
            var imageCreate = oldImage.CreateOption;
            if (imageCreate == null) throw new NotSupportedException("Target ImageView is not suppored to recreate");
            ImageAndImageViewCreateOption newOption = new ImageAndImageViewCreateOption()
            {
                ImageCreateOption = imageCreate,
                ImageViewCreateOption = viewCreate
            };
            return CreateImage(newOption, newExtent, old.Guid);
        }
        /// <summary>
        /// 创建缓冲
        /// </summary>
        /// <param name="option"></param>
        /// <param name="size"></param>
        /// <param name="guid"></param>
        /// <param name="flags"></param>
        /// <returns></returns>
        public VulkanBuffer CreateBuffer(BufferCreateOption option, ulong size, 
            MemoryPropertyFlags flags, Guid? guid = null)
        {
            if (option == null) throw new ArgumentNullException(nameof(option));
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            option.Size = size;
            unsafe
            {
                PinnedObject<uint[]> family = PinnedObject<uint[]>.Create(option.QueueFamilyIndices);
                BufferCreateInfo info = new BufferCreateInfo
                {
                    PQueueFamilyIndices = (uint*)family.ToPointer(),
                    QueueFamilyIndexCount = option.QueueFamilyIndices == null ? 0 : (uint)option.QueueFamilyIndices.Length,
                    SharingMode = option.SharingMode,
                    Size = size,
                    SType = StructureType.BufferCreateInfo,
                    Usage = option.Usage
                };
                var result = NativeAPI.CreateBuffer(RawHandle, in info, null, out var pBuffer);
                family.Free();
                if (result != Result.Success) throw new VulkanOperationException("CreateBuffer", result);
                VulkanBuffer vulkanBuffer = new(NativeAPI, RawHandle, pBuffer)
                {
                    CreateOption = option
                };
                vulkanBuffer.BindBufferMemory(flags);
                Guid id;
                if (guid == null)
                {
                    do { id = Guid.NewGuid(); }
                    while (Buffers.ContainsKey(id));
                    vulkanBuffer.Guid = id;
                    Buffers.TryAdd(id, vulkanBuffer);
                }
                else
                {
                    id = guid.Value;
                    vulkanBuffer.Guid = id;
                    Buffers.AddOrUpdate(id, vulkanBuffer, (key, value) => { value.Dispose(); return vulkanBuffer; });
                }
                return vulkanBuffer;
            }
        }
        /// <summary>
        /// 创建缓冲视图
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="format"></param>
        /// <param name="offset"></param>
        /// <param name="range"></param>
        /// <returns></returns>
        public VulkanBufferView CreateBufferView(VulkanBuffer buffer, Format format, ulong offset, ulong range)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            if (buffer == null) throw new ArgumentNullException(nameof(buffer));
            if (buffer.RawHandle.Handle == 0) throw new InvalidOperationException("Invalid Buffer");
            unsafe
            {
                BufferViewCreateInfo info = new BufferViewCreateInfo()
                {
                    Buffer = buffer.RawHandle,
                    Format = format,
                    Offset = offset,
                    Range = range,
                    SType = StructureType.BufferViewCreateInfo
                };
                var result = NativeAPI.CreateBufferView(RawHandle, in info, null, out BufferView view);
                if (result != Result.Success) throw new VulkanOperationException("CreateBufferView", result);
                VulkanBufferView bufferView = new VulkanBufferView(NativeAPI, RawHandle, view, buffer);
                bufferView.Offset = offset;
                bufferView.Range = range;
                bufferView.Format = format;
                BufferViews.AddOrUpdate(bufferView.Guid, bufferView, (key, value) => bufferView);
                return bufferView;
            }
        }
        /// <summary>
        /// 重建缓冲
        /// </summary>
        /// <param name="old"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public VulkanBuffer RecreateBuffer(VulkanBuffer old, ulong size)
        {
            var option = old.CreateOption;
            if (option == null || old.DeviceMemory == null) throw new NotSupportedException("Target Buffer is not supported to recreate");
            var memoryFlags = old.DeviceMemory.PropertyFlags;
            return CreateBuffer(option, size, memoryFlags, old.Guid);
        }
        /// <summary>
        /// 销毁缓冲
        /// </summary>
        /// <param name="buffer"></param>
        public void DestroyBuffer(VulkanBuffer buffer)
        {
            Buffers.TryRemove(buffer.Guid, out _);
            buffer.Dispose();
        }
        /// <summary>
        /// 创建设备内存
        /// </summary>
        /// <param name="flags"></param>
        /// <param name="requirements"></param>
        /// <returns></returns>
        public VulkanDeviceMemory CreateDeviceMemory(MemoryPropertyFlags flags, MemoryRequirements requirements)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            MemoryAllocateInfo info = new()
            {
                AllocationSize = requirements.Size,
                SType = StructureType.MemoryAllocateInfo
            };
            bool isFlag = MemoryTypeFromProperties(requirements.MemoryTypeBits, flags, out int index);
            if (isFlag)
            {
                info.MemoryTypeIndex = (uint)index;
                Result r;
                unsafe
                {
                    r = NativeAPI.AllocateMemory(RawHandle, in info, null, out DeviceMemory memory);
                    if (r != Result.Success) throw new VulkanOperationException("AllocateMemory", r);
                    VulkanDeviceMemory deviceMemory = new(NativeAPI, RawHandle, memory)
                    {
                        Requirements = requirements,
                        PropertyFlags = flags
                    };
                    return deviceMemory;
                }
            }
            else throw new NotSupportedException($"Target device is not have memory type for memory flags {flags}");
        }
        /// <summary>
        /// 创建渲染通道
        /// </summary>
        /// <param name="option"></param>
        /// <param name="mode"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public VulkanRenderPass CreateRenderPass(string name, RenderPassCreateOption option, RenderPassMode mode)
        {
            if (option == null) throw new ArgumentNullException(nameof(option));
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            if (string.IsNullOrEmpty(name)) 
            {
                name = Guid.NewGuid().ToString("N");
                option.Name = name;
            }
            if (RenderPasses.ContainsKey(name))
            {
                name = Guid.NewGuid().ToString("N");
                option.Name = name;
            }
            unsafe
            {
                PinnedObject<AttachmentDescription[]> attachments = PinnedObject<AttachmentDescription[]>.Create(option.Attachments);
                PinnedObject<SubpassDependency[]> dependency = PinnedObject<SubpassDependency[]>.Create(option.Dependencies);
                SubpassDescription[] descriptions = null;
                var subpass = option.Subpasses;
                if (subpass != null && subpass.Length > 0)
                {
                    descriptions = new SubpassDescription[subpass.Length];
                    for(int i = 0; i < descriptions.Length; i++)
                    {
                        SubpassDescriptionOption local = subpass[i];
                        fixed (AttachmentReference* pInput = local.InputAttachments, 
                            pColor = local.ColorAttachments)
                        {
                            fixed (uint* uP = local.PreserveAttachments)
                            {
                                AttachmentReference pD = local.DepthStencilAttachment;
                                AttachmentReference pR = local.ResolveAttachments;
                                descriptions[i] = new SubpassDescription
                                {
                                    ColorAttachmentCount = local.ColorAttachments != null ? (uint)local.ColorAttachments.Length : 0,
                                    PColorAttachments = pColor,
                                    InputAttachmentCount = local.InputAttachments != null ? (uint)local.InputAttachments.Length : 0,
                                    PInputAttachments = pInput,
                                    PDepthStencilAttachment = pD.Layout == ImageLayout.Undefined ? null : &pD,
                                    PPreserveAttachments = uP,
                                    PreserveAttachmentCount = local.PreserveAttachments != null ? (uint)local.PreserveAttachments.Length : 0,
                                    PResolveAttachments = pR.Layout == ImageLayout.Undefined ? null : &pR,
                                    PipelineBindPoint = local.PipelineBindPoint
                                };
                            }
                        }
                    }
                }
                PinnedObject<SubpassDescription[]> pDs = PinnedObject<SubpassDescription[]>.Create(descriptions);
                RenderPassCreateInfo info = new RenderPassCreateInfo();
                info.SType = StructureType.RenderPassCreateInfo;
                info.AttachmentCount = option.Attachments != null ? (uint)option.Attachments.Length : 0;
                info.DependencyCount = option.Dependencies != null ? (uint)option.Dependencies.Length : 0;
                info.PAttachments = (AttachmentDescription*)attachments.ToPointer();
                info.PDependencies = (SubpassDependency*)dependency.ToPointer();
                info.PSubpasses = (SubpassDescription*)pDs.ToPointer();
                info.SubpassCount = option.Subpasses != null ? (uint)option.Subpasses.Length : 0;
                var r = NativeAPI.CreateRenderPass(RawHandle, in info, null, out RenderPass renderPass);
                attachments.Free(); dependency.Free(); pDs.Free();
                if (r != Result.Success) throw new VulkanOperationException("CreateRenderPass", r);
                VulkanRenderPass vulkanRenderPass = new VulkanRenderPass(name, NativeAPI, renderPass, this);
                vulkanRenderPass.Mode = mode;
                RenderPasses.AddOrUpdate(name, vulkanRenderPass, (key, value) => vulkanRenderPass);
                return vulkanRenderPass;
            }
        } 
        /// <summary>
        /// 创建信号量
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public VulkanSemaphore CreateSemaphore(string name = null)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            unsafe
            {
                SemaphoreCreateInfo info = new SemaphoreCreateInfo(StructureType.SemaphoreCreateInfo, null, null);
                var r = NativeAPI.CreateSemaphore(RawHandle, in info, null, out Semaphore semaphore);
                if (r != Result.Success) throw new VulkanOperationException("CreateSemaphore", r);
                if (name == null) name = Guid.NewGuid().ToString("N");
                VulkanSemaphore vulkanSemaphore = new VulkanSemaphore(NativeAPI, RawHandle, semaphore, name);
                Semaphores.AddOrUpdate(name, vulkanSemaphore, (key, value) => vulkanSemaphore);
                return vulkanSemaphore;
            }
        }
        /// <summary>
        /// 创建栅栏
        /// </summary>
        /// <returns></returns>
        public VulkanFence CreateFence()
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            uint id = (uint)System.Threading.Interlocked.Increment(ref Id);
            unsafe
            {
                FenceCreateInfo info = new FenceCreateInfo(StructureType.FenceCreateInfo, null, null);
                var result = NativeAPI.CreateFence(RawHandle, in info, null, out Fence fence1);
                if (result != Result.Success) throw new VulkanOperationException("CreateFence", result);
                VulkanFence fence = new VulkanFence(fence1, RawHandle, NativeAPI, id);
                Fences.AddOrUpdate(id, fence, (key, value) => fence);
                return fence;
            }

        }
        /// <summary>
        /// 销毁栅栏
        /// </summary>
        public void DestroyFence(VulkanFence fence)
        {
            Fences.TryRemove(fence.Id, out VulkanFence fence1);
            fence1.Dispose();
        }
        /// <summary>
        /// 创建事件
        /// </summary>
        /// <returns></returns>
        public VulkanEvent CreateEvent(string name = null)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            name ??= Guid.NewGuid().ToString("N");
            unsafe
            {
                EventCreateInfo info = new EventCreateInfo(StructureType.EventCreateInfo, null, null);
                var result = NativeAPI.CreateEvent(RawHandle, in info, null, out Event pEvent);
                if (result != Result.Success) throw new VulkanOperationException("CreateEvent", result);
                VulkanEvent vulkanEvent = new VulkanEvent(NativeAPI, RawHandle, pEvent, name);
                Events.AddOrUpdate(name, vulkanEvent, (key, value) => vulkanEvent);
                return vulkanEvent;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void DestroyEvent(VulkanEvent @event)
        {
            Events.TryRemove(@event.Name, out VulkanEvent vulkanEvent);
            vulkanEvent.Dispose();
        }
        /// <summary>
        /// 创建采样器
        /// </summary>
        /// <param name="name"></param>
        /// <param name="createOption"></param>
        /// <returns></returns>
        public VulkanSampler CreateSampler(string name, SamplerCreateOption createOption)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            if (createOption == null) throw new ArgumentNullException(nameof(createOption));
            if (string.IsNullOrEmpty(name)) name = Guid.NewGuid().ToString("N") + "SAMPLER";
            if (Samplers.TryGetValue(name, out VulkanSampler old))
            {
                return old;
            }
            unsafe
            {
                SamplerCreateInfo info = new SamplerCreateInfo()
                {
                    AddressModeU = createOption.AddressModeU,
                    AddressModeV = createOption.AddressModeV,
                    AddressModeW = createOption.AddressModeW,
                    AnisotropyEnable = createOption.AnisotropyEnable,
                    MaxAnisotropy = createOption.MaxAnisotropy,
                    BorderColor = createOption.BorderColor,
                    CompareEnable = createOption.CompareEnable,
                    CompareOp = createOption.CompareOp,
                    MagFilter = createOption.MagFilter,
                    MaxLod = createOption.MaxLod,
                    MinFilter = createOption.MinFilter,
                    MinLod = createOption.MinLod,
                    MipLodBias = createOption.MipLodBias,
                    MipmapMode = createOption.MipmapMode,
                    UnnormalizedCoordinates = createOption.UnnormalizedCoordinates,
                    SType = StructureType.SamplerCreateInfo
                };
                var result = NativeAPI.CreateSampler(RawHandle, in info, null, out Sampler s);
                if (result != Result.Success) throw new VulkanOperationException("CreateSampler", result);
                VulkanSampler sampler = new VulkanSampler(NativeAPI, RawHandle, s, name);
                Samplers.AddOrUpdate(name, sampler, (key, value) => sampler);
                return sampler;
            }
        }

        /// <summary>
        /// 创建资源用命令池
        /// </summary>
        /// <param name="queueFimilyIndex"></param>
        /// <returns></returns>
        public VulkanCommandPool CreateResourcesCommandPool(uint queueFimilyIndex)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            unsafe
            {
                CommandPoolCreateInfo info = new CommandPoolCreateInfo(StructureType.CommandPoolCreateInfo, null, CommandPoolCreateFlags.TransientBit, queueFimilyIndex);
                var r = NativeAPI.CreateCommandPool(RawHandle, in info, null, out CommandPool commandPool);
                if (r != Result.Success) throw new VulkanOperationException("CreateCommandPool", r);
                VulkanCommandPool pool = new VulkanCommandPool(NativeAPI, commandPool, this, RawHandle);
                pool.QueueFamilyIndex = queueFimilyIndex;
                ResourcesCommandPool = pool;
                return pool;
            }
        }
        /// <summary>
        /// 创建命令池
        /// </summary>
        /// <param name="commandPool"></param>
        /// <returns></returns>
        public VulkanCommandPool CreateCommandPool(Action<VulkanDevice, CommandPoolCreateOption> commandPool)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            if (commandPool == null) throw new ArgumentNullException(nameof(commandPool));
            CommandPoolCreateOption commandPoolOption = new CommandPoolCreateOption();
            commandPool(this, commandPoolOption);
            unsafe
            {
                CommandPoolCreateInfo info = new CommandPoolCreateInfo(StructureType.CommandPoolCreateInfo, null,
                    commandPoolOption.Flags, commandPoolOption.QueueFamilyIndex);
                var r = NativeAPI.CreateCommandPool(RawHandle, in info, null, out CommandPool pool);
                if (r != Result.Success) throw new VulkanOperationException("CreateCommandPool", r);
                VulkanCommandPool vulkanCommandPool = new VulkanCommandPool(NativeAPI, pool, this, RawHandle);
                vulkanCommandPool.QueueFamilyIndex = info.QueueFamilyIndex;
                CommandPools.Add(vulkanCommandPool);
                return vulkanCommandPool;
            }
        }

        /// <summary>
        /// 创建描述符集池
        /// </summary>
        /// <returns></returns>
        public DescriptorPool CreateDescriptorPool(DescriptorPoolCreateOption option)
        {
            if (option == null) throw new ArgumentNullException(nameof(option));
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            DescriptorPoolCreateInfo info = new DescriptorPoolCreateInfo();
            unsafe
            {
                PinnedObject<DescriptorPoolSize[]> pPool = PinnedObject<DescriptorPoolSize[]>.Create(option.PoolSizes);
                info.Flags = option.Flags;
                info.MaxSets = option.MaxSets;
                info.PoolSizeCount = option.PoolSizes == null ? 0 : (uint)option.PoolSizes.Length;
                info.PPoolSizes = (DescriptorPoolSize*)pPool.ToPointer();
                info.SType = StructureType.DescriptorPoolCreateInfo;
                var result = NativeAPI.CreateDescriptorPool(RawHandle, in info, null, out DescriptorPool pool);
                pPool.Free();
                if (result != Result.Success) throw new VulkanOperationException("CreateDescriptorPool", result);
                return pool;
            }
        }
        /// <summary>
        /// 创建管线布局
        /// </summary>
        /// <param name="setlayouts"></param>
        /// <param name="option"></param>
        /// <returns></returns>
        public PipelineLayout CreatePipelineLayout(DescriptorSetLayout[] setlayouts, PipelineLayoutCreateOption option)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            unsafe
            {
                PipelineLayoutCreateInfo info = new PipelineLayoutCreateInfo();
                PinnedObject<PushConstantRange[]> range = PinnedObject<PushConstantRange[]>.Create(null);
                if (option != null)
                {
                    range = PinnedObject<PushConstantRange[]>.Create(option.PushConstantRanges);
                    info.PPushConstantRanges = (PushConstantRange*)range.ToPointer();
                    info.PushConstantRangeCount = option.PushConstantRanges == null ? 0 : (uint)option.PushConstantRanges.Length;
                }
                PinnedObject<DescriptorSetLayout[]> layout = PinnedObject<DescriptorSetLayout[]>.Create(setlayouts);
                info.PSetLayouts = (DescriptorSetLayout*)layout.ToPointer();
                info.SetLayoutCount = setlayouts == null ? 0 : (uint)setlayouts.Length;
                info.SType = StructureType.PipelineLayoutCreateInfo;
                var result = NativeAPI.CreatePipelineLayout(RawHandle, in info, null, out PipelineLayout pipelineLayout);
                range.Free(); layout.Free();
                if (result != Result.Success) throw new VulkanOperationException("CreatePipelineLayout", result);
                return pipelineLayout;
            }
        }
        /// <summary>
        /// 创建描述符布局
        /// </summary>
        /// <param name="option"></param>
        /// <returns></returns>
        public DescriptorSetLayout CreateDescriptorSetLayout(DescriptorSetLayoutCreateOption option)
        {
            if (option == null) throw new ArgumentNullException(nameof(option));
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            var bindings = option.Bindings;
            unsafe
            {
                Span<DescriptorSetLayoutBinding> layoutBindings = stackalloc DescriptorSetLayoutBinding[bindings == null ? 0 : bindings.Length];
                for (int i = 0; i < layoutBindings.Length; i++)
                {
                    var l = bindings[i];
                    layoutBindings[i] = new DescriptorSetLayoutBinding
                    {
                        Binding = l.Binding,
                        DescriptorCount = l.DescriptorCount,
                        DescriptorType = l.DescriptorType,
                        StageFlags = l.StageFlags,
                        PImmutableSamplers = null
                    };
                }
                fixed(DescriptorSetLayoutBinding* p = layoutBindings)
                {
                    DescriptorSetLayoutCreateInfo info = new DescriptorSetLayoutCreateInfo();
                    info.BindingCount = (uint)layoutBindings.Length;
                    info.Flags = option.Flags;
                    info.PBindings = p;
                    info.SType = StructureType.DescriptorSetLayoutCreateInfo;
                    var result = NativeAPI.CreateDescriptorSetLayout(RawHandle, in info, null, out DescriptorSetLayout setLayout);
                    if (result != Result.Success) throw new VulkanOperationException("CreateDescriptorSetLayout", result);
                    return setLayout;
                }
            }
        }
        /// <summary>
        /// 申请描述符集
        /// </summary>
        /// <returns></returns>
        public DescriptorSet[] AllocateDescriptorSet(DescriptorPool pool, params DescriptorSetLayout[] layouts)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            if (layouts == null || layouts.Length < 0) throw new ArgumentNullException("No SetLayout");
            unsafe
            {
                PinnedObject<DescriptorSetLayout[]> l = PinnedObject<DescriptorSetLayout[]>.Create(layouts);
                DescriptorSetAllocateInfo info = new DescriptorSetAllocateInfo()
                {
                    DescriptorPool = pool,
                    DescriptorSetCount = 1,
                    PSetLayouts = (DescriptorSetLayout*)l.ToPointer(),
                    SType = StructureType.DescriptorSetAllocateInfo
                };
                DescriptorSet[] sets = new DescriptorSet[layouts.Length];
                fixed(DescriptorSet* pSet = sets)
                {
                    var result = NativeAPI.AllocateDescriptorSets(RawHandle, info, pSet);
                    l.Free();
                    if (result != Result.Success) throw new VulkanOperationException("AllocateDescriptorSets", result);
                    return sets;
                }
            }
        }
        /// <summary>
        /// 摧毁描述符布局
        /// </summary>
        /// <param name="layouts"></param>
        public void DestroyDescriptorSetLayout(params DescriptorSetLayout[] layouts)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            unsafe
            {
                if(layouts != null)
                {
                    foreach(var item in layouts)
                    {
                        NativeAPI.DestroyDescriptorSetLayout(RawHandle, item, null);
                    }
                }
            }
        }
        /// <summary>
        /// 摧毁管线布局
        /// </summary>
        /// <param name="layout"></param>
        public void DestroyPipelineLayout(PipelineLayout layout)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            unsafe
            {
                NativeAPI.DestroyPipelineLayout(RawHandle, layout, null);
            }
        }
        /// <summary>
        /// 摧毁描述符池
        /// </summary>
        /// <param name="pool"></param>
        public void DestroyDescriptorPool(DescriptorPool pool)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            unsafe
            {
                NativeAPI.DestroyDescriptorPool(RawHandle, pool, null);
            }
        }
        /// <summary>
        /// 创建着色器模块
        /// </summary>
        /// <param name="size"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public ShaderModule CreateShaderModule(uint[] code, ulong size)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            unsafe
            {
                PinnedObject<uint[]> pC = PinnedObject<uint[]>.Create(code);
                ShaderModuleCreateInfo info = new ShaderModuleCreateInfo();
                info.CodeSize = new UIntPtr(size);
                info.PCode = (uint*)pC.ToPointer();
                info.SType = StructureType.ShaderModuleCreateInfo;
                var result = NativeAPI.CreateShaderModule(RawHandle, in info, null, out ShaderModule module);
                pC.Free();
                if (result != Result.Success) throw new VulkanOperationException("CreateShaderModule", result);
                return module;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Pipeline CreateGraphicsPipeline(VulkanRenderPass renderPass, PipelineCache pipelineCache, 
            PipelineLayout layout, GraphicsPipelineCreateOption createOption)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            unsafe
            {
                GraphicsPipelineCreateInfo info = new GraphicsPipelineCreateInfo();
                if (createOption.Stages == null) throw new NullReferenceException("Stages is null in this create option");
                var stages = createOption.Stages;
                var stageInfos = new PipelineShaderStageCreateInfo[stages.Length];
                PinnedObject<PipelineShaderStageCreateInfo[]> pStage = PinnedObject<PipelineShaderStageCreateInfo[]>.Create(stageInfos);
                PinnedObject<byte[]>[] pNames = new PinnedObject<byte[]>[stages.Length];
                for (int i = 0; i < stages.Length; i++)
                {
                    var local = stages[i];
                    string name = local.Name ?? "main";
                    pNames[i] = PinnedObject<byte[]>.Create(Encoding.UTF8.GetBytes(name));
                    stageInfos[i] = new PipelineShaderStageCreateInfo
                    {
                        Module = local.Module,
                        PName = pNames[i].ToPointer(),
                        Stage = local.Stage,
                        SType = StructureType.PipelineShaderStageCreateInfo,
                    };
                }
                info.PStages = (PipelineShaderStageCreateInfo*)pStage.ToPointer();
                info.StageCount = stageInfos == null ? 0 : (uint)stageInfos.Length;

                if (createOption.VertexInputState == null) throw new NullReferenceException($"{nameof(createOption.VertexInputState)} must be not null");
                PipelineVertexInputStateCreateOption inputStateOption = createOption.VertexInputState;
                PinnedObject<VertexInputAttributeDescription[]> pVInput = PinnedObject<VertexInputAttributeDescription[]>.Create(inputStateOption.VertexAttributeDescriptions);
                PinnedObject<VertexInputBindingDescription[]> pBinding = PinnedObject<VertexInputBindingDescription[]>.Create(inputStateOption.VertexBindingDescriptions);
                PipelineVertexInputStateCreateInfo inputInfo = new PipelineVertexInputStateCreateInfo
                {
                    PVertexAttributeDescriptions = (VertexInputAttributeDescription*)pVInput.ToPointer(),
                    VertexAttributeDescriptionCount = inputStateOption.VertexAttributeDescriptions == null ? 0 : (uint)inputStateOption.VertexAttributeDescriptions.Length,
                    PVertexBindingDescriptions = (VertexInputBindingDescription*)pBinding.ToPointer(),
                    VertexBindingDescriptionCount = inputStateOption.VertexBindingDescriptions == null ? 0 : (uint)inputStateOption.VertexBindingDescriptions.Length,
                    SType = StructureType.PipelineVertexInputStateCreateInfo
                };
                info.PVertexInputState = &inputInfo;

                if (createOption.InputAssemblyState == null) throw new NullReferenceException($"{nameof(createOption.InputAssemblyState)} must be not null");
                PipelineInputAssemblyStateCreateOption assemblyStateOption = createOption.InputAssemblyState;
                PipelineInputAssemblyStateCreateInfo assemblyInfo = new PipelineInputAssemblyStateCreateInfo
                {
                    PrimitiveRestartEnable = assemblyStateOption.PrimitiveRestartEnable,
                    Topology = assemblyStateOption.Topology,
                    SType = StructureType.PipelineInputAssemblyStateCreateInfo
                };
                info.PInputAssemblyState = &assemblyInfo;

                if (createOption.RasterizationState == null) throw new NullReferenceException($"{nameof(createOption.RasterizationState)} must be not null");
                PipelineRasterizationStateCreateOption restStateOption = createOption.RasterizationState;
                PipelineRasterizationStateCreateInfo rastInfo = new PipelineRasterizationStateCreateInfo
                {
                    CullMode = restStateOption.CullMode,
                    DepthBiasClamp = restStateOption.DepthBiasClamp,
                    DepthBiasConstantFactor = restStateOption.DepthBiasConstantFactor,
                    DepthBiasEnable = restStateOption.DepthBiasEnable,
                    DepthBiasSlopeFactor = restStateOption.DepthBiasSlopeFactor,
                    DepthClampEnable = restStateOption.DepthClampEnable,
                    FrontFace = restStateOption.FrontFace,
                    LineWidth = restStateOption.LineWidth,
                    PolygonMode = restStateOption.PolygonMode,
                    RasterizerDiscardEnable = restStateOption.RasterizerDiscardEnable,
                    SType = StructureType.PipelineRasterizationStateCreateInfo
                };
                info.PRasterizationState = &rastInfo;

                PinnedObject<Rect2D[]> pS = PinnedObject<Rect2D[]>.Null();
                PinnedObject<Viewport[]> pV = PinnedObject<Viewport[]>.Null();
                if (createOption.ViewportState != null)
                {
                    PipelineViewportStateCreateOption viewportStateOption = createOption.ViewportState;
                    pS = PinnedObject<Rect2D[]>.Create(viewportStateOption.Scissors);
                    pV = PinnedObject<Viewport[]>.Create(viewportStateOption.Viewports);
                    PipelineViewportStateCreateInfo viewportInfo = new PipelineViewportStateCreateInfo
                    {
                        PScissors = (Rect2D*)pS.ToPointer(),
                        PViewports = (Viewport*)pV.ToPointer(),
                        ScissorCount = viewportStateOption.Scissors == null ? 0 : (uint)viewportStateOption.Scissors.Length,
                        ViewportCount = viewportStateOption.Viewports == null ? 0 : (uint)viewportStateOption.Viewports.Length,
                        SType = StructureType.PipelineViewportStateCreateInfo
                    };
                    info.PViewportState = &viewportInfo;
                }

                PinnedObject<PipelineColorBlendAttachmentState[]> pC = PinnedObject<PipelineColorBlendAttachmentState[]>.Null();
                if (createOption.ColorBlendState != null)
                {
                    PipelineColorBlendStateCreateOption blendStateOption = createOption.ColorBlendState;
                    pC = PinnedObject<PipelineColorBlendAttachmentState[]>.Create(blendStateOption.Attachments);
                    PipelineColorBlendStateCreateInfo b = new PipelineColorBlendStateCreateInfo
                    {
                        PAttachments = (PipelineColorBlendAttachmentState*)pC.ToPointer(),
                        AttachmentCount = blendStateOption.Attachments == null ? 0 : (uint)blendStateOption.Attachments.Length,
                        LogicOp = blendStateOption.LogicOp,
                        LogicOpEnable = blendStateOption.LogicOpEnable,
                        SType = StructureType.PipelineColorBlendStateCreateInfo
                    };
                    PipelineColorBlendStateCreateInfo* pB = &b;
                    pB->BlendConstants[0] = blendStateOption.BlendConstants.Item1;
                    pB->BlendConstants[1] = blendStateOption.BlendConstants.Item2;
                    pB->BlendConstants[2] = blendStateOption.BlendConstants.Item3;
                    pB->BlendConstants[3] = blendStateOption.BlendConstants.Item4;
                    info.PColorBlendState = &b;
                }

                if (createOption.DepthStencilState != null)
                {
                    PipelineDepthStencilStateCreateOption dStencilStateOption = createOption.DepthStencilState;
                    PipelineDepthStencilStateCreateInfo dStencilStateInfo = new PipelineDepthStencilStateCreateInfo
                    {
                        Back = dStencilStateOption.Back,
                        DepthBoundsTestEnable = dStencilStateOption.DepthBoundsTestEnable,
                        DepthCompareOp = dStencilStateOption.DepthCompareOp,
                        DepthTestEnable = dStencilStateOption.DepthTestEnable,
                        DepthWriteEnable = dStencilStateOption.DepthWriteEnable,
                        Front = dStencilStateOption.Front,
                        MaxDepthBounds = dStencilStateOption.MaxDepthBounds,
                        MinDepthBounds = dStencilStateOption.MinDepthBounds,
                        StencilTestEnable = dStencilStateOption.StencilTestEnable,
                        SType = StructureType.PipelineDepthStencilStateCreateInfo
                    };
                    info.PDepthStencilState = &dStencilStateInfo;
                }

                PinnedObject<uint[]> pMask = PinnedObject<uint[]>.Null();
                if (createOption.MultisampleState != null)
                {
                    PipelineMultisampleStateCreateOption mOption = createOption.MultisampleState;
                    pMask = PinnedObject<uint[]>.Create(mOption.SampleMask);
                    PipelineMultisampleStateCreateInfo mStateInfo = new PipelineMultisampleStateCreateInfo
                    {
                        AlphaToCoverageEnable = mOption.AlphaToCoverageEnable,
                        AlphaToOneEnable = mOption.AlphaToOneEnable,
                        MinSampleShading = mOption.MinSampleShading,
                        RasterizationSamples = mOption.RasterizationSamples,
                        SampleShadingEnable = mOption.SampleShadingEnable,
                        PSampleMask = (uint*)pMask.ToPointer(),
                        SType = StructureType.PipelineMultisampleStateCreateInfo
                    };
                    info.PMultisampleState = &mStateInfo;
                }

                if (createOption.DynamicState != null)
                {
                    var state = createOption.DynamicState.DynamicStates;
                    if (state != null)
                    {
                        int* pState = stackalloc int[state.Length];
                        for(int i = 0; i < state.Length; i++)
                        {
                            *(pState + i) = (int)state[i];
                        }
                        PipelineDynamicStateCreateInfo dStateInfo = new PipelineDynamicStateCreateInfo
                        {
                            PDynamicStates = (DynamicState*)pState,
                            DynamicStateCount = (uint)state.Length,
                            SType = StructureType.PipelineDynamicStateCreateInfo
                        };
                        info.PDynamicState = &dStateInfo;
                    }
                }

                info.BasePipelineIndex = createOption.BasePipelineIndex;
                info.Layout = layout;
                info.RenderPass = renderPass.RawHandle;
                info.SType = StructureType.GraphicsPipelineCreateInfo;
                info.Subpass = createOption.Subpass;

                var result = NativeAPI.CreateGraphicsPipelines(RawHandle, PipelineCache, 1, in info, null, out var pipeline);
                pStage.Free(); foreach (var item in pNames) item.Free();
                pVInput.Free(); pBinding.Free(); pS.Free(); pV.Free();
                pC.Free(); pMask.Free();
                if (result != Result.Success) throw new VulkanOperationException("CreateGraphicsPipelines", result);
                return pipeline;
            }
        }
        /// <summary>
        /// 创建计算着色器
        /// </summary>
        /// <param name="pipelineCache"></param>
        /// <param name="layout"></param>
        /// <param name="option"></param>
        /// <returns></returns>
        public Pipeline CreateComputePipeline(PipelineCache pipelineCache, PipelineLayout layout, PipelineShaderStageCreateOption option)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            unsafe
            {
                ComputePipelineCreateInfo info = new ComputePipelineCreateInfo();
                byte[] name = Encoding.UTF8.GetBytes(option.Name);
                PinnedObject<byte[]> pName = PinnedObject<byte[]>.Create(name);
                PipelineShaderStageCreateInfo stage = new PipelineShaderStageCreateInfo()
                {
                    Module = option.Module,
                    PName = pName.ToPointer(),
                    Stage = option.Stage,
                    SType = StructureType.PipelineShaderStageCreateInfo
                };
                info.SType = StructureType.ComputePipelineCreateInfo;
                info.Stage = stage;
                info.Layout = layout;
                var result = NativeAPI.CreateComputePipelines(RawHandle, pipelineCache, 1, in info, null, out Pipeline pipeline);
                pName.Free();
                if (result != Result.Success) throw new VulkanOperationException("CreateGraphicsPipelines", result);
                return pipeline;
            }

        }
        /// <summary>
        /// 销毁着色器模块
        /// </summary>
        /// <param name="modules"></param>
        public void DestroyShaderModules(IEnumerable<ShaderModule> modules)
        {
            if(modules != null)
            {
                unsafe
                {
                    foreach(var item in modules)
                    {
                        NativeAPI.DestroyShaderModule(RawHandle, item, null);
                    }
                }
            }
        }

        /// <summary>
        /// 更新描述符集
        /// </summary>
        /// <param name="writes"></param>
        /// <param name="copys"></param>
        public void UpdateDescriptorSets(Span<WriteDescriptorSet> writes, Span<CopyDescriptorSet> copys)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            Silk.NET.Vulkan.WriteDescriptorSet[] writeDescriptors = null;
            Silk.NET.Vulkan.CopyDescriptorSet[] copyDescriptors = null;
            unsafe
            {
                Span<PinnedObject<DescriptorImageInfo[]>> pImages = null;
                Span<PinnedObject<DescriptorBufferInfo[]>> pBuffers = null;
                Span<PinnedObject<BufferView[]>> pViews = null;
                if (writes != null && writes.Length > 0)
                {
                    writeDescriptors = new Silk.NET.Vulkan.WriteDescriptorSet[writes.Length];
                    pImages = new PinnedObject<DescriptorImageInfo[]>[writes.Length];
                    pBuffers = new PinnedObject<DescriptorBufferInfo[]>[writes.Length];
                    pViews = new PinnedObject<BufferView[]>[writes.Length];
                    for (int i = 0; i < writes.Length; i++)
                    {
                        WriteDescriptorSet l = writes[i];
                        PinnedObject<DescriptorImageInfo[]> images = PinnedObject<DescriptorImageInfo[]>.Create(l.ImageInfo);
                        PinnedObject<DescriptorBufferInfo[]> buffers = PinnedObject<DescriptorBufferInfo[]>.Create(l.BufferInfo);
                        PinnedObject<BufferView[]> views = PinnedObject<BufferView[]>.Create(l.TexelBufferView);
                        pImages[i] = images; pBuffers[i] = buffers; pViews[i] = views;
                        writeDescriptors[i] = new Silk.NET.Vulkan.WriteDescriptorSet
                        {
                            DstArrayElement = l.DestinationArrayElement,
                            DescriptorCount = l.DescriptorCount,
                            DescriptorType = l.DescriptorType,
                            DstBinding = l.DestinationBinding,
                            DstSet = l.DestinationSet,
                            PBufferInfo = (DescriptorBufferInfo*)buffers.ToPointer(),
                            PImageInfo = (DescriptorImageInfo*)images.ToPointer(),
                            PTexelBufferView = (BufferView*)views.ToPointer(),
                            SType = StructureType.WriteDescriptorSet
                        };
                    }
                }
                if (copys != null && copys.Length > 0)
                {
                    copyDescriptors = new Silk.NET.Vulkan.CopyDescriptorSet[copys.Length];
                    for(int i = 0; i < copys.Length; i++)
                    {
                        CopyDescriptorSet l = copys[i];
                        copyDescriptors[i] = new Silk.NET.Vulkan.CopyDescriptorSet
                        {
                            DstArrayElement = l.DestinationArrayElement,
                            SrcArrayElement = l.SourceArrayElement,
                            DescriptorCount = l.DescriptorCount,
                            DstBinding = l.DestinationBinding,
                            DstSet = l.DestinationSet,
                            SrcBinding = l.SourceBinding,
                            SrcSet = l.SourceSet,
                            SType = StructureType.CopyDescriptorSet
                        };
                    }
                }
                ReadOnlySpan<Silk.NET.Vulkan.WriteDescriptorSet> wSpan = writeDescriptors == null ? Array.Empty<Silk.NET.Vulkan.WriteDescriptorSet>() : writeDescriptors;
                ReadOnlySpan<Silk.NET.Vulkan.CopyDescriptorSet> cSpan = copyDescriptors == null ? Array.Empty<Silk.NET.Vulkan.CopyDescriptorSet>() : copyDescriptors;
                NativeAPI.UpdateDescriptorSets(RawHandle, wSpan, cSpan);
                if (pImages != null) foreach (var item in pImages) item.Free();
                if (pBuffers != null) foreach (var item in pBuffers) item.Free();
                if (pViews != null) foreach (var item in pViews) item.Free();
            }
        }
        /// <summary>
        /// 提交
        /// </summary>
        /// <param name="queue"></param>
        /// <param name="infos"></param>
        /// <param name="fence"></param>
        public void Submit(Queue queue, Fence fence, params SubmitOption[] infos)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            if (infos == null || infos.Length < 1) throw new ArgumentNullException(nameof(infos));
            unsafe
            {
                Span<SubmitInfo> submits = new SubmitInfo[infos.Length];
                Span<PinnedObject<CommandBuffer[]>> pBuffers = stackalloc PinnedObject<CommandBuffer[]>[infos.Length];
                Span<PinnedObject<Semaphore[]>> pwSemaphore = stackalloc PinnedObject<Semaphore[]>[infos.Length];
                Span<PinnedObject<Semaphore[]>> psSemaphore = stackalloc PinnedObject<Semaphore[]>[infos.Length];
                Span<IntPtr> pMasks = stackalloc IntPtr[infos.Length];
                for(int i = 0; i < infos.Length; i++)
                {
                    var local = infos[i];
                    IntPtr pMask = IntPtr.Zero;
                    if(local.WaitDstStageMask != null)
                    {
                        pMask = Marshal.AllocHGlobal(local.WaitDstStageMask.Length * sizeof(int));
                        int* p = (int*)pMask;
                        for (int j = 0; j < local.WaitDstStageMask.Length; j++)
                        {
                            *(p + j) = (int)local.WaitDstStageMask[j];
                        }
                        pMasks[i] = pMask;
                    }
                    pBuffers[i] = PinnedObject<CommandBuffer[]>.Create(local.Buffers);
                    pwSemaphore[i] = PinnedObject<Semaphore[]>.Create(local.WaitSemaphores);
                    psSemaphore[i] = PinnedObject<Semaphore[]>.Create(local.SignalSemaphores);
                    submits[i] = new SubmitInfo
                    {
                        CommandBufferCount = local.Buffers == null ? 0 : (uint)local.Buffers.Length,
                        PCommandBuffers = (CommandBuffer*)pBuffers[i].ToPointer(),
                        SignalSemaphoreCount = local.SignalSemaphores == null ? 0 : (uint)local.SignalSemaphores.Length,
                        PSignalSemaphores = (Semaphore*)psSemaphore[i].ToPointer(),
                        PWaitSemaphores = (Semaphore*)pwSemaphore[i].ToPointer(),
                        WaitSemaphoreCount = local.WaitSemaphores == null ? 0 : (uint)local.WaitSemaphores.Length,
                        PWaitDstStageMask = (PipelineStageFlags*)pMask,
                        SType = StructureType.SubmitInfo
                    };
                }
                var result = NativeAPI.QueueSubmit(queue, submits, fence);
                foreach (var item in pBuffers) item.Free();
                foreach (var item in pwSemaphore) item.Free();
                foreach (var item in psSemaphore) item.Free();
                foreach (var item in pMasks)
                {
                    if (item != IntPtr.Zero) Marshal.FreeHGlobal(item);
                }
                if (result != Result.Success) throw new VulkanOperationException("QueueSubmit", result);

            }
        }

        /// <summary>
        /// 等待栅栏
        /// </summary>
        /// <param name="fences"></param>
        /// <param name="waitAll"></param>
        /// <param name="tiemout"></param>
        /// <returns></returns>
        public Result WaitForFences(Span<Fence> fences, bool waitAll, ulong tiemout)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            if (fences == null || fences.IsEmpty) return Result.Success;
            return NativeAPI.WaitForFences(RawHandle, fences, waitAll, tiemout);
        }
        /// <summary>
        /// 重置栅栏
        /// </summary>
        /// <param name="fences"></param>
        public void ResetFences(Span<Fence> fences)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            if (fences == null || fences.IsEmpty) return;
            NativeAPI.ResetFences(RawHandle, fences);
        }
        /// <summary>
        /// 等待帧渲染结束
        /// </summary>
        public void WaitIdle()
        {
            if (RawHandle.Handle == IntPtr.Zero) return;
            NativeAPI.DeviceWaitIdle(RawHandle);
        }
        /// <summary>
        /// 获取匹配的内存类型
        /// </summary>
        /// <param name="flags"></param>
        /// <param name="index"></param>
        /// <param name="typeBits"></param>
        /// <returns></returns>
        public bool MemoryTypeFromProperties(uint typeBits, MemoryPropertyFlags flags, out int index)
        {
            if (PhysicalDevice == null) throw new InvalidOperationException("Invalid Device, source PhysicalDevice is null");
            var phyp = PhysicalDevice.GetMemoryProperties();
            index = 0;
            for (int i = 0; i < 32; i++)
            {
                if ((typeBits & 1) == 1)
                {
                    if (flags == phyp.MemoryTypes[i].PropertyFlags || (flags & phyp.MemoryTypes[i].PropertyFlags) > 0)
                    {
                        index = i;
                        return true;
                    }
                }
                typeBits >>= 1;
            }
            return false;
        }

        /// <summary>
        /// 获取交换链扩展
        /// </summary>
        /// <returns></returns>
        private KhrSwapchain GetSwapchainExtension()
        {
            if (Instance.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Instance");
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            if (SwapchainExtension == null)
            {
                if (NativeAPI.TryGetDeviceExtension(Instance, RawHandle, out KhrSwapchain swapchain))
                {
                    SwapchainExtension = swapchain;
                    return swapchain;
                }
                else throw new NotSupportedException($"No Supported Extension {KhrSwapchain.ExtensionName}");
            }
            else return SwapchainExtension;
        }
        /// <summary>
        /// 获取内存属性
        /// </summary>
        /// <param name="imageUsage"></param>
        /// <returns></returns>
        private MemoryPropertyFlags GetMemoryType(ImageUsageFlags imageUsage)
        {
            MemoryPropertyFlags result = 0;
            if (imageUsage.HasFlag(ImageUsageFlags.ColorAttachmentBit))
            {
                result |= MemoryPropertyFlags.DeviceLocalBit | MemoryPropertyFlags.DeviceCoherentBitAmd;
            }
            else if (imageUsage.HasFlag(ImageUsageFlags.InputAttachmentBit))
            {
                result |= MemoryPropertyFlags.DeviceLocalBit | MemoryPropertyFlags.DeviceCoherentBitAmd;
            }
            else if (imageUsage.HasFlag(ImageUsageFlags.DepthStencilAttachmentBit))
            {
                result |= MemoryPropertyFlags.DeviceLocalBit | MemoryPropertyFlags.DeviceCoherentBitAmd;
            }
            else if (imageUsage.HasFlag(ImageUsageFlags.TransientAttachmentBit))
            {
                result |= MemoryPropertyFlags.LazilyAllocatedBit;
            }
            else if (imageUsage.HasFlag(ImageUsageFlags.SampledBit))
            {
                result |= MemoryPropertyFlags.DeviceLocalBit | MemoryPropertyFlags.DeviceCoherentBitAmd;
            }
            else if (imageUsage.HasFlag(ImageUsageFlags.TransferDstBit) ||
                imageUsage.HasFlag(ImageUsageFlags.TransferSrcBit))
            {
                result |= MemoryPropertyFlags.HostVisibleBit | MemoryPropertyFlags.HostCoherentBit;
            }
            else if (imageUsage.HasFlag(ImageUsageFlags.StorageBit))
            {
                result |= MemoryPropertyFlags.HostVisibleBit | MemoryPropertyFlags.HostCoherentBit;
            }
            else result = MemoryPropertyFlags.None;
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            if (IsDispose) return;
            IsDispose = true;
            if (RawHandle.Handle != IntPtr.Zero)
            {
                WaitIdle();
                VulkanMemoryManager.Current.FreeAll();
                foreach (var item in Samplers) item.Value?.Dispose();
                foreach (var item in Fences) item.Value?.Dispose();
                foreach (var item in Semaphores) item.Value?.Dispose();
                foreach (var item in Events) item.Value?.Dispose();
                foreach (var item in CommandPools) item?.Dispose();
                foreach (var item in RenderPasses) item.Value?.Dispose();
                foreach (var item in BufferViews) item.Value?.Dispose();
                foreach (var item in Buffers) item.Value?.Dispose();
                foreach (var item in Images) item.Value?.Dispose();
                CommandPools.Clear(); RenderPasses.Clear();
                Buffers.Clear(); Images.Clear();
                ResourcesCommandPool?.Dispose();
                unsafe 
                {
                    NativeAPI.DestroyPipelineCache(RawHandle, PipelineCache, null);
                    NativeAPI.DestroyDevice(RawHandle, null); 
                }

            }
        }
    }
}
