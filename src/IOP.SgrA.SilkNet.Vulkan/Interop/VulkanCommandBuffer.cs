﻿using Silk.NET.Vulkan;
using Silk.NET.Vulkan.Extensions.KHR;
using System;
using System.Collections.Generic;
using System.Text;
using Buffer = Silk.NET.Vulkan.Buffer;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 命令缓冲
    /// </summary>
    public class VulkanCommandBuffer : IVulkanWapper<CommandBuffer>
    {
        /// <summary>
        /// 
        /// </summary>
        public VulkanDevice DeviceWapper { get; }
        /// <summary>
        /// 
        /// </summary>
        public Device Device { get; }
        /// <summary>
        /// 
        /// </summary>
        public CommandBuffer RawHandle { get; }
        /// <summary>
        /// 命令池
        /// </summary>
        public VulkanCommandPool CommandPool { get; }
        /// <summary>
        /// 缓冲级别
        /// </summary>
        public CommandBufferLevel BufferLevel { get; protected internal set; }
        /// <summary>
        /// 底部API
        /// </summary>
        public Vk NativeAPI { get; protected internal set; } = Vk.GetApi();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="api"></param>
        /// <param name="vulkanDevice"></param>
        /// <param name="device"></param>
        /// <param name="buffer"></param>
        /// <param name="pool"></param>
        public VulkanCommandBuffer(Vk api, VulkanDevice vulkanDevice, Device device, CommandBuffer buffer, VulkanCommandPool pool)
        {
            NativeAPI = api;
            DeviceWapper = vulkanDevice ?? throw new ArgumentNullException(nameof(VulkanDevice));
            Device = device;
            RawHandle = buffer;
            CommandPool = pool;
        }

        /// <summary>
        /// 开始录制命令
        /// </summary>
        /// <param name="flags"></param>
        public void Begin(CommandBufferUsageFlags flags = CommandBufferUsageFlags.OneTimeSubmitBit)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            unsafe
            {
                CommandBufferBeginInfo info = new CommandBufferBeginInfo(StructureType.CommandBufferBeginInfo, null, flags, null);
                var result = NativeAPI.BeginCommandBuffer(RawHandle, in info);
                if (result != Result.Success) throw new VulkanOperationException("BeginCommandBuffer", result);
            }
        }
        /// <summary>
        /// 开始录制命令
        /// </summary>
        /// <param name="flags"></param>
        /// <param name="info"></param>
        public void Begin(CommandBufferUsageFlags flags, CommandBufferInheritanceInfo info)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            unsafe
            {
                info.SType = StructureType.CommandBufferInheritanceInfo;
                CommandBufferBeginInfo begin = new CommandBufferBeginInfo();
                begin.Flags = flags;
                begin.PInheritanceInfo = &info;
                begin.SType = StructureType.CommandBufferBeginInfo;
                var result = NativeAPI.BeginCommandBuffer(RawHandle, in begin);
                if (result != Result.Success) throw new VulkanOperationException("BeginCommandBuffer", result);
            }
        }
        /// <summary>
        /// 管线屏障
        /// </summary>
        /// <param name="srcStageMask"></param>
        /// <param name="dstStageMask"></param>
        /// <param name="memoryBarrier"></param>
        /// <param name="bufferBarrier"></param>
        /// <param name="imageBarrier"></param>
        /// <param name="flags"></param>
        public void PipelineBarrier(PipelineStageFlags srcStageMask, PipelineStageFlags dstStageMask,
            ReadOnlySpan<MemoryBarrier> memoryBarrier, ReadOnlySpan<BufferMemoryBarrier> bufferBarrier, ReadOnlySpan<ImageMemoryBarrier> imageBarrier,
            DependencyFlags flags = 0)
        {
            unsafe
            {
                NativeAPI.CmdPipelineBarrier(RawHandle, srcStageMask, dstStageMask, flags, memoryBarrier, bufferBarrier, imageBarrier);
            }
        }
        /// <summary>
        /// 管线屏障2
        /// </summary>
        /// <param name="info"></param>
        /// <exception cref="NotSupportedException"></exception>
        public void PipelineBarrier2KHR(DependencyInfo info)
        {
            bool r = DeviceWapper.TryGetDeviceExtensions(KhrSynchronization2.ExtensionName, out KhrSynchronization2 khr);
            if (!r) throw new NotSupportedException($"Cannot found extension with name {KhrSynchronization2.ExtensionName}");
            khr.CmdPipelineBarrier2(RawHandle, in info);
        }
        /// <summary>
        /// 结束命令录制
        /// </summary>
        public void End()
        {
            if (Device.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            var result = NativeAPI.EndCommandBuffer(RawHandle);
            if (result != Result.Success) throw new VulkanOperationException("EndCommandBuffer", result);
        }
        /// <summary>
        /// 重置
        /// </summary>
        public void Reset()
        {
            if (Device.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            var result = NativeAPI.ResetCommandBuffer(RawHandle, CommandBufferResetFlags.ReleaseResourcesBit);
            if (result != Result.Success) throw new VulkanOperationException("ResetCommandBuffer", result);
        }
        /// <summary>
        /// 开始渲染通道
        /// </summary>
        /// <param name="renderPass"></param>
        /// <param name="frameBuffer"></param>
        /// <param name="option"></param>
        /// <param name="contents"></param>
        public void BeginRenderPass(VulkanRenderPass renderPass, VulkanFrameBuffer frameBuffer, RenderPassBeginOption option, SubpassContents contents)
        {
            if (Device.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            if (option == null) throw new ArgumentNullException(nameof(option));
            if (option.ClearValues == null) option.ClearValues = Array.Empty<ClearValue>();
            unsafe
            {
                fixed(ClearValue* c = option.ClearValues)
                {
                    RenderPassBeginInfo info = new RenderPassBeginInfo();
                    info.ClearValueCount = (uint)option.ClearValues.Length;
                    info.PClearValues = c;
                    info.Framebuffer = frameBuffer.RawHandle;
                    info.RenderArea = option.RenderArea;
                    info.RenderPass = renderPass.RawHandle;
                    info.SType = StructureType.RenderPassBeginInfo;
                    NativeAPI.CmdBeginRenderPass(RawHandle, in info, contents);
                }               
            }
        }
        /// <summary>
        /// 下一个子通道
        /// </summary>
        /// <param name="contents"></param>
        /// <exception cref="InvalidOperationException"></exception>
        public void NextSubpass(SubpassContents contents)
        {
            if (Device.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            NativeAPI.CmdNextSubpass(RawHandle, contents);
        }
        /// <summary>
        /// 结束渲染通道
        /// </summary>
        public void EndRenderPass()
        {
            if (Device.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            NativeAPI.CmdEndRenderPass(RawHandle);
        }
        /// <summary>
        /// 绑定管线
        /// </summary>
        /// <param name="point"></param>
        /// <param name="pipeline"></param>
        public void BindPipeline(PipelineBindPoint point, VulkanPipeline pipeline)
        {
            if (Device.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            if (pipeline.Pipeline.Handle == 0) throw new InvalidOperationException("Invalid Pipeline");
            NativeAPI.CmdBindPipeline(RawHandle, point, pipeline.Pipeline);
        }
        /// <summary>
        /// 绑定描述符集
        /// </summary>
        /// <param name="point"></param>
        /// <param name="layout"></param>
        /// <param name="sets"></param>
        /// <param name="firstSet"></param>
        /// <param name="offsets"></param>
        public void BindDescriptorSets(PipelineBindPoint point, PipelineLayout layout, uint firstSet,
            Span<uint> offsets, params DescriptorSet[] sets)
        {
            if (Device.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            NativeAPI.CmdBindDescriptorSets(RawHandle, point, layout, firstSet, sets, offsets);
        }
        /// <summary>
        /// 绑定顶点缓冲
        /// </summary>
        public void BindVertexBuffers(uint firstBinding, Span<ulong> offsets, params VulkanBuffer[] buffers)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            if (buffers == null) throw new InvalidOperationException("No Vertex");
            unsafe
            {
                Span<Buffer> buffer = stackalloc Buffer[buffers.Length];
                for(int i = 0; i < buffer.Length; i++)
                {
                    buffer[i] = buffers[i].RawHandle;
                }
                NativeAPI.CmdBindVertexBuffers(RawHandle, firstBinding, buffer, offsets);
            }
        }
        /// <summary>
        /// 绑定索引缓冲
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="buffer"></param>
        /// <param name="type"></param>
        public void BindIndexBuffer(VulkanBuffer buffer, ulong offset, IndexType type = IndexType.Uint32)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            NativeAPI.CmdBindIndexBuffer(RawHandle, buffer.RawHandle, offset, type);
        }
        /// <summary>
        /// 推送常量
        /// </summary>
        /// <param name="layout"></param>
        /// <param name="flags"></param>
        /// <param name="offset"></param>
        /// <param name="data"></param>
        public void PushConstants(PipelineLayout layout, ShaderStageFlags flags, uint offset, Span<byte> data)
        {
            if (data == null || data.Length <= 0) return;
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            NativeAPI.CmdPushConstants(RawHandle, layout, flags, offset, data);
        }
        /// <summary>
        /// 绘制
        /// </summary>
        public void Draw(uint vertexCount, uint instanceCount, uint firstVex, uint firstInstance)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            NativeAPI.CmdDraw(RawHandle, vertexCount, instanceCount, firstVex, firstInstance);
        }
        /// <summary>
        /// 索引绘制
        /// </summary>
        /// <param name="indexCount"></param>
        /// <param name="instanceCount"></param>
        /// <param name="firstVex"></param>
        /// <param name="vertexOffet"></param>
        /// <param name="firstInstance"></param>
        public void DrawIndexed(uint indexCount, uint instanceCount, uint firstVex, int vertexOffet, uint firstInstance)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            NativeAPI.CmdDrawIndexed(RawHandle, indexCount, instanceCount, firstVex, vertexOffet, firstInstance);
        }
        /// <summary>
        /// 间接绘制
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <param name="drawCount"></param>
        /// <param name="stride"></param>
        public void DrawIndirect(VulkanBuffer buffer, ulong offset, uint drawCount, uint stride)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            NativeAPI.CmdDrawIndirect(RawHandle, buffer.RawHandle, offset, drawCount, stride);
        }
        /// <summary>
        /// 进行间接索引绘制
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <param name="drawCount"></param>
        /// <param name="stride"></param>
        public void DrawIndexedIndirect(VulkanBuffer buffer, ulong offset, uint drawCount, uint stride)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            NativeAPI.CmdDrawIndexedIndirect(RawHandle, buffer.RawHandle, offset, drawCount, stride);
        }
        /// <summary>
        /// 拷贝缓冲至图像
        /// </summary>
        public void CopyBufferToImage(VulkanBuffer buffer, VulkanImage image, ImageLayout finalLayout, 
            BufferImageCopy copy)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            unsafe
            {
                ReadOnlySpan<BufferImageCopy> pCopy = stackalloc BufferImageCopy[] { copy };
                NativeAPI.CmdCopyBufferToImage(RawHandle, buffer.RawHandle, image.RawHandle, finalLayout, pCopy);
            }
        }
        /// <summary>
        /// 将图片拷贝至缓冲
        /// </summary>
        /// <param name="image"></param>
        /// <param name="srcLayout"></param>
        /// <param name="buffer"></param>
        /// <param name="copy"></param>
        public void CopyImageToBuffer(VulkanImage image, ImageLayout srcLayout, VulkanBuffer buffer, BufferImageCopy copy)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            Span<BufferImageCopy> p = stackalloc BufferImageCopy[] { copy };
            NativeAPI.CmdCopyImageToBuffer(RawHandle, image.RawHandle, srcLayout, buffer.RawHandle, p);
        }
        /// <summary>
        /// 将图片拷贝至缓冲
        /// </summary>
        /// <param name="image"></param>
        /// <param name="srcLayout"></param>
        /// <param name="buffer"></param>
        /// <param name="copies"></param>
        /// <exception cref="InvalidOperationException"></exception>
        public void CopyImageToBuffer(VulkanImage image, ImageLayout srcLayout, VulkanBuffer buffer, Span<BufferImageCopy> copies)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            NativeAPI.CmdCopyImageToBuffer(RawHandle, image.RawHandle, srcLayout, buffer.RawHandle, copies);
        }
        /// <summary>
        /// 将图片拷贝至图片
        /// </summary>
        /// <param name="srcImage"></param>
        /// <param name="srcLayout"></param>
        /// <param name="dstImage"></param>
        /// <param name="dstLayout"></param>
        /// <param name="copies"></param>
        /// <exception cref="InvalidOperationException"></exception>
        public void CopyImage(VulkanImage srcImage, ImageLayout srcLayout, VulkanImage dstImage, ImageLayout dstLayout, Span<ImageCopy> copies)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            NativeAPI.CmdCopyImage(RawHandle, srcImage.RawHandle, srcLayout, dstImage.RawHandle, dstLayout, copies);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="src"></param>
        /// <param name="srcLayout"></param>
        /// <param name="dst"></param>
        /// <param name="dstLayout"></param>
        /// <param name="imageBlit"></param>
        /// <param name="filter"></param>
        public void BlitImage(VulkanImage src, ImageLayout srcLayout, VulkanImage dst, ImageLayout dstLayout,
            ImageBlit imageBlit, Filter filter)
        {
            if (Device.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            unsafe
            {
                ReadOnlySpan<ImageBlit> blits = stackalloc ImageBlit[] { imageBlit };
                NativeAPI.CmdBlitImage(RawHandle, src.RawHandle, srcLayout, dst.RawHandle, dstLayout, blits, filter);
            }
        }
        /// <summary>
        /// 清空图片颜色
        /// </summary>
        /// <param name="image"></param>
        /// <param name="srcLayout"></param>
        /// <param name="clearColor"></param>
        /// <param name="range"></param>
        public void ClearColorImage(VulkanImage image, ImageLayout srcLayout, ClearColorValue clearColor,
            ImageSubresourceRange range)
        {
            if (Device.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            unsafe
            {
                NativeAPI.CmdClearColorImage(RawHandle, image.RawHandle, srcLayout, in clearColor, 1, in range);
            }
        }
        /// <summary>
        /// 设置绘制窗口
        /// </summary>
        /// <param name="viewport"></param>
        /// <param name="reverse">反转NCD坐标系使其深度映射至0，1内，Y轴方向朝上，与其他图形API一致</param>
        public void SetViewport(Viewport viewport, bool reverse = true)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            if (reverse)
            {
                var height = viewport.Height;
                viewport.Height = -height;
                viewport.Y += height;
            }
            NativeAPI.CmdSetViewport(RawHandle, 0, stackalloc Viewport[] { viewport });
        }
        /// <summary>
        /// 设置裁剪区域
        /// </summary>
        /// <param name="rect"></param>
        public void SetScissor(Rect2D rect)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            NativeAPI.CmdSetScissor(RawHandle, 0, stackalloc Rect2D[] { rect });
        }
        /// <summary>
        /// 设置深度偏移
        /// </summary>
        /// <param name="depthBiasConstant"></param>
        /// <param name="depthBiasClamp"></param>
        /// <param name="depthBiasSlope"></param>
        public void SetDepthBias(float depthBiasConstant, float depthBiasClamp, float depthBiasSlope)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            NativeAPI.CmdSetDepthBias(RawHandle, depthBiasConstant, depthBiasClamp, depthBiasSlope);
        }
        /// <summary>
        /// 显示线框宽度
        /// </summary>
        public void SetLineWidth(float lineWidth)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            NativeAPI.CmdSetLineWidth(RawHandle, lineWidth);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="enable"></param>
        /// <exception cref="InvalidOperationException"></exception>
        public void SetRasterizerDiscardEnable(bool enable)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            NativeAPI.CmdSetRasterizerDiscardEnable(RawHandle, enable);
        }
        /// <summary>
        /// 设置深度测试
        /// </summary>
        /// <param name="enable"></param>
        /// <exception cref="InvalidOperationException"></exception>
        public void SetDepthTestEnable(bool enable)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            NativeAPI.CmdSetDepthTestEnable(RawHandle, enable);
        }
        /// <summary>
        /// 设置深度写入
        /// </summary>
        /// <param name="enable"></param>
        /// <exception cref="InvalidOperationException"></exception>
        public void SetDepthWriteEnable(bool enable)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            NativeAPI.CmdSetDepthWriteEnable(RawHandle, enable);
        }
        /// <summary>
        /// 执行次级缓冲
        /// </summary>
        /// <param name="buffers"></param>
        public void ExecuteCommands(Span<VulkanCommandBuffer> buffers)
        {
            if (buffers == null) throw new ArgumentNullException(nameof(buffers));
            if (buffers.IsEmpty) return;
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            Span<CommandBuffer> commands = stackalloc CommandBuffer[buffers.Length];
            for (int i = 0; i < commands.Length; i++)
            {
                commands[i] = buffers[i].RawHandle;
            }
            NativeAPI.CmdExecuteCommands(RawHandle, commands);
        }
        /// <summary>
        /// 重置事件
        /// </summary>
        public void ResetEvent(Event @event, PipelineStageFlags flags)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            if (@event.Handle == 0) throw new InvalidOperationException("Invalid Vulkan Event");
            NativeAPI.CmdResetEvent(RawHandle, @event, flags);
        }
        /// <summary>
        /// 触发事件
        /// </summary>
        /// <param name="event"></param>
        /// <param name="flags"></param>
        /// <exception cref="InvalidOperationException"></exception>
        public void SetEvent(Event @event, PipelineStageFlags flags)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            if (@event.Handle == 0) throw new InvalidOperationException("Invalid Vulkan Event");
            NativeAPI.CmdSetEvent(RawHandle, @event, flags);
        }
        /// <summary>
        /// 等待事件
        /// </summary>
        /// <param name="event"></param>
        /// <param name="srcFlags"></param>
        /// <param name="dstFlags"></param>
        public void WaitEvent(Event @event, PipelineStageFlags srcFlags, PipelineStageFlags dstFlags)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            if (@event.Handle == 0) throw new InvalidOperationException("Invalid Vulkan Event");
            unsafe
            {
                NativeAPI.CmdWaitEvents(RawHandle, stackalloc Event[] { @event }, srcFlags, dstFlags,
                    Array.Empty<MemoryBarrier>(), Array.Empty<BufferMemoryBarrier>(), Array.Empty<ImageMemoryBarrier>());
            }
        }
        /// <summary>
        /// 等待事件
        /// </summary>
        /// <param name="event"></param>
        /// <param name="srcFlags"></param>
        /// <param name="dstFlags"></param>
        /// <param name="memoryBarrier"></param>
        /// <param name="bufferBarrier"></param>
        /// <param name="imageBarrier"></param>
        public void WaitEvent(Event @event, PipelineStageFlags srcFlags, PipelineStageFlags dstFlags,
            ReadOnlySpan<MemoryBarrier> memoryBarrier, ReadOnlySpan<BufferMemoryBarrier> bufferBarrier, ReadOnlySpan<ImageMemoryBarrier> imageBarrier)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid CommandBuffer");
            if (@event.Handle == 0) throw new InvalidOperationException("Invalid Vulkan Event");
            unsafe
            {
                NativeAPI.CmdWaitEvents(RawHandle, stackalloc Event[] { @event }, srcFlags, dstFlags, memoryBarrier, bufferBarrier, imageBarrier);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
        }
    }
}
