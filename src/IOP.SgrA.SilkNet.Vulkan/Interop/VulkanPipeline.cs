﻿using Microsoft.Extensions.Configuration;
using Silk.NET.Vulkan;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 管线
    /// </summary>
    public class VulkanPipeline : IShaderPipeline<Pipeline>
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; }
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; }
        /// <summary>
        /// 配置
        /// </summary>
        public IConfiguration Configuration { get; }
        /// <summary>
        /// 管线
        /// </summary>
        public Pipeline Pipeline { get; protected internal set; }
        /// <summary>
        /// 设备
        /// </summary>
        public VulkanDevice Device { get; protected internal set; }
        /// <summary>
        /// 管线布局
        /// </summary>
        public PipelineLayout PipelineLayout { get; protected internal set; }
        /// <summary>
        /// 描述符布局
        /// </summary>
        public DescriptorSetLayout[] SetLayouts { get; protected internal set; }
        /// <summary>
        /// 描述符创建信息
        /// </summary>
        public DescriptorSetLayoutCreateOption[] DescriptorCreateInfo { get; internal set; }
        /// <summary>
        /// 推送常量信息
        /// </summary>
        public PushConstantRange[] Constants { get; internal set; }
        /// <summary>
        /// 推送常量
        /// </summary>
        public ulong ConstantRange { get; internal set; } = 0;
        /// <summary>
        /// 
        /// </summary>
        public List<ShaderModule> ShaderModules { get; protected internal set; } = new List<ShaderModule>();
        /// <summary>
        /// 管线描述符集
        /// </summary>
        public readonly ConcurrentDictionary<uint, DescriptorSet> PipelineSets = new ConcurrentDictionary<uint, DescriptorSet>();
        /// <summary>
        /// 管线用纹理
        /// </summary>
        public readonly ConcurrentDictionary<uint, VulkanTexture> PipelineTextures = new ConcurrentDictionary<uint, VulkanTexture>();

        /// <summary>
        /// vulkan管理器
        /// </summary>
        internal VulkanGraphicsManager VulkanManager { get; set; }
        /// <summary>
        /// 管线描述符池
        /// </summary>
        protected ConcurrentDictionary<uint, DescriptorPoolLinkedItem> PipelineDescriptorPool { get; set; } = new ConcurrentDictionary<uint, DescriptorPoolLinkedItem>();
        /// <summary>
        /// 对象用描述符集池
        /// </summary>
        protected ConcurrentDictionary<uint, LinkedList<DescriptorPoolLinkedItem>> ObjectsDescriptorPool { get; set; } = new ConcurrentDictionary<uint, LinkedList<DescriptorPoolLinkedItem>>();
        /// <summary>
        /// 同步锁
        /// </summary>
        protected readonly object SyncRoot = new object();
        private volatile bool IsDispose;
        private Vk NativeAPI = Vk.GetApi();

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="name"></param>
        /// <param name="id"></param>
        /// <param name="configuration"></param>
        /// <param name="logicDevice"></param>
        public VulkanPipeline(string name, int id, VulkanDevice logicDevice, IConfiguration configuration)
        {
            Name = name;
            Id = id;
            Device = logicDevice;
            Configuration = configuration;
            NativeAPI = logicDevice.NativeAPI;
        }

        /// <summary>
        /// 尝试获取管线纹理
        /// </summary>
        /// <param name="setIndex"></param>
        /// <param name="dstbinding"></param>
        /// <param name="vulkanTexture"></param>
        /// <returns></returns>
        public bool TryGetPipelineTexture(uint setIndex, uint dstbinding, out VulkanTexture vulkanTexture)
        {
            uint c = Combine(setIndex, dstbinding);
            return PipelineTextures.TryGetValue(c, out vulkanTexture);
        }
        /// <summary>
        /// 获取管线纹理
        /// </summary>
        /// <param name="setIndex"></param>
        /// <param name="dstbinding"></param>
        /// <returns></returns>
        public VulkanTexture GetPipelineTexture(uint setIndex, uint dstbinding)
        {
            uint c = Combine(setIndex, dstbinding);
            if (PipelineTextures.TryGetValue(c, out VulkanTexture texture))
            {
                return texture;
            }
            throw new ArgumentException($"Cannot found pipeline texture with set {setIndex}, binding {dstbinding}");
        }
        /// <summary>
        /// 添加管线纹理
        /// </summary>
        /// <param name="setIndex"></param>
        /// <param name="dstbinding"></param>
        /// <param name="texture"></param>
        public void AddPipelineTexture(uint setIndex, uint dstbinding, VulkanTexture texture)
        {
            uint c = Combine(setIndex, dstbinding);
            texture.SetIndex = setIndex;
            texture.Binding = dstbinding;
            if (texture.GetDescriptorSet().Handle == 0) texture.BindDescriptorSet(GetPipelineDescriptorSet(setIndex));
            texture.UpdateDescriptorSets();
            PipelineTextures.AddOrUpdate(c, texture, (key, value) => texture);
        }
        /// <summary>
        /// 获取管线描述符集
        /// </summary>
        /// <param name="setIndex"></param>
        /// <returns></returns>
        public DescriptorSet GetPipelineDescriptorSet(uint setIndex = 0)
        {
            if (PipelineSets.TryGetValue(setIndex, out var set)) return set;
            else throw new ArgumentOutOfRangeException(nameof(setIndex));
        }

        /// <summary>
        /// 构建管线布局
        /// </summary>
        /// <param name="options"></param>
        /// <param name="createOption"></param>
        public void CreatePipelineSetLayout(DescriptorSetLayoutCreateOption[] options, PipelineLayoutCreateOption createOption)
        {
            PipelineLayoutCreateOption option = createOption ?? new PipelineLayoutCreateOption();
            Constants = option.PushConstantRanges;
            if(Constants != null)
            {
                for(int i = 0; i < Constants.Length; i++)
                {
                    ulong end = Constants[i].Offset + Constants[i].Size;
                    if (ConstantRange < end) ConstantRange = end;
                }
            }
            if (options == null)
            {
                option.SetLayouts = new DescriptorSetLayout[0];
                SetLayouts = new DescriptorSetLayout[0];
                DescriptorCreateInfo = new DescriptorSetLayoutCreateOption[0];
                PipelineLayout = Device.CreatePipelineLayout(null, option);
                return;
            }
            SetLayouts = new DescriptorSetLayout[options.Length];
            DescriptorCreateInfo = options;
            for (int i = 0; i < options.Length; i++)
            {
                var opt = options[i];
                if (opt.Bindings == null || opt.Bindings.Length <= 0) throw new ArgumentException("Descriptor bindings must be not null and greater than 0");
                opt.SetIndex = (uint)i;
                if (opt.DescriptorSetTarget == DescriptorSetTarget.Pipeline)
                {
                    lock (SyncRoot)
                    {
                        var item = CreateDescriptorPoolItem(Device, opt, 1);
                        PipelineDescriptorPool.TryAdd((uint)i, item);
                        var layout = Device.CreateDescriptorSetLayout(opt);
                        SetLayouts[i] = layout;
                        var sets = Device.AllocateDescriptorSet(item.DescriptorPool, layout);
                        PipelineSets.TryAdd((uint)i, sets[0]);
                    }
                }
                else
                {
                    lock (SyncRoot)
                    {
                        var item = CreateDescriptorPoolItem(Device, opt, 8);
                        LinkedList<DescriptorPoolLinkedItem> items = new LinkedList<DescriptorPoolLinkedItem>();
                        items.AddLast(item);
                        var layout = Device.CreateDescriptorSetLayout(opt);
                        SetLayouts[i] = layout;
                        ObjectsDescriptorPool.AddOrUpdate(opt.SetIndex, items, (key, value) => items);
                    }
                }
            }
            PipelineLayout = Device.CreatePipelineLayout(SetLayouts, option);
        }
        /// <summary>
        /// 创建描述符集
        /// </summary>
        /// <param name="setIndex"></param>
        /// <returns></returns>
        public DescriptorSet CreateDescriptorSet(uint setIndex)
        {
            if (setIndex >= DescriptorCreateInfo.Length) throw new ArgumentOutOfRangeException(nameof(setIndex));
            var l = DescriptorCreateInfo[setIndex];
            if (l.DescriptorSetTarget == DescriptorSetTarget.Pipeline)
            {
                if (PipelineSets.TryGetValue(setIndex, out DescriptorSet set)) return set;
                else throw new ArgumentOutOfRangeException(nameof(setIndex));
            }
            else
            {
                if (ObjectsDescriptorPool.TryGetValue(setIndex, out LinkedList<DescriptorPoolLinkedItem> list))
                {
                    var last = list.Last.Value;
                    if (last.Used >= last.MaxCount)
                    {
                        lock (SyncRoot)
                        {
                            if (last.Used < last.MaxCount)
                            {
                                var sets = Device.AllocateDescriptorSet(last.DescriptorPool, SetLayouts[setIndex]);
                                last.Used++;
                                return sets[0];
                            }
                            else
                            {
                                DescriptorPoolLinkedItem newItem = CreateDescriptorPoolItem(Device, l, last.MaxCount + last.MaxCount / 2);
                                list.AddLast(newItem);
                                last = newItem;
                                var sets = Device.AllocateDescriptorSet(last.DescriptorPool, SetLayouts[setIndex]);
                                last.Used++;
                                return sets[0];
                            }
                        }
                    }
                    lock (SyncRoot)
                    {
                        var sets = Device.AllocateDescriptorSet(last.DescriptorPool, SetLayouts[setIndex]);
                        last.Used++;
                        return sets[0];
                    }
                }
                else
                {
                    lock (SyncRoot)
                    {
                        var item = CreateDescriptorPoolItem(Device, l, 8);
                        LinkedList<DescriptorPoolLinkedItem> items = new LinkedList<DescriptorPoolLinkedItem>();
                        items.AddLast(item);
                        ObjectsDescriptorPool.AddOrUpdate(setIndex, items, (key, value) => items);
                        var sets = Device.AllocateDescriptorSet(item.DescriptorPool, SetLayouts[setIndex]);
                        item.Used++;
                        return sets[0];
                    }
                }
            }
        }
        /// <summary>
        /// 创建推送常量
        /// </summary>
        /// <returns></returns>
        public ConstantBuffer[] CreateConstantBuffers()
        {
            if (Constants == null || Constants.Length <= 0) return Array.Empty<ConstantBuffer>();
            ConstantBuffer[] buffers = new ConstantBuffer[Constants.Length];
            for(int i = 0; i < buffers.Length; i++)
            {
                var l = Constants[i];
                buffers[i] = new ConstantBuffer(l.Size) { Name = nameof(ConstantBuffer) + i };
            }
            return buffers;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            if (IsDispose) return;
            IsDispose = true;
            Device.DestroyPipelineLayout(PipelineLayout);
            Device.DestroyDescriptorSetLayout(SetLayouts);
            foreach (var item in PipelineDescriptorPool)
            {
                Device.DestroyDescriptorPool(item.Value.DescriptorPool);
            }
            foreach(var item in ObjectsDescriptorPool)
            {
                foreach(var i in item.Value)
                {
                    Device.DestroyDescriptorPool(i.DescriptorPool);
                }
            }
            Device.DestroyShaderModules(ShaderModules);
            unsafe
            {
                NativeAPI.DestroyPipeline(Device.RawHandle, Pipeline, null);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public (bool, string) IsBuildFinish()
        {
            if (PipelineLayout.Handle == 0) return (false, "Please create pipeline layout first");
            if (Pipeline.Handle == 0) return (false, "Please build pipeline first");
            return (true, "");
        }

        /// <summary>
        /// 合并
        /// </summary>
        /// <param name="setIndex"></param>
        /// <param name="dstbinding"></param>
        /// <returns></returns>
        private uint Combine(uint setIndex, uint dstbinding) => setIndex << 16 | dstbinding;
        /// <summary>
        /// 解构
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private (uint, uint) Deconstruction(uint source) => (source >> 16, source & ushort.MaxValue);

        /// <summary>
        /// 创建描述符集池项
        /// </summary>
        /// <param name="device"></param>
        /// <param name="opt"></param>
        /// <param name="itemCount"></param>
        /// <returns></returns>
        private DescriptorPoolLinkedItem CreateDescriptorPoolItem(VulkanDevice device, DescriptorSetLayoutCreateOption opt, uint itemCount)
        {
            DynamicDescriptorPoolFlags flags = DynamicDescriptorPoolFlags.None;
            List<DescriptorPoolSize> poolSizes = new List<DescriptorPoolSize>();
            Dictionary<DescriptorType, uint> books = new Dictionary<DescriptorType, uint>();
            for (int i = 0; i < opt.Bindings.Length; i++)
            {
                var b = opt.Bindings[i];
                switch (b.DescriptorType)
                {
                    case DescriptorType.CombinedImageSampler:
                        flags |= DynamicDescriptorPoolFlags.CombinedImageSampler;
                        break;
                    case DescriptorType.InputAttachment:
                        flags |= DynamicDescriptorPoolFlags.InputAttachment;
                        break;
                    case DescriptorType.SampledImage:
                        flags |= DynamicDescriptorPoolFlags.SampledImage;
                        break;
                    case DescriptorType.Sampler:
                        flags |= DynamicDescriptorPoolFlags.Sampler;
                        break;
                    case DescriptorType.StorageBuffer:
                        flags |= DynamicDescriptorPoolFlags.StorageBuffer;
                        break;
                    case DescriptorType.StorageBufferDynamic:
                        flags |= DynamicDescriptorPoolFlags.StorageImage;
                        break;
                    case DescriptorType.StorageTexelBuffer:
                        flags |= DynamicDescriptorPoolFlags.StorageTexelBuffer;
                        break;
                    case DescriptorType.UniformBuffer:
                        flags |= DynamicDescriptorPoolFlags.UniformBuffer;
                        break;
                    case DescriptorType.UniformBufferDynamic:
                        flags |= DynamicDescriptorPoolFlags.UniformBufferDynamic;
                        break;
                    case DescriptorType.UniformTexelBuffer:
                        flags |= DynamicDescriptorPoolFlags.UniformTexelBuffer;
                        break;
                    default:
                        break;
                }
                if (books.ContainsKey(b.DescriptorType)) books[b.DescriptorType] += 1;
                else books.Add(b.DescriptorType, 1);
            }
            foreach (var book in books)
            {
                DescriptorPoolSize size = new DescriptorPoolSize(book.Key, itemCount * book.Value);
                poolSizes.Add(size);
            }
            var pool = device.CreateDescriptorPool(new DescriptorPoolCreateOption() 
            { 
                MaxSets = itemCount,
                Flags = DescriptorPoolCreateFlags.FreeDescriptorSetBit,
                PoolSizes = poolSizes.ToArray(),
            });
            DescriptorPoolLinkedItem item = new DescriptorPoolLinkedItem
            {
                DescriptorPool = pool,
                MaxCount = itemCount,
                SetIndex = opt.SetIndex,
                Type = flags
            };
            return item;
        }
    }
}
