﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 渲染通道
    /// </summary>
    public class VulkanRenderPass : IVulkanWapper<RenderPass>
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; }
        /// <summary>
        /// 
        /// </summary>
        public RenderPass RawHandle { get; }
        /// <summary>
        /// 设备
        /// </summary>
        public VulkanDevice Device { get; }
        /// <summary>
        /// 渲染通道模式
        /// </summary>
        public RenderPassMode Mode { get; protected internal set; }
        /// <summary>
        /// 帧缓存
        /// </summary>
        public List<VulkanFrameBuffer> Framebuffer { get; protected internal set; } = new List<VulkanFrameBuffer>();
        /// <summary>
        /// 开始配置
        /// </summary>
        public RenderPassBeginOption BeginOption { get; protected internal set; }

        private Vk NativeAPI = Vk.GetApi();
        private volatile bool IsDispose;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="api"></param>
        /// <param name="name"></param>
        /// <param name="renderPass"></param>
        /// <param name="device"></param>
        public VulkanRenderPass(string name, Vk api, RenderPass renderPass, VulkanDevice device)
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));
            RawHandle = renderPass;
            Device = device;
            NativeAPI = api;
            Name = name;
        }

        /// <summary>
        /// 获取帧缓冲
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public VulkanFrameBuffer GetFramebuffer(uint index)
        {
            if (index >= Framebuffer.Count) throw new IndexOutOfRangeException($"Framebuffer's length only {Framebuffer.Count}");
            return Framebuffer[(int)index];
        }

        /// <summary>
        /// 创建帧缓冲
        /// </summary>
        /// <param name="attachments"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="layers"></param>
        /// <returns></returns>
        public VulkanFrameBuffer CreateFrameBuffer(VulkanImageView[] attachments, uint width, uint height, uint layers = 1)
        {
            if (attachments == null) throw new ArgumentNullException(nameof(attachments));
            if (Device.RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            if (RawHandle.Handle == 0) throw new InvalidOperationException("Invalid RenderPass");
            unsafe
            {
                FramebufferCreateInfo info = new FramebufferCreateInfo();
                ImageView[] imageViews = new ImageView[attachments.Length];
                for(int i = 0; i < attachments.Length; i++)
                {
                    imageViews[i] = attachments[i].RawHandle;
                }
                fixed(ImageView* p = imageViews)
                {
                    info.AttachmentCount = (uint)attachments.Length;
                    info.Height = height;
                    info.Width = width;
                    info.Layers = layers;
                    info.PAttachments = p;
                    info.RenderPass = RawHandle;
                    info.SType = StructureType.FramebufferCreateInfo;
                    var r = NativeAPI.CreateFramebuffer(Device.RawHandle, in info, null, out var framebuffer);
                    if (r != Result.Success) throw new VulkanOperationException("CreateFramebuffer", r);
                    VulkanFrameBuffer frameBuffer = new VulkanFrameBuffer(NativeAPI, Device.RawHandle, framebuffer);
                    frameBuffer.Extent = new Extent3D(width, height, layers);
                    frameBuffer.Attachments = attachments;
                    Framebuffer.Add(frameBuffer);
                    return frameBuffer;
                }
            }
        }

        /// <summary>
        /// 配置通道渲染信息
        /// </summary>
        /// <param name="option"></param>
        public void ConfigBeginInfo(Action<RenderPassBeginOption> option)
        {
            if (option == null) throw new ArgumentNullException(nameof(option));
            RenderPassBeginOption beginOption = new RenderPassBeginOption();
            option(beginOption);
            BeginOption = beginOption;
        }

        /// <summary>
        /// 清理帧缓冲
        /// </summary>
        public void ClearFrameBuffer()
        {
            foreach(var item in Framebuffer)
            {
                item.Dispose();
            }
            Framebuffer.Clear();
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            if (IsDispose) return;
            IsDispose = true;
            if(RawHandle.Handle != 0 && Device.RawHandle.Handle != IntPtr.Zero)
            {
                foreach (var item in Framebuffer) item.Dispose();
                unsafe { NativeAPI.DestroyRenderPass(Device.RawHandle, RawHandle, null); }
            }
        }
    }

    /// <summary>
    /// 渲染模式
    /// </summary>
    public enum RenderPassMode
    {
        /// <summary>
        /// 常用
        /// </summary>
        Normal,
        /// <summary>
        /// 交换链
        /// </summary>
        Swapchain,
        /// <summary>
        /// 离屏
        /// </summary>
        OffScreen
    }
}
