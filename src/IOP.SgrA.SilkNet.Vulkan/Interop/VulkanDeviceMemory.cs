﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class VulkanDeviceMemory : IVulkanWapper<DeviceMemory>
    {
        /// <summary>
        /// 
        /// </summary>
        public DeviceMemory RawHandle { get; set; }
        /// <summary>
        /// 设备
        /// </summary>
        public Device Device { get; }
        /// <summary>
        /// 内存属性
        /// </summary>
        public MemoryPropertyFlags PropertyFlags { get; protected internal set; }
        /// <summary>
        /// 内存需求
        /// </summary>
        public MemoryRequirements Requirements { get; protected internal set; }
        /// <summary>
        /// 请求
        /// </summary>
        protected ConcurrentQueue<MemoryRequest> Requests { get; set; } = new ConcurrentQueue<MemoryRequest>();
        /// <summary>
        /// 
        /// </summary>
        protected long Counter = 0;

        private Vk NativeAPI = Vk.GetApi();
        private volatile bool IsDispose;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="api"></param>
        /// <param name="device"></param>
        /// <param name="memory"></param>
        public VulkanDeviceMemory(Vk api, Device device, DeviceMemory memory)
        {
            NativeAPI = api;
            Device = device;
            RawHandle = memory;
        }

        /// <summary>
        /// 内存映射
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public IntPtr Map(ulong offset, ulong size)
        {
            if (RawHandle.Handle == 0 || Device.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Memroy");
            var end = Requirements.Size;
            if ((offset + size) > end) throw new ArgumentOutOfRangeException("Update data is too large");

            ApplyForPermit();
            unsafe
            {
                void* marshalledData = default(void*);
                var result = NativeAPI.MapMemory(Device, RawHandle, offset, size, 0, &marshalledData);
                if (result != Result.Success) throw new VulkanOperationException("MapMemory", result);
                return new IntPtr(marshalledData);
            }
        }
        /// <summary>
        /// 结束内存映射
        /// </summary>
        public void UnMap()
        {
            try
            {
                if (RawHandle.Handle == 0 || Device.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Memroy");
                NativeAPI.UnmapMemory(Device, RawHandle);
            }
            finally
            {
                Requests.TryDequeue(out _);
            }

        }

        /// <summary>
        /// 更新内存数据
        /// </summary>
        /// <param name="source"></param>
        /// <param name="offset"></param>
        /// <exception cref="InvalidOperationException"></exception>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        /// <exception cref="VulkanOperationException"></exception>
        public void UpdateMemory(Span<byte> source, ulong offset)
        {
            if (RawHandle.Handle == 0 || Device.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Memroy");
            var end = Requirements.Size; ulong size = (ulong)source.Length;
            if ((offset + size) > end) throw new ArgumentOutOfRangeException("Update data is too large");

            ApplyForPermit();

            try
            {
                unsafe
                {
                    void* marshalledData = default;
                    var result = NativeAPI.MapMemory(Device, RawHandle, offset, size, 0, &marshalledData);
                    if (result != Result.Success) throw new VulkanOperationException("MapMemory", result);
                    fixed (byte* p = source)
                    {
                        System.Buffer.MemoryCopy(p, marshalledData, size, size);
                    }
                    NativeAPI.UnmapMemory(Device, RawHandle);
                }
            }
            finally
            {
                Requests.TryDequeue(out _);
            }
        }
        /// <summary>
        /// 更新内存数据
        /// </summary>
        /// <param name="source"></param>
        /// <param name="offset"></param>
        /// <param name="size"></param>
        public void UpdateMemory(IntPtr source, ulong size, ulong offset)
        {
            if (RawHandle.Handle == 0 || Device.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Memroy");
            var end = Requirements.Size;
            if ((offset + size) > end) throw new ArgumentOutOfRangeException("Update data is too large");

            ApplyForPermit();

            try
            {
                unsafe
                {
                    void* marshalledData = default;
                    var result = NativeAPI.MapMemory(Device, RawHandle, offset, size, 0, &marshalledData);
                    if (result != Result.Success) throw new VulkanOperationException("MapMemory", result);
                    System.Buffer.MemoryCopy(source.ToPointer(), marshalledData, size, size);
                    NativeAPI.UnmapMemory(Device, RawHandle);
                }
            }
            finally
            {
                Requests.TryDequeue(out _);
            }
        }
        /// <summary>
        /// 将映射数据拷贝至目标内存
        /// </summary>
        /// <param name="target"></param>
        /// <param name="offset"></param>
        /// <param name="size"></param>
        public void CopyTo(Span<byte> target, ulong offset, ulong size)
        {
            if (RawHandle.Handle == 0 || Device.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Memroy");
            var end = Requirements.Size; 
            if ((offset + size) > end || size > (ulong)target.Length) throw new ArgumentOutOfRangeException("Update data is too large");

            ApplyForPermit();

            try
            {
                unsafe
                {
                    void* marshalledData = default;
                    var result = NativeAPI.MapMemory(Device, RawHandle, offset, size, 0, &marshalledData);
                    if (result != Result.Success) throw new VulkanOperationException("MapMemory", result);
                    fixed (byte* p = target)
                    {
                        System.Buffer.MemoryCopy(marshalledData, p, size, size);
                    }
                    NativeAPI.UnmapMemory(Device, RawHandle);
                }
            }
            finally
            {
                Requests.TryDequeue(out _);
            }
        }
        /// <summary>
        /// 将映射数据拷贝至目标内存
        /// </summary>
        /// <param name="target"></param>
        /// <param name="size"></param>
        /// <param name="offset"></param>
        public void CopyTo(IntPtr target, ulong size, ulong offset)
        {
            if (RawHandle.Handle == 0 || Device.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Memroy");
            var end = Requirements.Size;
            if ((offset + size) > end) throw new ArgumentOutOfRangeException("Update data is too large");

            ApplyForPermit();
            try
            {
                unsafe
                {
                    void* marshalledData = default;
                    var result = NativeAPI.MapMemory(Device, RawHandle, offset, size, 0, &marshalledData);
                    if (result != Result.Success) throw new VulkanOperationException("MapMemory", result);
                    System.Buffer.MemoryCopy(marshalledData, target.ToPointer(), size, size);
                    NativeAPI.UnmapMemory(Device, RawHandle);
                }
            }
            finally
            {
                Requests.TryDequeue(out _);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            if (IsDispose) return;
            IsDispose = true;
            unsafe
            {
                if (RawHandle.Handle != 0 || Device.Handle != IntPtr.Zero)
                {
                    NativeAPI.FreeMemory(Device, RawHandle, null);
                }
            }
        }

        /// <summary>
        /// 获取请求许可
        /// </summary>
        /// <returns></returns>
        private MemoryRequest ApplyForPermit()
        {
            MemoryRequest req = new(Environment.CurrentManagedThreadId, 0, DateTime.Now);
            SpinWait wait = new(); long token = Counter;
            while (Interlocked.CompareExchange(ref Counter, Counter + 1, token) != token)
            {
                wait.SpinOnce();
                token = Counter;
            }
            req.Time = DateTime.Now;
            req.Token = token;
            Requests.Enqueue(req);
            while (Requests.TryPeek(out MemoryRequest r))
            {
                if (r.Token != token)
                {
                    wait.SpinOnce();
                    continue;
                }
                else break;
            }
            return req;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public struct MemoryRequest
    {
        /// <summary>
        /// 
        /// </summary>
        public int ThreadId;
        /// <summary>
        /// 
        /// </summary>
        public long Token;
        /// <summary>
        /// 
        /// </summary>
        public DateTime Time;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="threadId"></param>
        /// <param name="token"></param>
        /// <param name="time"></param>
        public MemoryRequest(int threadId, long token, DateTime time)
        {
            ThreadId = threadId;
            Token = token;
            Time = time;
        }
    }
}
