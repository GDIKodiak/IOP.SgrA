﻿using System;
using System.Collections.Generic;
using System.Text;
using Silk.NET.Vulkan;


namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class VulkanImage : IVulkanWapper<Image>
    {
        /// <summary>
        /// 
        /// </summary>
        public Extent3D Extent { get => CreateOption == null ? new Extent3D() : CreateOption.Extent; }
        /// <summary>
        /// 
        /// </summary>
        public Image RawHandle { get; }
        /// <summary>
        /// 设备
        /// </summary>
        public Device Device { get; }
        /// <summary>
        /// 内存块
        /// </summary>
        public VulkanMemoryBlock Block { get; protected internal set; } = null;
        /// <summary>
        /// 设备内存
        /// </summary>
        public VulkanDeviceMemory DeviceMemory { get; protected internal set; } = null;
        /// <summary>
        /// 创建配置
        /// </summary>
        public ImageCreateOption CreateOption { get; protected internal set; } = null;
        /// <summary>
        /// 所在内存偏移
        /// </summary>
        public ulong MemoryOffset { get; protected internal set; }

        private Vk NativeAPI = Vk.GetApi();
        private volatile bool IsDispose;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="api"></param>
        /// <param name="device"></param>
        /// <param name="image"></param>
        public VulkanImage(Vk api, Device device, Image image)
        {
            NativeAPI = api;
            Device = device;
            RawHandle = image;
        }
        /// <summary>
        /// 获取内存需求
        /// </summary>
        public MemoryRequirements GetMemoryRequirements()
        {
            if (Device.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            if (RawHandle.Handle == 0) throw new InvalidOperationException("Invalid Image");
            NativeAPI.GetImageMemoryRequirements(Device, RawHandle, out MemoryRequirements requirements);
            return requirements;
        }
        /// <summary>
        /// 尝试更新内存
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool TryUpdateImageMemory(uint offset, byte[] data)
        {
            if (data.Length <= 0) return true;
            if (CouldUpdateMemory())
            {
                try
                {
                    DeviceMemory.UpdateMemory(data, MemoryOffset + offset);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            return false;
        }
        /// <summary>
        /// 更新内存
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="data"></param>
        public void UpdateImageMemory(uint offset, byte[] data)
        {
            if (data.Length <= 0) return;
            Span<byte> local = data;
            UpdateMemory(offset, local);
        }
        /// <summary>
        /// 更新内存
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="data"></param>
        /// <exception cref="NotSupportedException"></exception>
        public void UpdateMemory(ulong offset, Span<byte> data)
        {
            if (data.Length <= 0) return;
            if (CouldUpdateMemory())
            {
                DeviceMemory.UpdateMemory(data, MemoryOffset + offset);
            }
            else throw new NotSupportedException("Cannot update target memory");
        }
        /// <summary>
        /// 是否允许更新内存
        /// </summary>
        /// <returns></returns>
        public bool CouldUpdateMemory()
        {
            if (DeviceMemory == null) return false;
            var type = DeviceMemory.PropertyFlags;
            if (type.HasFlag(MemoryPropertyFlags.HostCachedBit)) return true;
            else if (type.HasFlag(MemoryPropertyFlags.HostCoherentBit)) return true;
            else if (type.HasFlag(MemoryPropertyFlags.HostVisibleBit)) return true;
            else return false;
        }
        /// <summary>
        /// 绑定图像内存
        /// </summary>
        /// <param name="deviceMemory"></param>
        /// <param name="offset"></param>
        public void BindImageMemory(VulkanDeviceMemory deviceMemory, uint offset)
        {
            if (RawHandle.Handle == 0 || Device.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Image");
            if (deviceMemory.RawHandle.Handle == 0) throw new InvalidOperationException("Invalid Memory");
            var r = NativeAPI.BindImageMemory(Device, RawHandle, deviceMemory.RawHandle, offset);
            if(r != Result.Success) throw new VulkanOperationException("BindImageMemory", r);
            if (DeviceMemory != null) DeviceMemory.Dispose();
            DeviceMemory = deviceMemory;
        }
        /// <summary>
        /// 绑定图像内存
        /// </summary>
        /// <param name="flags"></param>
        public void BindImageMemory(MemoryPropertyFlags flags)
        {
            if (RawHandle.Handle == 0 || Device.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Image");
            if (Block != null) VulkanMemoryManager.Current.Recovery(Block);
            var block = VulkanMemoryManager.Current.Malloc(flags, GetMemoryRequirements());
            Block = block; var seq = block.Owner;
            var r = NativeAPI.BindImageMemory(Device, RawHandle, seq.Memory.RawHandle, block.Offset);
            if (r != Result.Success) throw new VulkanOperationException("BindImageMemory", r);
            DeviceMemory?.Dispose();
            DeviceMemory = seq.Memory;
            MemoryOffset = block.Offset;
        }
        /// <summary>
        /// 创建视图
        /// </summary>
        /// <param name="option"></param>
        /// <returns></returns>
        public VulkanImageView CreateImageView(ImageViewCreateOption option)
        {
            if (option == null) throw new ArgumentNullException(nameof(option));
            if (RawHandle.Handle == 0 || Device.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Image");
            unsafe
            {
                ImageViewCreateInfo info = new ImageViewCreateInfo()
                {
                    SType = StructureType.ImageViewCreateInfo,
                    Image = RawHandle,
                    Components = option.Components,
                    ViewType = option.ViewType,
                    Format = option.Format,
                    SubresourceRange = option.SubresourceRange
                };
                var r = NativeAPI.CreateImageView(Device, in info, null, out ImageView view);
                if (r != Result.Success) throw new VulkanOperationException("CreateImageView", r);
                VulkanImageView imageView = new VulkanImageView(NativeAPI, Device, view, this);
                imageView.CreateOption = option;
                return imageView;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            if (IsDispose) return;
            IsDispose = true;
            if (Block != null)
            {
                VulkanMemoryManager.Current.Recovery(Block);
                DeviceMemory = null;
            }
            else DeviceMemory?.Dispose();
            if (RawHandle.Handle != 0 && Device.Handle != IntPtr.Zero)
            {
                unsafe { NativeAPI.DestroyImage(Device, RawHandle, null); }
            }
        }
    }
}
