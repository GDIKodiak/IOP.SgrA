﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class VulkanBufferView : IVulkanWapper<BufferView>
    {
        /// <summary>
        /// 
        /// </summary>
        public BufferView RawHandle { get; }
        /// <summary>
        /// 
        /// </summary>
        public Device Device { get; }
        /// <summary>
        /// 格式
        /// </summary>
        public Format Format { get; protected internal set; }
        /// <summary>
        /// 偏移
        /// </summary>
        public ulong Offset { get; protected internal set; }
        /// <summary>
        /// 范围
        /// </summary>
        public ulong Range { get; protected internal set; }
        /// <summary>
        /// 缓冲
        /// </summary>
        public VulkanBuffer Buffer { get; protected internal set; }
        /// <summary>
        /// Id
        /// </summary>
        public Guid Guid { get; protected internal set; } = Guid.Empty;

        private Vk NativeAPI = Vk.GetApi();
        private volatile bool IsDispose;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="api"></param>
        /// <param name="device"></param>
        /// <param name="bufferView"></param>
        /// <param name="buffer"></param>
        public VulkanBufferView(Vk api, Device device, BufferView bufferView, VulkanBuffer buffer)
        {
            NativeAPI = api;
            Device = device;
            RawHandle = bufferView;
            Buffer = buffer;
            Guid = buffer.Guid;
        }

        /// <summary>
        /// 尝试更新内存
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool TryUpdateMemory(uint offset, byte[] data) => Buffer.TryUpdateMemory(offset, data);
        /// <summary>
        /// 更新内存
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="data"></param>
        public void UpdateMemory(uint offset, byte[] data) => Buffer?.UpdateMemory(offset, data);
        /// <summary>
        /// 更新内存
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="data"></param>
        public void UpdateMemory(uint offset, Span<byte> data) => Buffer?.UpdateMemory(offset, data);
        /// <summary>
        /// 更新内存
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="ptr"></param>
        /// <param name="size"></param>
        public void UpdateMemory(uint offset, in IntPtr ptr, ulong size) => Buffer?.UpdateMemory(offset, in ptr, size);
        /// <summary>
        /// 是否允许更新内存
        /// </summary>
        /// <returns></returns>
        public bool CouldUpdateMemory() => Buffer.CouldUpdateMemory();

        /// <summary>
        /// 销毁资源
        /// </summary>
        public void Dispose()
        {
            if (IsDispose) return;
            IsDispose = true;
            if (RawHandle.Handle != 0 && Device.Handle != IntPtr.Zero)
            {
                unsafe { NativeAPI.DestroyBufferView(Device, RawHandle, null); }
                Buffer = null;
            }
        }
    }
}
