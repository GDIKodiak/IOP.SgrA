﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// Vulkan包装类接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IVulkanWapper<T> : IDisposable
        where T : struct
    {
        /// <summary>
        /// 原始对象
        /// </summary>
        T RawHandle { get; }
    }
}
