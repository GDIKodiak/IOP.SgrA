﻿using Silk.NET.Core.Native;
using Silk.NET.Vulkan;
using Silk.NET.Vulkan.Extensions.KHR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 交换链
    /// </summary>
    public class VulkanSwapchain : IVulkanWapper<SwapchainKHR>
    {
        /// <summary>
        /// 原始类型
        /// </summary>
        public SwapchainKHR RawHandle { get; protected internal set; }
        /// <summary>
        /// 实例
        /// </summary>
        public Instance Instance { get; }
        /// <summary>
        /// 设备
        /// </summary>
        public Device Device { get; }
        /// <summary>
        /// 交换链支持细节
        /// </summary>
        public SwapChainSupportDetails Details { get; protected internal set; }
        /// <summary>
        /// 窗口曲面
        /// </summary>
        public SurfaceKHR Surface { get; protected internal set; }
        /// <summary>
        /// 队列族下标
        /// </summary>
        public QueueFamilyIndices[] QueueFamilyIndex { get; protected internal set; }
        /// <summary>
        /// 交换链表面格式
        /// </summary>
        public SurfaceFormatKHR SwapChainFormat { get; protected internal set; }
        /// <summary>
        /// 交换链呈现模式
        /// </summary>
        public PresentModeKHR SwapChainPresentMode { get; protected internal set; }
        /// <summary>
        /// 
        /// </summary>
        public SharingMode SharingMode { get; protected internal set; }
        /// <summary>
        /// 交换链交换范围
        /// </summary>
        public Extent2D SwapChainExtent { get; set; }
        /// <summary>
        /// 交换链图像
        /// </summary>
        public VulkanImage[] Images { get; protected internal set; }
        /// <summary>
        /// 交换链图像视图
        /// </summary>
        public VulkanImageView[] Views { get; protected internal set; }

        private Vk NativeAPI = Vk.GetApi();
        private KhrSwapchain SwapchainExtension = null;
        private volatile bool IsDispose;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="api"></param>
        /// <param name="swapchain"></param>
        /// <param name="device"></param>
        /// <param name="instance"></param>
        /// <param name="surface">窗口表面</param>
        public VulkanSwapchain(Vk api, SwapchainKHR swapchain, Device device, Instance instance, SurfaceKHR surface)
        {
            if (instance.Handle == IntPtr.Zero) throw new ArgumentNullException("Invalid Instance");
            if (device.Handle == IntPtr.Zero) throw new ArgumentNullException("Invalid Device");
            RawHandle = swapchain;
            Device = device;
            NativeAPI = api;
            Instance = instance;
            Surface = surface;
            if (api.TryGetDeviceExtension(instance, device, out KhrSwapchain khrSwapchain))
            {
                SwapchainExtension = khrSwapchain;
            }
            else throw new NotSupportedException($"Target Device not suppored extension {KhrSwapchain.ExtensionName}");
        }
        /// <summary>
        /// 获取交换链图像
        /// </summary>
        /// <returns></returns>
        public VulkanImage[] GetSwapchainImages()
        {
            if (Device.Handle == IntPtr.Zero) throw new ArgumentNullException("Invalid Device");
            if (RawHandle.Handle == 0) throw new ArgumentNullException("Invalid Swapchain");
            unsafe
            {
                if (Images == null || Images.Length <= 0)
                {
                    uint length = 0;
                    var r = SwapchainExtension.GetSwapchainImages(Device, RawHandle, ref length, null);
                    if (r != Result.Success) throw new VulkanOperationException("GetSwapchainImages", r);
                    Image[] images = new Image[length];
                    VulkanImage[] vulkanImages = new VulkanImage[length];
                    fixed (Image* p = images)
                    {
                        r = SwapchainExtension.GetSwapchainImages(Device, RawHandle, ref length, p);
                    }
                    if (r != Result.Success) throw new VulkanOperationException("GetSwapchainImages", r);
                    for (int i = 0; i < length; i++)
                    {
                        vulkanImages[i] = new VulkanImage(NativeAPI, Device, images[i]);
                    }
                    Images = vulkanImages;
                    return vulkanImages;
                }
                else return Images;
            }
        }
        /// <summary>
        /// 创建交换链图像视图
        /// </summary>
        /// <returns></returns>
        public VulkanImageView[] CreateSwapchainImageViews(Func<IEnumerable<VulkanImage>, IEnumerable<VulkanImageView>> createFunc)
        {
            if (Views != null) return Views;
            if (Device.Handle == IntPtr.Zero) throw new ArgumentNullException("Invalid Device");
            if (RawHandle.Handle == 0) throw new ArgumentNullException("Invalid Swapchain");
            var images = GetSwapchainImages();
            var views = createFunc(images);
            Views = views.ToArray();
            return Views;
        }
        /// <summary>
        /// 获取交换链下一张图片
        /// </summary>
        /// <param name="timeout"></param>
        /// <param name="semaphore"></param>
        /// <param name="fence"></param>
        /// <returns></returns>
        public uint AcquireNextImage(ulong timeout, Semaphore semaphore, Fence fence)
        {
            if (Device.Handle == IntPtr.Zero) throw new ArgumentNullException("Invalid Device");
            if (RawHandle.Handle == 0) throw new ArgumentNullException("Invalid Swapchain");
            uint pIndex = 0;
            var r = SwapchainExtension.AcquireNextImage(Device, RawHandle, timeout, semaphore, fence, ref pIndex);
            if (r != Result.Success) throw new VulkanOperationException("AcquireNextImage", r);
            return pIndex;
        }
        /// <summary>
        /// 呈现
        /// </summary>
        /// <param name="semaphores"></param>
        /// <param name="imageIndex"></param>
        /// <param name="queue"></param>
        public void Present(Queue queue, Span<Semaphore> semaphores, uint imageIndex)
        {
            if (Device.Handle == IntPtr.Zero) throw new ArgumentNullException("Invalid Device");
            if (RawHandle.Handle == 0) throw new ArgumentNullException("Invalid Swapchain");
            if (queue.Handle == IntPtr.Zero) throw new ArgumentNullException("Invalid Queue");
            PresentInfoKHR info = new PresentInfoKHR();
            unsafe
            {
                SwapchainKHR* sw = stackalloc SwapchainKHR[1] { RawHandle };
                uint* fIndex = stackalloc uint[1] { imageIndex };
                Result result;
                fixed(Semaphore* p = semaphores)
                {
                    info.PImageIndices = fIndex;
                    info.PSwapchains = sw;
                    info.PWaitSemaphores = p;
                    info.SType = StructureType.PresentInfoKhr;
                    info.SwapchainCount = 1;
                    info.WaitSemaphoreCount = semaphores != null ? (uint)semaphores.Length : 0;
                    result = SwapchainExtension.QueuePresent(queue, in info);
                    if (result != Result.Success) throw new VulkanOperationException("QueuePresent", result);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            if (IsDispose) return;
            IsDispose = true;
            if (Device.Handle != IntPtr.Zero)
            {
                unsafe
                {
                    if (Views != null)
                    {
                        foreach (var item in Views)
                        {
                            NativeAPI.DestroyImageView(Device, item.RawHandle, null);
                        }
                        Views = null;
                        Images = null;
                    }
                    SwapchainExtension?.DestroySwapchain(Device, RawHandle, null);
                }
            }
        }
    }
}
