﻿using Silk.NET.Vulkan;
using System;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// Instance
    /// </summary>
    public class VulkanInstance : IVulkanWapper<Instance>
    {
        /// <summary>
        /// 原始对象
        /// </summary>
        public Instance RawHandle { get; }

        private Vk NativeAPI = Vk.GetApi();
        private volatile bool IsDispose = false;
        private VulkanPhysicalDevice[] VulkanPhysicalDevices = null;
        private DebugReportCallback DebugReport = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="api"></param>
        public VulkanInstance(Vk api, Instance instance)
        {
            NativeAPI = api;
            RawHandle = instance;
        }

        /// <summary>
        /// 枚举物理设备
        /// </summary>
        /// <returns></returns>
        public VulkanPhysicalDevice[] EnumeratePhysicalDevices()
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new NullReferenceException("Invaild Instance");
            if (VulkanPhysicalDevices != null) return VulkanPhysicalDevices;
            unsafe
            {
                uint length = 0;
                var r = NativeAPI.EnumeratePhysicalDevices(RawHandle, ref length, null);
                if (r != Result.Success) throw new VulkanOperationException("EnumeratePhysicalDevices", r);
                PhysicalDevice* physicals = stackalloc PhysicalDevice[(int)length];
                r = NativeAPI.EnumeratePhysicalDevices(RawHandle, ref length, physicals);
                if (r == Result.Success)
                {
                    VulkanPhysicalDevice[] result = new VulkanPhysicalDevice[length];
                    for (int i = 0; i < length; i++)
                    {
                        result[i] = new VulkanPhysicalDevice(NativeAPI, RawHandle, *(physicals + i));
                    }
                    VulkanPhysicalDevices = result;
                    return result;
                }
                throw new VulkanOperationException("EnumeratePhysicalDevices", r);
            }
        }

        /// <summary>
        /// 创建调试报告
        /// </summary>
        /// <param name="callback"></param>
        /// <param name="flags"></param>
        /// <returns></returns>
        public DebugReportCallback CreateDebugReportCallback(DebugReportFunction callback, DebugReportFlagsEXT flags = DebugReportFlagsEXT.ErrorBitExt | DebugReportFlagsEXT.WarningBitExt)
        {
            if (callback == null) throw new ArgumentNullException(nameof(callback));
            if (RawHandle.Handle == IntPtr.Zero) throw new NullReferenceException("Invaild Instance");
            DebugReportCallback reportCallback = new DebugReportCallback(NativeAPI, RawHandle, flags, callback);
            DebugReport = reportCallback;
            return reportCallback;
        }

        /// <summary>
        /// 销毁
        /// </summary>
        public void Dispose()
        {
            if (IsDispose) return;
            IsDispose = true;
            if (DebugReport != null) DebugReport.Dispose();
            if (RawHandle.Handle != IntPtr.Zero)
            {
                unsafe { NativeAPI.DestroyInstance(RawHandle, null); }
            }
        }
    }
}
