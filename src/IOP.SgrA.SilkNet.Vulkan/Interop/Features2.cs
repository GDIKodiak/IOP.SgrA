﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public unsafe abstract class Features2
    {
        /// <summary>
        /// 
        /// </summary>
        internal Features2 Previous = null;
        /// <summary>
        /// 
        /// </summary>
        internal Features2 Next = null;
        /// <summary>
        /// 在链表末尾添加一项功能，并返回该功能
        /// 如果该功能存在则无视
        /// </summary>
        /// <param name="next"></param>
        /// <returns></returns>
        public abstract Features2 SetNext(Features2 next);
        /// <summary>
        /// 获取当前链表根
        /// </summary>
        /// <returns></returns>
        public abstract Features2 GetRoot();
        /// <summary>
        /// 获取当前功能链表指针
        /// </summary>
        /// <returns></returns>
        public abstract void* GetPointer();
        /// <summary>
        /// 销毁用于构建链表的内存
        /// </summary>
        public abstract void Free();
        /// <summary>
        /// 获取项名称
        /// </summary>
        /// <returns></returns>
        public abstract string GetFeaturesName();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TChain"></typeparam>
    public unsafe class Features2<TChain> : Features2
        where TChain : unmanaged, IExtendsChain<PhysicalDeviceFeatures2>
    {
        private TChain Local;
        private int Size;
        private unsafe byte* Memory = null;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="chain"></param>
        public Features2(TChain chain)
        {
            Local = chain;
            Size = sizeof(TChain);
            CreateMemory(Size);
            CopyToCache(Local);
        }
        /// <summary>
        /// 获取项名称
        /// </summary>
        /// <returns></returns>
        public override string GetFeaturesName() => typeof(TChain).Name;
        /// <summary>
        /// 添加项
        /// </summary>
        /// <param name="next"></param>
        /// <returns></returns>
        public override Features2 SetNext(Features2 next)
        {
            if (next == null) return this;
            if (next == Next) return next;
            if (PreviousCheck(next)) return this;
            Next?.Free();
            Next = next; next.Previous = this;
            void* pNext = Next.GetPointer();
            Local.PNext = (BaseInStructure*)pNext;
            CopyToCache(Local);
            return Next;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override Features2 GetRoot()
        {
            if(Previous == null) return this;
            Features2 temp = this;
            while(temp.Previous != null)
            {
                temp = temp.Previous;
            }
            return temp;
        }
        /// <summary>
        /// 获取当前链结构体指针
        /// </summary>
        /// <returns></returns>
        public override unsafe void* GetPointer() => Memory;
        /// <summary>
        /// 释放内存
        /// </summary>
        public override void Free()
        {
            if (Memory != null) NativeMemory.Free(Memory);
            Next?.Free();
        }
        /// <summary>
        /// 创建缓冲内存
        /// </summary>
        /// <param name="size"></param>
        private void CreateMemory(int size)
        {
            if(size <= 0) return;
            Free();
            void* ptr = NativeMemory.Alloc((nuint)size);
            Memory = (byte*)ptr;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="chain"></param>
        private void CopyToCache(TChain chain)
        {
            if (Memory == null) CreateMemory(Size);
            if (Memory == null) return;
            Span<TChain> temp = stackalloc TChain[] { chain };
            Span<byte> m = MemoryMarshal.AsBytes(temp);
            Span<byte> cache = new(Memory, Size);
            m.CopyTo(cache);
        }
        private bool PreviousCheck(Features2 target)
        {
            if (Previous == null) return false;
            if (this == target) return true;
            Features2 temp = this;
            while (temp.Previous != null)
            {
                temp = temp.Previous;
                if (temp == target) return true;
            }
            return false;
        }
    }
}
