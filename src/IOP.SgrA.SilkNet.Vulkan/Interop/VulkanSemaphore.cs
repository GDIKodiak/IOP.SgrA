﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class VulkanSemaphore : IVulkanWapper<Semaphore>
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; protected internal set; }
        /// <summary>
        /// 设备
        /// </summary>
        public Device Device { get; }
        /// <summary>
        /// 
        /// </summary>
        public Semaphore RawHandle { get; }

        private Vk NativeAPI = Vk.GetApi();
        private volatile bool IsDispose;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="api"></param>
        /// <param name="device"></param>
        /// <param name="semaphore"></param>
        /// <param name="name"></param>
        public VulkanSemaphore(Vk api, Device device, Semaphore semaphore, string name = null)
        {
            if (string.IsNullOrEmpty(name)) name = Guid.NewGuid().ToString("N");
            NativeAPI = api;
            Name = name;
            Device = device;
            RawHandle = semaphore;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            if (IsDispose) return;
            IsDispose = true;
            if(Device.Handle != IntPtr.Zero && RawHandle.Handle != 0)
            {
                unsafe
                {
                    NativeAPI.DestroySemaphore(Device, RawHandle, null);
                }
            }
        }
    }
}
