﻿using Silk.NET.Vulkan;
using Silk.NET.Vulkan.Extensions.KHR;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 帧缓冲
    /// </summary>
    public class VulkanFrameBuffer : IVulkanWapper<Framebuffer>
    {
        /// <summary>
        /// 
        /// </summary>
        public Framebuffer RawHandle { get; set; }
        /// <summary>
        /// 设备
        /// </summary>
        public Device Device { get; set; }
        /// <summary>
        /// 布局
        /// </summary>
        public Extent3D Extent { get; protected internal set; }
        /// <summary>
        /// 
        /// </summary>
        public VulkanImageView[] Attachments { get; protected internal set; }

        private Vk NativeAPI = Vk.GetApi();
        private volatile bool IsDispose;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="api"></param>
        /// <param name="device"></param>
        /// <param name="framebuffer"></param>
        public VulkanFrameBuffer(Vk api, Device device, Framebuffer framebuffer)
        {
            NativeAPI = api;
            Device = device;
            RawHandle = framebuffer;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            if (IsDispose) return;
            IsDispose = false;
            if(RawHandle.Handle != 0 && Device.Handle != IntPtr.Zero)
            {
                unsafe { NativeAPI.DestroyFramebuffer(Device, RawHandle, null); }
            }
        }
    }
}
