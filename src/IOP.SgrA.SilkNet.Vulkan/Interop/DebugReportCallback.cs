﻿using System;
using System.Collections.Generic;
using System.Text;
using Silk.NET.Core.Native;
using Silk.NET.Vulkan;
using Silk.NET.Vulkan.Extensions.EXT;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class DebugReportCallback : IVulkanWapper<DebugReportCallbackEXT>
    {
        /// <summary>
        /// 
        /// </summary>
        public DebugReportCallbackEXT RawHandle { get; private set; }

        private Instance Instance { get; }
        private volatile bool IsDispose = false;
        private Vk NativeAPI = Vk.GetApi();
        private ExtDebugReport Extension;
        private DebugReportFunction ExternalFunc;
        private DebugReportCallbackFunctionEXT InternalFunc;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="api"></param>
        /// <param name="instance"></param>
        /// <param name="flags"></param>
        /// <param name="func"></param>
        public DebugReportCallback(Vk api, Instance instance, DebugReportFlagsEXT flags, DebugReportFunction func)
        {
            NativeAPI = api;
            Instance = instance;
            api.TryGetInstanceExtension(instance, out ExtDebugReport report);
            Extension = report;
            ExternalFunc = func;
            Active(flags, func);
        }
        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            if (IsDispose) return;
            IsDispose = true;
            if(Instance.Handle != IntPtr.Zero && RawHandle.Handle != 0)
            {
                unsafe 
                {
                    Extension.DestroyDebugReportCallback(Instance, RawHandle, null); 
                }
            }
        }

        /// <summary>
        /// 激活委托
        /// </summary>
        /// <param name="flags"></param>
        /// <param name="func"></param>
        private void Active(DebugReportFlagsEXT flags, DebugReportFunction func)
        {
            if (Instance.Handle != IntPtr.Zero)
            {
                Result r = Result.Success;
                if (func != null)
                {
                    unsafe
                    {
                        InternalFunc = InternalCallBack;
                        DebugReportCallbackCreateInfoEXT ext = new DebugReportCallbackCreateInfoEXT()
                        {
                            Flags = flags,
                            PfnCallback = InternalFunc,
                            SType = StructureType.DebugReportCreateInfoExt,
                        };
                        DebugReportCallbackEXT rExt = new DebugReportCallbackEXT();
                        r = Extension.CreateDebugReportCallback(Instance, in ext, null, out rExt);
                        if (r != Result.Success) throw new VulkanOperationException("CreateDebugReportCallback", r);
                        RawHandle = rExt;
                    }
                }
            }
        }

        /// <summary>
        /// 内部委托
        /// </summary>
        /// <param name="flags"></param>
        /// <param name="objectType"></param>
        /// <param name="object"></param>
        /// <param name="location"></param>
        /// <param name="messageCode"></param>
        /// <param name="pLayerPrefix"></param>
        /// <param name="pMessage"></param>
        /// <param name="pUserData"></param>
        /// <returns></returns>
        private unsafe uint InternalCallBack(uint flags, DebugReportObjectTypeEXT objectType, ulong @object, UIntPtr location, int messageCode, byte* pLayerPrefix, byte* pMessage, void* pUserData)
        {
            if (ExternalFunc == null) return 0;
            else
            {
                unsafe
                {
                    try
                    {
                        string pL = new string((sbyte*)pLayerPrefix);
                        string pMsg = new string((sbyte*)pMessage);
                        IntPtr u = new IntPtr(pUserData);
                        return ExternalFunc((DebugReportFlagsEXT)flags, objectType, @object, location, messageCode, pL, pMsg, u);
                    }
                    catch
                    {
                        return int.MaxValue;
                    }
                }
            } 
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="flags"></param>
    /// <param name="objType"></param>
    /// <param name="obj"></param>
    /// <param name="location"></param>
    /// <param name="msgCode"></param>
    /// <param name="layer"></param>
    /// <param name="msg"></param>
    /// <param name="uData"></param>
    /// <returns></returns>
    public delegate uint DebugReportFunction(DebugReportFlagsEXT flags, DebugReportObjectTypeEXT objType, ulong obj, UIntPtr location, int msgCode, string layer, string msg, IntPtr uData);
}
