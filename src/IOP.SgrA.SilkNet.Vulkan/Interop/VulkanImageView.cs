﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class VulkanImageView : IVulkanWapper<ImageView>
    {
        /// <summary>
        /// 原始对象
        /// </summary>
        public ImageView RawHandle { get; }
        /// <summary>
        /// 设备
        /// </summary>
        public Device Device { get; }
        /// <summary>
        /// 创建配置
        /// </summary>
        public ImageViewCreateOption CreateOption { get; protected internal set; } = null;
        /// <summary>
        /// 格式
        /// </summary>
        public Format Format { get => CreateOption != null ? CreateOption.Format : 0; }
        /// <summary>
        /// vulkan图像
        /// </summary>
        public VulkanImage VulkanImage { get; protected internal set; } = null;
        /// <summary>
        /// Id
        /// </summary>
        public Guid Guid { get; protected internal set; } = Guid.Empty;

        private Vk NativeAPI = Vk.GetApi();
        private volatile bool IsDispose;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="api"></param>
        /// <param name="device"></param>
        /// <param name="image"></param>
        /// <param name="imageView"></param>
        public VulkanImageView(Vk api, Device device, ImageView imageView, VulkanImage image)
        {
            NativeAPI = api;
            Device = device;
            RawHandle = imageView;
            VulkanImage = image ?? throw new ArgumentNullException(nameof(image));
        }
        /// <summary>
        /// 尝试更新图片内存
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool TryUpdateImageMemory(uint offset, byte[] data) => VulkanImage.TryUpdateImageMemory(offset, data);
        /// <summary>
        /// 更新图像内存
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="data"></param>
        public void UpdateImageMemory(uint offset, byte[] data) => VulkanImage.UpdateImageMemory(offset, data);
        /// <summary>
        /// 更新图像内存
        /// </summary>
        /// <param name="memory"></param>
        /// <param name="offset"></param>
        public void UpdateImageMemory(VulkanDeviceMemory memory, uint offset) => VulkanImage.BindImageMemory(memory, offset);

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            if (IsDispose) return;
            IsDispose = true;
            if (RawHandle.Handle != 0 && Device.Handle != IntPtr.Zero)
            {
                unsafe { NativeAPI.DestroyImageView(Device, RawHandle, null); }
                VulkanImage.Dispose();
            }
        }
    }
}
