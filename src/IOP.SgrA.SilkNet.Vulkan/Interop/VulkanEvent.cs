﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// Vulkan事件
    /// </summary>
    public class VulkanEvent : IVulkanWapper<Event>
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; protected internal set; }
        /// <summary>
        /// 设备
        /// </summary>
        public Device Device { get; }
        /// <summary>
        /// 
        /// </summary>
        public Event RawHandle { get; }

        private Vk NativeAPI = Vk.GetApi();
        private volatile bool IsDispose;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="api"></param>
        /// <param name="device"></param>
        /// <param name="name"></param>
        /// <param name="event"></param>
        public VulkanEvent(Vk api, Device device, Event @event, string name = null)
        {
            if (string.IsNullOrEmpty(name)) name = Guid.NewGuid().ToString("N");
            NativeAPI = api;
            Name = name;
            Device = device;
            RawHandle = @event;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            if (IsDispose) return;
            IsDispose = true;
            if (Device.Handle != IntPtr.Zero && RawHandle.Handle != 0)
            {
                unsafe
                {
                    NativeAPI.DestroyEvent(Device, RawHandle, null);
                }
            }
        }
    }
}
