﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 命令池
    /// </summary>
    public class VulkanCommandPool : IVulkanWapper<CommandPool>
    {
        /// <summary>
        /// 
        /// </summary>
        public CommandPool RawHandle { get; }
        /// <summary>
        /// 
        /// </summary>
        public VulkanDevice DeviceWapper { get; }
        /// <summary>
        /// 
        /// </summary>
        public Device Device { get; }
        /// <summary>
        /// 队列族下标
        /// </summary>
        public uint QueueFamilyIndex { get; protected internal set; }

        private Vk NativeAPI = Vk.GetApi();
        private volatile bool IsDispose;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="api"></param>
        /// <param name="commandPool"></param>
        /// <param name="vulkanDevice"></param>
        /// <param name="device"></param>
        public VulkanCommandPool(Vk api, CommandPool commandPool, VulkanDevice vulkanDevice, Device device)
        {
            RawHandle = commandPool;
            DeviceWapper = vulkanDevice ?? throw new ArgumentNullException(nameof(VulkanDevice));
            Device = device;
            NativeAPI = api;
        }

        /// <summary>
        /// 创建多个命令缓冲
        /// </summary>
        /// <param name="level"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public VulkanCommandBuffer[] CreateCommandBuffers(CommandBufferLevel level, uint count)
        {
            if (RawHandle.Handle == 0) throw new InvalidOperationException("Invalid CommandPool");
            if (Device.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Device");
            if (count < 1) count = 1;
            unsafe
            {
                CommandBufferAllocateInfo info = new CommandBufferAllocateInfo(StructureType.CommandBufferAllocateInfo,
                    null, RawHandle, level, count);
                CommandBuffer[] buffers = new CommandBuffer[count];
                fixed (CommandBuffer* p = buffers)
                {
                    var result = NativeAPI.AllocateCommandBuffers(Device, in info, p);
                    if (result != Result.Success) throw new VulkanOperationException("AllocateCommandBuffers", result);
                }
                VulkanCommandBuffer[] vulkanCommands = new VulkanCommandBuffer[count];
                for(int i = 0; i < count; i++)
                {
                    vulkanCommands[i] = new VulkanCommandBuffer(NativeAPI, DeviceWapper, Device, buffers[i], this);
                    vulkanCommands[i].BufferLevel = level;
                }
                return vulkanCommands;
            }
        }
        /// <summary>
        /// 创建单个命令缓冲
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public VulkanCommandBuffer CreateCommandBuffer(CommandBufferLevel level)
        {
            var buffers = CreateCommandBuffers(level, 1);
            return buffers[0];
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            if (IsDispose) return;
            IsDispose = true;
            if (RawHandle.Handle == 0 || Device.Handle == IntPtr.Zero) return;
            unsafe { NativeAPI.DestroyCommandPool(Device, RawHandle, null); }
        }
    }
}
