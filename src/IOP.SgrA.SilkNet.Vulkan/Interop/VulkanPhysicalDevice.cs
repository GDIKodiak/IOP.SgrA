﻿using Silk.NET.Vulkan;
using Silk.NET.GLFW;
using System;
using System.Collections.Generic;
using System.Text;
using Silk.NET.Core.Native;
using Silk.NET.Vulkan.Extensions.KHR;
using Silk.NET.Core;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 物理设备
    /// </summary>
    public class VulkanPhysicalDevice : IVulkanWapper<PhysicalDevice>
    {
        /// <summary>
        /// 
        /// </summary>
        public PhysicalDevice RawHandle { get; }
        /// <summary>
        /// 
        /// </summary>
        public Instance Instance { get; }

        /// <summary>
        /// 
        /// </summary>
        private QueueFamilyProperties[] QueueFamilyProperties { get; set; } = null;
        private Vk NaitveAPI = Vk.GetApi();
        private KhrSurface SurfaceExtension = null;
        private PhysicalDeviceMemoryProperties? MemoryProperties;
        private PhysicalDeviceProperties? PhysicalDeviceProperties;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="device"></param>
        /// <param name="api"></param>
        /// <param name="instance"></param>
        public VulkanPhysicalDevice(Vk api, Instance instance, PhysicalDevice device)
        {
            if (instance.Handle == IntPtr.Zero) throw new ArgumentNullException("Invaild Instance");
            RawHandle = device;
            Instance = instance;
            NaitveAPI = api;
            if(api.TryGetInstanceExtension(Instance, out KhrSurface khr))
            {
                SurfaceExtension = khr;
            }
        }
        /// <summary>
        /// 是否支持设备扩展
        /// </summary>
        /// <param name="extension"></param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        public bool IsSupportDeviceExtension(string extension)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid PhysicalDevice");
            if (string.IsNullOrEmpty(extension)) return false;
            return NaitveAPI.IsDeviceExtensionPresent(Instance, extension);
        }
        /// <summary>
        /// 获取物理设备功能
        /// </summary>
        /// <returns></returns>
        public PhysicalDeviceFeatures GetFeatures()
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid PhysicalDevice");
            return NaitveAPI.GetPhysicalDeviceFeature(RawHandle);
        }
        /// <summary>
        /// 获取物理设备功能2
        /// </summary>
        /// <param name="fP2"></param>
        /// <returns></returns>
        public void GetFeatures2(out PhysicalDeviceFeatures2 fP2)
        {
            NaitveAPI.GetPhysicalDeviceFeatures2(RawHandle, out fP2);
        }
        /// <summary>
        /// 获取队列族属性
        /// </summary>
        /// <returns></returns>
        public QueueFamilyProperties[] GetQueueFamilyProperties()
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid PhysicalDevice");
            if (QueueFamilyProperties != null) return QueueFamilyProperties;
            else
            {
                unsafe
                {
                    uint length = 0;
                    NaitveAPI.GetPhysicalDeviceQueueFamilyProperties(RawHandle, ref length, null);
                    QueueFamilyProperties[] prop = new QueueFamilyProperties[length];
                    fixed (QueueFamilyProperties* p = prop)
                    {
                        NaitveAPI.GetPhysicalDeviceQueueFamilyProperties(RawHandle, ref length, p);
                        QueueFamilyProperties = prop;
                    }
                    return prop;
                }
            }
        }
        /// <summary>
        /// 获取物理设备内存属性
        /// </summary>
        /// <returns></returns>
        public PhysicalDeviceMemoryProperties GetMemoryProperties()
        {
            if (MemoryProperties.HasValue) return MemoryProperties.Value;
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid PhysicalDevice");
            NaitveAPI.GetPhysicalDeviceMemoryProperties(RawHandle, out PhysicalDeviceMemoryProperties properties);
            MemoryProperties = properties;
            return properties;
        }
        /// <summary>
        /// 获取物理设备属性
        /// </summary>
        /// <returns></returns>
        public PhysicalDeviceProperties GetDeviceProperties()
        {
            if(PhysicalDeviceProperties.HasValue) return PhysicalDeviceProperties.Value;
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid PhysicalDevice");
            NaitveAPI.GetPhysicalDeviceProperties(RawHandle, out PhysicalDeviceProperties properties);
            PhysicalDeviceProperties = properties;
            return properties;
        }
        /// <summary>
        /// 获取格式属性
        /// </summary>
        /// <param name="format"></param>
        /// <returns></returns>
        public FormatProperties GetFormatProperties(Format format)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid PhysicalDevice");
            NaitveAPI.GetPhysicalDeviceFormatProperties(RawHandle, format, out FormatProperties formatProperties);
            return formatProperties;
        }
        /// <summary>
        /// 获取格式属性2
        /// </summary>
        /// <param name="format"></param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        public FormatProperties2 GetFormatProperties2(Format format)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid PhysicalDevice");
            NaitveAPI.GetPhysicalDeviceFormatProperties2(RawHandle, format, out FormatProperties2 formatProperties2);
            return formatProperties2;
        }
        /// <summary>
        /// 获取物理设备设备族是否支持呈现
        /// </summary>
        /// <param name="family"></param>
        /// <returns></returns>
        public bool GetPhysicalDevicePresentationSupport(int family)
        {
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid PhysicalDevice");
            if (Instance.Handle == IntPtr.Zero) throw new InvalidOperationException("Invalid Instance");
            var glfw = Silk.NET.GLFW.Glfw.GetApi();
            return glfw.GetPhysicalDevicePresentationSupport(Instance.ToHandle(), new VkHandle(RawHandle.Handle), family);
        }
        /// <summary>
        /// 查询交换链支持细节
        /// </summary>
        /// <param name="surface"></param>
        /// <returns></returns>
        public SwapChainSupportDetails QuerySwapChainSupport(SurfaceKHR surface)
        {
            if (SurfaceExtension == null) throw new NotSupportedException("Target Instance not supported KhrSurface extensions");
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invaid PhysicalDevice");
            SwapChainSupportDetails r = new SwapChainSupportDetails();
            unsafe
            {
                uint length = 0;
                var result = SurfaceExtension.GetPhysicalDeviceSurfaceCapabilities(RawHandle, surface, out SurfaceCapabilitiesKHR surfaceCapabilities);
                if (result != Result.Success) throw new VulkanOperationException("GetPhysicalDeviceSurfaceCapabilities", result);
                result = SurfaceExtension.GetPhysicalDeviceSurfaceFormats(RawHandle, surface, ref length, null);
                if (result != Result.Success) throw new VulkanOperationException("GetPhysicalDeviceSurfaceFormats", result);
                SurfaceFormatKHR[] surfaceFormats = new SurfaceFormatKHR[length];
                fixed (SurfaceFormatKHR* p = surfaceFormats) { result = SurfaceExtension.GetPhysicalDeviceSurfaceFormats(RawHandle, surface, ref length, p); }
                if (result != Result.Success) throw new VulkanOperationException("GetPhysicalDeviceSurfaceFormats", result);
                result = SurfaceExtension.GetPhysicalDeviceSurfacePresentModes(RawHandle, surface, ref length, null);
                if (result != Result.Success) throw new VulkanOperationException("GetPhysicalDeviceSurfacePresentModes", result);
                PresentModeKHR[] presentModes = new PresentModeKHR[length];
                fixed (PresentModeKHR* p = presentModes) { result = SurfaceExtension.GetPhysicalDeviceSurfacePresentModes(RawHandle, surface, ref length, p); }
                if (result != Result.Success) throw new VulkanOperationException("GetPhysicalDeviceSurfacePresentModes", result);
                r.Capabilities = surfaceCapabilities;
                r.PresentModes = presentModes;
                r.Formats = surfaceFormats;
                return r;
            }
        }
        /// <summary>
        /// 此队列族是否支持表面
        /// </summary>
        /// <param name="familyIndex"></param>
        /// <param name="surface"></param>
        /// <returns></returns>
        public bool GetSurfaceSupport(uint familyIndex, SurfaceKHR surface)
        {
            if (SurfaceExtension == null) throw new NotSupportedException("Target Instance not supported KhrSurface extensions");
            if (RawHandle.Handle == IntPtr.Zero) throw new InvalidOperationException("Invaid PhysicalDevice");
            unsafe
            {
                var r = SurfaceExtension.GetPhysicalDeviceSurfaceSupport(RawHandle, familyIndex, surface, out Bool32 s);
                if (r != Result.Success) return false;
                else return s;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
        }
    }
}
