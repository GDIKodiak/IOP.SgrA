﻿using IOP.SgrA.ECS;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Collections.Concurrent;
using System.Text;
using System.Threading;
using System.Runtime.CompilerServices;
using System.Linq;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public class VulkanContextManager : IContextManager
    {
        /// <summary>
        /// 上下文列表
        /// </summary>
        private readonly List<ContextWapper> _ContextList = new List<ContextWapper>();
        /// <summary>
        /// 实体管理器
        /// </summary>
        private readonly IEntityManager _EntityManager = new EntityManager();
        /// <summary>
        /// 更新队列
        /// </summary>
        private readonly ConcurrentQueue<Action<VulkanContextManager>> _UpdateQueue = new ConcurrentQueue<Action<VulkanContextManager>>();

        private int _Index = 1;
        private bool _IsDispose;

        /// <summary>
        /// 添加组件程序集
        /// </summary>
        /// <param name="assembly"></param>
        public void AddComponentAssembly(Assembly assembly) => _EntityManager.AddComponentAssembly(assembly);
        /// <summary>
        /// 加载程序集
        /// </summary>
        public void LoadComponents() => _EntityManager.LoadComponents();

        /// <summary>
        /// 获取一个渲染目标原型
        /// </summary>
        /// <returns></returns>
        public Archetype RenderTargetArchetype() => _EntityManager.RenderTargetArchetype();
        /// <summary>
        /// 获取一个无主的原型
        /// </summary>
        /// <returns></returns>
        public Archetype UnownedArchetype() => _EntityManager.UnownedArchetype();
        /// <summary>
        /// 创建原型
        /// </summary>
        /// <param name="components"></param>
        /// <returns></returns>
        public Archetype CreateArchetype(params IComponentData[] components) => _EntityManager.CreateArchetype(components);
        /// <summary>
        /// 创建原型
        /// </summary>
        /// <param name="name"></param>
        /// <param name="components"></param>
        /// <returns></returns>
        public Archetype CreateArchetype(string name, params IComponentData[] components) => _EntityManager.CreateArchetype(name, components);
        /// <summary>
        /// 从指定结构体创建原型
        /// </summary>
        /// <typeparam name="TComponentGorup"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public Archetype CreateArchetype<TComponentGorup>(string name = "") where TComponentGorup : struct => _EntityManager.CreateArchetype<TComponentGorup>(name);
        /// <summary>
        /// 尝试获取原型
        /// </summary>
        /// <param name="name"></param>
        /// <param name="archetype"></param>
        /// <returns></returns>
        public bool TryGetArchetype(string name, out Archetype archetype) => _EntityManager.TryGetArchetype(name, out archetype);

        /// <summary>
        /// 创建上下文
        /// </summary>
        /// <param name="size"></param>
        /// <param name="archetype"></param>
        /// <param name="renderGroup"></param>
        /// <param name="renderObject"></param>
        /// <param name="deepCopy"></param>
        /// <returns></returns>
        public void CreateContexts(int size, Archetype archetype, IRenderGroup renderGroup, IRenderObject renderObject, bool deepCopy = false)
        {
            _UpdateQueue.Enqueue((manager) => manager.CreateContextsSync(size, archetype, renderGroup, renderObject, deepCopy));
        }
        /// <summary>
        /// 创建上下文
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="size"></param>
        /// <param name="archetype"></param>
        /// <param name="renderGroup"></param>
        /// <param name="renderObject"></param>
        /// <param name="deepCopy"></param>
        public void CreateContexts(Context parent, int size, Archetype archetype, IRenderGroup renderGroup, IRenderObject renderObject, bool deepCopy = false)
        {
            _UpdateQueue.Enqueue((manager) => manager.CreateContextsSync(parent, size, archetype, renderGroup, renderObject, deepCopy));
        }
        /// <summary>
        /// 创建上下文
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="size"></param>
        /// <param name="archetype"></param>
        /// <param name="renderGroup"></param>
        /// <param name="renderObject"></param>
        /// <param name="initData"></param>
        /// <param name="deepCopy"></param>
        public void CreateContexts<TGroup>(int size, Archetype archetype, IRenderGroup renderGroup, IRenderObject renderObject, TGroup initData, bool deepCopy = false)
            where TGroup : struct
        {
            _UpdateQueue.Enqueue((manager) => manager.CreateContextsSync(size, archetype, renderGroup, renderObject, initData, deepCopy));
        }
        /// <summary>
        /// 创建上下文
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="parent"></param>
        /// <param name="size"></param>
        /// <param name="archetype"></param>
        /// <param name="renderGroup"></param>
        /// <param name="renderObject"></param>
        /// <param name="initData"></param>
        /// <param name="deepCopy"></param>
        public void CreateContexts<TGroup>(Context parent, int size, Archetype archetype, IRenderGroup renderGroup, IRenderObject renderObject, TGroup initData, bool deepCopy = false)
            where TGroup : struct
        {
            _UpdateQueue.Enqueue((manager) => manager.CreateContextsSync(parent, size, archetype, renderGroup, renderObject, initData, deepCopy));
        }
        /// <summary>
        /// 创建上下文
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="size"></param>
        /// <param name="archetype"></param>
        /// <param name="renderGroup"></param>
        /// <param name="renderObject"></param>
        /// <param name="offset"></param>
        /// <param name="deepCopy"></param>
        public void CreateContexts(Context parent, int size, Archetype archetype, IRenderGroup renderGroup, IRenderObject renderObject, Transform offset, bool deepCopy = false)
        {
            _UpdateQueue.Enqueue((manager) => manager.CreateContextsSync(parent, size, archetype, renderGroup, renderObject, offset, deepCopy));
        }
        /// <summary>
        /// 创建上下文
        /// </summary>
        /// <param name="size"></param>
        /// <param name="archetype"></param>
        /// <param name="renderGroup"></param>
        /// <param name="renderObject"></param>
        /// <param name="transform"></param>
        /// <param name="deepCopy"></param>
        public void CreateContexts(int size, Archetype archetype, IRenderGroup renderGroup, IRenderObject renderObject, Transform transform, bool deepCopy = false)
        {
            _UpdateQueue.Enqueue((manager) => manager.CreateContextsSync(size, archetype, renderGroup, renderObject, transform, deepCopy));
        }

        /// <summary>
        /// 移除上下文
        /// </summary>
        /// <param name="index"></param>
        public void RemoveContext(int index)
        {
            _UpdateQueue.Enqueue((manager) => manager.RemoveContextSync(index));
        }
        /// <summary>
        /// 移除上下文
        /// </summary>
        /// <param name="entity"></param>
        public void RemoveContext(Entity entity)
        {
            _UpdateQueue.Enqueue((manager) => manager.RemoveContextSync(entity.Index));
        }
        /// <summary>
        /// 移除上下文
        /// </summary>
        /// <param name="indexes"></param>
        public void RemoveContexts(int[] indexes)
        {
            _UpdateQueue.Enqueue((manager) => manager.RemoveContextSync(indexes));
        }
        /// <summary>
        /// 移除上下文
        /// </summary>
        /// <param name="entities"></param>
        public void RemoveContexts(Entity[] entities)
        {
            _UpdateQueue.Enqueue((manager) => manager.RemoveContextSync(entities));
        }

        /// <summary>
        /// 遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="foreachAction"></param>
        public void Foreach<TGroup>(Action<TGroup> foreachAction) where TGroup : struct => _EntityManager.Foreach(foreachAction);
        /// <summary>
        /// 遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="foreachAction"></param>
        public void Foreach<TGroup>(Action<TGroup, int> foreachAction) where TGroup : struct => _EntityManager.Foreach(foreachAction);
        /// <summary>
        /// 遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="foreachFunc"></param>
        public void Foreach<TGroup>(Func<TGroup> foreachFunc) where TGroup : struct => _EntityManager.Foreach(foreachFunc);
        /// <summary>
        /// 遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="foreachFunc"></param>
        public void Foreach<TGroup>(Func<int, TGroup> foreachFunc) where TGroup : struct => _EntityManager.Foreach(foreachFunc);
        /// <summary>
        /// 遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="foreachFunc"></param>
        public void Foreach<TGroup>(Func<TGroup, TGroup> foreachFunc) where TGroup : struct => _EntityManager.Foreach(foreachFunc);
        /// <summary>
        /// 遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="foreachFunc"></param>
        public void Foreach<TGroup>(Func<TGroup, int, TGroup> foreachFunc) where TGroup : struct => _EntityManager.Foreach(foreachFunc);
        /// <summary>
        /// 遍历
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="handle"></param>
        public void Foreach<TGroup>(RefHandle<TGroup> handle) where TGroup : struct => _EntityManager.Foreach(handle);

        /// <summary>
        /// 获取指定Entity的数据
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="entity"></param>
        public void GetData<TGroup>(Entity entity) where TGroup : struct => _EntityManager.GetData<TGroup>(entity);
        /// <summary>
        /// 写入数据
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="entity"></param>
        /// <param name="group"></param>
        public void WriteData<TGroup>(Entity entity, TGroup group) where TGroup : struct => _EntityManager.WriteData(entity, group);

        /// <summary>
        /// 刷新提交队列
        /// 此方法在多线程环境下调用将会导致不正确的结果
        /// </summary>
        public void RefreshUpdateQueue()
        {
            while(_UpdateQueue.TryDequeue(out Action<VulkanContextManager> action))
            {
                action(this);
            }
        }

        /// <summary>
        /// 获取上下文
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Context GetContext(int index) => _ContextList[index].Context;
        /// <summary>
        /// 获取上下文
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Context GetContext(Entity entity) => _ContextList[entity.Index].Context;

        /// <summary>
        /// 释放资源
        /// </summary>
        public void Dispose()
        {
            if (!_IsDispose)
            {
                _IsDispose = true;
                _EntityManager.Dispose();
                _ContextList.Clear();
            }
        }

        /// <summary>
        /// 同步创建上下文
        /// </summary>
        /// <param name="size"></param>
        /// <param name="archetype"></param>
        /// <param name="renderGroup"></param>
        /// <param name="renderObject"></param>
        /// <param name="deepCopy"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void CreateContextsSync(int size, Archetype archetype, IRenderGroup renderGroup, IRenderObject renderObject, bool deepCopy)
        {
            VulkanContext[] contexts = CreateVulkanContexts(size, archetype, renderGroup);
            if (renderObject == null) renderGroup.BindingContext(contexts);
            else renderGroup.BindingContext(contexts, renderObject, deepCopy);
        }
        /// <summary>
        /// 同步创建上下文
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="size"></param>
        /// <param name="archetype"></param>
        /// <param name="renderGroup"></param>
        /// <param name="renderObject"></param>
        /// <param name="deepCopy"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void CreateContextsSync(Context parent, int size, Archetype archetype, IRenderGroup renderGroup, IRenderObject renderObject, bool deepCopy)
        {
            VulkanContext[] contexts = CreateVulkanContexts(parent, size, archetype, renderGroup);
            if (renderObject == null) renderGroup.BindingContext(contexts);
            else renderGroup.BindingContext(contexts, renderObject, deepCopy);
        }
        /// <summary>
        /// 同步创建上下文
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="size"></param>
        /// <param name="archetype"></param>
        /// <param name="renderGroup"></param>
        /// <param name="renderObject"></param>
        /// <param name="initData"></param>
        /// <param name="deepCopy"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void CreateContextsSync<TGroup>(int size, Archetype archetype, IRenderGroup renderGroup, IRenderObject renderObject, TGroup initData, bool deepCopy)
            where TGroup : struct
        {
            var contexts = CreateVulkanContexts(size, archetype, renderGroup, initData);
            if (renderObject == null) renderGroup.BindingContext(contexts);
            else renderGroup.BindingContext(contexts, renderObject, deepCopy);
        }
        /// <summary>
        /// 同步创建上下文
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="parent"></param>
        /// <param name="size"></param>
        /// <param name="archetype"></param>
        /// <param name="renderGroup"></param>
        /// <param name="renderObject"></param>
        /// <param name="initData"></param>
        /// <param name="deepCopy"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void CreateContextsSync<TGroup>(Context parent, int size, Archetype archetype, IRenderGroup renderGroup, IRenderObject renderObject, TGroup initData, bool deepCopy)
            where TGroup : struct
        {
            var contexts = CreateVulkanContexts(parent, size, archetype, renderGroup, initData);
            if (renderObject == null) renderGroup.BindingContext(contexts);
            else renderGroup.BindingContext(contexts, renderObject, deepCopy);
        }
        /// <summary>
        /// 同步创建数据上下文
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="size"></param>
        /// <param name="archetype"></param>
        /// <param name="renderGroup"></param>
        /// <param name="renderObject"></param>
        /// <param name="offset"></param>
        /// <param name="deepCopy"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void CreateContextsSync(Context parent, int size, Archetype archetype, IRenderGroup renderGroup, IRenderObject renderObject, Transform offset, bool deepCopy)
        {
            var contexts = CreateVulkanContexts(parent, size, archetype, renderGroup);
            if (renderObject == null) renderGroup.BindingContext(contexts);
            else renderGroup.BindingContext(contexts, renderObject, deepCopy);
            var trans = renderObject.GetComponents<Transform>().FirstOrDefault();
            if (trans == null) renderObject.AddComponent(offset);
            else
            {
                trans.Position = offset.Position;
                trans.Rotation = offset.Rotation;
                trans.Scaling = offset.Scaling;
            }
        }
        /// <summary>
        /// 同步创建数据上下文
        /// </summary>
        /// <param name="size"></param>
        /// <param name="archetype"></param>
        /// <param name="renderGroup"></param>
        /// <param name="renderObject"></param>
        /// <param name="transform"></param>
        /// <param name="deepCopy"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void CreateContextsSync(int size, Archetype archetype, IRenderGroup renderGroup, IRenderObject renderObject, Transform transform, bool deepCopy)
        {
            var contexts = CreateVulkanContexts(size, archetype, renderGroup);
            if (renderObject == null) renderGroup.BindingContext(contexts);
            else renderGroup.BindingContext(contexts, renderObject, deepCopy);
            var trans = renderObject.GetComponents<Transform>().FirstOrDefault();
            if (trans == null) renderObject.AddComponent(transform);
            else
            {
                trans.Position = transform.Position;
                trans.Rotation = transform.Rotation;
                trans.Scaling = transform.Scaling;
            }
        }

        /// <summary>
        /// 同步移除上下文
        /// </summary>
        /// <param name="index"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void RemoveContextSync(int index)
        {
            if (index >= _ContextList.Count) throw new ArgumentOutOfRangeException();
            ContextWapper wapper = _ContextList[index];
            if(wapper.RenderGroup != null)
            {
                wapper.RenderGroup.UnbindContext(wapper.Context);
                wapper.RenderGroup = null;
            }
            if (wapper.Context != null)
            {
                _EntityManager.RecoveryEntity(wapper.Context.Entity);
                wapper.Context = null;
            }
            _ContextList[index] = wapper;
        }
        /// <summary>
        /// 同步移除上下文
        /// </summary>
        /// <param name="index"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void RemoveContextSync(int[] index)
        {
            foreach (var item in index) RemoveContextSync(item);
        }
        /// <summary>
        /// 同步移除上下文
        /// </summary>
        /// <param name="entities"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void RemoveContextSync(Entity[] entities)
        {
            foreach (var item in entities) RemoveContextSync(item.Index);
        }

        /// <summary>
        /// 创建上下文
        /// </summary>
        /// <param name="size"></param>
        /// <param name="archetype"></param>
        /// <param name="renderGroup"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private VulkanContext[] CreateVulkanContexts(int size, Archetype archetype, IRenderGroup renderGroup)
        {
            var list = _ContextList;
            _EntityManager.CreateEntities(archetype, size, ref _Index, out Entity[] entities);
            int diff = _Index - list.Count;
            if (diff > 0)
            {
                ContextWapper[] wappers = new ContextWapper[diff];
                _ContextList.AddRange(wappers);
            }
            VulkanContext[] contexts = new VulkanContext[entities.Length];
            Entity e;
            for (int i = 0; i < entities.Length; i++)
            {
                e = entities[i];
                VulkanContext context = new VulkanContext()
                {
                    Entity = e,
                    Archetype = archetype
                };
                ContextWapper wapper = list[e.Index];
                wapper.Context = context;
                wapper.RenderGroup = renderGroup;
                list[e.Index] = wapper;
                contexts[i] = context;
            }
            return contexts;
        }
        /// <summary>
        /// 创建上下文
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="size"></param>
        /// <param name="archetype"></param>
        /// <param name="renderGroup"></param>
        /// <param name="initData"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private VulkanContext[] CreateVulkanContexts<TGroup>(int size, Archetype archetype, IRenderGroup renderGroup, TGroup initData)
            where TGroup : struct
        {
            var list = _ContextList;
            _EntityManager.CreateEntities(archetype, size, ref _Index, initData, out Entity[] entities);
            int diff = _Index - list.Count;
            if (diff > 0)
            {
                ContextWapper[] wappers = new ContextWapper[diff];
                _ContextList.AddRange(wappers);
            }
            VulkanContext[] contexts = new VulkanContext[entities.Length];
            Entity e;
            for (int i = 0; i < entities.Length; i++)
            {
                e = entities[i];
                VulkanContext context = new VulkanContext()
                {
                    Entity = e,
                    Archetype = archetype
                };
                ContextWapper wapper = list[e.Index];
                wapper.Context = context;
                wapper.RenderGroup = renderGroup;
                list[e.Index] = wapper;
                contexts[i] = context;
            }
            return contexts;
        }
        /// <summary>
        /// 创建上下文
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="size"></param>
        /// <param name="archetype"></param>
        /// <param name="renderGroup"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private VulkanContext[] CreateVulkanContexts(Context parent, int size, Archetype archetype, IRenderGroup renderGroup)
        {
            var list = _ContextList;
            _EntityManager.CreateEntities(archetype, size, ref _Index, out Entity[] entities);
            int diff = _Index - list.Count;
            if (diff > 0)
            {
                ContextWapper[] wappers = new ContextWapper[diff];
                _ContextList.AddRange(wappers);
            }
            VulkanContext[] contexts = new VulkanContext[entities.Length];
            Entity e;
            for (int i = 0; i < entities.Length; i++)
            {
                e = entities[i];
                VulkanContext context = new VulkanContext()
                {
                    Entity = e,
                    Parent = parent,
                    Archetype = archetype
                };
                parent.AddChildren(context);
                ContextWapper wapper = list[e.Index];
                wapper.Context = context;
                wapper.RenderGroup = renderGroup;
                list[e.Index] = wapper;
                contexts[i] = context;
            }
            return contexts;
        }
        /// <summary>
        /// 创建上下文
        /// </summary>
        /// <typeparam name="TGroup"></typeparam>
        /// <param name="parent"></param>
        /// <param name="size"></param>
        /// <param name="archetype"></param>
        /// <param name="renderGroup"></param>
        /// <param name="initData"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private VulkanContext[] CreateVulkanContexts<TGroup>(Context parent, int size, Archetype archetype, IRenderGroup renderGroup, TGroup initData)
            where TGroup : struct
        {
            var list = _ContextList;
            _EntityManager.CreateEntities(archetype, size, ref _Index, initData, out Entity[] entities);
            int diff = _Index - list.Count;
            if (diff > 0)
            {
                ContextWapper[] wappers = new ContextWapper[diff];
                _ContextList.AddRange(wappers);
            }
            VulkanContext[] contexts = new VulkanContext[entities.Length];
            Entity e;
            for (int i = 0; i < entities.Length; i++)
            {
                e = entities[i];
                VulkanContext context = new VulkanContext()
                {
                    Entity = e,
                    Parent = parent,
                    Archetype = archetype
                };
                parent.AddChildren(context);
                ContextWapper wapper = list[e.Index];
                wapper.Context = context;
                wapper.RenderGroup = renderGroup;
                list[e.Index] = wapper;
                contexts[i] = context;
            }
            return contexts;
        }

        /// <summary>
        /// 上下文包装器
        /// </summary>
        private struct ContextWapper
        {
            public VulkanContext Context;
            public IRenderGroup RenderGroup;
            /// <summary>
            /// 
            /// </summary>
            /// <param name="context"></param>
            /// <param name="group"></param>
            public ContextWapper(VulkanContext context, IRenderGroup group)
            {
                Context = context;
                RenderGroup = group;
            }
        }
    }
}
