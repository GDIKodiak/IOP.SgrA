﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// Vulkan用模块
    /// </summary>
    public abstract class VulkanModule : IGraphicsModule
    {
        /// <summary>
        /// 是否完成加载
        /// </summary>
        public bool IsLoaded { get; protected internal set; }
        /// <summary>
        /// 加载
        /// </summary>
        public event Action<IGraphicsModule> Loaded;
        /// <summary>
        /// 卸载
        /// </summary>
        public event Action<IGraphicsModule> Unloaded;
        /// <summary>
        /// 上下文管理器
        /// </summary>
        public IContextManager ContextManager { get; set; }
        /// <summary>
        /// 图形接口管理器
        /// </summary>
        internal protected VulkanGraphicsManager VulkanManager { get; set; }
        /// <summary>
        /// 父模块
        /// </summary>
        public IGraphicsModule Parent { get; internal set; } = null;
        /// <summary>
        /// 优先级
        /// </summary>
        public ModulePriority Priority { get; internal set; } = ModulePriority.None;
        /// <summary>
        /// 子模块
        /// </summary>
        internal List<IGraphicsModule> Childrens { get; set; } = new List<IGraphicsModule>();

        /// <summary>
        /// 加载
        /// </summary>
        /// <returns></returns>
        public virtual async Task Load()
        {
            await Load(VulkanManager);
            IsLoaded = true;
            Loaded?.Invoke(this);
        }
        /// <summary>
        /// 卸载
        /// </summary>
        /// <returns></returns>
        public virtual async Task Unload()
        {
            await Unload(VulkanManager);
            IsLoaded = false;
            Unloaded?.Invoke(this);
        }
        /// <summary>
        /// 枚举子模块
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IGraphicsModule> GetChildrens() => Childrens;

        /// <summary>
        /// 加载
        /// </summary>
        /// <param name="manager"></param>
        /// <returns></returns>
        protected abstract Task Load(VulkanGraphicsManager manager);
        /// <summary>
        /// 卸载模块
        /// </summary>
        /// <param name="manager"></param>
        /// <returns></returns>
        protected abstract Task Unload(VulkanGraphicsManager manager);
    }
}
