﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan.Scripted
{
    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class SubpassDependencyAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        public uint SrcSubpass {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public uint DstSubpass { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public AccessFlags SrcAccessMask { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public AccessFlags DstAccessMask { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public PipelineStageFlags SrcStageMask { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public PipelineStageFlags DstStageMask { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DependencyFlags DependencyFlags { get; set; }
    }
}
