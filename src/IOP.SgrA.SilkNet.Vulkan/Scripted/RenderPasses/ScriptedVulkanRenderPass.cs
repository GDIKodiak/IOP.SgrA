﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan.Scripted
{
    /// <summary>
    /// 脚本化渲染通道
    /// </summary>
    public class ScriptedVulkanRenderPass
    {
        /// <summary>
        /// 全局多重采样次数
        /// </summary>
        public SampleCount GlobalSampleCount { get; set; } = SampleCount.Sample1X;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="area"></param>
        /// <param name="device"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public virtual VulkanRenderPass Build(VulkanDevice device, Area area) => throw new NotImplementedException();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="device"></param>
        /// <param name="swapchain"></param>
        /// <param name="area"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public virtual VulkanRenderPass Build(VulkanDevice device, VulkanSwapchain swapchain, Area area) => throw new NotImplementedException();
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual RenderPassCreateOption BuildRenderPassOption() => new RenderPassCreateOption();
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual ImageAndImageViewCreateOption[] BuildFrameBufferOption() => Array.Empty<ImageAndImageViewCreateOption>();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="area"></param>
        public virtual RenderPassBeginOption BuildRenderPassBeginInfo(Area area) => new RenderPassBeginOption();
    }
}
