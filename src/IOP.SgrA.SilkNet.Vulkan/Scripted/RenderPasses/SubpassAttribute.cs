﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan.Scripted
{
    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class SubpassAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        public int SubpassIndex {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public PipelineBindPoint PipelineBindPoint { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="subpassIndex"></param>
        /// <param name="pipelineBindPoint"></param>
        public SubpassAttribute(int subpassIndex, PipelineBindPoint pipelineBindPoint)
        {
            SubpassIndex = subpassIndex;
            PipelineBindPoint = pipelineBindPoint;
        }

        private SubpassAttribute() { }
    }
}
