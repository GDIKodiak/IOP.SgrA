﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan.Scripted
{
    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = true)]
    public class SubpassAttachmentAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        public int SubpassIndex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public SubpassAttachmentType AttachmentType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ImageLayout ImageLayout { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="attachmentType"></param>
        /// <param name="subpassIndex"></param>
        /// <param name="imageLayout"></param>
        public SubpassAttachmentAttribute(int subpassIndex, SubpassAttachmentType attachmentType, ImageLayout imageLayout)
        {
            AttachmentType = attachmentType;
            SubpassIndex = subpassIndex;
            ImageLayout = imageLayout;
        }

        private SubpassAttachmentAttribute() { }
    }

    /// <summary>
    /// 
    /// </summary>
    public enum SubpassAttachmentType
    {
        /// <summary>
        /// 
        /// </summary>
        ColorAttachment,
        /// <summary>
        /// 
        /// </summary>
        DepthStencilAttachment,
        /// <summary>
        /// 
        /// </summary>
        InputAttachment,
        /// <summary>
        /// 
        /// </summary>
        ResolveAttachment
    }
}
