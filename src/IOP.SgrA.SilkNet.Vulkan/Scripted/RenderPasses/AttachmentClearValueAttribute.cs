﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan.Scripted
{
    /// <summary>
    /// 
    /// </summary>
    public class AttachmentClearValueAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        public float Float0 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public float Float1 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public float Float2 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public float Float3 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public float Depth { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public uint Stencil { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="f0"></param>
        /// <param name="f1"></param>
        /// <param name="f2"></param>
        /// <param name="f3"></param>
        public AttachmentClearValueAttribute(float f0, float f1, float f2, float f3)
        {
            Float0 = f0;
            Float1 = f1;
            Float2 = f2;
            Float3 = f3;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="depth"></param>
        /// <param name="stencil"></param>
        public AttachmentClearValueAttribute(float depth, uint stencil)
        {
            Depth = depth;
            Stencil = stencil;
        }

        private AttachmentClearValueAttribute() { }
    }
}
