﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan.Scripted
{
    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class RenderPassAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public RenderPassMode RenderPassMode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Layers { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int FrameBufferCount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="mode"></param>
        public RenderPassAttribute(string name, RenderPassMode mode)
        {
            Name = name;
            RenderPassMode = mode;
        }

        private RenderPassAttribute() { }
    }
}
