﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan.Scripted
{
    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class AttachmentAttribute : Attribute
    {
        /// <summary>
        /// 初始布局
        /// </summary>
        public ImageLayout InitialLayout { get; set; }
        /// <summary>
        /// 最终布局
        /// </summary>
        public ImageLayout FinalLayout { get; set; }
        /// <summary>
        /// 格式
        /// </summary>
        public Format Format { get; set; }
        /// <summary>
        /// 附件加载操作
        /// </summary>
        public AttachmentLoadOp LoadOp { get; set; }
        /// <summary>
        /// 附件存储操作
        /// </summary>
        public AttachmentStoreOp StoreOp { get; set; }
        /// <summary>
        /// 附件模板操作
        /// </summary>
        public AttachmentLoadOp StencilLoadOp { get; set; }
        /// <summary>
        /// 附件模板存储
        /// </summary>
        public AttachmentStoreOp StencilStoreOp { get; set; }
        /// <summary>
        /// 采样次数
        /// </summary>
        public SampleCountFlags Samples { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="finalLayout"></param>
        /// <param name="format"></param>
        public AttachmentAttribute(ImageLayout finalLayout, Format format)
        {
            InitialLayout = ImageLayout.Undefined;
            FinalLayout = finalLayout;
            Format = format;
            LoadOp = AttachmentLoadOp.Clear;
            StoreOp = AttachmentStoreOp.Store;
            StencilLoadOp = AttachmentLoadOp.DontCare;
            StencilStoreOp = AttachmentStoreOp.DontCare;
            Samples = SampleCountFlags.Count1Bit;
        }

        private AttachmentAttribute() { }
    }
}
