﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan.Scripted
{
    /// <summary>
    /// 几何着色器输出
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class GeometryVertexOutputAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        public GeometryOutputType OutputType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public uint MaxVertices {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="outputType"></param>
        /// <param name="maxVertices"></param>
        public GeometryVertexOutputAttribute(GeometryOutputType outputType, uint maxVertices)
        {
            OutputType = outputType;
            MaxVertices = maxVertices;
        }
        /// <summary>
        /// 
        /// </summary>
        private GeometryVertexOutputAttribute()
        {

        }
    }

    /// <summary>
    /// 
    /// </summary>
    public enum GeometryOutputType
    {
        /// <summary>
        /// 
        /// </summary>
        Points,
        /// <summary>
        /// 
        /// </summary>
        LineStrip,
        /// <summary>
        /// 
        /// </summary>
        TriangleStrip
    }
}
