﻿using System;
using IOP.SgrA.GLSL;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan.Scripted
{
    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class StorageImageAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        public uint Set { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public uint Binding { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public StorageFormat Layout { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DescriptorSetTarget Owner { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ImageMemoryQualifier AccessQualifier { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="set"></param>
        /// <param name="binding"></param>
        /// <param name="layout"></param>
        /// <param name="owner"></param>
        public StorageImageAttribute(uint set, uint binding, StorageFormat layout, DescriptorSetTarget owner)
        {
            Set = set;
            Binding = binding;
            Layout = layout;
            Owner = owner;
        }

        private StorageImageAttribute() { }
    }
}
