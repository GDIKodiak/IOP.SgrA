﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan.Scripted
{
    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class FragOutputAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        public uint Location { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool BlendEnable { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public BlendOp AlphaBlendOp {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public BlendOp ColorBlendOp { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public BlendFactor SrcColorBlendFactor {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public BlendFactor DstColorBlendFactor {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public BlendFactor SrcAlphaBlendFactor {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public BlendFactor DstAlphaBlendFactor { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ColorComponentFlags ColorWriteMask { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="location"></param>
        /// <param name="blendEnable"></param>
        public FragOutputAttribute(uint location, bool blendEnable)
        {
            Location = location;
            BlendEnable = blendEnable;
        }

        private FragOutputAttribute()
        {

        }
    }
}
