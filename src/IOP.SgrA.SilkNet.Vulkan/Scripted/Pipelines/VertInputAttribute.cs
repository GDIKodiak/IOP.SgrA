﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan.Scripted
{
    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class VertInputAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        public uint Location { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Format Format {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public uint Offset { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public uint Size { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public uint Binding { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="location"></param>
        /// <param name="format"></param>
        /// <param name="offset"></param>
        public VertInputAttribute(uint location, Format format)
        {
            Location = location;
            Format = format;
        }

        private VertInputAttribute()
        {

        }
    }
}
