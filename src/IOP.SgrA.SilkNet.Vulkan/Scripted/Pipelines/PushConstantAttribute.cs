﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan.Scripted
{
    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class PushConstantAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        public uint Size { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public uint Offset { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="size"></param>
        /// <param name="offset"></param>
        public PushConstantAttribute(uint size, uint offset)
        {
            Size = size;
            Offset = offset;
        }

        private PushConstantAttribute() { }
    }
}
