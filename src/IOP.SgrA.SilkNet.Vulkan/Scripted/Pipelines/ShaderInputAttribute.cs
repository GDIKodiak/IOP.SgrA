﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan.Scripted
{
    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ShaderInputAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        public uint Location { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="location"></param>
        public ShaderInputAttribute(uint location)
        {
            Location = location;
        }

        private ShaderInputAttribute() { }
    }
}
