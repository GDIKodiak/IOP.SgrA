﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan.Scripted
{
    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class ImageConfigAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        public ImageUsageFlags Usage { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ImageTiling Tiling { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public SharingMode SharingMode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Format Format { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public uint MipLevels { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public uint ArrayLayers { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ImageType ImageType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public SampleCountFlags Samples { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ImageLayout InitialLayout { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ImageViewType ViewType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ComponentSwizzle ComponentMappingR { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ComponentSwizzle ComponentMappingG { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ComponentSwizzle ComponentMappingB { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ComponentSwizzle ComponentMappingA { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ImageAspectFlags SubresourceRangeAspect { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public uint SubresourceRangeBaseMipLevel { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public uint SubresourceRangeMipLevelCount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public uint SubresourceRangeArrayLayer { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public uint SubresourceRangeLayersCount { get; set; }
    }
}
