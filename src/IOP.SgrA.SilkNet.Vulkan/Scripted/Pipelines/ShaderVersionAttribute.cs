﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan.Scripted
{
    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class ShaderVersionAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        public string Version { get; set; } = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="version"></param>
        public ShaderVersionAttribute(string version)
        {
            if(string.IsNullOrEmpty(version)) version = string.Empty;
            Version = version;
        }

        private ShaderVersionAttribute()
        {

        }
    }
}
