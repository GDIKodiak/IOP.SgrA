﻿using IOP.SgrA.GLSL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan.Scripted
{
    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class StorageBufferAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        public uint Set { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public uint Binding { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DescriptorSetTarget Owner { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public BufferLayout Layout { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="set"></param>
        /// <param name="binding"></param>
        /// <param name="owner"></param>
        public StorageBufferAttribute(uint set, uint binding, DescriptorSetTarget owner)
        {
            Set = set;
            Binding = binding;
            Owner = owner;
        }

        private StorageBufferAttribute() { }
    }
}
