﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan.Scripted
{
    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class GLSLExtensionAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        public string Extension { get; set; } = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="extension"></param>
        public GLSLExtensionAttribute(string extension)
        {
            if(string.IsNullOrEmpty(extension)) extension = string.Empty;
            Extension = extension;
        }

        private GLSLExtensionAttribute()
        {

        }
    }
}
