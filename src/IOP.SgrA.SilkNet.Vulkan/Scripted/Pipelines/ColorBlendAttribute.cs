﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan.Scripted
{
    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class ColorBlendAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        public bool LogicOpEnable { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public LogicOp LogicOp { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Vector4 BlendConstants {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="logicOpEnable"></param>
        /// <param name="logicOp"></param>
        /// <param name="blendConstants"></param>
        public ColorBlendAttribute(bool logicOpEnable, LogicOp logicOp)
        {
            LogicOpEnable = logicOpEnable;
            LogicOp = logicOp;
        }
    }
}
