﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan.Scripted
{
    /// <summary>
    /// 几何着色器输入
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class GeometryVertexInputAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        public GeometryInputType InputType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputType"></param>
        public GeometryVertexInputAttribute(GeometryInputType inputType)
        {
            InputType = inputType;
        }
        /// <summary>
        /// 
        /// </summary>
        private GeometryVertexInputAttribute()
        {

        }
    }

    /// <summary>
    /// 
    /// </summary>
    public enum GeometryInputType
    {
        /// <summary>
        /// 
        /// </summary>
        Points,
        /// <summary>
        /// 
        /// </summary>
        Lines,
        /// <summary>
        /// 
        /// </summary>
        LinesAdjacency,
        /// <summary>
        /// 
        /// </summary>
        Triangles,
        /// <summary>
        /// 
        /// </summary>
        TrianglesAdjacency
    }
}
