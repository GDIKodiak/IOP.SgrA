﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan.Scripted
{
    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class VertexInputBindingAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        public uint Binding {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public uint Stride {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public VertexInputRate InputRate {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="binding"></param>
        /// <param name="stride"></param>
        public VertexInputBindingAttribute(uint binding, uint stride)
        {
            Binding = binding;
            Stride = stride;
        }

        private VertexInputBindingAttribute() { }
    }
}
