﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan.Scripted
{
    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class InputAttachmentAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        public uint Set { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public uint Binding {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public uint AttachmentIndex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="set"></param>
        /// <param name="attachmentIndex"></param>
        /// <param name="binding"></param>
        public InputAttachmentAttribute(uint set, uint attachmentIndex, uint binding)
        {
            Set = set;
            AttachmentIndex = attachmentIndex;
            Binding = binding;
        }

        private InputAttachmentAttribute() { }
    }
}
