﻿using IOP.SgrA.GLSL;
using Silk.NET.Shaderc;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan.Scripted
{
    /// <summary>
    /// 脚本化管线
    /// </summary>
    public class ScriptedVulkanPipeline
    {
        /// <summary>
        /// 
        /// </summary>
        public virtual uint Subpass { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual PipelineVertexInputStateCreateOption CreateVertexInputState()
        {
            return new PipelineVertexInputStateCreateOption()
            {
                VertexAttributeDescriptions = Array.Empty<VertexInputAttributeDescription>(),
                VertexBindingDescriptions = Array.Empty<VertexInputBindingDescription>()
            };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual PipelineInputAssemblyStateCreateOption CreateInputAssemblyState() => new PipelineInputAssemblyStateCreateOption()
        {
            Topology = PrimitiveTopology.TriangleList,
            PrimitiveRestartEnable = false
        };
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual PipelineRasterizationStateCreateOption CreateRasterizationState()
        {
            return new PipelineRasterizationStateCreateOption()
            {
                PolygonMode = PolygonMode.Fill,
                CullMode = CullModeFlags.None,
                FrontFace = FrontFace.CounterClockwise,
                DepthClampEnable = true,
                RasterizerDiscardEnable = false,
                DepthBiasEnable = false,
                DepthBiasConstantFactor = 0,
                DepthBiasClamp = 0,
                DepthBiasSlopeFactor = 0,
                LineWidth = 1.0f
            };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual PipelineColorBlendStateCreateOption CreateColorBlendState()
        {
            return new PipelineColorBlendStateCreateOption()
            {
                Attachments = Array.Empty<PipelineColorBlendAttachmentState>(),
                BlendConstants = (0f, 0f, 0f, 0f),
                LogicOp = LogicOp.NoOp,
                LogicOpEnable = false
            };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual PipelineDepthStencilStateCreateOption CreateDepthStencilState()
        {
            return new PipelineDepthStencilStateCreateOption()
            {
                Back = new StencilOpState()
                {
                    FailOp = StencilOp.Keep,
                    PassOp = StencilOp.Keep,
                    CompareOp = CompareOp.Always,
                    CompareMask = 0,
                    Reference = 0,
                    DepthFailOp = StencilOp.Keep,
                    WriteMask = 0
                },
                Front = new StencilOpState()
                {
                    FailOp = StencilOp.Keep,
                    PassOp = StencilOp.Keep,
                    CompareOp = CompareOp.Always,
                    CompareMask = 0,
                    Reference = 0,
                    DepthFailOp = StencilOp.Keep,
                    WriteMask = 0
                },
                DepthWriteEnable = false,
                DepthTestEnable = false,
                DepthCompareOp = CompareOp.Greater,
                DepthBoundsTestEnable = false,
                MinDepthBounds = 0,
                MaxDepthBounds = 0,
                StencilTestEnable = false
            };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual PipelineMultisampleStateCreateOption CreateMultisampleState()
        {
            return new PipelineMultisampleStateCreateOption()
            {
                RasterizationSamples = SampleCountFlags.Count1Bit,
                SampleShadingEnable = false,
                AlphaToCoverageEnable = false,
                AlphaToOneEnable = false,
                MinSampleShading = 0.0f
            };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual PipelineDynamicStateCreateOption CreateDynamicState()
        {
            return new PipelineDynamicStateCreateOption()
            {
                DynamicStates = new DynamicState[]
                {
                    DynamicState.Viewport,
                    DynamicState.Scissor
                }
            };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public virtual GraphicsPipelineCreateOption BuildPipelineOption(Area area) => throw new NotImplementedException();
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public virtual PipelineLayoutCreateOption BuildPipelineLayoutOption() => throw new NotImplementedException();
        /// <summary>
        /// 
        /// </summary>
        public virtual DescriptorSetLayoutCreateOption[] BuildPipelineDescriptorSetLayoutOptions() => throw new NotImplementedException();
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual VulkanGLSLShaderInfo[] CompileScriptedShaders() => Array.Empty<VulkanGLSLShaderInfo>();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vert"></param>
        /// <returns></returns>
        /// <exception cref="NullReferenceException"></exception>
        protected virtual VulkanGLSLShaderInfo CompileShader(GLSLVert vert)
        {
            string code = vert.GetCode();
            if (string.IsNullOrEmpty(code)) throw new NullReferenceException("No GLSL source code");
            VulkanGLSLShaderInfo info = new VulkanGLSLShaderInfo(ShaderTypes.VertexShader, $"vert", code);
            info.CompileShader();
            return info;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="frag"></param>
        /// <returns></returns>
        /// <exception cref="NullReferenceException"></exception>
        protected virtual VulkanGLSLShaderInfo CompileShader(GLSLFrag frag)
        {
            string code = frag.GetCode();
            if (string.IsNullOrEmpty(code)) throw new NullReferenceException("No GLSL source code");
            VulkanGLSLShaderInfo info = new VulkanGLSLShaderInfo(ShaderTypes.FragmentShader, $"frag", code);
            info.CompileShader();
            return info;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="geom"></param>
        /// <returns></returns>
        /// <exception cref="NullReferenceException"></exception>
        protected virtual VulkanGLSLShaderInfo CompileShader(GLSLGeom geom)
        {
            string code = geom.GetCode();
            if (string.IsNullOrEmpty(code)) throw new NullReferenceException("No GLSL source code");
            VulkanGLSLShaderInfo info = new VulkanGLSLShaderInfo(ShaderTypes.GeometryShader, $"geom", code);
            info.CompileShader();
            return info;
        }
    }
}
