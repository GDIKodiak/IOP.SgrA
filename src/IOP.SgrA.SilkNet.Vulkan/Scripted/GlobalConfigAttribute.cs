﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan.Scripted
{
    /// <summary>
    /// 指示使用全局配置
    /// </summary>
    public class GlobalConfigAttribute : Attribute
    {
        /// <summary>
        /// 使用全局多重采样配置
        /// </summary>
        public bool UseGlobalSample { get; set; }
    }
}
