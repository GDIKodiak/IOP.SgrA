﻿using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 
    /// </summary>
    public static class VulkanTextExtension
    {
        /// <summary>
        /// 获取或者创建文字渲染对象
        /// </summary>
        /// <param name="text"></param>
        /// <param name="font"></param>
        /// <param name="column"></param>
        /// <param name="row"></param>
        /// <param name="manager"></param>
        /// <returns></returns>
        public static IRenderObject GetOrCreateTextRender(this VulkanGraphicsManager manager, string text, IFont font, int column, int row)
        {
            var device = manager.VulkanDevice;
            var name = $"{text}:{font.Name}:{column}{row}";
            if (manager.TryGetRenderObject(name, out RenderObject renderObject)) return renderObject;
            (float[] data, uint vCount) = font.CreateTextMeshData(text, column, row);
            var vro = manager.CreateEmptyMesh<VRO>(name).CreateVecticesBuffer(data, vCount, device, SharingMode.Exclusive);
            var obj = manager.CreateRenderObject<RenderObject>(name);
            obj.AddComponent(vro);
            obj.AddComponent(font);
            obj.AddComponent(font.GetFontTexture());
            return obj;
        }

        /// <summary>
        /// 移除纹理渲染对象
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="textRneder"></param>
        public static void RemoveTextRender(this VulkanGraphicsManager manager, IRenderObject textRneder) => manager.RemoveRenderObject(textRneder.Name);

        /// <summary>
        /// 创建有向距离场文字
        /// </summary>
        /// <param name="vulkanManager"></param>
        /// <param name="name"></param>
        /// <param name="creater"></param>
        /// <param name="texture"></param>
        /// <returns></returns>
        public static VulkanSDFFont CreateSDFFont(this VulkanGraphicsManager vulkanManager, string name, CharacterCreater creater, VulkanTexture texture)
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));
            VulkanSDFFont vulkanFont = new VulkanSDFFont
            {
                Creater = creater ?? throw new ArgumentNullException(nameof(creater)),
                FontTexture = texture ?? throw new ArgumentNullException(nameof(texture)),
                Name = name
            };
            vulkanManager.AddFont(vulkanFont);
            return vulkanFont;
        }
    }
}
