﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// 有向距离场字体
    /// </summary>
    public class VulkanSDFFont : IFont
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; protected internal set; }
        /// <summary>
        /// 文字贴图数据
        /// </summary>
        public VulkanTexture FontTexture { get; protected internal set; }
        /// <summary>
        /// 映射器
        /// </summary>
        public CharacterCreater Creater { get; protected internal set; }

        /// <summary>
        /// 创建文字网格数据
        /// </summary>
        /// <param name="str"></param>
        /// <param name="column"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        public virtual (float[], uint) CreateTextMeshData(string str, int column, int row)
        {
            return Creater.CreateTextMeshData(str, column, row);
        }
        /// <summary>
        /// 获取文字纹理
        /// </summary>
        /// <returns></returns>
        public ITexture GetFontTexture() => FontTexture;
        /// <summary>
        /// 拷贝至新的渲染对象
        /// </summary>
        /// <param name="newObj"></param>
        /// <exception cref="NotImplementedException"></exception>
        public void CloneToNewRender(IRenderObject newObj) => newObj.AddComponent(this);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderObject"></param>
        public void OnAttach(IRenderObject renderObject) { }
        /// <summary>
        /// 摧毁组件
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        public void Destroy()
        {
        }
    }
}
