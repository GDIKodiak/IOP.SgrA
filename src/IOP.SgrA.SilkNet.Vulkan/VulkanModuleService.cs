﻿using IOP.Extension.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// Vulkan模块服务
    /// </summary>
    public class VulkanModuleService : IModuleService
    {
        private readonly VulkanGraphicsManager GraphicsManager;
        private readonly IServiceProvider ServiceProvider;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="graphicsManager"></param>
        /// <param name="serviceProvider"></param>
        public VulkanModuleService(IGraphicsManager graphicsManager,
            IServiceProvider serviceProvider)
        {
            if (!(graphicsManager is VulkanGraphicsManager vulkan)) throw new InvalidCastException("No Vulkan Environment");
            GraphicsManager = vulkan;
            ServiceProvider = serviceProvider;
        }

        /// <summary>
        /// 创建模块
        /// </summary>
        /// <param name="types"></param>
        /// <returns></returns>
        public ModuleInfo[] CreateModules(params Type[] types)
        {
            if (types == null || types.Length == 0) return Array.Empty<ModuleInfo>();
            var modulesTypes = typeof(IGraphicsModule);
            var serviceProvider = ServiceProvider;
            List<ModuleInfo> infos = new List<ModuleInfo>();
            Dictionary<Type, IGraphicsModule> books = new();
            foreach (var type in types)
            {
                if (type.GetInterfaces().Any(x => x == modulesTypes))
                {
                    books.TryAdd(type, null);
                }
            }
            foreach (var type in types)
            {
                VulkanModule module = CreateModule(serviceProvider, type, books);
                if (module != null)
                {
                    infos.Add(new ModuleInfo(module.Priority, module));
                }
                else continue;
            }
            return infos.ToArray();
        }

        /// <summary>
        /// 创建模块
        /// </summary>
        /// <param name="extraModule"></param>
        /// <param name="types"></param>
        /// <returns></returns>
        public ModuleInfo[] CreateModules(Dictionary<Type, IGraphicsModule> extraModule, params Type[] types)
        {
            if (extraModule == null || extraModule.Count <= 0) return CreateModules(types);
            var modulesTypes = typeof(IGraphicsModule);
            List<ModuleInfo> infos = new List<ModuleInfo>();
            var serviceProvider = ServiceProvider;
            Dictionary<Type, IGraphicsModule> books = new(extraModule);
            foreach (var type in types)
            {
                if (type.GetInterfaces().Any(x => x == modulesTypes))
                {
                    books.TryAdd(type, null);
                }
            }
            foreach (var type in types)
            {
                VulkanModule module = CreateModule(serviceProvider, type, books);
                if (module != null)
                {
                    infos.Add(new ModuleInfo(module.Priority, module));
                }
                else continue;
            }
            return infos.ToArray();
        }

        /// <summary>
        /// 构建模块
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <param name="type"></param>
        /// <param name="books"></param>
        /// <returns></returns>
        private VulkanModule CreateModule(IServiceProvider serviceProvider, Type type, Dictionary<Type, IGraphicsModule> books)
        {
            var modulesTypes = typeof(IGraphicsModule);
            if (books.TryGetValue(type, out var modules) && modules is VulkanModule vm) 
                return vm;
            if (type.GetInterfaces().Any(x => x == modulesTypes))
            {
                var obj = serviceProvider.CreateAutowiredInstance(type) as IGraphicsModule;
                if (obj is VulkanModule vulkan)
                {
                    vulkan.VulkanManager = GraphicsManager;
                    ModulePriority priority = ModulePriority.None;
                    var attr = type.GetCustomAttributes();
                    if (attr.Any())
                    {
                        var p = attr.FirstOrDefault(x => x.GetType() == typeof(ModulePriorityAttribute));
                        if (p != null)
                        {
                            var pri = p as ModulePriorityAttribute;
                            priority = pri.ModulePriority;
                            vulkan.Priority = priority;
                        }
                        var pm = attr.FirstOrDefault(x => x.GetType() == typeof(ParentModuleAttribute));
                        if (pm != null)
                        {
                            ParentModuleAttribute pAttr = (ParentModuleAttribute)pm;
                            Type parentType = pAttr.ParentType;
                            if (books.TryGetValue(parentType, out IGraphicsModule gm))
                            {
                                if(gm == null)
                                {
                                    gm = CreateModule(serviceProvider, parentType, books);
                                    books[parentType] = gm;
                                }
                                if (gm is VulkanModule vgm)
                                {
                                    vulkan.Parent = vgm;
                                    vgm.Childrens.Add(vulkan);
                                }
                            }
                        }
                    }
                    books[type] = vulkan;
                    return vulkan;
                }
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
        }
    }
}
