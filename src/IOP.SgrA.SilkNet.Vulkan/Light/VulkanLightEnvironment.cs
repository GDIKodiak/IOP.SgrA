﻿using Microsoft.Extensions.Logging;
using Silk.NET.Vulkan;
using System;
using System.Buffers;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IOP.SgrA.SilkNet.Vulkan
{
    /// <summary>
    /// Vulkan光源环境
    /// </summary>
    public class VulkanLightEnvironment : ILightEnvironment
    {
        /// <summary>
        /// 所属场景
        /// </summary>
        public Scene Owner { get; protected internal set; }
        /// <summary>
        /// 当点光源数据源发生变更时
        /// </summary>
        public event Action<VulkanBufferTextureData, ILightEnvironment> OnPointLightSourceChanged;
        /// <summary>
        /// 当平行光源数据源发生变更时
        /// </summary>
        public event Action<VulkanBufferTextureData, ILightEnvironment> OnParallelLightSourceChanged;
        /// <summary>
        /// 环境光
        /// </summary>
        public Ambient Ambient { get; set; }

        /// <summary>
        /// 
        /// </summary>
        protected internal VulkanGraphicsManager Manager { get; set; }
        /// <summary>
        /// 点光源
        /// </summary>
        protected internal ConcurrentDictionary<string, PointLightInterop> PointLightDic { get; } = new ConcurrentDictionary<string, PointLightInterop>();
        /// <summary>
        /// 平行光源
        /// </summary>
        protected internal ConcurrentDictionary<string, ParallelLightInterop> ParaLightDic { get; } = new ConcurrentDictionary<string, ParallelLightInterop>();
        /// <summary>
        /// 
        /// </summary>
        protected internal readonly ConcurrentQueue<PointLightOperation> PointLightOperations = new ConcurrentQueue<PointLightOperation>();
        /// <summary>
        /// 
        /// </summary>
        protected internal readonly ConcurrentQueue<ParallelLightOperation> ParallelLightOperations = new ConcurrentQueue<ParallelLightOperation>();
        /// <summary>
        /// 
        /// </summary>
        protected internal readonly ConcurrentQueue<ChangeRequest> ChangeRequest = new ConcurrentQueue<ChangeRequest>();
        /// <summary>
        /// 点光源缓冲
        /// </summary>
        protected internal VulkanBufferTextureData[] PointLightBuffer;
        /// <summary>
        /// 
        /// </summary>
        protected internal VulkanBufferTextureData[] ParallelLightBuffer;
        /// <summary>
        /// 
        /// </summary>
        protected internal int PointLightBufferIndex = 0;
        /// <summary>
        /// 
        /// </summary>
        protected internal int ParallelLightBufferIndex = 0;
        /// <summary>
        /// 
        /// </summary>
        protected readonly object PointLSyncRoot = new();
        /// <summary>
        /// 
        /// </summary>
        protected long Counter = 0;
        /// <summary>
        /// 点光源
        /// </summary>
        public IEnumerable<PointLightInterop> PointLights
        {
            get
            {
                foreach(var item in PointLightDic.Values)
                {
                    yield return item;
                }
            }
        }
        /// <summary>
        /// 平行光源
        /// </summary>
        public IEnumerable<ParallelLightInterop> ParallelLights
        {
            get
            {
                foreach (var item in ParaLightDic.Values)
                {
                    yield return item;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scene"></param>
        /// <param name="manager"></param>
        public VulkanLightEnvironment(Scene scene, IGraphicsManager manager)
        {
            Owner = scene ?? throw new ArgumentNullException(nameof(scene));
            if (manager == null) throw new ArgumentNullException(nameof(manager));
            if (manager is not VulkanGraphicsManager vulkan) throw new InvalidOperationException("target graphics manager is not vulkan graphics manager");
            Manager = vulkan;
            PointLightBuffer = new VulkanBufferTextureData[2];
            PointLightBuffer[0] = Manager.CreateStorageBufferData(Guid.NewGuid().ToString("N"), (ulong)(Marshal.SizeOf<PointLightInterop>() * 8));
            PointLightBuffer[1] = Manager.CreateStorageBufferData(Guid.NewGuid().ToString("N"), (ulong)(Marshal.SizeOf<PointLightInterop>() * 8));
            ParallelLightBuffer = new VulkanBufferTextureData[2];
            ParallelLightBuffer[0] = Manager.CreateStorageBufferData(Guid.NewGuid().ToString("N"), (ulong)(Marshal.SizeOf<ParallelLightInterop>() * 4));
            ParallelLightBuffer[1] = Manager.CreateStorageBufferData(Guid.NewGuid().ToString("N"), (ulong)(Marshal.SizeOf<ParallelLightInterop>() * 4));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lightName"></param>
        /// <param name="position"></param>
        /// <param name="color"></param>
        /// <returns></returns>
        public string AddPointLight(string lightName, Vector3 position, Vector4 color)
        {
            int i = 0;
            while (PointLightDic.ContainsKey(lightName))
            {
                i++;
                lightName = $"{lightName}({i})";
            }
            PointLightInterop interop = new(position, color);
            NewPointLightOperation(LightOperation.Add, interop, lightName);
            return lightName;
        }
        /// <summary>
        /// 添加平行光
        /// </summary>
        /// <param name="lightName"></param>
        /// <param name="direction"></param>
        /// <param name="color"></param>
        /// <returns></returns>
        public string AddParallelLight(string lightName, Vector3 direction, Vector4 color)
        {
            int i = 0;
            while (ParaLightDic.ContainsKey(lightName))
            {
                i++;
                lightName = $"{lightName}({i})";
            }
            ParallelLightInterop interop = new(direction, color);
            NewParallelLightOperation(LightOperation.Add, interop, lightName);
            return lightName;
        }

        /// <summary>
        /// 移除点光源
        /// </summary>
        /// <param name="name"></param>
        public void RemovePointLight(string name) => NewPointLightOperation(LightOperation.Remove, default, name);
        /// <summary>
        /// 移除平行光源
        /// </summary>
        /// <param name="name"></param>
        public void RemoveParallelLight(string name) => NewParallelLightOperation(LightOperation.Remove, default, name);

        /// <summary>
        /// 更新点光源
        /// </summary>
        /// <param name="name"></param>
        /// <param name="newPosition"></param>
        public void ChangePointLight(string name, Vector3 newPosition)
        {
            if (string.IsNullOrEmpty(name)) return;
            if(PointLightDic.TryGetValue(name, out PointLightInterop p))
            {
                var newLight = new PointLightInterop(newPosition, p.Strength);
                NewPointLightOperation(LightOperation.Update, newLight, name);
            }
        }
        /// <summary>
        /// 更新点光源
        /// </summary>
        /// <param name="name"></param>
        /// <param name="newColor"></param>
        public void ChangePointLight(string name, Vector4 newColor)
        {
            if (string.IsNullOrEmpty(name)) return;
            if (PointLightDic.TryGetValue(name, out PointLightInterop p))
            {
                var newLight = new PointLightInterop(p.Position, newColor);
                NewPointLightOperation(LightOperation.Update, newLight, name);
            }
        }
        /// <summary>
        /// 更新点光源
        /// </summary>
        /// <param name="name"></param>
        /// <param name="newPosition"></param>
        /// <param name="newColor"></param>
        public void ChangePointLight(string name, Vector3 newPosition, Vector4 newColor)
        {
            if (string.IsNullOrEmpty(name)) return;
            var newLight = new PointLightInterop(newPosition, newColor);
            NewPointLightOperation(LightOperation.Update, newLight, name);
        }

        /// <summary>
        /// 更新平行光源
        /// </summary>
        /// <param name="name"></param>
        /// <param name="newDirection"></param>
        public void ChangeParallelLight(string name, Vector3 newDirection)
        {
            if (string.IsNullOrEmpty(name)) return;
            if (ParaLightDic.TryGetValue(name, out ParallelLightInterop p))
            {
                var newLight = new ParallelLightInterop(newDirection, p.Strength);
                NewParallelLightOperation(LightOperation.Update, newLight, name);
            }
        }
        /// <summary>
        /// 更新平行光源
        /// </summary>
        /// <param name="name"></param>
        /// <param name="newColor"></param>
        public void ChangeParallelLight(string name, Vector4 newColor)
        {
            if (string.IsNullOrEmpty(name)) return;
            if (ParaLightDic.TryGetValue(name, out ParallelLightInterop p))
            {
                var newLight = new ParallelLightInterop(p.Direction, newColor);
                NewParallelLightOperation(LightOperation.Update, newLight, name);
            }
        }
        /// <summary>
        /// 更新平行光源
        /// </summary>
        /// <param name="name"></param>
        /// <param name="newColor"></param>
        /// <param name="newDirection"></param>
        public void ChangeParallelLight(string name, Vector3 newDirection, Vector4 newColor)
        {
            if (string.IsNullOrEmpty(name)) return;
            var newLight = new ParallelLightInterop(newDirection, newColor);
            NewParallelLightOperation(LightOperation.Update, newLight, name);
        }

        /// <summary>
        /// 获取当前点光源缓冲
        /// </summary>
        /// <returns></returns>
        public VulkanTextureData GetCurrentPointLightBuffer()
        {
            var index = PointLightBufferIndex;
            SpinWait wait = new SpinWait();
            while(Interlocked.CompareExchange(ref PointLightBufferIndex, index, index) != index)
            {
                index = PointLightBufferIndex;
                wait.SpinOnce();
            }
            return PointLightBuffer[index];
        }
        /// <summary>
        /// 获取当前平行光缓冲
        /// </summary>
        /// <returns></returns>
        public VulkanTextureData GetCurrentParallelLightBuffer()
        {
            var index = ParallelLightBufferIndex;
            SpinWait wait = new SpinWait();
            while (Interlocked.CompareExchange(ref ParallelLightBufferIndex, index, index) != index)
            {
                index = ParallelLightBufferIndex;
                wait.SpinOnce();
            }
            return ParallelLightBuffer[index];
        }

        /// <summary>
        /// 获取当前点光源数量
        /// </summary>
        /// <returns></returns>
        public uint GetPointLightCount() => (uint)PointLightDic.Count;
        /// <summary>
        /// 获取当前平行光数量
        /// </summary>
        /// <returns></returns>
        public uint GetParallelLightCount() => (uint)(ParaLightDic.Count);

        /// <summary>
        /// 
        /// </summary>
        public void SyncLightBuffer()
        {
            long token = Interlocked.Increment(ref Counter);
            ChangeRequest request = new(token);
            if (ChangeRequest.TryPeek(out ChangeRequest r)) return;
            ChangeRequest.Enqueue(request);
            if (ChangeRequest.TryPeek(out r) && r.Token != token) return;
            Task.Factory.StartNew(() =>
            {
                SyncPointLightBuffer();
                SyncParallelLightBuffer();
            }).ContinueWith((t) => ChangeRequest.TryDequeue(out _));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="op"></param>
        /// <param name="light"></param>
        /// <param name="name"></param>
        protected void NewPointLightOperation(LightOperation op, PointLightInterop light, string name)
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));
            SpinWait wait = new();
            var index = PointLightBufferIndex;
            while(Interlocked.CompareExchange(ref PointLightBufferIndex, index, index) != index)
            {
                index = PointLightBufferIndex;
                wait.SpinOnce();
            }
            PointLightOperations.Enqueue(new PointLightOperation(index, op, light, name));
        }
        /// <summary>
        /// 新的平行光源操作
        /// </summary>
        /// <param name="op"></param>
        /// <param name="light"></param>
        /// <param name="name"></param>
        protected void NewParallelLightOperation(LightOperation op, ParallelLightInterop light, string name)
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));
            SpinWait wait = new();
            var index = ParallelLightBufferIndex;
            while (Interlocked.CompareExchange(ref ParallelLightBufferIndex, index, index) != index)
            {
                index = ParallelLightBufferIndex;
                wait.SpinOnce();
            }
            ParallelLightOperations.Enqueue(new ParallelLightOperation(index, op, light, name));
        }

        /// <summary>
        /// 
        /// </summary>
        protected void SyncPointLightBuffer()
        {
            if (PointLightOperations.IsEmpty) return;
            SpinWait wait = new();
            int index = PointLightBufferIndex; int nextIndex = (index + 1) % 2;
            while (Interlocked.CompareExchange(ref PointLightBufferIndex, nextIndex, index) != index)
            {
                index = PointLightBufferIndex;
                nextIndex = (index + 1) % 2;
                wait.SpinOnce();
            }
            while (PointLightOperations.TryDequeue(out PointLightOperation op))
            {
                if (op.Token != index)
                {
                    PointLightOperations.Enqueue(op);
                    break;
                }
                switch (op.Op)
                {
                    case LightOperation.Add:
                        PointLightDic.AddOrUpdate(op.Name, op.Light, (key, value) => op.Light);
                        break;
                    case LightOperation.Update:
                        if(PointLightDic.TryGetValue(op.Name ,out _))
                        {
                            PointLightDic.AddOrUpdate(op.Name, op.Light, (key, value) => op.Light);
                        }
                        break;
                    case LightOperation.Remove:
                        PointLightDic.TryRemove(op.Name, out _);
                        break;
                    default:
                        break;
                }
            }
            var count = PointLightDic.Count;
            if (count <= 0) return;
            var buffer = PointLightBuffer[index];
            ulong size = buffer.BytesSize;
            ulong dOffset = (ulong)(Marshal.SizeOf(typeof(PointLightOperation)));
            ulong end = (ulong)count * dOffset;
            if(end > size)
            {
                ulong newCount = (ulong)(size / dOffset * 1.414);
                var newBuffer = Manager.CreateStorageBufferData(Guid.NewGuid().ToString("N"), newCount * dOffset);
                Manager.DestroyTextureData(buffer);
                buffer = newBuffer;
                PointLightBuffer[index] = buffer;
            }
            byte[] local = ArrayPool<byte>.Shared.Rent((int)end);
            Span<byte> copy = local.AsSpan(0, (int)end); int i = 0;
            try
            {
                foreach (var item in PointLightDic.Values)
                {
                    int offset = i * (int)dOffset;
                    PointLightInterop p = item;
                    MemoryMarshal.Write(copy[offset..], ref p);
                }
                buffer.UpdateMemoryData(copy);
                OnPointLightSourceChanged?.Invoke(buffer, this);
            }
            finally
            {
                ArrayPool<byte>.Shared.Return(local);
            }
            if (PointLightOperations.IsEmpty)
            {
                Interlocked.Exchange(ref PointLightBufferIndex, index);
            }
        }
        /// <summary>
        /// 同步平行光
        /// </summary>
        protected void SyncParallelLightBuffer()
        {
            if (ParallelLightOperations.IsEmpty) return;
            SpinWait wait = new();
            int index = ParallelLightBufferIndex; int nextIndex = (index + 1) % 2;
            while (Interlocked.CompareExchange(ref ParallelLightBufferIndex, nextIndex, index) != index)
            {
                index = ParallelLightBufferIndex;
                nextIndex = (index + 1) % 2;
                wait.SpinOnce();
            }
            while (ParallelLightOperations.TryDequeue(out ParallelLightOperation op))
            {
                if (op.Token != index)
                {
                    ParallelLightOperations.Enqueue(op);
                    break;
                }
                switch (op.Op)
                {
                    case LightOperation.Add:
                        ParaLightDic.AddOrUpdate(op.Name, op.Light, (key, value) => op.Light);
                        break;
                    case LightOperation.Update:
                        if (ParaLightDic.TryGetValue(op.Name, out _))
                        {
                            ParaLightDic.AddOrUpdate(op.Name, op.Light, (key, value) => op.Light);
                        }
                        break;
                    case LightOperation.Remove:
                        ParaLightDic.TryRemove(op.Name, out _);
                        break;
                    default:
                        break;
                }
            }
            var count = ParaLightDic.Count;
            if (count <= 0) return;
            var buffer = ParallelLightBuffer[index];
            ulong size = buffer.BytesSize;
            ulong dOffset = (ulong)(Marshal.SizeOf(typeof(ParallelLightOperation)));
            ulong end = (ulong)count * dOffset;
            if (end > size)
            {
                ulong newCount = (ulong)(size / dOffset * 1.414);
                var newBuffer = Manager.CreateStorageBufferData(Guid.NewGuid().ToString("N"), newCount * dOffset);
                Manager.DestroyTextureData(buffer);
                buffer = newBuffer;
                ParallelLightBuffer[index] = buffer;
            }
            byte[] local = ArrayPool<byte>.Shared.Rent((int)end);
            Span<byte> copy = local.AsSpan(0, (int)end); int i = 0;
            try
            {
                foreach (var item in ParaLightDic.Values)
                {
                    int offset = i * (int)dOffset;
                    ParallelLightInterop p = item;
                    MemoryMarshal.Write(copy[offset..], ref p);
                }
                buffer.UpdateMemoryData(copy);
                OnParallelLightSourceChanged?.Invoke(buffer, this);
            }
            finally
            {
                ArrayPool<byte>.Shared.Return(local);
            }
            if (ParallelLightOperations.IsEmpty)
            {
                Interlocked.Exchange(ref ParallelLightBufferIndex, index);
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public struct PointLightOperation
    {
        /// <summary>
        /// 
        /// </summary>
        public int Token;
        /// <summary>
        /// 
        /// </summary>
        public PointLightInterop Light;
        /// <summary>
        /// 
        /// </summary>
        public string Name;
        /// <summary>
        /// 
        /// </summary>
        public LightOperation Op;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        /// <param name="light"></param>
        /// <param name="name"></param>
        /// <param name="op"></param>
        public PointLightOperation(int token, LightOperation op, PointLightInterop light, string name)
        {
            Light = light;
            Name = name;
            Token = token;
            Op = op;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public struct ParallelLightOperation
    {
        /// <summary>
        /// 
        /// </summary>
        public int Token;
        /// <summary>
        /// 
        /// </summary>
        public ParallelLightInterop Light;
        /// <summary>
        /// 
        /// </summary>
        public string Name;
        /// <summary>
        /// 
        /// </summary>
        public LightOperation Op;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        /// <param name="op"></param>
        /// <param name="light"></param>
        /// <param name="name"></param>
        public ParallelLightOperation(int token, LightOperation op, ParallelLightInterop light, string name)
        {
            Light = light;
            Name = name;
            Token = token;
            Op = op;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public struct ChangeRequest
    {
        /// <summary>
        /// 
        /// </summary>
        public long Token;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        public ChangeRequest(long token)
        {
            Token = token;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public enum LightOperation
    {
        /// <summary>
        /// 
        /// </summary>
        Add,
        /// <summary>
        /// 
        /// </summary>
        Update,
        /// <summary>
        /// 
        /// </summary>
        Remove
    }
}
