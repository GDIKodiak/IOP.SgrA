﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Kinematics
{
    /// <summary>
    /// 轴
    /// </summary>
    public class Joint
    {
        private static MatrixBuilder<double> M = Matrix<double>.Build;

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// 局部（物体）坐标系旋转轴
        /// </summary>
        public Vector<double> Axis { get; private set; }
        /// <summary>
        /// 当前轴在空间（世界）坐标系下的空间运动旋量
        /// </summary>
        public Screw VS { get; private set; }
        /// <summary>
        /// 上一个连杆
        /// </summary>
        public Link PreviewLink { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public Link NextLink { get; private set; }
        /// <summary>
        /// 最大轴值
        /// </summary>
        public double MaxTheta { get; set; } = Math.PI;
        /// <summary>
        /// 最小轴值
        /// </summary>
        public double MinTheta {  get; set; } = -Math.PI;

        /// <summary>
        /// 当前轴旋转角度
        /// </summary>
        public double CurrentTheta 
        {
            get => _Theta;
            internal set
            {
                if (_Theta != value)
                {
                    value = Math.Min(value, MaxTheta);
                    value = Math.Max(value, MinTheta);
                    _Theta = value;
                    _S = MatrixExp6(value);
                }
            }
        }

        /// <summary>
        /// 当前旋量绕CurrentTheta转动后的指数积
        /// </summary>
        public Matrix<double> CurrentExpS
        {
            get => _S;
        }

        private double _Theta = 0;
        private Matrix<double> _S = M.DenseIdentity(4, 4);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="axis"></param>
        /// <param name="s"></param>
        /// <param name="previewLink"></param>
        /// <param name="nextLink"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public Joint(string name, Vector<double> axis, Screw s, Link previewLink, Link nextLink)
        {
            if(string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));
            Name = name;
            Axis = axis ?? throw new ArgumentNullException(nameof(axis));
            VS = s ?? throw new ArgumentNullException(nameof(s));
            PreviewLink = previewLink ?? throw new ArgumentNullException(nameof(previewLink));
            NextLink = nextLink ?? throw new ArgumentNullException(nameof(nextLink));
            _S = M.MatrixExp6(s.Tose3(), 0);
        }

        /// <summary>
        /// 计算当前轴转动θ后的旋量指数积
        /// </summary>
        /// <param name="theta"></param>
        /// <returns></returns>
        public Matrix<double> MatrixExp6(double theta)
        {
            Matrix<double> se3 = VS.Tose3();
            var result = M.MatrixExp6(se3, theta);
            return result;
        }
    }
}
