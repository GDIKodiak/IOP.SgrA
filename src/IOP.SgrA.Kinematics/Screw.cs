﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

namespace IOP.SgrA.Kinematics
{
    /// <summary>
    /// 旋量
    /// </summary>
    public class Screw
    {
        private static readonly VectorBuilder<double> V = Vector<double>.Build;
        private static readonly MatrixBuilder<double> M = Matrix<double>.Build;

        /// <summary>
        /// 旋量主部
        /// 旋量方向向量
        /// </summary>
        public Vector<double> S;
        /// <summary>
        /// 旋量副部
        /// 该旋量上任意一点坐标与旋距的积
        /// </summary>
        public Vector<double> S0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="s0"></param>
        public Screw(Vector<double> s, Vector<double> s0)
        {
            if (s.Count != 3 || s0.Count != 3) throw new InvalidDataException("must be 3 dim vector");
            if (Math.Abs(s.L2Norm() - 1) > double.Epsilon)
            {
                s = s.Normalize(2);
            }
            S = s;
            S0 = s0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="s">单位向量</param>
        /// <param name="r">旋量上的点</param>
        /// <param name="h">旋距</param>
        public Screw(Vector<double> s, Vector<double> r, double h)
        {
            if (s.Count != 3 || r.Count != 3) throw new InvalidDataException("must be 3 dim vector");
            if (Math.Abs(s.L2Norm() - 1) > double.Epsilon)
            {
                s = s.Normalize(2);
            }
            S = s;
            S0 = r * h;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="s0"></param>
        public Screw(System.Numerics.Vector3 s, System.Numerics.Vector3 s0) 
        { 
            if(MathF.Abs(s.Length() - 1) > float.Epsilon)
            {
                s = System.Numerics.Vector3.Normalize(s);
            }
            S = V.Dense([s.X, s.Y, s.Z]);
            S0 = V.Dense([s0.X, s0.Y, s0.Z]);
        }

        /// <summary>
        /// 旋距
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public double Pitch()
        {
            double dot = S.DotProduct(S0);
            double s2 = S.DotProduct(S);
            return dot / s2;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"<{S[0]},{S[1]},{S[2]},{S0[0]},{S0[1]},{S0[2]}>";
        }

        /// <summary>
        /// 转换为6x1向量
        /// </summary>
        /// <returns></returns>
        public Vector<double> ToVector()
        {
            return V.Dense([S[0], S[1], S[2], S0[0], S0[1], S0[2]]);
        }

        /// <summary>
        /// 将旋量转换为se3矩阵形式
        /// </summary>
        /// <returns></returns>
        public Matrix<double> Tose3()
        {
            Vector<double> s = ToVector();
            return M.VectorTose3(s);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="screw"></param>
        /// <param name="theta"></param>
        /// <returns></returns>
        public static Screw operator *(Screw screw, double theta)
        {
            Vector<double> s = screw.S * theta;
            Vector<double> s0 = screw.S0 * theta;
            return new Screw(s, s0);

        }
    }
}
