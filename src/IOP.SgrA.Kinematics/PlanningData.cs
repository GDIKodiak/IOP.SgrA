﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Kinematics
{
    /// <summary>
    /// 规划数据
    /// </summary>
    public class PlanningData<T>
    {
        /// <summary>
        /// 状态
        /// </summary>
        public PlanningResult State { get; set; }
        /// <summary>
        /// 总执行时间
        /// </summary>
        public double Total {  get; set; }
        /// <summary>
        /// 关键帧
        /// </summary>
        public readonly List<PlanningKeyFrame<T>> KeyFrames = [];
        /// <summary>
        /// 
        /// </summary>
        public T StartData {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public T Data { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="state"></param>
        /// <param name="total"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        public PlanningData(PlanningResult state, double total, T start, T end)
        {
            State = state;
            Total = total;
            StartData = start;
            Data = end;
        }

        internal PlanningData(T start, T end)
        {
            StartData = start;
            Data = end;
        }
    }

    /// <summary>
    /// 规划关键帧
    /// </summary>
    public class PlanningKeyFrame<T>
    {
        /// <summary>
        /// 
        /// </summary>
        public int Index {  get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public double Time { get; private set; }
        /// <summary>
        /// 关键帧数据
        /// </summary>
        public T FrameData {  get; private set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="time"></param>
        /// <param name="t"></param>
        public PlanningKeyFrame(int index, double time, T t)
        {
            Index = index;
            Time = time;
            FrameData = t;
        }
    }
}
