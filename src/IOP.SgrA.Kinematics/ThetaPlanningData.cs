﻿namespace IOP.SgrA.Kinematics
{
    public class ThetaPlanningData : PlanningData<List<double>>
    {
        public ThetaPlanningData(PlanningResult state, double total, List<double> start, List<double> end) : 
            base(state, total, start, end)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        internal ThetaPlanningData(List<double> start, List<double> end)
            : base(start, end)
        {

        }
    }
}
