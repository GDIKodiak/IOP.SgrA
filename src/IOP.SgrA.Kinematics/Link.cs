﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Kinematics
{
    /// <summary>
    /// 连杆
    /// </summary>
    public class Link
    {
        private static MatrixBuilder<double> VM = Matrix<double>.Build;
        private Matrix<double> _M;

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// 上一个轴
        /// </summary>
        public Joint? PreviewJoint { get; set; }
        /// <summary>
        /// 下一个轴
        /// </summary>
        public Joint? NextJoint { get; set; }
        /// <summary>
        /// 当前连杆相对于上一个连杆的物体坐标系齐次矩阵
        /// 如果不存在上一个连杆则为单位齐次矩阵
        /// </summary>
        public Matrix<double> T {  get; set; }
        /// <summary>
        /// 当前连杆初始时刻相对于空间（世界）坐标系的位姿齐次矩阵
        /// </summary>
        public Matrix<double> M 
        {
            get => _M;
            set
            {
                if(value != null)
                {
                    _M = value;
                    CurrentM = value;
                }
            }
        }

        /// <summary>
        /// 当前连杆相对于空间坐标系的齐次转置矩阵
        /// </summary>
        public Matrix<double> CurrentM { get; internal set; }

        public Link(string name)
        {
            if(string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));
            Name = name;
            T = VM.DenseIdentity(4, 4);
            _M = VM.DenseIdentity(4, 4);
            CurrentM = VM.DenseIdentity(4, 4);
        }

        /// <summary>
        /// 遍历此连杆相连的所有父级轴
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Joint> EnumerablePreviewJoints()
        {
            if (PreviewJoint == null) yield break;
            Joint? local = PreviewJoint;
            Link localLink = this;
            while(local != null)
            {
                yield return local;
                localLink = local.PreviewLink;
                local = localLink.PreviewJoint;
            }
        }
        /// <summary>
        /// 遍历此连杆相连的所有子级轴
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Joint> EnumerableNextJoints()
        {
            if(NextJoint == null) yield break;
            Joint? local = NextJoint;
            Link localLink = this;
            while(local != null)
            {
                yield return local;
                localLink = local.NextLink;
                local = localLink.NextJoint;
            }
        }
        /// <summary>
        /// 获取与当前连杆关联的根连杆
        /// </summary>
        /// <returns></returns>
        public Link GetRootLink()
        {
            if (PreviewJoint == null) return this;
            Joint? local = PreviewJoint;
            Link result = this;
            while(local != null)
            {
                result = local.PreviewLink;
                local = result.PreviewJoint;
            }
            return result;
        }
        /// <summary>
        /// 获取当前连杆关联的终结连杆
        /// </summary>
        /// <returns></returns>
        public Link GetEndLink()
        {
            if(NextJoint == null) return this;
            Joint? local = NextJoint;
            Link result = this;
            while (local != null)
            {
                result = local.NextLink;
                local = result.NextJoint;
            }
            return result;
        }
    }
}
