﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Kinematics
{
    public static class NumbersExtension
    {
        /// <summary>
        /// 使用固定RPY角生成4x4旋转矩阵
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="r"></param>
        /// <param name="p"></param>
        /// <param name="yy"></param>
        /// <returns></returns>
        public static Matrix<double> CreateRotationFromRPY(this MatrixBuilder<double> builder, 
            double r, double p, double y)
        {
            Matrix<double> rx = builder.DenseOfArray(new double[,]
            {
                { 1, 0, 0, 0 },
                { 0, Math.Cos(r), -Math.Sin(r), 0 },
                { 0, Math.Sin(r), Math.Cos(r), 0 },
                { 0, 0, 0, 1 }
            });
            Matrix<double> ry = builder.DenseOfArray(new double[,]
            {
                { Math.Cos(p), 0, Math.Sin(p), 0 },
                { 0, 1, 0, 0 },
                { -Math.Sin(p), 0, Math.Cos(p), 0 },
                { 0, 0, 0, 1 }
            });
            Matrix<double> rz = builder.DenseOfArray(new double[,]
            {
                { Math.Cos(y), -Math.Sin(y), 0, 0 },
                { Math.Sin(y), Math.Cos(y), 0, 0 },
                { 0, 0, 1, 0 },
                { 0, 0, 0, 1 }
            });
            return rz * ry * rx;
        }
        /// <summary>
        /// 创建4x4偏移矩阵
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public static Matrix<double> CreateTranslation(this MatrixBuilder<double> builder, 
            double x, double y, double z)
        {
            Matrix<double> t = builder.DenseOfArray(new double[,]
            {
                { 1, 0, 0, x },
                { 0, 1, 0, y },
                { 0, 0, 1, z },
                { 0, 0, 0, 1 }
            });
            return t;
        }

        /// <summary>
        /// 求齐次矩阵的逆矩阵
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="T"></param>
        /// <returns></returns>
        public static Matrix<double> InverseSE3(this MatrixBuilder<double> builder, Matrix<double> T)
        {
            Matrix<double> r = T.SubMatrix(0, 3, 0, 3);
            Matrix<double> p = T.SubMatrix(0, 3, 3, 1);
            Matrix<double> transR = r.Transpose();
            Matrix<double> result = builder.DenseIdentity(4, 4);
            result.SetSubMatrix(0, 0, transR);
            result.SetSubMatrix(0, 3, -transR * p);
            return result;
        }
        /// <summary>
        /// 将向量转换为反对称矩阵
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="vec"></param>
        /// <returns></returns>
        public static Matrix<double> Vector2so3(this MatrixBuilder<double> builder, Vector<double> vec)
        {
            double x1 = vec[0];
            double x2 = vec[1];
            double x3 = vec[2];
            Matrix<double> result = builder.DenseOfRowArrays([[0, -x3, x2], [x3, 0, -x1], [-x2, x1, 0]]);
            return result;
        }
        /// <summary>
        /// 将指定反对称矩阵转为向量
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="so3Mat"></param>
        /// <returns></returns>
        public static Vector<double> so3ToVector(this VectorBuilder<double> builder, Matrix<double> so3Mat)
        {
            return builder.Dense([so3Mat[2, 1], so3Mat[0, 2], so3Mat[1, 0]]);
        }
        /// <summary>
        /// 将指定6x1旋量向量转换为se3矩阵
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="screw"></param>
        /// <returns></returns>
        public static Matrix<double> VectorTose3(this MatrixBuilder<double> builder, Vector<double> screw)
        {
            Vector<double> S = screw.SubVector(0, 3);
            Vector<double> S0 = screw.SubVector(3, 3);
            Matrix<double> result = builder.Dense(4, 4);
            Matrix<double> so3 = builder.Vector2so3(S);
            result.SetSubMatrix(0, 0, so3);
            Matrix<double> linear = builder.DenseOfColumnArrays([[S0[0], S0[1], S0[2]]]);
            result.SetSubMatrix(0, 3, linear);
            return result;
        }
        /// <summary>
        /// 将齐次矩阵转换为6x1向量
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="T"></param>
        /// <returns></returns>
        public static Vector<double> se3ToVector(this VectorBuilder<double> builder, Matrix<double> T)
        {
            return builder.Dense([T[2, 1], T[0, 2], T[1, 0], T[0, 3], T[1, 3], T[2, 3]]);
        }
        /// <summary>
        /// 计算反对称矩阵的指数积
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="so3Mat"></param>
        /// <returns></returns>
        public static Matrix<double> MatrixExp3(this MatrixBuilder<double> builder, Matrix<double> so3Mat, double theta)
        {
            VectorBuilder<double> V = Vector<double>.Build;
            Vector<double> omgtheta = V.so3ToVector(so3Mat);
            Matrix<double> m_ret = builder.DenseIdentity(3, 3);
            if(Math.Abs(omgtheta.L2Norm()) < double.Epsilon) return m_ret;
            else
            {
                Matrix<double> omgmat = so3Mat;
                var result = m_ret + (Math.Sin(theta) * omgmat) + (((1 - Math.Cos(theta)) * (omgmat * omgmat)));
                return result;
            }
        }
        /// <summary>
        /// 计算旋转矩阵的矩阵对数
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="R"></param>
        /// <param name="theta"></param>
        /// <returns></returns>
        public static Matrix<double> MatrixLog3(this MatrixBuilder<double> builder, Matrix<double> R, out double theta)
        {
            VectorBuilder<double> V = Vector<double>.Build;
            double acosinput = (R.Trace() - 1) * 0.5;
            Matrix<double> m_met = builder.Dense(3, 3);
            if(acosinput >= 1)
            {
                theta = 0;
                return m_met;
            }
            else if (acosinput <= -1)
            {
                Vector<double> omg;
                theta = Math.PI;
                if (Math.Abs(1 + R[2, 2]) >= double.Epsilon)
                    omg = (1.0 / Math.Sqrt(2 * (1 + R[2, 2]))) * V.Dense([R[0, 2], R[1, 2], 1 + R[2, 2]]);
                else if (Math.Abs(1 + R[1, 1]) >= double.Epsilon)
                    omg = (1.0 / Math.Sqrt(2 * (1 + R[1, 1]))) * V.Dense([R[0, 1], 1 + R[1, 1], R[2, 1]]);
                else
                    omg = (1.0 / Math.Sqrt(2 * (1 + R[0, 0]))) * V.Dense([1 + R[0, 0], R[1, 0], R[2, 0]]);
                m_met = builder.Vector2so3(theta * omg);
                return m_met;
            }
            else
            {
                theta = Math.Acos(acosinput);
                m_met = theta / 2.0 / Math.Sin(theta) * (R - R.Transpose());
                return m_met;
            }
        }
        /// <summary>
        /// 获取指定齐次矩阵表示的旋量指数积
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="se3Mat"></param>
        /// <returns></returns>
        public static Matrix<double> MatrixExp6(this MatrixBuilder<double> builder, Matrix<double> se3Mat, double theta)
        {
            VectorBuilder<double> V = Vector<double>.Build;
            Matrix<double> se3mat_cut = se3Mat.SubMatrix(0, 3, 0, 3);
            Vector<double> omgtheta = V.so3ToVector(se3Mat);
            Matrix<double> result = builder.DenseIdentity(4, 4);
            if(Math.Abs(omgtheta.L2Norm()) < double.Epsilon)
            {
                se3mat_cut = builder.DenseIdentity(3, 3);
                omgtheta = V.Dense([se3Mat[0, 3], se3Mat[1, 3], se3Mat[2, 3]]);
                result.SetSubMatrix(0, 0, se3mat_cut);
                result.SetColumn(3, 0, 3, omgtheta * theta);
            }
            else
            {
                Matrix<double> omgmat = se3Mat.SubMatrix(0, 3, 0, 3);
                Matrix<double> expExpand = (builder.DenseIdentity(3, 3) * theta) + ((1 - Math.Cos(theta)) * omgmat) + ((theta - Math.Sin(theta)) * (omgmat * omgmat));
                Vector<double> linear = V.Dense([se3Mat[0, 3], se3Mat[1, 3], se3Mat[2, 3]]);
                Vector<double> GThetaV = expExpand * linear;
                result.SetSubMatrix(0, 0, builder.MatrixExp3(se3mat_cut, theta));
                result.SetColumn(3, 0, 3, GThetaV);
            }
            return result;
        }
        /// <summary>
        /// 获取指定齐次矩阵的矩阵对数
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="T"></param>
        /// <param name="theta"></param>
        /// <returns></returns>
        public static Matrix<double> MatrixLog6(this MatrixBuilder<double> builder, Matrix<double> T, out double theta)
        {
            VectorBuilder<double> V = Vector<double>.Build;
            Matrix<double> m_ret = builder.Dense(4, 4);
            Matrix<double> R = T.SubMatrix(0, 3, 0, 3);
            Matrix<double> omgmat = builder.MatrixLog3(R, out theta);
            Vector<double> trans = T.Column(3).SubVector(0, 3);
            Matrix<double> zeros3d = builder.Dense(3, 3);
            if (Math.Abs(omgmat.FrobeniusNorm()) < double.Epsilon)
            {
                m_ret.SetSubMatrix(0, 0, zeros3d);
                m_ret.SetColumn(3, 0, 3, trans);
            }
            else
            {
                Matrix<double> logExpand1 = builder.DenseIdentity(3, 3) - omgmat / 2.0;
                Matrix<double> logExpand2 = (1.0 / theta - 1.0 / Math.Tan(theta / 2.0) / 2) * omgmat * omgmat / theta;
                Matrix<double> logExpand = logExpand1 + logExpand2;
                m_ret.SetSubMatrix(0, 0, omgmat);
                m_ret.SetColumn(3, 0, 3, logExpand * trans);
            }
            return m_ret;
        }
        /// <summary>
        /// 将向量转换为单位向量与旋转角的组合向量
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="vec"></param>
        /// <returns></returns>
        public static Vector<double> AxisAng3(this VectorBuilder<double> builder, Vector<double> vec)
        {
            Vector<double> axis = vec.Normalize(2);
            double theta = vec.L2Norm();
            return builder.Dense([axis[0], axis[1], axis[2], theta]);
        }
        /// <summary>
        /// 获取指定齐次矩阵的伴随矩阵
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="T"></param>
        /// <returns></returns>
        public static Matrix<double> Adjoint(this MatrixBuilder<double> builder, Matrix<double> T) 
        {
            Matrix<double> R = T.SubMatrix(0, 3, 0, 3);
            Vector<double> trans = T.Column(3).SubVector(0, 3);
            Matrix<double> ad_ret = builder.Dense(6, 6);
            Matrix<double> zeroes = builder.Dense(3, 3);
            ad_ret.SetSubMatrix(0, 3, 0, 3, R);
            ad_ret.SetSubMatrix(0, 3, 3, 3, zeroes);
            ad_ret.SetSubMatrix(3, 3, 0, 3, builder.Vector2so3(trans) * R);
            ad_ret.SetSubMatrix(3, 3, 3, 3, R);
            return ad_ret;
        }
        /// <summary>
        /// 计算给定初始旋量与旋转角列表的空间雅可比矩阵
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="sList"></param>
        /// <param name="thetaList"></param>
        /// <returns></returns>
        public static Matrix<double> JacobianSpace(this MatrixBuilder<double> builder, List<Vector<double>> sList, List<double> thetaList)
        {
            VectorBuilder<double> V = Vector<double>.Build;
            int min = Math.Min(sList.Count, thetaList.Count);
            Matrix<double> Js = builder.Dense(6, min);
            Matrix<double> T = builder.DenseIdentity(4, 4);
            Vector<double> sListTemp = V.Dense(sList[0].Count);
            for(int i = 0; i < min; i++)
            {
                Js.SetColumn(i, sList[i]);
            }
            for(int i = 1; i < min; i++)
            {
                sListTemp = sList[i - 1];
                T = T * builder.MatrixExp6(builder.VectorTose3(sListTemp), thetaList[i - 1]);
                Js.SetColumn(i, builder.Adjoint(T) * sList[i]);
            }
            return Js;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns></returns>
        public static System.Numerics.Matrix4x4 ToMatrix4x4(this Matrix<double> matrix)
        {
            var trans = matrix.Transpose();
            return new System.Numerics.Matrix4x4()
            {
                M11 = (float)trans[0, 0],
                M12 = (float)trans[0, 1],
                M13 = (float)trans[0, 2],
                M14 = (float)trans[0, 3],
                M21 = (float)trans[1, 0],
                M22 = (float)trans[1, 1],
                M23 = (float)trans[1, 2],
                M24 = (float)trans[1, 3],
                M31 = (float)trans[2, 0],
                M32 = (float)trans[2, 1],
                M33 = (float)trans[2, 2],
                M34 = (float)trans[2, 3],
                M41 = (float)trans[3, 0],
                M42 = (float)trans[3, 1],
                M43 = (float)trans[3, 2],
                M44 = (float)trans[3, 3]
            };
        }

        public static Matrix<double> ToMatrix(this System.Numerics.Matrix4x4 trans)
        {
            var M = Matrix<double>.Build;
            trans = System.Numerics.Matrix4x4.Transpose(trans);
            return M.DenseOfRowArrays([[trans.M11, trans.M12, trans.M13, trans.M14],
                [trans.M21, trans.M22, trans.M23, trans.M24],
                [trans.M31, trans.M32, trans.M33, trans.M34],
                [trans.M41, trans.M42, trans.M43, trans.M44]]);
        }
    }
}
