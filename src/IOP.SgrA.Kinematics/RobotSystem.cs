﻿using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Factorization;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Kinematics
{
    /// <summary>
    /// 
    /// </summary>
    public class RobotSystem
    {
        private static MatrixBuilder<double> VM = Matrix<double>.Build;
        private static VectorBuilder<double> V = Vector<double>.Build;

        /// <summary>
        /// 当Theta值发生变更时
        /// </summary>
        public event Action<RobotSystem, IDictionary<string, double>>? OnThetasChanged;
        /// <summary>
        /// 
        /// </summary>
        public event Action<RobotSystem, IDictionary<string, double>>? OnThetasChanging;

        /// <summary>
        /// 
        /// </summary>
        public string? Name {  get; set; }

        /// <summary>
        /// 角度公差值
        /// </summary>
        public double EomgEpsilon { get; set; } = 0.0001;
        /// <summary>
        /// 坐标公差值
        /// </summary>
        public double EvEpsilon { get; set; } = 0.0001;

        /// <summary>
        /// 当前系统的所有连杆
        /// </summary>
        public readonly Dictionary<string, Link> Links = new();
        /// <summary>
        /// 当前系统的所有轴
        /// </summary>
        public readonly Dictionary<string, Joint> Joints = new();
        /// <summary>
        /// 末端执行器字典
        /// </summary>
        public readonly Dictionary<string, Link> EndLinks = new();

        /// <summary>
        /// 更新指定轴的轴值
        /// </summary>
        /// <param name="joint"></param>
        /// <param name="theta"></param>
        public void UpdateJointTheta(string endJoint, double theta)
        {
            if (string.IsNullOrEmpty(endJoint)) throw new ArgumentNullException(nameof(endJoint));
            List<double> thetaList = new List<double>();
            if (Joints.TryGetValue(endJoint, out Joint? j))
            {
                j.CurrentTheta = theta;
                RefreshRobotState(j.Name);
                OnThetasChanging?.Invoke(this, new Dictionary<string, double>() { { j.Name, j.CurrentTheta } });
            }
            else throw new InvalidOperationException($"Cannot found joint name {endJoint}");
        }

        /// <summary>
        /// 估算IK迭代次数
        /// </summary>
        /// <param name="epsilon"></param>
        /// <param name="jointCount"></param>
        /// <returns></returns>
        public int EstimateIterations(double epsilon, int jointCount)
        {
            epsilon = Math.Max(epsilon, 0);
            epsilon = Math.Min(epsilon, 0.1);
            jointCount = Math.Max(jointCount, 1);
            double x = Math.Abs(Math.Log(epsilon, 10));
            double log = Math.Log(x, double.E);
            double b = Math.Pow(jointCount, log);
            return (int)(x * b * 2);
        }

        /// <summary>
        /// 计算指定连杆和轴值列表的空间正向运动学姿态
        /// 仅计算，不更新机器人状态
        /// </summary>
        /// <param name="endJoint">终结点轴</param>
        /// <param name="thetaList">
        /// 轴值列表
        /// </param>
        /// <returns></returns>
        public Matrix<double> FKinSpace(string endJoint, List<double> thetaList)
        {
            if (string.IsNullOrEmpty(endJoint)) throw new ArgumentNullException(nameof(endJoint));
            if (thetaList == null) throw new ArgumentNullException(nameof(thetaList));
            if (Joints.TryGetValue(endJoint, out Joint? j))
            {
                Link link = j.NextLink;
                List<Joint> joints = new List<Joint>(link!.EnumerablePreviewJoints());
                joints.Reverse();
                int diff = joints.Count - thetaList.Count;
                if(diff > 0)
                {
                    for(int i = diff - 1; i >= 0; i--)
                    {
                        thetaList.Insert(0, joints[i].CurrentTheta);
                    }
                }
                Matrix<double> T = link.M;
                int min = Math.Min(joints.Count, thetaList.Count);
                for(int i = min - 1; i >= 0; i--)
                {
                    double theta = thetaList[i];
                    Joint joint = joints[i];
                    Screw s = joint.VS;
                    Matrix<double> se3 = s.Tose3();
                    T = VM.MatrixExp6(se3, theta) * T;
                }
                return T;
            }
            else throw new InvalidOperationException($"Cannot found joint name {endJoint}");
        }

        /// <summary>
        /// 通过指定轴与位置计算反向运动学
        /// </summary>
        /// <param name="endJoint"></param>
        /// <param name="T"></param>
        /// <param name="thetaList"></param>
        /// <param name="maxiterations"></param>
        /// <param name="eomg"></param>
        /// <param name="ev"></param>
        /// <returns></returns>
        public IKResult IKinSpace(string endJoint, Matrix<double> T, out List<double> thetaList, int maxiterations = 20, double eomg = 0.0, double ev = 0.0)
        {
            if (string.IsNullOrEmpty(endJoint)) throw new ArgumentNullException(nameof(endJoint));
            if (T == null || T.RowCount != 4 || T.ColumnCount != 4) throw new InvalidDataException("Invalid input T matrix");
            thetaList = new List<double>();
            if (Joints.TryGetValue(endJoint, out Joint? j))
            {
                List<double> initTheta = new List<double>();
                List<Vector<double>> sList = new List<Vector<double>>();
                List<(double, double)> minMax = new();
                Link nextLink = j.NextLink;
                IEnumerable<Joint> joints = nextLink.EnumerablePreviewJoints().Reverse();
                foreach (var joint in joints)
                {
                    initTheta.Add(joint.CurrentTheta);
                    sList.Add(joint.VS.ToVector());
                    minMax.Add((joint.MinTheta, joint.MaxTheta));
                }
                Vector<double> thetas = V.Dense([.. initTheta]);
                thetaList.AddRange(initTheta);
                Matrix<double> M = nextLink.M;
                Matrix<double> Tfk = FKinSpace(endJoint, initTheta);
                Matrix<double> Tdiff = VM.InverseSE3(Tfk) * T;
                Vector<double> Vs = VM.Adjoint(Tfk) * V.se3ToVector(VM.MatrixLog6(Tdiff, out _));
                Vector<double> angular = V.Dense([Vs[0], Vs[1], Vs[2]]);
                Vector<double> linear = V.Dense([Vs[3], Vs[4], Vs[5]]);
                int i = 0;

                bool err = (angular.L2Norm() > eomg || linear.L2Norm() > ev);
                while(err && i < maxiterations)
                {
                    var Js = VM.JacobianSpace(sList, thetaList);
                    thetas += Js.PseudoInverse() * Vs;
                    for(int n = 0; n < thetas.Count; n++)
                    {
                        double t = thetas[n];
                        t = Math.Max(t, minMax[n].Item1);
                        t = Math.Min(t, minMax[n].Item2);
                        thetas[n] = t;
                    }
                    i += 1;

                    thetaList = [.. thetas];
                    Tfk = FKinSpace(endJoint, thetaList);
                    Tdiff = VM.InverseSE3(Tfk) * T;
                    Vs = VM.Adjoint(Tfk) * V.se3ToVector(VM.MatrixLog6(Tdiff, out _));
                    angular = V.Dense([Vs[0], Vs[1], Vs[2]]);
                    linear = V.Dense([Vs[3], Vs[4], Vs[5]]);
                    err = (angular.L2Norm() > eomg || linear.L2Norm() > ev);
                }
                if (err) return IKResult.Unsolvable;
                else return IKResult.Success;
            }
            else throw new InvalidOperationException($"Cannot found joint name {endJoint}");
        }

        /// <summary>
        /// 刷新机器人指定轴相关所有机构的状态
        /// </summary>
        /// <param name="joint"></param>
        public void RefreshRobotState(string endJoint)
        {
            if (string.IsNullOrEmpty(endJoint)) throw new ArgumentNullException(nameof(endJoint));
            if (Joints.TryGetValue(endJoint, out Joint? joint))
            {
                Link link = joint.NextLink;
                link = link.GetRootLink();
                List<Joint> joints = new List<Joint>(link.EnumerableNextJoints());
                joints.Reverse();
                for(int i = 0; i < joints.Count; i++)
                {
                    Joint local = joints[i];
                    Link endLink = local.NextLink;
                    Matrix<double> T = local.CurrentExpS * endLink.M;
                    int j = i + 1;
                    while(j < joints.Count)
                    {
                        Joint nextJoint = joints[j];
                        T = nextJoint.CurrentExpS * T;
                        j++;
                    }
                    endLink.CurrentM = T;
                }
            }
            else throw new InvalidOperationException($"Cannot found joint name {endJoint}");
        }
        /// <summary>
        /// 执行反向运动学并更新
        /// </summary>
        /// <param name="endJoint"></param>
        /// <param name="position"></param>
        /// <param name="thetaList"></param>
        /// <returns></returns>
        /// <exception cref="InvalidDataException"></exception>
        public IKResult IKinAndUpdate(string endJoint, Vector<double> position, out List<double> thetaList)
        {
            if (position.Count != 3) throw new InvalidDataException("Invalid input position");
            Matrix<double> T = VM.DenseIdentity(4, 4);
            T.SetColumn(3, 0, 3, position);
            return IKinAndUpdate(endJoint, T, out thetaList);
        }
        /// <summary>
        /// 执行反向运动学并更新状态
        /// </summary>
        /// <param name="endJoint"></param>
        /// <param name="T"></param>
        /// <param name="thetas"></param>
        /// <returns></returns>
        public IKResult IKinAndUpdate(string endJoint, Matrix<double> T, out List<double> thetaList)
        {
            if (string.IsNullOrEmpty(endJoint)) throw new ArgumentNullException(nameof(endJoint));
            if (T == null || T.RowCount != 4 || T.ColumnCount != 4) throw new InvalidDataException("Invalid input T matrix");
            thetaList = new List<double>();
            if (Joints.TryGetValue(endJoint, out Joint? j))
            {
                Link nextLink = j.NextLink;
                List<Joint> joints = nextLink.EnumerablePreviewJoints().Reverse().ToList();
                int iterCount = EstimateIterations(EomgEpsilon, joints.Count());
                var ikResult = IKinSpace(endJoint, T, out thetaList, iterCount, EomgEpsilon, EvEpsilon);
                if(ikResult == IKResult.Success)
                {
                    Dictionary<string, double> param = new();
                    for(int i = 0; i < joints.Count; i++)
                    {
                        joints[i].CurrentTheta = thetaList[i];
                        param.TryAdd(joints[i].Name, thetaList[i]);
                    }
                    RefreshRobotState(endJoint);
                    OnThetasChanged?.Invoke(this, param);
                    return ikResult;
                }
                else return ikResult;
            }
            else throw new InvalidOperationException($"Cannot found joint name {endJoint}");
        }

        /// <summary>
        /// 计算三次时间标度s(t)
        /// </summary>
        /// <param name="Tf"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        public double CubicTimeScaling(double Tf, double t)
        {
            double timeratio = 1.0 * t / Tf;
            double st = 3 * Math.Pow(timeratio, 2) - 2 * Math.Pow(timeratio, 3);
            return st;
        }
        /// <summary>
        /// 计算五次时间标度s(t)
        /// </summary>
        /// <param name="Tf"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        public double QuinticTimeScaling(double Tf, double t) 
        {
            double timeratio = 1.0 * t / Tf;
            double st = 10 * Math.Pow(timeratio, 3) - 15 * Math.Pow(timeratio, 4) + 6 * Math.Pow(timeratio, 5);
            return st;
        }

        /// <summary>
        /// 计算关节直线轨迹
        /// 仅计算，不刷新状态
        /// </summary>
        /// <param name="thetaStart"></param>
        /// <param name="thetaEnd"></param>
        /// <param name="Tf">总片段</param>
        /// <param name="gap">时间片</param>
        /// <param name="trajs"></param>
        /// <returns></returns>
        public PlanningResult JointTrajectory(string endJoint, List<double> thetaStart, List<double> thetaEnd, double Tf, double gap, out ThetaPlanningData trajs)
        {
            if (string.IsNullOrEmpty(endJoint)) throw new ArgumentNullException(nameof(endJoint));
            if (gap > Tf) throw new IndexOutOfRangeException("tiem span was out of total time");
            if (Joints.TryGetValue(endJoint, out Joint? j))
            {
                Link endLink = j.NextLink;
                List<Joint> joints = endLink.EnumerablePreviewJoints().Reverse().ToList();
                FillThetaList(j, thetaStart, thetaEnd);
                trajs = new ThetaPlanningData(thetaStart, thetaEnd);
                trajs.Total = Tf;
                double currentTime = gap; double st; int count = joints.Count;
                int index = 0;
                while(currentTime < Tf)
                {
                    st = QuinticTimeScaling(Tf, currentTime);
                    List<double> frame = new(joints.Count);
                    for (int i = 0; i < count; i++)
                    {
                        double newTheta = st * thetaEnd[i] + (1 - st) * thetaStart[i];
                        if (newTheta < joints[i].MinTheta || newTheta > joints[i].MaxTheta)
                        {
                            trajs.State = PlanningResult.Unreachable;
                            return PlanningResult.Unreachable;
                        }
                        frame.Add(newTheta);
                    }
                    trajs.KeyFrames.Add(new PlanningKeyFrame<List<double>>(index, currentTime, frame));
                    index++;
                    currentTime += gap;
                }
            }
            else throw new InvalidOperationException($"Cannot found joint name {endJoint}");
            return PlanningResult.Success;
        }

        private void FillThetaList(Joint joint, params List<double>[] thetaList)
        {
            if(thetaList == null || thetaList.Length == 0) throw new ArgumentNullException(nameof(thetaList));
            Link endLink = joint.NextLink;
            List<Joint> joints = endLink.EnumerablePreviewJoints().Reverse().ToList();
            int count = joints.Count;
            List<List<double>> needUpdate = new();
            for(int i = 0; i < thetaList.Length; i++)
            {
                if (thetaList[i].Count < count) needUpdate.Add(thetaList[i]); 
            }
            if (needUpdate.Count <= 0) return;
            for(int i = 0; i < count; i++)
            {
                for(int j = 0; j < needUpdate.Count; j++)
                {
                    List<double> thetas = needUpdate[j];
                    if (thetas.Count > i) continue;
                    else thetas.Add(joints[i].CurrentTheta);
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public enum IKResult
    {
        Success = 0,
        Unsolvable = -1
    }
    /// <summary>
    /// 规划结果
    /// </summary>
    public enum PlanningResult
    {
        /// <summary>
        /// 
        /// </summary>
        Success = 0,
        /// <summary>
        /// 路径不可达
        /// </summary>
        Unreachable = -1,
    }
}
