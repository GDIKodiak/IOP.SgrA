﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// ISO10303-21文档
    /// </summary>
    public class EXPRESSDocument : IEXPRESSDocument
    {
        /// <summary>
        /// 文件起始行
        /// </summary>
        public const string FileStart = "ISO-10303-21;\r\n";
        /// <summary>
        /// 头段
        /// </summary>
        public const string HEADER = "HEADER;\r\n";
        /// <summary>
        /// 数据段
        /// </summary>
        public const string DATA = "DATA";
        /// <summary>
        /// 段结束
        /// </summary>
        public const string ENDSEC = "ENDSEC;\r\n";
        /// <summary>
        /// 文件结束行
        /// </summary>
        public const string FileEnd = "END-ISO-10303-21;\r\n";
        /// <summary>
        /// 抽象语法树字典
        /// </summary>
        private ConcurrentDictionary<int, ASTNode> ASTTree { get; set; } = new ConcurrentDictionary<int, ASTNode>();
        /// <summary>
        /// EXPRESS编解码器
        /// </summary>
        private ConcurrentDictionary<string, IExpressCodec> Codecs { get; set; } = new ConcurrentDictionary<string, IExpressCodec>();
        /// <summary>
        /// EXPRESS类型编解码器
        /// </summary>
        private ConcurrentDictionary<string, IExpressTypeCodec> TypeCodecs { get; set; } = new ConcurrentDictionary<string, IExpressTypeCodec>();
        /// <summary>
        /// 实体字典
        /// </summary>
        private ConcurrentDictionary<int, Entity> Entities { get; set; } = new ConcurrentDictionary<int, Entity>();
        /// <summary>
        /// 头部实体
        /// </summary>
        private ConcurrentDictionary<string, Entity> HeaderEntities { get; set; } = new ConcurrentDictionary<string, Entity>();
        /// <summary>
        /// 数据段参数
        /// </summary>
        private List<Parameter> DataParameters { get; set; } = new List<Parameter>();
        /// <summary>
        /// 构造函数
        /// </summary>
        public EXPRESSDocument()
        {
            this.AddSchemaAP21Codecs();
        }
        /// <summary>
        /// 获取抽象语法树节点
        /// </summary>
        /// <param name="entityName"></param>
        /// <returns></returns>
        public ASTNode GetASTNode(int entityName) => ASTTree[entityName];
        /// <summary>
        /// 尝试获取抽象语法树节点
        /// </summary>
        /// <param name="entityName"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool TryGetASTNode(int entityName, out ASTNode entity) => ASTTree.TryGetValue(entityName, out entity);
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="entityName"></param>
        /// <returns></returns>
        public Entity GetEntity(int entityName)
        {
            if(Entities.TryGetValue(entityName, out Entity entity)) return entity;
            else
            {
                entity = DecodeFromAST(entityName);
                return entity;
            }
        }
        /// <summary>
        /// 尝试获取实体
        /// </summary>
        /// <param name="entityName"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool TryGetEntity(int entityName, out Entity entity)
        {
            if (Entities.TryGetValue(entityName, out entity)) return true;
            else
            {
                if (TryDecodeFromAST(entityName, out entity)) return true;
                else return false;
            }
        }
        /// <summary>
        /// 遍历当前文档所有实体
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Entity> ForeachEntities()
        {
            foreach(var item in ASTTree.Keys)
            {
                var entity = GetEntity(item);
                yield return entity;
            }
        }
        /// <summary>
        /// 添加编解码器
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="codec"></param>
        public void AddEntityCodec(string keyword, IExpressCodec codec) => Codecs.AddOrUpdate(keyword, codec, (key, value) => codec);
        /// <summary>
        /// 添加类型编解码器
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="codec"></param>
        public void AddTypeCodec(string keyword, IExpressTypeCodec codec) => TypeCodecs.AddOrUpdate(keyword, codec, (key,value) => codec);
        /// <summary>
        /// 获取解码器
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        public IExpressCodec GetEntityCodec(string keyword)
        {
            if (string.IsNullOrEmpty(keyword)) throw new ArgumentNullException(nameof(keyword));
            return Codecs.TryGetValue(keyword, out var codec) ? codec : throw new NullReferenceException($"Cannot found Codec with keyword {keyword}");
        }
        /// <summary>
        /// 获取类型编解码器
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        public IExpressTypeCodec GetTypeCodec(string keyword)
        {
            if (string.IsNullOrEmpty(keyword)) throw new ArgumentNullException(nameof(keyword));
            return TypeCodecs.TryGetValue(keyword, out var codec) ? codec : throw new NullReferenceException($"Cannot found TypeCodec with keyword {keyword}");
        }

        /// <summary>
        /// 解析一行
        /// </summary>
        /// <param name="line"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        public ASTNode ParseLine(int line, string source)
        {
            if (!IsSpecialText(source, line))
            {
                ExpressLineParser paser = new ExpressLineParser(line, source);
                var data = paser.Parse();
                if (data is EntityInstance instance)
                {
                    ASTTree.AddOrUpdate(instance.InstanceName, instance, (key, value) => instance);
                }
                else if (data is HeaderEntity he)
                {
                    var record = he.Record;
                    if(record != null)
                    {
                        if(Codecs.TryGetValue(record.Keyword, out IExpressCodec codec))
                        {
                            var entity = codec.Decode(record, this);
                            if(entity != null)
                            {
                                HeaderEntities.AddOrUpdate(entity.Keyword, entity, (key, value) => entity);
                            }
                        }
                    }
                }
                return data;
            }
            else return null;
        }
        /// <summary>
        /// 解析多行
        /// </summary>
        /// <param name="startLine"></param>
        /// <param name="sources"></param>
        public void ParseLines(int startLine, params string[] sources)
        {
            if (sources == null || sources.Length <= 0) return;
            for(int i = 0; i < sources.Length; i++)
            {
                string local = sources[i];
                if (string.IsNullOrEmpty(local)) continue;
                ParseLine(startLine + i, local);
            }
        }
        /// <summary>
        /// 从抽象语法树解析实体
        /// </summary>
        /// <param name="entityName"></param>
        /// <returns></returns>
        public Entity DecodeFromAST(int entityName)
        {
            ASTNode node = GetASTNode(entityName);
            if(node is EntityInstance instance)
            {
                if (instance.Record is SimpleRecord record)
                {
                    IExpressCodec codec = GetEntityCodec(record.Keyword);
                    var entity = codec.Decode(record, this);
                    Entities.AddOrUpdate(entityName, entity, (key, value) => entity);
                    return entity;
                }
                else if (instance.Record is SubsuperRecord subsuperRecord)
                {
                    IExpressCodec codec = GetEntityCodec(SchemaAP21.SUBSUPERRECORD);
                    var entity = codec.Decode(subsuperRecord, this);
                    Entities.AddOrUpdate(entityName, entity, (key, value) => entity);
                    return entity;
                }
                else throw new NotSupportedException("Unknown instance");
            }
            else throw new NotSupportedException("Unknown instance");
        }
        /// <summary>
        /// 尝试从抽象语法树解码实体
        /// </summary>
        /// <param name="entityName"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool TryDecodeFromAST(int entityName, out Entity entity)
        {
            try
            {
                entity = DecodeFromAST(entityName);
                return true;
            }
            catch (Exception)
            {
                entity = null;
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private bool IsSpecialText(string source, int line)
        {
            if (string.IsNullOrEmpty(source)) return true;
            else if (source == FileStart) return true;
            else if (source == HEADER) return true;
            else if (source == ENDSEC) 
                return true;
            else if (source == FileEnd) 
                return true;
            else if (source.Contains(DATA))
            {
                if (source == "DATA;\r\n") return true;
                else
                {
                    ExpressLineParser paser = new ExpressLineParser(line, source);
                    var data = paser.Parse();
                    if (data is HeaderEntity he)
                    {
                        DataParameters.AddRange(he.Record.Parameters);
                    }
                    return true;
                }
            }
            else return false;
        }
    }
}
