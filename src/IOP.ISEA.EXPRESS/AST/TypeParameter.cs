﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// 类型参数
    /// </summary>
    public class TypeParameter : Parameter
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public string Keyword { get; protected internal set; }
        /// <summary>
        /// 参数
        /// </summary>
        public Parameter Parameter { get; protected internal set; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="line"></param>
        /// <param name="position"></param>
        /// <param name="length"></param>
        public TypeParameter(int line, int position, int length, string keyword, Parameter parameter) 
            : base(ExpressTokenType.TypeParameter, line, position, length, nameof(TypeParameter))
        {
            Keyword = keyword;
            Parameter = parameter;
        }
    }
}
