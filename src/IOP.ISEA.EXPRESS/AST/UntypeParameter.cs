﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// 非类型参数
    /// </summary>
    public class UntypeParameter : Parameter
    {
        /// <summary>
        /// 参数类型
        /// </summary>
        public ExpressTokenType ParameterType { get; protected set; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="type"></param>
        /// <param name="line"></param>
        /// <param name="position"></param>
        /// <param name="length"></param>
        public UntypeParameter(int line, int position, int length, object parameter, ExpressTokenType paramType) 
            : base(ExpressTokenType.UntypeParameter, line, position, length, nameof(UntypeParameter))
        {
            ParameterValue = parameter;
            ParameterType = paramType;
        }
    }
}
