﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// 实体解析器异常
    /// </summary>
    public class EntityParserException : Exception
    {
        /// <summary>
        /// 函数
        /// </summary>
        public int Line { get; set; }
        /// <summary>
        /// 位置
        /// </summary>
        public int Position { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Length { get; set; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="line"></param>
        /// <param name="position"></param>
        /// <param name="message"></param>
        public EntityParserException(int line, int position, int length, string message)
            :base(message)
        {
            Line = line;
            Position = position;
            Length = length;
        }
    }
}
