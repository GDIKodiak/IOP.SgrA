﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Linq;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// Express解释器
    /// </summary>
    public struct ExpressLineParser
    {
        /// <summary>
        /// 起始行
        /// </summary>
        public int StartLine { get; }
        /// <summary>
        /// 源
        /// </summary>
        private string Source;
        /// <summary>
        /// 当前令牌
        /// </summary>
        private ExpressTokenType CurrentToken;
        /// <summary>
        /// 预测栈
        /// </summary>
        private readonly Stack<ParserToken> Forecast;
        /// <summary>
        /// 令牌栈
        /// </summary>
        private readonly Stack<ParserToken> TokenStack;
        /// <summary>
        /// 状态栈
        /// </summary>
        private readonly Stack<int> StateStack;
        /// <summary>
        /// ACTION跳表
        /// </summary>
        private readonly static Action<ExpressLineParser>[,] ACTIONTable = new Action<ExpressLineParser>[38, 15]
        {
            { M01, M02, E04, E04, E04, E04, E04, E04, E04, E04, E04, E04, E04, E04, E06 },
            { E01, E01, E01, E01, E01, E01, M05, E01, E01, E01, E01, E01, E01, E01, E06 },
            { E01, E01, M03, E02, E01, E03, E01, E01, E01, E01, E01, E01, E01, E01, E06 },
            { E01, E01, E01, E01, E01, R01, E01, E01, E01, E01, E01, E01, E01, E01, E06 },
            { E01, E01, E01, E01, E01, M06, E01, E01, E01, E01, E01, E01, E01, E01, E06 },
            { M08, M13, M09, M09, M09, E01, M15, M07, E01, E01, M17, M09, M09, M09, E06 },
            { M01, E01, E01, E01, E01, E01, M18, E01, E01, E01, E01, E01, E01, E01, E06 },
            { R02, E01, E01, E01, E01, E01, E01, R02, E01, R02, E01, E01, E01, E01, E06 },
            { E01, E01, E01, E01, E01, E01, M16, E01, E01, E01, E01, E01, E01, E01, E06 },
            { E01, E01, E01, E01, E01, E01, E01, R09, R09, E01, E01, E01, E01, E01, E06 },
            { E01, E01, E01, E01, E01, E01, E01, R10, R10, E01, E01, E01, E01, E01, E06 },
            { E01, E01, E01, E01, E01, E01, E01, R11, M12, E01, E01, E01, E01, E01, E06 },
            { M08, M13, M09, M09, M09, E01, M15, M27, E01, E01, M17, M09, M09, M09, E06 },
            { E01, E01, M14, E01, E01, E01, E01, E01, E01, E01, E01, E01, E01, E01, E06 },
            { E01, E01, E01, E01, E01, E01, E01, R01, R01, E01, E01, E01, E01, E01, E06 },
            { M08, M13, M09, M09, M09, E01, M15, M27, E01, E01, M17, M09, M09, M09, E06 },
            { M08, M13, M09, M09, M09, E01, M15, E10, E01, E01, M17, M09, M09, M09, E06 },
            { E01, E01, E01, E01, E01, E01, E01, R15, R15, E01, E01, E01, E01, E01, E06 },
            { M01, E01, E01, E01, E01, E01, E01, M25, E01, E01, E01, E01, E01, E01, E06 },
            { E01, E01, E01, E01, E01, E01, E01, E01, E01, R03, E01, E01, E01, E01, E06 },
            { E06, E06, E06, E06, E06, E06, E06, E06, E06, M21, E06, E06, E06, E06, E06 },
            { E06, E06, E06, E06, E06, E06, E06, E06, E06, E06, E06, E06, E06, E06, R04 },
            { E06, E06, E06, E06, E06, E06, E06, E06, E06, E06, E06, E06, E06, E06, R05 },
            { E01, E01, E01, E01, E01, E01, E01, E01, E01, M24, E01, E01, E01, E01, E06 },
            { E06, E06, E06, E06, E06, E06, E06, E06, E06, E06, E06, E06, E06, E06, R06 },
            { E06, E06, E06, E06, E06, E06, E06, E06, E06, R07, E06, E06, E06, E06, E06 },
            { E01, E01, E01, E01, E01, E01, E01, M28, E01, E01, E01, E01, E01, E01, E06 },
            { E01, E01, E01, E01, E01, E01, E01, R08, R08, E01, E01, E01, E01, E01, E06 },
            { R12, E01, E01, E01, E01, E01, E01, R12, E01, R12, E01, E01, E01, E01, E06 },
            { E01, E01, E01, E01, E01, E01, E01, M31, E01, E01, E01, E01, E01, E01, E06 },
            { E01, E01, E01, E01, E01, E01, E01, R13, E01, E01, E01, E01, E01, E01, E06 },
            { E01, E01, E01, E01, E01, E01, E01, R14, R14, E01, E01, E01, E01, E01, E06 },
            { E01, E01, E01, E01, E01, E01, E01, M34, E01, E01, E01, E01, E01, E01, E06 },
            { M01, E01, E01, E01, E01, E01, E01, R16, E01, E01, E01, E01, E01, E01, E06 },
            { E01, E01, E01, E01, E01, E01, E01, E01, E01, R17, E01, E01, E01, E01, E06 },
            { E01, E01, E01, E01, E01, E01, E01, R18, E01, E01, E01, E01, E01, E01, E06 },
            { E01, E01, E01, E01, E01, E01, E01, M37, E01, E01, E01, E01, E01, E01, E06 },
            { E01, E01, E01, E01, E01, E01, E01, R19, R19, E01, E01, E01, E01, E01, E06 },
        };
        /// <summary>
        /// GOTO跳表
        /// </summary>
        private readonly static int[,] GOTOTable = new int[38, 14]
        {
            { -1,22,22,-1,23,-1,-1,04,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,09,26,11,10,10,10,09 },
            { -1,-1,-1,20,19,19,-1,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,09,30,11,10,10,10,09 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,09,29,11,10,10,10,09 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,36,10,10,10,09 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,33,-1,32,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,33,-1,35,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },
            { -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },
        };
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="startLine"></param>
        /// <param name="sourceText"></param>
        public ExpressLineParser(int startLine, string sourceText)
        {
            if (string.IsNullOrEmpty(sourceText)) throw new ArgumentNullException(nameof(sourceText));
            Source = sourceText;
            StartLine = startLine;
            CurrentToken = ExpressTokenType.None;
            TokenStack = new Stack<ParserToken>();
            Forecast = new Stack<ParserToken>();
            StateStack = new Stack<int>();
        }
        /// <summary>
        /// 解析
        /// </summary>
        /// <returns></returns>
        public ASTNode Parse()
        {
            ReadOnlySpan<char> chars = Source.AsSpan();
            NewToken(ExpressTokenType.None, 0, 0, 0, "NONE");
            StateStack.Push(0);
            int line = StartLine; int position = 0;
            for (int i = 0; i < chars.Length; i++)
            {
                char next = chars[i];
                if (!IsLexical(next, ref line, ref position))
                {
                    LR1();
                }
            }
            NewToken(ExpressTokenType.End, line, position, 0, "END");
            LR1();
            while (TokenStack.TryPop(out ParserToken token))
            {
                if (token.TokenType == ExpressTokenType.Entity)
                {
                    if (token is ASTNode sourceEntity)
                    {
                        return sourceEntity;
                    }
                }
            }
            throw new InvalidCastException("Parse failed, Entity lost");
        }

        /// <summary>
        /// LR1
        /// </summary>
        /// <param name="next"></param>
        /// <param name="line"></param>
        /// <param name="positon"></param>
        private void LR1()
        {
            if(StateStack.TryPeek(out int state))
            {
                if(Forecast.TryPeek(out ParserToken token))
                {
                    int col = GetTerminatorInACTION(token);
                    Action<ExpressLineParser> action = ACTIONTable[state, col];
                    action(this);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void GOTO()
        {
            if (StateStack.TryPeek(out int state))
            {
                if(TokenStack.TryPeek(out ParserToken token))
                {
                    int col = GetNonTerminatorInGOTO(token);
                    int s = GOTOTable[state, col];
                    if (s < 0) throw new EntityParserException(token.Line, token.Position, token.Length, "Jump GOTO Failed");
                    else
                    {
                        StateStack.Push(s);
                        LR1();
                    }
                }
            }
        }
        /// <summary>
        /// 新的令牌
        /// </summary>
        /// <param name="type"></param>
        /// <param name="line"></param>
        /// <param name="position"></param>
        /// <param name="length"></param>
        /// <param name="value"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void NewToken(ExpressTokenType type, int line, int position, int length, object value)
        {
            ParserToken token = new ParserToken(type, line, position, length, value);
            Forecast.Push(token);
            CurrentToken = type;
        }

        #region 词法分析
        /// <summary>
        /// 是否为词法
        /// </summary>
        /// <param name="next"></param>
        /// <param name="line"></param>
        /// <param name="position"></param>
        /// <returns></returns>
        private bool IsLexical(char next, ref int line, ref int position)
        {
            if (CurrentToken == ExpressTokenType.Char)
            {
                if (IsSingeQuotes(next))
                {
                    LexicalString(next, ref line, ref position);
                    position++;
                    return false;
                }
                else
                {
                    NewToken(ExpressTokenType.Char, line, position, 1, next);
                    position++;
                    return true;
                }
            }
            else if (CurrentToken == ExpressTokenType.SingeQuotes)
            {
                if (IsSingeQuotes(next))
                {
                    LexicalString(next, ref line, ref position);
                    position++;
                    return false;
                }
                else
                {
                    NewToken(ExpressTokenType.Char, line, position, 1, next);
                    position++;
                    return true;
                }
            }
            else if (CurrentToken == ExpressTokenType.Spot)
            {
                if (IsSpot(next))
                {
                    LexicalEnumeration(next, ref line, ref position);
                    position++;
                    return false;
                }
                else if(IsUpper(next) || IsDigit(next))
                {
                    NewToken(ExpressTokenType.EnumerationChar, line, position, 1, next);
                    position++;
                    return true;
                }
                else throw new EntityParserException(line, position, 1, "Enumeration char must be upper or digit");
            }
            else if (CurrentToken == ExpressTokenType.DoubeQuotes)
            {
                if (IsDoubeQuotes(next))
                {
                    LexicalBinary(next, ref line, ref position);
                    position++;
                    return false;
                }
                else if(IsHex(next))
                {
                    NewToken(ExpressTokenType.BinaryChar, line, position, 1, next);
                    position++;
                    return true;
                }
                else throw new EntityParserException(line, position, 1, "Binary char must be hex char");
            }
            else if (CurrentToken == ExpressTokenType.Digit)
            {
                if (IsDigit(next) || IsE(next) || IsSpot(next) || IsSign(next))
                {
                    NewToken(ExpressTokenType.Digit, line, position, 1, next);
                    position++;
                    return true;
                }
                else
                {
                    LexicalNumber(next, ref line, ref position);
                    LR1();
                    return IsLexical(next, ref line, ref position);
                }
            }
            else if (CurrentToken == ExpressTokenType.KeywordChar)
            {
                if (IsUpper(next) || IsDigit(next))
                {
                    NewToken(ExpressTokenType.KeywordChar, line, position, 1, next);
                    position++;
                    return true;
                }
                else
                {
                    LexicalKeyword(next, ref line, ref position);
                    LR1();
                    return IsLexical(next, ref line, ref position);
                }
            }
            else if (CurrentToken == ExpressTokenType.EnumerationChar)
            {
                if (IsUpper(next) || IsDigit(next))
                {
                    NewToken(ExpressTokenType.EnumerationChar, line, position, 1, next);
                    position++;
                    return true;
                }
                else if(IsSpot(next))
                {
                    LexicalEnumeration(next, ref line, ref position);
                    position++;
                    return false;
                }
                else throw new EntityParserException(line, position, 1, "Enumeration char must be upper or digit");
            }
            else if (CurrentToken == ExpressTokenType.BinaryChar)
            {
                if (IsHex(next))
                {
                    NewToken(ExpressTokenType.BinaryChar, line, position, 1, next);
                    position++;
                    return true;
                }
                else if (IsDoubeQuotes(next))
                {
                    LexicalBinary(next, ref line, ref position);
                    position++;
                    return false;
                }
                else throw new EntityParserException(line, position, 1, "Binary char must be hex char");
            }
            else if (IsSign(next) || IsDigit(next))
            {
                NewToken(ExpressTokenType.Digit, line, position, 1, next);
                position++;
                return true;
            }
            else if (IsSingeQuotes(next))
            {
                NewToken(ExpressTokenType.SingeQuotes, line, position, 1, next);
                position++;
                return true;
            }
            else if (IsSpot(next))
            {
                NewToken(ExpressTokenType.Spot, line, position, 1, next);
                position++;
                return true;
            }
            else if (IsDoubeQuotes(next))
            {
                NewToken(ExpressTokenType.DoubeQuotes, line, position, 1, next);
                position++;
                return true;
            }
            else if (IsUpper(next) || IsExclamatory(next))
            {
                NewToken(ExpressTokenType.KeywordChar, line, position, 1, next);
                position++;
                return true;
            }
            else if (IsPound(next))
            {
                NewToken(ExpressTokenType.Pound, line, position, 1, next);
                position++;
                return false;
            }
            else if (IsEqual(next))
            {
                NewToken(ExpressTokenType.Equal, line, position, 1, next);
                position++;
                return false;
            }
            else if (IsLeftBackst(next))
            {
                NewToken(ExpressTokenType.LeftBacksts, line, position, 1, next);
                position++;
                return false;
            }
            else if (IsRightBackst(next))
            {
                NewToken(ExpressTokenType.RightBackets, line, position, 1, next);
                position++;
                return false;
            }
            else if (IsSemicolon(next))
            {
                NewToken(ExpressTokenType.Semicolon, line, position, 1, next);
                position++;
                return false;
            }
            else if (IsNewLine(next))
            {
                Source = Source[++position..];
                position = 0;
                line++;
                return true;
            }
            else if (IsComma(next))
            {
                NewToken(ExpressTokenType.Comma, line, position, 1, next);
                position++;
                return false;
            }
            else if (IsAsterisk(next))
            {
                NewToken(ExpressTokenType.Asterisk, line, position, 1, next);
                position++;
                return false;
            }
            else if (IsDollar(next))
            {
                NewToken(ExpressTokenType.Dollar, line, position, 1, next);
                position++;
                return false;
            }
            else if (IsCR(next) || IsSpace(next))
            {
                position++;
                return true;
            }
            else return true;
        }
        /// <summary>
        /// 解析字符串
        /// </summary>
        /// <param name="next"></param>
        /// <param name="line"></param>
        /// <param name="position"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void LexicalString(char next, ref int line, ref int position)
        {
            int length = 0;
            while (Forecast.TryPeek(out ParserToken token))
            {
                if (token.TokenType == ExpressTokenType.Char)
                {
                    length += token.Length;
                    Forecast.Pop();
                }
                else if (token.TokenType == ExpressTokenType.SingeQuotes)
                {
                    string s = string.Empty;
                    if (length > 0) s = Source[(token.Position + 1)..(token.Position + 1 + length)];
                    Forecast.Pop();
                    NewToken(ExpressTokenType.String, token.Line, token.Position + 1, length, s);
                    return;
                }
                else Forecast.Pop();
            }
            throw new EntityParserException(line, position, length, "Singe quotes lost for string");
        }
        /// <summary>
        /// 解析枚举
        /// </summary>
        /// <param name="next"></param>
        /// <param name="line"></param>
        /// <param name="position"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void LexicalEnumeration(char next, ref int line, ref int position)
        {
            int length = 0;
            while (Forecast.TryPeek(out ParserToken token))
            {
                if (token.TokenType == ExpressTokenType.EnumerationChar)
                {
                    length += token.Length;
                    Forecast.Pop();
                }
                else if (token.TokenType == ExpressTokenType.Spot)
                {
                    string s = string.Empty;
                    if (length > 0) s = Source[(token.Position + 1)..(token.Position + 1 + length)];
                    Forecast.Pop();
                    NewToken(ExpressTokenType.Enumeration, token.Line, token.Position + 1, length, s);
                    return;
                }
                else Forecast.Pop();
            }
            throw new EntityParserException(line, position, length, "Spot lost for enumeration");
        }
        /// <summary>
        /// 解析二进制字符串
        /// </summary>
        /// <param name="next"></param>
        /// <param name="line"></param>
        /// <param name="position"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void LexicalBinary(char next, ref int line, ref int position)
        {
            int length = 0;
            while (Forecast.TryPeek(out ParserToken token))
            {
                if (token.TokenType == ExpressTokenType.Binary)
                {
                    length += token.Length;
                    Forecast.Pop();
                }
                else if (token.TokenType == ExpressTokenType.DoubeQuotes)
                {
                    string s = string.Empty;
                    if (length > 0) s = Source[(token.Position + 1)..(token.Position + 1 + length)];
                    Forecast.Pop();
                    NewToken(ExpressTokenType.Binary, token.Line, token.Position + 1, length, s);
                    return;
                }
                else Forecast.Pop();
            }
            throw new EntityParserException(line, position, length, "DoubeQuotes lost for enumeration");
        }
        /// <summary>
        /// 解析数字
        /// </summary>
        /// <param name="next"></param>
        /// <param name="line"></param>
        /// <param name="position"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void LexicalNumber(char next, ref int line, ref int position)
        {
            int length = 0;
            ParserToken last = null;
            while (Forecast.TryPeek(out ParserToken token))
            {
                if (token.TokenType == ExpressTokenType.Digit)
                {
                    length += token.Length;
                    last = token;
                    Forecast.Pop();
                }
                else
                {
                    if (last == null) throw new EntityParserException(token.Line, token.Position, token.Length, "Target token is not number");
                    string s = Source[last.Position..(last.Position + length)];
                    if (int.TryParse(s, out int i))
                    {
                        NewToken(ExpressTokenType.Integer, last.Line, last.Position, length, i);
                        return;
                    }
                    else if (double.TryParse(s, out double d))
                    {
                        NewToken(ExpressTokenType.Real, last.Line, last.Position, length, d);
                        return;
                    }
                    else throw new EntityParserException(token.Line, token.Position, length, "Invalid number");
                }
            }
            throw new EntityParserException(0, 0, 0, "Invalid number");
        }
        /// <summary>
        /// 解析关键字
        /// </summary>
        /// <param name="next"></param>
        /// <param name="line"></param>
        /// <param name="position"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void LexicalKeyword(char next, ref int line, ref int position)
        {
            int length = 0;
            ParserToken last = null;
            while (Forecast.TryPeek(out ParserToken token))
            {
                if (token.TokenType == ExpressTokenType.KeywordChar)
                {
                    length += token.Length;
                    last = token;
                    Forecast.Pop();
                }
                else
                {
                    if (last == null) throw new EntityParserException(token.Line, token.Position, token.Length, "Target token is not keyword");
                    string s = Source[last.Position..(last.Position + length)];
                    NewToken(ExpressTokenType.Keyword, last.Line, last.Position, length, s);
                    return;
                }
            }
        }
        #endregion

        #region 规约
        /// <summary>
        /// R0产生式
        /// 不做任何动作
        /// </summary>
        /// <param name="parser"></param>
        private static void Nul(ExpressLineParser parser) { }
        /// <summary>
        /// R1产生式
        /// R1: ENTITY_INSTANCE_NAME -> # INTEGER
        /// </summary>
        /// <param name="parser"></param>
        private static void R01(ExpressLineParser parser)
        {
            var stack = parser.TokenStack;
            var state = parser.StateStack;
            int value = -1; int length = 0;
            while(stack.TryPeek(out ParserToken token))
            {
                if (token.TokenType == ExpressTokenType.Integer)
                {
                    int l = (int)token.SourceValue;
                    if (l <= 0) E05(parser);
                    else
                    {
                        value = l;
                        length += token.Length;
                        stack.Pop();
                        state.Pop();
                    }
                }
                else if (token.TokenType == ExpressTokenType.Pound)
                {
                    if (value <= 0) E03(parser);
                    else
                    {
                        stack.Pop();
                        state.Pop();
                        EntityInstanceName name = new EntityInstanceName(token.Line, token.Position, length + 1, value);
                        stack.Push(name);
                        parser.GOTO();
                        return;
                    }
                }
                else E01(parser);
            }
            E06(parser);
        }
        /// <summary>
        /// R2产生式
        /// R2：SIMPLE_RECORD  -> KEYWORD ( )
        /// </summary>
        /// <param name="parser"></param>
        private static void R02(ExpressLineParser parser)
        {
            var stack = parser.TokenStack;
            var state = parser.StateStack;
            int length = 0;
            while (stack.TryPeek(out ParserToken token))
            {
                if (token.TokenType == ExpressTokenType.RightBackets)
                {
                    length += token.Length;
                    stack.Pop();
                    state.Pop();
                }
                else if (token.TokenType == ExpressTokenType.LeftBacksts)
                {
                    length += token.Length;
                    stack.Pop();
                    state.Pop();
                }
                else if (token.TokenType == ExpressTokenType.Keyword)
                {
                    string s = token.SourceValue as string;
                    length += token.Length;
                    SimpleRecord record = new SimpleRecord(token.Line, token.Position, length, s ?? string.Empty);
                    stack.Pop();
                    state.Pop();
                    stack.Push(record);
                    parser.GOTO();
                    return;
                }
                else E01(parser);
            }
            E06(parser);
        }
        /// <summary>
        /// R3产生式
        /// R3：SIMPLE_RECORD -> RECORD | SUBSUPER_RECORD -> RECORD
        /// </summary>
        /// <param name="parser"></param>
        private static void R03(ExpressLineParser parser)
        {
            var stack = parser.TokenStack;
            var state = parser.StateStack;
            while (stack.TryPeek(out ParserToken token))
            {
                if(token.TokenType == ExpressTokenType.SimpleRecord || token.TokenType == ExpressTokenType.SubsuperRecord)
                {
                    state.Pop();
                    token.TokenType = ExpressTokenType.Record;
                    parser.GOTO();
                    return;
                }
                else E01(parser);
            }
            E06(parser);
        }
        /// <summary>
        /// R4产生式
        /// R4：ENTITY_INSTANCE  -> ENTITY_INSTANCE_NAME = RECORD ;
        /// </summary>
        /// <param name="parser"></param>
        private static void R04(ExpressLineParser parser)
        {
            var stack = parser.TokenStack;
            var state = parser.StateStack;
            int length = 0;
            Record record = null; int name = -1;
            while (stack.TryPeek(out ParserToken token))
            {
                if (token.TokenType == ExpressTokenType.Semicolon || token.TokenType == ExpressTokenType.Equal)
                {
                    state.Pop();
                    stack.Pop();
                    length += token.Length;
                }
                else if (token.TokenType == ExpressTokenType.Record)
                {
                    record = token as Record;
                    if (record == null) E07(parser);
                    state.Pop();
                    stack.Pop();
                    length += token.Length;
                }
                else if (token.TokenType == ExpressTokenType.EntityInstanceName)
                {
                    if (!(token is EntityInstanceName n)) throw new EntityParserException(token.Line, token.Position, token.Length, "E8: Invalid Instance name");
                    name = n.NameNumber;
                    if (record == null) E07(parser);
                    state.Pop();
                    stack.Pop();
                    length += token.Length;
                    EntityInstance instance = new EntityInstance(token.Line, token.Position, length,
                        record ?? throw new EntityParserException(token.Line, token.Position, token.Length, "E7: Invalid Record"), name);
                    stack.Push(instance);
                    parser.GOTO();
                    return;
                }
                else E06(parser);
            }
            E06(parser);
        }
        /// <summary>
        /// R5产生式
        /// R5： ENTITY  -> ENTITY_INSTANCE | ENTITY  -> HEADER_ENTITY
        /// </summary>
        /// <param name="parser"></param>
        private static void R05(ExpressLineParser parser)
        {
            var stack = parser.TokenStack;
            var state = parser.StateStack;
            while (stack.TryPeek(out ParserToken token))
            {
                if (token.TokenType == ExpressTokenType.EntityInstance || token.TokenType == ExpressTokenType.HeaderEntity)
                {
                    state.Pop();
                    token.TokenType = ExpressTokenType.Entity;
                    return;
                }
                else if (token.TokenType == ExpressTokenType.None) continue;
                else E06(parser);
            }
            E06(parser);
        }
        /// <summary>
        /// R6产生式
        /// R6：HEADER_ENTITY -> SIMPLE_RECORD ;
        /// </summary>
        /// <param name="parser"></param>
        private static void R06(ExpressLineParser parser)
        {
            var stack = parser.TokenStack;
            var state = parser.StateStack;
            int length = 0;
            while (stack.TryPeek(out ParserToken token))
            {
                if (token.TokenType == ExpressTokenType.Semicolon)
                {
                    state.Pop();
                    stack.Pop();
                    length += token.Length;
                }
                else if (token.TokenType == ExpressTokenType.SimpleRecord)
                {
                    if (!(token is SimpleRecord record)) throw new EntityParserException(token.Line, token.Position, token.Length, "E7: Invalid Record");
                    state.Pop();
                    stack.Pop();
                    length += token.Length;
                    HeaderEntity entity = new HeaderEntity(token.Line, token.Position, length, record);
                    stack.Push(entity);
                    parser.GOTO();
                    return;
                }
                else E06(parser);
            }
        }
        /// <summary>
        /// R7产生式
        /// R7：SUBSUPER_RECORD -> ( )
        /// </summary>
        /// <param name="parser"></param>
        private static void R07(ExpressLineParser parser)
        {
            var stack = parser.TokenStack;
            var state = parser.StateStack;
            int length = 0;
            while (stack.TryPeek(out ParserToken token))
            {
                if(token.TokenType == ExpressTokenType.RightBackets)
                {
                    state.Pop();
                    stack.Pop();
                    length += token.Length;
                }
                else if (token.TokenType == ExpressTokenType.LeftBacksts)
                {
                    state.Pop();
                    stack.Pop();
                    length += token.Length;
                    SubsuperRecord record = new SubsuperRecord(token.Line, token.Position, length);
                    stack.Push(record);
                    parser.GOTO();
                    return;
                }
                else E06(parser);
            }
            E06(parser);
        }
        /// <summary>
        /// R8产生式
        /// R8：LIST  -> ( )
        /// </summary>
        /// <param name="parser"></param>
        private static void R08(ExpressLineParser parser)
        {
            var stack = parser.TokenStack;
            var state = parser.StateStack;
            int length = 0;
            while (stack.TryPeek(out ParserToken token))
            {
                if (token.TokenType == ExpressTokenType.RightBackets)
                {
                    state.Pop();
                    stack.Pop();
                    length += token.Length;
                }
                else if (token.TokenType == ExpressTokenType.LeftBacksts)
                {
                    state.Pop();
                    stack.Pop();
                    length += token.Length;
                    ExpressList list = new ExpressList(token.Line, token.Position, length);
                    stack.Push(list);
                    parser.GOTO();
                    return;
                }
            }
        }
        /// <summary>
        /// R9产生式
        /// R9：UNTYPE_PARAMETER -> LIST
        /// UNTYPE_PARAMETER  -> $
        /// UNTYPE_PARAMETER  -> INTEGER
        /// UNTYPE_PARAMETER  -> REAL
        /// UNTYPE_PARAMETER  -> STRING
        /// UNTYPE_PARAMETER  -> BINARY
        /// UNTYPE_PARAMETER  -> ENUMERATION
        /// UNTYPE_PARAMETER  -> ENTITY_INSTANCE_NAME
        /// </summary>
        /// <param name="parser"></param>
        private static void R09(ExpressLineParser parser)
        {
            var stack = parser.TokenStack;
            var state = parser.StateStack;
            while (stack.TryPeek(out ParserToken token))
            {
                if (token.TokenType == ExpressTokenType.List)
                {
                    if (!(token is ExpressList list)) throw new EntityParserException(token.Line, token.Position, token.Length, "E9: Invalid Parameter");
                    state.Pop();
                    stack.Pop();
                    var parameter = new ListParameter(token.Line, token.Position, token.Length, list.Lists);
                    stack.Push(parameter);
                    parser.GOTO();
                    return;
                }
                else if(token.TokenType == ExpressTokenType.Dollar)
                {
                    state.Pop();
                    stack.Pop();
                    var parameter = new DefectParameter(token.Line, token.Position, token.Length);
                    stack.Push(parameter);
                    parser.GOTO();
                    return;
                }
                else if(token.TokenType == ExpressTokenType.Integer)
                {
                    state.Pop();
                    stack.Pop();
                    int v = (int)token.SourceValue;
                    var parameter = new RealParameter(token.Line, token.Position, token.Length, v);
                    stack.Push(parameter);
                    parser.GOTO();
                    return;
                }
                else if(token.TokenType == ExpressTokenType.Real)
                {
                    state.Pop();
                    stack.Pop();
                    var parameter = new RealParameter(token.Line, token.Position, token.Length, (double)token.SourceValue);
                    stack.Push(parameter);
                    parser.GOTO();
                    return;
                }
                else if (token.TokenType == ExpressTokenType.String)
                {
                    state.Pop();
                    stack.Pop();
                    var parameter = new StringParameter(token.Line, token.Position, token.Length, (string)token.SourceValue);
                    stack.Push(parameter);
                    parser.GOTO();
                    return;
                }
                else if(token.TokenType == ExpressTokenType.Enumeration)
                {
                    state.Pop();
                    stack.Pop();
                    var parameter = new EnumerationParameter(token.Line, token.Position, token.Length, (string)token.SourceValue);
                    stack.Push(parameter);
                    parser.GOTO();
                    return;
                }
                else if(token.TokenType == ExpressTokenType.Binary)
                {
                    state.Pop();
                    stack.Pop();
                    var parameter = new BinaryParameter(token.Line, token.Position, token.Length, (string)token.SourceValue);
                    stack.Push(parameter);
                    parser.GOTO();
                    return;
                }
                else if (token.TokenType == ExpressTokenType.EntityInstanceName)
                {
                    if (!(token is EntityInstanceName n)) throw new EntityParserException(token.Line, token.Position, token.Length, "E8: Invalid Instance name");
                    state.Pop();
                    stack.Pop();
                    var parameter = new EntityNameParameter(token.Line, token.Position, token.Length, n.NameNumber);
                    stack.Push(parameter);
                    parser.GOTO();
                    return;
                }
                else E01(parser);
            }
            E06(parser);
        }
        /// <summary>
        /// R10产生式
        /// R10：PARAMETER -> UNTYPE_PARAMETER
        /// PARAMETER      -> OMITTED_PARAMETER
        /// PARAMETER      -> TYPED_PARAMETER
        /// </summary>
        /// <param name="parser"></param>
        private static void R10(ExpressLineParser parser)
        {
            var stack = parser.TokenStack;
            var state = parser.StateStack;
            while (stack.TryPeek(out ParserToken token))
            {
                if (token.TokenType == ExpressTokenType.TypeParameter ||
                    token.TokenType == ExpressTokenType.UntypeParameter ||
                    token.TokenType == ExpressTokenType.OmittedParameter)
                {
                    state.Pop();
                    token.TokenType = ExpressTokenType.Parameter;
                    parser.GOTO();
                    return;
                }
                else E01(parser);
            }
            E06(parser);
        }
        /// <summary>
        /// R11产生式
        /// R11：PARAMETER_LIST  -> PARAMETER
        /// </summary>
        /// <param name="parser"></param>
        private static void R11(ExpressLineParser parser)
        {
            var stack = parser.TokenStack;
            var state = parser.StateStack;
            while (stack.TryPeek(out ParserToken token))
            {
                if(token.TokenType == ExpressTokenType.Parameter)
                {
                    if (!(token is Parameter parameter)) throw new EntityParserException(token.Line, token.Position, token.Length, "E9: Invalid Parameter");
                    stack.Pop();
                    state.Pop();
                    ParameterList list = new ParameterList(token.Line, token.Position, token.Length, parameter);
                    stack.Push(list);
                    parser.GOTO();
                    return;
                }
            }
        }
        /// <summary>
        /// R12产生式
        /// SIMPLE_RECORD   -> KEYWORD ( PARAMETER_LIST )
        /// </summary>
        /// <param name="parser"></param>
        private static void R12(ExpressLineParser parser)
        {
            var stack = parser.TokenStack;
            var state = parser.StateStack;
            ParameterList list = null; int length = 0;
            while (stack.TryPeek(out ParserToken token))
            {
                if (token.TokenType == ExpressTokenType.RightBackets ||
                    token.TokenType == ExpressTokenType.LeftBacksts)
                {
                    stack.Pop();
                    state.Pop();
                    length += token.Length;
                }
                else if (token.TokenType == ExpressTokenType.ParameterList)
                {
                    if (!(token is ParameterList l)) throw new EntityParserException(token.Line, token.Position, token.Length, "E9: Invalid Parameter");
                    list = l;
                    stack.Pop();
                    state.Pop();
                    length += token.Length;
                }
                else if (token.TokenType == ExpressTokenType.Keyword)
                {
                    if (list == null) throw new EntityParserException(token.Line, token.Position, token.Length, "E10: Parameter lost");
                    string s = token.SourceValue as string;
                    length += token.Length;
                    SimpleRecord record = new SimpleRecord(token.Line, token.Position, length, s ?? string.Empty);
                    record.Parameters.AddRange(list.Parameters);
                    stack.Pop();
                    state.Pop();
                    stack.Push(record);
                    parser.GOTO();
                    return;
                }
                else E01(parser);
            }
            E06(parser);
        }
        /// <summary>
        /// R13产生式
        /// R13：PARAMETER_LIST -> PARAMETER , PARAMETER_LIST
        /// </summary>
        /// <param name="parser"></param>
        private static void R13(ExpressLineParser parser)
        {
            var stack = parser.TokenStack;
            var state = parser.StateStack;
            ParameterList list = null; int length = 0;
            while (stack.TryPeek(out ParserToken token))
            {
                if (token.TokenType == ExpressTokenType.Comma)
                {
                    stack.Pop();
                    state.Pop();
                    length += token.Length;
                }
                else if (token.TokenType == ExpressTokenType.ParameterList)
                {
                    if (!(token is ParameterList l)) throw new EntityParserException(token.Line, token.Position, token.Length, "E9: Invalid Parameter");
                    list = l;
                    stack.Pop();
                    state.Pop();
                    length += token.Length;
                }
                else if (token.TokenType == ExpressTokenType.Parameter)
                {
                    if (!(token is Parameter p)) throw new EntityParserException(token.Line, token.Position, token.Length, "E9: Invalid Parameter");
                    if (list == null) throw new EntityParserException(token.Line, token.Position, token.Length, "E10: Parameter lost");
                    stack.Pop();
                    state.Pop();
                    length += token.Length;
                    list.Line = token.Line;
                    list.Position = token.Position;
                    list.Length = length;
                    list.Parameters.Add(p);
                }
                else if(token.TokenType == ExpressTokenType.LeftBacksts)
                {
                    if (list == null) throw new EntityParserException(token.Line, token.Position, token.Length, "E10: Parameter lost");
                    list.Parameters.Reverse();
                    stack.Push(list);
                    parser.GOTO();
                    return;
                }
                else E01(parser);
            }
            E06(parser);
        }
        /// <summary>
        /// R14产生式
        /// LIST -> ( PARAMETER_LIST )
        /// </summary>
        /// <param name="parser"></param>
        private static void R14(ExpressLineParser parser)
        {
            var stack = parser.TokenStack;
            var state = parser.StateStack;
            ParameterList list = null; int length = 0;
            while (stack.TryPeek(out ParserToken token))
            {
                if (token.TokenType == ExpressTokenType.RightBackets)
                {
                    state.Pop();
                    stack.Pop();
                    length += token.Length;
                }
                else if(token.TokenType == ExpressTokenType.ParameterList)
                {
                    if (!(token is ParameterList l)) throw new EntityParserException(token.Line, token.Position, token.Length, "E9: Invalid Parameter");
                    list = l;
                    stack.Pop();
                    state.Pop();
                    length += token.Length;
                }
                else if(token.TokenType == ExpressTokenType.LeftBacksts)
                {
                    if (list == null) throw new EntityParserException(token.Line, token.Position, token.Length, "E10: Parameter lost");
                    state.Pop();
                    stack.Pop();
                    length += token.Length;
                    ExpressList result = new ExpressList(token.Line, token.Position, length);
                    result.Lists.AddRange(list.Parameters);
                    stack.Push(result);
                    parser.GOTO();
                    return;
                }
            }
        }
        /// <summary>
        /// R15产生式
        /// OMITTED_PARAMETER -> *
        /// </summary>
        /// <param name="parser"></param>
        private static void R15(ExpressLineParser parser)
        {
            var stack = parser.TokenStack;
            var state = parser.StateStack;
            while (stack.TryPeek(out ParserToken token))
            {
                if (token.TokenType == ExpressTokenType.Asterisk)
                {
                    stack.Pop();
                    state.Pop();
                    OmittedParameter parameter = new OmittedParameter(token.Line, token.Position, token.Length);
                    stack.Push(parameter);
                    parser.GOTO();
                    return;
                }
                else E01(parser);
            }
            E06(parser);
        }
        /// <summary>
        /// R16产生式
        /// R16：SIMPLE_RECORD_LIST -> SIMPLE_RECORD
        /// </summary>
        /// <param name="parser"></param>
        private static void R16(ExpressLineParser parser)
        {
            var stack = parser.TokenStack;
            var state = parser.StateStack;
            while (stack.TryPeek(out ParserToken token))
            {
                if (token.TokenType == ExpressTokenType.SimpleRecord)
                {
                    if (!(token is SimpleRecord record)) throw new EntityParserException(token.Line, token.Position, token.Length, "E7: Invalid Record");
                    stack.Pop();
                    state.Pop();
                    SimpleRecordList list = new SimpleRecordList(token.Line, token.Position, token.Length);
                    list.SimpleRecords.Add(record);
                    stack.Push(list);
                    parser.GOTO();
                    return;
                }
                else E01(parser);
            }
            E06(parser);
        }
        /// <summary>
        /// R17产生式
        /// R17：SUBSUPER_RECORD -> ( SIMPLE_RECORD_LIST )
        /// </summary>
        /// <param name="parser"></param>
        private static void R17(ExpressLineParser parser)
        {
            var stack = parser.TokenStack;
            var state = parser.StateStack;
            SimpleRecordList list = null; int length = 0;
            while (stack.TryPeek(out ParserToken token))
            {
                if(token.TokenType == ExpressTokenType.RightBackets)
                {
                    stack.Pop();
                    state.Pop();
                    length += token.Length;
                }
                else if (token.TokenType == ExpressTokenType.SimpleRecordList)
                {
                    if (!(token is SimpleRecordList record)) throw new EntityParserException(token.Line, token.Position, token.Length, "E7: Invalid Record");
                    stack.Pop();
                    state.Pop();
                    list = record;
                    length += token.Length;
                }
                else if (token.TokenType == ExpressTokenType.LeftBacksts)
                {
                    if (list == null) throw new EntityParserException(token.Line, token.Position, length, "E11: Record lost");
                    stack.Pop();
                    state.Pop();
                    length += token.Length;
                    SubsuperRecord subsuper = new SubsuperRecord(token.Line, token.Position, length);
                    subsuper.Records.AddRange(list.SimpleRecords);
                    stack.Push(subsuper);
                    parser.GOTO();
                    return;
                }
            }
        }
        /// <summary>
        /// R18产生式
        /// R18：SIMPLE_RECORD_LIST -> SIMPLE_RECORD SIMPLE_RECORD_LIST
        /// </summary>
        /// <param name="parser"></param>
        private static void R18(ExpressLineParser parser)
        {
            var stack = parser.TokenStack;
            var state = parser.StateStack;
            SimpleRecordList list = null; int length = 0;
            while (stack.TryPeek(out ParserToken token))
            {
                if (token.TokenType == ExpressTokenType.SimpleRecordList)
                {
                    if (!(token is SimpleRecordList record)) throw new EntityParserException(token.Line, token.Position, token.Length, "E7: Invalid Record");
                    stack.Pop();
                    state.Pop();
                    list = record;
                    length += token.Length;
                }
                else if(token.TokenType == ExpressTokenType.SimpleRecord)
                {
                    if (!(token is SimpleRecord record)) throw new EntityParserException(token.Line, token.Position, token.Length, "E7: Invalid Record");
                    if (list == null) throw new EntityParserException(token.Line, token.Position, length, "E11: Record lost");
                    stack.Pop();
                    state.Pop();
                    list.SimpleRecords.Add(record);
                    length += token.Length;
                }
                else if(token.TokenType == ExpressTokenType.LeftBacksts)
                {
                    if (list == null) throw new EntityParserException(token.Line, token.Position, length, "E11: Record lost");
                    list.SimpleRecords.Reverse();
                    stack.Push(list);
                    parser.GOTO();
                    return;
                }
            }
        }
        /// <summary>
        /// R19产生式
        /// R19：TYPED_PARAMETER -> KEYWORD ( PARAMETER )
        /// </summary>
        /// <param name="parser"></param>
        private static void R19(ExpressLineParser parser)
        {
            var stack = parser.TokenStack;
            var state = parser.StateStack;
            Parameter parameter = null; int length = 0;
            while (stack.TryPeek(out ParserToken token))
            {
                if (token.TokenType == ExpressTokenType.RightBackets ||
                    token.TokenType == ExpressTokenType.LeftBacksts)
                {
                    stack.Pop();
                    state.Pop();
                    length += token.Length;
                }
                else if (token.TokenType == ExpressTokenType.Parameter)
                {
                    if (!(token is Parameter p)) throw new EntityParserException(token.Line, token.Position, token.Length, "E9: Invalid Parameter");
                    parameter = p;
                    stack.Pop();
                    state.Pop();
                    length += token.Length;
                }
                else if (token.TokenType == ExpressTokenType.Keyword)
                {
                    if (parameter == null) throw new EntityParserException(token.Line, token.Position, token.Length, "E10: Parameter lost");
                    string s = token.SourceValue as string;
                    length += token.Length;
                    TypeParameter type = new TypeParameter(token.Line, token.Position, length, s ?? string.Empty, parameter);
                    stack.Pop();
                    state.Pop();
                    stack.Push(type);
                    parser.GOTO();
                    return;
                }
                else E01(parser);
            }
            E06(parser);
        }
        #endregion

        #region 移进
        /// <summary>
        /// 将预测栈词法移入令牌栈
        /// </summary>
        /// <param name="parser"></param>
        private static void MoveIn(ExpressLineParser parser, int state)
        {
            if(parser.Forecast.TryPop(out ParserToken token))
            {
                parser.TokenStack.Push(token);
            }
            parser.StateStack.Push(state);
        }
        private static void M00(ExpressLineParser parser) => MoveIn(parser, 0);
        private static void M01(ExpressLineParser parser) => MoveIn(parser, 1);
        private static void M02(ExpressLineParser parser) => MoveIn(parser, 2);
        private static void M03(ExpressLineParser parser) => MoveIn(parser, 3);
        private static void M05(ExpressLineParser parser) => MoveIn(parser, 5);
        private static void M06(ExpressLineParser parser) => MoveIn(parser, 6);
        private static void M07(ExpressLineParser parser) => MoveIn(parser, 7);
        private static void M08(ExpressLineParser parser) => MoveIn(parser, 8);
        private static void M09(ExpressLineParser parser) => MoveIn(parser, 9);
        private static void M10(ExpressLineParser parser) => MoveIn(parser, 10);
        private static void M11(ExpressLineParser parser) => MoveIn(parser, 11);
        private static void M12(ExpressLineParser parser) => MoveIn(parser, 12);
        private static void M13(ExpressLineParser parser) => MoveIn(parser, 13);
        private static void M14(ExpressLineParser parser) => MoveIn(parser, 14);
        private static void M15(ExpressLineParser parser) => MoveIn(parser, 15);
        private static void M16(ExpressLineParser parser) => MoveIn(parser, 16);
        private static void M17(ExpressLineParser parser) => MoveIn(parser, 17);
        private static void M18(ExpressLineParser parser) => MoveIn(parser, 18);
        private static void M21(ExpressLineParser parser) => MoveIn(parser, 21);
        private static void M24(ExpressLineParser parser) => MoveIn(parser, 24);
        private static void M25(ExpressLineParser parser) => MoveIn(parser, 25);
        private static void M26(ExpressLineParser parser) => MoveIn(parser, 26);
        private static void M27(ExpressLineParser parser) => MoveIn(parser, 27);
        private static void M28(ExpressLineParser parser) => MoveIn(parser, 28);
        private static void M31(ExpressLineParser parser) => MoveIn(parser, 31);
        private static void M34(ExpressLineParser parser) => MoveIn(parser, 34);
        private static void M37(ExpressLineParser parser) => MoveIn(parser, 37);
        #endregion

        #region 错误
        /// <summary>
        /// E1：意外的字符
        /// </summary>
        /// <param name="parser"></param>
        /// <exception cref="EntityParserException"></exception>
        private static void E01(ExpressLineParser parser)
        {
            if (parser.TokenStack.TryPop(out ParserToken token))
            {
                throw new EntityParserException(token.Line, token.Position, token.Length, $"E1: Unexpected token {token}");
            }
            throw new EntityParserException(0, 0, 0, $"E4: Unexpected token");
        }
        /// <summary>
        /// E2：实例名必须为整数
        /// </summary>
        /// <param name="parser"></param>
        /// <exception cref="EntityParserException"></exception>
        private static void E02(ExpressLineParser parser)
        {
            if (parser.TokenStack.TryPop(out ParserToken token))
            {
                throw new EntityParserException(token.Line, token.Position, token.Length, $"E2: Entity instance name must be integer");
            }
            throw new EntityParserException(0, 0, 0, $"E2: Entity instance name must be integer");
        }
        /// <summary>
        /// E03：实例名丢失
        /// </summary>
        /// <param name="parser"></param>
        /// <exception cref="EntityParserException"></exception>
        private static void E03(ExpressLineParser parser)
        {
            if (parser.TokenStack.TryPop(out ParserToken token))
            {
                throw new EntityParserException(token.Line, token.Position, token.Length, $"E3: Entity instance name lost");
            }
            throw new EntityParserException(0, 0, 0, $"E3: Entity instance name lost");
        }
        /// <summary>
        /// E4:意外的起始字符
        /// </summary>
        /// <param name="parser"></param>
        private static void E04(ExpressLineParser parser)
        {
            if(parser.TokenStack.TryPop(out ParserToken token))
            {
                throw new EntityParserException(token.Line, token.Position, token.Length, $"E4: Unexpected start token {token}");
            }
            throw new EntityParserException(0, 0, 0, $"E4: Unexpected start token");
        }
        /// <summary>
        /// E5：实体名必须大于0
        /// </summary>
        /// <param name="parser"></param>
        /// <exception cref="EntityParserException"></exception>
        private static void E05(ExpressLineParser parser)
        {
            if (parser.TokenStack.TryPop(out ParserToken token))
            {
                throw new EntityParserException(token.Line, token.Position, token.Length, $"E5: Entity instance must be greater than 0");
            }
            throw new EntityParserException(0, 0, 0, $"E5: Entity instance must be greater than 0");
        }
        /// <summary>
        /// E6：数据源不完整
        /// </summary>
        /// <param name="parser"></param>
        private static void E06(ExpressLineParser parser)
        {
            if (parser.TokenStack.TryPop(out ParserToken token))
            {
                throw new EntityParserException(token.Line, token.Position, token.Length, $"E6: Data source is incompleted");
            }
            throw new EntityParserException(0, 0, 0, $"E6: Data source is incompleted");
        }
        /// <summary>
        /// E7：非法的记录
        /// </summary>
        /// <param name="parser"></param>
        private static void E07(ExpressLineParser parser)
        {
            if (parser.TokenStack.TryPop(out ParserToken token))
            {
                throw new EntityParserException(token.Line, token.Position, token.Length, $"E7: Invalid Record");
            }
            throw new EntityParserException(0, 0, 0, $"E7: Invalid Record");
        }
        /// <summary>
        /// E8：非法的实例名
        /// </summary>
        private static void E08(ExpressLineParser parser)
        {
            if (parser.TokenStack.TryPop(out ParserToken token))
            {
                throw new EntityParserException(token.Line, token.Position, token.Length, $"E8: Invalid Instance name");
            }
            throw new EntityParserException(0, 0, 0, $"E8: Invalid Instance name");
        }
        /// <summary>
        /// E9：非法的参数
        /// </summary>
        /// <param name="parser"></param>
        /// <exception cref="EntityParserException"></exception>
        private static void E09(ExpressLineParser parser)
        {
            if (parser.TokenStack.TryPop(out ParserToken token))
            {
                throw new EntityParserException(token.Line, token.Position, token.Length, $"E9: Invalid Parameter");
            }
            throw new EntityParserException(0, 0, 0, $"E9: Invalid Parameter");
        }
        /// <summary>
        /// E10：参数丢失
        /// </summary>
        /// <param name="parser"></param>
        /// <exception cref="EntityParserException"></exception>
        private static void E10(ExpressLineParser parser)
        {
            if (parser.TokenStack.TryPop(out ParserToken token))
            {
                throw new EntityParserException(token.Line, token.Position, token.Length, $"E10: Parameter lost");
            }
            throw new EntityParserException(0, 0, 0, $"E10: Parameter lost");
        }
        #endregion

        #region 判断
        /// <summary>
        /// 是否为井号键
        /// </summary>
        /// <param name="next"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool IsPound(char next) => next == '#';
        /// <summary>
        /// 是否为数字
        /// </summary>
        /// <param name="next"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool IsDigit(char next)
        {
            return next switch
            {
                '0' => true,
                '1' => true,
                '2' => true,
                '3' => true,
                '4' => true,
                '5' => true,
                '6' => true,
                '7' => true,
                '8' => true,
                '9' => true,
                _ => false,
            };
        }
        /// <summary>
        /// 是否为符号
        /// </summary>
        /// <param name="next"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool IsSign(char next) => next == '+' || next == '-';
        /// <summary>
        /// 是否为e
        /// </summary>
        /// <param name="next"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool IsE(char next) => next == 'E' || next == 'e';
        /// <summary>
        /// 是否为点
        /// </summary>
        /// <param name="next"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool IsSpot(char next) => next == '.';
        /// <summary>
        /// 是否为大写字符
        /// </summary>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool IsUpper(char next)
        {
            return next switch
            {
                'A' => true,
                'B' => true,
                'C' => true,
                'D' => true,
                'E' => true,
                'F' => true,
                'G' => true,
                'H' => true,
                'I' => true,
                'J' => true,
                'K' => true,
                'L' => true,
                'M' => true,
                'N' => true,
                'O' => true,
                'P' => true,
                'Q' => true,
                'R' => true,
                'S' => true,
                'T' => true,
                'U' => true,
                'V' => true,
                'W' => true,
                'X' => true,
                'Y' => true,
                'Z' => true,
                '_' => true,
                _ => false
            };
        }
        /// <summary>
        /// 是否为十六进制数
        /// </summary>
        /// <param name="next"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool IsHex(char next)
        {
            return next switch
            {
                '0' => true,
                '1' => true,
                '2' => true,
                '3' => true,
                '4' => true,
                '5' => true,
                '6' => true,
                '7' => true,
                '8' => true,
                '9' => true,
                'A' => true,
                'B' => true,
                'C' => true,
                'D' => true,
                'E' => true,
                'F' => true,
                _ => false
            };
        }
        /// <summary>
        /// 是否为分号
        /// </summary>
        /// <param name="next"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool IsSemicolon(char next) => next == ';';
        /// <summary>
        /// 是否为左括号
        /// </summary>
        /// <param name="next"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool IsLeftBackst(char next) => next == '(';
        /// <summary>
        /// 是否为右括号
        /// </summary>
        /// <param name="next"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool IsRightBackst(char next) => next == ')';
        /// <summary>
        /// 是否是等号
        /// </summary>
        /// <param name="next"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool IsEqual(char next) => next == '=';
        /// <summary>
        /// 是否为单引号
        /// </summary>
        /// <param name="next"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool IsSingeQuotes(char next) => next == '\'';
        /// <summary>
        /// 是否为双引号
        /// </summary>
        /// <param name="next"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool IsDoubeQuotes(char next) => next == '"';
        /// <summary>
        /// 是否为换行符
        /// </summary>
        /// <param name="next"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool IsNewLine(char next) => next == '\n';
        /// <summary>
        /// 是否为回车
        /// </summary>
        /// <param name="next"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool IsCR(char next) => next == '\r';
        /// <summary>
        /// 是否为逗号
        /// </summary>
        /// <param name="next"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool IsComma(char next) => next == ',';
        /// <summary>
        /// 是否为星号
        /// </summary>
        /// <param name="next"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool IsAsterisk(char next) => next == '*';
        /// <summary>
        /// 是否为美元符号
        /// </summary>
        /// <param name="next"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool IsDollar(char next) => next == '$';
        /// <summary>
        /// 是否为感叹号
        /// </summary>
        /// <param name="next"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool IsExclamatory(char next) => next == '!';
        /// <summary>
        /// 是否为空格
        /// </summary>
        /// <param name="next"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool IsSpace(char next) => next == ' ';
        /// <summary>
        /// 获取终结符在LR表中的位置
        /// </summary>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int GetTerminatorInACTION(ParserToken token)
        {
            return token.TokenType switch
            {
                ExpressTokenType.Keyword => 0,
                ExpressTokenType.Pound => 1,
                ExpressTokenType.Integer => 2,
                ExpressTokenType.Real => 3,
                ExpressTokenType.String => 4,
                ExpressTokenType.Equal => 5,
                ExpressTokenType.LeftBacksts => 6,
                ExpressTokenType.RightBackets => 7,
                ExpressTokenType.Comma => 8,
                ExpressTokenType.Semicolon => 9,
                ExpressTokenType.Asterisk => 10,
                ExpressTokenType.Dollar => 11,
                ExpressTokenType.Enumeration => 12,
                ExpressTokenType.Binary => 13,
                ExpressTokenType.End => 14,
                _ => throw new EntityParserException(token.Line, token.Position, token.Length, $"Unknown terminator token {token}"),
            };
        }
        /// <summary>
        /// 获取非终结符在GOTO表中的位置
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int GetNonTerminatorInGOTO(ParserToken token)
        {
            return token.TokenType switch
            {
                ExpressTokenType.Entity => 0,
                ExpressTokenType.HeaderEntity => 1,
                ExpressTokenType.EntityInstance => 2,
                ExpressTokenType.Record => 3,
                ExpressTokenType.SimpleRecord => 4,
                ExpressTokenType.SubsuperRecord => 5,
                ExpressTokenType.SimpleRecordList => 6,
                ExpressTokenType.EntityInstanceName => 7,
                ExpressTokenType.ParameterList => 8,
                ExpressTokenType.Parameter => 9,
                ExpressTokenType.TypeParameter => 10,
                ExpressTokenType.UntypeParameter => 11,
                ExpressTokenType.OmittedParameter => 12,
                ExpressTokenType.List => 13,
                _ => throw new EntityParserException(token.Line, token.Position, token.Length, $"Unknown nonterminator token {token}"),
            };
        }
        #endregion
    }
}
