﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// 简单记录列表
    /// </summary>
    public class SimpleRecordList : ParserToken
    {
        /// <summary>
        /// 简单记录列表
        /// </summary>
        public readonly List<SimpleRecord> SimpleRecords = new List<SimpleRecord>();
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="line"></param>
        /// <param name="position"></param>
        /// <param name="length"></param>
        public SimpleRecordList(int line, int position, int length) 
            : base(ExpressTokenType.SimpleRecordList, line, position, length, nameof(SimpleRecordList))
        {
        }
    }
}
