﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// 实体名参数
    /// </summary>
    public class EntityNameParameter : UntypeParameter
    {
        /// <summary>
        /// 参数值
        /// </summary>
        public new int ParameterValue { get; protected set; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="line"></param>
        /// <param name="position"></param>
        /// <param name="length"></param>
        /// <param name="parameter"></param>
        public EntityNameParameter(int line, int position, int length, int parameter) 
            : base(line, position, length, parameter, ExpressTokenType.EntityInstanceName)
        {
            ParameterValue = parameter;
            SourceValue = nameof(EntityNameParameter);
        }
    }
}
