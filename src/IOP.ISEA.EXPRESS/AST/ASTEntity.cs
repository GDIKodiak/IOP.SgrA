﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// 抽象语法树实体
    /// </summary>
    public class ASTNode : ParserToken
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="type"></param>
        /// <param name="line"></param>
        /// <param name="position"></param>
        /// <param name="length"></param>
        public ASTNode(ExpressTokenType type, int line, int position, int length, string value) 
            : base(type, line, position, length, value)
        {
        }
    }
}
