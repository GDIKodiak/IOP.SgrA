﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// 参数
    /// </summary>
    public class Parameter : ParserToken
    {
        /// <summary>
        /// 参数值
        /// </summary>
        public object ParameterValue { get; protected set; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="type"></param>
        /// <param name="line"></param>
        /// <param name="position"></param>
        /// <param name="length"></param>
        public Parameter(ExpressTokenType type, int line, int position, int length, string value) 
            : base(type, line, position, length, value)
        {
        }
    }
}
