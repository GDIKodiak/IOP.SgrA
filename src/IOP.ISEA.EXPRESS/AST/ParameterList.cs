﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// 参数列表
    /// </summary>
    public class ParameterList : ParserToken
    {
        /// <summary>
        /// 参数
        /// </summary>
        public readonly List<Parameter> Parameters = new List<Parameter>();
        /// <summary>
        /// 参数列表
        /// </summary>
        /// <param name="line"></param>
        /// <param name="position"></param>
        /// <param name="length"></param>
        public ParameterList(int line, int position, int length, params Parameter[] parameters) 
            : base(ExpressTokenType.ParameterList, line, position, length, nameof(ParameterList))
        {
            if(parameters != null && parameters.Length > 0)
            {
                Parameters.AddRange(parameters);
            }
        }
    }
}
