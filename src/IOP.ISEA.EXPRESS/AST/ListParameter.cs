﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// 列表参数
    /// </summary>
    public class ListParameter : UntypeParameter
    {
        /// <summary>
        /// 参数值
        /// </summary>
        public new List<Parameter> ParameterValue { get; protected set; } = new List<Parameter>();
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="line"></param>
        /// <param name="position"></param>
        /// <param name="length"></param>
        /// <param name="parameter"></param>
        public ListParameter(int line, int position, int length, IEnumerable<Parameter> parameter) 
            : base(line, position, length, parameter, ExpressTokenType.List)
        {
            ParameterValue.AddRange(parameter);
            SourceValue = nameof(ListParameter);
        }
    }
}
