﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// 缺省参数
    /// </summary>
    public class DefectParameter : UntypeParameter
    {
        /// <summary>
        /// 参数值
        /// </summary>
        public new string ParameterValue { get; protected set; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="line"></param>
        /// <param name="position"></param>
        /// <param name="length"></param>
        public DefectParameter(int line, int position, int length) 
            : base(line, position, length, "$", ExpressTokenType.Dollar)
        {
            ParameterValue = "$";
            SourceValue = nameof(DefectParameter);
        }
    }
}
