﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// 令牌
    /// </summary>
    public class ParserToken
    {
        /// <summary>
        /// 令牌类型
        /// </summary>
        public ExpressTokenType TokenType { get; set; }
        /// <summary>
        /// 令牌行号
        /// </summary>
        public int Line { get; protected internal set; }
        /// <summary>
        /// 令牌位置
        /// </summary>
        public int Position { get; protected internal set; }
        /// <summary>
        /// 长度
        /// </summary>
        public int Length { get; protected internal set; }
        /// <summary>
        /// 原始值
        /// </summary>
        public object SourceValue { get; protected set; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="type"></param>
        /// <param name="line"></param>
        /// <param name="position"></param>
        /// <param name="length"></param>
        /// <param name="value"></param>
        public ParserToken(ExpressTokenType type, int line, int position, int length, object value)
        {
            TokenType = type;
            Line = line;
            Position = position;
            Length = length;
            SourceValue = value;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return SourceValue == null ? string.Empty : SourceValue.ToString();
        }
    }
}
