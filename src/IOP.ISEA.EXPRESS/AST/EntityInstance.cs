﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// 实体实例
    /// </summary>
    public class EntityInstance : ASTNode
    {
        /// <summary>
        /// 实例名
        /// </summary>
        public int InstanceName { get; protected set; }
        /// <summary>
        /// 实例记录
        /// </summary>
        public Record Record { get; protected set; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="type"></param>
        /// <param name="line"></param>
        /// <param name="position"></param>
        /// <param name="length"></param>
        /// <param name="value"></param>
        public EntityInstance(int line, int position, int length, Record record, int name) 
            : base(ExpressTokenType.EntityInstance, line, position, length, nameof(EntityInstance))
        {
            Record = record;
            InstanceName = name;
        }
    }
}
