﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// Express令牌类型
    /// </summary>
    public enum ExpressTokenType
    {
        None,
        /// <summary>
        /// 起始行
        /// </summary>
        StartLine,
        /// <summary>
        /// 下一行
        /// </summary>
        NextLine,
        /// <summary>
        /// 行末尾
        /// </summary>
        EndLine,
        /// <summary>
        /// 结束符
        /// </summary>
        End,
        /// <summary>
        /// 左括号
        /// </summary>
        LeftBacksts,
        /// <summary>
        /// 右括号
        /// </summary>
        RightBackets,
        /// <summary>
        /// 单引号
        /// </summary>
        SingeQuotes,
        /// <summary>
        /// 双引号
        /// </summary>
        DoubeQuotes,
        /// <summary>
        /// 点
        /// </summary>
        Spot,
        /// <summary>
        /// 逗号
        /// </summary>
        Comma,
        /// <summary>
        /// 分号
        /// </summary>
        Semicolon,
        /// <summary>
        /// 空格
        /// </summary>
        Space,
        /// <summary>
        /// 井号
        /// </summary>
        Pound,
        /// <summary>
        /// 等于号
        /// </summary>
        Equal,
        /// <summary>
        /// 星号
        /// </summary>
        Asterisk,
        /// <summary>
        /// 美元符号
        /// </summary>
        Dollar,
        /// <summary>
        /// 符号
        /// </summary>
        Sign,
        /// <summary>
        /// 单个数
        /// </summary>
        Digit,
        /// <summary>
        /// 关键字字符
        /// </summary>
        KeywordChar,
        /// <summary>
        /// 枚举字符
        /// </summary>
        EnumerationChar,
        /// <summary>
        /// 十六进制字符
        /// </summary>
        BinaryChar,
        /// <summary>
        /// 单个字符
        /// </summary>
        Char,
        /// <summary>
        /// 字符串
        /// </summary>
        String,
        /// <summary>
        /// 整数
        /// </summary>
        Integer,
        /// <summary>
        /// 实数
        /// </summary>
        Real,
        /// <summary>
        /// 二进制数
        /// </summary>
        Binary,
        /// <summary>
        /// 十六进制数
        /// </summary>
        Hex,
        /// <summary>
        /// 枚举
        /// </summary>
        Enumeration,
        /// <summary>
        /// 关键字
        /// </summary>
        Keyword,
        /// <summary>
        /// 列表
        /// </summary>
        List,
        /// <summary>
        /// 实体名
        /// </summary>
        EntityInstanceName,
        /// <summary>
        /// 省略参数
        /// </summary>
        OmittedParameter,
        /// <summary>
        /// 非类型参数
        /// </summary>
        UntypeParameter,
        /// <summary>
        /// 类型参数
        /// </summary>
        TypeParameter,
        /// <summary>
        /// 参数
        /// </summary>
        Parameter,
        /// <summary>
        /// 参数列表
        /// </summary>
        ParameterList,
        /// <summary>
        /// 记录
        /// </summary>
        Record,
        /// <summary>
        /// 复合记录
        /// </summary>
        SubsuperRecord,
        /// <summary>
        /// 简单记录
        /// </summary>
        SimpleRecord,
        /// <summary>
        /// 简单记录列表
        /// </summary>
        SimpleRecordList,
        /// <summary>
        /// 实体实例
        /// </summary>
        EntityInstance,
        /// <summary>
        /// 头部实体
        /// </summary>
        HeaderEntity,
        /// <summary>
        /// 实体
        /// </summary>
        Entity
    }
}
