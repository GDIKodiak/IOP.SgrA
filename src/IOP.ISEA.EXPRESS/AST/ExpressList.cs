﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// 列表
    /// </summary>
    public class ExpressList : Parameter
    {
        /// <summary>
        /// 参数
        /// </summary>
        public List<Parameter> Lists { get; protected set; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="line"></param>
        /// <param name="position"></param>
        /// <param name="length"></param>
        public ExpressList(int line, int position, int length) 
            : base(ExpressTokenType.List, line, position, length, "List")
        {
            Lists = new List<Parameter>();
        }
    }
}
