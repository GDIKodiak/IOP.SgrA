﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// 简单记录
    /// </summary>
    public class SimpleRecord : Record
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public string Keyword { get; protected set; }
        /// <summary>
        /// 参数列表
        /// </summary>
        public List<Parameter> Parameters { get; protected set; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="line"></param>
        /// <param name="position"></param>
        /// <param name="length"></param>
        public SimpleRecord(int line, int position, int length, string name) 
            : base(ExpressTokenType.SimpleRecord, line, position, length, nameof(SimpleRecord))
        {
            Parameters = new List<Parameter>();
            Keyword = name;
        }
    }
}
