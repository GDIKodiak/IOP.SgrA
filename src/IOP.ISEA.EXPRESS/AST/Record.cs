﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// 记录
    /// </summary>
    public class Record : ParserToken
    {

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="type"></param>
        /// <param name="line"></param>
        /// <param name="position"></param>
        /// <param name="length"></param>
        /// <param name="value"></param>
        public Record(ExpressTokenType type, int line, int position, int length, string value) 
            : base(type, line, position, length, value)
        {
        }
    }
}
