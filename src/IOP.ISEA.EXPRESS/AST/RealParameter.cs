﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// 实数参数
    /// </summary>
    public class RealParameter : UntypeParameter
    {
        /// <summary>
        /// 参数值
        /// </summary>
        public new double ParameterValue { get; protected set; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="line"></param>
        /// <param name="position"></param>
        /// <param name="length"></param>
        /// <param name="parameter"></param>
        public RealParameter(int line, int position, int length, double parameter) 
            : base(line, position, length, parameter, ExpressTokenType.Real)
        {
            ParameterValue = parameter;
            SourceValue = nameof(RealParameter);
        }
    }
}
