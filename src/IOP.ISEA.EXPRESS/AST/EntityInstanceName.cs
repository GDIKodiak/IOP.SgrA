﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// 实体实例名称
    /// </summary>
    public class EntityInstanceName : ParserToken
    {
        /// <summary>
        /// 名称号
        /// </summary>
        public int NameNumber { get; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="line"></param>
        /// <param name="position"></param>
        /// <param name="length"></param>
        /// <param name="value"></param>
        public EntityInstanceName(int line, int position, int length, int value) : 
            base(ExpressTokenType.EntityInstanceName, line, position, length, value)
        {
            NameNumber = value;
        }
    }
}
