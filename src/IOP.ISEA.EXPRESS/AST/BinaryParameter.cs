﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// 二进制参数
    /// </summary>
    public class BinaryParameter : UntypeParameter
    {
        /// <summary>
        /// 参数值
        /// </summary>
        public new string ParameterValue { get; protected set; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="line"></param>
        /// <param name="position"></param>
        /// <param name="length"></param>
        /// <param name="parameter"></param>
        public BinaryParameter(int line, int position, int length, string parameter) 
            : base(line, position, length, parameter, ExpressTokenType.Binary)
        {
            ParameterValue = parameter;
            SourceValue = nameof(BinaryParameter);
        }
    }
}
