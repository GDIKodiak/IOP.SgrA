﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// 头部实体
    /// </summary>
    public class HeaderEntity : ASTNode
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public string Keyword { get; protected set; }
        /// <summary>
        /// 实例记录
        /// </summary>
        public SimpleRecord Record { get; protected set; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="line"></param>
        /// <param name="position"></param>
        /// <param name="length"></param>
        /// <param name="record"></param>
        public HeaderEntity(int line, int position, int length, SimpleRecord record) 
            : base(ExpressTokenType.HeaderEntity, line, position, length, nameof(HeaderEntity))
        {
            Record = record;
            Keyword = record.Keyword;
        }
    }
}
