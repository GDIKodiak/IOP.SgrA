﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// 未定值的参数
    /// </summary>
    public class OmittedParameter : Parameter
    {
        /// <summary>
        /// 参数
        /// </summary>
        public string Parameter { get; } = "*";
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="line"></param>
        /// <param name="position"></param>
        /// <param name="length"></param>
        public OmittedParameter(int line, int position, int length) 
            : base(ExpressTokenType.OmittedParameter, line, position, length, nameof(OmittedParameter))
        {
        }
    }
}
