﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// 复合记录
    /// </summary>
    public class SubsuperRecord : Record
    {
        /// <summary>
        /// 记录列表
        /// </summary>
        public List<SimpleRecord> Records { get; protected set; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="type"></param>
        /// <param name="line"></param>
        /// <param name="position"></param>
        /// <param name="length"></param>
        public SubsuperRecord(int line, int position, int length) 
            : base(ExpressTokenType.SubsuperRecord, line, position, length, nameof(SubsuperRecord))
        {
            Records = new List<SimpleRecord>();
        }
    }
}
