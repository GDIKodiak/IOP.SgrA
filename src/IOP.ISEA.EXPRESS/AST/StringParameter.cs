﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// 字符串参数
    /// </summary>
    public class StringParameter : UntypeParameter
    {
        /// <summary>
        /// 参数值
        /// </summary>
        public new string ParameterValue { get; protected set; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="line"></param>
        /// <param name="position"></param>
        /// <param name="length"></param>
        /// <param name="parameter"></param>
        public StringParameter(int line, int position, int length, string parameter) 
            : base(line, position, length, parameter, ExpressTokenType.String)
        {
            ParameterValue = parameter;
            SourceValue = nameof(StringParameter);
        }
    }
}
