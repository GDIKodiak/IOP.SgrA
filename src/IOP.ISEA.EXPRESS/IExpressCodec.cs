﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// Express编解码器
    /// </summary>
    public interface IExpressCodec
    {
        /// <summary>
        /// 关键字
        /// </summary>
        string Keyword { get; }
        /// <summary>
        /// 解码
        /// </summary>
        /// <param name="sourceEntity"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        Entity Decode(Record sourceEntity, IEXPRESSDocument document);
        /// <summary>
        /// 解码当处于复合实体中时
        /// </summary>
        /// <param name="target"></param>
        /// <param name="body"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        Entity DecodeWithComplex(SimpleRecord target, SubsuperRecord body, IEXPRESSDocument document);
    }
}
