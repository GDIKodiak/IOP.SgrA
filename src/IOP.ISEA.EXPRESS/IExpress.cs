﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// EXPRESS通用标识接口
    /// </summary>
    public interface IExpress
    {
        /// <summary>
        /// 标识关键字
        /// </summary>
        string Keyword { get; }
    }
}
