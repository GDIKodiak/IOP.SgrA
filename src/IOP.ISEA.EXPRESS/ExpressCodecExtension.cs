﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// 扩展
    /// </summary>
    public static class ExpressCodecExtension
    {
        /// <summary>
        /// 将基类参数转换为子类参数
        /// </summary>
        /// <typeparam name="TParmeter"></typeparam>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public static TParmeter As<TParmeter>(this Parameter parameter)
            where TParmeter : Parameter
        {
            if (parameter == null) throw new NullReferenceException(nameof(parameter));
            if (parameter is TParmeter tm) return tm;
            else throw new InvalidCastException($"source parameter is not {typeof(TParmeter).Name}");
        }
        /// <summary>
        /// 判断基类参数是否为某个子类参数
        /// </summary>
        /// <typeparam name="TParameter"></typeparam>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public static bool Is<TParameter>(this Parameter parameter)
            where TParameter : Parameter
        {
            if (parameter == null) throw new NullReferenceException(nameof(parameter));
            if (parameter is TParameter) return true;
            else return false;
        }
        /// <summary>
        /// 是否为可选参数
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public static bool IsOptional(this Parameter parameter)
        {
            if (parameter == null) throw new NullReferenceException(nameof(parameter));
            if (parameter is DefectParameter) return true;
            else return false;
        }
        /// <summary>
        /// 获取字符串参数值
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        /// <exception cref="NullReferenceException"></exception>
        public static string Value(this StringParameter parameter)
        {
            if (parameter == null) throw new NullReferenceException(nameof(parameter));
            return string.IsNullOrEmpty(parameter.ParameterValue) ? string.Empty : parameter.ParameterValue;
        }
        /// <summary>
        /// 获取实数值
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        /// <exception cref="NullReferenceException"></exception>
        public static double Value(this RealParameter parameter)
        {
            if (parameter == null) throw new NullReferenceException(nameof(parameter));
            return parameter.ParameterValue;
        }
        /// <summary>
        /// 将列表参数转换为字符串列表
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public static List<string> AsStringList(this Parameter parameter)
        {
            ListParameter listP = parameter as ListParameter ?? throw new NullReferenceException(nameof(parameter));
            if (listP == null) throw new InvalidCastException($"Cannot cast parameter {parameter} to {listP}");
            List<string> result = new List<string>(listP.ParameterValue.Count);
            foreach(var item in listP.ParameterValue)
            {
                string value = item.As<StringParameter>().Value();
                result.Add(value);
            }
            return result;
        }
        /// <summary>
        /// 将参数列表转换为整数列表
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public static List<int> AsIntegerList(this Parameter parameter)
        {
            ListParameter listP = parameter as ListParameter ?? throw new NullReferenceException(nameof(parameter));
            if (listP == null) throw new InvalidCastException($"Cannot cast parameter {parameter} to {listP}");
            List<int> result = new List<int>(listP.ParameterValue.Count);
            foreach (var item in listP.ParameterValue)
            {
                double value = item.As<RealParameter>().Value();
                result.Add((int)value);
            }
            return result;
        }
        /// <summary>
        /// 将参数列表转换为实数列表
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public static List<double> AsRealList(this Parameter parameter)
        {
            ListParameter listP = parameter as ListParameter ?? throw new NullReferenceException(nameof(parameter));
            if (listP == null) throw new InvalidCastException($"Cannot cast parameter {parameter} to {listP}");
            List<double> result = new List<double>(listP.ParameterValue.Count);
            foreach (var item in listP.ParameterValue)
            {
                double value = item.As<RealParameter>().Value();
                result.Add(value);
            }
            return result;
        }
        /// <summary>
        /// 将列表参数转换为特定实体类型的列表
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public static List<TEntity> AsEntityList<TEntity>(this Parameter parameter, IEXPRESSDocument document)
            where TEntity : Entity
        {
            if (parameter == null) throw new NullReferenceException(nameof(parameter));
            if(!(parameter is ListParameter lp)) throw new InvalidCastException($"Input parameter is not a list parameter");
            List<TEntity> list = new List<TEntity>(lp.ParameterValue.Count);
            foreach(var item in lp.ParameterValue)
            {
                var tEntity = item.AsEnttiy<TEntity>(document);
                list.Add(tEntity);
            }
            return list;
        }
        /// <summary>
        /// 将列表参数转换为三维向量
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public static Vector3 AsVector3(this Parameter parameter, out int dim)
        {
            ListParameter listP = parameter as ListParameter ?? throw new NullReferenceException(nameof(parameter));
            if (listP == null) throw new InvalidCastException($"Cannot cast parameter {parameter} to {listP}");
            Span<float> vs = stackalloc float[3];
            dim = listP.ParameterValue.Count;
            for (int i = 0; i < dim; i++)
            {
                RealParameter real = listP.ParameterValue[i].As<RealParameter>();
                vs[i] = (float)real.ParameterValue;
            }
            return new Vector3(vs[0], vs[1], vs[2]);
        }
        /// <summary>
        /// 将参数转换为实体
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="parameter"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public static TEntity AsEnttiy<TEntity>(this Parameter parameter, IEXPRESSDocument document)
            where TEntity : Entity
        {
            if (parameter.Is<EntityNameParameter>())
            {
                var name = parameter.As<EntityNameParameter>();
                var entity = document.GetEntity(name.ParameterValue);
                if (!(entity.AsSuperTypeRecursion(out TEntity te))) throw new InvalidCastException($"Cast to {typeof(TEntity)} failed");
                return te;
            }
            else throw new InvalidCastException("Mapping Entity must be EntityNameParameter in Parameter list");
        }
        /// <summary>
        /// 将类型参数转为基础类型
        /// </summary>
        /// <typeparam name="TType"></typeparam>
        /// <param name="parameter"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public static TType AsType<TType>(this TypeParameter parameter, IEXPRESSDocument document)
            where TType : IExpress
        {
            var keyword = parameter.Keyword;
            var codec = document.GetTypeCodec(keyword);
            IExpress type = codec.Decode(parameter, document);
            if (!(type is TType tt)) throw new InvalidCastException($"Cast to {typeof(TType)} failed");
            return tt;
        }
        /// <summary>
        /// 将参数转换为选择类型
        /// </summary>
        /// <typeparam name="TSelect"></typeparam>
        /// <param name="parameter"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public static TSelect AsSelect<TSelect>(this Parameter parameter, IEXPRESSDocument document)
            where TSelect : Select, new()
        {
            if (parameter.Is<EntityNameParameter>())
            {
                var name = parameter.As<EntityNameParameter>();
                var entity = document.GetEntity(name.ParameterValue);
                var select = Select.CreateSelect<TSelect>(entity);
                return select;
            }
            else if (parameter.Is<TypeParameter>())
            {
                var type = parameter.As<TypeParameter>();
                var codec = document.GetTypeCodec(type.Keyword);
                IExpress typeE = codec.Decode(type, document);
                var select = Select.CreateSelect<TSelect>(typeE);
                return select;
            }
            else throw new InvalidCastException($"Parameter {parameter.TokenType} cannot coverted to select");
        }
        /// <summary>
        /// 转换为枚举
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public static TEnum AsEnumeration<TEnum>(this Parameter parameter)
            where TEnum : struct, Enum
        {
            if (parameter == null) throw new NullReferenceException(nameof(parameter));
            if (!(parameter is EnumerationParameter enumeration)) throw new InvalidCastException("Paramter is not a enumeration");
            TEnum @enum = Enum.Parse<TEnum>(enumeration.ParameterValue);
            return @enum;
        }
        /// <summary>
        /// 转换为逻辑值
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public static bool AsLogical(this Parameter parameter)
        {
            if (parameter == null) throw new NullReferenceException(nameof(parameter));
            if(!(parameter is EnumerationParameter enumeration)) throw new InvalidCastException("Paramter is not a enumeration");
            Logical logical = Enum.Parse<Logical>(enumeration.ParameterValue);
            if (logical == Logical.T) return true;
            else return false;
        }
    }
}
