﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// EXPRESS类型编解码器
    /// </summary>
    public interface IExpressTypeCodec
    {
        /// <summary>
        /// 关键字
        /// </summary>
        string Keyword { get; }
        /// <summary>
        /// 解码
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        IExpress Decode(TypeParameter parameter, IEXPRESSDocument document);
    }
}
