﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// 实体共有基类
    /// </summary>
    public abstract class Entity : IExpress
    {
        /// <summary>
        /// 实体关键字
        /// </summary>
        public abstract string Keyword { get; }
        /// <summary>
        /// 子类字典
        /// </summary>
        protected readonly Dictionary<Type, Entity> SubTypes = new Dictionary<Type, Entity>();
        /// <summary>
        /// 父类字典
        /// </summary>
        protected readonly Dictionary<Type, Entity> SuperTypes = new Dictionary<Type, Entity>();
        /// <summary>
        /// 确认某实体是否为当前实体的父实体
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public abstract bool IsSuperType<TEntity>()
            where TEntity : Entity;
        /// <summary>
        /// 确认某实体是否为当前实体的子类实体
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public abstract bool IsSubType<TEntity>()
            where TEntity : Entity;
        /// <summary>
        /// 设置父对象
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="parent"></param>
        protected virtual void SetSuperType<TEntity>(TEntity parent)
            where TEntity : Entity
        {
            if (IsSuperType<TEntity>())
            {
                SuperTypes.Add(typeof(TEntity), parent);
                parent.SubTypes.Add(GetType(), this);
            }
            else throw new InvalidCastException($"Entity {typeof(TEntity).Name} is not {GetType().Name}'s SuperType");
        }
        /// <summary>
        /// 转换为父类型
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        public virtual TEntity AsSuperType<TEntity>()
            where TEntity : Entity
        {
            if(SuperTypes.TryGetValue(typeof(TEntity), out Entity entity))
            {
                return entity as TEntity;
            }
            throw new KeyNotFoundException($"Cannot found SuperType {typeof(TEntity).Name} from {Keyword}");
        }
        /// <summary>
        /// 转换为子类型
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public virtual TEntity AsSubType<TEntity>()
            where TEntity : Entity
        {
            if(SubTypes.TryGetValue(typeof(TEntity), out Entity entity))
            {
                return entity as TEntity;
            }
            throw new KeyNotFoundException($"Cannot found SubType {typeof(TEntity).Name} from {Keyword}");
        }
        /// <summary>
        /// 递归转换为父类型
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public virtual bool AsSuperTypeRecursion<TEntity>(out TEntity entity)
            where TEntity : Entity
        {
            entity = null;
            var type = typeof(TEntity);
            if (type == GetType())
            {
                entity = this as TEntity;
                return true;
            }
            foreach (var super in SuperTypes)
            {
                if (super.Key == type)
                {
                    entity = super.Value as TEntity;
                    return true;
                }
                else
                {
                    if(super.Value.AsSuperTypeRecursion(out entity))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
