﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipelines;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Buffers;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// ISO10303-21文件读取器
    /// </summary>
    public class EXPRESSFileReader : IDisposable
    {
        /// <summary>
        /// 当前读取文件位置
        /// </summary>
        public long Position { get => Stream != null ? Stream.Position : -1; }
        /// <summary>
        /// 文件信息
        /// </summary>
        private FileInfo FileInfo { get; set; } = null;
        /// <summary>
        /// 流
        /// </summary>
        private FileStream Stream { get; set; } = null;
        /// <summary>
        /// Pipeline
        /// </summary>
        private Pipe Pipe { get; set; } = null;
        /// <summary>
        /// 取消令牌
        /// </summary>
        private CancellationTokenSource TokenSource { get; set; } = null;

        public EXPRESSFileReader(string path)
        {
            if (string.IsNullOrEmpty(path)) throw new ArgumentNullException(nameof(path));
            FileInfo fileInfo = new FileInfo(path);
            if (!fileInfo.Exists) throw new FileNotFoundException("Cannot found file", path);
            FileInfo = fileInfo;
            var fileStream = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.Read);
            Stream = fileStream;
        }

        /// <summary>
        /// 读取文件
        /// </summary>
        /// <returns></returns>
        public async Task ReadAsync(IEXPRESSDocument document)
        {
            Pipe = new Pipe();
            TokenSource = new CancellationTokenSource();
            _ = WritePipeline(Pipe.Writer, Stream, TokenSource);
            await ReadPipeline(Pipe.Reader, document, TokenSource);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            if (Stream != null)
            {
                Stream.Close();
                Stream.Dispose();
            }
        }

        /// <summary>
        /// 写入管线
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="stream"></param>
        /// <returns></returns>
        private async Task WritePipeline(PipeWriter writer, FileStream stream, CancellationTokenSource source)
        {
            try
            {
                while (!source.IsCancellationRequested)
                {
                    Memory<byte> memory = writer.GetMemory(2048);
                    int bytesRead = await stream.ReadAsync(memory);
                    writer.Advance(bytesRead);
                    if (bytesRead == 0) 
                        break;
                    FlushResult result = await writer.FlushAsync();
                    if (result.IsCompleted) break;
                }

            }
            catch (OperationCanceledException) { }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                writer.Complete();
            }
        }
        /// <summary>
        /// 读取管线
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        private async Task ReadPipeline(PipeReader reader, IEXPRESSDocument document, 
            CancellationTokenSource source)
        {
            try
            {
                StringBuilder builder = new StringBuilder();
                int lineIndex = -1;
                while (!source.IsCancellationRequested)
                {
                    ReadResult result = await reader.ReadAsync();
                    ReadOnlySequence<byte> buffer = result.Buffer;
                    do
                    {
                        (string local, bool isWhole) = ParseLine(ref buffer, ref lineIndex);
                        if (string.IsNullOrEmpty(local)) 
                            break;
                        else
                        {
                            builder.Append(local);
                            if (isWhole)
                            {
                                document.ParseLine(lineIndex, builder.ToString());
                                builder.Clear();
                            }
                        }
                    }
                    while (!buffer.IsEmpty);
                    reader.AdvanceTo(buffer.Start);
                    if (result.IsCompleted) break;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                reader.Complete();
            }
        }
        /// <summary>
        /// 解析一行
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="lineIndex"></param>
        /// <returns></returns>
        private (string, bool) ParseLine(ref ReadOnlySequence<byte> buffer, ref int lineIndex)
        {
            ReadOnlySequence<byte> local;
            SequencePosition? position = buffer.PositionOf((byte)'\n');
            bool isWhole = false;
            if (position != null)
            {
                local = buffer.Slice(0, buffer.GetPosition(1, position.Value));
                string line = GetUTF8String(in local);
                lineIndex++;
                if (line.Contains(";\r\n") || line.Contains(";\n")) isWhole = true;
                buffer = buffer.Slice(buffer.GetPosition(1, position.Value));
                return (line, isWhole);
            }
            return (null, isWhole);
        }
        /// <summary>
        /// 生成UTF8字符串
        /// </summary>
        /// <param name="buffers"></param>
        /// <returns></returns>
        private static string GetUTF8String(in ReadOnlySequence<byte> buffers)
        {
            if (buffers.IsSingleSegment) return Encoding.UTF8.GetString(buffers.First.ToArray());
            else
            {
                return string.Create((int)buffers.Length, buffers, (span, sequence) =>
                {
                    foreach (var segment in sequence)
                    {
                        Encoding.UTF8.GetChars(segment.Span, span);
                        span = span.Slice(segment.Length);
                    }
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static async Task<EXPRESSFileReader> ReadFromFile(string path, IEXPRESSDocument document)
        {
            if(string.IsNullOrEmpty(path)) throw new ArgumentNullException(nameof(path));
            if(document == null) throw new ArgumentNullException(nameof(document));
            var reader = new EXPRESSFileReader(path);
            await reader.ReadAsync(document);
            return reader;
        }
    }
}
