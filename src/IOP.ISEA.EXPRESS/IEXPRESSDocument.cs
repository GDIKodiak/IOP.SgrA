﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// Step文档接口
    /// </summary>
    public interface IEXPRESSDocument
    {
        /// <summary>
        /// 获取抽象语法树节点
        /// </summary>
        /// <param name="entityName"></param>
        /// <returns></returns>
        ASTNode GetASTNode(int entityName);
        /// <summary>
        /// 尝试获取抽象语法树节点
        /// </summary>
        /// <param name="entityName"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool TryGetASTNode(int entityName, out ASTNode node);
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="entityName"></param>
        /// <returns></returns>
        Entity GetEntity(int entityName);
        /// <summary>
        /// 遍历当前文档所有实体
        /// </summary>
        /// <returns></returns>
        IEnumerable<Entity> ForeachEntities();
        /// <summary>
        /// 尝试获取实体
        /// </summary>
        /// <param name="entityName"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool TryGetEntity(int entityName, out Entity entity);
        /// <summary>
        /// 从抽象语法树解码实体
        /// </summary>
        /// <param name="entityName"></param>
        /// <returns></returns>
        Entity DecodeFromAST(int entityName);
        /// <summary>
        /// 尝试从抽象语法树解码实体
        /// </summary>
        /// <param name="entityName"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool TryDecodeFromAST(int entityName, out Entity entity);
        /// <summary>
        /// 添加实体编解码器
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="codec"></param>
        void AddEntityCodec(string keyword, IExpressCodec codec);
        /// <summary>
        /// 添加类型编解码器
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="codec"></param>
        void AddTypeCodec(string keyword, IExpressTypeCodec codec);
        /// <summary>
        /// 获取实体解码器
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        IExpressCodec GetEntityCodec(string keyword);
        /// <summary>
        /// 获取类型编解码器
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        IExpressTypeCodec GetTypeCodec(string keyword);
        /// <summary>
        /// 从字符串中解析一行
        /// </summary>
        /// <param name="line"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        ASTNode ParseLine(int line, string source);
    }
}
