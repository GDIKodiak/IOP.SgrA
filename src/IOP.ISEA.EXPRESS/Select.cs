﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// 先择数据类型
    /// </summary>
    public abstract class Select : IExpress
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public abstract string Keyword { get; }
        /// <summary>
        /// 选择列表
        /// </summary>
        protected readonly Dictionary<Type, IExpress> SelectList = new Dictionary<Type, IExpress>();

        /// <summary>
        /// 是否为选择项
        /// </summary>
        /// <param name="express"></param>
        /// <returns></returns>
        public abstract bool IsSelectItem(IExpress express);
        /// <summary>
        /// 当前选择类型是否包含特定EXPRESS类型
        /// </summary>
        /// <typeparam name="TExpress"></typeparam>
        /// <returns></returns>
        public virtual bool Contains<TExpress>() where TExpress : IExpress => SelectList.ContainsKey(typeof(TExpress));
        /// <summary>
        /// 遍历选择列表
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<IExpress> ForeachSelectList()
        {
            foreach(var item in SelectList.Values)
            {
                if (item is Select select)
                {
                    foreach(var child in select.ForeachSelectList())
                    {
                        yield return child;
                    }
                }
                else yield return item;
            }
        }
        /// <summary>
        /// 添加选择项
        /// </summary>
        /// <param name="express"></param>
        public virtual void AddItem(IExpress express)
        {
            if (IsSelectItem(express)) SelectList.TryAdd(express.GetType(), express);
            else throw new InvalidCastException($"{express.Keyword} is not belong to select {Keyword}");
        }
        /// <summary>
        /// 尝试获取选择项
        /// </summary>
        /// <typeparam name="TExpress"></typeparam>
        /// <returns></returns>
        public virtual bool TryGetSelectItem<TExpress>(out TExpress express)
            where TExpress : class, IExpress
        {
            if(SelectList.TryGetValue(typeof(TExpress), out IExpress e))
            {
                express = e as TExpress;
                return true;
            }
            else
            {
                express = default;
                return false;
            }
        }
        /// <summary>
        /// 创建选择类型
        /// </summary>
        /// <param name="expresses"></param>
        /// <returns></returns>
        public abstract Select CreateFrom(params IExpress[] expresses);

        /// <summary>
        /// 创建选择类型数据
        /// </summary>
        /// <typeparam name="TSelect"></typeparam>
        /// <param name="document"></param>
        /// <param name="expresses"></param>
        /// <returns></returns>
        public static TSelect CreateSelect<TSelect>(params IExpress[] expresses)
            where TSelect : Select, new()
        {
            var select = new TSelect();
            return select.CreateFrom(expresses) as TSelect;
        }
    }
}
