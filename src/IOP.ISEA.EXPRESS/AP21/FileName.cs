﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// 文件名
    /// </summary>
    public class FileName : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => SchemaAP21.FILE_NAME;
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 时间戳字符串
        /// </summary>
        public string TimeStamp { get; set; }
        /// <summary>
        /// 作者
        /// </summary>
        public List<string> Author { get; set; } = new List<string>();
        /// <summary>
        /// 组织
        /// </summary>
        public List<string> Organization { get; set; } = new List<string>();
        /// <summary>
        /// 
        /// </summary>
        public string PreprocessorVersion { get; set; }
        /// <summary>
        /// 数组来源
        /// </summary>
        public string OriginationSystem { get; set; }
        /// <summary>
        /// 负责人
        /// </summary>
        public string Authorization { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>() => false;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => false;
    }

    /// <summary>
    /// 文件名解码器
    /// </summary>
    public class FileNameCodec : IExpressCodec
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public string Keyword => SchemaAP21.FILE_NAME;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceEntity"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public Entity Decode(Record sourceEntity, IEXPRESSDocument document)
        {
            if (!(sourceEntity is SimpleRecord headerEntity)) throw new InvalidCastException("Target AST entity is not header entity");
            var record = headerEntity ?? throw new NullReferenceException(nameof(Record));
            var keyword = record.Keyword;
            if (keyword != Keyword) throw new InvalidCastException($"Target AST entity is not {SchemaAP21.FILE_NAME}");
            if (record.Parameters.Count != 7) throw new InvalidCastException($"{SchemaAP21.FILE_NAME} Attributes lost");
            string name = record.Parameters[0].As<StringParameter>().Value();
            string stamp = record.Parameters[1].As<StringParameter>().Value();
            List<string> author = record.Parameters[2].AsStringList();
            List<string> organization = record.Parameters[3].AsStringList();
            string prop = record.Parameters[4].As<StringParameter>().Value();
            string system = record.Parameters[5].As<StringParameter>().Value();
            string authorzation = record.Parameters[6].As<StringParameter>().Value();
            FileName fileName = new FileName
            {
                Name = name,
                TimeStamp = stamp,
                PreprocessorVersion = prop,
                OriginationSystem = system,
                Authorization = authorzation
            };
            fileName.Author.AddRange(author);
            fileName.Organization.AddRange(organization);
            return fileName;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        /// <param name="body"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public Entity DecodeWithComplex(SimpleRecord target, SubsuperRecord body, IEXPRESSDocument document)
        {
            throw new InvalidOperationException($"{Keyword} is must be simple entity");
        }
    }
}
