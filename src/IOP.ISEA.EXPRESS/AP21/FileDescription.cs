﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// 文件描述
    /// </summary>
    public class FileDescription : Entity
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public override string Keyword => SchemaAP21.FILE_DESCRIPTION;
        /// <summary>
        /// 描述列表
        /// </summary>
        public List<string> Description { get; private set; } = new List<string>();
        /// <summary>
        /// 一致性级别
        /// </summary>
        public string ImplementationLevel { get; set; } = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>() => false;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => false;
    }

    /// <summary>
    /// 文件描述解码器
    /// </summary>
    public class FileDescriptionCodec : IExpressCodec
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public string Keyword => SchemaAP21.FILE_DESCRIPTION;
        /// <summary>
        /// 解码
        /// </summary>
        /// <param name="sourceEntity"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Entity Decode(Record sourceEntity, IEXPRESSDocument document)
        {
            if (!(sourceEntity is SimpleRecord headerEntity)) throw new InvalidCastException("Target AST entity is not header entity");
            var record = headerEntity ?? throw new NullReferenceException(nameof(Record));
            var keyword = record.Keyword;
            if (keyword != Keyword) throw new InvalidCastException($"Target AST entity is not {SchemaAP21.FILE_DESCRIPTION}");
            if (record.Parameters.Count != 2) throw new InvalidCastException($"{SchemaAP21.FILE_DESCRIPTION} Attributes lost");
            var descriptions = record.Parameters[0].AsStringList();
            var level = record.Parameters[1].As<StringParameter>().Value();
            FileDescription description = new FileDescription();
            description.Description.AddRange(descriptions);
            description.ImplementationLevel = level;
            return description;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        /// <param name="body"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        public Entity DecodeWithComplex(SimpleRecord target, SubsuperRecord body, IEXPRESSDocument document)
        {
            throw new InvalidOperationException($"{Keyword} is must be simple entity");
        }
    }
}
