﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// 文件模式
    /// </summary>
    public class FileSchema : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => SchemaAP21.FILE_SCHEMA;
        /// <summary>
        /// 模式标识符
        /// </summary>
        public List<string> SchemaIdentifiers { get; set; } = new List<string>();
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>() => false;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => false;
    }

    /// <summary>
    /// 文件模式编解码器
    /// </summary>
    public class FileSchemaCodec : IExpressCodec
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public string Keyword => SchemaAP21.FILE_SCHEMA;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceEntity"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public Entity Decode(Record sourceEntity, IEXPRESSDocument document)
        {
            if (!(sourceEntity is SimpleRecord headerEntity)) throw new InvalidCastException("Target AST entity is not header entity");
            var record = headerEntity ?? throw new NullReferenceException(nameof(Record));
            var keyword = record.Keyword;
            if (keyword != Keyword) throw new InvalidCastException($"Target AST entity is not {SchemaAP21.FILE_SCHEMA}");
            if (record.Parameters.Count != 1) throw new InvalidCastException($"{SchemaAP21.FILE_SCHEMA} Attributes lost");
            List<string> list = record.Parameters[0].AsStringList();
            FileSchema schema = new FileSchema
            {
                SchemaIdentifiers = list
            };
            return schema;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        /// <param name="body"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public Entity DecodeWithComplex(SimpleRecord target, SubsuperRecord body, IEXPRESSDocument document)
        {
            throw new InvalidOperationException($"{Keyword} is must be simple entity");
        }
    }
}
