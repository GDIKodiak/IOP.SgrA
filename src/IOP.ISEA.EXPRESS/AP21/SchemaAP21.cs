﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// AP21纲要
    /// </summary>
    public static class SchemaAP21
    {
        /// <summary>
        /// 文件描述
        /// </summary>
        public const string FILE_DESCRIPTION = "FILE_DESCRIPTION";
        /// <summary>
        /// 文件名
        /// </summary>
        public const string FILE_NAME = "FILE_NAME";
        /// <summary>
        /// 文件模式
        /// </summary>
        public const string FILE_SCHEMA = "FILE_SCHEMA";
        /// <summary>
        /// 复合实体
        /// </summary>
        public const string SUBSUPERRECORD = "SUBSUPERRECORD";
        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        public static void AddSchemaAP21Codecs(this IEXPRESSDocument document)
        {
            document.AddEntityCodec(FILE_DESCRIPTION, new FileDescriptionCodec());
            document.AddEntityCodec(FILE_NAME, new FileNameCodec());
            document.AddEntityCodec(FILE_SCHEMA, new FileSchemaCodec());
            document.AddEntityCodec(SUBSUPERRECORD, new SubsuperRecordCodec());
        }
    }
}
