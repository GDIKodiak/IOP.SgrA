﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.EXPRESS
{
    /// <summary>
    /// 逻辑枚举
    /// </summary>
    public enum Logical
    {
        /// <summary>
        /// True
        /// </summary>
        T,
        /// <summary>
        /// False
        /// </summary>
        F
    }
}
