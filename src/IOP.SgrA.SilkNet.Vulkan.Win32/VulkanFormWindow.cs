﻿using IOP.Extension.DependencyInjection;
using IOP.SgrA.Input;
using Microsoft.Extensions.Logging;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IOP.SgrA.SilkNet.Vulkan.Win32
{
    public class VulkanFormWindow : VulkanRenderWapper, IGenericWindow
    {
        /// <summary>
        /// 渲染区域
        /// </summary>
        public override Area RenderArea { get => _Area; set => _Area = value; }
        /// <summary>
        /// 窗口句柄
        /// </summary>
        public IntPtr WindowHandle { get; protected internal set; } = IntPtr.Zero;
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; protected internal set; }
        /// <summary>
        /// 窗口
        /// </summary>
        public Form Form { get; protected internal set; }
        /// <summary>
        /// 配置项
        /// </summary>
        public WindowOption Option
        {
            get => _Option;
            protected internal set
            {
                if (value != null)
                {
                    _Option = value;
                    _Area = new Area(0, 0, value.Width, value.Height);
                }
            }
        }
        /// <summary>
        /// 宽纵比
        /// </summary>
        public float Aspect { get; private set; }
        /// <summary>
        /// 日志
        /// </summary>
        public ILogger<VulkanFormWindow> Logger { get; protected set; }
        /// <summary>
        /// 输入
        /// </summary>
        public InputStateController Input { get; protected set; } = new InputStateController();
        /// <summary>
        /// FPS工具类
        /// </summary>
        public FPSUtil FPS { get; protected set; }
        /// <summary>
        /// 窗口大小变更事件
        /// </summary>
        public event EventHandler<EventArgs> Resize;
        /// <summary>
        /// 键盘事件
        /// </summary>
        public event EventHandler<KeyboardEventArgs> PressKey;
        /// <summary>
        /// 鼠标按钮触发事件
        /// </summary>
        public event EventHandler<MouseButtonEventArgs> MouseButton;
        /// <summary>
        /// 当加载结束时
        /// </summary>
        public event EventHandler<EventArgs> Loaded;
        /// <summary>
        /// 鼠标移动事件
        /// </summary>
        public event EventHandler<MouseMoveEventArgs> MouseMove;
        /// <summary>
        /// 是否拥有呈现曲面
        /// </summary>
        /// <returns></returns>
        public override bool HasVulkanSurface() => Surface.Handle != 0;
        /// <summary>
        /// 获取呈现曲面
        /// </summary>
        /// <returns></returns>
        public override SurfaceKHR GetVulkanSurface() => Surface;
        /// <summary>
        /// 
        /// </summary>
        [Autowired]
        protected ILoggerFactory LoggerFactory { get; set; }
        /// <summary>
        /// 光标坐标
        /// </summary>
        protected Vector2 Position { get; set; }
        /// <summary>
        /// 表面
        /// </summary>
        protected internal SurfaceKHR Surface { get; set; }
        /// <summary>
        /// 当窗口大小发生变更时
        /// </summary>
        protected Action<VulkanFormWindow, uint, uint> Resized;
        /// <summary>
        /// 配置任务
        /// </summary>
        internal Func<Task> ConfigTask { get; set; }

        private Area _Area;
        private WindowOption _Option = new WindowOption();

        /// <summary>
        /// 初始化
        /// </summary>
        public override void Initialization()
        {
            var form = new Form();
            form.Width = (int)Option.Height;
            form.Height = (int)Option.Width;
            form.Resize += Form_Resize;
            form.KeyDown += Form_KeyDown;
            form.KeyUp += Form_KeyUp;
            form.MouseClick += Form_Click;
            WindowHandle = form.Handle;
            form.Load += Form_Load;
            Form = form;
            Logger = LoggerFactory.CreateLogger<VulkanFormWindow>();
        }

        /// <summary>
        /// 当窗口发生变更时
        /// </summary>
        /// <param name="args"></param>
        public virtual void OnResize(EventArgs args) => Resize?.Invoke(this, args);
        /// <summary>
        /// 键盘事件
        /// </summary>
        /// <param name="args"></param>
        public virtual void OnPressKey(KeyboardEventArgs args) => PressKey?.Invoke(this, args);
        /// <summary>
        /// 当鼠标发送按钮事件时
        /// </summary>
        /// <param name="args"></param>
        public virtual void OnMouseButton(MouseButtonEventArgs args) => MouseButton?.Invoke(this, args);
        /// <summary>
        /// 当鼠标移动时
        /// </summary>
        /// <param name="args"></param>
        public virtual void OnMouseMove(MouseMoveEventArgs args) => MouseMove?.Invoke(this, args);
        /// <summary>
        /// 销毁资源
        /// </summary>
        public void Dispose()
        {
            Form.Dispose();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        public virtual void OnLoaded(EventArgs args)
        {
            Loaded?.Invoke(this,args);
        }
        /// <summary>
        /// 运行配置任务
        /// </summary>
        /// <returns></returns>
        public Task RunConfigTask() => ConfigTask() ?? Task.CompletedTask;
        /// <summary>
        /// 运行窗口
        /// </summary>
        public void RunWindow() => RunWindow(120);
        /// <summary>
        /// 运行窗口
        /// </summary>
        /// <param name="updateRate"></param>
        public void RunWindow(double updateRate)
        {
            FPS = new FPSUtil(updateRate);
            var startTime = DateTime.Now;
            var endTime = startTime.AddSeconds(5);
            Form.Show();
            while (!Form.IsDisposed)
            {
                try
                {
                    FPS.Begin();
                    RenderDispatcher.Rendering();
                    Input.RefreshMouseStateFrame();
                    Input.RefreshKeyboardFrame();
                    Application.DoEvents();
                    GetCursorPosition();
                    FPS.Yield();
                    FPS.CallFPS();
                    RenderDispatcher.SendTimeStamp(FPS.GetLastStamp());
                    startTime = DateTime.Now;
                    if (startTime > endTime)
                    {
                        Logger.LogInformation($"FPS: {FPS.CurrentFPS}");
                        endTime = startTime.AddSeconds(5);
                    }
                }
                catch(Exception e)
                {
                    Logger.LogError(e.Message + "\r\n" + e.StackTrace);
                    break;
                }
            }
        }
        /// <summary>
        /// 当窗口被关闭时
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnExit(object sender, FormClosedEventArgs e)
        {
            Dispose();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_Resize(object sender, EventArgs e)
        {
            var size = Form.Size;
            uint width = (uint)size.Width;
            uint height = (uint)size.Height;
            Option.Width = width;
            Option.Height = height;
            _Area.Height = height;
            _Area.Width = width;
            RenderDispatcher.Stop();
            if (size.Width == 0 || size.Height == 0) return;
            Resized?.Invoke(this, width, height);
            OnResize(new EventArgs());
            RenderDispatcher.Start();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_KeyDown(object sender, KeyEventArgs e)
        {
            PressKey pKey = (PressKey)e.KeyCode;
            InputAction sAction = InputAction.Press;
            InputMods sMods = (InputMods)e.Modifiers;
            KeyboardEventArgs args = new KeyboardEventArgs(pKey, sAction, sMods);
            Input.KeyboardStateChanged(new PressKeyStateFrame(DateTime.Now, pKey, sAction, sMods));
            OnPressKey(args);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_KeyUp(object sender, KeyEventArgs e)
        {
            PressKey pKey = (PressKey)e.KeyCode;
            InputAction sAction = InputAction.Release;
            InputMods sMods = (InputMods)e.Modifiers;
            KeyboardEventArgs args = new KeyboardEventArgs(pKey, sAction, sMods);
            Input.KeyboardStateChanged(new PressKeyStateFrame(DateTime.Now, pKey, sAction, sMods));
            OnPressKey(args);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_Click(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            MouseButton sButton = (MouseButton)e.Button;
            InputAction sAction = (InputAction)e.Clicks;
            InputMods sMods = InputMods.None;
            MouseButtonEventArgs args = new MouseButtonEventArgs((int)Position.X, (int)Position.Y, sButton, sAction, sMods);
            Input.MouseButtonStateChanged(new MouseStateFrame(DateTime.Now, sButton, sAction, sMods));
            OnMouseButton(args);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_Load(object sender, EventArgs e)
        {
            OnLoaded(e);
        }
        /// <summary>
        /// 获取实时鼠标坐标
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void GetCursorPosition()
        {
            Vector2 old = Position;
            var p = Control.MousePosition;
            double x = p.X;
            double y = p.Y;
            Vector2 newP = new Vector2((float)x, (float)y);
            Vector2 diff = old - newP;
            if (diff.X != 0 || diff.Y != 0)
            {
                MouseMoveEventArgs args = new MouseMoveEventArgs((int)x, (int)y, (int)diff.X, (int)diff.Y);
                Position = newP;
                Input.MouseState.X = (int)x;
                Input.MouseState.Y = (int)y;
                Input.MouseState.XDelta = (int)diff.X;
                Input.MouseState.YDelta = (int)diff.Y;
                OnMouseMove(args);
            }
            else
            {
                Input.MouseState.XDelta = 0;
                Input.MouseState.YDelta = 0;
            }
        }
    }
}
