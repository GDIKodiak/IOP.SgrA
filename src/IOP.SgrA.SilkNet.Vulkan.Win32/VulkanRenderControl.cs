﻿using IOP.Extension.DependencyInjection;
using IOP.SgrA.ECS;
using IOP.SgrA.Input;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IOP.SgrA.SilkNet.Vulkan.Win32
{
    /// <summary>
    /// VulkanRenderControl.xaml 的交互逻辑
    /// </summary>
    [TemplatePart(Name = "PART_Image", Type = typeof(System.Windows.Controls.Image))]
    public partial class VulkanRenderControl : ContentControl
    {
        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyProperty TargetProperty = DependencyProperty.Register("Target", typeof(WriteableBitmap), typeof(VulkanRenderControl),
            new PropertyMetadata(null));
        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyProperty IntervalModeProperty = DependencyProperty.Register("IntervalMode", typeof(ThreadIntervalMode), typeof(VulkanRenderControl), 
            new FrameworkPropertyMetadata(ThreadIntervalMode.YieldMode, IntervalModeCallback));

        /// <summary>
        /// 渲染区域
        /// </summary>
        public Area RenderArea { get; protected set; }
        /// <summary>
        /// 调度器
        /// </summary>
        public VulkanOffScreenRenderDispatcher RenderDispatcher { get; protected set; }
        /// <summary>
        /// FPS工具类
        /// </summary>
        public FPSUtil FPS { get; protected set; }
        /// <summary>
        /// 渲染目标
        /// </summary>
        public WriteableBitmap Target 
        {
            get { return (WriteableBitmap)GetValue(TargetProperty); }
            set 
            {
                if (value == null) throw new NullReferenceException();
                SetValue(TargetProperty, value);
                BufferStride = (uint)value.BackBufferStride;
                BufferHeight = (uint)value.PixelHeight;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public ThreadIntervalMode IntervalMode
        {
            get { return (ThreadIntervalMode)GetValue(IntervalModeProperty); }
            set { SetValue(IntervalModeProperty, value); }
        }
        /// <summary>
        /// 输入
        /// </summary>
        public InputStateController Input { get; protected set; } = new InputStateController();
        /// <summary>
        /// 服务
        /// </summary>
        protected IServiceProvider ServiceProvider { get; set; }
        /// <summary>
        /// Vulkan绘图管理器
        /// </summary>
        protected VulkanGraphicsManager GraphicsManager { get; set; }
        /// <summary>
        /// 调度器
        /// </summary>
        protected VulkanRenderWapper RenderWapper { get; set; }
        /// <summary>
        /// 光标坐标
        /// </summary>
        protected Vector2 Position { get; set; }

        private DpiScale Dpi = new DpiScale(96, 96);
        private uint BufferStride = 0;
        private uint BufferHeight = 0;
        private volatile bool RenderIsLoaded = false;
        private bool Fource = false;
        private int stop = 0;
        private System.Windows.Controls.Image TargetImage;
        private CancellationTokenSource CancellationToken = new CancellationTokenSource();
        private ThreadIntervalMode LocalMode = ThreadIntervalMode.YieldMode;

        /// <summary>
        /// 
        /// </summary>
        public VulkanRenderControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(VulkanRenderControl), new FrameworkPropertyMetadata(typeof(VulkanRenderControl)));
            Dpi = VisualTreeHelper.GetDpi(this);
        }
        /// <summary>
        /// 初始化渲染器
        /// </summary>
        /// <param name="provider"></param>
        public virtual void InitializationRender(IServiceProvider provider, uint attachIndex = 0, params Type[] modules)
        {
            if (provider == null) throw new ArgumentNullException(nameof(provider));
            if (TargetImage == null) throw new InvalidOperationException("Invalid Template");
            ServiceProvider = provider;
            IGraphicsManager manager = provider.GetRequiredService<IGraphicsManager>();
            if (manager == null) throw new NullReferenceException("No Graphics Service");
            if (manager is not VulkanGraphicsManager graphicsManager) throw new NotSupportedException("No Vulkan Environment");
            GraphicsManager = graphicsManager;
            if(RenderDispatcher == null)
            {
                var device = graphicsManager.VulkanDevice;
                if (device == null) throw new InvalidOperationException("Please create device first");
                var queue = device.WorkQueues.Where(x => x.Type.HasFlag(QueueType.Graphics)).FirstOrDefault();
                if (queue == null) throw new NotSupportedException("Target Phydevice is not supported to Graphics");
                if (Target == null)
                {
                    Target = new WriteableBitmap((int)RenderArea.Width, (int)RenderArea.Height, Dpi.DpiScaleX, Dpi.DpiScaleY, PixelFormats.Bgra32, null);
                }
                TargetImage.Source = Target;
                var pass = CreateOffScreenRenderpass();
                var semaphore = device.CreateSemaphore();
                var dispatcher = CreateOffScreenRenderDispatcher(queue, pass, semaphore, 0);
                var wapper = CreateRenderWapper(provider, dispatcher, modules);
                RenderWapper = wapper;
                Focusable = true;
                RenderIsLoaded = true;
            }
        }
        /// <summary>
        /// 配置渲染通道信息
        /// </summary>
        /// <param name="action"></param>
        public virtual void ConfigRenderpassBeginInfo(Action<RenderPassBeginOption> action)
        {
            var pass = RenderDispatcher.OffScreenRenderPass;
            if (pass == null) throw new InvalidOperationException("Please create pass first");
            pass.ConfigBeginInfo(action);
        }
        /// <summary>
        /// 开始渲染
        /// </summary>
        /// <param name="updateRate"></param>
        public virtual void BeginRendering(double updateRate = 120)
        {
            CancellationToken = new CancellationTokenSource();
            Task.Factory.StartNew(async () => 
            {
                var logger = GraphicsManager.Logger;
                try
                {
                    var wapper = RenderWapper;
                    await wapper.LoadRenderModules();
                    RenderIsLoaded = true;
                    Interlocked.Exchange(ref stop, 0);
                    RenderDispatcher.Start();
                    Rendering(updateRate);
                }
                catch (Exception e)
                {
                    logger.LogError(e.Message + "\r\n" + e.StackTrace);
                }
            }, CancellationToken.Token);
        }
        /// <summary>
        /// 继续渲染
        /// </summary>
        public virtual void Continue()
        {
            Interlocked.Exchange(ref stop, 0);
            RenderDispatcher.Start();
        }
        /// <summary>
        /// 中断渲染
        /// </summary>
        public virtual void Interrupt()
        {
            Interlocked.Exchange(ref stop, 1);
            RenderDispatcher.Stop();
            GraphicsManager.VulkanDevice.WaitIdle();
        }
        /// <summary>
        /// 停止渲染
        /// </summary>
        public virtual void Stop()
        {
            Interlocked.Exchange(ref stop, 1);
            RenderDispatcher.Stop();
            CancellationToken.Cancel();
            GraphicsManager.VulkanDevice.WaitIdle();
        }

        /// <summary>
        /// 销毁
        /// </summary>
        public virtual void Destroy()
        {
            Stop();
            RenderIsLoaded = false;
            RenderDispatcher.DestroyDispatcher();
        }
        /// <summary>
        /// 
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            TargetImage = GetTemplateChild("PART_Image") as System.Windows.Controls.Image;
        }

        /// <summary>
        /// 执行渲染
        /// </summary>
        /// <param name="updateRate"></param>
        protected virtual void Rendering(double updateRate = 120)
        {
            FPS = new FPSUtil(updateRate);
            var startTime = DateTime.Now;
            var endTime = startTime.AddSeconds(5);
            var logger = GraphicsManager.Logger;
            SpinWait wait = new SpinWait();
            while (RenderIsLoaded && !CancellationToken.IsCancellationRequested)
            {
                int s = stop;
                if (Interlocked.CompareExchange(ref stop, s, s) != s)
                {
                    wait.SpinOnce();
                    continue;
                }
                if (s == 1)
                {
                    FPS.Sleep();
                    continue;
                }
                try
                {
                    FPS.Begin();
                    RenderDispatcher.Rendering();
                    var output = RenderDispatcher.GetOutputBuffer();
                    CopyBufferToTarget(output);
                    Input.RefreshMouseStateFrame();
                    Input.RefreshKeyboardFrame();
                    if (LocalMode == ThreadIntervalMode.YieldMode) FPS.Yield();
                    else FPS.Sleep();
                    FPS.CallFPS();
                    RenderDispatcher.SendTimeStamp(FPS.GetLastStamp());
                    startTime = DateTime.Now;
                    if (startTime > endTime)
                    {
                        logger.LogInformation($"FPS: {FPS.CurrentFPS}");
                        endTime = startTime.AddSeconds(5);
                    }
                }
                catch (Exception e)
                {
                    logger.LogError(e.Message + "\r\n" + e.StackTrace);
                    break;
                }
                finally
                {
                    Interlocked.Exchange(ref stop, 0);
                }
            }
        }

        /// <summary>
        /// 创建渲染包装器
        /// </summary>
        /// <param name="provider"></param>
        /// <param name="dispatcher"></param>
        /// <param name="modules"></param>
        /// <returns></returns>
        protected virtual VulkanRenderWapper CreateRenderWapper(IServiceProvider provider, VulkanOffScreenRenderDispatcher dispatcher, params Type[] modules)
        {
            VulkanRenderWapper wapper = GraphicsManager.CreateRenderWapper<VulkanRenderWapper>(null);
            wapper.RenderDispatcher = dispatcher;
            wapper.RenderArea = RenderArea;
            wapper.AddWapperModules(modules);
            return wapper;
        }
        /// <summary>
        /// 创建离屏渲染通道
        /// </summary>
        /// <returns></returns>
        protected virtual VulkanRenderPass CreateOffScreenRenderpass()
        {
            var Device = GraphicsManager.VulkanDevice;
            if (Device == null) throw new InvalidOperationException("Please create device first");
            RenderPassCreateOption option = new RenderPassCreateOption()
            {
                Attachments = new AttachmentDescription[]
                {
                    new AttachmentDescription()
                    {
                        FinalLayout = ImageLayout.TransferSrcOptimal,
                        Format = Format.B8G8R8A8Unorm,
                        InitialLayout = ImageLayout.Undefined,
                        LoadOp = AttachmentLoadOp.Clear,
                        StoreOp = AttachmentStoreOp.Store,
                        Samples = SampleCountFlags.SampleCount1Bit,
                        StencilLoadOp = AttachmentLoadOp.DontCare,
                        StencilStoreOp = AttachmentStoreOp.DontCare
                    },
                    new AttachmentDescription()
                    {
                        FinalLayout = ImageLayout.DepthStencilAttachmentOptimal,
                        Format = Format.D24UnormS8Uint,
                        LoadOp = AttachmentLoadOp.Clear,
                        StoreOp = AttachmentStoreOp.DontCare,
                        Samples = SampleCountFlags.SampleCount1Bit,
                        StencilLoadOp = AttachmentLoadOp.DontCare,
                        StencilStoreOp = AttachmentStoreOp.DontCare,
                        InitialLayout = ImageLayout.Undefined
                    }
                },
                Subpasses = new SubpassDescriptionOption[]
                {
                    new SubpassDescriptionOption()
                    {
                        ColorAttachments = new AttachmentReference[]
                        {
                            new AttachmentReference(){ Attachment = 0, Layout = ImageLayout.ColorAttachmentOptimal }
                        },
                        DepthStencilAttachment = new AttachmentReference { Attachment = 1, Layout = ImageLayout.DepthStencilAttachmentOptimal },
                        PipelineBindPoint = PipelineBindPoint.Graphics
                    }
                }
            };
            var pass = Device.CreateRenderPass($"OffScreenRenderpass{GraphicsManager.GetNewId()}", option, RenderPassMode.Normal);
            ImageAndImageViewCreateOption[] attachment = new ImageAndImageViewCreateOption[]
            {
                new ImageAndImageViewCreateOption()
                {
                    ImageCreateOption = new ImageCreateOption
                    {
                        ArrayLayers = 1,
                        Extent = new Extent3D(RenderArea.Width, RenderArea.Height, 1),
                        Format = Format.B8G8R8A8Unorm,
                        ImageType = ImageType.ImageType2D,
                        InitialLayout = ImageLayout.Undefined,
                        MipLevels = 1,
                        Samples = SampleCountFlags.SampleCount1Bit,
                        SharingMode = SharingMode.Exclusive,
                        Tiling = ImageTiling.Optimal,
                        Usage = ImageUsageFlags.ImageUsageColorAttachmentBit | ImageUsageFlags.ImageUsageTransferSrcBit
                    },
                    ImageViewCreateOption = new ImageViewCreateOption()
                    {
                        Components = new ComponentMapping(ComponentSwizzle.R, ComponentSwizzle.G, ComponentSwizzle.B, ComponentSwizzle.A),
                        Format = Format.B8G8R8A8Unorm,
                        SubresourceRange = new ImageSubresourceRange(ImageAspectFlags.ImageAspectColorBit, 0, 1, 0, 1),
                        ViewType = ImageViewType.ImageViewType2D
                    }
                },
                new ImageAndImageViewCreateOption()
                {
                    ImageCreateOption = new ImageCreateOption()
                    {
                        ArrayLayers = 1,
                        Extent = new Extent3D(RenderArea.Width, RenderArea.Height, 1),
                        Format = Format.D24UnormS8Uint,
                        ImageType = ImageType.ImageType2D,
                        InitialLayout = ImageLayout.Undefined,
                        MipLevels = 1,
                        Samples = SampleCountFlags.SampleCount1Bit,
                        SharingMode = SharingMode.Exclusive,
                        Tiling = ImageTiling.Optimal,
                        Usage = ImageUsageFlags.ImageUsageDepthStencilAttachmentBit,
                    },
                    ImageViewCreateOption = new ImageViewCreateOption()
                    {
                        Components = new ComponentMapping(ComponentSwizzle.R, ComponentSwizzle.G, ComponentSwizzle.B, ComponentSwizzle.A),
                        Format = Format.D24UnormS8Uint,
                        SubresourceRange = new ImageSubresourceRange(ImageAspectFlags.ImageAspectDepthBit, 0, 1, 0, 1),
                        ViewType = ImageViewType.ImageViewType2D
                    }
                }
            };
            pass.CreateOffScreenFrameBuffer(attachment, RenderArea.Height, RenderArea.Width, 1, 2);
            pass.ConfigBeginInfo((option) =>
            {
                option.RenderArea = new Rect2D { Extent = new Extent2D(RenderArea.Width, RenderArea.Height), Offset = new Offset2D { X = 0, Y = 0 } };
                option.ClearValues = new ClearValue[]
                {
                    new ClearValue() { Color = new ClearColorValue(0.2f, 0.2f, 0.2f, 0.2f) },
                    new ClearValue() { DepthStencil = new ClearDepthStencilValue(1.0f, 0) }
                };
            });
            return pass;
        }
        /// <summary>
        /// 创建离屏渲染调度器
        /// </summary>
        /// <param name="queue"></param>
        /// <param name="pass"></param>
        /// <param name="renderFinish"></param>
        /// <param name="targetAttach"></param>
        /// <returns></returns>
        protected virtual VulkanOffScreenRenderDispatcher CreateOffScreenRenderDispatcher(WorkQueue queue, VulkanRenderPass pass, 
            VulkanSemaphore renderFinish, uint targetAttach)
        {
            var device = pass.Device;
            VulkanOffScreenRenderDispatcher dispatcher = ServiceProvider.CreateAutowiredInstance<VulkanOffScreenRenderDispatcher>();
            var eventBus = ServiceProvider.CreateAutowiredInstance<SystemEventBus>();
            dispatcher.WorkQueue = queue;
            dispatcher.TargetAttachmentIndex = targetAttach;
            dispatcher.RenderFinishSemaphore = renderFinish.RawHandle;
            dispatcher.OffScreenRenderPass = pass;
            dispatcher.Manager = GraphicsManager;
            dispatcher.Input = new InputStateController();
            dispatcher.ContextManager = GraphicsManager.GetContextManager();
            var frameBuffers = pass.Framebuffer;
            if (frameBuffers == null || frameBuffers.Count < 1) throw new NotSupportedException("Invalid Renderpass, No Framebuffer");
            List<VulkanBuffer> buffers = new List<VulkanBuffer>(frameBuffers.Count);
            for(int i = 0; i < frameBuffers.Count; i++)
            {
                buffers.Add(dispatcher.CreateCopiedBuffer(device, frameBuffers[i], targetAttach));
            }
            dispatcher.OutputBuffer = buffers;
            dispatcher.Input = Input;
            dispatcher.EventBus = eventBus;
            RenderDispatcher = dispatcher;
            dispatcher.Initialization();
            return dispatcher;
        }
        /// <summary>
        /// 拷贝缓冲数据至目标
        /// </summary>
        /// <param name="buffer"></param>
        protected virtual void CopyBufferToTarget(VulkanBuffer buffer)
        {
            Dispatcher.Invoke(() =>
            {
                SpinWait wait = new SpinWait();
                int s = stop;
                if (Interlocked.CompareExchange(ref stop, s, s) != s)
                {
                    wait.SpinOnce();
                    return;
                }
                if (s == 1)
                {
                    return;
                }
                try
                {
                    if (buffer.IsDispose)
                    {
                        return;
                    }
                    Target.Lock();
                    ulong size = BufferStride * BufferHeight;
                    IntPtr ptr = Target.BackBuffer;
                    buffer.DeviceMemory.CopyTo(ptr, size, 0);
                    Target.AddDirtyRect(new Int32Rect(0, 0, Target.PixelWidth, Target.PixelHeight));
                    Target.Unlock();
                }
                catch (Exception e)
                {
                    GraphicsManager.Logger.LogError(e.Message + "\r\n" + e.StackTrace);
                    Target.Unlock();
                }
            });
        }

        /// <summary>
        /// 鼠标进入控件
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseEnter(System.Windows.Input.MouseEventArgs e)
        {
            Fource = true;
            InputManager.Current.PrimaryKeyboardDevice.Focus(this);
            Input.MouseState.XDelta = 0;
            Input.MouseState.YDelta = 0;
            base.OnMouseEnter(e);
        }
        /// <summary>
        /// 鼠标离开事件
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseLeave(System.Windows.Input.MouseEventArgs e)
        {
            Fource = false;
            InputManager.Current.PrimaryKeyboardDevice.ClearFocus();
            Input.MouseState.XDelta = 0;
            Input.MouseState.YDelta = 0;
            Input.MouseButtonStateChanged(new MouseStateFrame(DateTime.Now, SgrA.Input.MouseButton.ButtonLeft, InputAction.Release, InputMods.None));
            Input.MouseButtonStateChanged(new MouseStateFrame(DateTime.Now, SgrA.Input.MouseButton.ButtonRight, InputAction.Release, InputMods.None));
            Input.MouseButtonStateChanged(new MouseStateFrame(DateTime.Now, SgrA.Input.MouseButton.ButtonMiddle, InputAction.Release, InputMods.None));
            base.OnMouseLeave(e);
        }
        /// <summary>
        /// 当鼠标移动时
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseMove(System.Windows.Input.MouseEventArgs e)
        {
            if (Fource)
            {
                var old = Position;
                Point point = e.GetPosition(this);
                Vector2 newP = new Vector2((float)point.X, (float)point.Y);
                Vector2 diff = old - newP;
                Position = newP;
                if ((int)diff.X != 0 || (int)diff.Y != 0)
                {
                    Input.MouseState.X = (int)newP.X;
                    Input.MouseState.Y = (int)newP.Y;
                    Input.MouseState.XDelta = (int)diff.X;
                    Input.MouseState.YDelta = (int)diff.Y;
                }
                else
                {
                    Input.MouseState.XDelta = 0;
                    Input.MouseState.YDelta = 0;
                }
            }
            base.OnMouseMove(e);
        }
        /// <summary>
        /// 当鼠标点击按下时
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseDown(System.Windows.Input.MouseButtonEventArgs e)
        {
            var button = GetMouseButton(e.ChangedButton);
            var state = GetMouseState(e.ButtonState);
            Input.MouseButtonStateChanged(new MouseStateFrame(DateTime.Now, button, state, InputMods.None));
            base.OnMouseDown(e);
        }
        /// <summary>
        /// 当鼠标抬起时
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseUp(System.Windows.Input.MouseButtonEventArgs e)
        {
            var button = GetMouseButton(e.ChangedButton);
            var state = GetMouseState(e.ButtonState);
            Input.MouseButtonStateChanged(new MouseStateFrame(DateTime.Now, button, state, InputMods.None));
            base.OnMouseUp(e);
        }
        /// <summary>
        /// 鼠标滚轮事件
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseWheel(MouseWheelEventArgs e)
        {
            if (Fource)
            {
                Input.MouseState.WDelta = e.Delta;
            }
            base.OnMouseWheel(e);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            var key = GetKey(e.Key);
            var state = GetKeyState(e.KeyStates);
            Input.KeyboardStateChanged(new PressKeyStateFrame(DateTime.Now, key, state, InputMods.None));
            base.OnPreviewKeyDown(e);
        }
        /// <summary>
        /// 当键盘按下时
        /// </summary>
        /// <param name="e"></param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            var key = GetKey(e.Key);
            var state = GetKeyState(e.KeyStates);
            Input.KeyboardStateChanged(new PressKeyStateFrame(DateTime.Now, key, state, InputMods.None));
            base.OnKeyDown(e);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreviewKeyUp(KeyEventArgs e)
        {
            var key = GetKey(e.Key);
            var state = GetKeyState(e.KeyStates);
            Input.KeyboardStateChanged(new PressKeyStateFrame(DateTime.Now, key, state, InputMods.None));
            base.OnPreviewKeyUp(e);
        }
        /// <summary>
        /// 当键盘抬起时
        /// </summary>
        /// <param name="e"></param>
        protected override void OnKeyUp(KeyEventArgs e)
        {
            var key = GetKey(e.Key);
            var state = GetKeyState(e.KeyStates);
            Input.KeyboardStateChanged(new PressKeyStateFrame(DateTime.Now, key, state, InputMods.None));
            base.OnKeyUp(e);
        }
        /// <summary>
        /// 获取鼠标状态
        /// </summary>
        /// <param name="button"></param>
        /// <returns></returns>
        protected Input.MouseButton GetMouseButton(System.Windows.Input.MouseButton button)
        {
            return button switch
            {
                System.Windows.Input.MouseButton.Middle => SgrA.Input.MouseButton.ButtonMiddle,
                System.Windows.Input.MouseButton.Right => SgrA.Input.MouseButton.ButtonRight,
                System.Windows.Input.MouseButton.Left => SgrA.Input.MouseButton.ButtonLeft,
                _ => SgrA.Input.MouseButton.ButtonLast
            };
        }
        /// <summary>
        /// 获取鼠标状态
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        protected Input.InputAction GetMouseState(System.Windows.Input.MouseButtonState state)
        {
            return state switch
            {
                System.Windows.Input.MouseButtonState.Pressed => InputAction.Press,
                System.Windows.Input.MouseButtonState.Released => InputAction.Release,
                _ => InputAction.Release
            };
        }
        /// <summary>
        /// 获取按键
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        protected PressKey GetKey(Key key)
        {
            return key switch
            {
                Key.A => PressKey.A,
                Key.B => PressKey.B,
                Key.C => PressKey.C,
                Key.D => PressKey.D,
                Key.E => PressKey.E,
                Key.F => PressKey.F,
                Key.G => PressKey.G,
                Key.H => PressKey.H,
                Key.I => PressKey.I,
                Key.J => PressKey.J,
                Key.K => PressKey.K,
                Key.L => PressKey.L,
                Key.M => PressKey.M,
                Key.N => PressKey.N,
                Key.O => PressKey.O,
                Key.P => PressKey.P,
                Key.Q => PressKey.Q,
                Key.R => PressKey.R,
                Key.S => PressKey.S,
                Key.T => PressKey.T,
                Key.U => PressKey.U,
                Key.V => PressKey.V,
                Key.W => PressKey.W,
                Key.X => PressKey.X,
                Key.Y => PressKey.Y,
                Key.Z => PressKey.Z,
                Key.D0 => PressKey.Key_0,
                Key.D1 => PressKey.Key_1,
                Key.D2 => PressKey.Key_2,
                Key.D3 => PressKey.Key_3,
                Key.D4 => PressKey.Key_4,
                Key.D5 => PressKey.Key_5,
                Key.D6 => PressKey.Key_6,
                Key.D7 => PressKey.Key_7,
                Key.D8 => PressKey.Key_8,
                Key.D9 => PressKey.Key_9,
                Key.F1 => PressKey.F1,
                Key.F2 => PressKey.F2,
                Key.F3 => PressKey.F3,
                Key.F4 => PressKey.F4,
                Key.F5 => PressKey.F5,
                Key.F6 => PressKey.F6,
                Key.F7 => PressKey.F7,
                Key.F8 => PressKey.F8,
                Key.F9 => PressKey.F9,
                Key.F10 => PressKey.F10,
                Key.F11 => PressKey.F11,
                Key.F12 => PressKey.F12,
                Key.Space => PressKey.Space,
                Key.Subtract => PressKey.Minus,
                Key.NumLock => PressKey.NumLock,
                Key.NumPad0 => PressKey.KeyKp0,
                Key.NumPad1 => PressKey.KeyKp1,
                Key.NumPad2 => PressKey.KeyKp2,
                Key.NumPad3 => PressKey.KeyKp3,
                Key.NumPad4 => PressKey.KeyKp4,
                Key.NumPad5 => PressKey.KeyKp5,
                Key.NumPad6 => PressKey.KeyKp6,
                Key.NumPad7 => PressKey.KeyKp7,
                Key.NumPad8 => PressKey.KeyKp8,
                Key.NumPad9 => PressKey.KeyKp9,
                Key.Add => PressKey.KeyKpAdd,
                Key.Enter => PressKey.Enter,
                Key.Back => PressKey.Backspace,
                Key.Cancel => PressKey.Escape,
                _ => PressKey.Unknown
            };
        }
        /// <summary>
        /// 获取按键状态
        /// </summary>
        /// <param name="states"></param>
        /// <returns></returns>
        protected InputAction GetKeyState(KeyStates states)
        {
            return states switch
            {
                KeyStates.Down | KeyStates.Toggled => InputAction.Press,
                KeyStates.Down => InputAction.Press,
                KeyStates.None => InputAction.Release,
                _ => InputAction.Release
            };
        }
        /// <summary>
        /// 测量布局
        /// </summary>
        /// <param name="constraint"></param>
        /// <returns></returns>
        protected override Size MeasureOverride(Size constraint)
        {
            var size = constraint;
            if ((uint)size.Width != RenderArea.Width || (uint)size.Height != RenderArea.Height)
            {
                Target = new WriteableBitmap((int)size.Width, (int)size.Height, Dpi.DpiScaleX, Dpi.DpiScaleY, PixelFormats.Bgra32, BitmapPalettes.BlackAndWhiteTransparent);
                RenderArea = new Area(0, 0, (uint)size.Width, (uint)size.Height);
                TargetImage.Source = Target;
                if (RenderDispatcher != null && IsLoaded)
                {
                    Interrupt();
                    RenderDispatcher.RecreateRenderDispatcher((uint)size.Width, (uint)size.Height);
                    Continue();
                }
            }
            return constraint;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void IntervalModeCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            VulkanRenderControl control = d as VulkanRenderControl;
            control.LocalMode = (ThreadIntervalMode)e.NewValue;
        }
    }
}
