﻿using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;
using IOP.ISEA.STEP.Entities;

namespace IOP.ISEA.STEP.Types
{
    /// <summary>
    /// 曲线字体样式
    /// </summary>
    public class CurveStyleFontSelect : Select
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public override string Keyword => Schema.CURVE_STYLE_FONT_SELECT;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="font"></param>
        public CurveStyleFontSelect(CurveStyleFont font)
        {
            SelectList.TryAdd(typeof(CurveStyleFont), font);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="font"></param>
        public CurveStyleFontSelect(ExternallyDefinedCurveFont font)
        {
            SelectList.TryAdd(typeof(ExternallyDefinedCurveFont), font);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="font"></param>
        public CurveStyleFontSelect(DraughtingPreDefinedCurveFont font)
        {
            SelectList.TryAdd(typeof(DraughtingPreDefinedCurveFont), font);
        }
        /// <summary>
        /// 
        /// </summary>
        protected CurveStyleFontSelect() { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="express"></param>
        /// <returns></returns>
        public override bool IsSelectItem(IExpress express) => IsSelectItemStatic(express);
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TExpress"></typeparam>
        /// <returns></returns>
        public static bool IsSelectItemStatic(IExpress express)
        {
            bool r = express switch
            {
                CurveStyleFont _ => true,
                ExternallyDefinedCurveFont _ => true,
                PreDefinedCurveFont _ => true,
                DraughtingPreDefinedCurveFont _ => true,
                _ => false
            };
            return r;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="select"></param>
        /// <param name="expresses"></param>
        /// <returns></returns>
        public static CurveStyleFontSelect CreateFromStatic(params IExpress[] expresses)
        {
            if (expresses == null || expresses.Length <= 0) throw new ArgumentNullException(nameof(expresses));
            var select = new CurveStyleFontSelect();
            foreach (var item in expresses)
            {
                switch (item)
                {
                    case CurveStyleFont csf:
                        select.AddItem(csf);
                        break;
                    case ExternallyDefinedCurveFont edcf:
                        select.AddItem(edcf);
                        break;
                    case PreDefinedCurveFont pdcf:
                        select.AddItem(pdcf);
                        break;
                    case DraughtingPreDefinedCurveFont dpdcf:
                        select.AddItem(dpdcf.AsSuperType<PreDefinedCurveFont>());
                        break;
                    default:
                        throw new InvalidCastException($"{item.Keyword} is not belong to select {select.Keyword}");
                }
            }
            return select;
        }
        /// <summary>
        /// 创建
        /// </summary>
        /// <param name="expresses"></param>
        /// <returns></returns>
        public override Select CreateFrom(params IExpress[] expresses)
            => CreateFromStatic(expresses);
    }
}
