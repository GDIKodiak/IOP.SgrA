﻿using IOP.ISEA.EXPRESS;
using IOP.ISEA.STEP.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.STEP.Types
{
    /// <summary>
    /// 呈现样式选择
    /// </summary>
    public class PresentationStyleSelect : Select
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => "PRESENTATION_STYLE_SELECT";
        /// <summary>
        /// 
        /// </summary>
        /// <param name="expresses"></param>
        /// <returns></returns>
        public override Select CreateFrom(params IExpress[] expresses)
        {
            PresentationStyleSelect select = new PresentationStyleSelect();
            foreach (var expense in expresses)
            {
                switch (expense)
                {
                    case CurveStyle cs:
                        select.AddItem(cs);
                        break;
                    default:
                        break;
                }
            }
            return select;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="express"></param>
        /// <returns></returns>
        public override bool IsSelectItem(IExpress express)
        {
            return express switch
            {
                CurveStyle _ => true,
                _ => false
            };
        }
    }
}
