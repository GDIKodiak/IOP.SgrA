﻿using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.MeasureSchema
{
    /// <summary>
    /// 长度度量
    /// </summary>
    public struct LengthMeasure : IExpress
    {
        /// <summary>
        /// 
        /// </summary>
        public string Keyword => Schema.LENGTH_MEASURE;
        private double value;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public LengthMeasure(double v)
        {
            value = v;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString() => value.ToString();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public static explicit operator LengthMeasure(double value) => new LengthMeasure(value);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        public static implicit operator double(LengthMeasure identifier) => identifier.value;
    }
}
