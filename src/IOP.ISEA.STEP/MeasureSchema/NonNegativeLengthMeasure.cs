﻿using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.MeasureSchema
{
    /// <summary>
    /// 
    /// </summary>
    public struct NonNegativeLengthMeasure : IExpress
    {
        /// <summary>
        /// 
        /// </summary>
        public string Keyword => Schema.NON_NEGATIVE_LENGTH_MEASURE;
        private LengthMeasure Length;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public NonNegativeLengthMeasure(double v)
        {
            if (v < 0) throw new ArgumentOutOfRangeException($"{nameof(v)} is less than 0");
            Length = (LengthMeasure)v;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString() => Length.ToString();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public static explicit operator NonNegativeLengthMeasure(double value) => new NonNegativeLengthMeasure(value);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        public static implicit operator double(NonNegativeLengthMeasure identifier) => identifier.Length;
    }
}
