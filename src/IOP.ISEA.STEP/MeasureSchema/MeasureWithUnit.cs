﻿using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.MeasureSchema
{
    /// <summary>
    /// 单位度量
    /// </summary>
    public class MeasureWithUnit : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => Schema.MEASURE_WITH_UNIT;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>() => false;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>()
        {
            throw new NotImplementedException();
        }
    }
}
