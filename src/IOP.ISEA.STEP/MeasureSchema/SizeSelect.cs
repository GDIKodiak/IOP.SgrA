﻿using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.MeasureSchema
{
    /// <summary>
    /// 
    /// </summary>
    public class SizeSelect : Select
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => Schema.SIZE_SELECT;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="measure"></param>
        public SizeSelect(DescriptiveMeasure measure)
        {
            SelectList.TryAdd(typeof(DescriptiveMeasure), measure);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="measure"></param>
        public SizeSelect(MeasureWithUnit measure)
        {
            SelectList.TryAdd(typeof(MeasureWithUnit), measure);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="measure"></param>
        public SizeSelect(PositiveLengthMeasure measure)
        {
            SelectList.TryAdd(typeof(PositiveLengthMeasure), measure);
        }
        public SizeSelect() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="express"></param>
        /// <returns></returns>
        public override bool IsSelectItem(IExpress express) => IsSelectItemStatic(express);
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TExpress"></typeparam>
        /// <returns></returns>
        public static bool IsSelectItemStatic(IExpress express)
        {
            return express switch
            {
                DescriptiveMeasure _ => true,
                MeasureWithUnit _ => true,
                PositiveLengthMeasure _ => true,
                _ => false
            };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="expresses"></param>
        /// <returns></returns>
        public static SizeSelect CreateFromStatic(params IExpress[] expresses)
        {
            if (expresses == null || expresses.Length <= 0) throw new ArgumentNullException(nameof(expresses));
            var select = new SizeSelect();
            foreach (var item in expresses)
            {
                switch (item)
                {
                    case DescriptiveMeasure dm:
                        select.AddItem(dm);
                        break;
                    case MeasureWithUnit mwu:
                        select.AddItem(mwu);
                        break;
                    case PositiveLengthMeasure plm:
                        select.AddItem(plm);
                        break;
                    default:
                        throw new InvalidCastException($"{item.Keyword} is not belong to select {select.Keyword}");
                }
            }
            return select;
        }
        /// <summary>
        /// 创建
        /// </summary>
        /// <param name="expresses"></param>
        /// <returns></returns>
        public override Select CreateFrom(params IExpress[] expresses)
            => CreateFromStatic(expresses);
    }
}
