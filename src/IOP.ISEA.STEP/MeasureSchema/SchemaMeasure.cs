﻿using IOP.ISEA.STEP.MeasureSchema;
using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP
{
    /// <summary>
    /// 度量模式
    /// </summary>
    public static partial class Schema
    {
        /// <summary>
        /// 单位度量
        /// </summary>
        public const string MEASURE_WITH_UNIT = "MEASURE_WITH_UNIT";

        /// <summary>
        /// 长度度量
        /// </summary>
        public const string LENGTH_MEASURE = "LENGTH_MEASURE";
        /// <summary>
        /// 非负长度度量
        /// </summary>
        public const string NON_NEGATIVE_LENGTH_MEASURE = "NON_NEGATIVE_LENGTH_MEASURE";
        /// <summary>
        /// 正数长度度量
        /// </summary>
        public const string POSITIVE_LENGTH_MEASURE = "POSITIVE_LENGTH_MEASURE";
        /// <summary>
        /// 描述度量
        /// </summary>
        public const string DESCRIPTIVE_MEASURE = "DESCRIPTIVE_MEASURE";

        /// <summary>
        /// 长度选择
        /// </summary>
        public const string SIZE_SELECT = "SIZE_SELECT";

        /// <summary>
        /// 添加度量模式
        /// </summary>
        /// <param name="document"></param>
        public static void AddSchemaMeasure(this IEXPRESSDocument document)
        {
            document.AddTypeCodec(POSITIVE_LENGTH_MEASURE, new PositiveLengthMeasureExpressTypeCodec());
        }
    }
}
