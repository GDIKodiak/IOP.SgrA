﻿using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.MeasureSchema
{
    /// <summary>
    /// 
    /// </summary>
    public struct PositiveLengthMeasure : IExpress
    {
        /// <summary>
        /// 
        /// </summary>
        public string Keyword => Schema.POSITIVE_LENGTH_MEASURE;
        private NonNegativeLengthMeasure Length;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public PositiveLengthMeasure(double v)
        {
            if (v <= 0) throw new ArgumentOutOfRangeException($"{nameof(v)} is less or equal than 0");
            Length = (NonNegativeLengthMeasure)v;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString() => Length.ToString();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public static explicit operator PositiveLengthMeasure(double value) => new PositiveLengthMeasure(value);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        public static implicit operator double(PositiveLengthMeasure identifier) => identifier.Length;
    }

    /// <summary>
    /// 正数长度测量
    /// </summary>
    public class PositiveLengthMeasureExpressTypeCodec : IExpressTypeCodec
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public string Keyword => Schema.POSITIVE_LENGTH_MEASURE;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public IExpress Decode(TypeParameter parameter, IEXPRESSDocument document)
        {
            var value = parameter.Parameter.As<RealParameter>();
            PositiveLengthMeasure measure = new PositiveLengthMeasure(value.ParameterValue);
            return measure;
        }
    }
}
