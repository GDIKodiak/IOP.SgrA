﻿using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.ExternalReferenceSchema
{
    /// <summary>
    /// 外部定义曲线文字
    /// </summary>
    public class ExternallyDefinedCurveFont : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => Schema.EXTERNALLY_DEFINED_CURVE_FONT;

        public override bool IsSubType<TEntity>()
        {
            throw new NotImplementedException();
        }

        public override bool IsSuperType<TEntity>()
        {
            throw new NotImplementedException();
        }
    }
}
