﻿using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.ExternalReferenceSchema
{
    /// <summary>
    /// 外部源
    /// </summary>
    public class ExternalSource : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => Schema.EXTERNALSOURCE;
        /// <summary>
        /// 源Id
        /// </summary>
        public SourceItem SourceId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        public ExternalSource(Identifier identifier)
        {
            SourceId = new SourceItem(identifier);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public ExternalSource(Message message)
        {
            SourceId = new SourceItem(message);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        public ExternalSource(SourceItem id)
        {
            SourceId = id;
        }

        public override bool IsSubType<TEntity>()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => false;
    }
}
