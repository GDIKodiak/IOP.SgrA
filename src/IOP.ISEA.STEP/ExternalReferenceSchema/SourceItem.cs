﻿using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.ExternalReferenceSchema
{
    /// <summary>
    /// 
    /// </summary>
    public class SourceItem : Select
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => Schema.SOURCEITEM;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        public SourceItem(Identifier identifier)
        {
            SelectList.TryAdd(typeof(Identifier), identifier);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public SourceItem(Message message)
        {
            SelectList.TryAdd(typeof(Message), message);
        }
        /// <summary>
        /// 
        /// </summary>
        public SourceItem() { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="express"></param>
        /// <returns></returns>
        public override bool IsSelectItem(IExpress express)
        {
            return express switch
            {
                Message _ => true,
                Identifier _ => true,
                _ => false,
            };
        }
        /// <summary>
        /// 创建
        /// </summary>
        /// <param name="expresses"></param>
        /// <returns></returns>
        public override Select CreateFrom(params IExpress[] expresses)
            => CreateFromStatic(expresses);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="expresses"></param>
        /// <returns></returns>
        public static SourceItem CreateFromStatic(params IExpress[] expresses)
        {
            if (expresses == null || expresses.Length <= 0) throw new ArgumentNullException(nameof(expresses));
            var select = new SourceItem();
            foreach (var item in expresses)
            {
                switch (item)
                {
                    case Identifier dm:
                        select.AddItem(dm);
                        break;
                    case Message mwu:
                        select.AddItem(mwu);
                        break;
                    default:
                        throw new InvalidCastException($"{item.Keyword} is not belong to select {select.Keyword}");
                }
            }
            return select;
        }
    }
}
