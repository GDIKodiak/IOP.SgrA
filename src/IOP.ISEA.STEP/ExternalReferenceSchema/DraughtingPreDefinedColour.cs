﻿using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.ExternalReferenceSchema
{
    /// <summary>
    /// 
    /// </summary>
    public class DraughtingPreDefinedColour : Entity
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get => AsSuperType<PreDefinedColour>().Name; }
        /// <summary>
        /// 关键字
        /// </summary>
        public override string Keyword => Schema.DRAUGHTING_PRE_DEFINED_COLOUR;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public DraughtingPreDefinedColour(string name)
        {
            PreDefinedColour colour = new PreDefinedColour(name);
            SetSuperType(colour);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="super"></param>
        public DraughtingPreDefinedColour(PreDefinedColour super)
        {
            SetSuperType(super);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>() => false;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(PreDefinedColour);
    }

    /// <summary>
    /// 
    /// </summary>
    public class DraughtingPreDefinedColourCodec : IExpressCodec
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public string Keyword => Schema.DRAUGHTING_PRE_DEFINED_COLOUR;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceEntity"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public Entity Decode(Record sourceEntity, IEXPRESSDocument document)
        {
            var record = sourceEntity as SimpleRecord ?? throw new InvalidCastException($"{nameof(sourceEntity)} is not a SimpleRecord");
            if (record.Keyword != Keyword) throw new InvalidCastException($"Target instance is not {Keyword}");
            string name = string.Empty;
            if (record.Parameters.Count > 0)
            {
                name = record.Parameters[0].As<StringParameter>().ParameterValue;
            }
            DraughtingPreDefinedColour colour = new DraughtingPreDefinedColour(name);
            return colour;
        }
    }
}
