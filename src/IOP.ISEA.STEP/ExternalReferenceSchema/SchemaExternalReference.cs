﻿using IOP.ISEA.STEP.ExternalReferenceSchema;
using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP
{
    /// <summary>
    /// 
    /// </summary>
    public static partial class Schema
    {
        /// <summary>
        /// 待定义项
        /// </summary>
        public const string PRE_DEFINED_ITEM = "PRE_DEFINED_ITEM";

        /// <summary>
        /// 待定义曲线文字
        /// </summary>
        public const string PRE_DEFINED_CURVE_FONT = "PRE_DEFINED_CURVE_FONT";
        /// <summary>
        /// 带定义的制图曲线文字
        /// </summary>
        public const string DRAUGHTING_PRE_DEFINED_CURVE_FONT = "DRAUGHTING_PRE_DEFINED_CURVE_FONT";

        /// <summary>
        /// 待定义的颜色
        /// </summary>
        public const string PRE_DEFINED_COLOUR = "DRAUGHTING_PRE_DEFINED_COLOUR";
        /// <summary>
        /// 待定义的制图颜色
        /// </summary>
        public const string DRAUGHTING_PRE_DEFINED_COLOUR = "DRAUGHTING_PRE_DEFINED_COLOUR";

        /// <summary>
        /// 外部定义曲线字体
        /// </summary>
        public const string EXTERNALLY_DEFINED_CURVE_FONT = "EXTERNALLY_DEFINED_CURVE_FONT";

        /// <summary>
        /// 标识符
        /// </summary>
        public const string IDENTIFIER = "IDENTIFIER";
        /// <summary>
        /// 消息
        /// </summary>
        public const string MESSAGE = "MESSAGE";
        /// <summary>
        /// 源数据项
        /// </summary>
        public const string SOURCEITEM = "SOURCEITEM";
        /// <summary>
        /// 外部源
        /// </summary>
        public const string EXTERNALSOURCE = "EXTERNALSOURCE";

        /// <summary>
        /// 添加额外引用模式
        /// </summary>
        /// <param name="document"></param>
        public static void AddSchemaExternalReference(this IEXPRESSDocument document)
        {
            document.AddEntityCodec(DRAUGHTING_PRE_DEFINED_CURVE_FONT, new DraughtingPreDefinedCurveFontCodec());
            document.AddEntityCodec(DRAUGHTING_PRE_DEFINED_COLOUR, new DraughtingPreDefinedColourCodec());
        }
    }
}
