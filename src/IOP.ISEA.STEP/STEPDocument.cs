﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP
{
    public class STEPDocument : EXPRESSDocument
    {
        /// <summary>
        /// 
        /// </summary>
        public STEPDocument()
            : base()
        {
            this.AddStepMergedAPSchema();
        }

        /// <summary>
        /// 从文件中创建文档
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static async Task<STEPDocument> CreateFromFile(string path)
        {
            STEPDocument document = new STEPDocument();
            await EXPRESSFileReader.ReadFromFile(path, document);
            return document;
        }
    }
}
