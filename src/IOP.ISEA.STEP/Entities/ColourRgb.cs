﻿using IOP.ISEA.EXPRESS;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace IOP.ISEA.STEP.Entities
{
    /// <summary>
    /// RGB颜色
    /// </summary>
    public class ColourRgb : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => Schema.COLOUR_RGB;
        /// <summary>
        /// 
        /// </summary>
        public string Name { get => AsSuperType<ColourSpecification>().Name; }
        /// <summary>
        /// 
        /// </summary>
        public double Red { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public double Green { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public double Blue { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="rgb"></param>
        public ColourRgb(string name, Vector3 rgb)
        {
            ColourSpecification specification = new ColourSpecification(name);
            SetSuperType(specification);
            Red = rgb.X;
            Green = rgb.Y;
            Blue = rgb.Z;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="specification"></param>
        /// <param name="rgb"></param>
        public ColourRgb(ColourSpecification specification, Vector3 rgb)
        {
            SetSuperType(specification);
            Red = rgb.X;
            Green = rgb.Y;
            Blue = rgb.Z;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>() => false;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(ColourSpecification);
    }

    /// <summary>
    /// 
    /// </summary>
    public class ColourRgbCodec : IExpressCodec
    {
        /// <summary>
        /// 
        /// </summary>
        public string Keyword => Schema.COLOUR_RGB;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceEntity"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public Entity Decode(Record sourceEntity, IEXPRESSDocument document)
        {
            var record = sourceEntity as SimpleRecord ?? throw new InvalidCastException($"{nameof(sourceEntity)} is not a SimpleRecord");
            if (record.Keyword != Keyword) throw new InvalidCastException($"Target instance is not {Keyword}");
            if (record.Parameters.Count != 4) throw new InvalidCastException($"{Keyword} parameters lost");
            var name = record.Parameters[0].As<StringParameter>().Value();
            var red = record.Parameters[1].As<RealParameter>().Value();
            var green = record.Parameters[2].As<RealParameter>().Value();
            var blue = record.Parameters[3].As<RealParameter>().Value();
            ColourRgb colourRgb = new ColourRgb(name, new Vector3((float)red, (float)green, (float)blue));
            return colourRgb;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        /// <param name="body"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Entity DecodeWithComplex(SimpleRecord target, SubsuperRecord body, IEXPRESSDocument document)
        {
            throw new NotImplementedException();
        }
    }
}
