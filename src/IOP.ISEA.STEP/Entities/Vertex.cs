﻿using IOP.ISEA.EXPRESS;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.STEP.Entities
{
    /// <summary>
    /// 拓扑结构
    /// </summary>
    public class Vertex : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => Schema.VERTEX;
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get => AsSuperType<TopologicalRepresentationItem>().Name; }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>()
        {
            return typeof(TEntity) == typeof(VertexPoint);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(TopologicalRepresentationItem);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public Vertex(string name)
        {
            if (string.IsNullOrEmpty(name)) name = string.Empty;
            TopologicalRepresentationItem item = new TopologicalRepresentationItem(name);
            SetSuperType(item);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        public Vertex(RepresentationItem re)
        {
            if (re == null) throw new ArgumentNullException(nameof(RepresentationItem));
            TopologicalRepresentationItem item = new TopologicalRepresentationItem(re);
            SetSuperType(item);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        public Vertex(TopologicalRepresentationItem item)
        {
            if (item == null) throw new ArgumentNullException(nameof(TopologicalRepresentationItem));
            SetSuperType(item);
        }
    }
}
