﻿using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.Entities
{
    /// <summary>
    /// 制图待定义的曲线文字
    /// </summary>
    public class DraughtingPreDefinedCurveFont : Entity
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get => AsSuperType<PreDefinedCurveFont>().Name; }
        /// <summary>
        /// 关键字
        /// </summary>
        public override string Keyword => Schema.DRAUGHTING_PRE_DEFINED_CURVE_FONT;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>() => false;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(PreDefinedCurveFont);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public DraughtingPreDefinedCurveFont(string name)
        {
            PreDefinedCurveFont font = new PreDefinedCurveFont(name);
            SetSuperType(font);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="super"></param>
        public DraughtingPreDefinedCurveFont(PreDefinedCurveFont super)
        {
            SetSuperType(super);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DraughtingPreDefinedCurveFontCodec : IExpressCodec
    {
        public string Keyword => Schema.DRAUGHTING_PRE_DEFINED_CURVE_FONT;

        public Entity Decode(Record sourceEntity, IEXPRESSDocument document)
        {
            var record = sourceEntity as SimpleRecord ?? throw new InvalidCastException($"{nameof(sourceEntity)} is not a SimpleRecord");
            if (record.Keyword != Keyword) throw new InvalidCastException($"Target instance is not {Keyword}");
            string name = string.Empty;
            if (record.Parameters.Count > 0)
            {
                name = record.Parameters[0].As<StringParameter>().ParameterValue;
            }
            DraughtingPreDefinedCurveFont font = new DraughtingPreDefinedCurveFont(name);
            return font;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        /// <param name="body"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Entity DecodeWithComplex(SimpleRecord target, SubsuperRecord body, IEXPRESSDocument document)
        {
            throw new NotImplementedException();
        }
    }
}
