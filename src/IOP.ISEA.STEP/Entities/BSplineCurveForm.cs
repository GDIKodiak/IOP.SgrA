﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.STEP.Entities
{
    /// <summary>
    /// 由此B样条曲线具体的表示曲线类型
    /// </summary>
    public enum BSplineCurveForm
    {
        /// <summary>
        /// 一系列相连的直线段，通过1阶B样条基本函数表示
        /// </summary>
        POLYLINE_FORM,
        /// <summary>
        /// 一个圆弧，或一个完整的圆，由一个B样条曲线表示
        /// </summary>
        CIRCULAR_ARC,
        /// <summary>
        /// 一个椭圆弧或一个完整的椭圆，由一个B样条曲线表示
        /// </summary>
        ELLIPTIC_ARC,
        /// <summary>
        /// 一个有限长的抛物线弧，由一个B样条曲线表示
        /// </summary>
        PARABOLIC_ARC,
        /// <summary>
        /// 一个有限长的双曲线的一个分支弧，由B样条曲线表示
        /// </summary>
        HYPERBOLIC_ARC,
        /// <summary>
        /// 不表示任何具体曲线
        /// </summary>
        UNSPECIFIED,
    }
}
