﻿using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.Entities
{
    /// <summary>
    /// 曲线文字式样
    /// </summary>
    public class CurveStyleFontPattern : Entity
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public override string Keyword => Schema.CURVE_STYLE_FONT_PATTERN;
        /// <summary>
        /// 可见段长度
        /// </summary>
        public double VisibleSegmentLength { get; set; }
        /// <summary>
        /// 不可见段长度
        /// </summary>
        public double InvisibleSegmentLength { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vis"></param>
        /// <param name="invis"></param>
        public CurveStyleFontPattern(double vis, double invis)
        {
            VisibleSegmentLength = vis;
            InvisibleSegmentLength = invis;
            FoundedItem item = new FoundedItem();
            SetSuperType(item);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="vis"></param>
        /// <param name="invis"></param>
        public CurveStyleFontPattern(FoundedItem item, double vis, double invis)
        {
            if (item == null) throw new ArgumentNullException(nameof(item));
            VisibleSegmentLength = vis;
            InvisibleSegmentLength = invis;
            SetSuperType(item);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public override bool IsSubType<TEntity>() => false;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(FoundedItem);
    }
}
