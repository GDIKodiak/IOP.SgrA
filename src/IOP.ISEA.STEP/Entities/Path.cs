﻿using IOP.ISEA.EXPRESS;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.STEP.Entities
{
    /// <summary>
    /// 
    /// </summary>
    public class Path : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => Schema.PATH;
        /// <summary>
        /// 
        /// </summary>
        public string Name { get => AsSuperType<TopologicalRepresentationItem>().Name; }
        /// <summary>
        /// 边列表
        /// </summary>
        public List<OrientedEdge> EdgeList { get; set; } = new List<OrientedEdge>();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="edges"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public Path(string name, IEnumerable<OrientedEdge> edges)
        {
            if (string.IsNullOrEmpty(name)) name = string.Empty;
            if (edges == null) throw new ArgumentNullException(nameof(edges));
            EdgeList.AddRange(edges);
            TopologicalRepresentationItem item = new TopologicalRepresentationItem(name);
            SetSuperType(item);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="edges"></param>
        public Path(TopologicalRepresentationItem item, IEnumerable<OrientedEdge> edges)
        {
            if (item == null) throw new ArgumentNullException(nameof(item));
            if (edges == null) throw new ArgumentNullException(nameof(edges));
            SetSuperType(item);
            EdgeList.AddRange(edges);
        }

        public override bool IsSubType<TEntity>()
        {
            throw new NotImplementedException();
        }

        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(TopologicalRepresentationItem);
    }
}
