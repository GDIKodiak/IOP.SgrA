﻿using IOP.ISEA.EXPRESS;
using IOP.ISEA.STEP.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.STEP.Entities
{
    /// <summary>
    /// 样式项
    /// </summary>
    public class StyledItem : Entity
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public override string Keyword => Schema.STYLED_ITEM;
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get => AsSuperType<RepresentationItem>().Name; }
        /// <summary>
        /// 样式
        /// </summary>
        public List<PresentationStyleAssignment> Styles { get; private set; } = new List<PresentationStyleAssignment>();
        /// <summary>
        /// 
        /// </summary>
        public StyledItemTarget Item { get; private set; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="name"></param>
        /// <param name="styles"></param>
        /// <param name="item"></param>
        public StyledItem(string name, IEnumerable<PresentationStyleAssignment> styles, StyledItemTarget item)
        {
            RepresentationItem representation = new RepresentationItem(name);
            SetSuperType(representation);
            if (styles != null) Styles.AddRange(styles);
            Item = item ?? throw new ArgumentNullException(nameof(StyledItemTarget));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="styles"></param>
        /// <param name="target"></param>
        public StyledItem(RepresentationItem item, IEnumerable<PresentationStyleAssignment> styles, StyledItemTarget target)
        {
            SetSuperType(item);
            if (styles != null) Styles.AddRange(styles);
            Item = target ?? throw new ArgumentNullException(nameof(StyledItemTarget));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>() => false;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>()
        {
            return typeof(TEntity) == typeof(RepresentationItem);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public class StyledItemCodec : IExpressCodec
    {
        /// <summary>
        /// 
        /// </summary>
        public string Keyword => Schema.STYLED_ITEM;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceEntity"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public Entity Decode(Record sourceEntity, IEXPRESSDocument document)
        {
            var record = sourceEntity as SimpleRecord ?? throw new InvalidCastException($"{nameof(sourceEntity)} is not a SimpleRecord");
            if (record.Keyword != Keyword) throw new InvalidCastException($"Target instance is not {Keyword}");
            if (record.Parameters.Count != 3) throw new InvalidCastException($"{Keyword} parameters lost");
            var name = record.Parameters[0].As<StringParameter>().Value();
            var list = record.Parameters[1].As<ListParameter>();
            List<PresentationStyleAssignment> styles = new List<PresentationStyleAssignment>();
            foreach (var item in list.ParameterValue)
            {
                if (item.Is<EntityNameParameter>())
                {
                    var style = item.AsEnttiy<PresentationStyleAssignment>(document);
                    styles.Add(style);
                }
            }
            var target = record.Parameters[2].AsSelect<StyledItemTarget>(document);
            StyledItem styledItem = new StyledItem(name, styles, target);
            return styledItem;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        /// <param name="body"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Entity DecodeWithComplex(SimpleRecord target, SubsuperRecord body, IEXPRESSDocument document)
        {
            throw new NotImplementedException();
        }
    }
}
