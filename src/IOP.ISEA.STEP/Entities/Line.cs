﻿using IOP.ISEA.EXPRESS;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.STEP.Entities
{
    /// <summary>
    /// 
    /// </summary>
    public class Line : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => Schema.LINE;
        /// <summary>
        /// 
        /// </summary>
        public string Name { get => AsSuperType<Curve>().Name; }
        /// <summary>
        /// 起始点
        /// </summary>
        public CartesianPoint Pnt { get; set; }
        /// <summary>
        /// 方向
        /// </summary>
        public Vector Dir { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Dim { get => AsSuperType<Curve>().Dim; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="point"></param>
        /// <param name="vector"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public Line(string name, CartesianPoint point, Vector vector)
        {
            Pnt = point ?? throw new ArgumentNullException(nameof(point));
            Dir = vector ?? throw new ArgumentNullException(nameof(vector));
            Curve curve = new Curve(name, vector.Dim);
            SetSuperType(curve);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="curve"></param>
        /// <param name="point"></param>
        /// <param name="vector"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public Line(Curve curve, CartesianPoint point, Vector vector)
        {
            if (curve == null) throw new ArgumentNullException(nameof(curve));
            Pnt = point ?? throw new ArgumentNullException(nameof(point));
            Dir = vector ?? throw new ArgumentNullException(nameof(vector));
            SetSuperType(curve);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>() => false;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>()
        {
            return typeof(TEntity) == typeof(Curve);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public class LineCodec : IExpressCodec
    {
        /// <summary>
        /// 
        /// </summary>
        public string Keyword => Schema.LINE;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceEntity"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public Entity Decode(Record sourceEntity, IEXPRESSDocument document)
        {
            var record = sourceEntity as SimpleRecord ?? throw new InvalidCastException($"{nameof(sourceEntity)} is not a SimpleRecord");
            if (record.Keyword != Keyword) throw new InvalidCastException($"Target instance is not {Keyword}");
            if (record.Parameters.Count != 3) throw new InvalidCastException($"{Schema.DIRECTION} parameters lost");
            var name = record.Parameters[0].As<StringParameter>().Value();
            var pnt = record.Parameters[1].AsEnttiy<CartesianPoint>(document);
            var dir = record.Parameters[2].AsEnttiy<Vector>(document);
            Line line = new Line(name, pnt, dir);
            return line;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        /// <param name="body"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Entity DecodeWithComplex(SimpleRecord target, SubsuperRecord body, IEXPRESSDocument document)
        {
            throw new NotImplementedException();
        }
    }
}
