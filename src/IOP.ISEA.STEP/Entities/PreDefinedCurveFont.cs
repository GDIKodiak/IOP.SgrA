﻿using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.Entities
{
    /// <summary>
    /// 待定义曲线文字
    /// </summary>
    public class PreDefinedCurveFont : Entity
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get => AsSuperType<PreDefinedItem>().Name; }
        /// <summary>
        /// 关键字
        /// </summary>
        public override string Keyword => Schema.PRE_DEFINED_CURVE_FONT;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public PreDefinedCurveFont(string name)
        {
            PreDefinedItem item = new PreDefinedItem(name);
            SetSuperType(item);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="super"></param>
        public PreDefinedCurveFont(PreDefinedItem super)
        {
            SetSuperType(super);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>()
        {
            var type = typeof(TEntity);
            return type == typeof(DraughtingPreDefinedCurveFont);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(PreDefinedItem);
    }
}
