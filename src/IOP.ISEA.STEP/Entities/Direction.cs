﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.Entities
{
    /// <summary>
    /// 方向
    /// </summary>
    public class Direction : Entity
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public override string Keyword => Schema.DIRECTION;
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get => AsSuperType<GeometricRepresentationItem>().Name; }
        /// <summary>
        /// 维数
        /// </summary>
        public int Dim { get => AsSuperType<GeometricRepresentationItem>().Dim; }
        /// <summary>
        /// 方向向量
        /// </summary>
        public Vector3 DirectionRatios { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="dim"></param>
        public Direction(string name, Vector3 vector)
        {
            GeometricRepresentationItem item = new GeometricRepresentationItem(name, 3);
            SetSuperType(item);
            DirectionRatios = vector;
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="name"></param>
        /// <param name="vector"></param>
        public Direction(string name, Vector2 vector)
        {
            GeometricRepresentationItem item = new GeometricRepresentationItem(name, 3);
            SetSuperType(item);
            DirectionRatios = new Vector3(vector, 0);
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="name"></param>
        /// <param name="vector"></param>
        /// <param name="dim"></param>
        internal protected Direction(string name, Vector3 vector, int dim)
        {
            GeometricRepresentationItem item = new GeometricRepresentationItem(name, dim);
            SetSuperType(item);
            DirectionRatios = vector;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString() => DirectionRatios.ToString();
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(GeometricRepresentationItem);
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>() => false;
    }

    /// <summary>
    /// 方向解码器
    /// </summary>
    public class DirectionCodec : IExpressCodec
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public string Keyword => Schema.DIRECTION;
        /// <summary>
        /// 解码
        /// </summary>
        /// <param name="sourceEntity"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Entity Decode(Record sourceEntity, IEXPRESSDocument document)
        {
            var record = sourceEntity as SimpleRecord ?? throw new InvalidCastException($"{nameof(sourceEntity)} is not a SimpleRecord");
            if (record.Keyword != Keyword) throw new InvalidCastException($"Target instance is not {Keyword}");
            if (record.Parameters.Count != 2) throw new InvalidCastException($"{Schema.DIRECTION} parameters lost");
            var name = record.Parameters[0].As<StringParameter>().Value();
            var vector = record.Parameters[1].AsVector3(out int dim);
            if (dim < 2) throw new NotSupportedException($"DirectionRatios must be greater than 1");
            Direction direction = new Direction(name, vector);
            return direction;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        /// <param name="body"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Entity DecodeWithComplex(SimpleRecord target, SubsuperRecord body, IEXPRESSDocument document)
        {
            throw new NotImplementedException();
        }
    }
}
