﻿using IOP.ISEA.EXPRESS;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.STEP.Entities
{
    /// <summary>
    /// 向量
    /// </summary>
    public class Vector : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => Schema.VECTOR;
        /// <summary>
        /// 
        /// </summary>
        public string Name { get => AsSuperType<GeometricRepresentationItem>().Name; }
        /// <summary>
        /// 维度
        /// </summary>
        public int Dim { get => AsSuperType<GeometricRepresentationItem>().Dim; }
        /// <summary>
        /// 方向
        /// </summary>
        public Direction Orientation { get; set; }
        /// <summary>
        /// 幅度
        /// </summary>
        public double Magnitude { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="direction"></param>
        /// <param name="magnitude"></param>
        public Vector(string name, Direction direction, double magnitude)
        {
            Orientation = direction ?? throw new ArgumentNullException(nameof(direction));
            Magnitude = magnitude;
            GeometricRepresentationItem item = new GeometricRepresentationItem(name, direction.Dim);
            SetSuperType(item);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="direction"></param>
        /// <param name="magnitude"></param>
        public Vector(GeometricRepresentationItem item, Direction direction, double magnitude)
        {
            if (item == null) throw new ArgumentNullException(nameof(item));
            Orientation = direction ?? throw new ArgumentNullException(nameof(direction));
            Magnitude = magnitude;
            SetSuperType(item);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>() => false;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>()
        {
            return typeof(TEntity) == typeof(GeometricRepresentationItem);
        }

        public override string ToString()
        {
            return $"Dir:{Orientation} Magnitude:{Magnitude}";
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public class VectorCodec : IExpressCodec
    {
        /// <summary>
        /// 
        /// </summary>
        public string Keyword => Schema.VECTOR;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceEntity"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public Entity Decode(Record sourceEntity, IEXPRESSDocument document)
        {
            var record = sourceEntity as SimpleRecord ?? throw new InvalidCastException($"{nameof(sourceEntity)} is not a SimpleRecord");
            if (record.Keyword != Keyword) throw new InvalidCastException($"Target instance is not {Keyword}");
            if (record.Parameters.Count != 3) throw new InvalidCastException($"{Schema.DIRECTION} parameters lost");
            var name = record.Parameters[0].As<StringParameter>().Value();
            var direction = record.Parameters[1].AsEnttiy<Direction>(document);
            var m = record.Parameters[2].As<RealParameter>().ParameterValue;
            Vector vector = new Vector(name, direction, m);
            return vector;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        /// <param name="body"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Entity DecodeWithComplex(SimpleRecord target, SubsuperRecord body, IEXPRESSDocument document)
        {
            throw new NotImplementedException();
        }
    }
}
