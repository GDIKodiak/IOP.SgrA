﻿using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.Entities
{
    /// <summary>
    /// 消息
    /// </summary>
    public struct Message : IExpress
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public string Keyword => Schema.MESSAGE;
        /// <summary>
        /// 
        /// </summary>
        private string value;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public Message(string v)
        {
            value = v ?? string.Empty;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString() => value;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public static explicit operator Message(string value) => new Message(value);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public static implicit operator string(Message message) => message.value;
    }
}
