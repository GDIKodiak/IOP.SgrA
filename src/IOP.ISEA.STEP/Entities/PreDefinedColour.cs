﻿using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.Entities
{
    public class PreDefinedColour : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get => AsSuperType<PreDefinedItem>().Name; }
        /// <summary>
        /// 关键字
        /// </summary>
        public override string Keyword => Schema.PRE_DEFINED_COLOUR;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public PreDefinedColour(string name)
        {
            PreDefinedItem item = new PreDefinedItem(name);
            SetSuperType(item);
            SetSuperType(new Colour());
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="colour"></param>
        public PreDefinedColour(PreDefinedItem item, Colour colour)
        {
            SetSuperType(item);
            SetSuperType(colour);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>()
        {
            var type = typeof(TEntity);
            return type == typeof(DraughtingPreDefinedColour);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>()
        {
            var type = typeof(TEntity);
            return type == typeof(PreDefinedItem)
                || type == typeof(Colour);
        }
    }
}
