﻿using IOP.ISEA.EXPRESS;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.STEP.Entities
{
    /// <summary>
    /// 重定向边
    /// </summary>
    public class OrientedEdge : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => Schema.ORIENTED_EDGE;
        /// <summary>
        /// 
        /// </summary>
        public string Name { get => AsSuperType<Edge>().Name; }
        /// <summary>
        /// 起始点
        /// </summary>
        public Vertex EdgeStart { get => AsSuperType<Edge>().EdgeStart; }
        /// <summary>
        /// 结束点
        /// </summary>
        public Vertex EdgeEnd { get => AsSuperType<Edge>().EdgeEnd; }
        /// <summary>
        /// 原始边
        /// </summary>
        public Edge EdgeElement { get; set; }
        /// <summary>
        /// 方向
        /// </summary>
        public bool Orientation { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="source"></param>
        /// <param name="orientation"></param>
        public OrientedEdge(string name, Edge source, bool orientation)
        {
            if (string.IsNullOrEmpty(name)) name = string.Empty;
            Orientation = orientation;
            EdgeElement = source ?? throw new ArgumentNullException(nameof(source));
            var start = Orientation ? source.EdgeStart : source.EdgeEnd;
            var end = Orientation ? source.EdgeEnd : source.EdgeStart;
            Edge edge = new Edge(name, start, end);
            SetSuperType(edge);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public override bool IsSubType<TEntity>()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(Edge);
    }
    /// <summary>
    /// 
    /// </summary>
    public class OrientedEdgeCodec : IExpressCodec
    {
        /// <summary>
        /// 
        /// </summary>
        public string Keyword => Schema.ORIENTED_EDGE;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceEntity"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public Entity Decode(Record sourceEntity, IEXPRESSDocument document)
        {
            var record = sourceEntity as SimpleRecord ?? throw new InvalidCastException($"{nameof(sourceEntity)} is not a SimpleRecord");
            if (record.Keyword != Keyword) throw new InvalidCastException($"Target instance is not {Keyword}");
            if (record.Parameters.Count != 5) throw new InvalidCastException($"{Keyword} parameters lost");
            var name = record.Parameters[0].As<StringParameter>().Value();
            var edge = record.Parameters[3].AsEnttiy<Edge>(document);
            var o = record.Parameters[4].AsLogical();
            OrientedEdge oriented = new OrientedEdge(name, edge, o);
            return oriented;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        /// <param name="body"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Entity DecodeWithComplex(SimpleRecord target, SubsuperRecord body, IEXPRESSDocument document)
        {
            throw new NotImplementedException();
        }
    }
}
