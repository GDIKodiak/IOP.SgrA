﻿using IOP.ISEA.EXPRESS;
using IOP.ISEA.STEP.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.STEP.Entities
{
    /// <summary>
    /// 
    /// </summary>
    public class Circle : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => Schema.CIRCLE;
        /// <summary>
        /// 
        /// </summary>
        public string Name { get => AsSuperType<Conic>().Name; }
        /// <summary>
        /// 
        /// </summary>
        public int Dim { get => AsSuperType<Conic>().Dim; }
        /// <summary>
        /// 坐标
        /// </summary>
        public Axis2Placement Position { get => AsSuperType<Conic>().Position; }
        /// <summary>
        /// 半径
        /// </summary>
        public double Radius { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="placement"></param>
        /// <param name="redius"></param>
        public Circle(string name, Axis2Placement placement, double redius)
        {
            if (placement == null) throw new ArgumentNullException(nameof(placement));
            Conic conic = new Conic(name, placement);
            SetSuperType(conic);
            Radius = redius;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="conic"></param>
        /// <param name="redius"></param>
        public Circle(Conic conic, double redius)
        {
            if (conic == null) throw new ArgumentNullException(nameof(conic));
            SetSuperType(conic);
            Radius = redius;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>() => false;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(Conic);
    }
    /// <summary>
    /// 
    /// </summary>
    public class CircleCodec : IExpressCodec
    {
        /// <summary>
        /// 
        /// </summary>
        public string Keyword => Schema.CIRCLE;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceEntity"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public Entity Decode(Record sourceEntity, IEXPRESSDocument document)
        {
            var record = sourceEntity as SimpleRecord ?? throw new InvalidCastException($"{nameof(sourceEntity)} is not a SimpleRecord");
            if (record.Keyword != Keyword) throw new InvalidCastException($"Target instance is not {Keyword}");
            if (record.Parameters.Count != 3) throw new InvalidCastException($"{Keyword} parameters lost");
            var name = record.Parameters[0].As<StringParameter>().Value();
            var plantment = record.Parameters[1].AsSelect<Axis2Placement>(document);
            var redius = record.Parameters[2].As<RealParameter>().Value();
            Circle circle = new Circle(name, plantment, redius);
            return circle;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        /// <param name="body"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Entity DecodeWithComplex(SimpleRecord target, SubsuperRecord body, IEXPRESSDocument document)
        {
            throw new NotImplementedException();
        }
    }
}
