﻿using IOP.ISEA.EXPRESS;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.STEP.Entities
{
    /// <summary>
    /// 拓扑呈现项
    /// </summary>
    public class TopologicalRepresentationItem : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => Schema.TOPOLOGICAL_REPRESENTATION_ITEM;
        /// <summary>
        /// 
        /// </summary>
        public string Name { get => AsSuperType<RepresentationItem>().Name; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public TopologicalRepresentationItem(string name)
        {
            if (string.IsNullOrEmpty(name)) name = string.Empty;
            RepresentationItem item = new RepresentationItem(name);
            SetSuperType(item);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public TopologicalRepresentationItem(RepresentationItem item)
        {
            if (item == null) throw new ArgumentNullException(nameof(RepresentationItem));
            SetSuperType(item);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>()
        {
            return typeof(TEntity) == typeof(Vertex)
                || typeof(TEntity) == typeof(Edge)
                || typeof(TEntity) == typeof(Loop)
                || typeof(TEntity) == typeof(Path)
                || typeof(TEntity) == typeof(FaceBound)
                || typeof(TEntity) == typeof(Face);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(RepresentationItem);
    }
}
