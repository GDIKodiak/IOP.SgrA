﻿using IOP.ISEA.EXPRESS;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.STEP.Entities
{
    /// <summary>
    /// 曲线
    /// </summary>
    public class Curve : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => Schema.CURVE;
        /// <summary>
        /// 
        /// </summary>
        public string Name { get => AsSuperType<GeometricRepresentationItem>().Name; }
        /// <summary>
        /// 
        /// </summary>
        public int Dim { get => AsSuperType<GeometricRepresentationItem>().Dim; }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>()
        {
            var type = typeof(TEntity);
            return type == typeof(Line) ||
                type == typeof(Conic) ||
                type == typeof(BoundedCurve);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>()
        {
            return typeof(TEntity) == typeof(GeometricRepresentationItem);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="dim"></param>
        public Curve(string name, int dim)
        {
            GeometricRepresentationItem item = new GeometricRepresentationItem(name, dim);
            SetSuperType(item);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public Curve(GeometricRepresentationItem item)
        {
            if (item == null) throw new ArgumentNullException(nameof(GeometricRepresentationItem));
            SetSuperType(item);
        }
    }
}
