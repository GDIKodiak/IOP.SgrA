﻿using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.Entities
{
    /// <summary>
    /// 标识符
    /// </summary>
    public struct Identifier : IExpress
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public string Keyword => Schema.IDENTIFIER;
        /// <summary>
        /// 
        /// </summary>
        private string value;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public Identifier(string v)
        {
            value = v ?? string.Empty;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString() => value;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public static explicit operator Identifier(string value) => new Identifier(value);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        public static implicit operator string(Identifier identifier) => identifier.value;
    }
}
