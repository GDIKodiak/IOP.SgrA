﻿using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.Entities
{
    /// <summary>
    /// 几何表达项
    /// </summary>
    public class GeometricRepresentationItem : Entity
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get => AsSuperType<RepresentationItem>().Name; }
        /// <summary>
        /// 维数
        /// </summary>
        public int Dim { get; set; }
        /// <summary>
        /// 关键字
        /// </summary>
        public override string Keyword => Schema.GEOMETRIC_REPRESENTATION_ITEM;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="name"></param>
        /// <param name="dim"></param>
        public GeometricRepresentationItem(string name, int dim)
        {
            RepresentationItem item = new RepresentationItem(name);
            SetSuperType(item);
            Dim = dim;
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="parnet"></param>
        /// <param name="dim"></param>
        public GeometricRepresentationItem(RepresentationItem parent, int dim)
        {
            if (parent == null) throw new ArgumentNullException(nameof(parent));
            SetSuperType(parent);
            Dim = dim;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(RepresentationItem);
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>()
        {
            var type = typeof(TEntity);
            return type == typeof(Point) ||
                type == typeof(VertexPoint) ||
                type == typeof(Direction) ||
                type == typeof(Surface) ||
                type == typeof(FaceSurface);
        }
    }
}
