﻿using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.Entities
{
    /// <summary>
    /// 平面
    /// </summary>
    public class Plane : Entity
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get => AsSuperType<ElementarySurface>().Name; }
        /// <summary>
        /// 维数
        /// </summary>
        public int Dim { get => AsSuperType<ElementarySurface>().Dim; }
        /// <summary>
        /// 坐标
        /// </summary>
        public Axis2Placement3D Position { get => AsSuperType<ElementarySurface>().Position; }
        /// <summary>
        /// 关键字
        /// </summary>
        public override string Keyword => Schema.PLANE;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="name"></param>
        /// <param name="dim"></param>
        /// <param name="position"></param>
        public Plane(string name, Axis2Placement3D position)
        {
            ElementarySurface surface = new ElementarySurface(name, position);
            SetSuperType(surface);
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="super"></param>
        /// <param name="position"></param>
        public Plane(ElementarySurface super)
        {
            SetSuperType(super);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>() => false;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(ElementarySurface);
    }

    /// <summary>
    /// 解码器
    /// </summary>
    public class PlaneCodec : IExpressCodec
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public string Keyword => Schema.PLANE;
        /// <summary>
        /// 解码
        /// </summary>
        /// <param name="sourceEntity"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Entity Decode(Record sourceEntity, IEXPRESSDocument document)
        {
            var record = sourceEntity as SimpleRecord ?? throw new InvalidCastException($"{nameof(sourceEntity)} is not a SimpleRecord");
            if (record.Keyword != Keyword) throw new InvalidCastException($"Target instance is not {Keyword}");
            string name = string.Empty;
            Axis2Placement3D position = null;
            foreach (var item in record.Parameters)
            {
                if (item.Is<StringParameter>())
                {
                    name = item.As<StringParameter>().ParameterValue;
                }
                else if (item.Is<EntityNameParameter>())
                {
                    position = item.AsEnttiy<Axis2Placement3D>(document);
                }
            }
            if (position == null) throw new InvalidCastException($"{Schema.PLANE} parameters lost");
            Plane plane = new Plane(name, position);
            return plane;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        /// <param name="body"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Entity DecodeWithComplex(SimpleRecord target, SubsuperRecord body, IEXPRESSDocument document)
        {
            throw new NotImplementedException();
        }
    }
}
