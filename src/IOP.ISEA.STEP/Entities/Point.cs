﻿using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.Entities
{
    /// <summary>
    /// 抽象点
    /// </summary>
    public class Point : Entity
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get => AsSuperType<GeometricRepresentationItem>().Name; }
        /// <summary>
        /// 维数
        /// </summary>
        public int Dim { get => AsSuperType<GeometricRepresentationItem>().Dim; }
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => Schema.POINT;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="dim"></param>
        public Point(string name, int dim)
        {
            GeometricRepresentationItem item = new GeometricRepresentationItem(name, dim);
            SetSuperType(item);
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="dim"></param>
        public Point(GeometricRepresentationItem parent, int dim)
        {
            if (parent == null) throw new ArgumentNullException(nameof(parent));
            SetSuperType(parent);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(GeometricRepresentationItem);
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>()
        {
            var type = typeof(TEntity);
            return type == typeof(CartesianPoint);
        }
    }
}
