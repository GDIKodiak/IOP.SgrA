﻿using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.Entities
{
    /// <summary>
    /// 三维双轴安置点
    /// </summary>
    public class Axis2Placement3D : Entity
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get => AsSuperType<Placement>().Name; }
        /// <summary>
        /// 位置
        /// </summary>
        public CartesianPoint Location { get => AsSuperType<Placement>().Location; }
        /// <summary>
        /// 维数
        /// </summary>
        public int Dim { get; }
        /// <summary>
        /// 轴
        /// </summary>
        public Direction Axis { get; set; }
        /// <summary>
        /// 参考方向
        /// </summary>
        public Direction RefDirection { get; set; }
        /// <summary>
        /// 关键字
        /// </summary>
        public override string Keyword => Schema.AXIS2_PLACEMENT_3D;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="name"></param>
        /// <param name="point"></param>
        public Axis2Placement3D(string name, CartesianPoint point, Direction axis, Direction refDirection)
        {
            Placement placement = new Placement(name, point);
            SetSuperType(placement);
            Dim = 3;
            Axis = axis ?? throw new ArgumentNullException(nameof(axis));
            RefDirection = refDirection ?? throw new ArgumentNullException(nameof(refDirection));
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="super"></param>
        /// <param name="axis"></param>
        /// <param name="refDirection"></param>
        public Axis2Placement3D(Placement super, Direction axis, Direction refDirection)
        {
            SetSuperType(super);
            Dim = 3;
            Axis = axis ?? throw new ArgumentNullException(nameof(axis));
            RefDirection = refDirection ?? throw new ArgumentNullException(nameof(refDirection));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(Placement);
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>() => false;
    }

    /// <summary>
    /// 解码器
    /// </summary>
    public class Axis2Placement3DCodec : IExpressCodec
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public string Keyword => Schema.AXIS2_PLACEMENT_3D;
        /// <summary>
        /// 解码
        /// </summary>
        /// <param name="sourceEntity"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public Entity Decode(Record sourceEntity, IEXPRESSDocument document)
        {
            var record = sourceEntity as SimpleRecord ?? throw new InvalidCastException($"{nameof(sourceEntity)} is not a SimpleRecord");
            if (record.Keyword != Keyword) throw new InvalidCastException($"Target instance is not {Keyword}");
            if (record.Parameters.Count != 4) throw new InvalidCastException($"{Schema.AXIS2_PLACEMENT_3D} parameters lost");
            var name = record.Parameters[0].As<StringParameter>().Value();
            var location = record.Parameters[1].AsEnttiy<CartesianPoint>(document);
            var axis = record.Parameters[2].AsEnttiy<Direction>(document);
            var refDir = record.Parameters[3].AsEnttiy<Direction>(document);
            Axis2Placement3D placement3D = new Axis2Placement3D(name, location, axis, refDir);
            return placement3D;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        /// <param name="body"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Entity DecodeWithComplex(SimpleRecord target, SubsuperRecord body, IEXPRESSDocument document)
        {
            throw new NotImplementedException();
        }
    }
}
