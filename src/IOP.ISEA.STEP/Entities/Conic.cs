﻿using IOP.ISEA.EXPRESS;
using IOP.ISEA.STEP.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.STEP.Entities
{
    /// <summary>
    /// 平面曲线
    /// </summary>
    public class Conic : Entity
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public override string Keyword => Schema.CONIC;
        /// <summary>
        /// 
        /// </summary>
        public string Name { get => AsSuperType<Curve>().Name; }
        /// <summary>
        /// 
        /// </summary>
        public int Dim { get => AsSuperType<Curve>().Dim; }
        /// <summary>
        /// 
        /// </summary>
        public Axis2Placement Position { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="placement"></param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="InvalidOperationException"></exception>
        public Conic(string name, Axis2Placement placement)
        {
            if (placement == null) throw new ArgumentNullException(nameof(placement));
            if (placement.TryGetSelectItem<Axis2Placement3D>(out var _))
            {
                Curve curve = new Curve(name, 3);
                SetSuperType(curve);
            }
            else throw new InvalidOperationException($"Select {placement.Keyword} is a invalid select, select item lost");
            Position = placement;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="curve"></param>
        /// <param name="placement"></param>
        public Conic(Curve curve, Axis2Placement placement)
        {
            if (placement == null) throw new ArgumentNullException(nameof(placement));
            if (placement.TryGetSelectItem<Axis2Placement3D>(out var _))
            {
                if (curve.Dim != 3) throw new InvalidOperationException($"Parameter {placement.Keyword} Dim is 3, but SuperType's Dim is 2");
                SetSuperType(curve);
            }
            else throw new InvalidOperationException($"Select {placement.Keyword} is a invalid select, select item lost");
            Position = placement;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>()
        {
            var type = typeof(TEntity);
            return type == typeof(Circle);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(Curve);
    }
}
