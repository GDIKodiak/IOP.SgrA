﻿using IOP.ISEA.EXPRESS;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.STEP.Entities
{
    /// <summary>
    /// 高级拓扑实体
    /// </summary>
    public class AdvancedFace : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => Schema.ADVANCED_FACE;
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get => AsSuperType<FaceSurface>().Name; }
        /// <summary>
        /// 边界
        /// </summary>
        public List<FaceBound> Bounds { get => AsSuperType<FaceSurface>().Bounds; }
        /// <summary>
        /// 关联几何
        /// </summary>
        public Surface FaceGeometry { get => AsSuperType<FaceSurface>().FaceGeometry; }
        /// <summary>
        /// 是否同向
        /// </summary>
        public bool SameSense { get => AsSuperType<FaceSurface>().SameSense; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public AdvancedFace(FaceSurface p)
        {
            if (p == null) throw new ArgumentNullException(nameof(p));
            SetSuperType(p);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="bounds"></param>
        /// <param name="surface"></param>
        /// <param name="sameSence"></param>
        public AdvancedFace(string name, List<FaceBound> bounds, Surface surface, bool sameSence)
        {
            FaceSurface p = new FaceSurface(name, bounds, surface, sameSence);
            SetSuperType(p);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>() => false;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(FaceSurface);
    }

    /// <summary>
    /// 
    /// </summary>
    public class AdvancedFaceCodec : IExpressCodec
    {
        /// <summary>
        /// 
        /// </summary>
        public string Keyword => Schema.ADVANCED_FACE;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceEntity"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public Entity Decode(Record sourceEntity, IEXPRESSDocument document)
        {
            var record = sourceEntity as SimpleRecord ?? throw new InvalidCastException($"{nameof(sourceEntity)} is not a SimpleRecord");
            if (record.Keyword != Keyword) throw new InvalidCastException($"Target instance is not {Keyword}");
            if (record.Parameters.Count != 4) throw new InvalidCastException($"{Keyword} parameters lost");
            var name = record.Parameters[0].As<StringParameter>().Value();
            List<FaceBound> bounds = record.Parameters[1].AsEntityList<FaceBound>(document);
            Surface surface = record.Parameters[2].AsEnttiy<Surface>(document);
            bool sameSence = record.Parameters[3].AsLogical();
            AdvancedFace face = new AdvancedFace(name, bounds, surface, sameSence);
            return face;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        /// <param name="body"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Entity DecodeWithComplex(SimpleRecord target, SubsuperRecord body, IEXPRESSDocument document)
        {
            throw new NotImplementedException();
        }
    }
}
