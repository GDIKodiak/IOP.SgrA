﻿using IOP.ISEA.EXPRESS;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.STEP.Entities
{
    /// <summary>
    /// 拓扑曲面
    /// </summary>
    public class FaceSurface : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => Schema.FACE_SURFACE;
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get => AsSuperType<GeometricRepresentationItem>().Name; }
        /// <summary>
        /// 边界
        /// </summary>
        public List<FaceBound> Bounds { get => AsSuperType<Face>().Bounds; }
        /// <summary>
        /// 几何曲面
        /// </summary>
        public Surface FaceGeometry { get; set; }
        /// <summary>
        /// 是否同向
        /// </summary>
        public bool SameSense { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="dim"></param>
        /// <param name="bounds"></param>
        /// <param name="surface"></param>
        /// <param name="sameSence"></param>
        public FaceSurface(string name, List<FaceBound> bounds, Surface surface, bool sameSence)
        {
            if (string.IsNullOrEmpty(name)) name = string.Empty;
            if (bounds == null) throw new ArgumentNullException(nameof(bounds));
            FaceGeometry = surface ?? throw new ArgumentNullException(nameof(surface));
            GeometricRepresentationItem p = new GeometricRepresentationItem(name, surface.Dim);
            Face face = new Face(name, bounds);
            SetSuperType(p); SetSuperType(face);
            SameSense = sameSence;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="face"></param>
        /// <param name="surface"></param>
        /// <param name="sameSence"></param>
        public FaceSurface(GeometricRepresentationItem item, Face face, Surface surface, bool sameSence)
        {
            if (item == null) throw new ArgumentNullException(nameof(item));
            if (face == null) throw new ArgumentNullException(nameof(face));
            SetSuperType(item); SetSuperType(face);
            FaceGeometry = surface ?? throw new ArgumentNullException(nameof(surface));
            SameSense = sameSence;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>()
        {
            var type = typeof(TEntity);
            return type == typeof(AdvancedFace);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>()
        {
            var t = typeof(TEntity);
            return t == typeof(Face) ||
                t == typeof(GeometricRepresentationItem);
        }
    }
}
