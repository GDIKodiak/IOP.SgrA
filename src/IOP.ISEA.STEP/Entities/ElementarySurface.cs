﻿using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.Entities
{
    /// <summary>
    /// 简单解析曲面
    /// </summary>
    public class ElementarySurface : Entity
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get => AsSuperType<Surface>().Name; }
        /// <summary>
        /// 维数
        /// </summary>
        public int Dim { get => AsSuperType<Surface>().Dim; }
        /// <summary>
        /// 关键字
        /// </summary>
        public override string Keyword => Schema.ELEMENTARY_SURFACE;
        /// <summary>
        /// 位置
        /// </summary>
        public Axis2Placement3D Position { get; set; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="name"></param>
        /// <param name="dim"></param>
        /// <param name="position"></param>
        public ElementarySurface(string name, Axis2Placement3D position)
        {
            Surface surface = new Surface(name, 3);
            SetSuperType(surface);
            Position = position;
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="super"></param>
        /// <param name="position"></param>
        public ElementarySurface(Surface super, Axis2Placement3D position)
        {
            SetSuperType(super);
            Position = position;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>() => typeof(TEntity) == typeof(Plane);
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(Surface);
    }
}
