﻿using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.Entities
{
    /// <summary>
    /// 待定义项
    /// </summary>
    public class PreDefinedItem : Entity
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 关键字
        /// </summary>
        public override string Keyword => Schema.PRE_DEFINED_ITEM;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="name"></param>
        public PreDefinedItem(string name)
        {
            if (string.IsNullOrEmpty(name)) name = string.Empty;
            Name = name;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>()
        {
            var type = typeof(TEntity);
            return type == typeof(PreDefinedCurveFont);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => false;
    }
}
