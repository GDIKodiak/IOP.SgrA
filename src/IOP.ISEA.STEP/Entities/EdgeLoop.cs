﻿using IOP.ISEA.EXPRESS;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.STEP.Entities
{
    /// <summary>
    /// 边环
    /// </summary>
    public class EdgeLoop : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => Schema.EDGE_LOOP;
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get => AsSuperType<Loop>().Name; }
        /// <summary>
        /// 边列表
        /// </summary>
        public List<OrientedEdge> EdgeList { get => AsSuperType<Path>().EdgeList; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public EdgeLoop(string name, IEnumerable<OrientedEdge> edges)
        {
            if (string.IsNullOrEmpty(name)) name = string.Empty;
            if (edges == null) throw new ArgumentNullException(nameof(edges));
            TopologicalRepresentationItem parent = new TopologicalRepresentationItem(name);
            Loop loop = new Loop(parent);
            Path path = new Path(parent, edges);
            SetSuperType(loop);
            SetSuperType(path);
        }

        public override bool IsSubType<TEntity>()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>()
        {
            return typeof(TEntity) == typeof(Loop) ||
                typeof(TEntity) == typeof(Path);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class EdgeLoopCodec : IExpressCodec
    {
        /// <summary>
        /// 
        /// </summary>
        public string Keyword => Schema.EDGE_LOOP;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceEntity"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public Entity Decode(Record sourceEntity, IEXPRESSDocument document)
        {
            var record = sourceEntity as SimpleRecord ?? throw new InvalidCastException($"{nameof(sourceEntity)} is not a SimpleRecord");
            if (record.Keyword != Keyword) throw new InvalidCastException($"Target instance is not {Keyword}");
            if (record.Parameters.Count != 2) throw new InvalidCastException($"{Keyword} parameters lost");
            var name = record.Parameters[0].As<StringParameter>().Value();
            var list = record.Parameters[1].AsEntityList<OrientedEdge>(document);
            EdgeLoop edgeLoop = new EdgeLoop(name, list);
            return edgeLoop;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        /// <param name="body"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Entity DecodeWithComplex(SimpleRecord target, SubsuperRecord body, IEXPRESSDocument document)
        {
            throw new NotImplementedException();
        }
    }
}
