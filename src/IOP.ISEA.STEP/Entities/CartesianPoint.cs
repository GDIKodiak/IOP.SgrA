﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.Entities
{
    /// <summary>
    /// 笛卡尔点
    /// </summary>
    public class CartesianPoint : Entity
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get => AsSuperType<Point>().Name; }
        /// <summary>
        /// 维数
        /// </summary>
        public int Dim { get => AsSuperType<Point>().Dim; }
        /// <summary>
        /// 坐标
        /// </summary>
        public Vector3 Coordinates { get; set; }
        /// <summary>
        /// 关键字
        /// </summary>
        public override string Keyword => Schema.CARTESIAN_POINT;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="dim"></param>
        public CartesianPoint(string name, Vector3 vector)
        {
            Point point = new Point(name, 3);
            SetSuperType(point);
            Coordinates = vector;
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="name"></param>
        /// <param name="v"></param>
        public CartesianPoint(string name, double v)
        {
            Point point = new Point(name, 1);
            SetSuperType(point);
            Coordinates = new Vector3((float)v, 0, 0);
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="name"></param>
        /// <param name="vector"></param>
        public CartesianPoint(string name, Vector2 vector)
        {
            Point point = new Point(name, 2);
            SetSuperType(point);
            Coordinates = new Vector3(vector, 0);
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="vector"></param>
        public CartesianPoint(Point parent, Vector3 vector)
        {
            if (parent == null) throw new ArgumentNullException(nameof(parent));
            SetSuperType(parent);
            Coordinates = vector;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(Point);
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="name"></param>
        /// <param name="vector"></param>
        /// <param name="dim"></param>
        internal protected CartesianPoint(string name, Vector3 vector, int dim)
        {
            Point point = new Point(name, dim);
            SetSuperType(point);
            Coordinates = vector;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString() => Coordinates.ToString();
    }

    /// <summary>
    /// 笛卡尔点编解码器
    /// </summary>
    public class CartesianPointCodec : IExpressCodec
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public string Keyword => Schema.CARTESIAN_POINT;
        /// <summary>
        /// 解码
        /// </summary>
        /// <param name="sourceEntity"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Entity Decode(Record sourceEntity, IEXPRESSDocument document)
        {
            var record = sourceEntity as SimpleRecord ?? throw new InvalidCastException($"{nameof(sourceEntity)} is not a SimpleRecord");
            if (record.Keyword != Keyword) throw new InvalidCastException($"Target instance is not {Keyword}");
            if (record.Parameters.Count != 2) throw new InvalidCastException($"{nameof(CartesianPoint)} parameters lost");
            var name = record.Parameters[0].As<StringParameter>().Value();
            var vector = record.Parameters[1].AsVector3(out int dim);
            CartesianPoint point = new CartesianPoint(name, vector, dim);
            return point;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        /// <param name="body"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Entity DecodeWithComplex(SimpleRecord target, SubsuperRecord body, IEXPRESSDocument document)
        {
            throw new NotImplementedException();
        }
    }
}
