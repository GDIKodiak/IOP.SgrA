﻿using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.Entities
{
    /// <summary>
    /// 
    /// </summary>
    public struct DescriptiveMeasure : IExpress
    {
        /// <summary>
        /// 
        /// </summary>
        public string Keyword => Schema.DESCRIPTIVE_MEASURE;
        /// <summary>
        /// 
        /// </summary>
        private string value;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public DescriptiveMeasure(string v)
        {
            value = v ?? string.Empty;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString() => value;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public static explicit operator DescriptiveMeasure(string value) => new DescriptiveMeasure(value);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public static implicit operator string(DescriptiveMeasure message) => message.value;
    }
}
