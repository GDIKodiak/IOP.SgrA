﻿using IOP.ISEA.STEP.RepresentationSchema;
using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP
{
    /// <summary>
    /// 模式
    /// </summary>
    public static partial class Schema
    {
        #region KEYWORDS
        /// <summary>
        /// 呈现项
        /// </summary>
        public const string REPRESENTATION_ITEM = "REPRESENTATION_ITEM";
        /// <summary>
        /// 几何呈现项
        /// </summary>
        public const string GEOMETRIC_REPRESENTATION_ITEM = "GEOMETRIC_REPRESENTATION_ITEM";
        /// <summary>
        /// 拓扑呈现项
        /// </summary>
        public const string TOPOLOGICAL_REPRESENTATION_ITEM = "TOPOLOGICAL_REPRESENTATION_ITEM";

        /// <summary>
        /// 方向
        /// </summary>
        public const string DIRECTION = "DIRECTION";
        /// <summary>
        /// 向量
        /// </summary>
        public const string VECTOR = "VECTOR";
        /// <summary>
        /// 拓扑结构
        /// </summary>
        public const string VERTEX = "VERTEX";

        /// <summary>
        /// 曲线
        /// </summary>
        public const string CURVE = "CURVE";
        /// <summary>
        /// 直线
        /// </summary>
        public const string LINE = "LINE";
        /// <summary>
        /// 平面曲线
        /// </summary>
        public const string CONIC = "CONIC";
        /// <summary>
        /// 圆
        /// </summary>
        public const string CIRCLE = "CIRCLE";
        /// <summary>
        /// 有限弧长封闭曲线
        /// </summary>
        public const string BOUNDED_CURVE = "BOUNDED_CURVE";
        /// <summary>
        /// B样条曲线
        /// </summary>
        public const string B_SPLINE_CURVE = "B_SPLINE_CURVE";
        /// <summary>
        /// 非均匀B样条曲线
        /// </summary>
        public const string B_SPLINE_CURVE_WITH_KNOTS = "B_SPLINE_CURVE_WITH_KNOTS";
        /// <summary>
        /// 边
        /// </summary>
        public const string EDGE = "EDGE";
        /// <summary>
        /// 边曲线
        /// </summary>
        public const string EDGE_CURVE = "EDGE_CURVE";
        /// <summary>
        /// 重定向边
        /// </summary>
        public const string ORIENTED_EDGE = "ORIENTED_EDGE";
        /// <summary>
        /// 环
        /// </summary>
        public const string LOOP = "LOOP";
        /// <summary>
        /// 路径
        /// </summary>
        public const string PATH = "PATH";
        /// <summary>
        /// 边环
        /// </summary>
        public const string EDGE_LOOP = "EDGE_LOOP";

        /// <summary>
        /// 点
        /// </summary>
        public const string POINT = "POINT";
        /// <summary>
        /// 笛卡尔点
        /// </summary>
        public const string CARTESIAN_POINT = "CARTESIAN_POINT";
        /// <summary>
        /// 拓扑点
        /// </summary>
        public const string VERTEX_POINT = "VERTEX_POINT";

        /// <summary>
        /// 双轴安置点空间
        /// </summary>
        public const string AXIS2_PLACEMENT = "AXIS2_PLACEMENT";
        /// <summary>
        /// 三维双轴安置点
        /// </summary>
        public const string AXIS2_PLACEMENT_3D = "AXIS2_PLACEMENT_3D";
        /// <summary>
        /// 安置点
        /// </summary>
        public const string PLACEMENT = "PLACEMENT";

        /// <summary>
        /// 拓扑面
        /// </summary>
        public const string FACE = "FACE";
        /// <summary>
        /// 曲面
        /// </summary>
        public const string SURFACE = "SURFACE";
        /// <summary>
        /// 简单解析曲面
        /// </summary>
        public const string ELEMENTARY_SURFACE = "ELEMENTARY_SURFACE";
        /// <summary>
        /// 平面
        /// </summary>
        public const string PLANE = "PLANE";
        /// <summary>
        /// 拓扑曲面
        /// </summary>
        public const string FACE_SURFACE = "FACE_SURFACE";
        /// <summary>
        /// 高级拓扑实体
        /// </summary>
        public const string ADVANCED_FACE = "ADVANCED_FACE";

        /// <summary>
        /// 面边界
        /// </summary>
        public const string FACE_BOUND = "FACE_BOUND";
        /// <summary>
        /// 面外边界
        /// </summary>
        public const string FACE_OUTER_BOUND = "FACE_OUTER_BOUND";

        /// <summary>
        /// 颜色
        /// </summary>
        public const string COLOUR = "COLOUR";
        /// <summary>
        /// 颜色规范
        /// </summary>
        public const string COLOUR_SPECIFICATION = "COLOUR_SPECIFICATION";
        /// <summary>
        /// RGB颜色
        /// </summary>
        public const string COLOUR_RGB = "COLOUR_RGB";

        /// <summary>
        /// 创建项
        /// </summary>
        public const string FOUNDED_ITEM = "FOUNDED_ITEM";
        /// <summary>
        /// 样式项
        /// </summary>
        public const string STYLED_ITEM = "STYLED_ITEM";
        /// <summary>
        /// 样式项目标
        /// </summary>
        public const string STYLED_ITEM_TARGET = "STYLED_ITEM_TARGET";
        /// <summary>
        /// 曲线样式
        /// </summary>
        public const string CURVE_STYLE = "CURVE_STYLE";
        /// <summary>
        /// 曲线文字样式
        /// </summary>
        public const string CURVE_STYLE_FONT = "CURVE_STYLE_FONT";
        /// <summary>
        /// 曲线字体式样
        /// </summary>
        public const string CURVE_STYLE_FONT_PATTERN = "CURVE_STYLE_FONT_PATTERN";
        /// <summary>
        /// 曲线字体样式或者缩放曲线样式选择
        /// </summary>
        public const string CURVE_FONT_OR_SCALED_CURVE_FONT_SELECT = "CURVE_FONT_OR_SCALED_CURVE_FONT_SELECT";
        /// <summary>
        /// 曲线字体样式选择
        /// </summary>
        public const string CURVE_STYLE_FONT_SELECT = "CURVE_STYLE_FONT_SELECT";
        /// <summary>
        /// 曲线字体样式和缩放
        /// </summary>
        public const string CURVE_STYLE_FONT_AND_SCALING = "CURVE_STYLE_FONT_AND_SCALING";
        /// <summary>
        /// 呈现样式分配
        /// </summary>
        public const string PRESENTATION_STYLE_ASSIGNMENT = "PRESENTATION_STYLE_ASSIGNMENT";
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        public static void AddSchemaRepresentation(this IEXPRESSDocument document)
        {
            document.AddEntityCodec(VERTEX_POINT, new VertexPointCodec());
            document.AddEntityCodec(CARTESIAN_POINT, new CartesianPointCodec());
            document.AddEntityCodec(DIRECTION, new DirectionCodec());
            document.AddEntityCodec(VECTOR, new VectorCodec());
            document.AddEntityCodec(LINE, new LineCodec());
            document.AddEntityCodec(CIRCLE, new CircleCodec());
            document.AddEntityCodec(AXIS2_PLACEMENT_3D, new Axis2Placement3DCodec());
            document.AddEntityCodec(PLANE, new PlaneCodec());
            document.AddEntityCodec(CURVE_STYLE, new CurveStyleCodec());
            document.AddEntityCodec(PRESENTATION_STYLE_ASSIGNMENT, new PresentationStyleAssignmentCodec());
            document.AddEntityCodec(STYLED_ITEM, new StyledItemCodec());
            document.AddEntityCodec(COLOUR_RGB, new ColourRgbCodec());
            document.AddEntityCodec(B_SPLINE_CURVE_WITH_KNOTS, new BSplineCurveWithKnotsCodec());
            document.AddEntityCodec(EDGE_CURVE, new EdgeCurveCodec());
            document.AddEntityCodec(ORIENTED_EDGE, new OrientedEdgeCodec());
            document.AddEntityCodec(EDGE_LOOP, new EdgeLoopCodec());
            document.AddEntityCodec(FACE_OUTER_BOUND, new FaceOuterBoundCodec());
            document.AddEntityCodec(ADVANCED_FACE, new AdvancedFaceCodec());
        }
    }
}
