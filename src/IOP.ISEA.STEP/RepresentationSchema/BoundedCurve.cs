﻿using IOP.ISEA.EXPRESS;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.STEP.RepresentationSchema
{
    /// <summary>
    /// 有限弧长封闭曲线
    /// </summary>
    public class BoundedCurve : Entity
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public override string Keyword => Schema.BOUNDED_CURVE;
        /// <summary>
        /// 
        /// </summary>
        public string Name { get => AsSuperType<Curve>().Name; }
        /// <summary>
        /// 
        /// </summary>
        public int Dim { get => AsSuperType<Curve>().Dim; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="dim"></param>
        public BoundedCurve(string name, int dim)
        {
            Curve curve = new Curve(name, dim);
            SetSuperType(curve);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="curve"></param>
        public BoundedCurve(Curve curve)
        {
            if (curve == null) throw new ArgumentNullException(nameof(Curve));
            SetSuperType(curve);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>()
        {
            var type = typeof(TEntity);
            return type == typeof(BSplineCurve);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(Curve);
    }
}
