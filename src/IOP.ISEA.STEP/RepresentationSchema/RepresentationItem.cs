﻿using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.RepresentationSchema
{
    /// <summary>
    /// 表达项
    /// </summary>
    public class RepresentationItem : Entity
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 关键字
        /// </summary>
        public override string Keyword => Schema.REPRESENTATION_ITEM;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="name"></param>
        public RepresentationItem(string name)
        {
            if (string.IsNullOrEmpty(name)) name = string.Empty;
            Name = name;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => false;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>()
        {
            var type = typeof(TEntity);
            return type == typeof(GeometricRepresentationItem);
        }
    }
}
