﻿using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.RepresentationSchema
{
    /// <summary>
    /// 曲线文字样式
    /// </summary>
    public class CurveStyleFont : Entity
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public override string Keyword => Schema.CURVE_STYLE_FONT;
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 式样列表
        /// </summary>
        public readonly List<CurveStyleFontPattern> PatternList = new List<CurveStyleFontPattern>();
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="name"></param>
        /// <param name="patterns"></param>
        public CurveStyleFont(string name, params CurveStyleFontPattern[] patterns)
        {
            if (patterns == null || patterns.Length < 1) throw new ArgumentNullException(nameof(patterns));
            Name = name;
            PatternList.AddRange(patterns);
            FoundedItem item = new FoundedItem();
            SetSuperType(item);
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="item"></param>
        /// <param name="name"></param>
        /// <param name="patterns"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public CurveStyleFont(FoundedItem item, string name, params CurveStyleFontPattern[] patterns)
        {
            if (item == null) throw new ArgumentNullException(nameof(item));
            if (patterns == null || patterns.Length < 1) throw new ArgumentNullException(nameof(patterns));
            Name = name;
            PatternList.AddRange(patterns);
            SetSuperType(item);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(FoundedItem);
    }
}
