﻿using IOP.ISEA.EXPRESS;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.STEP.RepresentationSchema
{
    /// <summary>
    /// 边
    /// </summary>
    public class Edge : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => Schema.EDGE;
        /// <summary>
        /// 
        /// </summary>
        public string Name { get => AsSuperType<TopologicalRepresentationItem>().Name; }
        /// <summary>
        /// 起始点
        /// </summary>
        public Vertex EdgeStart { get; set; }
        /// <summary>
        /// 终止点
        /// </summary>
        public Vertex EdgeEnd { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public Edge(string name , Vertex start, Vertex end)
        {
            if(string.IsNullOrEmpty(name)) name = string.Empty;
            EdgeStart = start ?? throw new ArgumentNullException(nameof(start));
            EdgeEnd = end ?? throw new ArgumentNullException(nameof(end));
            TopologicalRepresentationItem item = new TopologicalRepresentationItem(name);
            SetSuperType(item);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        public Edge(RepresentationItem item, Vertex start, Vertex end)
        {
            if(item == null) throw new ArgumentNullException(nameof(item));
            EdgeStart = start ?? throw new ArgumentNullException(nameof(start));
            EdgeEnd = end ?? throw new ArgumentNullException(nameof(end));
            TopologicalRepresentationItem p = new TopologicalRepresentationItem(item);
            SetSuperType(p);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>()
        {
            return typeof(TEntity) == typeof(EdgeCurve);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(TopologicalRepresentationItem);
    }
}
