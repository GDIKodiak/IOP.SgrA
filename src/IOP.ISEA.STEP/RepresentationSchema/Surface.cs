﻿using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.RepresentationSchema
{
    /// <summary>
    /// 曲面基类
    /// </summary>
    public class Surface : Entity
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get => AsSuperType<GeometricRepresentationItem>().Name; }
        /// <summary>
        /// 关键字
        /// </summary>
        public override string Keyword => Schema.SURFACE;
        /// <summary>
        /// 维数
        /// </summary>
        public int Dim { get => AsSuperType<GeometricRepresentationItem>().Dim; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="name"></param>
        /// <param name="dim"></param>
        public Surface(string name, int dim) 
        {
            GeometricRepresentationItem item = new GeometricRepresentationItem(name, dim);
            SetSuperType(item);
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="super"></param>
        public Surface(GeometricRepresentationItem super)
        {
            if (super == null) throw new ArgumentNullException(nameof(super));
            SetSuperType(super);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>()
        {
            var type = typeof(TEntity);
            return type == typeof(ElementarySurface);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(GeometricRepresentationItem);
    }
}
