﻿using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.RepresentationSchema
{
    /// <summary>
    /// 安置点
    /// </summary>
    public class Placement : Entity
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get => AsSuperType<GeometricRepresentationItem>().Name; }
        /// <summary>
        /// 维数
        /// </summary>
        public int Dim { get => AsSuperType<GeometricRepresentationItem>().Dim; }
        /// <summary>
        /// 位置
        /// </summary>
        public CartesianPoint Location { get; set; }
        /// <summary>
        /// 关键字
        /// </summary>
        public override string Keyword => Schema.PLACEMENT;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="name"></param>
        /// <param name="point"></param>
        public Placement(string name, CartesianPoint point) 
        {
            GeometricRepresentationItem item = new GeometricRepresentationItem(name, point.Dim);
            SetSuperType(item);
            Location = point ?? throw new ArgumentNullException(nameof(point));
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="super"></param>
        /// <param name="point"></param>
        public Placement(GeometricRepresentationItem super, CartesianPoint point)
        {
            SetSuperType(super);
            Location = point;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(GeometricRepresentationItem);
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>()
        {
            var type = typeof(TEntity);
            return type == typeof(Axis2Placement3D);
        }
    }
}
