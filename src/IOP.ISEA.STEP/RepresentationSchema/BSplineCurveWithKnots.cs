﻿using IOP.ISEA.EXPRESS;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.STEP.RepresentationSchema
{
    /// <summary>
    /// 非均匀有理B样条曲线
    /// </summary>
    public class BSplineCurveWithKnots : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => Schema.B_SPLINE_CURVE_WITH_KNOTS;
        /// <summary>
        /// 
        /// </summary>
        public string Name { get => AsSuperType<BSplineCurve>().Name; }
        /// <summary>
        /// 基函数代数阶
        /// </summary>
        public int Degree { get => AsSuperType<BSplineCurve>().Degree; }
        /// <summary>
        /// 控制点列表
        /// </summary>
        public List<CartesianPoint> ControlPointsList { get => AsSuperType<BSplineCurve>().ControlPointsList; }
        /// <summary>
        /// 此曲线代表的某具体曲线
        /// </summary>
        public BSplineCurveForm CurveForm { get => AsSuperType<BSplineCurve>().CurveForm; }
        /// <summary>
        /// 是否为封闭曲线
        /// </summary>
        public bool ClosedCurve { get => AsSuperType<BSplineCurve>().ClosedCurve; }
        /// <summary>
        /// 曲线是否自相交
        /// </summary>
        public bool SelfIntersect { get => AsSuperType<BSplineCurve>().SelfIntersect; }
        /// <summary>
        /// 节点权重数
        /// </summary>
        public List<int> KnotMultiplicities { get; set; } = new List<int>();
        /// <summary>
        /// 节点列表
        /// </summary>
        public List<double> Knots { get; set; } = new List<double>();
        /// <summary>
        /// 节点类型
        /// </summary>
        public KnotType KnotType { get; set; } = KnotType.UNSPECIFIED;
        /// <summary>
        /// 
        /// </summary>
        public int Dim { get => AsSuperType<BSplineCurve>().Dim; }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>() => false;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(BSplineCurve);

        public BSplineCurveWithKnots(BSplineCurve bSplineCurve)
        {
            if (bSplineCurve == null) throw new ArgumentNullException(nameof(BSplineCurve));
            SetSuperType(bSplineCurve);
        }
    }

    /// <summary>
    /// 非均匀有理B样条曲线编解码器
    /// </summary>
    public class BSplineCurveWithKnotsCodec : IExpressCodec
    {
        /// <summary>
        /// 
        /// </summary>
        public string Keyword => Schema.B_SPLINE_CURVE_WITH_KNOTS;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceEntity"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public Entity Decode(Record sourceEntity, IEXPRESSDocument document)
        {
            var record = sourceEntity as SimpleRecord ?? throw new InvalidCastException($"{nameof(sourceEntity)} is not a SimpleRecord");
            if (record.Keyword != Keyword) throw new InvalidCastException($"Target instance is not {Keyword}");
            if (record.Parameters.Count != 9) throw new InvalidCastException($"{Keyword} parameters lost");
            var name = record.Parameters[0].As<StringParameter>().Value();
            BoundedCurve bounded = new BoundedCurve(name, 3);
            BSplineCurve bSplineCurve = new BSplineCurve(bounded);
            int degree = (int)record.Parameters[1].As<RealParameter>().Value();
            bSplineCurve.Degree = degree;
            List<CartesianPoint> controls = record.Parameters[2].AsEntityList<CartesianPoint>(document);
            bSplineCurve.ControlPointsList.AddRange(controls);
            bSplineCurve.CurveForm = record.Parameters[3].AsEnumeration<BSplineCurveForm>();
            bSplineCurve.ClosedCurve = record.Parameters[4].AsLogical();
            bSplineCurve.SelfIntersect = record.Parameters[5].AsLogical();
            BSplineCurveWithKnots knots = new BSplineCurveWithKnots(bSplineCurve);
            knots.KnotMultiplicities.AddRange(record.Parameters[6].AsIntegerList());
            knots.Knots.AddRange(record.Parameters[7].AsRealList());
            knots.KnotType = record.Parameters[8].AsEnumeration<KnotType>();
            return knots;
        }
    }

    /// <summary>
    /// 节点类型
    /// </summary>
    public enum KnotType
    {
        /// <summary>
        /// 
        /// </summary>
        UNIFORM_KNOTS,
        /// <summary>
        /// 
        /// </summary>
        QUASI_UNIFORM_KNOTS,
        /// <summary>
        /// 
        /// </summary>
        PIECEWISE_BEZIER_KNOTS,
        /// <summary>
        /// 
        /// </summary>
        UNSPECIFIED
    }
}
