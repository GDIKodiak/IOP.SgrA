﻿using IOP.ISEA.EXPRESS;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.STEP.RepresentationSchema
{
    /// <summary>
    /// 双轴安置点空间选择
    /// </summary>
    public class Axis2Placement : Select
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => Schema.AXIS2_PLACEMENT;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="expresses"></param>
        /// <returns></returns>
        public override Select CreateFrom(params IExpress[] expresses)
        {
            Axis2Placement placement = new Axis2Placement();
            foreach (var exp in expresses)
            {
                if(exp == null) continue;
                else if (exp is Axis2Placement3D p3d)
                {
                    placement.AddItem(p3d);
                }
            }
            return placement;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="express"></param>
        /// <returns></returns>
        public override bool IsSelectItem(IExpress express)
        {
            return express is Axis2Placement3D;
        }
    }
}
