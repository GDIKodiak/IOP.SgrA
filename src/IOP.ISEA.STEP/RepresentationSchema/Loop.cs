﻿using IOP.ISEA.EXPRESS;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.STEP.RepresentationSchema
{
    /// <summary>
    /// 环
    /// </summary>
    public class Loop : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => Schema.LOOP;
        /// <summary>
        /// 
        /// </summary>
        public string Name { get => AsSuperType<TopologicalRepresentationItem>().Name; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public Loop(string name)
        {
            if(string.IsNullOrEmpty(name)) name = string.Empty;
            TopologicalRepresentationItem item = new TopologicalRepresentationItem(name);
            SetSuperType(item);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        public Loop(TopologicalRepresentationItem item)
        {
            if(item == null) throw new ArgumentNullException("item");
            SetSuperType(item);
        }

        public override bool IsSubType<TEntity>()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(TopologicalRepresentationItem);
    }
}
