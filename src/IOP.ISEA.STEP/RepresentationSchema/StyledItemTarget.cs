﻿using IOP.ISEA.EXPRESS;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.STEP.RepresentationSchema
{
    /// <summary>
    /// 样式项目标
    /// </summary>
    public class StyledItemTarget : Select
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public override string Keyword => Schema.STYLED_ITEM_TARGET;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="expresses"></param>
        /// <returns></returns>
        public override Select CreateFrom(params IExpress[] expresses)
        {
            StyledItemTarget target = new StyledItemTarget();
            foreach (var item in expresses)
            {
                if(item is Entity entity)
                {
                    if(entity.AsSuperTypeRecursion<GeometricRepresentationItem>(out var gri))
                    {
                        target.AddItem(gri);
                    }
                }
            }
            return target;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="express"></param>
        /// <returns></returns>
        public override bool IsSelectItem(IExpress express)
        {
            return express switch
            {
                GeometricRepresentationItem _ => true,
                _ => false
            };
        }
    }
}
