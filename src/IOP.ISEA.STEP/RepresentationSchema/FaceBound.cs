﻿using IOP.ISEA.EXPRESS;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.STEP.RepresentationSchema
{
    /// <summary>
    /// 拓扑边界
    /// </summary>
    public class FaceBound : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => Schema.FACE_BOUND;
        /// <summary>
        /// 
        /// </summary>
        public string Name { get => AsSuperType<TopologicalRepresentationItem>().Name; }
        /// <summary>
        /// 环
        /// </summary>
        public Loop Bound { get; set; }
        /// <summary>
        /// 方向
        /// </summary>
        public bool Orientation { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="loop"></param>
        /// <param name="o"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public FaceBound(string name, Loop loop, bool o)
        {
            if(string.IsNullOrEmpty(name)) name = string.Empty;
            TopologicalRepresentationItem item = new TopologicalRepresentationItem(name);
            SetSuperType(item);
            Bound = loop ?? throw new ArgumentNullException(nameof(loop));
            Orientation = o;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="loop"></param>
        /// <param name="o"></param>
        public FaceBound(TopologicalRepresentationItem item, Loop loop, bool o)
        {
            if(item == null) throw new ArgumentNullException(nameof(item));
            SetSuperType(item);
            Bound = loop ?? throw new ArgumentNullException(nameof(loop));
            Orientation = o;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>() => typeof(TEntity) == typeof(FaceOuterBound);
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(TopologicalRepresentationItem);
    }
}
