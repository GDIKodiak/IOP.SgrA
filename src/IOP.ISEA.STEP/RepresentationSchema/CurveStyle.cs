﻿using IOP.ISEA.STEP.MeasureSchema;
using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.RepresentationSchema
{
    /// <summary>
    /// 曲线样式
    /// </summary>
    public class CurveStyle : Entity
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public override string Keyword => Schema.CURVE_STYLE;
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// 曲线文字样式
        /// </summary>
        public CurveFontOrScaledCurveFontSelect CurveFont { get; private set; }
        /// <summary>
        /// 曲线宽度
        /// </summary>
        public SizeSelect CurveWidth { get; private set; }
        /// <summary>
        /// 曲线颜色
        /// </summary>
        public Colour CurveColour { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public override bool IsSubType<TEntity>()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(FoundedItem);
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="name"></param>
        public CurveStyle(string name)
        {
            if (string.IsNullOrEmpty(name)) name = string.Empty;
            Name = name;
            FoundedItem item = new FoundedItem();
            SetSuperType(item);
        }
        /// <summary>
        /// 添加曲线文字
        /// </summary>
        /// <param name="select"></param>
        public void AddCurveFont(CurveFontOrScaledCurveFontSelect select)
        {
            if (select == null) return;
            CurveFont = select;
        }
        /// <summary>
        /// 添加宽度
        /// </summary>
        /// <param name="select"></param>
        public void AddCurveWidth(SizeSelect select)
        {
            if(select == null) return;
            CurveWidth = select;
        }
        /// <summary>
        /// 添加曲线颜色
        /// </summary>
        /// <param name="colour"></param>
        public void AddCurveColour(Colour colour)
        {
            if(colour == null) return;
            CurveColour = colour;
        }
    }
    /// <summary>
    /// 曲线样式编解码器
    /// </summary>
    public class CurveStyleCodec : IExpressCodec
    {
        /// <summary>
        /// 
        /// </summary>
        public string Keyword => Schema.CURVE_STYLE;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceEntity"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public Entity Decode(Record sourceEntity, IEXPRESSDocument document)
        {
            var record = sourceEntity as SimpleRecord ?? throw new InvalidCastException($"{nameof(sourceEntity)} is not a SimpleRecord");
            if (record.Keyword != Keyword) throw new InvalidCastException($"Target instance is not {Keyword}");
            if (record.Parameters.Count != 4) throw new InvalidCastException($"{Keyword} parameters lost");
            var name = record.Parameters[0].As<StringParameter>().Value();
            CurveStyle style = new CurveStyle(name);
            if (!record.Parameters[1].IsOptional()) style.AddCurveFont(record.Parameters[1].AsSelect<CurveFontOrScaledCurveFontSelect>(document));
            if (!record.Parameters[2].IsOptional()) style.AddCurveWidth(record.Parameters[2].AsSelect<SizeSelect>(document));
            if (!record.Parameters[3].IsOptional()) style.AddCurveColour(record.Parameters[3].AsEnttiy<Colour>(document));
            return style;
        }
    }
}
