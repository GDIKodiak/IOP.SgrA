﻿using IOP.ISEA.EXPRESS;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.STEP.RepresentationSchema
{
    /// <summary>
    /// 拓扑外边界
    /// </summary>
    public class FaceOuterBound : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => Schema.FACE_BOUND;
        /// <summary>
        /// 
        /// </summary>
        public string Name { get => AsSuperType<FaceBound>().Name; }
        /// <summary>
        /// 
        /// </summary>
        public Loop Bound { get => AsSuperType<FaceBound>().Bound; }
        /// <summary>
        /// 
        /// </summary>
        public bool Orientation { get => AsSuperType<FaceBound>().Orientation; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="loop"></param>
        /// <param name="o"></param>
        public FaceOuterBound(string name, Loop loop, bool o)
        {
            if(string.IsNullOrEmpty(name)) name = string.Empty;
            if(loop == null) throw new ArgumentNullException(nameof(loop));
            FaceBound bound = new FaceBound(name, loop, o);
            SetSuperType(bound);

        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>() => false;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(FaceBound);
    }
    /// <summary>
    /// 
    /// </summary>
    public class FaceOuterBoundCodec : IExpressCodec
    {
        /// <summary>
        /// 
        /// </summary>
        public string Keyword => Schema.FACE_OUTER_BOUND;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceEntity"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Entity Decode(Record sourceEntity, IEXPRESSDocument document)
        {
            var record = sourceEntity as SimpleRecord ?? throw new InvalidCastException($"{nameof(sourceEntity)} is not a SimpleRecord");
            if (record.Keyword != Keyword) throw new InvalidCastException($"Target instance is not {Keyword}");
            if (record.Parameters.Count != 3) throw new InvalidCastException($"{Keyword} parameters lost");
            var name = record.Parameters[0].As<StringParameter>().Value();
            Loop loop = record.Parameters[1].AsEnttiy<Loop>(document);
            bool o = record.Parameters[2].AsLogical();
            FaceOuterBound bound = new FaceOuterBound(name, loop, o);
            return bound;
        }
    }
}
