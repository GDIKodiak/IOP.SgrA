﻿using IOP.ISEA.EXPRESS;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.STEP.RepresentationSchema
{
    /// <summary>
    /// 边曲线
    /// </summary>
    public class EdgeCurve : Entity
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public override string Keyword => Schema.EDGE_CURVE;
        /// <summary>
        /// 
        /// </summary>
        public string Name { get => AsSuperType<Edge>().Name; }
        /// <summary>
        /// 起始点
        /// </summary>
        public Vertex EdgeStart { get => AsSuperType<Edge>().EdgeStart; }
        /// <summary>
        /// 结束点
        /// </summary>
        public Vertex EdgeEnd { get => AsSuperType<Edge>().EdgeEnd; }
        /// <summary>
        /// 边曲线
        /// </summary>
        public Curve EdgeGeometry { get; set; }
        /// <summary>
        /// 相同曲线
        /// </summary>
        public bool SameSense { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="curve"></param>
        /// <param name="same"></param>
        public EdgeCurve(string name, Vertex start, Vertex end, Curve curve, bool same)
        {
            if(string.IsNullOrEmpty(name)) name = string.Empty;
            if (start == null) throw new ArgumentNullException(nameof(start));
            if (end == null) throw new ArgumentNullException(nameof(end));
            if(curve == null) throw new ArgumentNullException(nameof(curve));
            Edge edge = new Edge(name, start, end);
            SetSuperType(edge);
            GeometricRepresentationItem item = curve.AsSuperType<GeometricRepresentationItem>();
            SetSuperType(item);
            EdgeGeometry = curve;
            SameSense = same;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>() => false;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>()
        {
            return typeof(TEntity) == typeof(Edge)
                || typeof(TEntity) == typeof(GeometricRepresentationItem);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public class EdgeCurveCodec : IExpressCodec
    {
        /// <summary>
        /// 
        /// </summary>
        public string Keyword => Schema.EDGE_CURVE;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceEntity"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public Entity Decode(Record sourceEntity, IEXPRESSDocument document)
        {
            var record = sourceEntity as SimpleRecord ?? throw new InvalidCastException($"{nameof(sourceEntity)} is not a SimpleRecord");
            if (record.Keyword != Keyword) throw new InvalidCastException($"Target instance is not {Keyword}");
            if (record.Parameters.Count != 5) throw new InvalidCastException($"{Keyword} parameters lost");
            var name = record.Parameters[0].As<StringParameter>().Value();
            var start = record.Parameters[1].AsEnttiy<Vertex>(document);
            var end = record.Parameters[2].AsEnttiy<Vertex>(document);
            var geno = record.Parameters[3].AsEnttiy<Curve>(document);
            bool sence = record.Parameters[4].AsLogical();
            return new EdgeCurve(name, start, end, geno, sence);
        }
    }
}
