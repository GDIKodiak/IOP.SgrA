﻿using IOP.ISEA.EXPRESS;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.STEP.RepresentationSchema
{
    /// <summary>
    /// 颜色规范
    /// </summary>
    public class ColourSpecification : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => Schema.COLOUR_SPECIFICATION;
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public ColourSpecification(string name)
        {
            if(string.IsNullOrEmpty(name))name = string.Empty;
            Colour colour = new Colour();
            SetSuperType(colour);
            Name = name;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public override bool IsSubType<TEntity>()
        {
            var type = typeof(TEntity);
            return type == typeof(ColourRgb);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(Colour);
    }
}
