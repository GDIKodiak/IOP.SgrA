﻿using IOP.ISEA.EXPRESS;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.STEP.RepresentationSchema
{
    /// <summary>
    /// 呈现样式分配
    /// </summary>
    public class PresentationStyleAssignment : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => Schema.PRESENTATION_STYLE_ASSIGNMENT;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>()
        {
            return false;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>()
        {
            var type = typeof(TEntity);
            return type == typeof(FoundedItem);
        }
        /// <summary>
        /// 
        /// </summary>
        public List<PresentationStyleSelect> Styles { get; private set; } = new List<PresentationStyleSelect>();
        /// <summary>
        /// 
        /// </summary>
        public PresentationStyleAssignment(params PresentationStyleSelect[] selects)
        {
            FoundedItem foundedItem = new FoundedItem();
            SetSuperType(foundedItem);
            if(selects != null) Styles.AddRange(selects);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="selects"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public PresentationStyleAssignment(FoundedItem item, params PresentationStyleSelect[] selects)
        {
            if(item == null) throw new ArgumentNullException(nameof(item));
            if (selects == null || selects.Length < 0) throw new ArgumentNullException(nameof(selects));
            SetSuperType(item);
            Styles.AddRange(selects);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class PresentationStyleAssignmentCodec : IExpressCodec
    {
        /// <summary>
        /// 
        /// </summary>
        public string Keyword => Schema.PRESENTATION_STYLE_ASSIGNMENT;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceEntity"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public Entity Decode(Record sourceEntity, IEXPRESSDocument document)
        {
            var record = sourceEntity as SimpleRecord ?? throw new InvalidCastException($"{nameof(sourceEntity)} is not a SimpleRecord");
            if (record.Keyword != Keyword) throw new InvalidCastException($"Target instance is not {Keyword}");
            if (record.Parameters.Count != 1) throw new InvalidCastException($"{Keyword} parameters lost");
            var list = record.Parameters[0].As<ListParameter>();
            PresentationStyleAssignment assignment = new PresentationStyleAssignment();
            var l = assignment.Styles;
            foreach (var item in list.ParameterValue)
            {
                var select = item.AsSelect<PresentationStyleSelect>(document);
                l.Add(select);
            }
            return assignment;
        }
    }
}
