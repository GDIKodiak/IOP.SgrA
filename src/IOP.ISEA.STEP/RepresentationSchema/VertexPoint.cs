﻿using IOP.ISEA.EXPRESS;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.STEP.RepresentationSchema
{
    /// <summary>
    /// 拓扑点
    /// </summary>
    public class VertexPoint : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => Schema.VERTEX_POINT;
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get => AsSuperType<Vertex>().Name; }
        /// <summary>
        /// 拓扑几何点
        /// </summary>
        public Point VertexGeometry { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>() => false;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>()
        {
            return typeof(TEntity) == typeof(Vertex) || 
                typeof(TEntity) == typeof(GeometricRepresentationItem);
        }
        /// <summary>
        /// 拓扑点
        /// </summary>
        /// <param name="name"></param>
        /// <param name="point"></param>
        public VertexPoint(string name, Point point)
        {
            if(point == null) throw new ArgumentNullException(nameof(Point));
            if(string.IsNullOrEmpty(name)) name = string.Empty;
            RepresentationItem parent = new RepresentationItem(name);
            Vertex vertex = new Vertex(parent);
            GeometricRepresentationItem geometric = new GeometricRepresentationItem(parent, point.Dim);
            SetSuperType(vertex);
            SetSuperType(geometric);
            VertexGeometry = point;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class VertexPointCodec : IExpressCodec
    {
        /// <summary>
        /// 
        /// </summary>
        public string Keyword => Schema.VERTEX_POINT;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceEntity"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public Entity Decode(Record sourceEntity, IEXPRESSDocument document)
        {
            var record = sourceEntity as SimpleRecord ?? throw new InvalidCastException($"{nameof(sourceEntity)} is not a SimpleRecord");
            if (record.Keyword != Keyword) throw new InvalidCastException($"Target instance is not {Keyword}");
            if (record.Parameters.Count != 2) throw new InvalidCastException($"{Keyword} parameters lost");
            var name = record.Parameters[0].As<StringParameter>().Value();
            var point = record.Parameters[1].AsEnttiy<Point>(document);
            VertexPoint vertex = new VertexPoint(name, point);
            return vertex;
        }
    }
}
