﻿using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.RepresentationSchema
{
    /// <summary>
    /// 缩放曲线字体样式
    /// </summary>
    public class CurveStyleFontAndScaling : Entity
    {
        public override string Keyword => Schema.CURVE_STYLE_FONT_AND_SCALING;

        public override bool IsSubType<TEntity>()
        {
            throw new NotImplementedException();
        }

        public override bool IsSuperType<TEntity>()
        {
            throw new NotImplementedException();
        }
    }
}
