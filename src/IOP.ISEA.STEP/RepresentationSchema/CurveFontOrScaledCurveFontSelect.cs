﻿using System;
using System.Collections.Generic;
using System.Text;
using IOP.ISEA.EXPRESS;

namespace IOP.ISEA.STEP.RepresentationSchema
{
    /// <summary>
    /// 曲线或者缩放曲线字体选择
    /// </summary>
    public class CurveFontOrScaledCurveFontSelect : Select
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public override string Keyword => Schema.CURVE_FONT_OR_SCALED_CURVE_FONT_SELECT;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="font"></param>
        public CurveFontOrScaledCurveFontSelect(CurveStyleFontAndScaling font)
        {
            SelectList.Add(typeof(CurveStyleFontAndScaling), font);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="select"></param>
        public CurveFontOrScaledCurveFontSelect(CurveStyleFontSelect select)
        {
            SelectList.Add(typeof(CurveStyleFontSelect), select);
        }
        /// <summary>
        /// 
        /// </summary>
        public CurveFontOrScaledCurveFontSelect() { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="express"></param>
        /// <returns></returns>
        public override bool IsSelectItem(IExpress express) => IsSelectItemStatic(express);
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TExpress"></typeparam>
        /// <returns></returns>
        public static bool IsSelectItemStatic(IExpress express)
        {
            bool r = express switch
            {
                CurveStyleFontAndScaling _ => true,
                CurveStyleFontSelect _ => true,
                _ => false
            };
            return r;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="expresses"></param>
        /// <returns></returns>
        public static CurveFontOrScaledCurveFontSelect CreateFromStatic(params IExpress[] expresses)
        {
            if (expresses == null || expresses.Length <= 0) throw new ArgumentNullException(nameof(expresses));
            var select = new CurveFontOrScaledCurveFontSelect();
            foreach (var item in expresses)
            {
                if(item is CurveStyleFontAndScaling csfas)
                {
                    select.AddItem(csfas);
                }
                else
                {
                    CurveStyleFontSelect csfs = CurveStyleFontSelect.CreateFromStatic(item);
                    select.AddItem(csfs);
                }
            }
            return select;
        }
        /// <summary>
        /// 创建
        /// </summary>
        /// <param name="expresses"></param>
        /// <returns></returns>
        public override Select CreateFrom(params IExpress[] expresses)
            => CreateFromStatic(expresses);
    }
}
