﻿using IOP.ISEA.EXPRESS;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.STEP.RepresentationSchema
{
    /// <summary>
    /// B样条曲线
    /// </summary>
    public class BSplineCurve : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => Schema.B_SPLINE_CURVE;
        /// <summary>
        /// 
        /// </summary>
        public string Name { get => AsSuperType<BoundedCurve>().Name; }
        /// <summary>
        /// 
        /// </summary>
        public int Dim { get => AsSuperType<BoundedCurve>().Dim; }
        /// <summary>
        /// 基函数代数阶
        /// </summary>
        public int Degree { get; set; }
        /// <summary>
        /// 控制点列表
        /// </summary>
        public List<CartesianPoint> ControlPointsList { get; set; } = new List<CartesianPoint>();
        /// <summary>
        /// 此曲线代表的某具体曲线
        /// </summary>
        public BSplineCurveForm CurveForm { get; set; } = BSplineCurveForm.UNSPECIFIED;
        /// <summary>
        /// 是否为封闭曲线
        /// </summary>
        public bool ClosedCurve { get; set; }
        /// <summary>
        /// 曲线是否自相交
        /// </summary>
        public bool SelfIntersect { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="curve"></param>
        public BSplineCurve(BoundedCurve curve)
        {
            if(curve == null) throw new ArgumentNullException(nameof(curve));
            SetSuperType(curve);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>()
        {
            var type = typeof(TEntity);
            return type == typeof(BSplineCurveWithKnots);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(BoundedCurve);
    }
}
