﻿using IOP.ISEA.EXPRESS;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.STEP.RepresentationSchema
{
    /// <summary>
    /// 拓扑面
    /// </summary>
    public class Face : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Keyword => Schema.FACE;
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get => AsSuperType<TopologicalRepresentationItem>().Name; }
        /// <summary>
        /// 边界
        /// </summary>
        public List<FaceBound> Bounds { get; set; } = new List<FaceBound>();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="bounds"></param>
        public Face(string name, List<FaceBound> bounds)
        {
            TopologicalRepresentationItem item = new TopologicalRepresentationItem(name);
            SetSuperType(item);
            Bounds = bounds;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSubType<TEntity>()
        {
            var type = typeof(TEntity);
            return type == typeof(FaceSurface);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public override bool IsSuperType<TEntity>() => typeof(TEntity) == typeof(TopologicalRepresentationItem);
    }
}
