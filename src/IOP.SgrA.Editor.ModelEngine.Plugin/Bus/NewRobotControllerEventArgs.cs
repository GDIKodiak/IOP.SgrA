﻿using CodeWF.EventBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.ModelEngine.Plugin.Bus
{
    public class NewRobotControllerEventArgs : Command
    {
        public RobotController RobotController { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="robotController"></param>
        public NewRobotControllerEventArgs(RobotController robotController)
        {
            RobotController = robotController;
        }
    }
}
