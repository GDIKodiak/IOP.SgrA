﻿using Avalonia;
using IOP.ISEA.STL;
using IOP.ISEA.URDF;
using IOP.SgrA.Editor.VulkanEngine.Bus;
using IOP.SgrA.Kinematics;
using IOP.SgrA.SilkNet.Vulkan;
using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.ModelEngine.Plugin
{
    /// <summary>
    /// 机器人控制器
    /// </summary>
    public class RobotController
    {
        private static VectorBuilder<double> V = Vector<double>.Build;
        private RobotSystem _System;

        /// <summary>
        /// 
        /// </summary>
        public float UnitScale { get; set; } = 1000;
        /// <summary>
        /// 核心模型
        /// </summary>
        public RobotSystem Kernel 
        {
            get
            {
                return _System;
            }
            private set
            {
                if(value != null)
                {
                    _System = value;
                }
            }
        }
        /// <summary>
        /// 机器人可视化对象
        /// </summary>
        public readonly Dictionary<string, IRenderObject> Visuals = new();
        /// <summary>
        /// 是否正在执行动作
        /// </summary>
        public bool IsPlaying { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="core"></param>
        public RobotController(RobotSystem core) 
        {
            if (core == null) throw new ArgumentNullException(nameof(core));
            Kernel = core;
        }

        /// <summary>
        /// 更新指定轴的轴值
        /// </summary>
        /// <param name="jointName"></param>
        /// <param name="theta"></param>
        public void UpdateJointTheta(string jointName, double theta)
        {
            Kernel.UpdateJointTheta(jointName, theta);
            UpdateVisual();
        }
        /// <summary>
        /// 更新可视化对象
        /// </summary>
        public void UpdateVisual()
        {
            foreach (var v in Visuals)
            {
                if (Kernel.Links.TryGetValue(v.Key, out Link link))
                {
                    var trans = v.Value.GetTransform();
                    System.Numerics.Matrix4x4 transform = link.CurrentM.ToMatrix4x4();
                    var component = v.Value.GetComponent<RobotLinkComponent>(nameof(RobotLinkComponent));
                    System.Numerics.Matrix4x4 final = transform * component.LocalMatrix;
                    trans.Rotation = System.Numerics.Quaternion.CreateFromRotationMatrix(transform);
                    trans.Position = transform.Translation * UnitScale;
                }
            }
        }

        /// <summary>
        /// 计算指定轴与坐标的反向运动学结果并刷新状态
        /// </summary>
        /// <param name="jointName"></param>
        /// <param name="position"></param>
        /// <returns></returns>
        public IKResult IKinAndUpdate(string jointName, System.Numerics.Vector3 position)
        {
            Vector<double> v = V.Dense([position.X, position.Y, position.Z]);
            var r = Kernel.IKinAndUpdate(jointName, v, out _);
            if (r == IKResult.Success) UpdateVisual();
            return r;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="jointName"></param>
        /// <param name="T"></param>
        /// <returns></returns>
        public IKResult IKinAndUpdate(string jointName, System.Numerics.Matrix4x4 T)
        {
            Matrix<double> TT = T.ToMatrix();
            var r = Kernel.IKinAndUpdate(jointName, TT, out _);
            if (r == IKResult.Success) UpdateVisual();
            return r;
        }
        /// <summary>
        /// 使用当前轴值与指定结束轴值执行轴值规划
        /// </summary>
        /// <param name="jointName"></param>
        /// <param name="endTheta"></param>
        /// <param name="planningData"></param>
        /// <returns></returns>
        public PlanningResult JointTrajectoryFromCurrent(string jointName, List<double> endTheta, double Tf, double gap, out ThetaPlanningData planningData)
        {
            if (string.IsNullOrEmpty(jointName)) throw new ArgumentNullException(nameof(jointName));
            if(Kernel.Joints.TryGetValue(jointName, out Joint j))
            {
                var endLink = j.NextLink;
                List<double> initTheta = endLink.EnumerablePreviewJoints().Reverse().Select(x => x.CurrentTheta).ToList();
                return Kernel.JointTrajectory(jointName, initTheta, endTheta, Tf, gap, out planningData);
            }
            throw new InvalidOperationException($"cannot found joint from name {jointName}");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        public static async Task<RobotController> CreateControllerFromURDF(IGraphicsManager graphicsManager, URDFDocument document)
        {
            RobotSystem core = document.GenerateSystem();
            RobotController controller = new RobotController(core);
            await CreateVisuals(graphicsManager, controller, document);
            foreach(var visual in controller.Visuals)
            {
                if(controller.Kernel.Links.TryGetValue(visual.Key, out Link link))
                {
                    System.Numerics.Matrix4x4 m = link.M.ToMatrix4x4();
                    var component = visual.Value.GetComponent<RobotLinkComponent>(nameof(RobotLinkComponent));
                    System.Numerics.Matrix4x4 final = m * component.LocalMatrix;
                    var trans = visual.Value.GetTransform();
                    trans.Rotation = System.Numerics.Quaternion.CreateFromRotationMatrix(final);
                    trans.Position = final.Translation * controller.UnitScale;
                    trans.Scaling = new System.Numerics.Vector3(controller.UnitScale);
                    if(link.PreviewJoint != null)
                    {
                        var a = link.PreviewJoint.Axis;
                        component.Axis = new System.Numerics.Vector3((float)a[0], (float)a[1], (float)a[2]);
                    }
                }
            }
            return controller;
        }
        /// <summary>
        /// 创建可视化
        /// </summary>
        /// <param name="graphicsManager"></param>
        /// <param name="controller"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        private static async Task CreateVisuals(IGraphicsManager graphicsManager, RobotController controller, URDFDocument document) 
        {
            var visuals = document.GetLinksVisual();
            Dictionary<string, IRenderObject> table = new Dictionary<string, IRenderObject>();
            List<Task> tt = new List<Task>();
            foreach (var visual in visuals) 
            {
                if (visual.Value == null) continue;
                var g = visual.Value.Geometry;
                switch (g)
                {
                    case URDFMesh uMesh:
                        var path = uMesh.Filename;
                        var t = LoadMesh(visual.Value, visual.Key, path, graphicsManager, table);
                        tt.Add(t);
                        break;
                    default:
                        continue;
                }
            }
            await Task.WhenAll(tt);
            foreach (var visual in visuals)
            {
                if(table.TryGetValue(visual.Key, out IRenderObject obj))
                {
                    var mat = visual.Value.Material;
                    if (mat != null)
                    {
                        switch (mat)
                        {
                            case URDFColorMaterial color:
                                graphicsManager.CreateSimplePBRMaterial(obj, new System.Numerics.Vector3((float)color.R, (float)color.G, (float)color.B), 0.8f, 0.2f, 0, 0);
                                break;
                            default:
                                graphicsManager.CreateSimplePBRMaterial(obj, new System.Numerics.Vector3(0.56f, 0.57f, 0.58f), 0.8f, 0.2f, 0, 0);
                                break;
                        }
                    }
                    else graphicsManager.CreateSimplePBRMaterial(obj, new System.Numerics.Vector3(0.56f, 0.57f, 0.58f), 0.8f, 0.2f, 0, 0);
                    controller.Visuals.Add(visual.Key, obj);
                }
            }
        }

        private static async Task LoadMesh(URDFVisual n, string name, string path, IGraphicsManager graphicsManager, Dictionary<string, IRenderObject> table)
        {
            try
            {
                STLDocument stl = await STLDocument.LoadAsync(path);
                var data = stl.CreateAdjTriangleSequence();
                var mesh = graphicsManager.CreateIndexedMesh(graphicsManager.NewStringToken(),
                    data.Vertexes, data.Indexes, data.VertexCount, data.MinVertor, data.MinVertor);
                var c = graphicsManager.CreateUniformBuffer(64, 1, 0, 1, 0);
                var render = graphicsManager.CreateRenderObject<RenderObject>($"{graphicsManager.NewStringToken()}-MESH", mesh, c);
                render.SetBounds(mesh.MinVector, mesh.MaxVector);
                var component = new RobotLinkComponent() { LinkName = name };
                if (n.Origin != null) component.LocalMatrix = n.Origin.ToMatrix4x4();
                render.AddComponent(component);
                table.Add(name, render);
            }
            catch (Exception)
            {
                return;
            }
        }
    }
}
