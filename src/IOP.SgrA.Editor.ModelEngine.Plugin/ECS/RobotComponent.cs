﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.ModelEngine.Plugin.ECS
{
    public class RobotComponent : IRenderComponent
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name => nameof(RobotComponent);
        /// <summary>
        /// 
        /// </summary>
        public RobotController Controller { get; set; }

        public void CloneToNewRender(IRenderObject newObj)
        {
            newObj.AddComponent(new RobotComponent() { Controller = Controller });
        }

        public void Destroy()
        {
        }

        public void OnAttach(IRenderObject renderObject)
        {
        }
    }
}
