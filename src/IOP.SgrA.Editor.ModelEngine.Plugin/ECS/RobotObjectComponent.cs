﻿using IOP.SgrA.ECS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.ModelEngine.Plugin.ECS
{
    public struct RobotObjectComponent : IComponentData
    {
        public Matrix4x4 EndMatrix;
    }
}
