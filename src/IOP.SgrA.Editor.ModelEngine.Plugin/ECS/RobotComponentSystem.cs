﻿using IOP.SgrA.ECS;
using IOP.SgrA.Editor.Modules;
using IOP.SgrA.Editor.Modules.ECS;
using IOP.SgrA.Kinematics;
using MathNet.Numerics.Distributions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.ModelEngine.Plugin.ECS
{
    public class RobotComponentSystem : ComponentSystem
    {
        private IGraphicsManager GraphicsManager {  get; set; }
        private Camera EditorCamera { get; set; }

        private IRenderObject SelectedArrow = null;

        public RobotComponentSystem(IGraphicsManager graphicsManager)
        {
            GraphicsManager = graphicsManager;
            var r = graphicsManager.TryGetCamera("MainCamera", out Camera camera);
            if (!r) throw new NullReferenceException("Cannot get camera with name MainCamera");
            EditorCamera = camera;
        }

        public override void Update(TimeSpan lastStamp)
        {
            ContextManager.Foreach<RobotGroup>((g) =>
            {
                Context context = ContextManager.GetContext(g.Entity);
                var obj = context.GetContextRenderObject();
                var robot = obj.GetComponent<RobotComponent>(nameof(RobotComponent));
                var controller = robot.Controller;
                foreach(var link in obj.GetChildrens())
                {
                    if (link.TryGetComponent(nameof(RobotLinkComponent), out RobotLinkComponent com))
                    {
                        foreach (var child in link.GetChildrens())
                        {
                            if (child is TranslateRender trans)
                            {
                                var dir = Vector3.Normalize(com.Axis);
                                var right = Vector3.Normalize(Vector3.Cross(dir, Vector3.UnitY));
                                var up = Vector3.Normalize(Vector3.Cross(right, dir));
                                var rotation = new Matrix4x4(right.X, up.X, -dir.X, 0,
                                    right.Y, up.Y, -dir.Y, 0,
                                    right.Z, up.Z, -dir.Z, 0,
                                    0, 0, 0, 1);
                                Matrix4x4.Invert(rotation, out rotation);

                                var lTrans = link.GetTransform();
                                var cTrans = child.GetTransform();
                                var matrix = Matrix4x4.CreateFromQuaternion(lTrans.Rotation);
                                matrix = rotation * matrix;
                                cTrans.Rotation = Quaternion.CreateFromRotationMatrix(matrix);
                                cTrans.Position = lTrans.Position;
                                trans.Update();
                                var pointerState = Input.GetMouseButtonState(SgrA.Input.MouseButton.ButtonLeft);
                                bool s = trans.SelectedCheck(EditorCamera, Owner, Input, out IRenderObject selected);
                                if (s && SelectedArrow == null)
                                    SelectedArrow = selected;
                                else if (SelectedArrow != null && pointerState.Action == SgrA.Input.InputAction.Up)
                                    SelectedArrow = null;
                                if(SelectedArrow != null && pointerState.Action == SgrA.Input.InputAction.Press)
                                {
                                    var diff = Input.GetMousePositionDelta();
                                    if(diff.X != 0 || diff.Y != 0)
                                    {
                                        var winPos = Input.GetMousePosition();
                                        var lastPos = winPos - diff;
                                        var lastWorldPos = EditorCamera.ScreenToWorldPoint(lastPos, 0.5f);
                                        var currWorldPos = EditorCamera.ScreenToWorldPoint(winPos, 0.5f);
                                        Vector3 offsetPos = (currWorldPos - lastWorldPos);
                                        if (controller.Kernel.EndLinks.TryGetValue(com.LinkName, out Link kLink))
                                        {
                                            ArrowParameter arrowParameter = SelectedArrow.GetComponent<ArrowParameter>(nameof(ArrowParameter));
                                            var arrowAxis = arrowParameter.Axis;
                                            Vector3 diffP = new Vector3(arrowAxis.X * -offsetPos.X, arrowAxis.Y * offsetPos.Y, arrowAxis.Z * offsetPos.Z);
                                            diffP = Vector3.Transform(diffP, matrix);
                                            Matrix4x4 newM = kLink.CurrentM.ToMatrix4x4() * Matrix4x4.CreateTranslation(diffP);
                                            var r = controller.IKinAndUpdate(kLink.PreviewJoint!.Name, newM);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }
    }

    public struct RobotGroup
    {
        public Entity Entity;
        public RobotObjectComponent Obj;
    }
}
