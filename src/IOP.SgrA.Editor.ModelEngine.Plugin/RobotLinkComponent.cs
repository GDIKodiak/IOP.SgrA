﻿using IOP.SgrA.Kinematics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.ModelEngine.Plugin
{
    public class RobotLinkComponent : IRenderComponent
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name => nameof(RobotLinkComponent);
        /// <summary>
        /// 局部坐标系偏移矩阵
        /// </summary>
        public Matrix4x4 LocalMatrix { get; set; }
        /// <summary>
        /// 全局坐标系偏移矩阵
        /// </summary>
        public Matrix4x4 TransMatrix { get; set; }
        /// <summary>
        /// 轴向
        /// </summary>
        public Vector3 Axis { get; set; } = Vector3.Zero;
        /// <summary>
        /// 连杆名
        /// </summary>
        public string LinkName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newObj"></param>
        public void CloneToNewRender(IRenderObject newObj)
        {
            newObj.AddComponent(new RobotLinkComponent() 
            { 
                LinkName = LinkName, 
                LocalMatrix = LocalMatrix, 
                TransMatrix = TransMatrix, 
                Axis = Axis 
            });
        }
        /// <summary>
        /// 
        /// </summary>
        public void Destroy()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderObject"></param>
        public void OnAttach(IRenderObject renderObject)
        {
        }
    }
}
