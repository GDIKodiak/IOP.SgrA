﻿using IOP.Extension.DependencyInjection;
using IOP.Halo;
using IOP.SgrA.Editor.Plugin;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using IOP.SgrA.SilkNet.Vulkan;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using IOP.SgrA.Editor.VulkanEngine.Bus;
using Avalonia.Rendering.SceneGraph;
using OCCSharp;
using System.Diagnostics;
using DynamicData;
using CodeWF.EventBus;
using IOP.ISEA.STL;
using IOP.ISEA;
using IOP.ISEA.URDF;
using Avalonia.Threading;
using IOP.SgrA.Editor.ModelEngine.Plugin.Controls;
using Avalonia;
using IOP.Halo.EventBus;
using IOP.SgrA.Editor.ModelEngine.Plugin.Modules;
using IOP.SgrA.Editor.ModelEngine.Plugin.Bus;

namespace IOP.SgrA.Editor.ModelEngine.Plugin
{
    public class ModelEnginePlugin : IEditorPlugin
    {
        /// <summary>
        /// 
        /// </summary>
        [Autowired]
        public ILogger<ModelEnginePlugin> Logger { get; set; }
        public IGraphicsManager GraphicsManager { get; private set; }
        public IEventBus EventBus { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        protected IFrameworkContainer Container;

        protected const string MainScene = "MainWorkSpace";

        public ModelEnginePlugin(IEventBus eventBus)
        {
            EventBus = eventBus;
        }

        /// <summary>
        /// 初始化
        /// </summary>
        /// <returns></returns>
        public ValueTask Initialization()
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows) || (RuntimeInformation.IsOSPlatform(OSPlatform.Linux)))
            {
                EventBus.Subscribe<NewSceneEventArgs>(OnNewScene);
            }
            else throw new NotSupportedException("Target OSPlatform is not unsupproted");
            return ValueTask.CompletedTask;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="container"></param>
        /// <returns></returns>
        public ValueTask OnAttachedToContext(IFrameworkContainer container)
        {
            Container = container;
            GraphicsManager = Container.Services.GetRequiredService<IGraphicsManager>();
            Container.RegistFileLoadedFunc(".stl", (p) => STLFileFunc(p));
            Container.RegistFileLoadedFunc(".stp", (p) => STEPFileFunc(p));
            Container.RegistFileLoadedFunc(".step", (p) => STEPFileFunc(p));
            Container.RegistFileLoadedFunc(".urdf", (p) => URDFFileFunc(p));
            return ValueTask.CompletedTask;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceDescriptors"></param>
        public void RegistService(IServiceCollection serviceDescriptors)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private async Task STLFileFunc(string path)
        {
            if (string.IsNullOrEmpty(path)) return;
            FileInfo info = new FileInfo(path);
            if (!info.Exists || info.Extension.ToLower() != ".stl") return;
            try
            {
                STLDocument document = await STLDocument.LoadAsync(info.FullName);
                IndexesData data = document.CreateAdjTriangleSequence();
                var mesh = GraphicsManager.CreateIndexedMesh(GraphicsManager.NewStringToken(),
                    data.Vertexes, data.Indexes, data.VertexCount, data.MinVertor, data.MinVertor);
                var c = GraphicsManager.CreateUniformBuffer(64, 1, 0, 1, 0);
                var render = GraphicsManager.CreateRenderObject<RenderObject>($"{info.Name}-MESH", mesh, c);
                GraphicsManager.CreateSimplePBRMaterial(render, new Vector3(0.56f, 0.57f, 0.58f), 0.8f, 0.2f, 0, 0);
                render.SetBounds(mesh.MinVector, mesh.MaxVector);
                render.GetTransform().Scaling = new Vector3(1000);
                SceneAddEventArgs args = new(MainScene, render);
                EventBus.Publish(args);
            }
            catch (Exception e)
            {
                Logger?.LogError(e, $"{e.Message}");
            }
            return;
        }
        /// <summary>
        /// 加载URDF文件
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private async Task URDFFileFunc(string path)
        {
            if (string.IsNullOrEmpty(path)) return;
            FileInfo info = new FileInfo(path);
            if (!info.Exists || !info.Extension.Equals(".urdf", StringComparison.CurrentCultureIgnoreCase)) return;
            try
            {
                URDFDocument document = URDFDocument.ParseFromFile(info.FullName);
                var controller = await RobotController.CreateControllerFromURDF(GraphicsManager, document);
                var objs = controller.Visuals.Values.ToList();
                SceneAddManyEventArgs args = new SceneAddManyEventArgs("MainWorkSpace", objs);
                NewRobotControllerEventArgs newArgs = new NewRobotControllerEventArgs(controller);
                EventBus.Publish(newArgs);
                EventBus.Publish(args);
                Dispatcher.UIThread.Invoke(() =>
                {
                    var panel = new RobotControllerPanel();
                    panel.Controller = controller;
                    DialogWindowArgs args = new DialogWindowArgs(600, 350, panel);
                    EventBus.Publish(args);
                });
            }
            catch (Exception e)
            {
                Logger?.LogError(e, $"{e.Message}");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        private void OnNewScene(NewSceneEventArgs args)
        {
            if(args == null || args.Scene == null) return;
            var scene = args.Scene;
            if(!string.IsNullOrEmpty(scene.Name) && scene.Name == MainScene)
            {
                scene.LoadSceneModuleDynamic<RobotContollerModule>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private Task STEPFileFunc(string path)
        {
            if (string.IsNullOrEmpty(path)) return Task.CompletedTask;
            FileInfo info = new FileInfo(path);
            if (!info.Exists || !(info.Extension == ".stp" || info.Extension == ".step")) return Task.CompletedTask;
            using STEPControl_Reader reader = new STEPControl_Reader();
            try
            {
                var result = reader.ReadFile(info.FullName);
                if (result != IFSelect_ReturnStatus.IFSelect_RetDone)
                {
                    Logger?.LogError($"Read STEP File {info.Name} failed with status {result}");
                    return Task.CompletedTask;
                }
                reader.PrintCheckLoad(false, IFSelect_PrintCount.IFSelect_EntitiesByItem);
                int nbs = reader.NbRootsForTransfer();
                var msg = new Message_ProgressRange();
                int num = reader.TransferRoots(msg);
                num = reader.NbRootsForTransfer();
                for(int i = 1; i <= num; i++)
                {
                    TopoDS_Shape shape = reader.Shape(i);
                    var ex = new TopExp_Explorer(shape, TopAbs_ShapeEnum.TopAbs_FACE, TopAbs_ShapeEnum.TopAbs_SHAPE);
                    List<Vector3> buffers = new List<Vector3>();
                    List<uint> indexed = new List<uint>();
                    HashSet<Edge> edges = new HashSet<Edge>();
                    List<Triangle> triangles = new List<Triangle>();
                    PriorityQueue<VertexData, uint> queue = new PriorityQueue<VertexData, uint>();
                    Vector3 minVector = Vector3.One; Vector3 maxVector = Vector3.One;
                    while (ex.More())
                    {
                        var local = ex.Current();
                        var localFace = TopoDS.Face(local);
                        var location = local.Location();
                        BRepMesh_IncrementalMesh incrementalMesh = new BRepMesh_IncrementalMesh(localFace, 0.1, false, 0.5, false);
                        var theTriangulation = BRep_Tool.Triangulation(localFace, location, 0);
                        if (theTriangulation != null)
                        {
                            var size = theTriangulation.NbTriangles();
                            var vertexSize = theTriangulation.NbNodes();
                            int tIndex = triangles.Count;
                            int vIndex = queue.Count;
                            Triangle[] localTriangles = new Triangle[size];
                            triangles.AddRange(localTriangles);

                            if (!theTriangulation.HasNormals()) theTriangulation.ComputeNormals();
                            for (int v = 1; v <= vertexSize; v++)
                            {
                                uint vv = (uint)(vIndex + v - 1);
                                var vp = theTriangulation.Node(v);
                                var np = theTriangulation.Normal(v);
                                Vector3 p = new((float)vp.X(), (float)vp.Y(), (float)vp.Z());
                                Vector3 n = new((float)np.X(), (float)np.Y(), (float)np.Z());
                                VertexData vertex = new VertexData(vv, p, n, Vector3.Zero);
                                queue.Enqueue(vertex, vv);
                                minVector = Vector3.Min(minVector, p); maxVector = Vector3.Max(maxVector, p);
                            }
                            for (int f = 1; f <= size; f++)
                            {
                                var t = theTriangulation.Triangles().Value(f);
                                int i1 = t.Value(1); int i2 = t.Value(2); int i3 = t.Value(3);
                                uint vi1 = (uint)(i1 - 1 + vIndex);
                                uint vi2 = (uint)(i2 - 1 + vIndex);
                                uint vi3 = (uint)(i3 - 1 + vIndex);
                                Edge e0 = new Edge(vi1, vi2, f - 1 + tIndex);
                                Edge e1 = new Edge(vi2, vi3, f - 1 + tIndex);
                                Edge e2 = new Edge(vi3, vi1, f - 1 + tIndex);
                                if (edges.TryGetValue(e0, out Edge ade0))
                                {
                                    ade0.F1 = f - 1 + tIndex;
                                    e0.F1 = ade0.F0;
                                }
                                else edges.Add(e0);
                                if (edges.TryGetValue(e1, out Edge ade1))
                                {
                                    ade1.F1 = f - 1 + tIndex;
                                    e1.F1 = ade1.F0;
                                }
                                else edges.Add(e1);
                                if(edges.TryGetValue(e2, out Edge ade2))
                                {
                                    ade2.F1 = f - 1 + tIndex;
                                    e2.F1 = ade2.F0;
                                }
                                else edges.Add(e2);
                                Triangle triangle = new Triangle()
                                {
                                    I0 = vi1, I1 = vi2, I2 = vi3,
                                    E0 = e0, E1 = e1, E2 = e2,
                                    Index = f - 1 + tIndex
                                };
                                triangles[f - 1 + tIndex] = triangle;
                            }
                        }
                        ex.Next();
                    }
                    foreach(var face in triangles)
                    {
                        var e0 = face.E0;
                        indexed.Add(e0.I0);
                        if (e0.F1 >= 0)
                        {
                            var adv = triangles[e0.F1];
                            if (adv.E0.Equals(e0)) indexed.Add(adv.I2);
                            else if (adv.E1.Equals(e0)) indexed.Add(adv.I0);
                            else indexed.Add(adv.I1);
                        }
                        else indexed.Add(e0.I0);
                        indexed.Add(e0.I1);
                        var e1 = face.E1;
                        if (e1.F1 >= 0)
                        {
                            var adv = triangles[e1.F1];
                            if (adv.E0.Equals(e1)) indexed.Add(adv.I2);
                            else if (adv.E1.Equals(e1)) indexed.Add(adv.I0);
                            else indexed.Add(adv.I1);
                        }
                        else indexed.Add(e1.I0);
                        indexed.Add(e1.I1);
                        var e2 = face.E2;
                        if (e2.F1 >= 0)
                        {
                            var adv = triangles[e2.F1];
                            if (adv.E0.Equals(e2)) indexed.Add(adv.I2);
                            else if (adv.E1.Equals(e2)) indexed.Add(adv.I0);
                            else indexed.Add(adv.I1);
                        }
                        else indexed.Add(e2.I0);
                    }
                    uint vertexCount = (uint)queue.Count;
                    while (queue.TryDequeue(out VertexData v, out uint index))
                    {
                        buffers.Add(v.Position);
                        buffers.Add(v.Normal);
                        buffers.Add(v.UV);
                    }
                    var mesh = GraphicsManager.CreateIndexedMesh(GraphicsManager.NewStringToken(),
                        [.. buffers], [.. indexed], vertexCount,
                        minVector, maxVector);
                    var c = GraphicsManager.CreateUniformBuffer(64, 1, 0, 1, 0);
                    var render = GraphicsManager.CreateRenderObject<RenderObject>($"{info.Name}-MESH-{i}", mesh, c);
                    GraphicsManager.CreateSimplePBRMaterial(render, new Vector3(0.56f, 0.57f, 0.58f), 0.8f, 0.2f, 0, 0);
                    render.SetBounds(mesh.MinVector, mesh.MaxVector);
                    SceneAddEventArgs args = new("MainWorkSpace", render);
                    EventBus.Publish(args);
                }
            }
            catch (Exception e)
            {
                Logger?.LogError(e, $"{e.Message}");
            }
            return Task.CompletedTask;
        }
    }
}
