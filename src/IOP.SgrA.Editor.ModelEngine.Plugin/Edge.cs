﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.ModelEngine.Plugin
{
    public class Edge
    {
        public uint I0;
        public uint I1;

        public int F0;
        public int F1;

        public Edge(uint i0, uint i1, int f0)
        {
            I0 = i0;
            I1 = i1;
            F0 = f0;
            F1 = -1;
        }

        public override int GetHashCode() => (int)(I0 * I1);

        public override bool Equals([NotNullWhen(true)] object obj)
        {
            if(obj is not Edge edge) return false;
            return (edge.I0 == I0 && edge.I1 == I1) || (edge.I0 == I1 && edge.I1 == I0);
        }
    }
}
