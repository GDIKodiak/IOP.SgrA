﻿using CodeWF.EventBus;
using IOP.SgrA.Editor.ModelEngine.Plugin.Bus;
using IOP.SgrA.Editor.ModelEngine.Plugin.ECS;
using IOP.SgrA.Editor.Modules.Bus;
using IOP.SgrA.SilkNet.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.ModelEngine.Plugin.Modules
{
    /// <summary>
    /// 
    /// </summary>
    public class RobotContollerModule : VulkanModule, ISceneModule
    {
        /// <summary>
        /// 
        /// </summary>
        public IEventBus EventBus { get; set; }

        public RobotContollerModule(IEventBus eventBus)
        {
            EventBus = eventBus;
        }

        /// <summary>
        /// 
        /// </summary>
        public Scene Scene { get; set; }

        protected override Task Load(VulkanGraphicsManager manager)
        {
            EventBus.Subscribe<NewRobotControllerEventArgs>(OnNewRobotController);
            manager.ContextManager.CreateArchetype("Robot", new RobotObjectComponent());
            Scene.AddComponentSystem(typeof(RobotComponentSystem));
            return Task.CompletedTask;
        }

        protected override Task Unload(VulkanGraphicsManager manager)
        {
            return Task.CompletedTask;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        private void OnNewRobotController(NewRobotControllerEventArgs args)
        {
            if (args == null || args.RobotController == null) return;
            var manager = VulkanManager;
            var controller = args.RobotController;
            var name = string.IsNullOrEmpty(controller.Kernel.Name) ? manager.NewStringToken() : controller.Kernel.Name;
            var obj = manager.CreateRenderObject<RenderObject>(name, new RobotComponent() { Controller = controller });
            foreach(var item in controller.Visuals.Values)
            {
                obj.AddChildren(item);
            }
            foreach(var endLink in controller.Kernel.EndLinks)
            {
                if(controller.Visuals.TryGetValue(endLink.Key, out IRenderObject endObj))
                {
                    NewCoordinateHelperEventArgs helperEventArgs = new NewCoordinateHelperEventArgs(endObj, CoordinateHelperType.TranslateHelper);
                    EventBus.Publish(helperEventArgs);
                }
            }
            manager.CreateContext(Scene, 1, "Robot", VulkanGraphicsManager.EmptyGroup, obj);
        }
    }
}
