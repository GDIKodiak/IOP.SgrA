using Avalonia.Controls;
using Avalonia;
using ReactiveUI;
using System.Runtime.CompilerServices;
using Avalonia.Threading;
using System.Collections.ObjectModel;
using Avalonia.Data;
using System.ComponentModel;
using System.Reactive;
using System.Reflection.Metadata;
using MathNet.Numerics.LinearAlgebra;
using System.Numerics;

namespace IOP.SgrA.Editor.ModelEngine.Plugin.Controls
{
    public partial class RobotControllerPanel : UserControl
    {
        public static readonly DirectProperty<RobotControllerPanel, RobotController> ControllerProperty =
            AvaloniaProperty.RegisterDirect<RobotControllerPanel, RobotController>("Controller",
                (owner) => owner._Controller,
                UpdateController, null, BindingMode.OneWay);

        private RobotController _Controller = null;

        /// <summary>
        /// 
        /// </summary>
        public RobotController Controller 
        { 
            get => GetValue(ControllerProperty);
            set => SetValue(ControllerProperty, value);
        }


        public RobotControllerPanel()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="panel"></param>
        /// <param name="controller"></param>
        private static void UpdateController(RobotControllerPanel panel, RobotController controller)
        {
            if (panel == null || controller == null) return;
            panel._Controller = controller;
            RobotControllerVM vm = new RobotControllerVM() { RobotController = controller };
            panel.DataContext = vm;
            foreach (var joint in controller.Kernel.Joints) 
            {
                JointVM jvm = new JointVM(joint.Key, 0);
                vm.NewJoint(jvm);
            }
        }
    }

    public class RobotControllerVM : IDisposable, INotifyPropertyChanged
    {
        private RobotController _Controller;
        private double _EndX;
        private double _EndY;
        private double _EndZ;
        private string _EndJointName;
        private string _EndLinkName;

        public RobotController RobotController 
        {
            get => _Controller;
            set
            {
                if(value != null)
                {
                    if (_Controller != null) _Controller.Kernel.OnThetasChanged -= Kernel_OnThetasChanged;
                    _Controller = value;
                    _Controller.Kernel.OnThetasChanged += Kernel_OnThetasChanged;
                    UpdateEndJointAndLink(value);
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// 
        /// </summary>
        public bool UpdateFromSource { get; set; }
        public double EndX 
        {
            get => _EndX;
            set
            {
                _EndX = value;
                if (!UpdateFromSource) OnPropertyChanged();
            }
        }
        public double EndY 
        {
            get => _EndY;
            set
            {
                _EndY = value;
                if (!UpdateFromSource) OnPropertyChanged();
            }
        }
        public double EndZ 
        {
            get => _EndZ;
            set
            {
                _EndZ = value;
                if (!UpdateFromSource) OnPropertyChanged();
            }
        }

        public ReactiveCommand<Unit, Unit> IKCommand { get; set; }

        public ObservableCollection<JointVM> Joints { get; private set; } = new ObservableCollection<JointVM>();

        public RobotControllerVM()
        {
            IKCommand = ReactiveCommand.Create(IKin);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="joint"></param>
        public void NewJoint(JointVM joint)
        {
            if (joint == null) return;
            joint.PropertyChanged += Joint_PropertyChanged;
            Joints.Add(joint);
        }
        /// <summary>
        /// 
        /// </summary>
        public void IKin()
        {
            if (string.IsNullOrEmpty(_EndJointName)) return;
            Vector3 p = new Vector3((float)EndX, (float)EndY, (float)EndZ);
            RobotController.IKinAndUpdate(_EndJointName, p);
        }

        public void Dispose()
        {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        public void OnPropertyChanged([CallerMemberName]string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void UpdateEndJointAndLink(RobotController controller)
        {
            var l = controller.Kernel.Links.Values.FirstOrDefault();
            if(l != null)
            {
                var endLink = l.GetEndLink();
                _EndJointName = endLink.PreviewJoint!.Name;
                _EndLinkName = endLink.Name;
                var m = endLink.CurrentM;
                EndX = m[0, 3];
                EndY = m[1, 3];
                EndZ = m[2, 3];
            }
        }
        private void Joint_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (sender is not JointVM joint) return;
            if (e.PropertyName == "Theta")
            {
                if (!joint.UpdateFromSource)
                {
                    var name = joint.Name;
                    if (RobotController.Kernel.Joints.TryGetValue(name, out var j))
                    {
                        RobotController.UpdateJointTheta(j.Name, joint.Theta);
                        if (!string.IsNullOrEmpty(_EndLinkName) &&
                            RobotController.Kernel.Links.TryGetValue(_EndLinkName, out var link))
                        {
                            var m = link.CurrentM;
                            EndX = m[0, 3];
                            EndY = m[1, 3];
                            EndZ = m[2, 3];
                        }
                    }
                }
            }
        }
        private void Kernel_OnThetasChanged(Kinematics.RobotSystem arg1, IDictionary<string, double> arg2)
        {
            foreach(var joint in Joints)
            {
                if(arg2.TryGetValue(joint.Name, out double theta))
                {
                    joint.UpdateFromSource = true;
                    joint.Theta = theta;
                    joint.UpdateFromSource = false;
                }
            }
        }
    }

    public class JointVM : INotifyPropertyChanged
    {
        private string _Name;
        private double _Theta;

        /// <summary>
        /// 
        /// </summary>
        public string Name 
        {
            get => _Name;
            set
            {
                _Name = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public double Theta 
        {
            get => _Theta;
            set
            {
                _Theta = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool UpdateFromSource { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="theta"></param>
        public JointVM(string name, double theta)
        {
            Name = name;
            Theta = theta;
        }

        /// <summary>
        /// 
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        public void OnPropertyChanged([CallerMemberName]string propertyName = "") 
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
