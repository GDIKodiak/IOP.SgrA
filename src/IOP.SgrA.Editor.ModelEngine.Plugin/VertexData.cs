﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.ModelEngine.Plugin
{
    public struct VertexData
    {
        public uint Index;
        public Vector3 Position;
        public Vector3 Normal;
        public Vector3 UV;

        public VertexData(uint index, Vector3 p, Vector3 n, Vector3 uv)
        {
            Index = index;
            Position = p;
            Normal = n;
            UV = uv;
        }
    }
}
