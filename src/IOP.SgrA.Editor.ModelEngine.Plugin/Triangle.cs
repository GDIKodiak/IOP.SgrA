﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.ModelEngine.Plugin
{
    public struct Triangle
    {
        public int Index;
        public uint I0;
        public uint I1;
        public uint I2;
        public Edge E0;
        public Edge E1;
        public Edge E2;
    }
}
