﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    /// <summary>
    /// 
    /// </summary>
    [Generator]
    public class ScriptedRenderPassGenerator : ISourceGenerator
    {
        public void Execute(GeneratorExecutionContext context)
        {
#if DEBUG
            //Debugger.Launch();
#endif
            var compilation = context.Compilation;
            try
            {
                if (context.SyntaxReceiver != null && context.SyntaxReceiver is ScriptedSyntaxReceiver ss)
                {
                    var l = ss.Syntaxes;
                    foreach (var item in l)
                    {
                        var semanticModel = compilation.GetSemanticModel(item.SyntaxTree);
                        var classDeclarationSymbol = semanticModel.GetDeclaredSymbol(item);
                        if (classDeclarationSymbol != null)
                        {
                            var baseType = classDeclarationSymbol.BaseType;
                            if (baseType != null)
                            {
                                if (baseType.Name.Contains("ScriptedVulkanRenderPass"))
                                {
                                    GeneratorVulkanRenderPass(classDeclarationSymbol, context);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                context.ReportDiagnostic(Diagnostic.Create(Descriptors.GeneratorFailed, Location.None, e.Message));
            }
        }

        #region VulkanRenderPass
        private void GeneratorVulkanRenderPass(INamedTypeSymbol classDeclarationSymbol, 
            GeneratorExecutionContext context)
        {
            var pass = new RenderPassConfigWapper();
            List<AttachmentWrapper> attachments = new List<AttachmentWrapper>();
            List<SubpassAttachmentsWapper> subpass = new List<SubpassAttachmentsWapper>();
            List<string> dependencics = new List<string>();
            StringBuilder builder = new StringBuilder();
            var np = classDeclarationSymbol.ContainingNamespace;
            var classAttrs = classDeclarationSymbol.GetAttributes();
            foreach (var attr in classAttrs)
            {
                ParseRenderPassAttr(attr, 0, "", pass, attachments, subpass, dependencics);
            }
            var members = classDeclarationSymbol.GetMembers();
            int pIndex = 0;
            foreach (var member in members)
            {
                if (member is IPropertySymbol property)
                {
                    var memberType = property.Type;
                    if(memberType.Name == "RenderPassAttachment")
                    {
                        var attrs = property.GetAttributes();
                        foreach (var attr in attrs)
                        {
                            ParseRenderPassAttr(attr, pIndex, memberType.Name, pass, attachments, subpass, dependencics);
                        }
                        pIndex++;
                    }
                    else if(memberType.Name == "PassDependency")
                    {
                        var attrs = property.GetAttributes();
                        foreach (var attr in attrs)
                        {
                            ParseRenderPassAttr(attr, pIndex, memberType.Name, pass, attachments, subpass, dependencics);
                        }
                    }
                }
                else if (member is IFieldSymbol field)
                {
                    var fileType = field.Type;
                    if(fileType.Name == "RenderPassAttachment" && !field.IsImplicitlyDeclared)
                    {
                        var attrs = field.GetAttributes();
                        foreach (var attr in attrs)
                        {
                            ParseRenderPassAttr(attr, pIndex, fileType.Name, pass, attachments, subpass, dependencics);
                        }
                        pIndex++;
                    }
                    else if(fileType.Name == "PassDependency" && !field.IsImplicitlyDeclared)
                    {
                        var attrs = fileType.GetAttributes();
                        foreach (var attr in attrs)
                        {
                            ParseRenderPassAttr(attr, pIndex, fileType.Name, pass, attachments, subpass, dependencics);
                        }
                    }
                }
            }
            builder.AppendLine("using IOP.SgrA.SilkNet.Vulkan.Scripted;");
            builder.AppendLine("using Silk.NET.Vulkan;");
            builder.AppendLine("using System;");
            builder.AppendLine("using System.Collections.Generic;");
            builder.AppendLine("using IOP.SgrA.SilkNet.Vulkan;");
            builder.AppendLine("using IOP.SgrA;");
            if (np != null)
            {
                builder.AppendLine($"namespace {np}");
                builder.AppendLine("{");
                builder.AppendLine($"\tpublic partial class {classDeclarationSymbol.Name} : {classDeclarationSymbol.BaseType.Name}");
                builder.AppendLine("\t{");
                GeneratorOvrriveRenderPassMethod(builder, classDeclarationSymbol, pass);
                builder.AppendLine();
                GeneratorOvrriveGetRenderPassOption(builder, classDeclarationSymbol, attachments, subpass, dependencics);
                builder.AppendLine();
                GeneratorOvrriveBuildFramebufferConfig(builder, classDeclarationSymbol, attachments);
                builder.AppendLine();
                GeneratorOvrriveBuildClearValue(builder, classDeclarationSymbol, attachments);
                builder.AppendLine("\t}");
                builder.AppendLine("}");
            }
            context.AddSource($"{classDeclarationSymbol.Name}.g.cs", builder.ToString());
        }
        private void ParseRenderPassAttr(AttributeData attr, int index, string owner, RenderPassConfigWapper pass,
            List<AttachmentWrapper> attachments, List<SubpassAttachmentsWapper> subpass, List<string> dependencics)
        {
            if (attr.AttributeClass != null)
            {
                if (attr.AttributeClass.Name == "AttachmentAttribute" && owner == "RenderPassAttachment")
                {
                    BuildAttachment(attr, index, attachments);
                }
                else if (attr.AttributeClass.Name == "SubpassAttachmentAttribute" && owner == "RenderPassAttachment")
                {
                    BuildSubpassAttachment(attr, index, subpass);
                }
                else if (attr.AttributeClass.Name == "GlobalConfigAttribute" && owner == "RenderPassAttachment")
                {
                    UseGlobalConfig(attr, index, attachments);
                }
                else if (attr.AttributeClass.Name == "RenderPassAttribute")
                {
                    BuildGlobalRenderPassConfig(attr, pass);
                }
                else if(attr.AttributeClass.Name == "SubpassAttribute")
                {
                    BuildSubpass(attr, subpass);
                }
                else if(attr.AttributeClass.Name == "SubpassDependencyAttribute" && owner == "PassDependency")
                {
                    BuildSubpassDependency(attr, dependencics);
                }
                else if(attr.AttributeClass.Name == "ImageConfigAttribute" && owner == "RenderPassAttachment")
                {
                    BuildImageConfig(attr, index, attachments);
                }
                else if(attr.AttributeClass.Name == "AttachmentClearValueAttribute" && owner == "RenderPassAttachment")
                {
                    BuildClearValue(attr, index, attachments);
                }
            }
        }

        private void BuildGlobalRenderPassConfig(AttributeData attr, RenderPassConfigWapper pass)
        {
            var contr = attr.ConstructorArguments;
            if (contr.Length == 2)
            {
                var name = contr[0];
                pass.Name = name.Value.ToString();
                var mode = contr[1];
                pass.RenderPassMode = mode.Value.ToString();
            }
            foreach (var names in attr.NamedArguments)
            {
                switch (names.Key)
                {
                    case "Layers":
                        pass.Layers = names.Value.Value.ToString();
                        break;
                    case "FrameBufferCount":
                        pass.FrameBufferCount = names.Value.Value.ToString();
                        break;
                    default:
                        break;
                }
            }
        }
        private void BuildAttachment(AttributeData attr, int index, List<AttachmentWrapper> attachments)
        {
            var contr = attr.ConstructorArguments;
            AttachmentWrapper attachment = GetAttachment(index, attachments);
            if (contr.Length == 2)
            {
                var fLayout = contr[0];
                attachment.FinalLayout = $"({fLayout.Type.Name}){fLayout.Value}";
                var format = contr[1];
                attachment.Format = $"({format.Type.Name}){format.Value}";
            }
            foreach (var names in attr.NamedArguments)
            {
                switch (names.Key)
                {
                    case "InitialLayout":
                        attachment.InitialLayout = $"({names.Value.Type.Name}){names.Value.Value}";
                        break;
                    case "LoadOp":
                        attachment.LoadOp = $"({names.Value.Type.Name}){names.Value.Value}";
                        break;
                    case "StoreOp":
                        attachment.StoreOp = $"({names.Value.Type.Name}){names.Value.Value}";
                        break;
                    case "StencilLoadOp":
                        attachment.StencilLoadOp = $"({names.Value.Type.Name}){names.Value.Value}";
                        break;
                    case "StencilStoreOp":
                        attachment.StencilStoreOp = $"({names.Value.Type.Name}){names.Value.Value}";
                        break;
                    case "Samples":
                        attachment.Samples = $"({names.Value.Type.Name}){names.Value.Value}";
                        break;
                    default:
                        break;
                }
            }
        }
        private void BuildImageConfig(AttributeData attr, int index, List<AttachmentWrapper> attachments)
        {
            Dictionary<string, string> props = new Dictionary<string, string>();
            AttachmentWrapper attachment = GetAttachment(index, attachments);
            foreach (var names in attr.NamedArguments)
            {
                switch (names.Key)
                {
                    case "Usage":
                    case "Tiling":
                    case "SharingMode":
                    case "ImageType":
                    case "InitialLayout":
                    case "ViewType":
                    case "ComponentMappingR":
                    case "ComponentMappingG":
                    case "ComponentMappingB":
                    case "ComponentMappingA":
                    case "SubresourceRangeAspect":
                        props.Add(names.Key, $"({names.Value.Type.Name}){names.Value.Value}");
                        break;
                    case "MipLevels":
                    case "ArrayLayers":
                    case "SubresourceRangeBaseMipLevel":
                    case "SubresourceRangeMipLevelCount":
                    case "SubresourceRangeArrayLayer":
                    case "SubresourceRangeLayersCount":
                        props.Add(names.Key, $"{names.Value.Value}");
                        break;
                    default:
                        break;
                }
            }
            var imageConfig = attachment.ImageConfig;
            foreach(var item in props)
            {
                switch (item.Key)
                {
                    case "Usage":
                        imageConfig.Usage = item.Value;
                        break;
                    case "Tiling":
                        imageConfig.Tiling = item.Value;
                        break;
                    case "SharingMode":
                        imageConfig.SharingMode = item.Value;
                        break;
                    case "ImageType":
                        imageConfig.ImageType = item.Value;
                        break;
                    case "InitialLayout":
                        imageConfig.InitialLayout = item.Value;
                        break;
                    case "ViewType":
                        imageConfig.ViewType = item.Value;
                        break;
                    case "ComponentMappingR":
                        imageConfig.ComponentMappingR = item.Value;
                        break;
                    case "ComponentMappingG":
                        imageConfig.ComponentMappingG = item.Value;
                        break;
                    case "ComponentMappingB":
                        imageConfig.ComponentMappingB = item.Value;
                        break;
                    case "ComponentMappingA":
                        imageConfig.ComponentMappingA = item.Value;
                        break;
                    case "SubresourceRangeAspect":
                        imageConfig.SubresourceRangeAspect = item.Value;
                        break;
                    case "MipLevels":
                        imageConfig.MipLevels = item.Value;
                        break;
                    case "ArrayLayers":
                        imageConfig.ArrayLayers = item.Value;
                        break;
                    case "SubresourceRangeBaseMipLevel":
                        imageConfig.SubresourceRangeBaseMipLevel = item.Value;
                        break;
                    case "SubresourceRangeMipLevelCount":
                        imageConfig.SubresourceRangeMipLevelCount = item.Value;
                        break;
                    case "SubresourceRangeArrayLayer":
                        imageConfig.SubresourceRangeArrayLayer = item.Value;
                        break;
                    case "SubresourceRangeLayersCount":
                        imageConfig.SubresourceRangeLayersCount = item.Value;
                        break;
                    default:
                        break;
                }
            }
        }
        private void UseGlobalConfig(AttributeData attr, int index, List<AttachmentWrapper> attachments)
        {
            AttachmentWrapper attachment = GetAttachment(index, attachments);
            foreach (var names in attr.NamedArguments)
            {
                switch (names.Key)
                {
                    case "UseGlobalSample":
                        bool isUse = (bool)names.Value.Value;
                        if (isUse) attachment.Samples = "(SampleCountFlags)GlobalSampleCount";
                        break;
                    default:
                        break;
                }
            }
        }
        private void BuildSubpass(AttributeData attr, List<SubpassAttachmentsWapper> subpass)
        {
            var contr = attr.ConstructorArguments;
            if(contr != null && contr.Length == 2)
            {
                int sIndex = (int)contr[0].Value;
                var target = GetSubpass(sIndex, subpass);
                string binding = contr[1].Value.ToString();
                target.PipelineBindPoint = $"(PipelineBindPoint){binding}";
            }
        }
        private void BuildSubpassAttachment(AttributeData attr, int attachIndex, List<SubpassAttachmentsWapper> subpass)
        {
            var contr = attr.ConstructorArguments;
            if (contr != null && contr.Length == 3)
            {
                int sIndex = (int)contr[0].Value;
                var target = GetSubpass(sIndex, subpass);
                string attachmentType = contr[1].Value.ToString();
                var layout = contr[2];
                string reference = $"new AttachmentReference() {{ Attachment = {attachIndex}, Layout = ({layout.Type.Name}){layout.Value} }}";
                switch (attachmentType)
                {
                    case "0":
                        target.ColorAttachments.Add(reference);
                        break;
                    case "1":
                        target.DepthAttachment = reference;
                        break;
                    case "2":
                        target.InputAttachments.Add(reference);
                        break;
                    case "3":
                        target.ResolveAttachment = reference;
                        break;
                    default:
                        break;
                }
            }
        }
        private void BuildSubpassDependency(AttributeData attr, List<string> dependencics)
        {
            Dictionary<string, string> props = new Dictionary<string, string>();
            foreach (var names in attr.NamedArguments)
            {
                switch (names.Key)
                {
                    case "SrcSubpass":
                    case "DstSubpass":
                        props.Add(names.Key, $"\t\t\t\t\t\t{names.Key} = {names.Value.Value}");
                        break;
                    case "SrcStageMask":
                    case "DstStageMask":
                    case "SrcAccessMask":
                    case "DstAccessMask":
                    case "DependencyFlags":
                        props.Add(names.Key, $"\t\t\t\t\t\t{names.Key} = ({names.Value.Type.Name}){names.Value.Value}");
                        break;
                    default:
                        break;
                }
            }
            if (!props.ContainsKey("SrcSubpass"))
                props.Add("SrcSubpass", $"\t\t\t\t\t\tSrcSubpass = 0");
            if (!props.ContainsKey("DstSubpass"))
                props.Add("DstSubpass", $"\t\t\t\t\t\tDstSubpass = 0");
            if (!props.ContainsKey("SrcStageMask"))
                props.Add("DstStageMask", $"\t\t\t\t\t\tDstStageMask = 0");
            if (!props.ContainsKey("DstStageMask"))
                props.Add("DstStageMask", $"\t\t\t\t\t\tDstStageMask = 0");
            if (!props.ContainsKey("SrcAccessMask"))
                props.Add("SrcAccessMask", $"\t\t\t\t\t\tSrcAccessMask = 0");
            if (!props.ContainsKey("DstAccessMask"))
                props.Add("DstAccessMask", $"\t\t\t\t\t\tDstAccessMask = 0");
            if (!props.ContainsKey("DependencyFlags"))
                props.Add("DependencyFlags", $"\t\t\t\t\t\tDependencyFlags = 0");
            string result = "\t\t\t\t\tnew SubpassDependency() \r\n\t\t\t\t\t{\r\n";
            result += string.Join(",\r\n", props.Values);
            result += "\r\n\t\t\t\t\t}";
            dependencics.Add(result);
        }
        private void BuildClearValue(AttributeData attr, int attachIndex, List<AttachmentWrapper> attachments)
        {
            AttachmentWrapper attachment = GetAttachment(attachIndex, attachments);
            var clearValue = attachment.ClearValue;
            var contrs = attr.ConstructorArguments;
            if(contrs != null)
            {
                if(contrs.Length == 4)
                {
                    var f0 = contrs[0]; var f1 = contrs[1]; var f2 = contrs[2]; var f3 = contrs[3];
                    string output = $"new ClearValue() {{ Color = new ClearColorValue({f0.Value}f, {f1.Value}f, {f2.Value}f, {f3.Value}f) }}";
                    clearValue.Output = output;
                }
                else if(contrs.Length == 2)
                {
                    var depth = contrs[0];
                    var stencil = contrs[1];
                    string output = $"new ClearValue() {{ DepthStencil = new ClearDepthStencilValue({depth.Value}f, {stencil.Value}) }}";
                    clearValue.Output = output;
                }
            }
            else
            {
                clearValue.Output = "new ClearValue()";
            }
        }
        private SubpassAttachmentsWapper GetSubpass(int sIndex, List<SubpassAttachmentsWapper> subpass)
        {
            if (sIndex < 0) sIndex = 0;
            else if (sIndex >= 1024) sIndex = 1024;
            if (sIndex >= subpass.Count)
            {
                int diff = sIndex - subpass.Count;
                for (int i = 0; i <= diff; i++)
                {
                    subpass.Add(new SubpassAttachmentsWapper());
                }
            }
            return subpass[sIndex];
        }
        private AttachmentWrapper GetAttachment(int sIndex, List<AttachmentWrapper> attachments)
        {
            if (sIndex < 0) sIndex = 0;
            else if (sIndex >= 1024) sIndex = 1024;
            if (sIndex >= attachments.Count)
            {
                int diff = sIndex - attachments.Count;
                for (int i = 0; i <= diff; i++)
                {
                    attachments.Add(new AttachmentWrapper());
                }
            }
            return attachments[sIndex];
        }

        private void GeneratorOvrriveGetRenderPassOption(StringBuilder builder,
            INamedTypeSymbol classDeclarationSymbol, 
            List<AttachmentWrapper> attachments, List<SubpassAttachmentsWapper> subpasses, 
            List<string> dependencics)
        {
            var baseType = classDeclarationSymbol.BaseType;
            if (baseType == null) return;
            var m = baseType.GetMembers("BuildRenderPassOption").Where(m => m is IMethodSymbol ms && ms.IsVirtual).FirstOrDefault();
            if (m is not IMethodSymbol method) return;
            var retType = method.ReturnType;
            builder.AppendLine($"\t\tpublic override {retType.Name} {method.Name}()");
            builder.AppendLine("\t\t{");
            builder.AppendLine("\t\t\tRenderPassCreateOption option = new RenderPassCreateOption()");
            builder.AppendLine("\t\t\t{");
            builder.AppendLine("\t\t\t\tAttachments = new AttachmentDescription[]");
            builder.AppendLine("\t\t\t\t{");
            foreach(var attachment in attachments )
            {
                builder.AppendLine("\t\t\t\t\tnew AttachmentDescription()");
                builder.AppendLine("\t\t\t\t\t{");
                builder.AppendLine($"\t\t\t\t\t\tFinalLayout = {attachment.FinalLayout},");
                builder.AppendLine($"\t\t\t\t\t\tFormat = {attachment.Format},");
                builder.AppendLine($"\t\t\t\t\t\tInitialLayout = {attachment.InitialLayout},");
                builder.AppendLine($"\t\t\t\t\t\tLoadOp = {attachment.LoadOp},");
                builder.AppendLine($"\t\t\t\t\t\tStoreOp = {attachment.StoreOp},");
                builder.AppendLine($"\t\t\t\t\t\tStencilLoadOp = {attachment.StencilLoadOp},");
                builder.AppendLine($"\t\t\t\t\t\tStencilStoreOp = {attachment.StencilStoreOp},");
                builder.AppendLine($"\t\t\t\t\t\tSamples = {attachment.Samples}");
                builder.AppendLine("\t\t\t\t\t},");
            }
            builder.AppendLine("\t\t\t\t},");
            builder.AppendLine("\t\t\t\tSubpasses = new SubpassDescriptionOption[]");
            builder.AppendLine("\t\t\t\t{");
            foreach (var subPass in subpasses)
            {
                builder.AppendLine("\t\t\t\t\tnew SubpassDescriptionOption()");
                builder.AppendLine("\t\t\t\t\t{");
                if (subPass.ColorAttachments.Count > 0)
                {
                    builder.AppendLine("\t\t\t\t\t\tColorAttachments = new AttachmentReference[]");
                    builder.AppendLine("\t\t\t\t\t\t{");
                    foreach (var color in subPass.ColorAttachments)
                    {
                        builder.AppendLine($"\t\t\t\t\t\t\t{color},");
                    }
                    builder.AppendLine("\t\t\t\t\t\t},");
                }
                if (subPass.InputAttachments.Count > 0)
                {
                    builder.AppendLine("\t\t\t\t\t\tInputAttachments = new AttachmentReference[]");
                    builder.AppendLine("\t\t\t\t\t\t{");
                    foreach (var color in subPass.InputAttachments)
                    {
                        builder.AppendLine($"\t\t\t\t\t\t\t{color},");
                    }
                    builder.AppendLine("\t\t\t\t\t\t},");
                }
                if (subPass.DepthAttachment != string.Empty)
                {
                    builder.AppendLine($"\t\t\t\t\t\tDepthStencilAttachment = {subPass.DepthAttachment},");
                }
                if (subPass.ResolveAttachment != string.Empty)
                {
                    builder.AppendLine($"\t\t\t\t\t\tResolveAttachments = {subPass.ResolveAttachment},");
                }
                if (subPass.PipelineBindPoint != string.Empty)
                {
                    builder.AppendLine($"\t\t\t\t\t\tPipelineBindPoint = {subPass.PipelineBindPoint}");
                }
                builder.AppendLine("\t\t\t\t\t},");
            }
            builder.AppendLine("\t\t\t\t},");
            builder.AppendLine("\t\t\t\tDependencies = new SubpassDependency[]");
            builder.AppendLine("\t\t\t\t{");
            builder.AppendLine(string.Join(",\r\n", dependencics));
            builder.AppendLine("\t\t\t\t}");
            builder.AppendLine("\t\t\t};");
            builder.AppendLine("\t\t\treturn option;");
            builder.AppendLine("\t\t}");
        }
        private void GeneratorOvrriveBuildFramebufferConfig(StringBuilder builder,
            INamedTypeSymbol classDeclarationSymbol, List<AttachmentWrapper> attachments)
        {
            var baseType = classDeclarationSymbol.BaseType;
            if (baseType == null) return;
            var m = baseType.GetMembers("BuildFrameBufferOption").Where(m => m is IMethodSymbol ms && ms.IsVirtual).FirstOrDefault();
            if (m is not IMethodSymbol method) return;
            builder.AppendLine($"\t\tpublic override ImageAndImageViewCreateOption[] {method.Name}()");
            builder.AppendLine("\t\t{");
            builder.AppendLine("\t\t\tImageAndImageViewCreateOption[] option = new ImageAndImageViewCreateOption[]");
            builder.AppendLine("\t\t\t{");
            foreach(var item in attachments)
            {
                var config = item.ImageConfig;
                builder.AppendLine("\t\t\t\tnew ImageAndImageViewCreateOption()");
                builder.AppendLine("\t\t\t\t{");
                builder.AppendLine("\t\t\t\t\tImageCreateOption = new ImageCreateOption()");
                builder.AppendLine("\t\t\t\t\t{");
                builder.AppendLine($"\t\t\t\t\t\tImageType = {config.ImageType},");
                builder.AppendLine($"\t\t\t\t\t\tFormat = {item.Format},");
                builder.AppendLine($"\t\t\t\t\t\tMipLevels = {config.MipLevels},");
                builder.AppendLine($"\t\t\t\t\t\tArrayLayers = {config.ArrayLayers},");
                builder.AppendLine($"\t\t\t\t\t\tSamples = {item.Samples} ,");
                builder.AppendLine($"\t\t\t\t\t\tTiling = {config.Tiling},");
                builder.AppendLine($"\t\t\t\t\t\tUsage = {config.Usage},");
                builder.AppendLine($"\t\t\t\t\t\tSharingMode = {config.SharingMode},");
                builder.AppendLine($"\t\t\t\t\t\tInitialLayout = {config.InitialLayout}");
                builder.AppendLine("\t\t\t\t\t},");
                builder.AppendLine("\t\t\t\t\tImageViewCreateOption = new ImageViewCreateOption()");
                builder.AppendLine("\t\t\t\t\t{");
                builder.AppendLine($"\t\t\t\t\t\tComponents = new ComponentMapping({config.ComponentMappingR},{config.ComponentMappingG},{config.ComponentMappingB},{config.ComponentMappingA}),");
                builder.AppendLine($"\t\t\t\t\t\tFormat = {item.Format},");
                builder.AppendLine($"\t\t\t\t\t\tSubresourceRange = new ImageSubresourceRange({config.SubresourceRangeAspect}, {config.SubresourceRangeBaseMipLevel}, {config.SubresourceRangeMipLevelCount}, {config.SubresourceRangeArrayLayer}, {config.SubresourceRangeLayersCount}),");
                builder.AppendLine($"\t\t\t\t\t\tViewType = {config.ViewType}");
                builder.AppendLine("\t\t\t\t\t}");
                builder.AppendLine("\t\t\t\t},");
            }
            builder.AppendLine("\t\t\t};");
            builder.AppendLine("\t\t\treturn option;");
            builder.AppendLine("\t\t}");
        }
        private void GeneratorOvrriveBuildClearValue(StringBuilder builder, 
            INamedTypeSymbol classDeclarationSymbol, List<AttachmentWrapper> attachments)
        {
            var baseType = classDeclarationSymbol.BaseType;
            if (baseType == null) return;
            var m = baseType.GetMembers("BuildRenderPassBeginInfo").Where(m => m is IMethodSymbol ms && ms.IsVirtual).FirstOrDefault();
            if (m is not IMethodSymbol method) return;
            var arg = method.Parameters[0];
            builder.AppendLine($"\t\tpublic override RenderPassBeginOption BuildRenderPassBeginInfo({arg.Type.Name} {arg.Name})");
            builder.AppendLine("\t\t{");
            builder.AppendLine("\t\t\tvar option = new RenderPassBeginOption()");
            builder.AppendLine("\t\t\t{");
            builder.AppendLine($"\t\t\t\tRenderArea = new Rect2D {{ Extent = new Extent2D({arg.Name}.Width, {arg.Name}.Height), Offset = new Offset2D({arg.Name}.X, {arg.Name}.Y) }},");
            builder.AppendLine("\t\t\t\tClearValues = new ClearValue[]");
            builder.AppendLine("\t\t\t\t{");
            foreach(var item in attachments)
            {
                var clearValue = item.ClearValue;
                builder.AppendLine($"\t\t\t\t\t{clearValue.Output},");
            }
            builder.AppendLine("\t\t\t\t}");
            builder.AppendLine("\t\t\t};");
            builder.AppendLine("\t\t\treturn option;");
            builder.AppendLine("\t\t}");
        }
        private void GeneratorOvrriveRenderPassMethod(StringBuilder builder, INamedTypeSymbol classDeclarationSymbol,
            RenderPassConfigWapper renderPass)
        {
            var baseType = classDeclarationSymbol.BaseType;
            if (baseType == null) return;
            var m = baseType.GetMembers("Build").Where(m => m is IMethodSymbol ms && ms.IsVirtual);
            foreach(var b in m)
            {
                if (b is not IMethodSymbol method) return;
                if(method.Parameters.Length == 2)
                {
                    var mode = renderPass.RenderPassMode;
                    var retType = method.ReturnType;
                    string[] args = new string[method.Parameters.Length];
                    for (int i = 0; i < args.Length; i++)
                    {
                        var local = method.Parameters[i];
                        args[i] = $"{local.Type.Name} {local.Name}";
                    }
                    var name = renderPass.Name;
                    builder.AppendLine($"\t\tpublic override {retType.Name} {method.Name}({string.Join(",", args)})");
                    builder.AppendLine("\t\t{");
                    if (mode == "1")
                    {
                        builder.AppendLine("\t\t\tthrow new NotSupportedException(\"Target RenderPass is Swapchain RenderPass\");");
                        builder.AppendLine("\t\t}");
                        continue;
                    }
                    else
                    {
                        builder.AppendLine("\t\t\tvar option = BuildRenderPassOption();");
                        builder.AppendLine($"\t\t\tvar pass = {method.Parameters[0].Name}.CreateRenderPass(\"{name}\", option, (RenderPassMode){renderPass.RenderPassMode});");
                        builder.AppendLine("\t\t\tvar attachment = BuildFrameBufferOption();");
                        if (mode == "2")
                        {
                            builder.AppendLine($"\t\t\tpass.CreateOffScreenFrameBuffer(attachment, {method.Parameters[1].Name}.Height, {method.Parameters[1].Name}.Width, {renderPass.Layers}, {renderPass.FrameBufferCount});");
                            builder.AppendLine("\t\t\tpass.ConfigBeginInfo((option) =>");
                            builder.AppendLine("\t\t\t{");
                            builder.AppendLine($"\t\t\t\tvar info = BuildRenderPassBeginInfo({method.Parameters[1].Name});");
                            builder.AppendLine("\t\t\t\toption.RenderArea = info.RenderArea;");
                            builder.AppendLine("\t\t\t\toption.ClearValues = info.ClearValues;");
                            builder.AppendLine("\t\t\t});");
                        }
                        else
                        {
                            builder.AppendLine($"\t\t\tpass.CreateFrameBuffer(attachment, {method.Parameters[1].Name}.Height, {method.Parameters[1].Name}.Width, {renderPass.Layers});");
                            builder.AppendLine("\t\t\tpass.ConfigBeginInfo((option) =>");
                            builder.AppendLine("\t\t\t{");
                            builder.AppendLine($"\t\t\t\tvar info = BuildRenderPassBeginInfo({method.Parameters[1].Name});");
                            builder.AppendLine("\t\t\t\toption.RenderArea = info.RenderArea;");
                            builder.AppendLine("\t\t\t\toption.ClearValues = info.ClearValues;");
                            builder.AppendLine("\t\t\t});");
                        }
                        builder.AppendLine("\t\t\treturn pass;");
                        builder.AppendLine("\t\t}");
                    }
                }
                else if(method.Parameters.Length == 3)
                {
                    var mode = renderPass.RenderPassMode;
                    var retType = method.ReturnType;
                    string[] args = new string[method.Parameters.Length];
                    for (int i = 0; i < args.Length; i++)
                    {
                        var local = method.Parameters[i];
                        args[i] = $"{local.Type.Name} {local.Name}";
                    }
                    var name = renderPass.Name;
                    builder.AppendLine($"\t\tpublic override {retType.Name} {method.Name}({string.Join(",", args)})");
                    builder.AppendLine("\t\t{");
                    if(mode != "1")
                    {
                        builder.AppendLine("\t\t\tthrow new NotSupportedException(\"Target RenderPass is not Swapchain RenderPass\");");
                        builder.AppendLine("\t\t}");
                        continue;
                    }
                    else
                    {
                        builder.AppendLine("\t\t\tvar option = BuildRenderPassOption();");
                        builder.AppendLine("\t\t\tif(option.Attachments.Length <= 0) throw new NullReferenceException(\"No have RenderPassOption\");");
                        builder.AppendLine($"\t\t\toption.Attachments[0].Format = {method.Parameters[1].Name}.SwapChainFormat.Format;");
                        builder.AppendLine($"\t\t\tvar pass = {method.Parameters[0].Name}.CreateRenderPass(\"{name}\", option, (RenderPassMode){renderPass.RenderPassMode});");
                        builder.AppendLine("\t\t\tvar attachment = BuildFrameBufferOption();");
                        builder.AppendLine("\t\t\tif(attachment.Length > 0)");
                        builder.AppendLine("\t\t\t{");
                        builder.AppendLine("\t\t\t\tvar extra = new ImageAndImageViewCreateOption[attachment.Length - 1];");
                        builder.AppendLine("\t\t\t\tfor(int i = 0; i < extra.Length; i++)");
                        builder.AppendLine("\t\t\t\t{");
                        builder.AppendLine("\t\t\t\t\textra[i] = attachment[i + 1];");
                        builder.AppendLine("\t\t\t\t}");
                        builder.AppendLine($"\t\t\t\tpass.CreateSwapchainFrameBuffer({method.Parameters[1].Name}, extra, {method.Parameters[2].Name}.Height, {method.Parameters[2].Name}.Width, {renderPass.Layers});");
                        builder.AppendLine("\t\t\t\tpass.ConfigBeginInfo((option) =>");
                        builder.AppendLine("\t\t\t\t{");
                        builder.AppendLine($"\t\t\t\t\tvar info = BuildRenderPassBeginInfo({method.Parameters[2].Name});");
                        builder.AppendLine("\t\t\t\t\toption.RenderArea = info.RenderArea;");
                        builder.AppendLine("\t\t\t\t\toption.ClearValues = info.ClearValues;");
                        builder.AppendLine("\t\t\t\t});");
                        builder.AppendLine("\t\t\t}");
                        builder.AppendLine("\t\t\treturn pass;");
                        builder.AppendLine("\t\t}");
                    }
                }
            }
        }
        #endregion

        public void Initialize(GeneratorInitializationContext context)
        {
            context.RegisterForSyntaxNotifications(() => new ScriptedSyntaxReceiver());
        }
    }

    public class ScriptedSyntaxReceiver : ISyntaxReceiver
    {
        public List<ClassDeclarationSyntax> Syntaxes { get; } = new List<ClassDeclarationSyntax>();

        public void OnVisitSyntaxNode(SyntaxNode syntaxNode)
        {
            if(syntaxNode is BaseListSyntax bl)
            {
                foreach(var b in bl.Types)
                {
                    if(b is SimpleBaseTypeSyntax sbt && sbt.ToString() == "ScriptedVulkanRenderPass")
                    {
                        if(bl.Parent != null && bl.Parent is ClassDeclarationSyntax cd)
                        {
                            Syntaxes.Add(cd);
                        }
                    }
                }
            }
        }
    }
}
