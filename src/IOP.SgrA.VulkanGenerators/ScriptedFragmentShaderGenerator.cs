﻿using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    public partial class ScriptedPipelineGenerator
    {

        private bool ParseGLSLFragShader(GeneratorExecutionContext context, ITypeSymbol frag, 
            FragmentShaderWrapper fragmentShader)
        {
            bool result = true;
            string flags = "ShaderStageFlags.FragmentBit";
            var fields = fragmentShader.ShaderContext.InlineFileds;
            foreach (var attr in frag.GetAttributes())
            {
                if (attr.AttributeClass.Name == "ColorBlendAttribute")
                {
                    BuildColorBlend(context, frag, attr, fragmentShader.ColorBlend);
                }
                else if (attr.AttributeClass.Name == "GLSLExtensionAttribute")
                {
                    BuildShaderExtension(context, frag, attr, fragmentShader.ShaderContext.ShaderExtensions);
                }
                else if (attr.AttributeClass.Name == "ShaderVersionAttribute")
                {
                    BuildShaderVersion(context, frag, attr, fragmentShader.ShaderContext);
                }
            }
            foreach (var prop in frag.GetMembers().Where(x => x.Kind == SymbolKind.Property))
            {
                var p = prop as IPropertySymbol;
                foreach (var attr in p.GetAttributes())
                {
                    if (attr.AttributeClass.Name == "UniformBufferAttribute")
                    {
                        bool r = BuildShaderBuffer(context, p, attr, flags, "DescriptorType.UniformBuffer",
                            fragmentShader.DescriptorSetLayouts, fields);
                        if (!r) result = r;
                    }
                    else if(attr.AttributeClass.Name == "DynamicUniformBufferAttribute")
                    {
                        bool r = BuildShaderBuffer(context, p, attr, flags, "DescriptorType.UniformBufferDynamic", 
                            fragmentShader.DescriptorSetLayouts, fields);
                        if (!r) result = r;
                    }
                    else if (attr.AttributeClass.Name == "StorageBufferAttribute")
                    {
                        bool r = BuildShaderBuffer(context, p, attr, flags, "DescriptorType.StorageBuffer",
                            fragmentShader.DescriptorSetLayouts, fields);
                        if (!r) result = r;
                    }
                    else if (attr.AttributeClass.Name == "DynamicStorageBufferAttribute")
                    {
                        bool r = BuildShaderBuffer(context, p, attr, flags, "DescriptorType.StorageBufferDynamic",
                            fragmentShader.DescriptorSetLayouts, fields);
                        if (!r) result = r;
                    }
                    else if (attr.AttributeClass.Name == "SampledImageAttribute")
                    {
                        bool r = BuildSamplerImage(context, p, attr, flags, fragmentShader.DescriptorSetLayouts, fields);
                        if (!r) result = r;
                    }
                    else if (attr.AttributeClass.Name == "StorageImageAttribute")
                    {
                        bool r = BuildStorageImage(context, p, attr, flags, fragmentShader.DescriptorSetLayouts, fields);
                        if (!r) result = r;
                    }
                    else if (attr.AttributeClass.Name == "InputAttachmentAttribute")
                    {
                        bool r = BuildInputAttachment(context, p, attr, flags, fragmentShader.DescriptorSetLayouts, fields);
                        if (!r) result = r;
                    }
                    else if (attr.AttributeClass.Name == "PushConstantAttribute")
                    {
                        bool r = BuildPushConstant(context, p, attr, flags, fragmentShader.PushConstants, fields);
                        if (!r) result = r;
                    }
                    else if (attr.AttributeClass.Name == "FragOutputAttribute")
                    {
                        bool r = BuildFragOutput(context, p, attr, fragmentShader.ColorBlend.FragOutputs, fields);
                        if (!r) result = r;
                    }
                    else if (attr.AttributeClass.Name == "ShaderInputAttribute")
                    {
                        bool r = BuildInput(context, p, attr, flags, fields);
                        if (!r) result = r;
                    }
                    else if (attr.AttributeClass.Name == "ShaderOutputAttribute")
                    {
                        bool r = BuildOutput(context, p, attr, flags, fields);
                        if (!r) result = r;
                    }
                }
            }
            if (!ParsedFragments.ContainsKey(frag.ToString()))
            {
                ParseFragmentShader(context, frag, fragmentShader.ShaderContext);
                ParsedFragments.Add(frag.ToString(), fragmentShader);
            }
            return result;
        }

        private bool ParseFragmentShader(GeneratorExecutionContext context, ITypeSymbol vert, InlineShaderContext fragmentShader)
        {
            IMethodSymbol main = null;
            bool result = false;
            var mainFuncs = vert.GetMembers().Where(x => x.Kind == SymbolKind.Method && x.Name == "main");
            foreach (var func in mainFuncs)
            {
                if (func is not IMethodSymbol method) continue;
                if (method.Parameters.Length != 0 || method.MethodKind != MethodKind.Ordinary) continue;
                main = method;
                break;
            }
            if (main != null)
            {
                AddFragInlineField(fragmentShader.InlineFileds);
                StringBuilder shader = new StringBuilder();
                result = ParseInlineGLSLMethod(context, main, fragmentShader, shader);
                if (!result) return result;
                StringBuilder code = new StringBuilder();
                code.AppendLine($"#version {fragmentShader.ShaderVersion}");
                foreach (var ext in fragmentShader.ShaderExtensions)
                {
                    code.AppendLine($"#extension {ext}");
                }
                List<InlineShaderMethod> needParseMethod = new List<InlineShaderMethod>();
                Dictionary<string, InlineShaderMethod> methodBooks = new Dictionary<string, InlineShaderMethod>();
                result = CheckInlineMethods(context, fragmentShader, code, needParseMethod);
                if (result)
                {
                    Dictionary<string, IShaderField> books = new Dictionary<string, IShaderField>();
                    var dic = fragmentShader.InlineFileds;
                    foreach (var item in dic.Values)
                    {
                        WriteShdaerFields(books, item, code);
                    }
                    foreach (var item in fragmentShader.InlineMethods)
                    {
                        var local = item.Value;
                        WriteMethods(methodBooks, local, code);
                    }
                    StringBuilder fileBuilder = new StringBuilder();
                    fileBuilder.AppendLine("using IOP.SgrA.SilkNet.Vulkan;");
                    fileBuilder.AppendLine("using IOP.SgrA.SilkNet.Vulkan.Scripted;");
                    fileBuilder.AppendLine("using Silk.NET.Vulkan;");
                    fileBuilder.AppendLine("using IOP.SgrA.GLSL;");
                    fileBuilder.AppendLine("using IOP.SgrA;");
                    var np = vert.ContainingNamespace;
                    if (np != null)
                    {
                        fileBuilder.AppendLine($"namespace {np}");
                        fileBuilder.AppendLine("{");
                        fileBuilder.AppendLine($"\tpublic partial class {vert.Name} : {vert.BaseType.Name}");
                        fileBuilder.AppendLine("\t{");
                        fileBuilder.AppendLine("\t\tprivate string _GeneratorCode = @\"");
                        fileBuilder.Append(code.ToString());
                        fileBuilder.AppendLine("\";");
                        fileBuilder.AppendLine($"\t\tpublic override string GetCode()");
                        fileBuilder.AppendLine("\t\t{");
                        fileBuilder.AppendLine("\t\t\treturn _GeneratorCode;");
                        fileBuilder.AppendLine("\t\t}");
                        fileBuilder.AppendLine("\t}");
                        fileBuilder.AppendLine("}");
                        context.AddSource($"{vert.Name}.g.cs", fileBuilder.ToString());
                    }
                }
                else return result;
            }
            return result;
        }

        private void AddFragInlineField(Dictionary<string, IShaderField> fields)
        {
            NormalShaderField gl_FragCoord = new NormalShaderField() { Skip = true };
            fields.Add("gl_FragCoord", gl_FragCoord);
            NormalShaderField gl_FragDepth = new NormalShaderField() { Skip = true };
            fields.Add("gl_FragDepth", gl_FragDepth);
            NormalShaderField gl_PointCoord = new NormalShaderField() { Skip = true };
            fields.Add("gl_PointCoord", gl_PointCoord);
            NormalShaderField gl_FrontFacing = new NormalShaderField() { Skip = true };
            fields.Add("gl_FrontFacing", gl_FrontFacing);
            NormalShaderField gl_HelperInvocation = new NormalShaderField() { Skip = true };
            fields.Add("gl_HelperInvocation", gl_HelperInvocation);
            NormalShaderField gl_Layer = new NormalShaderField() { Skip = true };
            fields.Add("gl_Layer", gl_Layer);
            NormalShaderField gl_NumSamples = new NormalShaderField() { Skip = true };
            fields.Add("gl_NumSamples", gl_NumSamples);
            NormalShaderField gl_PrimitiveID = new NormalShaderField() { Skip = true };
            fields.Add("gl_PrimitiveID", gl_PrimitiveID);
            NormalShaderField gl_SampleID = new NormalShaderField() { Skip = true };
            fields.Add("gl_SampleID", gl_SampleID);
            NormalShaderField gl_SampleMask = new NormalShaderField() { Skip = true };
            fields.Add("gl_SampleMask", gl_SampleMask);
            NormalShaderField gl_SampleMaskIn = new NormalShaderField() { Skip = true };
            fields.Add("gl_SampleMaskIn", gl_SampleMaskIn);
            NormalShaderField gl_SamplePosition = new NormalShaderField() { Skip = true };
            fields.Add("gl_SamplePosition", gl_SamplePosition);
            NormalShaderField gl_ViewportIndex = new NormalShaderField() { Skip = true };
            fields.Add("gl_ViewportIndex", gl_ViewportIndex);
        }
    }
}
