﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    public class ImageConfigWapper
    {
        public string ImageType { get; set; } = "ImageType.Type2D";
        public string Format { get; set; } = "Format.R8G8B8A8Unorm";
        public string MipLevels { get; set; } = "1";
        public string ArrayLayers { get; set; } = "1";
        public string Samples { get; set; } = "SampleCountFlags.Count1Bit";
        public string Tiling { get; set; } = "ImageTiling.Optimal";
        public string Usage { get; set; } = "0";
        public string SharingMode { get; set; } = "SharingMode.Exclusive";
        public string InitialLayout { get; set; } = "ImageLayout.Undefined";
        public string ComponentMappingR { get; set; } = "ComponentSwizzle.R";
        public string ComponentMappingG { get; set; } = "ComponentSwizzle.G";
        public string ComponentMappingB { get; set; } = "ComponentSwizzle.B";
        public string ComponentMappingA { get; set; } = "ComponentSwizzle.A";
        public string SubresourceRangeAspect { get; set; } = "ImageAspectFlags.ColorBit";
        public string SubresourceRangeBaseMipLevel { get; set; } = "0";
        public string SubresourceRangeMipLevelCount { get; set; } = "1";
        public string SubresourceRangeArrayLayer { get; set; } = "0";
        public string SubresourceRangeLayersCount { get; set; } = "1";
        public string ViewType { get; set; } = "ImageViewType.Type2D";
    }
}
