﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.CSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Diagnostics;
using System.Reflection;
using System.Numerics;
using System.Data;
using static System.Net.WebRequestMethods;
using System.Runtime.CompilerServices;
using Microsoft.CodeAnalysis.Operations;

namespace IOP.SgrA.VulkanGenerators
{
    [Generator]
    public partial class ScriptedPipelineGenerator : ISourceGenerator
    {
        Dictionary<string, VertexShaderWrapper> ParsedVertexes = new Dictionary<string, VertexShaderWrapper>();
        Dictionary<string, FragmentShaderWrapper> ParsedFragments = new Dictionary<string, FragmentShaderWrapper>();
        Dictionary<string, GeometryShaderWraaper> ParsedGeometries = new Dictionary<string, GeometryShaderWraaper>();

        public void Execute(GeneratorExecutionContext context)
        {
#if DEBUG
            //Debugger.Launch();
#endif
            var compilation = context.Compilation;
            try
            {
                if (context.SyntaxReceiver != null && context.SyntaxReceiver is ScriptedPipelineReceiver ss)
                {
                    var l = ss.Pipes;
                    foreach (var item in l)
                    {
                        var semanticModel = compilation.GetSemanticModel(item.SyntaxTree);
                        var classDeclarationSymbol = semanticModel.GetDeclaredSymbol(item);
                        if (classDeclarationSymbol != null)
                        {
                            var baseType = classDeclarationSymbol.BaseType;
                            if (baseType != null)
                            {
                                if (baseType.ToString() == "IOP.SgrA.SilkNet.Vulkan.Scripted.ScriptedVulkanPipeline")
                                {
                                    GeneratorVulkanPipeline(classDeclarationSymbol, context);
                                }
                                else if(baseType.ToString() == "IOP.SgrA.GLSL.GLSLVert")
                                {
                                    if (!ParsedVertexes.ContainsKey(classDeclarationSymbol.ToString()))
                                    {
                                        VertexShaderWrapper vertexShader = new VertexShaderWrapper();
                                        ParseGLSLVertShader(context, classDeclarationSymbol, vertexShader);
                                    }
                                }
                                else if(baseType.ToString() == "IOP.SgrA.GLSL.GLSLFrag")
                                {
                                    if (!ParsedFragments.ContainsKey(classDeclarationSymbol.ToString()))
                                    {
                                        FragmentShaderWrapper fragmentShader = new FragmentShaderWrapper();
                                        ParseGLSLFragShader(context, classDeclarationSymbol, fragmentShader);
                                    }
                                }
                                else if(baseType.ToString() == "IOP.SgrA.GLSL.GLSLGeom")
                                {
                                    if (!ParsedGeometries.ContainsKey(classDeclarationSymbol.ToString()))
                                    {
                                        GeometryShaderWraaper geometryShader = new GeometryShaderWraaper();
                                        ParseGLSLGeomShader(context, classDeclarationSymbol, geometryShader);
                                    }
                                }
                            }
                        }
                    }

                }
            }
            catch (Exception e)
            {
                context.ReportDiagnostic(Diagnostic.Create(Descriptors.GeneratorFailed, Location.None, e.Message));
            }
        }

        public void Initialize(GeneratorInitializationContext context)
        {
            context.RegisterForSyntaxNotifications(() => new ScriptedPipelineReceiver());
        }

        private void GeneratorVulkanPipeline(INamedTypeSymbol pieline, GeneratorExecutionContext context) 
        {
            Dictionary<string, PipelineShaderStageWrapper> shaders = new Dictionary<string, PipelineShaderStageWrapper>();
            List<DescriptorSetLayoutWrapper> setLayouts = new List<DescriptorSetLayoutWrapper>();
            List<PushConstantRangeWrapper> pushConstants = new List<PushConstantRangeWrapper>();

            ShaderWrappers wrappers = new ShaderWrappers();
            VertexShaderWrapper vertexShader = new VertexShaderWrapper();
            vertexShader.PushConstants = pushConstants;
            vertexShader.DescriptorSetLayouts = setLayouts;
            FragmentShaderWrapper fragmentShader = new FragmentShaderWrapper();
            fragmentShader.DescriptorSetLayouts = setLayouts;
            fragmentShader.PushConstants = pushConstants;
            GeometryShaderWraaper geometryShader = new GeometryShaderWraaper();
            geometryShader.DescriptorSetLayouts = setLayouts;
            geometryShader.PushConstants = pushConstants;

            wrappers.VertexShaderWrapper = vertexShader;
            wrappers.FragmentShaderWrapper = fragmentShader;
            wrappers.GeometryShaderWraaper = geometryShader;


            bool r = ForeachShaders(context, pieline, shaders, wrappers);
            if (r)
            {

                StringBuilder builder = new StringBuilder();
                builder.AppendLine("using IOP.SgrA.SilkNet.Vulkan;");
                builder.AppendLine("using IOP.SgrA.SilkNet.Vulkan.Scripted;");
                builder.AppendLine("using Silk.NET.Vulkan;");
                builder.AppendLine("using IOP.SgrA.GLSL;");
                builder.AppendLine("using IOP.SgrA;");
                builder.AppendLine("using System;");
                builder.AppendLine("using System.Collections.Generic;");
                builder.AppendLine("using System.Linq;");

                var np = pieline.ContainingNamespace;
                if (np != null)
                {
                    builder.AppendLine($"namespace {np}");
                    builder.AppendLine("{");
                    builder.AppendLine($"\tpublic partial class {pieline.Name} : {pieline.BaseType.Name}");
                    builder.AppendLine("\t{");
                    BuildOverrideBuildPipelineDescriptorSetLayout(pieline, builder, setLayouts);
                    builder.AppendLine();
                    BuildOverrideBuildPipelineLayoutOption(pieline, builder, pushConstants);
                    builder.AppendLine();
                    BuildOverrideCreateVertexInputState(pieline, builder, vertexShader.VertexInputs);
                    builder.AppendLine();
                    BuildOverridePipelineColorBlendState(pieline, builder, fragmentShader.ColorBlend);
                    builder.AppendLine();
                    BuildOverridePipelineOption(pieline, builder, shaders);
                    builder.AppendLine();
                    BuildOverrideCompileScriptedShaders(pieline, builder, wrappers.Shaders);
                    builder.AppendLine("\t}");
                    builder.AppendLine("}");
                }

                context.AddSource($"{pieline.Name}.g.cs", builder.ToString());
            }
        }

        private bool ForeachShaders(GeneratorExecutionContext context, INamedTypeSymbol pieline, 
            Dictionary<string, PipelineShaderStageWrapper> dic, ShaderWrappers wrappers)
        {
            bool result = true;
            foreach (var item in pieline.GetMembers().Where(x => x.Kind == SymbolKind.Property))
            {
                if (!result) break;
                if (item is IPropertySymbol prop)
                {
                    var type = prop.Type;
                    if (type.BaseType != null)
                    {
                        var bName = type.BaseType.Name;
                        switch (bName)
                        {
                            case "GLSLVert":
                                if (dic.ContainsKey(bName))
                                {
                                    var location = prop.Locations.FirstOrDefault() ?? Location.None;
                                    context.ReportDiagnostic(Diagnostic.Create(Descriptors.ShaderWasAlreadyExist, location, "VertexBit"));
                                    continue;
                                }
                                else
                                {
                                    if(ParsedVertexes.TryGetValue(type.ToString(), out VertexShaderWrapper wrapper))
                                    {
                                        var old = wrappers.VertexShaderWrapper;
                                        wrapper.DescriptorSetLayouts = old.DescriptorSetLayouts;
                                        wrapper.PushConstants = old.PushConstants;
                                        wrapper.VertexInputs = old.VertexInputs;
                                        wrappers.VertexShaderWrapper = wrapper;
                                    }
                                    result = ParseGLSLVertShader(context, type, wrappers.VertexShaderWrapper);
                                    dic.Add(bName, new PipelineShaderStageWrapper()
                                    {
                                        Name = "main",
                                        Stage = "ShaderStageFlags.VertexBit"
                                    });
                                    wrappers.Shaders.Add(prop);
                                }
                                break;
                            case "GLSLFrag":
                                if (dic.ContainsKey(bName))
                                {
                                    var location = prop.Locations.FirstOrDefault() ?? Location.None;
                                    context.ReportDiagnostic(Diagnostic.Create(Descriptors.ShaderWasAlreadyExist, location, "FragmentBit"));
                                    continue;
                                }
                                else
                                {
                                    if(ParsedFragments.TryGetValue(type.ToString(), out FragmentShaderWrapper wrapper))
                                    {
                                        var old = wrappers.FragmentShaderWrapper;
                                        wrapper.DescriptorSetLayouts = old.DescriptorSetLayouts;
                                        wrapper.PushConstants = old.PushConstants;
                                        wrapper.ColorBlend = old.ColorBlend;
                                        wrappers.FragmentShaderWrapper = wrapper;
                                    }
                                    result = ParseGLSLFragShader(context, type, wrappers.FragmentShaderWrapper);
                                    dic.Add(bName, new PipelineShaderStageWrapper()
                                    {
                                        Name = "main",
                                        Stage = "ShaderStageFlags.FragmentBit"
                                    });
                                    wrappers.Shaders.Add(prop);
                                }
                                break;
                            case "GLSLGeom":
                                if (dic.ContainsKey(bName))
                                {
                                    var location = prop.Locations.FirstOrDefault() ?? Location.None;
                                    context.ReportDiagnostic(Diagnostic.Create(Descriptors.ShaderWasAlreadyExist, location, "FragmentBit"));
                                    continue;
                                }
                                else
                                {
                                    if(ParsedGeometries.TryGetValue(type.ToString(), out GeometryShaderWraaper wrapper))
                                    {
                                        var old = wrappers.GeometryShaderWraaper;
                                        wrapper.DescriptorSetLayouts = old.DescriptorSetLayouts;
                                        wrapper.PushConstants = old.PushConstants;
                                        wrapper.VertexInput = old.VertexInput;
                                        wrapper.VertexOutput = old.VertexOutput;
                                        wrappers.GeometryShaderWraaper = wrapper;
                                    }
                                    result = ParseGLSLGeomShader(context, type, wrappers.GeometryShaderWraaper);
                                    dic.Add(bName, new PipelineShaderStageWrapper()
                                    {
                                        Name = "main",
                                        Stage = "ShaderStageFlags.GeometryBit"
                                    });
                                    wrappers.Shaders.Add(prop);
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            return result;
        }

        #region override methods
        private void BuildOverrideBuildPipelineDescriptorSetLayout(INamedTypeSymbol pieline, StringBuilder builder, List<DescriptorSetLayoutWrapper> setLayouts)
        {
            var baseType = pieline.BaseType;
            if (baseType == null) return;
            var m = baseType.GetMembers("BuildPipelineDescriptorSetLayoutOptions").Where(m => m is IMethodSymbol ms && ms.IsVirtual).FirstOrDefault();
            if (m is not IMethodSymbol method) return;
            var retType = method.ReturnType;
            builder.AppendLine($"\t\tpublic override {retType} {method.Name}()");
            builder.AppendLine("\t\t{");
            builder.AppendLine("\t\t\tDescriptorSetLayoutCreateOption[] options = new DescriptorSetLayoutCreateOption[]");
            builder.AppendLine("\t\t\t{");
            foreach(var item in setLayouts)
            {
                builder.AppendLine("\t\t\t\tnew DescriptorSetLayoutCreateOption()");
                builder.AppendLine("\t\t\t\t{");
                builder.AppendLine("\t\t\t\t\tBindings = new DescriptorSetLayoutBindingOption[]");
                builder.AppendLine("\t\t\t\t\t{");
                foreach(var binding in item.Bindings)
                {
                    builder.AppendLine("\t\t\t\t\t\tnew DescriptorSetLayoutBindingOption()");
                    builder.AppendLine("\t\t\t\t\t\t{");
                    builder.AppendLine($"\t\t\t\t\t\t\tBinding = {binding.Binding},");
                    builder.AppendLine($"\t\t\t\t\t\t\tDescriptorCount = {binding.DescriptorCount},");
                    builder.AppendLine($"\t\t\t\t\t\t\tDescriptorType = {binding.DescriptorType},");
                    builder.AppendLine($"\t\t\t\t\t\t\tStageFlags = {binding.StageFlags}");
                    builder.AppendLine("\t\t\t\t\t\t},");
                }
                builder.AppendLine("\t\t\t\t\t},");
                builder.AppendLine($"\t\t\t\t\tDescriptorSetTarget = (DescriptorSetTarget){item.DescriptorSetTarget},");
                builder.AppendLine($"\t\t\t\t\tSetIndex = {item.SetIndex},");
                builder.AppendLine("\t\t\t\t},");
            }
            builder.AppendLine("\t\t\t};");
            builder.AppendLine("\t\t\treturn options;");
            builder.AppendLine("\t\t}");
        }
        private void BuildOverrideBuildPipelineLayoutOption(INamedTypeSymbol pieline, StringBuilder builder, List<PushConstantRangeWrapper> pushConstants)
        {
            var baseType = pieline.BaseType;
            if (baseType == null) return;
            var m = baseType.GetMembers("BuildPipelineLayoutOption").Where(m => m is IMethodSymbol ms && ms.IsVirtual).FirstOrDefault();
            if (m is not IMethodSymbol method) return;
            var retType = method.ReturnType;
            builder.AppendLine($"\t\tpublic override {retType} {method.Name}()");
            builder.AppendLine("\t\t{");
            builder.AppendLine("\t\t\tPipelineLayoutCreateOption option = new PipelineLayoutCreateOption()");
            builder.AppendLine("\t\t\t{");
            builder.AppendLine("\t\t\t\tPushConstantRanges = new PushConstantRange[]");
            builder.AppendLine("\t\t\t\t{");
            foreach(var item in pushConstants)
            {
                builder.AppendLine("\t\t\t\t\tnew PushConstantRange()");
                builder.AppendLine("\t\t\t\t\t{");
                builder.AppendLine($"\t\t\t\t\t\tStageFlags = {item.StageFlags},");
                builder.AppendLine($"\t\t\t\t\t\tSize = {item.Size},");
                builder.AppendLine($"\t\t\t\t\t\tOffset = {item.Offset}");
                builder.AppendLine("\t\t\t\t\t},");
            }
            builder.AppendLine("\t\t\t\t},");
            builder.AppendLine("\t\t\t};");
            builder.AppendLine("\t\t\treturn option;");
            builder.AppendLine("\t\t}");
        }
        private void BuildOverrideCreateVertexInputState(INamedTypeSymbol pieline, StringBuilder builder, List<VertexInputBindingWrapper> vertices)
        {
            if (vertices.Count <= 0) return;
            var baseType = pieline.BaseType;
            if (baseType == null) return;
            var m = baseType.GetMembers("CreateVertexInputState").Where(m => m is IMethodSymbol ms && ms.IsVirtual).FirstOrDefault();
            if (m is not IMethodSymbol method) return;
            var retType = method.ReturnType;
            List<VertexAttributeWrapper> attrs = new List<VertexAttributeWrapper>();
            builder.AppendLine($"\t\tpublic override {retType} {method.Name}()");
            builder.AppendLine("\t\t{");
            builder.AppendLine("\t\t\treturn new PipelineVertexInputStateCreateOption()");
            builder.AppendLine("\t\t\t{");
            builder.AppendLine("\t\t\t\tVertexBindingDescriptions = new VertexInputBindingDescription[]");
            builder.AppendLine("\t\t\t\t{");
            foreach (var v in vertices)
            {
                uint stride = v.Stride;
                var inputs = v.InputAttributes;
                for(int i = 0; i < inputs.Count; i++)
                {
                    var local = inputs[i];
                    if(i == inputs.Count - 1 && stride <= 0)
                    {
                        stride = local.Size + local.Offset;
                    }
                    attrs.Add(local);
                }
                builder.AppendLine("\t\t\t\t\tnew VertexInputBindingDescription()");
                builder.AppendLine("\t\t\t\t\t{");
                builder.AppendLine($"\t\t\t\t\t\tBinding = {v.Binding},");
                builder.AppendLine($"\t\t\t\t\t\tStride = {stride},");
                builder.AppendLine($"\t\t\t\t\t\tInputRate = (VertexInputRate){v.InputRate}");
                builder.AppendLine("\t\t\t\t\t},");
            }
            builder.AppendLine("\t\t\t\t},");
            builder.AppendLine("\t\t\t\tVertexAttributeDescriptions = new VertexInputAttributeDescription[]");
            builder.AppendLine("\t\t\t\t{");
            foreach(var attr in attrs)
            {
                builder.AppendLine("\t\t\t\t\tnew VertexInputAttributeDescription()");
                builder.AppendLine("\t\t\t\t\t{");
                builder.AppendLine($"\t\t\t\t\t\tLocation = {attr.Location},");
                builder.AppendLine($"\t\t\t\t\t\tBinding = {attr.Binding},");
                builder.AppendLine($"\t\t\t\t\t\tFormat = (Format){attr.Format},");
                builder.AppendLine($"\t\t\t\t\t\tOffset = {attr.Offset},");
                builder.AppendLine("\t\t\t\t\t},");
            }
            builder.AppendLine("\t\t\t\t}");
            builder.AppendLine("\t\t\t};");
            builder.AppendLine("\t\t}");
        }
        private void BuildOverridePipelineColorBlendState(INamedTypeSymbol pieline, StringBuilder builder, ColorBlendWrapper colorBlend)
        {
            var baseType = pieline.BaseType;
            if (baseType == null) return;
            var m = baseType.GetMembers("CreateColorBlendState").Where(m => m is IMethodSymbol ms && ms.IsVirtual).FirstOrDefault();
            if (m is not IMethodSymbol method) return;
            var retType = method.ReturnType;
            builder.AppendLine($"\t\tpublic override {retType} {method.Name}()");
            builder.AppendLine("\t\t{");
            builder.AppendLine("\t\t\treturn new PipelineColorBlendStateCreateOption()");
            builder.AppendLine("\t\t\t{");
            builder.AppendLine("\t\t\t\tAttachments = new PipelineColorBlendAttachmentState[]");
            builder.AppendLine("\t\t\t\t{");
            foreach(var item in colorBlend.FragOutputs)
            {
                string enable = item.BlendEnable ? "true" : "false";
                builder.AppendLine("\t\t\t\t\tnew PipelineColorBlendAttachmentState()");
                builder.AppendLine("\t\t\t\t\t{");
                builder.AppendLine($"\t\t\t\t\t\tColorWriteMask = {item.ColorWriteMask},");
                builder.AppendLine($"\t\t\t\t\t\tBlendEnable = {enable},");
                builder.AppendLine($"\t\t\t\t\t\tAlphaBlendOp = {item.AlphaBlendOp},");
                builder.AppendLine($"\t\t\t\t\t\tColorBlendOp = {item.ColorBlendOp},");
                builder.AppendLine($"\t\t\t\t\t\tSrcColorBlendFactor = {item.SrcColorBlendFactor},");
                builder.AppendLine($"\t\t\t\t\t\tDstColorBlendFactor = {item.DstColorBlendFactor},");
                builder.AppendLine($"\t\t\t\t\t\tSrcAlphaBlendFactor = {item.SrcAlphaBlendFactor},");
                builder.AppendLine($"\t\t\t\t\t\tDstAlphaBlendFactor = {item.DstAlphaBlendFactor},");
                builder.AppendLine("\t\t\t\t\t},");
            }
            builder.AppendLine("\t\t\t\t},");
            builder.AppendLine($"\t\t\t\tLogicOpEnable = {colorBlend.LogicOpEnable},");
            builder.AppendLine($"\t\t\t\tLogicOp = {colorBlend.LogicOp},");
            builder.AppendLine($"\t\t\t\tBlendConstants = {colorBlend.BlendConstants}");
            builder.AppendLine("\t\t\t};");
            builder.AppendLine("\t\t}");
        }
        private void BuildOverridePipelineOption(INamedTypeSymbol pipeline, StringBuilder builder, Dictionary<string, PipelineShaderStageWrapper> dic)
        {
            var baseType = pipeline.BaseType;
            if (baseType == null) return;
            var m = baseType.GetMembers("BuildPipelineOption").Where(m => m is IMethodSymbol ms && ms.IsVirtual).FirstOrDefault();
            if (m is not IMethodSymbol method) return;
            var retType = method.ReturnType;
            string[] args = new string[method.Parameters.Length];
            for (int i = 0; i < args.Length; i++)
            {
                var local = method.Parameters[i];
                args[i] = $"{local.Type.Name} {local.Name}";
            }
            builder.AppendLine($"\t\tpublic override {retType} {method.Name}({string.Join(",", args)})");
            builder.AppendLine("\t\t{");
            builder.AppendLine("\t\t\tGraphicsPipelineCreateOption option = new GraphicsPipelineCreateOption()");
            builder.AppendLine("\t\t\t{");
            builder.AppendLine("\t\t\t\tStages = new PipelineShaderStageCreateOption[]");
            builder.AppendLine("\t\t\t\t{");
            foreach(var value in dic.Values)
            {
                builder.AppendLine("\t\t\t\t\tnew PipelineShaderStageCreateOption() { Name = \"main\" },");
            }
            builder.AppendLine("\t\t\t\t},");
            builder.AppendLine("\t\t\t\tVertexInputState = CreateVertexInputState(),");
            builder.AppendLine("\t\t\t\tInputAssemblyState = CreateInputAssemblyState(),");
            builder.AppendLine("\t\t\t\tViewportState = new PipelineViewportStateCreateOption()");
            builder.AppendLine("\t\t\t\t{");
            string area = method.Parameters[0].Name;
            builder.AppendLine("\t\t\t\t\tViewports = new Viewport[]");
            builder.AppendLine("\t\t\t\t\t{");
            builder.AppendLine($"\t\t\t\t\t\tnew Viewport({area}.X, {area}.Y, {area}.Width, {area}.Height, 0, 1.0f)");
            builder.AppendLine("\t\t\t\t\t},");
            builder.AppendLine("\t\t\t\t\tScissors = new Rect2D[]");
            builder.AppendLine("\t\t\t\t\t{");
            builder.AppendLine($"\t\t\t\t\t\tnew Rect2D(new Offset2D({area}.X, {area}.Y), new Extent2D({area}.Width, {area}.Height))");
            builder.AppendLine("\t\t\t\t\t},");
            builder.AppendLine("\t\t\t\t},");
            builder.AppendLine("\t\t\t\tRasterizationState = CreateRasterizationState(),");
            builder.AppendLine("\t\t\t\tColorBlendState = CreateColorBlendState(),");
            builder.AppendLine("\t\t\t\tDepthStencilState = CreateDepthStencilState(),");
            builder.AppendLine("\t\t\t\tMultisampleState = CreateMultisampleState(),");
            builder.AppendLine("\t\t\t\tDynamicState = CreateDynamicState(),");
            builder.AppendLine("\t\t\t\tSubpass = Subpass");
            builder.AppendLine("\t\t\t};");
            builder.AppendLine("\t\t\treturn option;");
            builder.AppendLine("\t\t}");
        }
        private void BuildOverrideCompileScriptedShaders(INamedTypeSymbol pipeline, StringBuilder builder, List<IPropertySymbol> shaders)
        {
            var baseType = pipeline.BaseType;
            if (baseType == null) return;
            var m = baseType.GetMembers("CompileScriptedShaders").Where(m => m is IMethodSymbol ms && ms.IsVirtual).FirstOrDefault();
            if (m is not IMethodSymbol method) return;
            builder.AppendLine($"\t\tpublic override VulkanGLSLShaderInfo[] {method.Name}()");
            builder.AppendLine("\t\t{");
            builder.AppendLine($"\t\t\tVulkanGLSLShaderInfo[] result = new VulkanGLSLShaderInfo[{shaders.Count}];");
            for(int i = 0; i < shaders.Count; i++)
            {
                builder.AppendLine($"\t\t\tVulkanGLSLShaderInfo info{i} = CompileShader({shaders[i].Name} ?? new {shaders[i].Type.Name}());");
                builder.AppendLine($"\t\t\tresult[{i}] = info{i};");
            }
            builder.AppendLine("\t\t\treturn result;");
            builder.AppendLine("\t\t}");
        }
        #endregion

        #region AttributeBuilders
        private bool BuildShaderBuffer(GeneratorExecutionContext context, IPropertySymbol property,
            AttributeData data, string stage, string bufferType, 
            List<DescriptorSetLayoutWrapper> setLayouts, Dictionary<string, IShaderField> fields)
        {
            bool result = false;
            var type = property.Type;
            if (type.TypeKind == TypeKind.Struct && CheckStruct(context, type))
            {
                if(IsImage(type) || IsSamplerImage(type)) 
                {
                    context.ReportDiagnostic(Diagnostic.Create(Descriptors.UniformBufferCannotBeImage, GetAttributeLocation(data)));
                    result = false;
                    return result;
                }
                if (data.ConstructorArguments.Length == 3)
                {
                    uint set = (uint)data.ConstructorArguments[0].Value;
                    uint binding = (uint)data.ConstructorArguments[1].Value;
                    string owner = data.ConstructorArguments[2].Value.ToString();
                    var layout = GetDescriptorSetLayout(setLayouts, (int)set, out bool isNew);
                    if (!isNew && layout.DescriptorSetTarget != owner)
                    {
                        var location = GetAttributeLocation(data);
                        context.ReportDiagnostic(Diagnostic.Create(Descriptors.DifferentDescriptorSetTarget, location, set));
                    }
                    else
                    {
                        var layoutBinding = GetDescriptorSetLayoutBinding(layout.Bindings, (int)binding, out bool isBindingNotUsed);
                        if (!isBindingNotUsed)
                        {
                            if(!layoutBinding.StageFlags.Contains(stage))
                            {
                                if (string.IsNullOrEmpty(layoutBinding.StageFlags))
                                {
                                    layoutBinding.DescriptorType = bufferType;
                                    layoutBinding.StageFlags = stage;
                                }
                                else layoutBinding.StageFlags += $" | {stage}";
                                if (bufferType == "DescriptorType.UniformBuffer" || bufferType == "DescriptorType.UniformBufferDynamic")
                                {
                                    result = AddUniformBufferShaderField(context, property, layout.SetIndex, layoutBinding, fields);
                                }
                                else if (bufferType == "DescriptorType.StorageBuffer" || bufferType == "DescriptorType.StorageBufferDynamic")
                                {
                                    result = AddStorageBufferShaderField(context, property, layout.SetIndex, layoutBinding, fields);
                                }
                            }
                            else
                            {
                                var location = GetAttributeLocation(data);
                                context.ReportDiagnostic(Diagnostic.Create(Descriptors.DescriptorAttachmentIsUsed, location, set, binding));
                            }
                        }
                        else
                        {
                            layout.SetIndex = set.ToString();
                            layout.DescriptorSetTarget = owner;
                            layoutBinding.DescriptorType = bufferType;
                            layoutBinding.Binding = binding.ToString();
                            layoutBinding.StageFlags = stage;
                            foreach (var args in data.NamedArguments)
                            {
                                if (args.Key == "Layout")
                                {
                                    layoutBinding.DataLayout = GetUniformDataLayoutEnumValue(args.Value.Value.ToString());
                                }
                            }
                            if(bufferType == "DescriptorType.UniformBuffer" || bufferType == "DescriptorType.UniformBufferDynamic")
                            {
                                result = AddUniformBufferShaderField(context, property, layout.SetIndex, layoutBinding, fields);
                            }
                            else if(bufferType == "DescriptorType.StorageBuffer" || bufferType == "DescriptorType.StorageBufferDynamic")
                            {
                                result = AddStorageBufferShaderField(context, property, layout.SetIndex, layoutBinding, fields);
                            }
                        }
                    }
                }
            }
            else
            {
                var location = property.Locations.FirstOrDefault() ?? Location.None;
                context.ReportDiagnostic(Diagnostic.Create(Descriptors.NotGLSLDataType, location, property.Name));
                return result;
            }
            return result;
        }
        private bool BuildSamplerImage(GeneratorExecutionContext context, IPropertySymbol property,
            AttributeData data, string stage, List<DescriptorSetLayoutWrapper> setLayouts, Dictionary<string, IShaderField> fields)
        {
            bool result = false;
            var type = property.Type;
            if (type.TypeKind == TypeKind.Struct && IsSamplerImage(type))
            {
                if (data.ConstructorArguments.Length == 3)
                {
                    uint set = (uint)data.ConstructorArguments[0].Value;
                    uint binding = (uint)data.ConstructorArguments[1].Value;
                    string owner = data.ConstructorArguments[2].Value.ToString();
                    var layout = GetDescriptorSetLayout(setLayouts, (int)set, out bool isNew);
                    if (!isNew && layout.DescriptorSetTarget != owner)
                    {
                        var location = GetAttributeLocation(data);
                        context.ReportDiagnostic(Diagnostic.Create(Descriptors.DifferentDescriptorSetTarget, location, set));
                    }
                    else
                    {
                        var layoutBinding = GetDescriptorSetLayoutBinding(layout.Bindings, (int)binding, out bool isBindingNotUsed);
                        if (!isBindingNotUsed)
                        {
                            if (!layoutBinding.StageFlags.Contains(stage))
                            {
                                if (string.IsNullOrEmpty(layoutBinding.StageFlags))
                                {
                                    layoutBinding.DescriptorType = "DescriptorType.CombinedImageSampler";
                                    layoutBinding.StageFlags = stage;
                                }
                                else layoutBinding.StageFlags += $" | {stage}";
                            }
                            else
                            {
                                var location = GetAttributeLocation(data);
                                context.ReportDiagnostic(Diagnostic.Create(Descriptors.DescriptorAttachmentIsUsed, location, set, binding));
                            }
                        }
                        else
                        {
                            layout.SetIndex = set.ToString();
                            layout.DescriptorSetTarget = owner;
                            layoutBinding.DescriptorType = "DescriptorType.CombinedImageSampler";
                            layoutBinding.Binding = binding.ToString();
                            layoutBinding.StageFlags = stage;
                            result = AddSamplerImageShaderField(context, property, layout.SetIndex, layoutBinding, fields);
                        }
                    }
                }
            }
            else
            {
                var location = GetAttributeLocation(data);
                context.ReportDiagnostic(Diagnostic.Create(Descriptors.NotSamplerImage, location));
                return result;
            }
            return result;
        }
        private bool BuildStorageImage(GeneratorExecutionContext context, IPropertySymbol property,
            AttributeData data, string stage, List<DescriptorSetLayoutWrapper> setLayouts, Dictionary<string, IShaderField> fields)
        {
            bool result = false;
            var type = property.Type;
            if (type.TypeKind == TypeKind.Struct && IsImage(type))
            {
                if (data.ConstructorArguments.Length == 4)
                {
                    uint set = (uint)data.ConstructorArguments[0].Value;
                    uint binding = (uint)data.ConstructorArguments[1].Value;
                    string imageFormat = data.ConstructorArguments[2].Value.ToString();
                    string owner = data.ConstructorArguments[3].Value.ToString();
                    var layout = GetDescriptorSetLayout(setLayouts, (int)set, out bool isNew);
                    if (!isNew && layout.DescriptorSetTarget != owner)
                    {
                        var location = GetAttributeLocation(data);
                        context.ReportDiagnostic(Diagnostic.Create(Descriptors.DifferentDescriptorSetTarget, location, set));
                    }
                    else
                    {
                        var layoutBinding = GetDescriptorSetLayoutBinding(layout.Bindings, (int)binding, out bool isBindingNotUsed);
                        if (!isBindingNotUsed)
                        {
                            if (!layoutBinding.StageFlags.Contains(stage))
                            {
                                if (string.IsNullOrEmpty(layoutBinding.StageFlags))
                                {
                                    layoutBinding.DescriptorType = "DescriptorType.StorageImage";
                                    layoutBinding.StageFlags = stage;
                                }
                                else layoutBinding.StageFlags += $" | {stage}";
                            }
                            else
                            {
                                var location = GetAttributeLocation(data);
                                context.ReportDiagnostic(Diagnostic.Create(Descriptors.DescriptorAttachmentIsUsed, location, set, binding));
                            }
                        }
                        else
                        {
                            layout.SetIndex = set.ToString();
                            layout.DescriptorSetTarget = owner;
                            layoutBinding.DescriptorType = "DescriptorType.StorageImage";
                            layoutBinding.Binding = binding.ToString();
                            layoutBinding.StageFlags = stage;
                            layoutBinding.StorageFormat = $"(StorageFormat){imageFormat}";
                            result = AddStorageImageShaderField(context, property, layout.SetIndex, layoutBinding, fields);
                            result = true;
                        }
                    }
                }
            }
            else
            {
                var location = GetAttributeLocation(data);
                context.ReportDiagnostic(Diagnostic.Create(Descriptors.NotStorageImage, location));
                return result;
            }
            return result;
        }
        private bool BuildInputAttachment(GeneratorExecutionContext context, IPropertySymbol property,
            AttributeData data, string stage, List<DescriptorSetLayoutWrapper> setLayouts, Dictionary<string, IShaderField> fields)
        {
            bool result = false;
            var type = property.Type;
            if (type.TypeKind == TypeKind.Struct && type.Name == "subpassInput")
            {
                if (data.ConstructorArguments.Length == 3)
                {
                    uint set = (uint)data.ConstructorArguments[0].Value;
                    uint binding = (uint)data.ConstructorArguments[2].Value;
                    uint attachmentIndex = (uint)data.ConstructorArguments[1].Value;
                    string owner = "0";
                    var layout = GetDescriptorSetLayout(setLayouts, (int)set, out bool isNew);
                    if (!isNew && layout.DescriptorSetTarget != owner)
                    {
                        var location = GetAttributeLocation(data);
                        context.ReportDiagnostic(Diagnostic.Create(Descriptors.DifferentDescriptorSetTarget, location, set));
                    }
                    else
                    {
                        var layoutBinding = GetDescriptorSetLayoutBinding(layout.Bindings, (int)binding, out bool isBindingNotUsed);
                        if (!isBindingNotUsed)
                        {
                            var location = GetAttributeLocation(data);
                            context.ReportDiagnostic(Diagnostic.Create(Descriptors.DescriptorAttachmentIsUsed, location, set, binding));
                        }
                        else
                        {
                            layout.SetIndex = set.ToString();
                            layout.DescriptorSetTarget = owner;
                            layoutBinding.DescriptorType = "DescriptorType.InputAttachment";
                            layoutBinding.Binding = binding.ToString();
                            layoutBinding.StageFlags = stage;
                            layoutBinding.AttachmentIndex = attachmentIndex.ToString();
                            AddSubpassInputShaderField(context, property, layout.SetIndex, layoutBinding.AttachmentIndex,
                                layoutBinding.Binding, layoutBinding, fields);
                            result = true;
                        }
                    }
                }
            }
            else
            {
                var location = GetAttributeLocation(data);
                context.ReportDiagnostic(Diagnostic.Create(Descriptors.NotSubpassInput, location));
                return result;
            }
            return result;
        }
        private bool BuildPushConstant(GeneratorExecutionContext context, IPropertySymbol property,
            AttributeData data, string stage, List<PushConstantRangeWrapper> pushConstants, 
            Dictionary<string, IShaderField> fields)
        {
            bool result = false;
            var type = property.Type;
            if (type.TypeKind == TypeKind.Struct && CheckStruct(context, type))
            {
                if (data.ConstructorArguments.Length == 2)
                {
                    uint size = (uint)data.ConstructorArguments[0].Value;
                    uint offset = (uint)data.ConstructorArguments[1].Value;
                    PushConstantRangeWrapper wrapper = new PushConstantRangeWrapper()
                    {
                        Offset = offset.ToString(),
                        Size = size.ToString(),
                        StageFlags = stage
                    };
                    pushConstants.Add(wrapper);
                    result = AddPushConstantShaderField(context, property, fields);
                }
            }
            else
            {
                var location = property.Locations.FirstOrDefault() ?? Location.None;
                context.ReportDiagnostic(Diagnostic.Create(Descriptors.NotGLSLDataType, location, property.Name));
                return result;
            }
            return result;
        }
        private bool BuildVertexInput(GeneratorExecutionContext context, IPropertySymbol property,
            AttributeData data, List<VertexInputBindingWrapper> wrappers, Dictionary<string, IShaderField> fields)
        {
            bool result = false;
            var type = property.Type;
            if (type.TypeKind == TypeKind.Struct && IsInputOutputType(type))
            {
                uint binding = 0;
                if (data.ConstructorArguments.Length == 2)
                {
                    uint location = (uint)data.ConstructorArguments[0].Value;
                    string format = data.ConstructorArguments[1].Value.ToString();
                    uint size = Descriptors.GetFormatSize(format);
                    VertexInputBindingWrapper bindingWrapper = GetVertexInputBinding(wrappers, (int)binding);
                    VertexAttributeWrapper attr = GetVertexAttribute(bindingWrapper.InputAttributes, (int)location, out bool isNewLocation);
                    VertexAttributeWrapper preAttr = GetVertexAttribute(bindingWrapper.InputAttributes, ((int)location) - 1, out _);
                    bindingWrapper.Binding = binding;
                    if (isNewLocation)
                    {
                        uint offset = preAttr.Size + preAttr.Offset;
                        foreach (var n in data.NamedArguments)
                        {
                            switch (n.Key)
                            {
                                case "Binding":
                                    binding = (uint)n.Value.Value;
                                    break;
                                case "Size":
                                    size = (uint)n.Value.Value;
                                    break;
                                case "Offset":
                                    offset = (uint)n.Value.Value;
                                    break;
                                default:
                                    break;
                            }
                        }
                        attr.Offset = offset;
                        attr.Size = size;
                        attr.Location = location;
                        attr.Format = format;
                        attr.Binding = binding.ToString();
                        AddInputShaderField(context, property, attr.Location.ToString(), fields);
                        result = true;
                    }
                    else
                    {
                        var attrLocation = GetAttributeLocation(data);
                        context.ReportDiagnostic(Diagnostic.Create(Descriptors.VertexInputLocationIsUsed, attrLocation, location));
                        return result;
                    }
                }
            }
            else
            {
                var location = GetAttributeLocation(data);
                context.ReportDiagnostic(Diagnostic.Create(Descriptors.VertexInputTypeError, location));
                return result;
            }
            return result;
        }
        private bool BuildVertexInputBinding(GeneratorExecutionContext context, ITypeSymbol vert,
            AttributeData data, List<VertexInputBindingWrapper> wrappers)
        {
            if (data.ConstructorArguments.Length == 2)
            {
                uint binding = (uint)data.ConstructorArguments[0].Value;
                uint stride = (uint)data.ConstructorArguments[1].Value;
                string rate = "0";
                VertexInputBindingWrapper bindingWrapper = GetVertexInputBinding(wrappers, (int)binding);
                foreach (var l in data.NamedArguments)
                {
                    switch (l.Key)
                    {
                        case "InputRate":
                            rate = l.Value.ToString();
                            break;
                        default:
                            break;
                    }
                }
                bindingWrapper.Binding = binding;
                bindingWrapper.Stride = stride;
                bindingWrapper.InputRate = rate;
                return true;
            }
            return false;
        }
        private bool BuildFragOutput(GeneratorExecutionContext context, IPropertySymbol property,
            AttributeData data, List<FragOutputWrapper> wrappers, Dictionary<string, IShaderField> fields)
        {
            bool result = false;
            var type = property.Type;
            if (type.TypeKind == TypeKind.Struct && IsInputOutputType(type))
            {
                if (data.ConstructorArguments.Length == 2)
                {
                    uint location = (uint)data.ConstructorArguments[0].Value;
                    bool enable = (bool)data.ConstructorArguments[1].Value;
                    var output = GetFragOutput(wrappers, (int)location, out bool isFragNew);
                    if (isFragNew)
                    {
                        output.Location = location;
                        output.BlendEnable = enable;
                        foreach (var n in data.NamedArguments)
                        {
                            switch (n.Key)
                            {
                                case "AlphaBlendOp":
                                    output.AlphaBlendOp = $"(BlendOp){n.Value.Value}";
                                    break;
                                case "ColorBlendOp":
                                    output.ColorBlendOp = $"(BlendOp){n.Value.Value}";
                                    break;
                                case "SrcColorBlendFactor":
                                    output.SrcColorBlendFactor = $"(BlendFactor){n.Value.Value}";
                                    break;
                                case "DstColorBlendFactor":
                                    output.DstColorBlendFactor = $"(BlendFactor){n.Value.Value}";
                                    break;
                                case "SrcAlphaBlendFactor":
                                    output.SrcAlphaBlendFactor = $"(BlendFactor){n.Value.Value}";
                                    break;
                                case "DstAlphaBlendFactor":
                                    output.DstAlphaBlendFactor = $"(BlendFactor){n.Value.Value}";
                                    break;
                                case "ColorComponentFlags":
                                    output.ColorWriteMask = $"(ColorComponentFlags){n.Value.Value}";
                                    break;
                                default:
                                    break;
                            }
                        }
                        AddOutputShaderField(context, property, location.ToString(), fields);
                        result = true;
                    }
                    else
                    {
                        var attrLocation = GetAttributeLocation(data);
                        context.ReportDiagnostic(Diagnostic.Create(Descriptors.FragOutputLocationIsUsed, attrLocation, location));
                        return result;
                    }
                }
            }
            else
            {
                var location = GetAttributeLocation(data);
                context.ReportDiagnostic(Diagnostic.Create(Descriptors.FragOutputTypeError, location));
                return result;
            }
            return result;
        }
        private bool BuildColorBlend(GeneratorExecutionContext context, ITypeSymbol frag,
            AttributeData data, ColorBlendWrapper colorBlend)
        {
            if (data.ConstructorArguments.Length == 2)
            {
                bool enable = (bool)data.ConstructorArguments[0].Value;
                string logicOp = data.ConstructorArguments[1].Value.ToString();
                colorBlend.LogicOpEnable = enable.ToString();
                colorBlend.LogicOp = $"(LogicOp){logicOp}";
                foreach (var n in data.NamedArguments)
                {
                    switch (n.Key)
                    {
                        case "BlendConstants":
                            Vector4 vector = (Vector4)n.Value.Value;
                            colorBlend.BlendConstants = $"({vector.X}f, {vector.Y}f, {vector.Z}f, {vector.W}f)";
                            break;
                        default:
                            break;
                    }
                }
                return true;
            }
            return false;
        }
        private bool BuildShaderVersion(GeneratorExecutionContext context, ITypeSymbol vert,
            AttributeData data, InlineShaderContext shaderContext)
        {
            if (data.ConstructorArguments.Length == 1)
            {
                string version = data.ConstructorArguments[0].Value.ToString();
                shaderContext.ShaderVersion = version;
                return true;
            }
            return false;
        }
        private bool BuildShaderExtension(GeneratorExecutionContext context, ITypeSymbol shader,
            AttributeData data, List<string> extensions)
        {
            if (data.ConstructorArguments.Length == 1)
            {
                string extension = data.ConstructorArguments[0].Value.ToString();
                extensions.Add(extension);
            }
            return false;
        }
        private bool BuildInput(GeneratorExecutionContext context, IPropertySymbol property, AttributeData data,
            string stage, Dictionary<string, IShaderField> fields)
        {
            bool result = false;
            var location = GetAttributeLocation(data);
            if (stage == "ShaderStageFlags.VertexBit")
            {
                context.ReportDiagnostic(Diagnostic.Create(Descriptors.PleaseUseVertInputAttribute, location));
                return result;
            }
            var type = property.Type;
            if (IsInputOutputType(type))
            {
                if (data.ConstructorArguments.Length == 1)
                {
                    uint loc = (uint)data.ConstructorArguments[0].Value;
                    AddInputShaderField(context, property, loc.ToString(), fields);
                    result = true;
                }
            }
            else
            {
                context.ReportDiagnostic(Diagnostic.Create(Descriptors.VertexInputTypeError, location));
                return result;
            }
            return result;
        }
        private bool BuildOutput(GeneratorExecutionContext context, IPropertySymbol property, AttributeData data,
            string stage, Dictionary<string, IShaderField> fields)
        {
            bool result = false;
            var location = GetAttributeLocation(data);
            if (stage == "ShaderStageFlags.FragmentBit")
            {
                context.ReportDiagnostic(Diagnostic.Create(Descriptors.PleaseUseFragOutputAttribute, location));
                return result;
            }
            var type = property.Type;
            if (IsInputOutputType(type))
            {
                if (data.ConstructorArguments.Length == 1)
                {
                    uint loc = (uint)data.ConstructorArguments[0].Value;
                    AddOutputShaderField(context, property, loc.ToString(), fields);
                    result = true;
                }
            }
            else
            {
                context.ReportDiagnostic(Diagnostic.Create(Descriptors.FragOutputTypeError, location));
                return result;
            }
            return result;
        }
        #endregion

        #region shaderfield
        private bool AddUniformBufferShaderField(GeneratorExecutionContext context, 
            IPropertySymbol property, string setIndex, DescriptorSetLayoutBindingWrapper wrapper, 
            Dictionary<string, IShaderField> dic)
        {
            if (dic.ContainsKey(property.Name)) return true;
            var type = property.Type;
            var location = property.Locations.FirstOrDefault() ?? Location.None;
            UniformBufferShaderField shaderField = new UniformBufferShaderField(setIndex, wrapper.Binding, wrapper.DataLayout, type, property.Name);
            bool result = CheckUniformFieldType(context, type, shaderField, location, dic);
            if(result) dic.Add(property.Name, shaderField);
            return result;
        }
        private bool AddStorageBufferShaderField(GeneratorExecutionContext context,
            IPropertySymbol property, string setIndex, DescriptorSetLayoutBindingWrapper wrapper, Dictionary<string, IShaderField> dic)
        {
            if (dic.ContainsKey(property.Name)) return true;
            var type = property.Type;
            var location = property.Locations.FirstOrDefault() ?? Location.None;
            StorageBufferShaderField shaderField = new StorageBufferShaderField(setIndex, wrapper.Binding, wrapper.DataLayout, type, property.Name);
            bool result = CheckUniformFieldType(context, type, shaderField, location, dic);
            if(result) dic.Add(property.Name, shaderField);
            return result;
        }
        private bool AddPushConstantShaderField(GeneratorExecutionContext context, IPropertySymbol property, Dictionary<string, IShaderField> dic)
        {
            if (dic.ContainsKey(property.Name)) return true;
            var type = property.Type;
            var location = property.Locations.FirstOrDefault() ?? Location.None;
            PushConstantShaderField shaderField = new PushConstantShaderField(type, property.Name);
            bool result = CheckUniformFieldType(context, type, shaderField, location, dic);
            if (result) dic.Add(property.Name, shaderField);
            return result;
        }
        private bool AddSamplerImageShaderField(GeneratorExecutionContext context, IPropertySymbol property, 
            string setIndex, DescriptorSetLayoutBindingWrapper wrapper, Dictionary<string, IShaderField> dic)
        {
            if (dic.ContainsKey(property.Name)) return true;
            var type = property.Type;
            var location = property.Locations.FirstOrDefault() ?? Location.None;
            SamplerImageShaderField shaderField = new SamplerImageShaderField(setIndex, wrapper.Binding, type, property.Name);
            bool result = CheckUniformFieldType(context, type, shaderField, location, dic);
            if (result) dic.Add(property.Name, shaderField);
            return result;
        }
        private bool AddStorageImageShaderField(GeneratorExecutionContext context, IPropertySymbol property,
            string setIndex, DescriptorSetLayoutBindingWrapper wrapper, Dictionary<string, IShaderField> dic)
        {
            if (dic.ContainsKey(property.Name)) return true;
            var type = property.Type;
            var location = property.Locations.FirstOrDefault() ?? Location.None;
            StorageImageShaderField shaderField = new StorageImageShaderField(setIndex, wrapper.Binding,
                GetBufferLayoutEnumValue(wrapper.StorageFormat), type, property.Name);
            bool result = CheckUniformFieldType(context, type, shaderField, location, dic);
            if (result) dic.Add(property.Name, shaderField);
            return result;
        }
        private bool AddSubpassInputShaderField(GeneratorExecutionContext context, IPropertySymbol property,
            string setIndex, string attachmentIndex, string binding, DescriptorSetLayoutBindingWrapper wrapper, Dictionary<string, IShaderField> dic)
        {
            if (dic.ContainsKey(property.Name)) return true;
            var type = property.Type;
            var location = property.Locations.FirstOrDefault() ?? Location.None;
            SubpassInputShaderField shaderField = new SubpassInputShaderField(property.Type, property.Name,
                setIndex, attachmentIndex, binding);
            dic.Add(property.Name, shaderField);
            return true;
        }
        private bool AddInputShaderField(GeneratorExecutionContext context, IPropertySymbol property,
            string location, Dictionary<string, IShaderField> dic)
        {
            if (dic.ContainsKey(property.Name)) return true;
            var type = property.Type;
            InputShaderField shaderField = new InputShaderField(location, type, property.Name);
            dic.Add(property.Name, shaderField);
            return true;
        }
        private bool AddOutputShaderField(GeneratorExecutionContext context, IPropertySymbol property,
            string location, Dictionary<string, IShaderField> dic)
        {
            if (dic.ContainsKey(property.Name)) return true;
            var type = property.Type;
            OutputShaderField shaderField = new OutputShaderField(location, type, property.Name);
            dic.Add(property.Name, shaderField);
            return true;
        }
        private bool CheckUniformFieldType(GeneratorExecutionContext context, ITypeSymbol type, IShaderField shaderField,
            Location location, Dictionary<string, IShaderField> dic)
        {
            if (IsGLSLBaseType(type))
            {
                context.ReportDiagnostic(Diagnostic.Create(Descriptors.NotAllowedNonOpaqueUniformsOutsideBlock, location));
                return false;
            }
            else if(IsSamplerImage(type)) return true;
            else if(IsImage(type)) return true;
            else
            {
                if (type.TypeKind == TypeKind.Struct)
                {
                    return AddNewStructShaderField(context, type, shaderField, location, dic);
                }
                else
                {
                    context.ReportDiagnostic(Diagnostic.Create(Descriptors.NotGLSLDataType, location));
                    return false;
                }
            }
        }
        private bool CheckNormalFieldType(GeneratorExecutionContext context, ITypeSymbol type, IShaderField shaderField, Location location, Dictionary<string, IShaderField> dic)
        {
            location ??= Location.None;
            if(IsGLSLBaseType(type)) return true;
            else if(IsSamplerImage(type) || IsImage(type))
            {
                context.ReportDiagnostic(Diagnostic.Create(Descriptors.SamplerOrImageMustbeDefineInUniform, location));
                return false;
            }
            else
            {
                if(type.TypeKind == TypeKind.Array)
                {
                    IArrayTypeSymbol arrayType = type as IArrayTypeSymbol;
                    var elementType = arrayType.ElementType;
                    return CheckNormalFieldType(context, elementType, shaderField, location, dic);
                }
                if (type.TypeKind == TypeKind.Struct)
                {
                    return AddNewStructShaderField(context, type, shaderField, location, dic);
                }
                else
                {
                    context.ReportDiagnostic(Diagnostic.Create(Descriptors.NotGLSLDataType, location, type.Name));
                    return false;
                }
            }
        }
        private bool AddNewStructShaderField(GeneratorExecutionContext context, ITypeSymbol type, IShaderField shaderField, Location location, Dictionary<string, IShaderField> dic)
        {
            foreach (var item in type.GetMembers().Where(x => x.Kind == SymbolKind.Field || 
                x.Kind == SymbolKind.Property))
            {
                ITypeSymbol symbol = null;
                if (item is IPropertySymbol property)
                {
                    var pType = property.Type;
                    if (pType is IArrayTypeSymbol aType) pType = aType.ElementType;
                    symbol = pType;

                }
                else if (item is IFieldSymbol field)
                {
                    if (field.IsImplicitlyDeclared) continue;
                    var fType = field.Type;
                    if (fType is IArrayTypeSymbol aType) fType = aType.ElementType;
                    symbol = fType;
                }
                if (symbol.TypeKind != TypeKind.Struct)
                {
                    context.ReportDiagnostic(Diagnostic.Create(Descriptors.NotGLSLDataType, location));
                    return false;
                }
                if (IsGLSLBaseType(symbol)) continue;
                if (dic.ContainsKey(symbol.Name)) continue;
                else
                {
                    StructTypeShaderField str = new StructTypeShaderField(symbol);
                    var loc = symbol.Locations.FirstOrDefault() ?? Location.None;
                    bool local = CheckNormalFieldType(context, symbol, str, loc, dic);
                    if (local)
                    {
                        if (!shaderField.CheckShaderField(context)) return false;
                        else
                        {
                            shaderField.Childrens.Add(str);
                            dic.Add(symbol.Name, str);
                        }
                    }
                    else return local;
                }
            }
            return true;
        }
        /// <summary>
        /// 添加新的普通用户定义字段
        /// </summary>
        /// <param name="context"></param>
        /// <param name="target"></param>
        /// <param name="location"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        private bool AddNewNormalShaderField(GeneratorExecutionContext context, InlineShaderMethod caller, SemanticModel model, IFieldSymbol target,
            Location location, InlineShaderContext shaderContext)
        {
            location ??= Location.None;
            var type = target.Type;
            NormalShaderField normal = new NormalShaderField();
            normal.Name = target.Name;
            bool result = CheckNormalFieldType(context, type, normal, location, shaderContext.InlineFileds);
            if (result)
            {
                string fieldCode = string.Empty;
                var refs = target.DeclaringSyntaxReferences;
                if (refs.Length > 0)
                {
                    var syntax = refs[0].GetSyntax() as VariableDeclaratorSyntax;
                    var init = syntax.Initializer;
                    if (init == null)
                    {
                        if(type.TypeKind == TypeKind.Array)
                        {
                            context.ReportDiagnostic(Diagnostic.Create(Descriptors.InitArrayLengthLost, location, target.Name));
                            return false;
                        }
                        else
                        {
                            result = NewFieldCode(context, target, location, out fieldCode);
                            normal.Code = fieldCode;
                        }
                    }
                    else
                    {
                        var op = model.GetOperation(init);
                        if (op != null && op is IFieldInitializerOperation initializer)
                        {
                            var value = initializer.Value;
                            if(type.TypeKind == TypeKind.Array)
                            {
                                var arrayOP = value as IArrayCreationOperation;
                                var arrayType = target.Type as IArrayTypeSymbol;
                                result = GetGLSLTypeName(context, arrayType.ElementType, location, out string name);
                                if (!result) return result;
                                result = GetArrayCreationCode(context, caller, arrayOP, name, shaderContext, out string arraySize, out string code);
                                if (!result) return result;
                                fieldCode += $"{name}{arraySize} {target.Name}{code}";
                                fieldCode += ";\r\n";
                                normal.Code = fieldCode;
                                shaderContext.InlineFileds.Add(target.Name, normal);
                            }
                            else
                            {
                                result = GetInitializerCode(context, caller, value, type, shaderContext, out string code);
                                if (result)
                                {
                                    if (target.IsConst) fieldCode += "const ";
                                    result = GetGLSLTypeName(context, target.Type, location, out string name);
                                    if (result) fieldCode += $"{name} {target.Name} = {code};\r\n";
                                    normal.Code = fieldCode;
                                    shaderContext.InlineFileds.Add(target.Name, normal);
                                }
                            }
                        }
                        else
                        {
                            result = NewFieldCode(context, target, location, out fieldCode);
                            normal.Code = fieldCode;
                        }
                    }

                }
                else
                {
                    context.ReportDiagnostic(Diagnostic.Create(Descriptors.InlineGLSLFieldSourceLost, location, target.Name));
                    return false;
                }
            }
            return result;
        }
        /// <summary>
        /// 添加新的普通用户定义字段
        /// </summary>
        /// <param name="context"></param>
        /// <param name="target"></param>
        /// <param name="location"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        private bool AddNewNormalShaderField(GeneratorExecutionContext context, InlineShaderMethod caller, SemanticModel model, IPropertySymbol target,
            Location location, InlineShaderContext shaderContext)
        {
            location ??= Location.None;
            var type = target.Type;
            NormalShaderField normal = new NormalShaderField();
            bool result = CheckNormalFieldType(context, type, normal, location, shaderContext.InlineFileds);
            if (result)
            {
                string fieldCode = string.Empty;
                var refs = target.DeclaringSyntaxReferences;
                if (refs.Length > 0)
                {
                    var syntax = refs[0].GetSyntax();
                    if (syntax is VariableDeclaratorSyntax variable)
                    {
                        var init = variable.Initializer;
                        if (init == null)
                        {
                            if (type.TypeKind == TypeKind.Array)
                            {
                                context.ReportDiagnostic(Diagnostic.Create(Descriptors.InitArrayLengthLost, location, target.Name));
                                return false;
                            }
                            else
                            {
                                result = NewPropertyCode(context, target, location, out fieldCode);
                                normal.Code = fieldCode;
                            }
                        }
                        else
                        {
                            var op = model.GetOperation(init);
                            if (op != null && op is IPropertyInitializerOperation initializer)
                            {
                                var value = initializer.Value;
                                if (type.TypeKind == TypeKind.Array)
                                {
                                    var arrayOP = value as IArrayCreationOperation;
                                    var arrayType = target.Type as IArrayTypeSymbol;
                                    result = GetGLSLTypeName(context, arrayType.ElementType, location, out string name);
                                    if (!result) return result;
                                    result = GetArrayCreationCode(context, caller, arrayOP, name, shaderContext, out string arraySize, out string code);
                                    if (!result) return result;
                                    fieldCode += $"{name}{arraySize} {target.Name} {code}";
                                    fieldCode = fieldCode.TrimEnd();
                                    fieldCode += ";\r\n";
                                    normal.Code = fieldCode;
                                    shaderContext.InlineFileds.Add(target.Name, normal);
                                }
                                else
                                {
                                    result = GetInitializerCode(context, caller, value, type, shaderContext, out string code);
                                    if (result)
                                    {
                                        result = GetGLSLTypeName(context, target.Type, location, out string name);
                                        if (result) fieldCode += $"{name} {target.Name} = {code};\r\n";
                                        normal.Code = fieldCode;
                                        shaderContext.InlineFileds.Add(target.Name, normal);
                                    }
                                }
                            }
                            else
                            {
                                result = NewPropertyCode(context, target, location, out fieldCode);
                                normal.Code = fieldCode;
                            }
                        }
                    }
                    else
                    {
                        if(type.TypeKind == TypeKind.Array)
                        {
                            context.ReportDiagnostic(Diagnostic.Create(Descriptors.InitArrayLengthLost, location, target.Name));
                            return false;
                        }
                        else
                        {
                            result = NewPropertyCode(context, target, location, out fieldCode);
                            normal.Code = fieldCode;
                        }
                    }
                }
                else
                {
                    context.ReportDiagnostic(Diagnostic.Create(Descriptors.InlineGLSLFieldSourceLost, location, target.Name));
                    return false;
                }
            }
            return result;
        }
        #endregion

        private bool IsInputOutputType(ITypeSymbol type)
        {
            switch (type.SpecialType)
            {
                case SpecialType.None:
                    break;
                case SpecialType.System_Int32:
                case SpecialType.System_UInt32:
                case SpecialType.System_Single:
                case SpecialType.System_Double:
                    return true;
                default:
                    return false;
            }
            if (type.TypeKind == TypeKind.Struct)
            {
                switch (type.Name)
                {
                    case "dvec2":
                    case "dvec3":
                    case "dvec4":
                    case "ivec2":
                    case "ivec3":
                    case "ivec4":
                    case "uvec2":
                    case "uvec3":
                    case "uvec4":
                    case "vec2":
                    case "vec3":
                    case "vec4":
                        return true;
                    default:
                        return false;
                }
            }
            return false;
        }
        /// <summary>
        /// 是否为采样器类型
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private bool IsSamplerImage(ITypeSymbol type)
        {
            if (type.TypeKind == TypeKind.Struct)
            {
                switch (type.Name)
                {
                    case "sampler1D":
                    case "isampler1D":
                    case "usampler1D":
                    case "sampler1DArray":
                    case "isampler1DArray":
                    case "usampler1DArray":
                    case "sampler2D":
                    case "isampler2D":
                    case "usampler2D":
                    case "sampler2DArray":
                    case "isampler2DArray":
                    case "usampler2DArray":
                    case "sampler2DMS":
                    case "isampler2DMS":
                    case "usampler2DMS":
                    case "sampler2DMSArray":
                    case "isampler2DMSArray":
                    case "usampler2DMSArray":
                    case "sampler2DRect":
                    case "isampler2DRect":
                    case "usampler2DRect":
                    case "sampler3D":
                    case "isampler3D":
                    case "usampler3D":
                    case "samplerCube":
                    case "isamplerCube":
                    case "usamplerCube":
                    case "samplerCubeArray":
                    case "isamplerCubeArray":
                    case "usamplerCubeArray":
                    case "sampler1DShadow":
                    case "sampler2DShadow":
                    case "samplerCubeShadow":
                    case "sampler2DRectShadow":
                    case "sampler1DArrayShadow":
                    case "sampler2DArrayShadow":
                    case "samplerCubeArrayShadow":
                        return true;
                }
            }
            return false;
        }
        private bool IsImage(ITypeSymbol type)
        {
            if (type.TypeKind == TypeKind.Struct)
            {
                switch(type.Name)
                {
                    case "image1D":
                    case "iimage1D":
                    case "uimage1D":
                    case "image1DArray":
                    case "iimage1DArray":
                    case "uimage1DArray":
                    case "image2D":
                    case "iimage2D":
                    case "uimage2D":
                    case "image2DArray":
                    case "iimage2DArray":
                    case "uimage2DArray":
                    case "image2DMS":
                    case "iimage2DMS":
                    case "uimage2DMS":
                    case "image2DMSArray":
                    case "iimage2DMSArray":
                    case "uimage2DMSArray":
                    case "image2DRect":
                    case "iimage2DRect":
                    case "uimage2DRect":
                    case "image3D":
                    case "iimage3D":
                    case "uimage3D":
                    case "imageCube":
                    case "iimageCube":
                    case "uimageCube":
                    case "imageCubeArray":
                    case "iimageCubeArray":
                    case "uimageCubeArray":
                        return true;
                }
            }
            return false;
        }
        private Location GetAttributeLocation(AttributeData data)
        {
            var location = Location.None;
            var node = data.ApplicationSyntaxReference?.GetSyntax();
            if (node != null && node is AttributeSyntax attribute)
            {
                location = attribute.GetLocation();
            }
            return location;
        }

        private DescriptorSetLayoutWrapper GetDescriptorSetLayout(List<DescriptorSetLayoutWrapper> setLayouts, int setIndex, out bool isNew)
        {
            isNew = false;
            if (setIndex < 0) setIndex = 0;
            else if (setIndex >= 1024) setIndex = 1024;
            if (setIndex >= setLayouts.Count)
            {
                int diff = setIndex - setLayouts.Count;
                for (int i = 0; i <= diff; i++)
                {
                    setLayouts.Add(new DescriptorSetLayoutWrapper());
                    isNew = true;
                }
            }
            return setLayouts[setIndex];
        }
        private DescriptorSetLayoutBindingWrapper GetDescriptorSetLayoutBinding(List<DescriptorSetLayoutBindingWrapper> bindings, int binding, out bool isNew)
        {
            isNew = false;
            if (binding < 0) binding = 0;
            else if (binding >= 1024) binding = 1024;
            if (binding >= bindings.Count)
            {
                int diff = binding - bindings.Count;
                for (int i = 0; i <= diff; i++)
                {
                    isNew = true;
                    bindings.Add(new DescriptorSetLayoutBindingWrapper());
                }
            }
            return bindings[binding];
        }
        private VertexInputBindingWrapper GetVertexInputBinding(List<VertexInputBindingWrapper> bindings, int binding)
        {
            if (binding < 0) binding = 0;
            else if (binding >= 1024) binding = 1024;
            if (binding >= bindings.Count)
            {
                int diff = binding - bindings.Count;
                for (int i = 0; i <= diff; i++)
                {
                    bindings.Add(new VertexInputBindingWrapper());
                }
            }
            return bindings[binding];
        }
        private VertexAttributeWrapper GetVertexAttribute(List<VertexAttributeWrapper> bindings, int attribute, out bool isNew)
        {
            isNew = false;
            if (attribute < 0) attribute = 0;
            else if (attribute >= 1024) attribute = 1024;
            if (attribute >= bindings.Count)
            {
                int diff = attribute - bindings.Count;
                for (int i = 0; i <= diff; i++)
                {
                    isNew = true;
                    bindings.Add(new VertexAttributeWrapper());
                }
            }
            return bindings[attribute];
        }
        private FragOutputWrapper GetFragOutput(List<FragOutputWrapper> bindings, int location, out bool isNew)
        {
            isNew = false;
            if (location < 0) location = 0;
            else if (location >= 1024) location = 1024;
            if (location >= bindings.Count)
            {
                int diff = location - bindings.Count;
                for (int i = 0; i <= diff; i++)
                {
                    isNew = true;
                    bindings.Add(new FragOutputWrapper());
                }
            }
            return bindings[location];
        }
        private string GetBufferLayoutEnumValue(string v)
        {
            return v switch
            {
                "1" => "rgba16f",
                "2" => "rg32f",
                "3" => "rg16f",
                "4" => "r11f_g11f_b10f",
                "5" => "r32f",
                "6" => "r16f",
                "7" => "rgba16",
                "8" => "rgb10_a2",
                "9" => "rgba8",
                "10" => "rg16",
                "11" => "rg8",
                "12" => "r16",
                "13" => "r8",
                "14" => "rgba16_snorm",
                "15" => "rgba8_snorm",
                "16" => "rg16_snorm",
                "17" => "rg8_snorm",
                "18" => "r16_snorm",
                "19" => "r8_snorm",
                "20" => "rgba32i",
                "21" => "rgba16i",
                "22" => "rgba8i",
                "23" => "rg32i",
                "24" => "rg16i",
                "25" => "rg8i",
                "26" => "r32i",
                "27" => "r16i",
                "28" => "r8i",
                "29" => "rgba32ui",
                "30" => "rgba16ui",
                "31" => "rgb10_a2ui",
                "32" => "rgba8ui",
                "33" => "rg32ui",
                "34" => "rg16ui",
                "35" => "rg8ui",
                "36" => "r32ui",
                "37" => "r16ui",
                "38" => "r8ui",
                _ => "rgba32f"
            };
        }
        private string GetUniformDataLayoutEnumValue(string v)
        {
            return v switch
            {
                "1" => "std430",
                _ => "std140"
            };
        }
    }

    public class ScriptedPipelineReceiver : ISyntaxReceiver
    {
        public List<ClassDeclarationSyntax> Pipes { get; } = new();

        public void OnVisitSyntaxNode(SyntaxNode syntaxNode)
        {
            if (syntaxNode is BaseListSyntax bl)
            {
                foreach (var b in bl.Types)
                {
                    if (b is SimpleBaseTypeSyntax sbt)
                    {
                        if(sbt.ToString() == "ScriptedVulkanPipeline")
                        {
                            if (bl.Parent != null && bl.Parent is ClassDeclarationSyntax cd)
                            {
                                Pipes.Add(cd);
                            }
                        }
                        else if(sbt.ToString() == "GLSLVert")
                        {
                            if (bl.Parent != null && bl.Parent is ClassDeclarationSyntax cd)
                            {
                                Pipes.Add(cd);
                            }
                        }
                        else if (sbt.ToString() == "GLSLFrag")
                        {
                            if (bl.Parent != null && bl.Parent is ClassDeclarationSyntax cd)
                            {
                                Pipes.Add(cd);
                            }
                        }
                        else if (sbt.ToString() == "GLSLGeom")
                        {
                            if (bl.Parent != null && bl.Parent is ClassDeclarationSyntax cd)
                            {
                                Pipes.Add(cd);
                            }
                        }
                    }
                }
            }
        }
    }
}
