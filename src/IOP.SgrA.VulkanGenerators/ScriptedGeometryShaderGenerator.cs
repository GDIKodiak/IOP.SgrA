﻿using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    public partial class ScriptedPipelineGenerator
    {
        private bool ParseGLSLGeomShader(GeneratorExecutionContext context, ITypeSymbol geom, 
            GeometryShaderWraaper geometryShader)
        {
            bool result = true;
            string flags = "ShaderStageFlags.GeometryBit";
            var fields = geometryShader.ShaderContext.InlineFileds;
            foreach (var attr in geom.GetAttributes())
            {
                if (attr.AttributeClass.Name == "GeometryVertexInputAttribute")
                {
                    BuildGeomVertexInput(context, geom, attr, geometryShader.VertexInput);
                }
                else if(attr.AttributeClass.Name == "GeometryVertexOutputAttribute")
                {
                    BuildGeomVertexOutput(context, geom, attr, geometryShader.VertexOutput);
                }
                else if (attr.AttributeClass.Name == "GLSLExtensionAttribute")
                {
                    BuildShaderExtension(context, geom, attr, geometryShader.ShaderContext.ShaderExtensions);
                }
                else if (attr.AttributeClass.Name == "ShaderVersionAttribute")
                {
                    BuildShaderVersion(context, geom, attr, geometryShader.ShaderContext);
                }
            }
            foreach (var prop in geom.GetMembers().Where(x => x.Kind == SymbolKind.Property))
            {
                var p = prop as IPropertySymbol;
                foreach (var attr in p.GetAttributes())
                {
                    if (attr.AttributeClass.Name == "UniformBufferAttribute")
                    {
                        bool r = BuildShaderBuffer(context, p, attr, flags, "DescriptorType.UniformBuffer",
                            geometryShader.DescriptorSetLayouts, fields);
                        if (!r) result = r;
                    }
                    else if (attr.AttributeClass.Name == "DynamicUniformBufferAttribute")
                    {
                        bool r = BuildShaderBuffer(context, p, attr, flags, "DescriptorType.UniformBufferDynamic",
                            geometryShader.DescriptorSetLayouts, fields);
                        if (!r) result = r;
                    }
                    else if (attr.AttributeClass.Name == "StorageBufferAttribute")
                    {
                        bool r = BuildShaderBuffer(context, p, attr, flags, "DescriptorType.StorageBuffer",
                            geometryShader.DescriptorSetLayouts, fields);
                        if (!r) result = r;
                    }
                    else if (attr.AttributeClass.Name == "DynamicStorageBufferAttribute")
                    {
                        bool r = BuildShaderBuffer(context, p, attr, flags, "DescriptorType.StorageBufferDynamic",
                            geometryShader.DescriptorSetLayouts, fields);
                        if (!r) result = r;
                    }
                    else if (attr.AttributeClass.Name == "SampledImageAttribute")
                    {
                        bool r = BuildSamplerImage(context, p, attr, flags, geometryShader.DescriptorSetLayouts, fields);
                        if (!r) result = r;
                    }
                    else if (attr.AttributeClass.Name == "StorageImageAttribute")
                    {
                        bool r = BuildStorageImage(context, p, attr, flags, geometryShader.DescriptorSetLayouts, fields);
                        if (!r) result = r;
                    }
                    else if (attr.AttributeClass.Name == "PushConstantAttribute")
                    {
                        bool r = BuildPushConstant(context, p, attr, flags, geometryShader.PushConstants, fields);
                        if (!r) result = r;
                    }
                    else if (attr.AttributeClass.Name == "ShaderInputAttribute")
                    {
                        bool r = BuildInput(context, p, attr, flags, fields);
                        if (!r) result = r;
                    }
                    else if (attr.AttributeClass.Name == "ShaderOutputAttribute")
                    {
                        bool r = BuildOutput(context, p, attr, flags, fields);
                        if (!r) result = r;
                    }
                    else if (attr.AttributeClass.Name == "GeometryInputAttribute")
                    {
                        bool r = BuildGeometryInput(context, p, attr, fields);
                        if (!r) result = r;
                    }
                }
            }
            if (!ParsedGeometries.ContainsKey(geom.ToString()))
            {
                ParseGeometryShader(context, geom, geometryShader, geometryShader.ShaderContext);
                ParsedGeometries.Add(geom.ToString(), geometryShader);
            }
            return result;
        }
        private bool ParseGeometryShader(GeneratorExecutionContext context, ITypeSymbol geom, 
            GeometryShaderWraaper wraaper, InlineShaderContext geomShader)
        {
            IMethodSymbol main = null;
            bool result = false;
            var mainFuncs = geom.GetMembers().Where(x => x.Kind == SymbolKind.Method && x.Name == "main");
            foreach (var func in mainFuncs)
            {
                if (func is not IMethodSymbol method) continue;
                if (method.Parameters.Length != 0 || method.MethodKind != MethodKind.Ordinary) continue;
                main = method;
                break;
            }
            if (main != null)
            {
                AddGeomInlineField(geomShader.InlineFileds);
                StringBuilder shader = new StringBuilder();
                result = ParseInlineGLSLMethod(context, main, geomShader, shader);
                if (!result) return result;
                StringBuilder code = new StringBuilder();
                code.AppendLine($"#version {geomShader.ShaderVersion}");
                foreach (var ext in geomShader.ShaderExtensions)
                {
                    code.AppendLine($"#extension {ext}");
                }
                code.AppendLine($"layout({wraaper.VertexInput.VertexInputType}) in;");
                code.AppendLine($"layout({wraaper.VertexOutput.VertexOutputType}, max_vertices = {wraaper.VertexOutput.MaxVertices}) out;");
                List<InlineShaderMethod> needParseMethod = new List<InlineShaderMethod>();
                Dictionary<string, InlineShaderMethod> methodBooks = new Dictionary<string, InlineShaderMethod>();
                result = CheckInlineMethods(context, geomShader, code, needParseMethod);
                if (result)
                {
                    Dictionary<string, IShaderField> books = new Dictionary<string, IShaderField>();
                    var dic = geomShader.InlineFileds;
                    foreach (var item in dic.Values)
                    {
                        WriteShdaerFields(books, item, code);
                    }
                    foreach (var item in geomShader.InlineMethods)
                    {
                        var local = item.Value;
                        WriteMethods(methodBooks, local, code);
                    }
                }
                else return result;
                StringBuilder fileBuilder = new StringBuilder();
                fileBuilder.AppendLine("using IOP.SgrA.SilkNet.Vulkan;");
                fileBuilder.AppendLine("using IOP.SgrA.SilkNet.Vulkan.Scripted;");
                fileBuilder.AppendLine("using Silk.NET.Vulkan;");
                fileBuilder.AppendLine("using IOP.SgrA.GLSL;");
                fileBuilder.AppendLine("using IOP.SgrA;");
                var np = geom.ContainingNamespace;
                if (np != null)
                {
                    fileBuilder.AppendLine($"namespace {np}");
                    fileBuilder.AppendLine("{");
                    fileBuilder.AppendLine($"\tpublic partial class {geom.Name} : {geom.BaseType.Name}");
                    fileBuilder.AppendLine("\t{");
                    fileBuilder.AppendLine("\t\tprivate string _GeneratorCode = @\"");
                    fileBuilder.Append(code.ToString());
                    fileBuilder.AppendLine("\";");
                    fileBuilder.AppendLine($"\t\tpublic override string GetCode()");
                    fileBuilder.AppendLine("\t\t{");
                    fileBuilder.AppendLine("\t\t\treturn _GeneratorCode;");
                    fileBuilder.AppendLine("\t\t}");
                    fileBuilder.AppendLine("\t}");
                    fileBuilder.AppendLine("}");
                    context.AddSource($"{geom.Name}.g.cs", fileBuilder.ToString());
                }
            }
            return result;
        }

        private void AddGeomInlineField(Dictionary<string, IShaderField> fields)
        {
            NormalShaderField gl_FragCoord = new NormalShaderField() { Skip = true };
            fields.Add("gl_PrimitiveID", gl_FragCoord);
            NormalShaderField gl_FragDepth = new NormalShaderField() { Skip = true };
            fields.Add("gl_Position", gl_FragDepth);
            NormalShaderField gl_PointCoord = new NormalShaderField() { Skip = true };
            fields.Add("gl_PointSize", gl_PointCoord);
            NormalShaderField gl_FrontFacing = new NormalShaderField() { Skip = true };
            fields.Add("gl_ClipDistance", gl_FrontFacing);
            NormalShaderField gl_HelperInvocation = new NormalShaderField() { Skip = true };
            fields.Add("gl_CullDistance", gl_HelperInvocation);
            NormalShaderField gl_Layer = new NormalShaderField() { Skip = true };
            fields.Add("gl_in", gl_Layer);
            NormalShaderField gl_NumSamples = new NormalShaderField() { Skip = true };
            fields.Add("gl_Layer", gl_NumSamples);
            NormalShaderField gl_PrimitiveID = new NormalShaderField() { Skip = true };
            fields.Add("gl_PrimitiveIDIn", gl_PrimitiveID);
            NormalShaderField gl_SampleID = new NormalShaderField() { Skip = true };
            fields.Add("gl_ViewportIndex", gl_SampleID);
        }

        private bool BuildGeometryInput(GeneratorExecutionContext context, IPropertySymbol property, AttributeData data,
            Dictionary<string, IShaderField> fields)
        {
            bool result = false;
            var location = GetAttributeLocation(data);
            var type = property.Type;
            if (IsGeometryInput(type))
            {
                if (data.ConstructorArguments.Length == 1)
                {
                    uint loc = (uint)data.ConstructorArguments[0].Value;
                    AddInputShaderField(context, property, loc.ToString(), fields);
                    result = true;
                }
            }
            else
            {
                context.ReportDiagnostic(Diagnostic.Create(Descriptors.GeometryInputTypeError, location));
                return result;
            }
            return result;
        }

        private bool IsGeometryInput(ITypeSymbol type)
        {
            switch (type.SpecialType)
            {
                case SpecialType.None:
                    break;
                case SpecialType.System_Int32:
                case SpecialType.System_UInt32:
                case SpecialType.System_Single:
                case SpecialType.System_Double:
                    return true;
                default:
                    return false;
            }
            if (type.TypeKind == TypeKind.Struct)
            {
                switch (type.Name)
                {
                    case "dvec2":
                    case "dvec3":
                    case "dvec4":
                    case "ivec2":
                    case "ivec3":
                    case "ivec4":
                    case "uvec2":
                    case "uvec3":
                    case "uvec4":
                    case "vec2":
                    case "vec3":
                    case "vec4":
                        return true;
                    default:
                        return false;
                }
            }
            else if (type.TypeKind == TypeKind.Array)
            {
                var arrayType = type as IArrayTypeSymbol;
                var eleType = arrayType.ElementType;
                return IsInputOutputType(eleType);
            }
            return false;
        }
        private bool BuildGeomVertexInput(GeneratorExecutionContext context, ITypeSymbol geom,
            AttributeData data, GeometryVertexInputWrapper wrapper)
        {
            if (data.ConstructorArguments.Length == 1)
            {
                string type = data.ConstructorArguments[0].Value.ToString();
                type = GetGeomInputVertexType(type);
                wrapper.VertexInputType = type;
                return true;
            }
            else return false;
        }
        private bool BuildGeomVertexOutput(GeneratorExecutionContext context, ITypeSymbol geom,
            AttributeData data, GeometryVertexOutputWrapper wrapper)
        {
            if (data.ConstructorArguments.Length == 2)
            {
                string type = data.ConstructorArguments[0].Value.ToString();
                type = GetGeomOutputVertexType(type);
                wrapper.VertexOutputType = type;
                uint max = (uint)data.ConstructorArguments[1].Value;
                wrapper.MaxVertices = max;
                return true;
            }
            else return false;
        }

        private string GetGeomInputVertexType(string num)
        {
            return num switch
            {
                "0" => "points",
                "1" => "lines",
                "2" => "lines_adjacency",
                "3" => "triangles",
                "4" => "triangles_adjacency",
                _ => "points"
            };
        }
        private string GetGeomOutputVertexType(string num)
        {
            return num switch
            {
                "0" => "points",
                "1" => "line_strip",
                "2" => "triangle_strip",
                _ => "points"
            };
        }
    }
}
