﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    public class SubpassAttachmentsWapper
    {
        public List<string> ColorAttachments { get; set; } = new();

        public List<string> InputAttachments { get; set; } = new();

        public string ResolveAttachment { get; set; } = string.Empty;

        public string DepthAttachment { get; set; } = string.Empty;

        public string PipelineBindPoint { get; set; } = "PipelineBindPoint.Graphics";
    }
}
