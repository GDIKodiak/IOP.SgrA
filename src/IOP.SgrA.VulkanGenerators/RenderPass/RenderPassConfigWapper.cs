﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    public class RenderPassConfigWapper
    {
        public string Name { get; set; } = string.Empty;
        public string RenderPassMode { get; set; } = "0";
        public string Layers { get; set; } = "1";
        public string FrameBufferCount { get; set; } = "1";
    }
}
