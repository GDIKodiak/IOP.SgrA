﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    public class AttachmentClearValueWrapper
    {
        public string Float0 { get; set; } = "0.0f";
        public string Float1 { get; set; } = "0.0f";
        public string Float2 { get; set; } = "0.0f";
        public string Float3 { get; set; } = "0.0f";
        public string Depth { get; set; } = "0.0f";
        public string Stencil { get; set; } = "0";
        public string Output { get; set; } = string.Empty;
    }
}
