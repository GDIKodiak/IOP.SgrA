﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    public class AttachmentWrapper
    {
        public string FinalLayout = "ImageLayout.Undefined";
        public string Format = "Format.R8G8B8A8Unorm";
        public string InitialLayout = "ImageLayout.Undefined";
        public string LoadOp = "AttachmentLoadOp.Clear";
        public string StoreOp = "AttachmentStoreOp.Store";
        public string StencilLoadOp = "AttachmentLoadOp.DontCare";
        public string StencilStoreOp = "AttachmentStoreOp.DontCare";
        public string Samples = "SampleCountFlags.Count1Bit";

        public ImageConfigWapper ImageConfig { get; set; } = new ImageConfigWapper();
        public AttachmentClearValueWrapper ClearValue { get; set; } = new AttachmentClearValueWrapper();

        public AttachmentWrapper() { }
    }
}
