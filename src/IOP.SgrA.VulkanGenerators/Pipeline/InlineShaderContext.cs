﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    internal class InlineShaderContext
    {
        public string ShaderVersion { get; set; } = "460";
        /// <summary>
        /// 
        /// </summary>
        public List<string> ShaderExtensions { get; private set; } = new List<string>();
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, IShaderField> InlineFileds { get; set; } = new Dictionary<string, IShaderField>();
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, InlineShaderMethod> InlineMethods { get; } = new Dictionary<string, InlineShaderMethod>();
    }
}
