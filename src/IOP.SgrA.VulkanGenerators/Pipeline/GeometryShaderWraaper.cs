﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    public class GeometryShaderWraaper
    {
        /// <summary>
        /// 
        /// </summary>
        public List<DescriptorSetLayoutWrapper> DescriptorSetLayouts { get; internal set; } = new List<DescriptorSetLayoutWrapper>();
        /// <summary>
        /// 
        /// </summary>
        public List<PushConstantRangeWrapper> PushConstants { get; internal set; } = new List<PushConstantRangeWrapper>();

        public GeometryVertexInputWrapper VertexInput { get; internal set; } = new GeometryVertexInputWrapper();

        public GeometryVertexOutputWrapper VertexOutput { get; internal set; } = new GeometryVertexOutputWrapper();

        /// <summary>
        /// 
        /// </summary>
        internal InlineShaderContext ShaderContext { get; set; } = new InlineShaderContext();
    }
}
