﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    /// <summary>
    /// 
    /// </summary>
    public class DescriptorSetLayoutWrapper
    {

        public List<DescriptorSetLayoutBindingWrapper> Bindings { get; } = new List<DescriptorSetLayoutBindingWrapper>();

        public string DescriptorSetTarget = "0";

        public string SetIndex = "0";
    }
}
