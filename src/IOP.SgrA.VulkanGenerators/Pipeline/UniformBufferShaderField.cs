﻿using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    /// <summary>
    /// 
    /// </summary>
    public class UniformBufferShaderField : ShaderField
    {
        /// <summary>
        /// 
        /// </summary>
        public string SetIndex {  get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public string Binding { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public string DataLayout {  get; private set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="setIndex"></param>
        /// <param name="binding"></param>
        /// <param name="dataLayout"></param>
        /// <param name="type"></param>
        public UniformBufferShaderField(string setIndex, string binding, string dataLayout, ITypeSymbol type, string name)
        {
            SetIndex = setIndex;
            Binding = binding;
            DataLayout = dataLayout;
            Type = type;
            Name = name;
        }

        public override void WriteToCode(StringBuilder builder)
        {
            builder.AppendLine($"uniform layout(set = {SetIndex}, binding = {Binding}, {DataLayout}) {Type.Name}");
            builder.AppendLine("{");
            WriteUniformStructString(Type, builder, "\t");
            builder.AppendLine($"}} {Name};");
        }
    }
}
