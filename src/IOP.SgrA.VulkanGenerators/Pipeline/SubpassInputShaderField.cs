﻿using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    public class SubpassInputShaderField : ShaderField
    {
        public string SetIndex {  get; set; }
        public string AttachmentIndex {  get; set; }
        public string Binding {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="typeInfo"></param>
        /// <param name="name"></param>
        public SubpassInputShaderField(ITypeSymbol typeInfo, string name, string setIndex, string attachmentIndex, string binding)
        {
            Type = typeInfo;
            Name = name;
            SetIndex = setIndex;
            AttachmentIndex = attachmentIndex;
            Binding = binding;
        }

        public override void WriteToCode(StringBuilder builder)
        {
            builder.AppendLine($"uniform layout(set = {SetIndex}, input_attachment_index = {AttachmentIndex}, binding = {Binding}) subpassInput {Name};");
        }
    }
}
