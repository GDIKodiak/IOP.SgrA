﻿using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    public class InputShaderField : ShaderField
    {
        public string Location { get; private set; }

        public bool IsFlat {  get; private set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="location"></param>
        public InputShaderField(string location, ITypeSymbol type, string name)
        {
            Location = location;
            Type = type;
            Name = name;
        }

        public override void WriteToCode(StringBuilder builder)
        {
            string typeName = GetGLSLTypeName(Type, out bool flat);
            string f = flat ? "flat " : "";
            builder.AppendLine($"layout(location = {Location}) {f}in {typeName} {Name};");
        }
    }
}
