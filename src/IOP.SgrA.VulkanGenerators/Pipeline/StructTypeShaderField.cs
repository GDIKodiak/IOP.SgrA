﻿using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    /// <summary>
    /// 
    /// </summary>
    public class StructTypeShaderField : ShaderField
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        public StructTypeShaderField(ITypeSymbol type)
        {
            Type = type;
            Name = type.Name;
        }

        public override bool CheckShaderField(GeneratorExecutionContext context)
        {
            var symbol = Type;
            foreach (var item in symbol.GetMembers().Where(x => x.Kind == SymbolKind.Property 
                || x.Kind == SymbolKind.Field))
            {
                if (item is IPropertySymbol property)
                {
                    var type = property.Type;
                    if (type.Kind == SymbolKind.ArrayType)
                    {
                        var location = type.Locations.FirstOrDefault() ?? Location.None;
                        context.ReportDiagnostic(Diagnostic.Create(Descriptors.InitArrayLengthLost, location, property.Name));
                        return false;
                    }
                    else if (ScriptedPipelineGenerator.IsGLSLBaseType(type)) continue;
                    else if (type.TypeKind == TypeKind.Struct) continue;
                    else
                    {
                        var location = type.Locations.FirstOrDefault() ?? Location.None;
                        context.ReportDiagnostic(Diagnostic.Create(Descriptors.NotGLSLDataType, location, type.Name));
                        return false;
                    }
                }
                else if (item is IFieldSymbol field)
                {
                    if (field.IsImplicitlyDeclared) continue;
                    var type = field.Type;
                    if (type.Kind == SymbolKind.ArrayType)
                    {
                        var location = type.Locations.FirstOrDefault() ?? Location.None;
                        context.ReportDiagnostic(Diagnostic.Create(Descriptors.InitArrayLengthLost, location, field.Name));
                        return false;
                    }
                    else if (ScriptedPipelineGenerator.IsGLSLBaseType(type)) continue;
                    else if (type.TypeKind == TypeKind.Struct) continue;
                    else
                    {
                        var location = type.Locations.FirstOrDefault() ?? Location.None;
                        context.ReportDiagnostic(Diagnostic.Create(Descriptors.NotGLSLDataType, location, type.Name));
                        return false;
                    }
                }
            }
            return true;
        }

        public override void WriteToCode(StringBuilder builder)
        {
            var symbol = Type;
            builder.AppendLine($"struct {Type.Name}");
            builder.AppendLine("{");
            foreach (var item in symbol.GetMembers().Where(x => x.Kind == SymbolKind.Property 
                || x.Kind == SymbolKind.Field))
            {
                if(item is IFieldSymbol field)
                {
                    if(item.IsImplicitlyDeclared) continue;
                    var fType = field.Type;
                    builder.AppendLine($"\t{GetGLSLTypeName(fType, out _)} {field.Name};");
                }
                else if(item is IPropertySymbol property)
                {
                    var pType = property.Type;
                    builder.AppendLine($"\t{GetGLSLTypeName(pType, out _)} {property.Name};");
                }
            }
            builder.AppendLine("};");
        }
    }
}
