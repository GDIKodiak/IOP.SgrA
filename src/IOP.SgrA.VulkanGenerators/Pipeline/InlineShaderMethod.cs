﻿using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    internal class InlineShaderMethod
    {
        internal IMethodSymbol Method;
        internal StringBuilder CodeBuilder;
        internal bool Parsed;
        internal List<InlineShaderMethod> Childrens { get; } = new List<InlineShaderMethod>();

        internal InlineShaderMethod(IMethodSymbol method, StringBuilder codeBuilder)
        {
            Method = method;
            CodeBuilder = codeBuilder;
            Parsed = false;
        }
    }
}
