﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    public class DescriptorSetLayoutBindingWrapper
    {
        public string Binding { get; set; } = "0";
        public string DescriptorCount { get; set; } = "1";
        public string DescriptorType { get; set; } = "";
        public string StageFlags { get; set; } = "";
        public string DataLayout { get; set; } = "std140";
        public string StorageFormat { get; set; } = "0";
        public string AttachmentIndex { get; set; } = "0";
    }
}
