﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    public class ColorBlendWrapper
    {
        public string LogicOpEnable { get; set; } = "false";
        public string LogicOp { get; set; } = "LogicOp.NoOp";
        public string BlendConstants { get; set; } = "(1.0f, 1.0f, 1.0f, 1.0f)";

        public List<FragOutputWrapper> FragOutputs { get; private set; } = new List<FragOutputWrapper>();
    }
}
