﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    public class VertexAttributeWrapper
    {
        public uint Location { get; set; } = 0;
        public string Format { get; set; } = "Format.Undefined";
        public uint Offset { get; set; } = 0;
        public uint Size { get; set; } = 0;
        public string Binding { get; set; } = "0";
    }
}
