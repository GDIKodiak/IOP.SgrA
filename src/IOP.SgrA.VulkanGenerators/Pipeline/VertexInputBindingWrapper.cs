﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    public class VertexInputBindingWrapper
    {
        public uint Binding { get; set; } = 0;
        public uint Stride { get; set; } = 0;
        public string InputRate { get; set; } = "0";

        public List<VertexAttributeWrapper> InputAttributes { get; set; } = new List<VertexAttributeWrapper>();
    }
}
