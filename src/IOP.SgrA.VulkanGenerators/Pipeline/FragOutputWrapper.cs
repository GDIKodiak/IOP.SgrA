﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    /// <summary>
    /// 
    /// </summary>
    public class FragOutputWrapper
    {
        public uint Location {  get; set; }
        public bool BlendEnable { get; set; } = false;
        public string AlphaBlendOp { get; set; } = "BlendOp.Add";
        public string ColorBlendOp { get; set; } = "BlendOp.Add";
        public string SrcColorBlendFactor { get; set; } = "BlendFactor.Zero";
        public string DstColorBlendFactor { get; set; } = "BlendFactor.Zero";
        public string SrcAlphaBlendFactor { get; set; } = "BlendFactor.Zero";
        public string DstAlphaBlendFactor { get; set; } = "BlendFactor.Zero";
        public string ColorWriteMask { get; set; } = "(ColorComponentFlags)15";
    }
}
