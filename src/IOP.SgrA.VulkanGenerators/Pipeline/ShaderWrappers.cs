﻿using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    public class ShaderWrappers
    {
        /// <summary>
        /// 
        /// </summary>
        public VertexShaderWrapper VertexShaderWrapper { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public FragmentShaderWrapper FragmentShaderWrapper { get; set; }
        public GeometryShaderWraaper GeometryShaderWraaper { get; set; } = null;
        /// <summary>
        /// 
        /// </summary>
        public List<IPropertySymbol> Shaders { get; } = new List<IPropertySymbol>();
    }
}
