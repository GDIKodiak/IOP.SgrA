﻿using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    public class StorageBufferShaderField : ShaderField
    {
        /// <summary>
        /// 
        /// </summary>
        public string SetIndex {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Binding { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DataLayout { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="setIndex"></param>
        /// <param name="binding"></param>
        /// <param name="dataLayout"></param>
        /// <param name="type"></param>
        public StorageBufferShaderField(string setIndex, string binding, string dataLayout, ITypeSymbol type, string name)
        {
            SetIndex = setIndex;
            Binding = binding;
            DataLayout = dataLayout;
            Type = type;
            Name = name;
        }

        public override void WriteToCode(StringBuilder builder)
        {
            builder.AppendLine($"buffer layout(set = {SetIndex}, binding = {Binding}, {DataLayout}) {Type.Name}");
            builder.AppendLine("{");
            WriteUniformStructString(Type, builder, "\t");
            builder.AppendLine($"}} {Name};");
        }
    }
}
