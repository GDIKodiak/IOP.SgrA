﻿using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    /// <summary>
    /// 
    /// </summary>
    public class OutputShaderField : ShaderField
    {
        /// <summary>
        /// 
        /// </summary>
        public string Location { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="location"></param>
        public OutputShaderField(string location, ITypeSymbol type, string name)
        {
            Location = location;
            Type = type;
            Name = name;
        }

        public override void WriteToCode(StringBuilder builder)
        {
            builder.AppendLine($"layout(location = {Location}) out {GetGLSLTypeName(Type, out _)} {Name};");
        }
    }
}
