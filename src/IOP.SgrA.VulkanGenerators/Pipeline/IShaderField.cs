﻿using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    /// <summary>
    /// 
    /// </summary>
    public interface IShaderField
    {
        /// <summary>
        /// 
        /// </summary>
        bool Skip { get; }
        /// <summary>
        /// 
        /// </summary>
        string Name { get; }
        /// <summary>
        /// 
        /// </summary>
        ITypeSymbol Type { get; }
        /// <summary>
        /// 
        /// </summary>
        List<IShaderField> Childrens { get; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        void WriteToCode(StringBuilder builder);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        bool CheckShaderField(GeneratorExecutionContext context);
    }
}
