﻿using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    public class ShaderField : IShaderField
    {
        /// <summary>
        /// 跳过此字段
        /// </summary>
        public bool Skip { get; internal set; }
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; protected internal set; }
        /// <summary>
        /// 
        /// </summary>
        public ITypeSymbol Type { get; protected set; }
        /// <summary>
        /// 
        /// </summary>
        public List<IShaderField> Childrens { get; private set; } = new List<IShaderField>();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="builder"></param>
        /// <returns></returns>
        public virtual void WriteToCode(StringBuilder builder) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public virtual bool CheckShaderField(GeneratorExecutionContext context) { return true; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="symbol"></param>
        /// <param name="builder"></param>
        /// <param name="tabs"></param>
        protected void WriteUniformStructString(ITypeSymbol symbol, StringBuilder builder, string tabs)
        {
            foreach(var item in symbol.GetMembers().Where(x => x.Kind == SymbolKind.Property || 
                x.Kind == SymbolKind.Field))
            {
                if(item is IPropertySymbol property)
                {
                    var type = property.Type;
                    if (type.Kind == SymbolKind.ArrayType)
                    {
                        var ft = type as IArrayTypeSymbol;
                        builder.AppendLine($"{tabs}{GetGLSLTypeName(ft.ElementType, out _)}[] {property.Name};");
                    }
                    else builder.AppendLine($"{tabs}{GetGLSLTypeName(property.Type, out _)} {property.Name};");
                }
                else if(item is IFieldSymbol field)
                {
                    if (field.IsImplicitlyDeclared) continue;
                    var type = field.Type;
                    if (type.Kind == SymbolKind.ArrayType)
                    {
                        var ft = type as IArrayTypeSymbol;
                        builder.AppendLine($"{tabs}{GetGLSLTypeName(ft.ElementType, out _)}[] {field.Name};");
                    }
                    else builder.AppendLine($"{tabs}{GetGLSLTypeName(field.Type, out _)} {field.Name};");
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="typeSymbol"></param>
        /// <param name="typeName"></param>
        /// <returns></returns>
        protected string GetGLSLTypeName(ITypeSymbol type, out bool flat)
        {
            flat = false;
            if (type.TypeKind == TypeKind.Struct)
            {
                switch (type.SpecialType)
                {
                    case SpecialType.None:
                        return type.Name;
                    case SpecialType.System_Int32:
                        flat = true;
                        return "int";
                    case SpecialType.System_UInt32:
                        flat = true;
                        return "uint";
                    case SpecialType.System_Boolean:
                        return "bool";
                    case SpecialType.System_Single:
                        return "float";
                    case SpecialType.System_Double:
                        return "double";
                    default:
                        return type.Name;
                }
            }
            else if(type.TypeKind == TypeKind.Array)
            {
                var arrayType = type as IArrayTypeSymbol;
                var elementType = arrayType.ElementType;
                return $"{GetGLSLTypeName(elementType, out _)}[]";
            }
            else return type.Name;
        }
    }
}
