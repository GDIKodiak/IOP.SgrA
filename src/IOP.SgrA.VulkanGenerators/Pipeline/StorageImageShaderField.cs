﻿using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    public class StorageImageShaderField : ShaderField
    {
        /// <summary>
        /// 
        /// </summary>
        public string SetIndex { get; private set; }
        public string Binding {  get; private set; }
        public string Format {  get; private set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="setIndex"></param>
        /// <param name="binding"></param>
        /// <param name="format"></param>
        /// <param name="type"></param>
        /// <param name="name"></param>
        public StorageImageShaderField(string setIndex, string binding, string format, ITypeSymbol type, string name)
        {
            SetIndex = setIndex;
            Binding = binding;
            Format = format;
            Type = type;
            Name = name;
        }

        public override void WriteToCode(StringBuilder builder)
        {
            builder.AppendLine($"uniform layout(set = {SetIndex}, binding = {Binding}, {Format}) {Type.Name} {Name};");
        }
    }
}
