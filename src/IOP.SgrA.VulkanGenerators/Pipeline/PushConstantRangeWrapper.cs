﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    public class PushConstantRangeWrapper
    {
        public string StageFlags { get; set; } = "ShaderStageFlags.VertexBit";
        public string Offset {  get; set; }
        public string Size { get; set; }    
    }
}
