﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    public class FragmentShaderWrapper
    {
        /// <summary>
        /// 
        /// </summary>
        public List<DescriptorSetLayoutWrapper> DescriptorSetLayouts { get; internal set; } = new List<DescriptorSetLayoutWrapper>();
        /// <summary>
        /// 
        /// </summary>
        public List<PushConstantRangeWrapper> PushConstants { get; internal set; } = new List<PushConstantRangeWrapper>();
        /// <summary>
        /// 
        /// </summary>
        public ColorBlendWrapper ColorBlend { get; internal set; } = new ColorBlendWrapper();

        /// <summary>
        /// 
        /// </summary>
        internal InlineShaderContext ShaderContext { get; set; } = new InlineShaderContext();
    }
}
