﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    public class VertexShaderWrapper
    {
        /// <summary>
        /// 
        /// </summary>
        public List<DescriptorSetLayoutWrapper> DescriptorSetLayouts { get; internal set; } = new List<DescriptorSetLayoutWrapper>();
        /// <summary>
        /// 
        /// </summary>
        public List<PushConstantRangeWrapper> PushConstants { get; internal set; } = new List<PushConstantRangeWrapper>();
        /// <summary>
        /// 
        /// </summary>
        public List<VertexInputBindingWrapper> VertexInputs { get; internal set; } = new List<VertexInputBindingWrapper>();

        /// <summary>
        /// 
        /// </summary>
        internal InlineShaderContext ShaderContext { get; set; } = new InlineShaderContext();
    }
}
