﻿using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    /// <summary>
    /// 
    /// </summary>
    public class PushConstantShaderField : ShaderField
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="typeInfo"></param>
        /// <param name="name"></param>
        public PushConstantShaderField(ITypeSymbol typeInfo, string name)
        {
            Type = typeInfo;
            Name = name;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        public override void WriteToCode(StringBuilder builder)
        {
            builder.AppendLine($"uniform layout(push_constant) {Type.Name}");
            builder.AppendLine("{");
            WriteUniformStructString(Type, builder, "\t");
            builder.AppendLine($"}} {Name};");
        }
    }
}
