﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    public class PipelineShaderStageWrapper
    {
        public string Name { get; set; } = "main";
        public string Stage { get; set; } = "ShaderStageFlags.VertexBit";
    }
}
