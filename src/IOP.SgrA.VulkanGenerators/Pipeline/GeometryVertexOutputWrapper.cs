﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    public class GeometryVertexOutputWrapper
    {
        public string VertexOutputType {  get; set; }

        public uint MaxVertices { get; set; } = 1;
    }
}
