﻿using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    public class SamplerImageShaderField : ShaderField
    {
        /// <summary>
        /// 
        /// </summary>
        public string SetIndex { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public string Binding { get; private set; }

        public SamplerImageShaderField(string setIndex, string binding, ITypeSymbol type, string name)
        {
            SetIndex = setIndex;
            Binding = binding;
            Type = type;
            Name = name;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        public override void WriteToCode(StringBuilder builder)
        {
            builder.AppendLine($"uniform layout(set = {SetIndex}, binding = {Binding}) {Type.Name} {Name};");
        }
    }
}
