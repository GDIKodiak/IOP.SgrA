﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    public class NormalShaderField : ShaderField
    {
        public string Code { get; internal set; } = string.Empty;

        public override void WriteToCode(StringBuilder builder)
        {
            builder.Append(Code);
        }
    }
}
