﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.SgrA.VulkanGenerators
{
    public class GeometryVertexInputWrapper
    {
        public string VertexInputType { get; set; } = "points";
    }
}
