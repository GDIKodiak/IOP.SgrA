﻿using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using System.Linq;
using Microsoft.CodeAnalysis.Operations;
using System.Xml.Linq;

namespace IOP.SgrA.VulkanGenerators
{
    public partial class ScriptedPipelineGenerator
    {
        /// <summary>
        /// 是否为GLSL基础类型
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        internal static bool IsGLSLBaseType(ITypeSymbol type)
        {
            switch (type.SpecialType)
            {
                case SpecialType.None:
                    break;
                case SpecialType.System_Int32:
                case SpecialType.System_UInt32:
                case SpecialType.System_Boolean:
                case SpecialType.System_Single:
                case SpecialType.System_Double:
                    return true;
                default:
                    return false;
            }
            if (type.TypeKind == TypeKind.Struct)
            {
                switch (type.Name)
                {
                    case "bvec2":
                    case "bvec3":
                    case "bvec4":
                    case "dvec2":
                    case "dvec3":
                    case "dvec4":
                    case "ivec2":
                    case "ivec3":
                    case "ivec4":
                    case "mat2":
                    case "mat2x2":
                    case "mat2x3":
                    case "mat2x4":
                    case "mat3":
                    case "mat3x2":
                    case "mat3x3":
                    case "mat3x4":
                    case "mat4":
                    case "mat4x2":
                    case "mat4x3":
                    case "mat4x4":
                    case "dmat2":
                    case "dmat2x2":
                    case "dmat2x3":
                    case "dmat2x4":
                    case "dmat3":
                    case "dmat3x2":
                    case "dmat3x3":
                    case "dmat3x4":
                    case "dmat4":
                    case "dmat4x2":
                    case "dmat4x3":
                    case "dmat4x4":
                    case "uvec2":
                    case "uvec3":
                    case "uvec4":
                    case "vec2":
                    case "vec3":
                    case "vec4":
                        return true;
                    default:
                        return false;
                }
            }
            return false;
        }
        /// <summary>
        /// 获取类型名称
        /// </summary>
        /// <param name="context"></param>
        /// <param name="type"></param>
        /// <param name="location"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        private bool GetGLSLTypeName(GeneratorExecutionContext context, ITypeSymbol type, Location location, out string name)
        {
            if (location == null) location = Location.None;
            name = string.Empty;
            if(type.TypeKind == TypeKind.Struct)
            {
                switch (type.SpecialType)
                {
                    case SpecialType.None:
                        name = type.Name;
                        return true;
                    case SpecialType.System_Int32:
                        name = "int";
                        return true;
                    case SpecialType.System_UInt32:
                        name = "uint";
                        return true;
                    case SpecialType.System_Boolean:
                        name = "bool";
                        return true;
                    case SpecialType.System_Single:
                        name = "float";
                        return true;
                    case SpecialType.System_Double:
                        name = "double";
                        return true;
                    case SpecialType.System_Void:
                        name = "void";
                        return true;
                    default:
                        context.ReportDiagnostic(Diagnostic.Create(Descriptors.NotGLSLDataType, location, type.Name));
                        return false;
                }
            }
            else
            {
                context.ReportDiagnostic(Diagnostic.Create(Descriptors.NotGLSLDataType, location, type.Name));
                return false;
            }
        }
        /// <summary>
        /// 检查类型是否符合GLSL编译要求
        /// </summary>
        /// <param name="context"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private bool CheckStruct(GeneratorExecutionContext context, ITypeSymbol type)
        {
            if (IsGLSLBaseType(type)) return true;
            var result = true;
            if (type.TypeKind == TypeKind.Struct && type.SpecialType == SpecialType.None)
            {
                foreach (var item in type.GetMembers().Where(x => x.Kind == SymbolKind.Property | x.Kind == SymbolKind.Field))
                {
                    if (item is IPropertySymbol prop)
                    {
                        var pT = prop.Type;
                        if (pT.TypeKind == TypeKind.Array)
                        {
                            if (pT is not IArrayTypeSymbol apT)
                            {
                                result = false;
                                break;
                            }
                            var bType = apT.ElementType;
                            result = CheckStruct(context, bType);
                            if (!result) break;
                        }
                        else if (pT.TypeKind == TypeKind.Struct)
                        {
                            result = CheckStruct(context, pT);
                            if (!result) break;
                        }
                        else
                        {
                            var location = prop.Locations.FirstOrDefault() ?? Location.None;
                            context.ReportDiagnostic(Diagnostic.Create(Descriptors.NotGLSLDataType, location, prop.Name));
                            result = false;
                            break;
                        }
                    }
                    else if (item is IFieldSymbol field)
                    {
                        if (field.IsImplicitlyDeclared) continue;
                        var fT = field.Type;
                        if (fT.TypeKind == TypeKind.Array)
                        {
                            if (fT is not IArrayTypeSymbol apT)
                            {
                                result = false;
                                break;
                            }
                            var bType = apT.ElementType;
                            result = CheckStruct(context, bType);
                            if (!result) break;
                        }
                        else if (fT.TypeKind == TypeKind.Struct)
                        {
                            result = CheckStruct(context, fT);
                            if (!result) break;
                        }
                        else
                        {
                            var location = field.Locations.FirstOrDefault() ?? Location.None;
                            context.ReportDiagnostic(Diagnostic.Create(Descriptors.NotGLSLDataType, location, field.Name));
                            result = false;
                            break;
                        }
                    }
                    else continue;
                }
            }
            else
            {
                var location = type.Locations.FirstOrDefault() ?? Location.None;
                context.ReportDiagnostic(Diagnostic.Create(Descriptors.NotGLSLDataType, location, type.Name));
                result = false;
            }
            return result;
        }

        #region Parameters
        /// <summary>
        /// 获取方法参数代码
        /// </summary>
        /// <param name="context"></param>
        /// <param name="type"></param>
        /// <param name="location"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        private bool GetGLSLMethodParameterCode(GeneratorExecutionContext context, IParameterSymbol param, out string code)
        {
            var location = param.Locations.Length > 0 ? param.Locations[0] : Location.None;
            code = string.Empty;
            string inOut = string.Empty;
            var type = param.Type;
            bool result = GetGLSLTypeName(context, type, location, out string pType);
            if (!result) return result;
            if (param.HasExplicitDefaultValue)
            {
                context.ReportDiagnostic(Diagnostic.Create(Descriptors.NoSupportDefaultValueForCustomMethodParameter, location));
                return result;
            }
            int inOutCode = HasInOutAttribute(param);
            if (inOutCode == 1) 
                inOut = "out ";
            else if (inOutCode == 2) 
                inOut = "in ";
            else if (inOutCode == 3) 
                inOut = "inout ";
            code = $"{inOut}{pType} {param.Name}";
            result = true;
            return result;
        }
        /// <summary>
        /// 是否存在InAttribute, OutAttribute
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        private int HasInOutAttribute(IParameterSymbol param)
        {
            int result = 0;
            var attrs = param.GetAttributes();
            if(attrs.Length > 0)
            {
                foreach(var attr in attrs)
                {
                    if(attr.AttributeClass != null)
                    {
                        var space = attr.AttributeClass.ContainingNamespace;
                        var name = attr.AttributeClass.Name;
                        if(space.Name == "InteropServices")
                        {
                            if (name == "InAttribute") result |= 2;
                            else if (name == "OutAttribute") result |= 1;
                        }
                    }
                }
            }
            return result;
        }
        private bool NewFieldCode(GeneratorExecutionContext context, IFieldSymbol target, Location location, out string code)
        {
            string fieldCode = string.Empty;
            if (target.IsConst) fieldCode += "const ";
            bool result = GetGLSLTypeName(context, target.Type, location, out string name);
            if (result) fieldCode += $"{name} {target.Name};";
            code = fieldCode;
            return result;
        }
        private bool NewPropertyCode(GeneratorExecutionContext context, IPropertySymbol target, Location location, out string code)
        {
            string fieldCode = string.Empty;
            bool result = GetGLSLTypeName(context, target.Type, location, out string name);
            if (result) fieldCode += $"{name} {target.Name};";
            code = fieldCode;
            return result;
        }
        #endregion

        #region Initializer
        /// <summary>
        /// 获取初始化代码
        /// </summary>
        /// <param name="context"></param>
        /// <param name="initOp"></param>
        /// <param name="initType"></param>
        /// <param name="inlineShader"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        private bool GetInitializerCode(GeneratorExecutionContext context, InlineShaderMethod caller, IOperation initOp, 
            ITypeSymbol initType, InlineShaderContext inlineShader, out string code)
        {
            bool result = GetOperationCode(context, caller, initOp, inlineShader, out code);
            return result;
        }
        /// <summary>
        /// 获取GLSL基础类型默认初始化器代码
        /// </summary>
        /// <param name="context"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private bool GetGLSLBaseTypeDefaultInitializerCode(GeneratorExecutionContext context,
            Location location, ITypeSymbol type, out string code)
        {
            if (location == null) location = Location.None;
            code = string.Empty;
            bool result = false; ;
            switch (type.SpecialType)
            {
                case SpecialType.None:
                    break;
                case SpecialType.System_Int32:
                case SpecialType.System_UInt32:
                    code = "0";
                    result = true;
                    break;
                case SpecialType.System_Boolean:
                    result = true;
                    code = "false";
                    break;
                case SpecialType.System_Single:
                case SpecialType.System_Double:
                    result = true;
                    code = "0.0";
                    break;
                default:
                    context.ReportDiagnostic(Diagnostic.Create(Descriptors.NotGLSLDataType, location, type.Name));
                    return false;
            }
            if (result) return result;
            if (type.TypeKind == TypeKind.Struct)
            {
                switch (type.Name)
                {
                    case "bvec2":
                        code = "bvec2(false)";
                        result = true;
                        break;
                    case "bvec3":
                        code = "bvec3(false)";
                        result = true;
                        break;
                    case "bvec4":
                        code = "bvec4(false)";
                        result = true;
                        break;
                    case "dvec2":
                        code = "dvec2(0.0)";
                        result = true;
                        break;
                    case "dvec3":
                        code = "dvec3(0.0)";
                        result = true;
                        break;
                    case "dvec4":
                        code = "dvec4(0.0)";
                        result = true;
                        break;
                    case "ivec2":
                        code = "ivec2(0)";
                        result = true;
                        break;
                    case "ivec3":
                        code = "ivec3(0)";
                        result = true;
                        break;
                    case "ivec4":
                        code = "ivec4(0)";
                        result = true;
                        break;
                    case "mat2":
                        code = "mat2(0.0)";
                        result = true;
                        break;
                    case "mat2x2":
                        code = "mat2x2(0.0)";
                        result = true;
                        break;
                    case "mat2x3":
                        code = "mat2x3(0.0)";
                        result = true;
                        break;
                    case "mat2x4":
                        code = "mat2x4(0.0)";
                        result = true;
                        break;
                    case "mat3":
                        code = "mat3(0.0)";
                        result = true;
                        break;
                    case "mat3x2":
                        code = "mat3x2(0.0)";
                        result = true;
                        break;
                    case "mat3x3":
                        code = "mat3x3(0.0)";
                        result = true;
                        break;
                    case "mat3x4":
                        code = "mat3x4(0.0)";
                        result = true;
                        break;
                    case "mat4":
                        code = "mat4(0.0)";
                        result = true;
                        break;
                    case "mat4x2":
                        code = "mat4x2(0.0)";
                        result = true;
                        break;
                    case "mat4x3":
                        code = "mat4x3(0.0)";
                        result = true;
                        break;
                    case "mat4x4":
                        code = "mat4x4(0.0)";
                        result = true;
                        break;
                    case "dmat2":
                        code = "dmat2(0.0)";
                        result = true;
                        break;
                    case "dmat2x2":
                        code = "dmat2x2(0.0)";
                        result = true;
                        break;
                    case "dmat2x3":
                        code = "dmat2x3(0.0)";
                        result = true;
                        break;
                    case "dmat2x4":
                        code = "dmat2x4(0.0)";
                        result = true;
                        break;
                    case "dmat3":
                        code = "dmat3(0.0)";
                        result = true;
                        break;
                    case "dmat3x2":
                        code = "dmat3x2(0.0)";
                        result = true;
                        break;
                    case "dmat3x3":
                        code = "dmat3x3(0.0)";
                        result = true;
                        break;
                    case "dmat3x4":
                        code = "dmat3x4(0.0)";
                        result = true;
                        break;
                    case "dmat4":
                        code = "dmat4(0.0)";
                        result = true;
                        break;
                    case "dmat4x2":
                        code = "dmat4x2(0.0)";
                        result = true;
                        break;
                    case "dmat4x3":
                        code = "dmat4x3(0.0)";
                        result = true;
                        break;
                    case "dmat4x4":
                        code = "dmat4x4(0.0)";
                        result = true;
                        break;
                    case "uvec2":
                        code = "uvec2(0)";
                        result = true;
                        break;
                    case "uvec3":
                        code = "uvec3(0)";
                        result = true;
                        break;
                    case "uvec4":
                        code = "uvec4(0)";
                        result = true;
                        break;
                    case "vec2":
                        code = "vec2(0)";
                        result = true;
                        break;
                    case "vec3":
                        code = "vec3(0)";
                        result = true;
                        break;
                    case "vec4":
                        code = "vec4(0)";
                        result = true;
                        break;
                    default:
                        context.ReportDiagnostic(Diagnostic.Create(Descriptors.NotGLSLDataType, location, type.Name));
                        return false;
                }
            }
            else
            {
                context.ReportDiagnostic(Diagnostic.Create(Descriptors.NotGLSLDataType, location, type.Name));
                return false;
            }
            return result;
        }
        /// <summary>
        /// 获取结构体默认初始化器代码
        /// </summary>
        /// <param name="context"></param>
        /// <param name="objCreate"></param>
        /// <param name="type"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        private bool GetStructTypeDefaultInitializerCode(GeneratorExecutionContext context,
            Location location, ITypeSymbol type, out string code)
        {
            if (location == null) location = Location.None;
            bool result = false;
            code = string.Empty;
            if(IsGLSLBaseType(type))
            {
                result = GetGLSLBaseTypeDefaultInitializerCode(context, location, type, out code);
            }
            else
            {
                if(type.TypeKind == TypeKind.Struct && type.SpecialType == SpecialType.None)
                {
                    code += $"{type.Name}(";
                    string member = string.Empty;
                    foreach (var item in type.GetMembers().Where(x => x.Kind == SymbolKind.Property | x.Kind == SymbolKind.Field))
                    {
                        if (item is IPropertySymbol prop)
                        {
                            var pT = prop.Type;
                            if (pT.TypeKind == TypeKind.Array)
                            {
                                context.ReportDiagnostic(Diagnostic.Create(Descriptors.InitArrayLengthLost, location, prop.Name));
                            }
                            else if (pT.TypeKind == TypeKind.Struct)
                            {
                                result = GetStructTypeDefaultInitializerCode(context, location, pT, out string local);
                                if (!result) break;
                                member += $"{local},";
                            }
                            else
                            {
                                context.ReportDiagnostic(Diagnostic.Create(Descriptors.NotGLSLDataType, location, prop.Name));
                            }
                        }
                        else if (item is IFieldSymbol field)
                        {
                            var fT = field.Type;
                            if (fT.TypeKind == TypeKind.Array)
                            {
                                context.ReportDiagnostic(Diagnostic.Create(Descriptors.InitArrayLengthLost, location, fT.Name));
                            }
                            else if (fT.TypeKind == TypeKind.Struct)
                            {
                                result = GetStructTypeDefaultInitializerCode(context, location, fT, out string local);
                                if (!result) break;
                                member += $"{local},";
                            }
                            else
                            {
                                context.ReportDiagnostic(Diagnostic.Create(Descriptors.NotGLSLDataType, location, fT.Name));
                            }
                        }
                        else continue;
                    }
                    member = member.TrimEnd(',');
                    code += member;
                    code += ")";
                }
                else
                {
                    context.ReportDiagnostic(Diagnostic.Create(Descriptors.NotGLSLDataType, location, type.Name));
                }
            }
            return result;
        }
        #endregion

        #region GetOperationCode
        /// <summary>
        /// 获取参数代码
        /// </summary>
        /// <param name="context"></param>
        /// <param name="argument"></param>
        /// <param name="inlineShader"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        private bool GetArgumentCode(GeneratorExecutionContext context, InlineShaderMethod caller,
            IArgumentOperation argument, InlineShaderContext inlineShader, out string code)
        {
            var v = argument.Value;
            bool result = GetOperationCode(context, caller, v, inlineShader, out code);
            return result;
        }
        /// <summary>
        /// 获取字段代码
        /// </summary>
        /// <param name="context"></param>
        /// <param name="op"></param>
        /// <param name="shaderInfo"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        private bool GetFieldReferenceCode(GeneratorExecutionContext context, InlineShaderMethod caller,
            IFieldReferenceOperation op, InlineShaderContext shaderInfo, out string code)
        {
            code = op.Field.Name;
            var instance = op.Instance;
            string parent = string.Empty;
            bool result;
            if (instance != null)
            {
                if (instance is IInstanceReferenceOperation)
                {
                    if (!shaderInfo.InlineFileds.ContainsKey(op.Field.Name))
                    {
                        result = AddNewNormalShaderField(context, caller, op.SemanticModel, op.Field, op.Syntax.GetLocation(), shaderInfo);
                        return result;
                    }
                    else return true;
                }
                else
                {
                    result = GetOperationCode(context, caller, instance, shaderInfo, out string nCode);
                    if (result)
                    {
                        if (instance is IBinaryOperation)
                        {
                            nCode = $"({nCode})";
                        }
                        parent = nCode;
                    }
                }
                if (result) code = $"{parent}.{code}";
            }
            else
            {
                if (!shaderInfo.InlineFileds.ContainsKey(op.Field.Name))
                {
                    result = AddNewNormalShaderField(context, caller, op.SemanticModel, op.Field, op.Syntax.GetLocation(), shaderInfo);
                    return result;
                }
                else return true;
            }
            return result;
        }
        /// <summary>
        /// 获取属性代码
        /// </summary>
        /// <param name="context"></param>
        /// <param name="op"></param>
        /// <param name="shaderInfo"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        private bool GetPropertyReferenceCode(GeneratorExecutionContext context, InlineShaderMethod caller,
            IPropertyReferenceOperation op, InlineShaderContext shaderInfo, out string code)
        {
            code = op.Property.Name;
            bool result = false;
            if (code == "this[]")
            {
                code = string.Empty;
                var args = op.Arguments;
                for(int i = 0; i < args.Length; i++)
                {
                    var arg = args[i];
                    result = GetArgumentCode(context, caller, arg, shaderInfo, out string local);
                    if (!result) return result;
                    code += $"[{local}]";
                }
                var instance = op.Instance;
                string parent = string.Empty;
                if (instance != null)
                {
                    if (instance is IInstanceReferenceOperation)
                    {
                        if (!shaderInfo.InlineFileds.ContainsKey(op.Property.Name))
                        {
                            result = AddNewNormalShaderField(context, caller, op.SemanticModel, op.Property, op.Syntax.GetLocation(), shaderInfo);
                            return result;
                        }
                        else return true;
                    }
                    else
                    {
                        result = GetOperationCode(context, caller, instance, shaderInfo, out string nCode);
                        if (result)
                        {
                            if (instance is IBinaryOperation)
                            {
                                nCode = $"({nCode})";
                            }
                            parent = nCode;
                        }
                    }
                    if (result && !string.IsNullOrEmpty(parent)) code = $"{parent}{code}";
                }
            }
            else
            {
                var instance = op.Instance;
                string parent = string.Empty;
                if (instance != null)
                {
                    if (instance is IInstanceReferenceOperation)
                    {
                        if (!shaderInfo.InlineFileds.ContainsKey(op.Property.Name))
                        {
                            result = AddNewNormalShaderField(context, caller, op.SemanticModel, op.Property, op.Syntax.GetLocation(), shaderInfo);
                            return result;
                        }
                        else return true;
                    }
                    else
                    {
                        result = GetOperationCode(context, caller, instance, shaderInfo, out string nCode);
                        if (result)
                        {
                            if (instance is IBinaryOperation)
                            {
                                nCode = $"({nCode})";
                            }
                            parent = nCode;
                        }
                    }
                    if (result && !string.IsNullOrEmpty(parent)) code = $"{parent}.{code}";
                }
                else
                {
                    if (!shaderInfo.InlineFileds.ContainsKey(op.Property.Name))
                    {
                        result = AddNewNormalShaderField(context, caller, op.SemanticModel, op.Property, op.Syntax.GetLocation(), shaderInfo);
                        return result;
                    }
                    else return true;
                }
            }
            return result;
        }
        /// <summary>
        /// 获取数组元素引用代码
        /// </summary>
        /// <param name="context"></param>
        /// <param name="caller"></param>
        /// <param name="op"></param>
        /// <param name="shaderInfo"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        private bool GetArrayElementReferenceCode(GeneratorExecutionContext context, InlineShaderMethod caller,
            IArrayElementReferenceOperation op, InlineShaderContext shaderInfo, out string code)
        {
            code = string.Empty;
            bool result = GetOperationCode(context, caller, op.ArrayReference, shaderInfo, out string target);
            if (!result) return result;
            string[] locals = new string[op.Indices.Length];
            for (int i = 0; i < locals.Length; i++)
            {
                result = GetOperationCode(context, caller, op.Indices[i], shaderInfo, out string ls);
                if (!result) return result;
                locals[i] = $"[{ls}]";
            }
            code += $"{target}{string.Join("", locals)}";
            result = true;
            return result;
        }
        /// <summary>
        /// 获取局部变量定义代码
        /// </summary>
        /// <param name="context"></param>
        /// <param name="op"></param>
        /// <param name="shaderInfo"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        private bool GetLocalReferenceCode(GeneratorExecutionContext context,
            ILocalReferenceOperation op, InlineShaderContext shaderInfo, out string code)
        {
            code = op.Local.Name;
            return true;
        }
        /// <summary>
        /// 获取参数引用代码
        /// </summary>
        /// <param name="context"></param>
        /// <param name="op"></param>
        /// <param name="shaderInfo"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        private bool GetParameterReferenceCode(GeneratorExecutionContext context,
            IParameterReferenceOperation op, InlineShaderContext shaderInfo, out string code)
        {
            code = op.Parameter.Name;
            return true;
        }
        /// <summary>
        /// 获取方法操作代码
        /// </summary>
        /// <param name="context"></param>
        /// <param name="op"></param>
        /// <param name="shaderInfo"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        private bool GetInvocationOperationCode(GeneratorExecutionContext context, InlineShaderMethod caller,
            IInvocationOperation op, InlineShaderContext shaderInfo, out string code)
        {
            code = string.Empty;
            var method = op.TargetMethod;
            bool result;
            if (IsShaderBuildInMethod(method) && method.Name == "discard")
            {
                code = "discard";
                result = true;
            }
            else
            {
                string[] args = new string[op.Arguments.Length];
                for (int i = 0; i < args.Length; i++)
                {
                    var local = op.Arguments[i];
                    result = GetArgumentCode(context, caller, local, shaderInfo, out string arg);
                    if (!result) return result;
                    args[i] = arg;
                }
                code = $"{method.Name}({string.Join(",", args)})";
                var token = CreateMethodToken(method);
                if (!IsShaderBuildInMethod(method))
                {
                    if (!shaderInfo.InlineMethods.TryGetValue(token, out InlineShaderMethod inline))
                    {
                        inline = new InlineShaderMethod(method, new StringBuilder());
                        shaderInfo.InlineMethods.Add(token, inline);
                    }
                    caller.Childrens.Add(inline);
                }
                result = true;
            }
            return result;
        }
        /// <summary>
        /// 解析二元操作符操作
        /// </summary>
        /// <param name="context"></param>
        /// <param name="op"></param>
        /// <param name="shaderInfo"></param>
        /// <param name="shader"></param>
        /// <returns></returns>
        private bool GetBinaryOperationCode(GeneratorExecutionContext context, InlineShaderMethod caller,
            IBinaryOperation op, InlineShaderContext shaderInfo, out string code)
        {
            var location = op.Syntax.GetLocation();
            code = string.Empty;
            bool result;
            string opToken = string.Empty;
            switch (op.OperatorKind)
            {
                case BinaryOperatorKind.Add:
                    opToken = "+";
                    break;
                case BinaryOperatorKind.Subtract:
                    opToken = "-";
                    break;
                case BinaryOperatorKind.Multiply:
                    opToken = "*";
                    break;
                case BinaryOperatorKind.Divide:
                    opToken = "/";
                    break;
                case BinaryOperatorKind.Remainder:
                    opToken = "%";
                    break;
                case BinaryOperatorKind.ConditionalAnd:
                    opToken = "&&";
                    break;
                case BinaryOperatorKind.ConditionalOr:
                    opToken = "||";
                    break;
                case BinaryOperatorKind.Equals:
                    opToken = "==";
                    break;
                case BinaryOperatorKind.NotEquals:
                    opToken = "!=";
                    break;
                case BinaryOperatorKind.LessThan:
                    opToken = "<";
                    break;
                case BinaryOperatorKind.LessThanOrEqual:
                    opToken = "<=";
                    break;
                case BinaryOperatorKind.GreaterThanOrEqual:
                    opToken = ">=";
                    break;
                case BinaryOperatorKind.GreaterThan:
                    opToken = ">";
                    break;
                default:
                    context.ReportDiagnostic(Diagnostic.Create(Descriptors.NoSupportExpressionSyntax, location));
                    break;
            }
            result = GetOperationCode(context, caller, op.LeftOperand, shaderInfo, out string mCode);
            if (!result) return result;
            if (op.LeftOperand is IBinaryOperation or IConversionOperation) mCode = $"({mCode})";
            code = $"{mCode} {opToken} ";
            result = GetOperationCode(context, caller, op.RightOperand, shaderInfo, out mCode);
            if (op.RightOperand is IBinaryOperation or IConversionOperation) mCode = $"({mCode})";
            if (!result) return result;
            code += mCode;
            return result;
        }
        /// <summary>
        /// 获取对象生成操作代码
        /// </summary>
        /// <param name="context"></param>
        /// <param name="op"></param>
        /// <param name="shaderInfo"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        private bool GetObjectCreateOperation(GeneratorExecutionContext context, InlineShaderMethod caller,
            IObjectCreationOperation op, InlineShaderContext shaderInfo, out string code)
        {
            bool result = false;
            code = string.Empty;
            var type = op.Type;
            if (op.Arguments.Length <= 0)
            {
                result = GetStructTypeDefaultInitializerCode(context, op.Syntax.GetLocation(), type, out code);
            }
            if (IsGLSLBaseType(type))
            {
                code = $"{type.Name}(";
                int count = op.Arguments.Length;
                string[] param = new string[count];
                for (int i = 0; i < count; i++)
                {
                    var arg = op.Arguments[i];
                    result = GetArgumentCode(context, caller, arg, shaderInfo, out string local);
                    if (!result) break;
                    param[i] = local;
                }
                code += string.Join(",", param);
                code += ")";
            }
            else
            {
                context.ReportDiagnostic(Diagnostic.Create(Descriptors.CustomStructInitializerNotSupperted, op.Syntax.GetLocation()));
                return false;
            }
            return result;
        }
        /// <summary>
        /// 获取一元操作符
        /// </summary>
        /// <param name="context"></param>
        /// <param name="op"></param>
        /// <param name="shaderInfo"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        private bool GetUnaryOperation(GeneratorExecutionContext context, InlineShaderMethod caller,
            IUnaryOperation op, InlineShaderContext shaderInfo, out string code)
        {
            code = string.Empty;
            bool result;
            if (op.OperatorKind == UnaryOperatorKind.True)
            {
                code = "true";
                result = true;
            }
            else if (op.OperatorKind == UnaryOperatorKind.False)
            {
                code = "false";
                result = true;
            }
            else
            {
                string opCode = string.Empty;
                switch (op.OperatorKind)
                {
                    case UnaryOperatorKind.Minus:
                        opCode = "-";
                        break;
                    case UnaryOperatorKind.Plus:
                        opCode = "+";
                        break;
                    case UnaryOperatorKind.Not:
                        opCode = "!";
                        break;
                    default:
                        context.ReportDiagnostic(Diagnostic.Create(Descriptors.NoSupportExpressionSyntax, op.Syntax.GetLocation()));
                        break;
                }
                result = GetOperationCode(context, caller, op.Operand, shaderInfo, out string c);
                if (result)
                {
                    code = $"{opCode}{c}";
                }
            }
            return result;
        }
        /// <summary>
        /// 获取自增加代码
        /// </summary>
        /// <param name="context"></param>
        /// <param name="op"></param>
        /// <param name="shaderInfo"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        private bool GetIncrementOrDecrementOperation(GeneratorExecutionContext context, InlineShaderMethod caller,
            IIncrementOrDecrementOperation op, InlineShaderContext shaderInfo, out string code)
        {
            var t = op.Target;
            code = string.Empty;
            bool result = GetOperationCode(context, caller, t, shaderInfo, out string tar);
            if (result)
            {
                if (op.Kind == OperationKind.Increment)
                {
                    if (op.IsPostfix) code = $"{tar}++";
                    else code = $"++{tar}";
                }
                else if (op.Kind == OperationKind.Decrement)
                {
                    if (op.IsPostfix) code = $"{tar}--";
                    else code = $"--{tar}";
                }
                else
                {
                    result = false;
                    context.ReportDiagnostic(Diagnostic.Create(Descriptors.NoSupportExpressionSyntax, op.Syntax.GetLocation()));
                }
            }
            return result;
        }
        private bool GetVariableDeclarationGroupOperation(GeneratorExecutionContext context, InlineShaderMethod caller,
            IVariableDeclarationGroupOperation op, InlineShaderContext shaderInfo, out string code, string tabs = "")
        {
            bool hasError = false;
            code = string.Empty;
            bool result;
            foreach (var declaration in op.Declarations)
            {
                if (hasError) break;
                int count = declaration.Declarators.Length;
                if (count <= 0) continue;
                var type = declaration.Declarators[0].Symbol.Type;
                if (type.Kind == SymbolKind.ArrayType)
                {
                    var arrayType = type as IArrayTypeSymbol;
                    result = GetArrayVariableDeclarationOperation(context, caller, declaration,
                        arrayType, shaderInfo, out string f, tabs);
                    if (!result)
                    {
                        hasError = true;
                        break;
                    }
                    code += f;
                }
                else
                {
                    string[] fragments = new string[count];
                    if (!CheckStruct(context, type))
                    {
                        hasError = true;
                        break;
                    }
                    if (!GetGLSLTypeName(context, type, declaration.Syntax.GetLocation(), out string name))
                    {
                        hasError = true;
                        break;
                    }
                    code += $"{tabs}{name} ";
                    for (int i = 0; i < count; i++)
                    {
                        string fragment = string.Empty;
                        var local = declaration.Declarators[i];
                        ILocalSymbol localSymbol = local.Symbol;
                        if (localSymbol.IsConst) fragment += "const ";
                        fragment += localSymbol.Name;
                        if (local.Initializer != null)
                        {
                            var init = local.Initializer;
                            var value = init.Value;
                            fragment += " = ";
                            result = GetInitializerCode(context, caller, value, localSymbol.Type, shaderInfo, out string ll);
                            if (!result)
                            {
                                hasError = true;
                                break;
                            }
                            fragment += ll;
                            fragments[i] = fragment;
                        }
                        else fragments[i] = fragment;
                    }
                    code += $"{string.Join(",", fragments)};";
                }
            }
            if (hasError) result = false;
            else result = true;
            return result;
        }
        private bool GetArrayVariableDeclarationOperation(GeneratorExecutionContext context, InlineShaderMethod caller,
            IVariableDeclarationOperation op, IArrayTypeSymbol type, InlineShaderContext shaderInfo, out string code, string tabs = "")
        {
            bool result = false; code = string.Empty;
            var elemetType = type.ElementType;
            if (!CheckStruct(context, elemetType)) return false;
            if (!GetGLSLTypeName(context, elemetType, op.Syntax.GetLocation(), out string name)) return false;
            int count = op.Declarators.Length;
            string[] fragments = new string[count];
            for (int i = 0; i < count; i++)
            {
                string fragment = $"{tabs}{name}[SIZESLOT] ";
                var local = op.Declarators[i];
                ILocalSymbol localSymbol = local.Symbol;
                fragment += localSymbol.Name;
                if(local.Initializer == null)
                {
                    result = false;
                    context.ReportDiagnostic(Diagnostic.Create(Descriptors.InitArrayLengthLost, op.Syntax.GetLocation()));
                    break;
                }
                var value = local.Initializer.Value;
                if (value is not IArrayCreationOperation arrayCreation)
                {
                    result = false;
                    context.ReportDiagnostic(Diagnostic.Create(Descriptors.InitArrayLengthLost, op.Syntax.GetLocation()));
                    break;
                }
                result = GetArrayCreationCode(context, caller, arrayCreation, name, shaderInfo, out string localSize, out string lCode);
                if (!result) break;
                fragment = fragment.Replace("[SIZESLOT]", localSize);
                fragment += $"{lCode};\r\n";
                fragments[i] = fragment;
            }
            code += string.Join("", fragments);
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="op"></param>
        /// <param name="shaderInfo"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        private bool GetBlockOperation(GeneratorExecutionContext context, InlineShaderMethod caller,
            IBlockOperation op, InlineShaderContext shaderInfo, out string code, string tabs = "")
        {
            bool result = false;
            code = string.Empty;
            StringBuilder codeBuilder = new StringBuilder();
            foreach(var child in op.Operations)
            {
                result = GetOperationCode(context, caller, child, shaderInfo, out string local, tabs);
                if (result) codeBuilder.AppendLine(local);
                else break;
            }
            if (result) code = codeBuilder.ToString();
            return result;
        }
        /// <summary>
        /// If
        /// </summary>
        /// <param name="context"></param>
        /// <param name="op"></param>
        /// <param name="shaderInfo"></param>
        /// <param name="code"></param>
        /// <param name="tabs"></param>
        /// <returns></returns>
        private bool GetConditionalOperation(GeneratorExecutionContext context, InlineShaderMethod caller,
            IConditionalOperation op, InlineShaderContext shaderInfo, out string code, string tabs = "")
        {
            code = string.Empty; bool result;
            StringBuilder local = new StringBuilder();
            var syntax = op.Syntax;
            if(syntax is ConditionalExpressionSyntax)
            {
                local.Append(tabs);
                result = GetOperationCode(context, caller, op.Condition, shaderInfo, out string be);
                if (!result) return result;
                local.Append(be);
                local.Append(" ? ");
                result = GetOperationCode(context, caller, op.WhenTrue, shaderInfo, out string wt);
                if (!result) return result;
                local.Append(wt);
                local.Append(" : ");
                result = GetOperationCode(context, caller, op.WhenFalse, shaderInfo, out string wFalse);
                if (!result) return result;
                local.Append(wFalse);
                code = local.ToString();
            }
            else
            {
                local.Append(tabs);
                local.Append("if(");
                result = GetOperationCode(context, caller, op.Condition, shaderInfo, out string be);
                if (result) local.Append(be);
                else return result;
                local.AppendLine(")");
                local.AppendLine($"{tabs}{{");
                result = GetOperationCode(context, caller, op.WhenTrue, shaderInfo, out string wt, $"{tabs}\t");
                if (result) local.AppendLine(wt);
                else return result;
                local.AppendLine($"{tabs}}}");
                if (op.WhenFalse != null)
                {
                    if (op.WhenFalse is IConditionalOperation)
                    {
                        local.Append($"{tabs}else ");
                        result = GetOperationCode(context, caller, op.WhenFalse, shaderInfo, out string ifElse, tabs);
                        if (result)
                        {
                            ifElse = ifElse.TrimStart(tabs.ToCharArray());
                            local.Append(ifElse);
                        }
                        else return result;
                    }
                    else
                    {
                        local.AppendLine($"{tabs}else");
                        local.AppendLine($"{tabs}{{");
                        result = GetOperationCode(context, caller, op.WhenFalse, shaderInfo, out string wFalse, $"{tabs}\t");
                        if (result) local.AppendLine(wFalse);
                        else return result;
                        local.AppendLine($"{tabs}}}");
                    }
                }
                code = local.ToString();
            }
            return result;
        }
        /// <summary>
        /// For
        /// </summary>
        /// <param name="context"></param>
        /// <param name="op"></param>
        /// <param name="shaderInfo"></param>
        /// <param name="code"></param>
        /// <param name="tabs"></param>
        /// <returns></returns>
        private bool GetForOperation(GeneratorExecutionContext context, InlineShaderMethod caller,
            IForLoopOperation op, InlineShaderContext shaderInfo, out string code, string tabs = "")
        {
            bool result = false; code = string.Empty;
            StringBuilder codeBuilder = new StringBuilder();
            codeBuilder.Append(tabs);
            codeBuilder.Append("for(");
            string be = string.Empty;
            foreach (var before in op.Before)
            {
                result = GetOperationCode(context, caller, before, shaderInfo, out string c);
                if (result)
                {
                    c = c.Replace(";", "");
                    be += $"{c},";
                }
                else break;
            }
            if (result) be = be.TrimEnd(',');
            else return result;
            codeBuilder.Append($"{be};");
            result = GetOperationCode(context, caller, op.Condition, shaderInfo, out string sceond);
            if (result) codeBuilder.Append($"{sceond};");
            else return result;
            string lastCode = string.Empty;
            foreach (var last in op.AtLoopBottom)
            {
                result = GetOperationCode(context, caller, last, shaderInfo, out string l);
                if (result)
                {
                    l = l.Replace(";", "");
                    lastCode += $"{l},";
                }
                else break;
            }
            if (result)
            {
                lastCode = lastCode.TrimEnd(',');
                codeBuilder.Append(lastCode);
            }
            else return result;
            codeBuilder.AppendLine(")");
            codeBuilder.AppendLine($"{tabs}{{");
            var body = op.Body;
            result = GetOperationCode(context, caller, body, shaderInfo, out string b, $"{tabs}\t");
            if (result) codeBuilder.Append(b);
            else return result;
            codeBuilder.AppendLine($"{tabs}}}");
            code = codeBuilder.ToString();
            return result;
        }
        /// <summary>
        /// 获取While操作
        /// </summary>
        /// <param name="context"></param>
        /// <param name="caller"></param>
        /// <param name="op"></param>
        /// <param name="shaderInfo"></param>
        /// <param name="code"></param>
        /// <param name="tabs"></param>
        /// <returns></returns>
        private bool GetWhileOperation(GeneratorExecutionContext context, InlineShaderMethod caller,
            IWhileLoopOperation op, InlineShaderContext shaderInfo, out string code, string tabs = "")
        {
            code = string.Empty;
            StringBuilder codeBuilder = new StringBuilder();
            codeBuilder.Append(tabs);
            bool result;
            if (op.ConditionIsTop)
            {
                codeBuilder.Append("while(");
                result = GetOperationCode(context, caller, op.Condition, shaderInfo, out string condition);
                if (result) codeBuilder.Append(condition);
                else return result;
                codeBuilder.AppendLine(")");
            }
            else codeBuilder.AppendLine("do");
            codeBuilder.AppendLine($"{tabs}{{");
            result = GetOperationCode(context, caller, op.Body, shaderInfo, out string body, $"{tabs}\t");
            if (result) codeBuilder.Append(body);
            else return result;
            if(op.ConditionIsTop) codeBuilder.AppendLine($"{tabs}}}");
            else
            {
                codeBuilder.Append("while(");
                result = GetOperationCode(context, caller, op.Condition, shaderInfo, out string condition);
                if (result) codeBuilder.Append(condition);
                else return result;
                codeBuilder.AppendLine(");");
            }
            code = codeBuilder.ToString();
            return result;
        }
        private bool GetExpressionStatement(GeneratorExecutionContext context, InlineShaderMethod caller,
            IExpressionStatementOperation op, InlineShaderContext shaderInfo, out string code, string tabs = "")
        {
            var body = op.Operation; code = string.Empty;
            bool result = GetOperationCode(context, caller, body, shaderInfo, out string e, tabs);
            if (result) code = $"{tabs}{e};";
            return result;
        }
        private bool GetCompoundAssignmentCode(GeneratorExecutionContext context, InlineShaderMethod caller,
            ICompoundAssignmentOperation op, InlineShaderContext shaderInfo, out string code)
        {
            bool result = false; code = string.Empty;
            var left = op.Target;
            result = GetOperationCode(context, caller, left, shaderInfo, out string l);
            if (!result) return result;
            code = l;
            string opt = string.Empty;
            switch (op.OperatorKind)
            {
                case BinaryOperatorKind.Add:
                    opt = " += ";
                    break;
                case BinaryOperatorKind.Subtract:
                    opt = " -= ";
                    break;
                case BinaryOperatorKind.Multiply:
                    opt = " *= ";
                    break;
                case BinaryOperatorKind.Divide:
                    opt = " /= ";
                    break;
                case BinaryOperatorKind.Remainder:
                    opt = " %= ";
                    break;
                case BinaryOperatorKind.And:
                    opt = " &= ";
                    break;
                case BinaryOperatorKind.Or:
                    opt = " |= ";
                    break;
                default:
                    context.ReportDiagnostic(Diagnostic.Create(Descriptors.NoSupportExpressionSyntax, op.Syntax.GetLocation()));
                    break;
            }
            code += opt;
            var right = op.Value;
            result = GetOperationCode(context, caller, right, shaderInfo, out string r);
            if (!result) return result;
            code += r;
            return result;
        }
        private bool GetArrayCreationCode(GeneratorExecutionContext context, InlineShaderMethod caller,
            IArrayCreationOperation op, string elementType, InlineShaderContext shaderInfo, out string arraySizeCode, out string code)
        {
            bool result = false; code = string.Empty; arraySizeCode = string.Empty;
            foreach(var sizeOp in op.DimensionSizes)
            {
                if(sizeOp is not ILiteralOperation literal)
                {
                    result = false;
                    context.ReportDiagnostic(Diagnostic.Create(Descriptors.InitArrayLengthLost, op.Syntax.GetLocation()));
                    break;
                }
                if (literal.Syntax is not LiteralExpressionSyntax syntax)
                {
                    arraySizeCode += $"[{literal.ConstantValue}]";
                }
                else
                {
                    result = GetLiteralExpressionSyntaxValue(context, syntax, literal.Type, out string lCode);
                    if (!result) break;
                    arraySizeCode += $"[{lCode}]";
                }
            }
            if(op.Initializer != null)
            {
                code += $" = {elementType}{arraySizeCode}";
                code += "(";
                int c = op.Initializer.ElementValues.Length;
                string[] values = new string[c];
                for(int i = 0; i < c; i++)
                {
                    var item = op.Initializer.ElementValues[i];
                    result = GetOperationCode(context, caller, item, shaderInfo, out string v);
                    if (!result) break;
                    values[i] = v;
                }
                code += string.Join(",", values);
                code += ")";
            }
            return result;
        }
        private bool GetReturnOperationCode(GeneratorExecutionContext context, InlineShaderMethod caller,
            IReturnOperation op, InlineShaderContext shaderInfo, out string code, string tabs = "")
        {
            bool result;
            code = string.Empty;
            code += $"{tabs}return";
            if (op.ReturnedValue != null)
            {
                result = GetOperationCode(context, caller, op.ReturnedValue, shaderInfo, out string value);
                if (!result) return result;
                code += $" {value}";
            }
            code += ";";
            result = true;
            return result;
        }
        private bool GetBranchOperationCode(GeneratorExecutionContext context, InlineShaderMethod caller,
            IBranchOperation op, InlineShaderContext shaderInfo, out string code, string tabs = "")
        {
            bool result = false; code = string.Empty;
            code += tabs;
            switch (op.BranchKind)
            {
                case BranchKind.Break:
                    result = true;
                    code = "break;";
                    break;
                case BranchKind.Continue:
                    code = "continue;";
                    result = true;
                    break;
                default:
                    context.ReportDiagnostic(Diagnostic.Create(Descriptors.NoSupportExpressionSyntax, op.Syntax.GetLocation()));
                    break;
            }
            return result;
        }
        /// <summary>
        /// 获取操作代码
        /// </summary>
        /// <param name="context"></param>
        /// <param name="op"></param>
        /// <param name="shaderInfo"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        private bool GetOperationCode(GeneratorExecutionContext context, InlineShaderMethod caller, IOperation op,
            InlineShaderContext shaderInfo, out string code, string tabs = "")
        {
            code = string.Empty;
            bool result = false;
            switch (op)
            {
                case ISimpleAssignmentOperation simple:
                    var left = simple.Target;
                    result = GetOperationCode(context, caller, left, shaderInfo, out string l);
                    if (!result) return result;
                    code = $"{l} = ";
                    var right = simple.Value;
                    result = GetOperationCode(context, caller, right, shaderInfo, out string r);
                    if (!result) return result;
                    code += r;
                    break;
                case IPropertyReferenceOperation propertyReference:
                    result = GetPropertyReferenceCode(context, caller, propertyReference, shaderInfo, out string pCode);
                    if (!result) return result;
                    code = pCode;
                    break;
                case IFieldReferenceOperation fieldReference:
                    result = GetFieldReferenceCode(context, caller, fieldReference, shaderInfo, out string fCode);
                    if (!result) return result;
                    code = fCode;
                    break;
                case IArrayElementReferenceOperation arrayElementReference:
                    result = GetArrayElementReferenceCode(context, caller, arrayElementReference, shaderInfo, out code);
                    break;
                case IInvocationOperation invocation:
                    result = GetInvocationOperationCode(context, caller, invocation, shaderInfo, out string iCode);
                    if (!result) return result;
                    code = iCode;
                    break;
                case IBinaryOperation binaryOperation:
                    result = GetBinaryOperationCode(context, caller, binaryOperation, shaderInfo, out string bCode);
                    if(!result) return result;
                    code = bCode;
                    break;
                case IObjectCreationOperation objectCreationOperation:
                    result = GetObjectCreateOperation(context, caller, objectCreationOperation, shaderInfo, out string oCode);
                    if (!result) return result;
                    code = oCode;
                    break;
                case ILiteralOperation literalOperation:
                    var syntax = literalOperation.Syntax as LiteralExpressionSyntax;
                    result =  GetLiteralExpressionSyntaxValue(context, syntax, literalOperation.Type, out string lCode);
                    if (!result) return result;
                    code = lCode;
                    break;
                case ILocalReferenceOperation localReferenceOperation:
                    result = GetLocalReferenceCode(context, localReferenceOperation, shaderInfo, out string localC);
                    if (!result) return result;
                    code = localC;
                    break;
                case IParameterReferenceOperation parameterReferenceOperation:
                    result = GetParameterReferenceCode(context, parameterReferenceOperation, shaderInfo, out string paraC);
                    if (!result) return result;
                    code = paraC;
                    break;
                case IConversionOperation conversionOperation:
                    if (conversionOperation.IsImplicit)
                    {
                        result = GetOperationCode(context, caller, conversionOperation.Operand, shaderInfo, out string cCode);
                        if (!result) return result;
                        code = cCode;
                    }
                    else
                    {
                        result = GetOperationCode(context, caller, conversionOperation.Operand, shaderInfo, out string cCode);
                        if (!result) return result;
                        result = GetGLSLTypeName(context, conversionOperation.Type, conversionOperation.Syntax.GetLocation(), out string tName);
                        if (!result) return result;
                        code = $"{tName}({cCode})";
                    }
                    break;
                case IDefaultValueOperation literalOperation:
                    var literal = literalOperation.Syntax as LiteralExpressionSyntax;
                    result = GetLiteralExpressionSyntaxValue(context, literal, literalOperation.Type, out code);
                    break;
                case IUnaryOperation unaryOperation:
                    result = GetUnaryOperation(context, caller, unaryOperation, shaderInfo, out code);
                    break;
                case IExpressionStatementOperation expressionStatement:
                    result = GetExpressionStatement(context, caller, expressionStatement, shaderInfo, out code, tabs);
                    break;
                case IIncrementOrDecrementOperation incrementOrDecrement:
                    result = GetIncrementOrDecrementOperation(context, caller, incrementOrDecrement, shaderInfo, out code);
                    break;
                case IBlockOperation blockOperation:
                    result = GetBlockOperation(context, caller, blockOperation, shaderInfo, out code, tabs);
                    break;
                case IVariableDeclarationGroupOperation variableDeclaration:
                    result = GetVariableDeclarationGroupOperation(context, caller, variableDeclaration, shaderInfo, out code, tabs);
                    break;
                case IForLoopOperation forLoopOperation:
                    result = GetForOperation(context, caller, forLoopOperation, shaderInfo, out code, tabs);
                    break;
                case IWhileLoopOperation whileLoopOperation:
                    result = GetWhileOperation(context, caller, whileLoopOperation, shaderInfo, out code, tabs);
                    break;
                case IConditionalOperation conditionalOperation:
                    result = GetConditionalOperation(context, caller, conditionalOperation, shaderInfo, out code, tabs);
                    break;
                case ICompoundAssignmentOperation compoundAssignment:
                    result = GetCompoundAssignmentCode(context, caller, compoundAssignment, shaderInfo, out code);
                    break;
                case IReturnOperation returnOperation:
                    result = GetReturnOperationCode(context, caller, returnOperation, shaderInfo, out code, tabs);
                    break;
                case IBranchOperation branchOperation:
                    result = GetBranchOperationCode(context, caller, branchOperation, shaderInfo, out code, tabs);
                    break;
                default:
                    context.ReportDiagnostic(Diagnostic.Create(Descriptors.NoSupportExpressionSyntax, op.Syntax.GetLocation()));
                    break;
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="syntax"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        private bool GetLiteralExpressionSyntaxValue(GeneratorExecutionContext context,
            LiteralExpressionSyntax syntax, ITypeSymbol initType, out string value)
        {
            bool result = false;
            value = string.Empty;
            switch (syntax.Kind())
            {
                case SyntaxKind.NumericLiteralExpression:
                    value = syntax.Token.Text;
                    result = true;
                    break;
                case SyntaxKind.TrueLiteralExpression:
                    value = "true";
                    result = true;
                    break;
                case SyntaxKind.FalseLiteralExpression:
                    value = "false";
                    result = true;
                    break;
                case SyntaxKind.DefaultLiteralExpression:
                    result = GetStructTypeDefaultInitializerCode(context, syntax.GetLocation(), initType, out value);
                    break;
                default:
                    context.ReportDiagnostic(Diagnostic.Create(Descriptors.NoSupportLiteralExpressionSyntax, syntax.GetLocation()));
                    break;
            }
            return result;
        }
        #endregion

        #region ParseExpression
        /// <summary>
        /// 解析局部变量片段
        /// </summary>
        /// <param name="context"></param>
        /// <param name="vert"></param>
        /// <param name="op"></param>
        /// <param name="shader"></param>
        /// <param name="tabs"></param>
        /// <returns></returns>
        private bool ParseLocalStatementDeclaration(GeneratorExecutionContext context, InlineShaderMethod caller,
            IVariableDeclarationGroupOperation op, InlineShaderContext shaderInfo,
            StringBuilder shader, string tabs = "")
        {
            bool result = GetVariableDeclarationGroupOperation(context, caller, op, shaderInfo, out string code, tabs);
            if (result) shader.AppendLine(code);
            return result;
        }
        /// <summary>
        /// 解析表达式片段
        /// </summary>
        /// <param name="context"></param>
        /// <param name="op"></param>
        /// <param name="shaderInfo"></param>
        /// <param name="shader"></param>
        /// <param name="tabs"></param>
        /// <returns></returns>
        private bool ParseExpressionSyntax(GeneratorExecutionContext context, InlineShaderMethod caller, IOperation op,
            InlineShaderContext shaderInfo, StringBuilder shader, string tabs = "")
        {
            shader.Append(tabs);
            bool result = GetOperationCode(context, caller, op, shaderInfo, out string code);
            if (result)
            {
                shader.AppendLine($"{code};");
            }
            return result;
        }
        /// <summary>
        /// 解析返回表达式片段
        /// </summary>
        /// <param name="context"></param>
        /// <param name="operation"></param>
        /// <param name="shaderInfo"></param>
        /// <param name="shader"></param>
        /// <param name="tabs"></param>
        /// <returns></returns>
        private bool ParseReturnExpression(GeneratorExecutionContext context, InlineShaderMethod caller, IReturnOperation operation,
            InlineShaderContext shaderInfo, StringBuilder shader, string tabs = "")
        {
            bool result = GetReturnOperationCode(context, caller, operation, shaderInfo, out string code, tabs);
            if (result) shader.AppendLine(code);
            return result;
        }
        private bool ParseForStatement(GeneratorExecutionContext context, InlineShaderMethod caller, IForLoopOperation operation, 
            InlineShaderContext shaderInfo, StringBuilder shader, string tabs = "")
        {
            bool result = GetForOperation(context, caller, operation, shaderInfo, out string code, tabs);
            if (result) shader.Append(code);
            return result;
        }
        private bool ParseWhiteStatement(GeneratorExecutionContext context, InlineShaderMethod caller, IWhileLoopOperation operation,
            InlineShaderContext shaderInfo, StringBuilder shader, string tabs = "")
        {
            bool result = GetWhileOperation(context, caller, operation, shaderInfo, out string code, tabs);
            if (result) shader.Append(code);
            return result;
        }
        private bool ParseConditionalStatement(GeneratorExecutionContext context, InlineShaderMethod caller, IConditionalOperation operation,
            InlineShaderContext shaderInfo, StringBuilder shader, string tabs = "")
        {
            bool result = GetConditionalOperation(context, caller, operation, shaderInfo, out string code, tabs);
            if (result) shader.Append(code);
            return result;
        }
        #endregion

        /// <summary>
        /// 解析至GLSL函数
        /// </summary>
        /// <param name="context"></param>
        /// <param name="method"></param>
        /// <param name="inlineTable"></param>
        /// <returns></returns>
        private bool ParseInlineGLSLMethod(GeneratorExecutionContext context, IMethodSymbol method,
            InlineShaderContext shaderInfo, StringBuilder codeBuilder)
        {
            var astTrees = method.DeclaringSyntaxReferences;
            if (astTrees.Length > 0)
            {
                var refence = astTrees[0];
                var ast = refence.SyntaxTree;
                if (ast.Options.Errors.Length > 0) return false;
                else
                {
                    if (refence.GetSyntax() is not MethodDeclarationSyntax root) return false;
                    else
                    {
                        SemanticModel model = context.Compilation.GetSemanticModel(ast);
                        var location = method.Locations.Length > 0 ? method.Locations[0] : Location.None;
                        bool result;
                        var retType = method.ReturnType;
                        string[] parameters = new string[method.Parameters.Length];
                        result = GetGLSLTypeName(context, retType, location, out string retName);
                        if (!result) return result;
                        for (int i = 0; i < parameters.Length; i++)
                        {
                            var param = method.Parameters[i];
                            result = GetGLSLMethodParameterCode(context, param, out string local);
                            if (!result) return result;
                            parameters[i] = local;
                        }
                        codeBuilder.AppendLine($"{retName} {method.Name}({string.Join(",", parameters)})");
                        codeBuilder.AppendLine("{");
                        var body = root.Body;
                        bool hasError = false;
                        if (body != null)
                        {
                            string token = CreateMethodToken(method);
                            if (!shaderInfo.InlineMethods.TryGetValue(token, out InlineShaderMethod caller))
                            {
                                caller = new InlineShaderMethod(method, codeBuilder);
                                shaderInfo.InlineMethods.Add(token, caller);
                            }
                            caller.Parsed = true;
                            var statments = body.Statements;
                            foreach (var statment in statments)
                            {
                                switch (statment)
                                {
                                    case ExpressionStatementSyntax expression:
                                        var local = expression.Expression;
                                        var exp = model.GetOperation(local);
                                        result = ParseExpressionSyntax(context, caller, exp, shaderInfo, codeBuilder, "\t");
                                        if (!result) hasError = true;
                                        break;
                                    case LocalDeclarationStatementSyntax localstate:
                                        var op = model.GetOperation(localstate) as IVariableDeclarationGroupOperation;
                                        result = ParseLocalStatementDeclaration(context, caller, op, shaderInfo, codeBuilder, "\t");
                                        if (!result) hasError = true;
                                        break;
                                    case ReturnStatementSyntax returnstate:
                                        var ret = model.GetOperation(returnstate) as IReturnOperation;
                                        result = ParseReturnExpression(context, caller, ret, shaderInfo, codeBuilder, "\t");
                                        if(!result) hasError = true;
                                        break;
                                    case ForStatementSyntax forstate:
                                        var forsyntax = model.GetOperation(forstate) as IForLoopOperation;
                                        result = ParseForStatement(context, caller, forsyntax, shaderInfo, codeBuilder, "\t");
                                        if(!result) hasError = true;
                                        break;
                                    case WhileStatementSyntax whilestate:
                                        var whileop = model.GetOperation(whilestate) as IWhileLoopOperation;
                                        result = ParseWhiteStatement(context, caller, whileop, shaderInfo, codeBuilder, "\t");
                                        if (!result) hasError = true;
                                        break;
                                    case IfStatementSyntax ifstate:
                                        var ifOp = model.GetOperation(ifstate) as IConditionalOperation;
                                        result = ParseConditionalStatement(context, caller, ifOp, shaderInfo, codeBuilder, "\t");
                                        if(!result) hasError = true;
                                        break;
                                    default:
                                        context.ReportDiagnostic(Diagnostic.Create(Descriptors.NoSupportExpressionSyntax, statment.GetLocation()));
                                        continue;
                                }
                            }
                        }
                        else
                        {
                            hasError = true;
                            context.ReportDiagnostic(Diagnostic.Create(Descriptors.InlineGLSLMethodSourceLost, location));
                        }
                        codeBuilder.AppendLine("}");
                        return !hasError;
                    }
                }
            }
            else
            {
                context.ReportDiagnostic(Diagnostic.Create(Descriptors.InlineGLSLMethodSourceLost, Location.None, method.Name));
                return false;
            }
        }
        /// <summary>
        /// 检查内建函数
        /// </summary>
        /// <param name="context"></param>
        /// <param name="inlineShader"></param>
        /// <param name="code"></param>
        /// <param name="needParsed"></param>
        /// <returns></returns>
        private bool CheckInlineMethods(GeneratorExecutionContext context, 
            InlineShaderContext inlineShader, StringBuilder code, List<InlineShaderMethod> needParsed)
        {
            bool result = true;
            bool needParse = false;
            foreach (var method in inlineShader.InlineMethods)
            {
                if (!method.Value.Parsed)
                {
                    needParse = true;
                    needParsed.Add(method.Value);
                }
            }
            if (needParse)
            {
                foreach (var item in needParsed)
                {
                    result = ParseInlineGLSLMethod(context, item.Method, inlineShader, item.CodeBuilder);
                    if (!result)
                    {
                        result = false;
                        break;
                    }
                }
                if (result)
                {
                    needParsed.Clear();
                    CheckInlineMethods(context, inlineShader, code, needParsed);
                }
            }
            return result;
        }
        /// <summary>
        /// 是否为着色器内建函数
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        private bool IsShaderBuildInMethod(IMethodSymbol method)
        {
            var type = method.ReceiverType;
            if (type == null) return false;
            else if (type.ToString() == "IOP.SgrA.GLSL.GLSLVert") return true;
            else if (type.ToString() == "IOP.SgrA.GLSL.GLSLFrag") return true;
            else if (type.ToString() == "IOP.SgrA.GLSL.GLSLGeom") return true;
            else if (type.ToString() == "IOP.SgrA.GLSL.GLSLShaderBase") return true;
            return false;
        }
        private string CreateMethodToken(IMethodSymbol method)
        {
            return $"{method.ReturnType} {method.Name}({string.Join(",", method.Parameters.Select(x => x.Type.Name))})";
        }
        private void WriteMethods(Dictionary<string, InlineShaderMethod> books, InlineShaderMethod caller, StringBuilder coder)
        {
            var token = CreateMethodToken(caller.Method);
            if (books.ContainsKey(token)) return;
            if (caller.Childrens.Count > 0)
            {
                foreach (var child in caller.Childrens)
                {
                    if (books.ContainsKey(CreateMethodToken(child.Method))) continue;
                    else WriteMethods(books, child, coder);
                }
            }
            coder.AppendLine();
            coder.AppendLine(caller.CodeBuilder.ToString());
            books.Add(token, caller);
        }
        private void WriteShdaerFields(Dictionary<string, IShaderField> books, IShaderField field, StringBuilder coder)
        {
            var token = field.Name;
            if (field.Skip) return;
            if (books.ContainsKey(token)) return;
            foreach (var child in field.Childrens)
            {
                if (books.ContainsKey(child.Name)) continue;
                else WriteShdaerFields(books, child, coder);
            }
            field.WriteToCode(coder);
            books.Add(token, field);
        }
    }
}
