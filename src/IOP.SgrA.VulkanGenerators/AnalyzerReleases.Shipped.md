﻿## Release 1.0

### New Rules

Rule ID | Category | Severity | Notes
--------|----------|----------|--------------------
VKGF001 |  DiagnosticsGenerator  |  Error | VKGF001_AnalyzerName, Source Generator build failed
VKGF002 |  DiagnosticsGenerator  |  Error | VKGF002_AnalyzerName, Target type must be struct
VKGF003 |  DiagnosticsGenerator  |  Error | VKGF003_AnalyzerName, Target type is not glsl data type or target struct contains non-glsl data type
VKGF004 |  DiagnosticsGenerator  |  Error | VKGF004_AnalyzerName, Same DescriptorSet must be have same DescriptorSetTarget
VKGF005 |  DiagnosticsGenerator  |  Error | VKGF005_AnalyzerName, SampledImageAttribute must be used to GLSL sampler image type
VKGF006 |  DiagnosticsGenerator  |  Error | VKGF006_AnalyzerName, DescriptorSet Attachment is used
VKGF007 |  DiagnosticsGenerator  |  Error | VKGF007_AnalyzerName, StorageImageAttribute must be used to GLSL image type
VKGF008 |  DiagnosticsGenerator  |  Error | VKGF008_AnalyzerName, InputAttachmentAttribute must be used to strcut subpassInput
VKGF009 |  DiagnosticsGenerator  |  Error | VKGF009_AnalyzerName, VertexInputAttribute must be used to GLSL base type
VKGF010 |  DiagnosticsGenerator  |  Error | VKGF010_AnalyzerName, Target VertexInput location is already used
VKGF011 |  DiagnosticsGenerator  |  Error | VKGF011_AnalyzerName, FragOutputAttribute must be used to GLSL base type
VKGF012 |  DiagnosticsGenerator  |  Error | VKGF012_AnalyzerName, Target FragOutput location is already used
VKGF013 |  DiagnosticsGenerator  |  Error | VKGF013_AnalyzerName, Shader with type was already exist in pipeline
VKGF014 |  DiagnosticsGenerator  |  Error | VKGF014_AnalyzerName, 'non-opaque uniforms outside a block' : not allowed when using GLSL for Vulkan
VKGF015 |  DiagnosticsGenerator  |  Error | VKGF015_AnalyzerName, Please use VertInputAttribute instead
VKGF016 |  DiagnosticsGenerator  |  Error | VKGF015_AnalyzerName, Please use FragOutputAttribute instead
VKGF017 |  DiagnosticsGenerator  |  Error | VKGF017_AnalyzerName, Uniform or Storage buffer attribute cannot be used in image
VKGF018 |  DiagnosticsGenerator  |  Error | VKGF018_AnalyzerName, GeometryInputAttribute must be used to GLSL base type or base type array

VKGF500 |  ScriptedShaderGenerator  |  Error | VKGF500_AnalyzerName, Not Support Expression Syntax with GLSL
VKGF501 |  ScriptedShaderGenerator  |  Error | VKGF501_AnalyzerName, Array Length data lost when initialization
VKGF502 |  ScriptedShaderGenerator  |  Error | VKGF502_AnalyzerName, Custom struct initializer was not supported to translation GLSL code
VKGF503 |  ScriptedShaderGenerator  |  Error | VKGF503_AnalyzerName, Not Support Argument Syntax with GLSL
VKGF504 |  ScriptedShaderGenerator  |  Error | VKGF504_AnalyzerName, Not Support literal expression with GLSL
VKGF505 |  ScriptedShaderGenerator  |  Error | VKGF505_AnalyzerName, Not Support Default Value expression with GLSL custom method parameter
VKGF506 |  ScriptedShaderGenerator  |  Error | VKGF506_AnalyzerName, Cannot found sourcs code with target method
VKGF507 |  ScriptedShaderGenerator  |  Error | VKGF507_AnalyzerName, Samper or image must be define in uniform variables
VKGF508 |  ScriptedShaderGenerator  |  Error | VKGF508_AnalyzerName, Cannot found sourcs code with target field

