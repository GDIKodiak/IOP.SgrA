﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Operations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace IOP.SgrA.VulkanGenerators
{
    public partial class ScriptedPipelineGenerator
    {
        private bool ParseGLSLVertShader(GeneratorExecutionContext context, ITypeSymbol vert, 
            VertexShaderWrapper vertexShader)
        {
            bool result = true;
            string flags = "ShaderStageFlags.VertexBit";
            var fields = vertexShader.ShaderContext.InlineFileds;
            foreach (var attr in vert.GetAttributes())
            {
                if (attr.AttributeClass.Name == "VertexInputBindingAttribute")
                {
                    BuildVertexInputBinding(context, vert, attr, vertexShader.VertexInputs);
                }
                else if (attr.AttributeClass.Name == "GLSLExtensionAttribute")
                {
                    BuildShaderExtension(context, vert, attr, vertexShader.ShaderContext.ShaderExtensions);
                }
                else if (attr.AttributeClass.Name == "ShaderVersionAttribute")
                {
                    BuildShaderVersion(context, vert, attr, vertexShader.ShaderContext);
                }
            }
            foreach (var prop in vert.GetMembers().Where(x => x.Kind == SymbolKind.Property))
            {
                var p = prop as IPropertySymbol;
                foreach (var attr in p.GetAttributes())
                {
                    if (attr.AttributeClass.Name == "UniformBufferAttribute")
                    {
                        bool r = BuildShaderBuffer(context, p, attr, flags, "DescriptorType.UniformBuffer",
                            vertexShader.DescriptorSetLayouts, fields);
                        if (!r) result = r;
                    }
                    else if (attr.AttributeClass.Name == "DynamicUniformBufferAttribute")
                    {
                        bool r = BuildShaderBuffer(context, p, attr, flags, "DescriptorType.UniformBufferDynamic",
                            vertexShader.DescriptorSetLayouts, fields);
                        if (!r) result = r;
                    }
                    else if (attr.AttributeClass.Name == "StorageBufferAttribute")
                    {
                        bool r = BuildShaderBuffer(context, p, attr, flags, "DescriptorType.StorageBuffer",
                            vertexShader.DescriptorSetLayouts, fields);
                        if (!r) result = r;
                    }
                    else if (attr.AttributeClass.Name == "DynamicStorageBufferAttribute")
                    {
                        bool r = BuildShaderBuffer(context, p, attr, flags, "DescriptorType.StorageBufferDynamic",
                            vertexShader.DescriptorSetLayouts, fields);
                        if (!r) result = r;
                    }
                    else if (attr.AttributeClass.Name == "SampledImageAttribute")
                    {
                        bool r = BuildSamplerImage(context, p, attr, flags, vertexShader.DescriptorSetLayouts, fields);
                        if (!r) result = r;
                    }
                    else if (attr.AttributeClass.Name == "StorageImageAttribute")
                    {
                        bool r = BuildStorageImage(context, p, attr, flags, vertexShader.DescriptorSetLayouts, fields);
                        if (!r) result = r;
                    }
                    else if (attr.AttributeClass.Name == "InputAttachmentAttribute")
                    {
                        bool r = BuildInputAttachment(context, p, attr, flags, vertexShader.DescriptorSetLayouts, fields);
                        if (!r) result = r;
                    }
                    else if (attr.AttributeClass.Name == "PushConstantAttribute")
                    {
                        bool r = BuildPushConstant(context, p, attr, flags, vertexShader.PushConstants, fields);
                        if (!r) result = r;
                    }
                    else if (attr.AttributeClass.Name == "VertInputAttribute")
                    {
                        bool r = BuildVertexInput(context, p, attr, vertexShader.VertexInputs, fields);
                        if (!r) result = r;
                    }
                    else if (attr.AttributeClass.Name == "ShaderInputAttribute")
                    {
                        bool r = BuildInput(context, p, attr, flags, fields);
                        if (!r) result = r;
                    }
                    else if (attr.AttributeClass.Name == "ShaderOutputAttribute")
                    {
                        bool r = BuildOutput(context, p, attr, flags, fields);
                        if (!r) result = r;
                    }
                }
            }
            if (!ParsedVertexes.ContainsKey(vert.ToString()))
            {
                ParseVertexShader(context, vert, vertexShader.ShaderContext);
                ParsedVertexes.Add(vert.ToString(), vertexShader);
            }
            return result;
        }

        private bool ParseVertexShader(GeneratorExecutionContext context, ITypeSymbol vert, InlineShaderContext vertexShader)
        {
            IMethodSymbol main = null;
            bool result = false;
            var mainFuncs = vert.GetMembers().Where(x => x.Kind == SymbolKind.Method && x.Name == "main");
            foreach(var func in mainFuncs)
            {
                if (func is not IMethodSymbol method) continue;
                if (method.Parameters.Length != 0 || method.MethodKind != MethodKind.Ordinary) continue;
                main = method;
                break;
            }
            if(main != null)
            {
                AddVertInlineField(vertexShader.InlineFileds);
                StringBuilder shader = new StringBuilder();
                result = ParseInlineGLSLMethod(context, main, vertexShader, shader);
                if (!result) return result;
                StringBuilder code = new StringBuilder();
                code.AppendLine($"#version {vertexShader.ShaderVersion}");
                foreach(var ext in vertexShader.ShaderExtensions)
                {
                    code.AppendLine($"#extension {ext}");
                }
                List<InlineShaderMethod> needParseMethod = new List<InlineShaderMethod>();
                Dictionary<string, InlineShaderMethod> methodBooks = new Dictionary<string, InlineShaderMethod>();
                result = CheckInlineMethods(context, vertexShader, code, needParseMethod);
                if (result)
                {
                    Dictionary<string, IShaderField> books = new Dictionary<string, IShaderField>();
                    var dic = vertexShader.InlineFileds;
                    foreach (var item in dic.Values)
                    {
                        WriteShdaerFields(books, item, code);
                    }
                    foreach (var item in vertexShader.InlineMethods)
                    {
                        var local = item.Value;
                        WriteMethods(methodBooks, local, code);
                    }
                    StringBuilder fileBuilder = new StringBuilder();
                    fileBuilder.AppendLine("using IOP.SgrA.SilkNet.Vulkan;");
                    fileBuilder.AppendLine("using IOP.SgrA.SilkNet.Vulkan.Scripted;");
                    fileBuilder.AppendLine("using Silk.NET.Vulkan;");
                    fileBuilder.AppendLine("using IOP.SgrA.GLSL;");
                    fileBuilder.AppendLine("using IOP.SgrA;");
                    var np = vert.ContainingNamespace;
                    if (np != null)
                    {
                        fileBuilder.AppendLine($"namespace {np}");
                        fileBuilder.AppendLine("{");
                        fileBuilder.AppendLine($"\tpublic partial class {vert.Name} : {vert.BaseType.Name}");
                        fileBuilder.AppendLine("\t{");
                        fileBuilder.AppendLine("\t\tprivate string _GeneratorCode = @\"");
                        fileBuilder.Append(code.ToString());
                        fileBuilder.AppendLine("\";");
                        fileBuilder.AppendLine($"\t\tpublic override string GetCode()");
                        fileBuilder.AppendLine("\t\t{");
                        fileBuilder.AppendLine("\t\t\treturn _GeneratorCode;");
                        fileBuilder.AppendLine("\t\t}");
                        fileBuilder.AppendLine("\t}");
                        fileBuilder.AppendLine("}");
                        context.AddSource($"{vert.Name}.g.cs", fileBuilder.ToString());
                    }
                }
            }
            return result;
        }

        private void AddVertInlineField(Dictionary<string, IShaderField> fields)
        {
            NormalShaderField gl_Position = new NormalShaderField() { Skip = true };
            fields.Add("gl_Position", gl_Position);
            NormalShaderField gl_PointSize = new NormalShaderField() { Skip = true };
            fields.Add("gl_PointSize", gl_PointSize);
            NormalShaderField gl_ClipDistance = new NormalShaderField() { Skip = true };
            fields.Add("gl_ClipDistance", gl_ClipDistance);
            NormalShaderField gl_CullDistance = new NormalShaderField() { Skip = true };
            fields.Add("gl_CullDistance", gl_CullDistance);
            NormalShaderField gl_InstanceID = new NormalShaderField() { Skip = true };
            fields.Add("gl_InstanceIndex", gl_InstanceID);
            NormalShaderField gl_VertexID = new NormalShaderField() { Skip = true };
            fields.Add("gl_VertexID", gl_VertexID);
            NormalShaderField gl_VertexIndex = new NormalShaderField() { Skip = true };
            fields.Add("gl_VertexIndex", gl_VertexIndex);
        }
    }
}
