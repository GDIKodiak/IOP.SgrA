﻿using Avalonia.Controls;
using Microsoft.Extensions.DependencyInjection;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IOP.Extension.DependencyInjection;
using Avalonia.Threading;
using Splat;
using System.IO;
using Avalonia.Controls.Presenters;
using CodeWF.EventBus;

namespace IOP.Halo
{
#nullable disable
    /// <summary>
    /// 默认的框架容器
    /// </summary>
    public class DefaultFrameworkContainer : IFrameworkContainer
    {
        private IServiceCollection _ServiceDescriptors = new ServiceCollection();
        internal ContainerWindow _ContainerWindow = null;

        protected ConcurrentDictionary<string, List<Func<string, Task>>> FileLoadedFuncs { get; set; }
            = new ConcurrentDictionary<string, List<Func<string, Task>>>();

        /// <summary>
        /// 服务
        /// </summary>
        public IServiceProvider Services { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public DefaultFrameworkContainer()
        {
            _ServiceDescriptors.AddSingleton<IFrameworkContainer>(this);
            _ServiceDescriptors.AddSingleton<IEventBus>(CodeWF.EventBus.EventBus.Default);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public virtual Control Build(object data)
        {
            if (Services == null) return new TextBlock { Text = "service provider lost" };

            var name = data.GetType().FullName!.Replace("ViewModel", "View");
            var type = Type.GetType(name);
            if (type != null)
            {
                return (Control)Services.CreateAutowiredInstance(type)!;
            }
            else
            {
                return new TextBlock { Text = "Not Found: " + name };
            }
        }
        /// <summary>
        /// 配置服务
        /// </summary>
        /// <param name="action"></param>
        public void ConfigureServices(Action<IServiceCollection> action)
        {
            action?.Invoke(_ServiceDescriptors);
            Services = _ServiceDescriptors.BuildServiceProvider();
        }
#nullable enable
        /// <summary>
        /// 获取首个服务
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T? GetService<T>() where T : class
        {
            if (Services == null) return null;
            return Services.GetService<T>();
        }
#nullable disable
        /// <summary>
        /// 获取首个服务，如果不存在则上报错误
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetRequiredService<T>() where T : class
        {
            if (Services == null) throw new NullReferenceException("No Container services, please configure first");
            return Services.GetRequiredService<T>();
        }
        /// <summary>
        /// 创建自动注入实例
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T CreateAutowiredInstance<T>() where T : class
        {
            Services ??= _ServiceDescriptors.BuildServiceProvider();
            return Services.CreateAutowiredInstance<T>();
        }
        /// <summary>
        /// 添加标题栏顶部菜单
        /// </summary>
        /// <param name="descriptor"></param>
        public void AddTitleBarMenu(MenuDescriptor descriptor)
        {
            if (descriptor == null || _ContainerWindow == null) return;
            _ContainerWindow.TitleBarMenus.AddToTopMenu(descriptor);
        }
        /// <summary>
        /// 添加标题栏
        /// </summary>
        /// <param name="header"></param>
        /// <param name="descriptor"></param>
        public void AddTitleBarMenuItem(string name, MenuDescriptor descriptor)
        {
            if (string.IsNullOrEmpty(name) || descriptor == null || _ContainerWindow == null) return;
            _ContainerWindow.TitleBarMenus.AddToMenu(name, descriptor);
        }
        /// <summary>
        /// 打开新的工作空间
        /// </summary>
        /// <typeparam name="TView"></typeparam>
        /// <typeparam name="TVM"></typeparam>
        /// <param name="header"></param>
        /// <param name="couldClosed"></param>
        /// <exception cref="NotImplementedException"></exception>
        public void OpenNewWorkSpace<TView, TVM>(string header, bool couldClosed = true, bool fource = false)
            where TView : ContentControl
            where TVM : FrameworkViewModel
        {
            if(string.IsNullOrEmpty(header) || _ContainerWindow == null) return;
            var vm = CreateAutowiredInstance<TVM>();
            vm.Initialition();
            Dispatcher.UIThread.Post(() =>
            {
                TView view = CreateAutowiredInstance<TView>();
                view.DataContext = vm;
                WorkspaceDescriptor descriptor = new(header)
                {
                    Workspace = new Workspace() { Content = view },
                    CouldClose = couldClosed
                };
                _ContainerWindow.NewWorkspace(descriptor, fource);
            });
        }
        /// <summary>
        /// 打开新的工作空间
        /// </summary>
        /// <typeparam name="TView"></typeparam>
        /// <typeparam name="TVM"></typeparam>
        /// <param name="header"></param>
        /// <param name="initializing"></param>
        /// <param name="onClosed"></param>
        /// <param name="force"></param>
        /// <exception cref="NotImplementedException"></exception>
        public void OpenNewWorkSpace<TView, TVM>(string header, 
            Func<TView, TVM, ValueTask> initializing, 
            Func<TView, TVM, ValueTask> onClosed, bool force = false)
            where TView : ContentControl
            where TVM : FrameworkViewModel
        {
            if (string.IsNullOrEmpty(header) || _ContainerWindow == null) return;
            var vm = CreateAutowiredInstance<TVM>();
            vm.Initialition();
            Dispatcher.UIThread.Post(() =>
            {
                TView view = CreateAutowiredInstance<TView>();
                view.DataContext = vm;
                Task.Factory.StartNew(async () =>
                {
                    if(initializing != null) { await initializing(view, vm); }
                }).ContinueWith((t) =>
                {
                    if (t.IsCompletedSuccessfully)
                    {
                        Dispatcher.UIThread.Post(() =>
                        {
                            WorkspaceDescriptor<TView, TVM> descriptor = new(header, view, vm, onClosed)
                            {
                                Workspace = new Workspace() { Content = view },
                                CouldClose = false
                            };
                            _ContainerWindow.NewWorkspace(descriptor, force);
                        });
                    }
                });
            });
        }
        /// <summary>
        /// 打开新的工作空间，且无法关闭
        /// </summary>
        /// <typeparam name="TView"></typeparam>
        /// <typeparam name="TVM"></typeparam>
        /// <param name="header"></param>
        /// <param name="view"></param>
        /// <param name="viewModel"></param>
        public void OpenNewWorkSpace<TView, TVM>(string header,
            TView view, TVM viewModel)
            where TView : ContentControl
            where TVM : FrameworkViewModel
        {
            view.DataContext = viewModel;
            Dispatcher.UIThread.Post(() =>
            {
                WorkspaceDescriptor descriptor = new(header)
                {
                    Workspace = new Workspace() { Content = view },
                    CouldClose = true
                };
                _ContainerWindow.NewWorkspace(descriptor, true);
            });
        }
        /// <summary>
        /// 打开新的工作空间,且无法关闭
        /// </summary>
        /// <typeparam name="TView"></typeparam>
        /// <typeparam name="TVM"></typeparam>
        /// <param name="header"></param>
        /// <param name="onLoading"></param>
        /// <exception cref="NotImplementedException"></exception>
        public void OpenNewWorkSpace<TView, TVM>(string header, 
            Func<TView, TVM, ValueTask> initializing)
            where TView : ContentControl
            where TVM : FrameworkViewModel
        {
            if (string.IsNullOrEmpty(header) || _ContainerWindow == null) return;
            var vm = CreateAutowiredInstance<TVM>();
            vm.Initialition();
            Dispatcher.UIThread.Post(() =>
            {
                TView view = CreateAutowiredInstance<TView>();
                view.DataContext = vm;
                Task.Factory.StartNew(async () =>
                {
                    if (initializing != null) { await initializing(view, vm); }
                }).ContinueWith((t) =>
                {
                    if (t.IsCompletedSuccessfully)
                    {
                        WorkspaceDescriptor descriptor = new(header)
                        {
                            Workspace = new Workspace() { Content = view },
                            CouldClose = true
                        };
                        _ContainerWindow.NewWorkspace(descriptor, true);
                    }
                });
            });
        }
        /// <summary>
        /// 注册特定文件加载委托
        /// </summary>
        /// <param name="func"></param>
        public void RegistFileLoadedFunc(string extension, Func<string, Task> func)
        {
            if (func == null || string.IsNullOrEmpty(extension)) return;
            if(FileLoadedFuncs.TryGetValue(extension, out List<Func<string, Task>> list))
            {
                list.Add(func);
            }
            else
            {
                List<Func<string, Task>> newList = new() { func };
                FileLoadedFuncs.AddOrUpdate(extension, newList, (key, value) =>
                {
                    if (value != null)
                    {
                        value?.Add(func);
                        return value;
                    }
                    else return newList;
                });
            }
        }
        /// <summary>
        /// 执行文件加载函数
        /// </summary>
        /// <param name="extension"></param>
        /// <returns></returns>
        public Task ExecuteFileLoadingFuncs(string path)
        {
            if (!File.Exists(path)) return Task.FromException(new FileNotFoundException(path));
            FileInfo info = new FileInfo(path);
            string extension = info.Extension.ToLower();
            if (!FileLoadedFuncs.TryGetValue(extension, out var list)) return Task.CompletedTask;
            return Task.Factory.StartNew(() =>
            {
                foreach(var item in list)
                {
                    item?.Invoke(path);
                }
            });
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool Match(object data)
        {
            if (data == null) return false;
            var name = data.GetType().FullName!.Replace("ViewModel", "View");
            var type = Type.GetType(name);
            if (type == null) return false;
            else if (type.FullName != data.GetType().FullName) return false;
            else return true;
        }

#nullable enable

        /// <summary>
        /// 获取容器窗口
        /// </summary>
        /// <typeparam name="TWindow"></typeparam>
        /// <returns></returns>
        public TWindow? GetContainerWindow<TWindow>()
            where TWindow : ContainerWindow
        {
            if (_ContainerWindow == null || _ContainerWindow is not TWindow window) return null;
            return window;
        }
    }
}
