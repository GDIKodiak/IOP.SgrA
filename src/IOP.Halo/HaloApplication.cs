﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using Microsoft.Extensions.DependencyInjection;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;
using IOP.Extension.DependencyInjection;
using Avalonia.Platform;
using Avalonia.Threading;
using Avalonia.Controls.Primitives;

namespace IOP.Halo
{
    /// <summary>
    /// 
    /// </summary>
    public class HaloApplication : Application
    {
        private DefaultFrameworkContainer _Container = new();
        /// <summary>
        /// 框架容器
        /// </summary>
        public IFrameworkContainer Container { get => _Container; }
        /// <summary>
        /// 服务
        /// </summary>
        public IServiceProvider ServiceProvider { get => Container.Services; }

        /// <summary>
        /// 注册服务
        /// </summary>
        /// <param name="descriptors"></param>
        public virtual void RegistServices(IServiceCollection descriptors) { }
        /// <summary>
        /// 创建加载页面
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public virtual LoaderControl CreateLoadingPage() => null;
        /// <summary>
        /// 创建容器页面
        /// </summary>
        /// <returns></returns>
        public virtual ContainerWindow CreateContainerWindow() => new();
        /// <summary>
        /// 加载任务
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public virtual Task LoadingTask(LoaderControl control) => Task.CompletedTask;
        /// <summary>
        /// 创建主窗口
        /// </summary>
        /// <param name="container"></param>
        /// <returns></returns>
        public virtual Window CreateMainWindow(ContainerWindow container)
        {
            if (container == null) return new Window();
            return container;
        }
        /// <summary>
        /// 当从主窗口退出时
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnExit(object sender, ShutdownRequestedEventArgs e)
        {

        }
        /// <summary>
        /// 创建自动注入实例
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public virtual T CreateAutowiredInstance<T>()
            where T : class => Container.CreateAutowiredInstance<T>();
        /// <summary>
        /// 
        /// </summary>
        public override void OnFrameworkInitializationCompleted()
        {
            Container.ConfigureServices(RegistServices);
            if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                desktop.ShutdownRequested += OnExit;
                var loader = CreateLoadingPage();
                var main = CreateContainerWindow();
                if (main == null)
                {
                    desktop.TryShutdown();
                    return;
                }
                _Container._ContainerWindow = main;
                if (loader != null)
                {
                    var load = new Window()
                    {
                        Height = loader.Height,
                        Width = loader.Width,
                        Content = loader,
                        WindowStartupLocation = WindowStartupLocation.CenterScreen,
                        ExtendClientAreaChromeHints = ExtendClientAreaChromeHints.NoChrome,
                        SystemDecorations = SystemDecorations.BorderOnly
                    };
                    load.Show();
                    var t = loader.Loading((loader) => LoadingTask(loader));
                    t.ContinueWith((tt) =>
                    {
                        if (tt.IsCompletedSuccessfully)
                        {
                            Dispatcher.UIThread.Post(() =>
                            {
                                var mainWindow = CreateMainWindow(main);
                                desktop.MainWindow = mainWindow;
                                mainWindow.Show();
                                load.Close();
                            });
                        }
                        else
                        {
                            //todo
                            load.Close();
                        }
                    });
                }
                else desktop.MainWindow = CreateMainWindow(main);
            }
            base.OnFrameworkInitializationCompleted();
        }
    }
}
