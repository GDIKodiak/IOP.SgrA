﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.Halo
{
    /// <summary>
    /// 本机内存管理器
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed unsafe class NativeMemoryManager<T> : MemoryManager<T>
        where T : unmanaged
    {
        private void* _pointer;
        private uint _length;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="length"></param>
        public NativeMemoryManager(uint length)
        {
            unsafe
            {
                int size = sizeof(T);
                uint total = (uint)(length * size);
                void* ptr = NativeMemory.Alloc(total);
                _pointer  = ptr;
                _length = length;
            }
        }

        /// <summary>
        /// Obtains a span that represents the region
        /// </summary>
        public override Span<T> GetSpan() => new Span<T>(_pointer, (int)_length);
        /// <summary>
        /// Provides access to a pointer that represents the data (note: no actual pin occurs)
        /// </summary>
        public override MemoryHandle Pin(int elementIndex = 0)
        {
            int offset = sizeof(T) * elementIndex;
            if (offset < 0 || offset >= _length)
                throw new ArgumentOutOfRangeException(nameof(elementIndex));
            void* t = (byte*)_pointer + offset;
            return new MemoryHandle(t);
        }
        /// <summary>
        /// Has no effect
        /// </summary>
        public override void Unpin() { }
        /// <summary>
        /// Releases all resources associated with this object
        /// </summary>
        protected override void Dispose(bool disposing) 
        {
            if (_pointer == null) return;
            var old = _pointer;
            _pointer = null;
            _length = 0;
            NativeMemory.Free(old);
        }
        /// <summary>
        /// 
        /// </summary>
        public void Dispose() { Dispose(true); }
    }
}
