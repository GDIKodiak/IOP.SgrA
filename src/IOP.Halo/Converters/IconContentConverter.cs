﻿using Avalonia.Controls;
using Avalonia.Data.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.Halo.Converters
{
    /// <summary>
    /// 根据字符串转换为指定图标内容控件
    /// </summary>
    public class IconContentConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value != null && value is string s)
            {
                ContentControl control = new ContentControl();
                control.Classes.Add(s);            
                return control;
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
