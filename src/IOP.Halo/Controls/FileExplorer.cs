﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.FreeDesktop.DBusIme;
using Avalonia.Threading;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.Halo
{
    public class FileExplorer : DialogWindow
    {
        public static readonly DirectProperty<FileExplorer, string> SelectedFilePathProperty
            = AvaloniaProperty.RegisterDirect<FileExplorer, string>(nameof(SelectedFilePath), (x) => x.SelectedFilePath);
        /// <summary>
        /// 被选中的文件地址
        /// </summary>
        public string SelectedFilePath
        {
            get => _SelectedFilePath;
            private set { SetAndRaise(SelectedFilePathProperty, ref _SelectedFilePath, value); }
        }

        private FileExplorerControl control;
        private string _SelectedFilePath = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        public FileExplorer()
        {
            Height = 475;
            Width = 750;
            control = new FileExplorerControl();
            control.EnterCommand = ReactiveCommand.Create(Enter, control.CounldEnter);
            control.CancelCommand = ReactiveCommand.Create(Cancel);
            Content = control;
        }

        public void Enter()
        {
            if (control == null) return;
            SelectedFilePath = control.SelectedPath;
            Close(true);
        }
        /// <summary>
        /// 
        /// </summary>
        public void Cancel() => Close(false);
    }
}
