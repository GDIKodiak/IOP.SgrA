﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Styling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IOP.Halo
{
    /// <summary>
    /// 工作空间基类
    /// </summary>
    public class Workspace : ContentControl, IAsyncDisposable
    {
        private volatile int _IsClosed = 0;

        /// <summary>
        /// 工作空间是否关闭
        /// </summary>
        public bool IsClosed
        {
            get
            {
                SpinWait wait = new();
                int old = _IsClosed;
                while (Interlocked.CompareExchange(ref _IsClosed, old, old) != old)
                {
                    wait.SpinOnce();
                    old = _IsClosed;
                }
                return old == 1;
            }
            set
            {
                SpinWait wait = new();
                int v = value ? 1 : 0;
                int old = _IsClosed;
                while (Interlocked.CompareExchange(ref _IsClosed, v, old) != old)
                {
                    wait.SpinOnce();
                    old = _IsClosed;
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected override Type StyleKeyOverride => typeof(Workspace);
        /// <summary>
        /// 初始化
        /// </summary>
        /// <returns></returns>
        public virtual ValueTask Initialization()
        {
            return ValueTask.CompletedTask;
        }
        /// <summary>
        /// 关闭当前工作空间
        /// </summary>
        /// <returns></returns>
        public virtual ValueTask Close()
        {
            IsClosed = true;
            return ValueTask.CompletedTask;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual async ValueTask DisposeAsync()
        {
            if (!IsClosed) await Close();
        }
    }
}
