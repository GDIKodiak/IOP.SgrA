﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Threading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.Halo
{
    /// <summary>
    /// 加载器控件
    /// </summary>
    public class LoaderControl : UserControl
    {
        public static readonly StyledProperty<double> ProgressProperty =
            AvaloniaProperty.Register<LoaderControl, double>("Progress", 0);
        public static readonly StyledProperty<string> MessageProperty =
            AvaloniaProperty.Register<LoaderControl, string>("Message", string.Empty);

        /// <summary>
        /// 进度条
        /// </summary>
        public double Progress
        {
            get { return GetValue(ProgressProperty); }
            set { SetValue(ProgressProperty, value); }
        }
        /// <summary>
        /// 加载消息
        /// </summary>
        public string Message
        {
            get { return GetValue(MessageProperty); }
            set { if (!string.IsNullOrEmpty(value)) SetValue(MessageProperty, value); }
        }

        /// <summary>
        /// 执行加载任务
        /// </summary>
        /// <param name="func"></param>
        /// <returns></returns>
        public virtual Task Loading(Func<LoaderControl, Task> func)
        {
            if (func == null) return Task.CompletedTask;
            return func(this);
        }
        /// <summary>
        /// 变更显示消息
        /// </summary>
        /// <param name="msg"></param>
        public virtual void ChangeMessage(string msg)
        {
            if(string.IsNullOrEmpty(msg)) return;
            Dispatcher.UIThread.Post(() => Message = msg);
        }
        /// <summary>
        /// 变更进度条
        /// </summary>
        /// <param name="newValue"></param>
        public virtual void ChangeProgress(double newValue)
        {
            Dispatcher.UIThread.Post(() => Progress = newValue);
        }
    }
}
