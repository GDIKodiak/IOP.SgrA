﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Generators;
using Avalonia.Controls.Metadata;
using Avalonia.Controls.Presenters;
using Avalonia.Controls.Primitives;
using Avalonia.Controls.Templates;
using Avalonia.Styling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.Halo
{
    /// <summary>
    /// 面包屑导航栏
    /// </summary>
    [PseudoClasses(":overWrap")]
    [TemplatePart(Name = "PART_RootItem", Type = typeof(ContentPresenter))]
    public class Breadcrumb : ItemsControl
    {
        public static readonly DirectProperty<Breadcrumb, bool> IsOverWrapProperty =
            AvaloniaProperty.RegisterDirect<Breadcrumb, bool>(nameof(IsOverWrap), (c) => c.IsOverWrap);
        public static readonly StyledProperty<object> RootItemContentProperty =
            AvaloniaProperty.Register<Breadcrumb, object>(nameof(RootItemContent));
        public static readonly StyledProperty<IDataTemplate> RootItemTemplateProperty =
            AvaloniaProperty.Register<Breadcrumb, IDataTemplate>(nameof(RootItemTemplate));
        /// <summary>
        /// 
        /// </summary>
        private static readonly FuncTemplate<Panel> DefaultPanel
            = new FuncTemplate<Panel>(() => new BreadcrumbPanel());
        private bool _IsOverWrap = false;

        protected override Type StyleKeyOverride => typeof(Breadcrumb);

        /// <summary>
        /// 显示内容是否溢出
        /// </summary>
        public bool IsOverWrap
        {
            get { return _IsOverWrap; }
            private set { SetAndRaise(IsOverWrapProperty, ref _IsOverWrap, value); }
        }
        /// <summary>
        /// 根节点模板
        /// </summary>
        public IDataTemplate RootItemTemplate
        {
            get { return GetValue(RootItemTemplateProperty); }
            set { SetValue(RootItemTemplateProperty, value); }
        }
        /// <summary>
        /// 根节点项
        /// </summary>
        public object RootItemContent
        {
            get { return GetValue(RootItemContentProperty); }
            set { SetValue(RootItemContentProperty, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        static Breadcrumb()
        {
            ItemsPanelProperty.OverrideDefaultValue<Breadcrumb>(DefaultPanel);
            IsOverWrapProperty.Changed.AddClassHandler<Breadcrumb>((x, e) => x.OnOverWrapChanged(e));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnApplyTemplate(TemplateAppliedEventArgs e)
        {
            base.OnApplyTemplate(e);
            var p = e.NameScope.Find<ContentPresenter>("PART_RootItem");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="availableSize"></param>
        /// <returns></returns>
        protected override Size MeasureOverride(Size availableSize)
        {
            var size = base.MeasureOverride(availableSize);
            if(Presenter != null && Presenter.Panel != null && Presenter.Panel is BreadcrumbPanel bp)
            {
                if (bp.OverWrap) IsOverWrap = true;
                else IsOverWrap = false;
            }
            return size;
        }

        protected override Control CreateContainerForItemOverride(object item, int index, object recycleKey)
        {
            return new BreadcrumbItem()
            {
                DataContext = item,
                VerticalContentAlignment = Avalonia.Layout.VerticalAlignment.Center,
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void OnOverWrapChanged(AvaloniaPropertyChangedEventArgs e)
        {
            var newValue = (bool?)e.NewValue;
            if (newValue.HasValue)
            {
                PseudoClasses.Set(":overWrap", newValue.HasValue);
            }
        }
    }
}
