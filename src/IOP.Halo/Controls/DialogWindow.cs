﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Chrome;
using Avalonia.Controls.Metadata;
using Avalonia.Controls.Presenters;
using Avalonia.Controls.Primitives;
using Avalonia.Controls.Templates;
using Avalonia.Styling;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Xml.Linq;

namespace IOP.Halo
{
    /// <summary>
    /// 弹出框窗口
    /// </summary>
    [TemplatePart(Name = "PART_DragSpace", Type = typeof(Grid))]
    public class DialogWindow : Window
    {
        public static readonly StyledProperty<object> TitleIconProperty =
            AvaloniaProperty.Register<DialogWindow, object>(nameof(TitleIcon));
        public static readonly StyledProperty<IDataTemplate> TitleIconTemplateProperty =
            AvaloniaProperty.Register<DialogWindow, IDataTemplate>(nameof(TitleIconTemplate));
        public static readonly StyledProperty<object> WindowToolsProperty =
            AvaloniaProperty.Register<ContainerWindow, object>(nameof(WindowTools));
        public static readonly StyledProperty<IDataTemplate> WindowToolsTemplateProperty =
            AvaloniaProperty.Register<ContainerWindow, IDataTemplate>(nameof(WindowToolsTemplate));
        public static readonly DirectProperty<DialogWindow, ICommand> CloseCommandProperty =
            AvaloniaProperty.RegisterDirect<DialogWindow, ICommand>(nameof(CloseCommand), control => control.CloseCommand, (control, command) => control.CloseCommand = command, enableDataValidation: true);

        /// <summary>
        /// 标题栏图标
        /// </summary>
        public object TitleIcon
        {
            get { return GetValue(TitleIconProperty); }
            set { SetValue(TitleIconProperty, value); }
        }
        /// <summary>
        /// 窗口工具
        /// </summary>
        public object WindowTools
        {
            get { return GetValue(WindowToolsProperty); }
            set { SetValue(WindowToolsProperty, value); }
        }
        /// <summary>
        /// 标题栏图标模板
        /// </summary>
        public IDataTemplate TitleIconTemplate
        {
            get { return GetValue(TitleIconTemplateProperty); }
            set { SetValue(TitleIconTemplateProperty, value); }
        }
        /// <summary>
        /// 窗口工具模板
        /// </summary>
        public IDataTemplate WindowToolsTemplate
        {
            get { return GetValue(WindowToolsTemplateProperty); }
            set { SetValue(WindowToolsTemplateProperty, value); }
        }
        /// <summary>
        /// 关闭窗口命令
        /// </summary>
        public ICommand CloseCommand
        {
            get { return _CloseCommand; }
            set { SetAndRaise(CloseCommandProperty, ref _CloseCommand, value); }
        }


        protected ICommand _CloseCommand;
        protected Grid _TtitleBar;
        protected override Type StyleKeyOverride => typeof(DialogWindow);

        /// <summary>
        /// 
        /// </summary>
        public DialogWindow()
        {
            CloseCommand = ReactiveCommand.Create(() => Close());
        }

        protected override void OnApplyTemplate(TemplateAppliedEventArgs e)
        {
            base.OnApplyTemplate(e);
            var grid = e.NameScope.Find<Grid>("PART_DragSpace");
            if (grid != null)
            {
                if (_TtitleBar != null) _TtitleBar.PointerPressed -= Grid_PointerPressed;
                grid.PointerPressed += Grid_PointerPressed;
                _TtitleBar = grid;
            }
        }
        private void Grid_PointerPressed(object sender, Avalonia.Input.PointerPressedEventArgs e)
        {
            var point = e.GetCurrentPoint(this);
            if (point.Properties.IsLeftButtonPressed)
            {
                if (WindowState == WindowState.FullScreen)
                {
                    var width = Width;
                    double temp = point.Position.X;
                    WindowState = WindowState.Normal;
                    var rel = Width;
                    double x = temp * rel / width;
                    x = temp - x;
                    Position = new PixelPoint((int)(x), 0);
                }

                BeginMoveDrag(e);
                e.Handled = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="presenter"></param>
        /// <returns></returns>
        protected override bool RegisterContentPresenter(ContentPresenter presenter)
        {
            if (presenter.Name == "PART_TitleIcon")
            {
                return true;
            }
            else if (presenter.Name == "PART_WindowTools")
            {
                return true;
            }
            else return base.RegisterContentPresenter(presenter);
        }
    }
}
