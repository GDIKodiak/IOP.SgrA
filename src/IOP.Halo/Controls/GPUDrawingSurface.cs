﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Rendering;
using Avalonia.Platform;
using Avalonia.Rendering.Composition;
using Avalonia.Threading;
using Avalonia.VisualTree;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Avalonia.Input;

namespace IOP.Halo
{
    /// <summary>
    /// GPU渲染表面
    /// </summary>
    public abstract class GPUDrawingSurface : Control
    {
        private CompositionSurfaceVisual _visual;
        private Compositor _compositor;
        private ICompositionGpuInterop _gpuInterop;
        private string _info;
        private int _updateQueued;
        private int _initialized;
        private int _NeedStop;

        /// <summary>
        /// 
        /// </summary>
        public Action FrameAction { get; set; } = null;

        protected CompositionDrawingSurface Surface { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        protected ICompositionImportedGpuSemaphore RenderFinish {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        protected ICompositionImportedGpuSemaphore ImageAvailable {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        protected ICompositionGpuInterop GpuInterop { get => _gpuInterop; }

        protected virtual async void Initialize()
        {
            if (IsRenderInitialized) return;
            try
            {

                var selfVisual = ElementComposition.GetElementVisual(this)!;
                _compositor = selfVisual.Compositor;

                Surface = _compositor.CreateDrawingSurface();
                _visual = _compositor.CreateSurfaceVisual();
                _visual.Size = new(Bounds.Width, Bounds.Height);
                _visual.Surface = Surface;
                ElementComposition.SetElementChildVisual(this, _visual);
                var interop = await _compositor.TryGetCompositionGpuInterop();
                if (interop == null)
                {
                    _info = "Compositor doesn't support interop for the current backend";
                    return;
                }
                (bool s, string str) = InitializeGraphicsResources(_compositor, Surface, interop);
                if (s)
                {
                    _gpuInterop = interop;
                    IsRenderInitialized = true;
                }
                else _info = str;
            }
            catch (Exception e)
            {
                _info = e.Message;
            }
        }

        /// <summary>
        /// 渲染器是否完成初始化
        /// </summary>
        public virtual bool IsRenderInitialized 
        { 
            get
            {
                SpinWait wait = new SpinWait();
                int old = _initialized;
                while(Interlocked.CompareExchange(ref _initialized, old, old) != old)
                {
                    wait.SpinOnce();
                    old = _initialized;
                }
                return old == 1;
            } 
            set
            {
                Interlocked.Exchange(ref _initialized, value ? 1 : 0);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public virtual bool IsQueueUpdate
        {
            get
            {
                SpinWait wait = new SpinWait();
                int old = _updateQueued;
                while (Interlocked.CompareExchange(ref _updateQueued, old, old) != old)
                {
                    wait.SpinOnce();
                    old = _updateQueued;
                }
                return old == 1;
            }
            set
            {
                Interlocked.Exchange(ref _updateQueued, value ? 1 : 0);
            }
        }
        /// <summary>
        /// 呈现
        /// </summary>
        /// <param name="image"></param>
        public virtual void Present()
        {
            if (!IsRenderInitialized) throw new InvalidOperationException("The reander is not readly");
            if (IsRenderInitialized && IsQueueUpdate && _compositor != null)
            {
                Dispatcher.UIThread.InvokeAsync(() => _compositor?.RequestCompositionUpdate(UpdateFrame), DispatcherPriority.Render);
            }
        }
        /// <summary>
        /// 下一帧
        /// </summary>
        /// <returns></returns>
        public virtual void NextFrame()
        {
            if (!IsRenderInitialized) throw new InvalidOperationException("The reander is not readly");
            if (NeedStop) return;
            AcquireNextImage(_gpuInterop);
        }
        /// <summary>
        /// 
        /// </summary>
        public virtual void CallStop()
        {
            NeedStop = true;
            IsQueueUpdate = false;
        }

        /// <summary>
        /// 需要停止
        /// </summary>
        protected bool NeedStop
        {
            get
            {
                SpinWait wait = new SpinWait();
                int old = _NeedStop;
                while (Interlocked.CompareExchange(ref _NeedStop, old, old) != old)
                {
                    wait.SpinOnce();
                    old = _NeedStop;
                }
                return old == 1;
            }
            set { Interlocked.Exchange(ref _NeedStop, value ? 1 : 0); }
        }

        /// <summary>
        /// 初始化资源
        /// </summary>
        /// <param name="compositor"></param>
        /// <param name="compositionDrawingSurface"></param>
        /// <param name="gpuInterop"></param>
        /// <returns></returns>
        protected abstract (bool success, string info) InitializeGraphicsResources(Compositor compositor, 
            CompositionDrawingSurface compositionDrawingSurface, ICompositionGpuInterop gpuInterop);
        /// <summary>
        /// 释放资源
        /// </summary>
        protected abstract void FreeResource();
        /// <summary>
        /// 获取当前帧图像
        /// </summary>
        /// <returns></returns>
        protected abstract ICompositionImportedGpuImage GetCurrentFrameImage(ICompositionGpuInterop gpuInterop);
        /// <summary>
        /// 创建渲染完成信号量
        /// </summary>
        /// <param name="gpuInterop"></param>
        /// <returns></returns>
        protected abstract ICompositionImportedGpuSemaphore CreateRenderFinishSemaphore(ICompositionGpuInterop gpuInterop);
        /// <summary>
        /// 创建图像可达信号量
        /// </summary>
        /// <param name="gpuInterop"></param>
        /// <returns></returns>
        protected abstract ICompositionImportedGpuSemaphore CreateImageAvailableSemaphore(ICompositionGpuInterop gpuInterop);
        /// <summary>
        /// 申请下一张图片
        /// </summary>
        /// <param name="gpuInterop"></param>
        /// <param name="handle"></param>
        /// <returns></returns>
        protected abstract void AcquireNextImage(ICompositionGpuInterop gpuInterop);
        /// <summary>
        /// 更新帧数据
        /// </summary>
        protected virtual void UpdateFrame()
        {
            var root = this.GetVisualRoot();
            if (root == null) return;
            FrameAction?.Invoke();
            ICompositionImportedGpuImage image = GetCurrentFrameImage(_gpuInterop);
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                _ = Surface.UpdateWithKeyedMutexAsync(image, 1, 0);
            else
            {
                ImageAvailable ??= CreateImageAvailableSemaphore(_gpuInterop);
                RenderFinish ??= CreateRenderFinishSemaphore(_gpuInterop);
                _ = Surface.UpdateWithSemaphoresAsync(image, ImageAvailable, RenderFinish);
            }
            IsQueueUpdate = false;
        }

        protected override void OnDetachedFromVisualTree(VisualTreeAttachmentEventArgs e)
        {
            base.OnDetachedFromVisualTree(e);
            NeedStop = true;
            FreeResource();
        }
    }
}
