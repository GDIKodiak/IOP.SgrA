﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Metadata;
using Avalonia.Controls.Primitives;
using Avalonia.Media;
using Avalonia.Media.Imaging;
using Avalonia.Platform;
using Avalonia.Styling;
using Avalonia.Threading;
using System;
using System.Drawing;
using System.Threading;
using Image = Avalonia.Controls.Image;
using Size = Avalonia.Size;

namespace IOP.Halo
{
    /// <summary>
    /// 可渲染画布
    /// </summary>
    [TemplatePart(Name = "PART_Viewer", Type = typeof(Image))]
    public class RenderableCanvas : ContentControl, IDisposable
    {
        public static readonly StyledProperty<Size> ResolutionRatioProperty =
            AvaloniaProperty.Register<RenderableCanvas, Size>(nameof(ResolutionRatio));
        public static readonly StyledProperty<IImage> RenderTargetProperty =
            AvaloniaProperty.Register<RenderableCanvas, IImage>(nameof(RenderTarget));

        /// <summary>
        /// 分辨率
        /// </summary>
        public Size ResolutionRatio
        {
            get { return GetValue(ResolutionRatioProperty); }
            protected set { SetValue(ResolutionRatioProperty, value); }
        }
        /// <summary>
        /// 渲染目标
        /// </summary>
        public IImage RenderTarget
        {
            get { return GetValue(RenderTargetProperty); }
            protected set { SetValue(RenderTargetProperty, value); }
        }
        /// <summary>
        /// 总画布字节数
        /// </summary>
        public int TotalBytesSize { get; protected set; }
        /// <summary>
        /// 内存块数量
        /// </summary>
        public const int BlockCount = 3;

#nullable disable
        /// <summary>
        /// 
        /// </summary>
        protected override Type StyleKeyOverride => typeof(RenderableCanvas);
        protected Image _Viewer;
        protected NativeMemoryManager<byte> _MemoryManager;
        protected Memory<byte> _Buffer;
        protected WriteableBitmap _Bitmap;
        protected readonly object SyncRoot = new();
#nullable enable
        private int BlockIndex = 0;

        /// <summary>
        /// 创建RGBA8888格式渲染目标
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="dpi"></param>
        public void CreateRgbaRenderTarget(double width, double height)
        {
            WriteableBitmap bitmap = new(new PixelSize((int)width, (int)height), new Vector(96, 96),
                PixelFormat.Rgba8888,
                AlphaFormat.Premul);
            ResolutionRatio = new Size(width, height);
            lock (SyncRoot)
            {
                if (RenderTarget != null) DisposeRenderTarget();
                using var buffer = bitmap.Lock();
                TotalBytesSize = buffer.RowBytes * buffer.Size.Height;
                BlockIndex = 0; int total = TotalBytesSize * BlockCount;
                _MemoryManager = new NativeMemoryManager<byte>((uint)total);
                _Buffer = _MemoryManager.Memory;
                _Bitmap = bitmap;
                RenderTarget = _Bitmap;
            }
        }
        /// <summary>
        /// 获取下一个帧内存
        /// </summary>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        public Memory<byte> NextFrameMemory()
        {
            if (_Bitmap == null) throw new InvalidOperationException("Invalid operation, RenderTarget was not create");
            SpinWait wait = new();
            int index = BlockIndex; int next = (index + 1) % BlockCount;
            while (Interlocked.CompareExchange(ref BlockIndex, next, index) != index)
            {
                wait.SpinOnce();
                index = BlockIndex; next = (index + 1) % BlockCount;
            }
            return _Buffer.Slice(next * TotalBytesSize, TotalBytesSize);
        }
        /// <summary>
        /// 呈现当前帧内存的数据
        /// </summary>
        public void Present()
        {
            if (_Bitmap == null) throw new InvalidOperationException("Invalid operation, RenderTarget was not create");
            SpinWait wait = new();
            int index = BlockIndex;
            while (Interlocked.CompareExchange(ref BlockIndex, index, index) != index)
            {
                wait.SpinOnce();
                index = BlockIndex;
            }
            int size = TotalBytesSize;
            var local = _Buffer.Slice(index * size, size);
            Dispatcher.UIThread.Invoke(() =>
            {
                unsafe
                {
                    fixed (byte* p = &local.Span.GetPinnableReference())
                    {
                        using var buffer = _Bitmap.Lock();
                        Buffer.MemoryCopy(p, buffer.Address.ToPointer(), buffer.RowBytes * buffer.Size.Height, size);
                    }
                }
                _Viewer?.InvalidateVisual();
            }, DispatcherPriority.Render);
        }
        /// <summary>
        /// 直接将特定内存内的数据进行呈现
        /// </summary>
        /// <param name="ptr"></param>
        /// <exception cref="InvalidOperationException"></exception>
        public void Present(BufferHandle handle)
        {
            if (_Bitmap == null) throw new InvalidOperationException("Invalid operation, RenderTarget was not create");
            int size = TotalBytesSize;
            IntPtr ptr = handle.Pointer;
            if (ptr == IntPtr.Zero) return;
            unsafe
            {
                byte* p = (byte*)ptr.ToPointer();
                using var buffer = _Bitmap.Lock();
                Buffer.MemoryCopy(p, buffer.Address.ToPointer(), buffer.RowBytes * buffer.Size.Height, size);
                handle.IsUsed = true;
            }
            Dispatcher.UIThread.Invoke(() =>
            {
                _Viewer?.InvalidateVisual();
            }, DispatcherPriority.Render);
        }
        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            DisposeRenderTarget();
        }

#nullable disable
        /// <summary>
        /// 销毁当前渲染目标
        /// </summary>
        public void DisposeRenderTarget()
        {
            if (RenderTarget == null) return;
            lock (SyncRoot)
            {
                var oldMap = _Bitmap;
                _Bitmap = null; RenderTarget = null; oldMap.Dispose();
                _Buffer = null; _MemoryManager.Dispose(); _MemoryManager = null;
            }
        }
#nullable enable

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnApplyTemplate(TemplateAppliedEventArgs e)
        {
            base.OnApplyTemplate(e);
            var viewer = e.NameScope.Find<Image>("PART_Viewer");
            if (viewer != null)
            {
                _Viewer = viewer;
                viewer.Stretch = Stretch.Uniform;
                viewer.StretchDirection = StretchDirection.Both;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="finalSize"></param>
        /// <returns></returns>
        protected override Size ArrangeOverride(Size finalSize)
        {
            return base.ArrangeOverride(finalSize);
        }

        protected override void OnDetachedFromVisualTree(VisualTreeAttachmentEventArgs e)
        {
            base.OnDetachedFromVisualTree(e);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class BufferHandle
    {
        private long _version;

        /// <summary>
        /// 
        /// </summary>
        public long Version { get => _version; }
        /// <summary>
        /// 
        /// </summary>
        public IntPtr Pointer { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsUsed {  get; internal set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pointer"></param>
        public BufferHandle(IntPtr pointer)
        {
            Pointer = pointer;
            IsUsed = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pointer"></param>
        /// <param name="version"></param>
        public void NewVersion(IntPtr pointer, long version)
        {
            Pointer = pointer;
            IsUsed = false;
            _version = version;
        }
    }
}
