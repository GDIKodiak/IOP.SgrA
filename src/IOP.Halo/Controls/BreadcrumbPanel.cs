﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.LogicalTree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.Halo
{
    /// <summary>
    /// 面包屑导航面板
    /// </summary>
    public class BreadcrumbPanel : Panel
    {
        /// <summary>
        /// 内容是否溢出
        /// </summary>
        public bool OverWrap { get; private set; } = false;
        /// <summary>
        /// 溢出差值
        /// </summary>
        public double OverWrapDifferent { get; private set; } = 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="availableSize"></param>
        /// <returns></returns>
        protected override Size MeasureOverride(Size availableSize)
        {
            var size = new Size(0, availableSize.Height);
            foreach(var child in  Children)
            {
                child.Measure(availableSize);
                var h = Math.Max(size.Height, child.DesiredSize.Height);
                var w = child.DesiredSize.Width + size.Width;
                size = new Size(w, h);
            }
            if (size.Width > availableSize.Width)
            {
                OverWrap = true;
                OverWrapDifferent = size.Width - availableSize.Width;
            }
            else
            {
                OverWrap = false;
                OverWrapDifferent = 0;
            }
            return size;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="finalSize"></param>
        /// <returns></returns>
        protected override Size ArrangeOverride(Size finalSize)
        {
            int last = Children.Count - 1; bool firstE = true;
            double x = DesiredSize.Width;
            if(OverWrap)
            {
                double min = 0; int firstIndex = 0; bool foundFirst = false;
                for (int i = last; i >= 0; i--)
                {
                    var child = Children[i];
                    if (firstE && child is BreadcrumbItem item)
                    {
                        item.ShowSeparator = false;
                        firstE = !firstE;
                    }
                    double local = x - child.DesiredSize.Width;
                    x -= child.DesiredSize.Width;
                    if (local < min)
                    {
                        child.Arrange(new Rect(0, 0, 0, 0));
                        if (!foundFirst)
                        {
                            foundFirst = !foundFirst;
                            firstIndex = i + 1;
                        }
                    }
                }
                x = 0;
                for(int i = firstIndex; i <= last; i++)
                {
                    var child = Children[i];
                    child.Arrange(new Rect(x, 0, child.DesiredSize.Width, finalSize.Height));
                    x += child.DesiredSize.Width;
                }
            }
            else
            {
                for (int i = last; i >= 0; i--)
                {
                    var child = Children[i];
                    if (firstE && child is BreadcrumbItem item)
                    {
                        item.ShowSeparator = false;
                        firstE = !firstE;
                    }
                    x -= child.DesiredSize.Width;
                    child.Arrange(new Rect(x, 0, child.DesiredSize.Width, finalSize.Height));
                }
            }
            return finalSize;
        }
    }
}
