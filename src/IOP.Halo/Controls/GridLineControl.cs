﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Shapes;
using Avalonia.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.Halo
{
    /// <summary>
    /// 网格线控件
    /// </summary>
    public class GridLineControl : Control
    {
        public static readonly StyledProperty<ISolidColorBrush> GridLineBrushProperty =
            AvaloniaProperty.Register<GridLineControl, ISolidColorBrush>(nameof(GridLineBrush), new SolidColorBrush(Color.Parse("#111211"), 0.8));
        public static readonly DirectProperty<GridLineControl, int> GridLineSizeProperty =
            AvaloniaProperty.RegisterDirect<GridLineControl, int>(nameof(GridLineSize),
                o => o._GridLineSize,
                OnGridLineSizeChanged, 20);
        /// <summary>
        /// 网格线笔刷
        /// </summary>
        public ISolidColorBrush GridLineBrush
        {
            get { return GetValue(GridLineBrushProperty); }
            set { SetValue(GridLineBrushProperty, value); }
        }
        /// <summary>
        /// 网格线笔刷
        /// </summary>
        public int GridLineSize
        {
            get { return _GridLineSize; }
            set { SetAndRaise(GridLineSizeProperty, ref _GridLineSize, value); }
        }

        private StreamGeometry _GridGeometry;
        private int _GridLineSize = 20;
        /// <summary>
        /// 
        /// </summary>
        public GridLineControl()
        {
            StreamGeometry geometry = new StreamGeometry();
            using (StreamGeometryContext c = geometry.Open())
            {
                c.BeginFigure(new Avalonia.Point(0, 0), true);
                c.LineTo(new Avalonia.Point(20, 0));
                c.LineTo(new Avalonia.Point(20, 20));
                c.LineTo(new Avalonia.Point(0, 20));
                c.LineTo(new Avalonia.Point(0, 0));
            }
            _GridGeometry = geometry;
        }

        public override void Render(DrawingContext context)
        {
            DrawBackgroundGrid(context);
            base.Render(context);
        }

        /// <summary>
        /// 绘制背景网格线段
        /// </summary>
        /// <param name="context"></param>
        private void DrawBackgroundGrid(DrawingContext context)
        {
            Path path = new()
            {
                Data = _GridGeometry,
                Stroke = GridLineBrush,
                StrokeThickness = 1,
            };
            VisualBrush brush = new VisualBrush(path);
            brush.TileMode = TileMode.Tile;
            brush.Stretch = Stretch.Fill;
            brush.AlignmentX = AlignmentX.Left; brush.AlignmentY = AlignmentY.Top;
            brush.SourceRect = new RelativeRect(0, 0, GridLineSize, GridLineSize, RelativeUnit.Absolute);
            brush.DestinationRect = new RelativeRect(0, 0, GridLineSize, GridLineSize, RelativeUnit.Absolute);
            context.DrawRectangle(brush, new Pen(), new Rect(0, 0, Bounds.Width, Bounds.Height));
        }

        private static void OnGridLineSizeChanged(GridLineControl o, int v)
        {
            if (v <= 0) v = 20;
            o._GridLineSize = v;
            StreamGeometry geometry = new StreamGeometry();
            using (StreamGeometryContext c = geometry.Open())
            {
                c.BeginFigure(new Avalonia.Point(0, 0), true);
                c.LineTo(new Avalonia.Point(v, 0));
                c.LineTo(new Avalonia.Point(v, v));
                c.LineTo(new Avalonia.Point(0, v));
                c.LineTo(new Avalonia.Point(0, 0));
            }
            o._GridGeometry = geometry;
        }
    }
}
