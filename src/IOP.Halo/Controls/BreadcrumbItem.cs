﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Metadata;
using Avalonia.Data;
using Avalonia.Input;
using Avalonia.Interactivity;
using Avalonia.LogicalTree;
using Avalonia.Media;
using Avalonia.Styling;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace IOP.Halo
{
    /// <summary>
    /// 
    /// </summary>
    [PseudoClasses(":hiddenSeparator")]
    public class BreadcrumbItem : ContentControl
    {
        /// <summary>
        /// 
        /// </summary>
        public static readonly StyledProperty<bool> ShowSeparatorProperty =
            AvaloniaProperty.Register<BreadcrumbItem, bool>(nameof(ShowSeparator), true);
        public static readonly StyledProperty<IBrush> SeparatorBrushProperty =
            AvaloniaProperty.Register<BreadcrumbItem, IBrush>(nameof(SeparatorBrush), Brushes.Black);
        public static readonly DirectProperty<Button, ICommand> CommandProperty =
            AvaloniaProperty.RegisterDirect<Button, ICommand>(nameof(Command),
                button => button.Command, (button, command) => button.Command = command, enableDataValidation: true);

        private ICommand _command;
        private bool _commandCanExecute = true;
        static BreadcrumbItem()
        {
            ShowSeparatorProperty.Changed.AddClassHandler<BreadcrumbItem>((x, e) => x.OnShowSeparatorChanged(e));
            CommandProperty.Changed.Subscribe(CommandChanged);
        }

        /// <summary>
        /// 是否显示分隔符
        /// </summary>
        public bool ShowSeparator
        {
            get { return GetValue(ShowSeparatorProperty); }
            set { SetValue(ShowSeparatorProperty, value); }
        }
        /// <summary>
        /// 分隔符笔刷
        /// </summary>
        public IBrush SeparatorBrush
        {
            get { return GetValue(SeparatorBrushProperty); }
            set { SetValue(SeparatorBrushProperty, value); }
        }
        /// <summary>
        /// 选项卡被点击时事件
        /// </summary>
        public ICommand Command
        {
            get { return _command; }
            set { SetAndRaise(CommandProperty, ref _command, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        protected override Type StyleKeyOverride => typeof(BreadcrumbItem);

        protected override void OnAttachedToLogicalTree(LogicalTreeAttachmentEventArgs e)
        {
            base.OnAttachedToLogicalTree(e);
            if (Command != null)
            {
                Command.CanExecuteChanged += CanExecuteChanged;
                CanExecuteChanged(this, EventArgs.Empty);
            }
        }
        protected override void OnDetachedFromLogicalTree(LogicalTreeAttachmentEventArgs e)
        {
            base.OnDetachedFromLogicalTree(e);

            if (Command != null)
            {
                Command.CanExecuteChanged -= CanExecuteChanged;
            }
        }
        protected override void OnPointerPressed(PointerPressedEventArgs e)
        {
            base.OnPointerPressed(e);

            if (e.GetCurrentPoint(this).Properties.IsLeftButtonPressed)
            {
                e.Handled = true;
                OnClick();
            }
        }
        //protected override void UpdateDataValidation<T>(AvaloniaProperty<T> property, BindingValue<T> value)
        //{
        //    base.UpdateDataValidation(property, value);
        //    if (property == CommandProperty)
        //    {
        //        if (value.Type == BindingValueType.BindingError)
        //        {
        //            if (_commandCanExecute)
        //            {
        //                _commandCanExecute = false;
        //                UpdateIsEffectivelyEnabled();
        //            }
        //        }
        //    }
        //}
        /// <summary>
        /// Invokes the <see cref="Click"/> event.
        /// </summary>
        protected virtual void OnClick()
        {
            if (IsEffectivelyEnabled)
            {
                if (Command?.CanExecute(null) == true)
                {
                    Command.Execute(null);
                }
            }
        }

        /// <summary>
        /// Called when the <see cref="ICommand.CanExecuteChanged"/> event fires.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event args.</param>
        private void CanExecuteChanged(object sender, EventArgs e)
        {
            var canExecute = Command == null;

            if (canExecute != _commandCanExecute)
            {
                _commandCanExecute = canExecute;
                UpdateIsEffectivelyEnabled();
            }
        }
        /// <summary>
        /// Called when the <see cref="Command"/> property changes.
        /// </summary>
        /// <param name="e">The event args.</param>
        private static void CommandChanged(AvaloniaPropertyChangedEventArgs e)
        {
            if (e.Sender is BreadcrumbItem item)
            {
                if (((ILogical)item).IsAttachedToLogicalTree)
                {
                    if (e.OldValue is ICommand oldCommand)
                    {
                        oldCommand.CanExecuteChanged -= item.CanExecuteChanged;
                    }

                    if (e.NewValue is ICommand newCommand)
                    {
                        newCommand.CanExecuteChanged += item.CanExecuteChanged;
                    }
                }

                item.CanExecuteChanged(item, EventArgs.Empty);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void OnShowSeparatorChanged(AvaloniaPropertyChangedEventArgs e)
        {
            var newValue = (bool?)e.NewValue;
            if (newValue.HasValue)
            {
                PseudoClasses.Set(":hiddenSeparator", !newValue.Value);
            }
        }
    }
}
