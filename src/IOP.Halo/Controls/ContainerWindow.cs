﻿using Avalonia;
using Avalonia.Collections;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Controls.Chrome;
using Avalonia.Controls.Metadata;
using Avalonia.Controls.Presenters;
using Avalonia.Controls.Primitives;
using Avalonia.Controls.Templates;
using Avalonia.Layout;
using Avalonia.LogicalTree;
using Avalonia.Metadata;
using Avalonia.Styling;
using Avalonia.Threading;
using DynamicData;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Drawing;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace IOP.Halo
{
    /// <summary>
    /// 容器控件
    /// </summary>
    [TemplatePart(Name = "PART_DragSpace", Type = typeof(Grid))]
    public class ContainerWindow : Window
    {
        public static readonly StyledProperty<object> TitleIconProperty =
            AvaloniaProperty.Register<ContainerWindow, object>(nameof(TitleIcon));
        public static readonly StyledProperty<object> WindowToolsProperty =
            AvaloniaProperty.Register<ContainerWindow, object>(nameof(WindowTools));
        public static readonly StyledProperty<object> ShortcutBarProperty =
            AvaloniaProperty.Register<ContainerWindow, object>(nameof(ShortcutBar));
        public static readonly StyledProperty<IDataTemplate> TitleIconTemplateProperty =
            AvaloniaProperty.Register<ContainerWindow, IDataTemplate>(nameof(TitleIconTemplate));
        public static readonly StyledProperty<IDataTemplate> WindowToolsTemplateProperty =
            AvaloniaProperty.Register<ContainerWindow, IDataTemplate>(nameof(WindowToolsTemplate));
        public static readonly StyledProperty<IDataTemplate> ShortcutBarTemplateProperty =
            AvaloniaProperty.Register<ContainerWindow, IDataTemplate>(nameof(ShortcutBarTemplate));
        public static readonly DirectProperty<ContainerWindow, ICommand> MinusCommandProperty =
            AvaloniaProperty.RegisterDirect<ContainerWindow, ICommand>(nameof(MinusCommand), control => control.MinusCommand, (control, command) => control.MinusCommand = command, enableDataValidation: true);
        public static readonly DirectProperty<ContainerWindow, ICommand> WindowedCommandProperty =
            AvaloniaProperty.RegisterDirect<ContainerWindow, ICommand>(nameof(WindowedCommand), control => control.WindowedCommand, (control, command) => control.WindowedCommand = command, enableDataValidation: true);
        public static readonly DirectProperty<ContainerWindow, ICommand> CloseCommandProperty =
            AvaloniaProperty.RegisterDirect<ContainerWindow, ICommand>(nameof(CloseCommand), control => control.CloseCommand, (control, command) => control.CloseCommand = command, enableDataValidation: true);
        public static readonly DirectProperty<ContainerWindow, TitleBarMenuDescriptor> TitleBarMenusProperty =
            AvaloniaProperty.RegisterDirect<ContainerWindow, TitleBarMenuDescriptor>(nameof(TitleBarMenus), control => control.TitleBarMenus, (contorl, menus) => contorl.TitleBarMenus = menus, enableDataValidation: true);
        public static readonly DirectProperty<ContainerWindow, ObservableCollection<WorkspaceDescriptor>> WorkspacesProperty =
            AvaloniaProperty.RegisterDirect<ContainerWindow, ObservableCollection<WorkspaceDescriptor>>(nameof(Workspaces), control => control.Workspaces, (contorl, spacecs) => contorl.Workspaces = spacecs, enableDataValidation: true);
        public static readonly DirectProperty<ContainerWindow, WorkspaceDescriptor> ActivatedWorkspaceProperty =
            AvaloniaProperty.RegisterDirect<ContainerWindow, WorkspaceDescriptor>(nameof(ActivatedWorkspace), control => control.ActivatedWorkspace, (control, space) => control.ActivatedWorkspace = space, enableDataValidation: true
                , defaultBindingMode: Avalonia.Data.BindingMode.TwoWay);

        /// <summary>
        /// 标题栏图标
        /// </summary>
        public object TitleIcon
        {
            get { return GetValue(TitleIconProperty); }
            set { SetValue(TitleIconProperty, value); }
        }
        /// <summary>
        /// 窗口工具
        /// </summary>
        public object WindowTools
        {
            get { return GetValue(WindowToolsProperty); }
            set { SetValue(WindowToolsProperty, value); }
        }
        /// <summary>
        /// 快捷栏
        /// </summary>
        public object ShortcutBar
        {
            get { return GetValue(ShortcutBarProperty); }
            set { SetValue(ShortcutBarProperty, value); }
        }
        /// <summary>
        /// 标题栏图标模板
        /// </summary>
        public IDataTemplate TitleIconTemplate
        {
            get { return GetValue(TitleIconTemplateProperty); }
            set { SetValue(TitleIconTemplateProperty, value); }
        }
        /// <summary>
        /// 窗口工具模板
        /// </summary>
        public IDataTemplate WindowToolsTemplate
        {
            get { return GetValue(WindowToolsTemplateProperty); }
            set { SetValue(WindowToolsTemplateProperty, value); }
        }
        /// <summary>
        /// 快捷栏模板
        /// </summary>
        public IDataTemplate ShortcutBarTemplate
        {
            get { return GetValue(ShortcutBarTemplateProperty); }
            set { SetValue(ShortcutBarTemplateProperty, value); }
        }
        /// <summary>
        /// 标题栏菜单
        /// </summary>
        public TitleBarMenuDescriptor TitleBarMenus
        {
            get { return _TitleBarMenus; }
            set { if (value != null) SetAndRaise(TitleBarMenusProperty, ref _TitleBarMenus, value); }
        }
        /// <summary>
        /// 工作空间集合
        /// </summary>
        public ObservableCollection<WorkspaceDescriptor> Workspaces
        {
            get { return _Workspaces; }
            set { if (value != null) SetAndRaise(WorkspacesProperty, ref _Workspaces, value); }
        }
        /// <summary>
        /// 激活的工作空间
        /// </summary>
        public WorkspaceDescriptor ActivatedWorkspace
        {
            get { return _ActivatedWorkspace; }
            set { if (value != null) SetAndRaise(ActivatedWorkspaceProperty, ref _ActivatedWorkspace, value); }
        }
        /// <summary>
        /// 最小化窗口命令
        /// </summary>
        public ICommand MinusCommand
        {
            get { return _MinusCommand; }
            set { SetAndRaise(MinusCommandProperty, ref _MinusCommand, value); }
        }
        /// <summary>
        /// 窗口或最大化命令
        /// </summary>
        public ICommand WindowedCommand
        {
            get { return _WindowedCommand; }
            set { SetAndRaise(WindowedCommandProperty, ref _WindowedCommand, value); }
        }
        /// <summary>
        /// 关闭窗口命令
        /// </summary>
        public ICommand CloseCommand
        {
            get { return _CloseCommand; }
            set { SetAndRaise(CloseCommandProperty, ref _CloseCommand, value); }
        }

#nullable disable
        protected override Type StyleKeyOverride => typeof(ContainerWindow);
        /// <summary>
        /// 
        /// </summary>
        public ContainerWindow()
        {
            MinusCommand = ReactiveCommand.Create(() => WindowState = WindowState.Minimized);
            WindowedCommand = ReactiveCommand.Create(() =>
            {
                if (WindowState == WindowState.FullScreen) WindowState = WindowState.Normal;
                else if (WindowState == WindowState.Normal) WindowState = WindowState.FullScreen;
            });
            CloseCommand = ReactiveCommand.Create(() => Close());
            _DefaultWorkspaceCloseCommand = ReactiveCommand.Create<WorkspaceDescriptor>((space) =>
            {
                if (space == null) return;
                Task.Factory.StartNew(async () =>
                {
                    await space.CloseWorkspace();
                }).ContinueWith((t) =>
                {
                    Dispatcher.UIThread.Post(() => 
                    {
                        ActivatedWorkspace = Workspaces.Count >= 1 ? Workspaces[0] : null;
                        Workspaces.Remove(space); 
                    });
                });
            });
        }
#nullable enable
        /// <summary>
        /// 新工作空间
        /// </summary>
        /// <param name="descriptor"></param>
        public void NewWorkspace(WorkspaceDescriptor descriptor, bool fource = false)
        {
            if(descriptor == null) return;
            var space = descriptor.Workspace; 
            if (space == null) return;
            if (descriptor.CouldClose) descriptor.CloseCommand = _DefaultWorkspaceCloseCommand;
            Task.Factory.StartNew(async () =>
            {
                await space.Initialization();
            }).ContinueWith((t) =>
            {
                if (t.IsCompletedSuccessfully)
                {
                    Dispatcher.UIThread.Post(() =>
                    {
                        Workspaces.Add(descriptor);
                        if (fource) ActivatedWorkspace = descriptor;
                    });
                }
            });
        }
        /// <summary>
        /// 异步添加新工作空间
        /// </summary>
        /// <param name="descriptor"></param>
        /// <returns></returns>
        public async ValueTask NewWorkspaceAsync(WorkspaceDescriptor descriptor)
        {
            if (descriptor == null) return;
            var space = descriptor.Workspace;
            if (space == null) return;
            if (descriptor.CouldClose && descriptor.CloseCommand == null) descriptor.CloseCommand = _DefaultWorkspaceCloseCommand;
            var t = Task.Factory.StartNew(async () =>
            {
                await space.Initialization();
            }).ContinueWith((t) =>
            {
                if (t.IsCompletedSuccessfully)
                {
                    Dispatcher.UIThread.Post(() =>
                    {
                        Workspaces.Add(descriptor);
                    });
                }
            });
            await t;
        }

#nullable disable

        protected ICommand _MinusCommand;
        protected ICommand _WindowedCommand;
        protected ICommand _CloseCommand;
        protected ICommand _DefaultWorkspaceCloseCommand;
        protected TitleBarMenuDescriptor _TitleBarMenus = new();
        protected ObservableCollection<WorkspaceDescriptor> _Workspaces = new();
        protected WorkspaceDescriptor _ActivatedWorkspace;
        protected Grid _TtitleBar;
#nullable enable

        /// <summary>
        /// 
        /// </summary>
        /// <param name="presenter"></param>
        /// <returns></returns>
        protected override bool RegisterContentPresenter(ContentPresenter presenter)
        {
            if(presenter.Name == "PART_TitleIcon")
            {
                return true;
            }
            else if(presenter.Name == "PART_WindowTools")
            {
                return true;
            }
            else return base.RegisterContentPresenter(presenter);
        }

        protected override void OnApplyTemplate(TemplateAppliedEventArgs e)
        {
            base.OnApplyTemplate(e);
            var grid = e.NameScope.Find<Grid>("PART_DragSpace");
            if(grid != null)
            {
                if (_TtitleBar != null) _TtitleBar.PointerPressed -= Grid_PointerPressed;
                grid.PointerPressed += Grid_PointerPressed;
                _TtitleBar = grid;
            }
        }

        private void Grid_PointerPressed(object? sender, Avalonia.Input.PointerPressedEventArgs e)
        {
            var point = e.GetCurrentPoint(this);
            if(point.Properties.IsLeftButtonPressed)
            {
                if (WindowState == WindowState.FullScreen)
                {
                    var width = Width;
                    double temp = point.Position.X;
                    WindowState = WindowState.Normal;
                    var rel = Width;
                    double x = temp * rel / width;
                    x = temp - x;
                    Position = new PixelPoint((int)(x), 0);
                }
                
                BeginMoveDrag(e);
                e.Handled = true;
            }
        }
    }
}
