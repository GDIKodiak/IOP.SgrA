using Avalonia;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Interactivity;
using Avalonia.LogicalTree;
using Avalonia.Markup.Xaml.Templates;
using DynamicData;
using IOP.Extension;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Runtime.InteropServices;
using System.Windows.Input;

namespace IOP.Halo
{
    /// <summary>
    /// 
    /// </summary>
    public partial class FileExplorerControl : UserControl
    {
        public static readonly DirectProperty<FileExplorerControl, ObservableCollection<FileInfoModel>>
            FileModelsProperty = AvaloniaProperty.RegisterDirect<FileExplorerControl, ObservableCollection<FileInfoModel>>(nameof(FileModels), (control) => control.FileModels, (control, value) => control.FileModels = value, enableDataValidation: true);
        public static readonly DirectProperty<FileExplorerControl, ObservableCollection<TreeFileInfoModel>>
            BreadcrumbDirectoryProperty = AvaloniaProperty.RegisterDirect<FileExplorerControl, ObservableCollection<TreeFileInfoModel>>(nameof(BreadcrumbDirectory), (control) => control.BreadcrumbDirectory, (control, value) => control.BreadcrumbDirectory = value, enableDataValidation: true);
        public static readonly DirectProperty<FileExplorerControl, ObservableCollection<TreeFileInfoModel>>
            TreeDirectoryProperty = AvaloniaProperty.RegisterDirect<FileExplorerControl, ObservableCollection<TreeFileInfoModel>>(nameof(TreeDirectory), (control) => control.TreeDirectory, (control, value) => control.TreeDirectory = value, enableDataValidation: true);
        public static readonly DirectProperty<FileExplorerControl, string>
            SelectedPathProperty = AvaloniaProperty.RegisterDirect<FileExplorerControl, string>(nameof(SelectedPath), (control) => control.SelectedPath, (control, value) => control.SelectedPath = value, enableDataValidation: true);
        public static readonly DirectProperty<FileExplorerControl, string>
            SelectedFileNameProperty = AvaloniaProperty.RegisterDirect<FileExplorerControl, string>(nameof(SelectedFileName), (control) => control.SelectedFileName, (control, value) => control.SelectedFileName = value, enableDataValidation: true);
        public static readonly DirectProperty<FileExplorerControl, string>
            BasePathProperty = AvaloniaProperty.RegisterDirect<FileExplorerControl, string>(nameof(BasePath), (control) => control.BasePath, (control, value) => control.BasePath = value, enableDataValidation: true);
        public static readonly StyledProperty<ICommand>
            BackCommandProperty = AvaloniaProperty.Register<FileExplorerControl, ICommand>(nameof(BackCommand));
        public static readonly StyledProperty<ICommand>
            ForwardCommandProperty = AvaloniaProperty.Register<FileExplorerControl, ICommand>(nameof(ForwardCommand));
        public static readonly StyledProperty<ICommand>
            CancelCommandProperty = AvaloniaProperty.Register<FileExplorerControl, ICommand>(nameof(CancelCommand));
        public static readonly StyledProperty<ICommand>
            EnterCommandProperty = AvaloniaProperty.Register<FileExplorerControl, ICommand>(nameof(EnterCommand));
        private ObservableCollection<FileInfoModel> _FileModels = new ObservableCollection<FileInfoModel>();
        private ObservableCollection<TreeFileInfoModel> _BreadcrumbDirectory = new ObservableCollection<TreeFileInfoModel>();
        private ObservableCollection<TreeFileInfoModel> _TreeDirectory = new ObservableCollection<TreeFileInfoModel>();
        private string _BasePath = string.Empty;
        private string _SelectedPath = string.Empty;
        private string _SelectedFileName = string.Empty;
        private const string ROOT = "ROOT";

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<FileInfoModel> FileModels
        {
            get { return _FileModels; }
            set { if (value != null) SetAndRaise(FileModelsProperty, ref _FileModels, value); }
        }
        /// <summary>
        /// 当前面包屑文件夹
        /// </summary>
        public ObservableCollection<TreeFileInfoModel> BreadcrumbDirectory
        {
            get { return _BreadcrumbDirectory; }
            set { if (value != null) SetAndRaise(BreadcrumbDirectoryProperty, ref _BreadcrumbDirectory, value); }
        }
        /// <summary>
        /// 文件夹列表
        /// </summary>
        public ObservableCollection<TreeFileInfoModel> TreeDirectory
        {
            get { return _TreeDirectory; }
            set { if (value != null) SetAndRaise(TreeDirectoryProperty, ref _TreeDirectory, value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public FileInfoModel SelectedItem
        {
            get => _SelectedItem;
            set
            {
                if(value != null)
                {
                    _SelectedItem = value;
                    if (!value.IsDirectory)
                    {
                        SelectedPath = value.Path;
                        SelectedFileName = value.Name;
                    }
                }
            }
        }
        /// <summary>
        /// 基地址
        /// </summary>
        public string BasePath
        {
            get { return _BasePath; }
            set { if (!string.IsNullOrEmpty(value)) SetAndRaise(BasePathProperty, ref _BasePath, value); }
        }
        /// <summary>
        /// 被选中的地址
        /// </summary>
        public string SelectedPath 
        {
            get => _SelectedPath;
            set { if (!string.IsNullOrEmpty(value)) SetAndRaise(SelectedPathProperty, ref _SelectedPath, value); }
        }
        /// <summary>
        /// 被选中的文件名称
        /// </summary>
        public string SelectedFileName
        {
            get => _SelectedFileName;
            set { if (!string.IsNullOrEmpty(value)) SetAndRaise(SelectedFileNameProperty, ref _SelectedFileName, value); }
        }
        /// <summary>
        /// 向后导航命令
        /// </summary>
        public ICommand BackCommand
        {
            get { return GetValue(BackCommandProperty); }
            set { SetValue(BackCommandProperty, value); }
        }
        /// <summary>
        /// 向前导航
        /// </summary>
        public ICommand ForwardCommand
        {
            get { return GetValue(ForwardCommandProperty); }
            set { SetValue(ForwardCommandProperty, value); }
        }
        /// <summary>
        /// 取消命令
        /// </summary>
        public ICommand CancelCommand 
        {
            get { return GetValue(CancelCommandProperty); }
            set { SetValue(CancelCommandProperty, value); }
        }
        /// <summary>
        /// 确定命令
        /// </summary>
        public ICommand EnterCommand
        {
            get { return GetValue(EnterCommandProperty); }
            set { SetValue(EnterCommandProperty, value); }
        }
        /// <summary>
        /// 是否允许点击确定
        /// </summary>
        public IObservable<bool> CounldEnter
        {
            get; private set;
        }

        protected LinkedList<string> NavHistory { get; set; } = new LinkedList<string>();
        protected FileInfoModel _SelectedItem = null;

        /// <summary>
        /// 
        /// </summary>
        public FileExplorerControl()
        {
            InitializeComponent();
            DataContext = this;
            filesView.LoadingRow += FilesView_LoadingRow;
            filesView.UnloadingRow += FilesView_UnloadingRow;
            IObservable<bool> c1 = this.WhenAnyValue<FileExplorerControl, bool, string>(x => x.BasePath, p => CouldBack());
            IObservable<bool> c2 = this.WhenAnyValue<FileExplorerControl, bool, string>(x => x.BasePath, p => CoundForward());
            CounldEnter = this.WhenAnyValue<FileExplorerControl, bool, string>(x => x.SelectedPath, p => EnterCheck(p));
            BackCommand = ReactiveCommand.Create(Back, c1);
            ForwardCommand = ReactiveCommand.Create(Forward, c2);
        }

        public void OnRootPointerPressed(object sender, PointerPressedEventArgs e)
        {
            NavToRoot();
        }

        protected override void OnAttachedToLogicalTree(LogicalTreeAttachmentEventArgs e)
        {
            base.OnAttachedToLogicalTree(e);
            BuildDirectoryTree();
            if (string.IsNullOrEmpty(BasePath))
            {
                BasePath = AppContext.BaseDirectory;
                NavHistory.AddFirst(BasePath);
                FroeachDirectory(BasePath);
                BuildBreadcrumbDirectory(BasePath);
            }
        }

        private void FroeachDirectory(string path)
        {
            if (string.IsNullOrEmpty(path)) return;
            if (Directory.Exists(path))
            {
                _FileModels.Clear();
                DirectoryInfo info = new(path);
                foreach (var item in info.EnumerateDirectories())
                {
                    FileInfoModel model = new(item);
                    model.OnDirectoryTapped = FileTapped;
                    _FileModels.Add(model);
                }
                foreach(var item in info.EnumerateFiles())
                {
                    FileInfoModel model = new(item);
                    _FileModels.Add(model);
                }
            }
            else if (File.Exists(path))
            {
                FileInfo fileInfo = new(path);
                DirectoryInfo p = fileInfo.Directory;
                if(p != null) FroeachDirectory(p.FullName);
            }
            else return;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        private void BuildBreadcrumbDirectory(string path)
        {
            if (string.IsNullOrEmpty(path)) return;
            DirectoryInfo parent;
            if (Directory.Exists(path)) parent = new DirectoryInfo(path);
            else if (File.Exists(path))
            {
                FileInfo fileInfo = new(path);
                parent = fileInfo.Directory;
            }
            else return;
            List<TreeFileInfoModel> models = new List<TreeFileInfoModel>();
            while(parent != null)
            {
                TreeFileInfoModel d = new(parent)
                {
                    OnDirectoryTapped = FileTapped
                };
                d.TreeItemCommand = d.CreateTreeItemCommand();
                models.Add(d);
                parent = parent.Parent;
            }
            models.Reverse();
            var old = BreadcrumbDirectory;
            BreadcrumbDirectory.Clear();
            BreadcrumbDirectory.AddRange(models);

        }
        /// <summary>
        /// 构建左侧文件夹树
        /// </summary>
        /// <exception cref="NotSupportedException"></exception>
        private void BuildDirectoryTree()
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                string[] roots = Environment.GetLogicalDrives();
                foreach (var item in roots)
                {
                    DirectoryInfo i = new DirectoryInfo(item);
                    TreeFileInfoModel m = new TreeFileInfoModel(i);
                    m.TreeItemCommand = m.CreateTreeItemCommand();
                    m.OnDirectoryTapped = FileTapped;
                    _TreeDirectory.Add(m);
                }
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                DirectoryInfo root = new DirectoryInfo("/");
                TreeFileInfoModel m = new TreeFileInfoModel(root);
                m.TreeItemCommand = m.CreateTreeItemCommand();
                m.OnDirectoryTapped = FileTapped;
                var l = m.SubFileInfos;
                _TreeDirectory.Add(m);
            }
            else throw new NotSupportedException("UnSupported OSPlatform");
        }

        private void FilesView_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            var row = e.Row;
            if (row == null || row.DataContext == null || row.DataContext is not FileInfoModel m) return;
            Gestures.AddDoubleTappedHandler(row, m.OnTapped);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FilesView_UnloadingRow(object sender, DataGridRowEventArgs e)
        {
            var row = e.Row;
            if (row == null || row.DataContext == null || row.DataContext is not FileInfoModel m) return;
            Gestures.RemoveDoubleTappedHandler(row, m.OnTapped);
        }
        /// <summary>
        /// 文件列表项被选中后
        /// </summary>
        /// <param name="model"></param>
        private void FileTapped(FileInfoModel model) => NewPath(model);

        private void NewPath(FileInfoModel model)
        {
            if (model != null && model.IsDirectory)
            {
                var old = BasePath;
                var newPath = model.Info.FullName;
                var t = NavHistory.Find(old);
                if (t != null)
                {
                    if (t.Next != null)
                    {
                        var n = t.Next;
                        while(n != null)
                        {
                            var local = n;
                            n = n.Next;
                            NavHistory.Remove(local);
                        }
                    }
                    NavHistory.AddAfter(t, newPath);
                }
                else NavHistory.AddLast(newPath);
                BasePath = newPath;
                FroeachDirectory(BasePath);
                BuildBreadcrumbDirectory(BasePath);
            }
        }
        private bool CouldBack()
        {
            var t = NavHistory.Find(BasePath);
            return t != null && t.Previous != null;
        }
        private bool CoundForward()
        {
            var t = NavHistory.Find(BasePath);
            return t != null && t.Next != null;
        }
        private void Back()
        {
            var c = NavHistory.Find(BasePath);
            if (c == null) return;
            var p = c.Previous;
            if (p == null) return;
            var path = p.Value;
            if (string.IsNullOrEmpty(path)) return;
            BasePath = path;
            if(path == ROOT)
            {
                NavToRoot();
                return;
            }
            else
            {
                FroeachDirectory(BasePath);
                BuildBreadcrumbDirectory(BasePath);
            }
        }
        private void Forward()
        {
            var c = NavHistory.Find(BasePath);
            if (c == null) return;
            var n = c.Next;
            if (n == null) return;
            var path = n.Value;
            if (string.IsNullOrEmpty(path)) return;
            BasePath = path;
            if (path == ROOT)
            {
                NavToRoot();
                return;
            }
            else
            {
                FroeachDirectory(BasePath);
                BuildBreadcrumbDirectory(BasePath);
            }
        }
        /// <summary>
        /// 导航至根目录
        /// </summary>
        private void NavToRoot()
        {          
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                string[] roots = Environment.GetLogicalDrives();
                _FileModels.Clear();
                foreach (var item in roots)
                {
                    DirectoryInfo i = new DirectoryInfo(item);
                    FileInfoModel model = new FileInfoModel(i);
                    model.OnDirectoryTapped = FileTapped;
                    _FileModels.Add(model);
                }
                NavHistory.AddLast(ROOT);
                BasePath = ROOT;
                BreadcrumbDirectory.Clear();
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                DirectoryInfo root = new DirectoryInfo("/");
                FileInfoModel file = new FileInfoModel(root);
                NewPath(file);
            }
            else throw new NotSupportedException("UnSupported OSPlatform");
        }
        /// <summary>
        /// 
        /// </summary>
        private bool EnterCheck(string path)
        {
            return !string.IsNullOrEmpty(path) && File.Exists(path);
        }
    }
}
