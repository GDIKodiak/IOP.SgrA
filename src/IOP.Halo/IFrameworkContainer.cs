﻿using Avalonia.Controls;
using Avalonia.Controls.Templates;
using Microsoft.Extensions.DependencyInjection;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.Halo
{
    /// <summary>
    /// 框架容器
    /// </summary>
    public interface IFrameworkContainer : IDataTemplate
    {
        /// <summary>
        /// 服务提供者
        /// </summary>
        IServiceProvider Services { get; }
        /// <summary>
        /// 注册服务
        /// </summary>
        /// <param name="action"></param>
        void ConfigureServices(Action<IServiceCollection> action);
        /// <summary>
        /// 获取首个服务，如果不存在则为空
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T GetService<T>() where T : class;
        /// <summary>
        /// 获取首个服务，如果不存在则上报错误
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T GetRequiredService<T>() where T : class;
        /// <summary>
        /// 创建自动注入实例
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T CreateAutowiredInstance<T>() where T : class;
        /// <summary>
        /// 获取容器窗口
        /// </summary>
        /// <typeparam name="TWindow"></typeparam>
        /// <returns></returns>
        TWindow GetContainerWindow<TWindow>() where TWindow : ContainerWindow;
        /// <summary>
        /// 添加顶部菜单至标题栏菜单
        /// </summary>
        /// <param name="descriptor"></param>
        void AddTitleBarMenu(MenuDescriptor descriptor);
        /// <summary>
        /// 添加次级菜单至标题栏菜单
        /// </summary>
        /// <param name="header"></param>
        /// <param name="descriptor"></param>
        void AddTitleBarMenuItem(string header, MenuDescriptor descriptor);
        /// <summary>
        /// 打开新的工作空间
        /// </summary>
        /// <typeparam name="TView"></typeparam>
        /// <param name="header"></param>
        /// <param name="couldClosed"></param>
        void OpenNewWorkSpace<TView, TVM>(string header, bool couldClosed = true, bool force = false) 
            where TView : ContentControl
            where TVM : FrameworkViewModel;
        /// <summary>
        /// 打开新的工作空间
        /// </summary>
        /// <typeparam name="TView"></typeparam>
        /// <typeparam name="TVM"></typeparam>
        /// <param name="header"></param>
        /// <param name="onLoading"></param>
        /// <param name="onClosed"></param>
        /// <param name="force"></param>
        void OpenNewWorkSpace<TView, TVM>(string header, Func<TView, TVM, ValueTask> initializing,
            Func<TView, TVM, ValueTask> onClosed, bool force = false)
            where TView : ContentControl
            where TVM : FrameworkViewModel;
        /// <summary>
        /// 打开新的工作空间
        /// </summary>
        /// <typeparam name="TView"></typeparam>
        /// <typeparam name="TVM"></typeparam>
        /// <param name="header"></param>
        /// <param name="onLoading"></param>
        void OpenNewWorkSpace<TView, TVM>(string header, Func<TView, TVM, ValueTask> initializing)
            where TView : ContentControl
            where TVM : FrameworkViewModel;
        /// <summary>
        /// 打开新的工作空间，且无法关闭
        /// </summary>
        /// <typeparam name="TView"></typeparam>
        /// <typeparam name="TVM"></typeparam>
        /// <param name="header"></param>
        /// <param name="view"></param>
        /// <param name="viewModel"></param>
        void OpenNewWorkSpace<TView, TVM>(string header,
            TView view, TVM viewModel)
            where TView : ContentControl
            where TVM : FrameworkViewModel;
        /// <summary>
        /// 注册文件加载处理函数
        /// </summary>
        /// <param name="extension"></param>
        /// <param name="func"></param>
        void RegistFileLoadedFunc(string extension, Func<string, Task> func);
        /// <summary>
        /// 执行文件加载函数
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        Task ExecuteFileLoadingFuncs(string path);
    }
}
