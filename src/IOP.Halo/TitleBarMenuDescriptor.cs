﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.Halo
{
    /// <summary>
    /// 标题栏菜单描述器
    /// </summary>
    public class TitleBarMenuDescriptor
    {
        private ObservableCollection<MenuDescriptor> _Menus = new();
        /// <summary>
        /// 顶部菜单栏
        /// </summary>
        public IList<MenuDescriptor> TopMenus { get => _Menus; }
#nullable disable
        /// <summary>
        /// 尝试根据标题获取描述器
        /// </summary>
        /// <param name="header"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public bool TryFindDescriptor(string name, out MenuDescriptor target)
        {
            target = default;
            if (string.IsNullOrEmpty(name)) return false;
            foreach (var item in TopMenus)
            {
                if (item.TryFindDescriptor(name, out target)) { return true; }
            }
            return false;
        }
#nullable enable
        /// <summary>
        /// 将描述器添加至顶部菜单
        /// </summary>
        /// <param name="topMenu"></param>
        public void AddToTopMenu(MenuDescriptor topMenu)
        {
            if (topMenu == null) return;
            _Menus.Add(topMenu);
        }
        /// <summary>
        /// 将描述器添加至指定名称下的描述器
        /// </summary>
        /// <param name="header"></param>
        /// <param name="menu"></param>
        public void AddToMenu(string name, MenuDescriptor menu)
        {
            if (menu == null || string.IsNullOrEmpty(name)) return;
            if (TryFindDescriptor(name, out MenuDescriptor target))
            {
                target.AddToMenu(menu);
            }
            else
            {
                var top = new MenuDescriptor(name);
                top.AddToMenu(menu);
                AddToTopMenu(top);
            }
        }
    }
}
