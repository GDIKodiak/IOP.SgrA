﻿using Avalonia;
using Avalonia.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.Halo.Attach
{
    public partial class AttachContent
    {
#nullable disable
        /// <summary>
        /// 附加图标
        /// </summary>
        public static readonly AttachedProperty<ContentControl> AttachIconProperty = AvaloniaProperty.RegisterAttached<AttachContent, ContentControl, ContentControl>("AttachIcon",
            default, false, Avalonia.Data.BindingMode.OneWay, null, null);
        /// <summary>
        /// 设置附加图标
        /// </summary>
        /// <param name="element"></param>
        /// <param name="control"></param>
        public static void SetAttachIcon(AvaloniaObject element, ContentControl control)
        {
            if (control == null) return;
            element.SetValue(AttachIconProperty, control);
        }
        /// <summary>
        /// 获取附加图标
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static ContentControl GetAttachIcon(AvaloniaObject element)
        {
            return element.GetValue(AttachIconProperty);
        }
#nullable restore
    }
}
