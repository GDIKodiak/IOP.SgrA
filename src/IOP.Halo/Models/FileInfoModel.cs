﻿using Avalonia.Interactivity;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace IOP.Halo
{
    /// <summary>
    /// 文件信息模型
    /// </summary>
    public class FileInfoModel : ReactiveObject
    {  
        /// <summary>
        /// 
        /// </summary>
        public FileSystemInfo Info { get; }
        public string Path { get => Info == null ? string.Empty : Info.FullName; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 是否为文件夹
        /// </summary>
        public bool IsDirectory { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Extension { get; set; }
        /// <summary>
        /// 大小
        /// </summary>
        public long? Size { get; set; }
        /// <summary>
        /// 图标
        /// </summary>
        public string Icon { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime LastUpdate { get; set; }
        /// <summary>
        /// 当文件夹被双击时触发命令
        /// </summary>
        public Action<FileInfoModel> OnDirectoryTapped { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public FileInfoModel(FileInfo info)
        {
            Info = info ?? throw new ArgumentNullException(nameof(info));
            Name = info.Name;
            Extension = Info.Extension;
            IsDirectory = false;
            Size = info.Length;
            LastUpdate = info.LastWriteTime;
            Icon = GetIcon(info);
            OnTapped = (s, e) => { };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public FileInfoModel(DirectoryInfo info)
        {
            Info = info ?? throw new ArgumentNullException(nameof(info));
            Name = info.Name;
            Extension = Info.Extension;
            IsDirectory = true;
            LastUpdate = info.LastWriteTime;
            Size = null;
            Icon = GetIcon(info);
            OnTapped = (s, e) => 
            { 
                OnDirectoryTapped?.Invoke(this); 
            };
        }
        /// <summary>
        /// 当该模型绑定对象被选中时
        /// </summary>
        public EventHandler<RoutedEventArgs> OnTapped { get; private set; }

        private string GetIcon(FileInfo info)
        {
            var e = info.Extension;
            return e switch
            {
              ".dll" => "Icon-Dll",
                _ => "Icon-Blank",
            };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        private string GetIcon(DirectoryInfo info) => "Icon-Folder";
    }

    public class TreeFileInfoModel : FileInfoModel
    {
        private Lazy<ObservableCollection<TreeFileInfoModel>> _SubItems;

        /// <summary>
        /// 
        /// </summary>
        public ICommand TreeItemCommand { get; set; }
        /// <summary>
        /// 子文件信息
        /// </summary>
        public ObservableCollection<TreeFileInfoModel> SubFileInfos
        {
            get => _SubItems.Value;
        }

        public TreeFileInfoModel(DirectoryInfo info)
            :base(info)
        {
            _SubItems = new Lazy<ObservableCollection<TreeFileInfoModel>>(ForeachSubItems);
        }
        public TreeFileInfoModel(FileInfo info)
            : base(info) 
        {
            _SubItems = new Lazy<ObservableCollection<TreeFileInfoModel>>(ForeachSubItems);
        }
        /// <summary>
        /// 遍历子项
        /// </summary>
        public ObservableCollection<TreeFileInfoModel> ForeachSubItems()
        {
            var r = new ObservableCollection<TreeFileInfoModel>();
            if (Info == null || !Info.Exists || Info is FileInfo) return r;
            if (Info is not DirectoryInfo d) return r;
            var f = d.EnumerateDirectories();
            foreach(var item in f)
            {
                if(item.Attributes.HasFlag(FileAttributes.Hidden)
                    || item.Attributes.HasFlag(FileAttributes.Encrypted)) 
                    continue;
                TreeFileInfoModel model = new(item);
                model.TreeItemCommand = CreateTreeItemCommand();
                model.OnDirectoryTapped = OnDirectoryTapped;
                r.Add(model);
            }
            return r;
        }
        /// <summary>
        /// 创建树形列表点击命令
        /// </summary>
        /// <returns></returns>
        public ICommand CreateTreeItemCommand()
        {
            return ReactiveCommand.Create(() =>
            {
                if (IsDirectory)
                {
                    OnDirectoryTapped?.Invoke(this);
                }
            });
        }
    }
}
