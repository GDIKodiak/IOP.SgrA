﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace IOP.Halo
{
    /// <summary>
    /// 菜单描述器
    /// </summary>
    public class MenuDescriptor : ReactiveObject
    {
#nullable disable
        private ObservableCollection<MenuDescriptor> _Items = new();
        private ICommand _Command;
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; } = string.Empty;
        /// <summary>
        /// 菜单头
        /// </summary>
        public string Header { get; set; } = string.Empty;
        /// <summary>
        /// 命令
        /// </summary>
        public ICommand Command 
        { 
            get => _Command; 
            set 
            { 
                if (value != null) 
                { 
                    _Command = value; this.RaisePropertyChanged(); 
                } 
            } 
        }
        /// <summary>
        /// 命令参数
        /// </summary>
        public object CommandParameter { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        public bool IsEnable { get; set; } = true;
        /// <summary>
        /// 子项
        /// </summary>
        public IList<MenuDescriptor> Items
        {
            get => _Items;
        }

        /// <summary>
        /// 尝试搜索指定名称的描述器
        /// </summary>
        /// <param name="header"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public bool TryFindDescriptor(string name, out MenuDescriptor target)
        {
            target = default;
            if (string.IsNullOrEmpty(Name)) return false;
            if(Header == name) { target = this; return true;  }
            foreach(var item in Items)
            {
                if(item.TryFindDescriptor(name, out target)) { return true; }
            }
            return false;
        }
#nullable enable
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public MenuDescriptor(string name)
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));
            Name = Header = name;
        }

        /// <summary>
        /// 将描述器添加至指定名称下的描述器
        /// </summary>
        /// <param name="header"></param>
        /// <param name="menu"></param>
        public void AddToMenu(string name, MenuDescriptor menu)
        {
            if (menu == null) return;
            if (Header == name) AddToMenu(menu);
            if (TryFindDescriptor(name, out MenuDescriptor target))
            {
                target.AddToMenu(menu);
            }
        }
        /// <summary>
        /// 将描述器添加至当前描述器
        /// </summary>
        /// <param name="header"></param>
        /// <param name="menu"></param>
        public void AddToMenu(MenuDescriptor menu)
        {
            if (menu == null) return;
            _Items.Add(menu);
        }
    }
}
