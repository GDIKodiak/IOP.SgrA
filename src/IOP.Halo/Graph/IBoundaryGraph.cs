﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.Halo.Graph
{
    /// <summary>
    /// 
    /// </summary>
    public interface IBoundaryGraph
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Bound2D GetBound();
    }
}
