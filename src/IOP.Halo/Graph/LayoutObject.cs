﻿using DynamicData.Aggregation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.Halo.Graph
{
    /// <summary>
    /// 布局对象
    /// </summary>
    public class LayoutObject
    {
        private IBoundaryGraph _DecoratedObject = default;

        /// <summary>
        /// 包围盒
        /// </summary>
        public Bound2D Bound { get; private set; }
        /// <summary>
        /// 被装饰者
        /// </summary>
        public IBoundaryGraph DecoratedObject
        {
            get => _DecoratedObject;
            set
            {
                if (value == null) Bound = new Bound2D();
                else Bound = value.GetBound();
                _DecoratedObject = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool CollisionCheck(LayoutObject other)
        {
            if (other == null) return false;
            Bound2D boundA = Bound;
            Bound2D boundB = other.Bound;
            Vector2 maxA = boundA.MaxVector();
            Vector2 minA = boundA.MinVector();
            Vector2 maxB = boundB.MaxVector();
            Vector2 minB = boundB.MinVector();
            return maxA.X >= minB.X && maxB.X >= minA.X &&
                maxA.Y >= minB.Y && maxB.Y >= minA.Y;
        }
    }
}
