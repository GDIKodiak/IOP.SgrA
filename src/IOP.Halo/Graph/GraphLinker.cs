﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Data;
using Avalonia.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.Halo.Graph
{
    /// <summary>
    /// 图链接器
    /// </summary>
    public class GraphLinker : Control
    {
        public static DirectProperty<GraphLinker, Vector2> StartPositionProperty =
            AvaloniaProperty.RegisterDirect<GraphLinker, Vector2>(nameof(StartPosition),
                (o) => o._StartPosition,
                (o, v) => { o._StartPosition = v; PositionHasChanged(o, v, o._EndPosition); }, Vector2.Zero, BindingMode.TwoWay);
        public static DirectProperty<GraphLinker, Vector2> EndPositionProperty =
            AvaloniaProperty.RegisterDirect<GraphLinker, Vector2>(nameof(EndPosition),
                (o) => o._EndPosition,
                (o, v) => { o._EndPosition = v; PositionHasChanged(o, v, o._StartPosition); }, Vector2.Zero, BindingMode.TwoWay);
        public static DirectProperty<GraphLinker, Vector2> LineSpacingProperty =
            AvaloniaProperty.RegisterDirect<GraphLinker, Vector2>(nameof(LineSpacing),
                (o) => o._LineSpacing,
                (o, v) => { o._LineSpacing = v; }, default, BindingMode.TwoWay);

        private Vector2 _StartPosition;
        private Vector2 _EndPosition;
        private Vector2 _LineSpacing = new Vector2(2, 2);
        private readonly List<LayoutWapper> _Lines = new List<LayoutWapper>();

        public Vector2 StartPosition
        {
            get => _StartPosition;
            set { SetAndRaise(StartPositionProperty, ref _StartPosition, value); }
        }
        public Vector2 EndPosition 
        {
            get => _EndPosition;
            set { SetAndRaise(EndPositionProperty, ref _EndPosition, value); }
        }
        /// <summary>
        /// 线段间距
        /// </summary>
        public Vector2 LineSpacing
        {
            get => _LineSpacing;
            set 
            {
                if (value.X < float.Epsilon) value = new Vector2(2, value.Y);
                if (value.Y < float.Epsilon) value = new Vector2(value.X, 2);
                SetAndRaise(LineSpacingProperty, ref _LineSpacing, value); 
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Vector2 MaxVector { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public Vector2 MinVector { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public Matrix3x2 Transform { get; set; } = Matrix3x2.Identity;

        /// <summary>
        /// 生成路径
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="tree"></param>
        public void GeneratorPath(Vector2 start, Vector2 end, LayoutTree tree)
        {
            if (tree == null) return;
            Vector2 space = LineSpacing;
            if (MathF.Abs(start.Y - end.Y) < float.Epsilon)
            {
                LayoutWapper line = GeneratorLineX(start, end, tree);
                _Lines.Add(line);
                return;
            }
            else
            {
                Vector2 center = (start + end) * 0.5f;
                LayoutWapper line1 = GeneratorLineX(start, new Vector2(center.X, start.Y), tree);
                _Lines.Add(line1);
                start = line1.Line.End;
                LayoutWapper line2 = GeneratorLineY(start, new Vector2(start.X, end.Y), tree);
                _Lines.Add(line2);
                start = line2.Line.End;
                GraphLine line3 = new GraphLine(start, end, space.X);
                LayoutObject obj3 = new LayoutObject() { DecoratedObject = line3 };
                tree.TryInsert(obj3);
                LayoutWapper endLine = new LayoutWapper(obj3, line3);
                _Lines.Add(endLine);
            }
        }

        public override void Render(DrawingContext context)
        {
            IPen pen = new Pen(Brushes.White, thickness: 2);
            foreach (var line in _Lines)
            {
                var l = line.Line;
                Vector2 start = l.Start;
                Vector2 end = l.End;
                start = Vector2.Transform(start, Transform);
                end = Vector2.Transform(end, Transform);
                context.DrawLine(pen, new Point(-start.X, -start.Y), new Point(-end.X, -end.Y));
            }
            base.Render(context);
        }

        private LayoutWapper GeneratorLineX(Vector2 start, Vector2 end, LayoutTree tree)
        {
            Vector2 space = LineSpacing;
            GraphLine line = new GraphLine(start, end, space.X);
            LayoutObject obj = new LayoutObject() { DecoratedObject = line };
            if (tree.Collision(obj, out List<LayoutObject> collidingObjs))
            {
                Vector2 normalA = line.GetNormal();
                foreach (var c in collidingObjs)
                {
                    if (c.DecoratedObject is GraphLine l)
                    {
                        if (MathF.Abs(l.Start.Y - line.Start.Y) < float.Epsilon)
                        {
                            tree.TryInsert(obj);
                            return new LayoutWapper(obj, line);
                        }
                        Vector2 normalB = l.GetNormal();
                        float cos = MathF.Abs(Vector2.Dot(normalA, normalB));
                        if(cos < 0.867f)
                        {
                            tree.TryInsert(obj);
                            return new LayoutWapper(obj, line);
                        }
                    }
                    else continue;
                }
                float x = end.X - space.X;
                while (x > start.X + space.X * 2)
                {
                    line = new GraphLine(start, new Vector2(x, start.Y), space.X);
                    obj.DecoratedObject = line;
                    bool hasCollision = tree.Collision(obj, out _);
                    if (!hasCollision)
                    {
                        tree.TryInsert(obj);
                        return new LayoutWapper(obj, line);
                    }
                    else x -= space.X;
                }
                tree.TryInsert(obj);
                return new LayoutWapper(obj, line);
            }
            else
            {
                tree.TryInsert(obj);
                return new LayoutWapper(obj, line);
            }
        }
        private LayoutWapper GeneratorLineY(Vector2 start, Vector2 end, LayoutTree tree)
        {
            Vector2 space = LineSpacing;
            GraphLine line = new GraphLine(start, end, space.Y);
            LayoutObject obj = new LayoutObject() { DecoratedObject = line };
            if (tree.Collision(obj, out List<LayoutObject> collidingObjs))
            {
                Vector2 normalA = line.GetNormal();
                foreach (var c in collidingObjs)
                {
                    if (c.DecoratedObject is GraphLine l)
                    {
                        if (MathF.Abs(l.Start.X - line.Start.X) < float.Epsilon)
                        {
                            tree.TryInsert(obj);
                            return new LayoutWapper(obj, line);
                        }
                        Vector2 normalB = l.GetNormal();
                        float cos = MathF.Abs(Vector2.Dot(normalA, normalB));
                        if (cos < 0.867f)
                        {
                            tree.TryInsert(obj);
                            return new LayoutWapper(obj, line);
                        }
                    }
                    else continue;
                }
                float y = end.Y - space.Y;
                while (y > start.Y + space.Y * 2)
                {
                    line = new GraphLine(start, new Vector2(start.X, y), space.Y);
                    obj.DecoratedObject = line;
                    bool hasCollision = tree.Collision(obj, out _);
                    if (!hasCollision)
                    {
                        tree.TryInsert(obj);
                        return new LayoutWapper(obj, line);
                    }
                    else y -= space.Y;
                }
                tree.TryInsert(obj);
                return new LayoutWapper(obj, line);
            }
            else
            {
                tree.TryInsert(obj);
                return new LayoutWapper(obj, line);
            }
        }

        private static void PositionHasChanged(GraphLinker linker, Vector2 position, Vector2 comparer)
        {
            float minX = MathF.Min(position.X, comparer.X);
            float minY = MathF.Min(position.Y, comparer.Y);
            linker.MinVector = new Vector2(minX, minY);

            float maxX = MathF.Max(position.X, comparer.X);
            float maxY = MathF.Max(position.Y, comparer.Y);
            linker.MaxVector = new Vector2(maxX, maxY);
        }

    }

    internal struct LayoutWapper
    {
        internal LayoutObject Object;
        internal GraphLine Line;

        internal LayoutWapper(LayoutObject obj, GraphLine line)
        {
            Object = obj;
            Line = line;
        }
    }
}
