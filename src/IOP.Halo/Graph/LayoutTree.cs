﻿using Avalonia.Controls;
using System;
using System.Buffers;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IOP.Halo.Graph
{
    /// <summary>
    /// 布局树（四叉树）
    /// </summary>
    public class LayoutTree
    {
        /// <summary>
        /// 
        /// </summary>
        internal protected readonly ConcurrentDictionary<ulong, LayoutNode> Nodes = new();
        /// <summary>
        /// 
        /// </summary>
        internal protected readonly ConcurrentDictionary<LayoutObject, LayoutNode> Indexes = new();
        /// <summary>
        /// 最大深度
        /// </summary>
        internal uint MAXDEPTH = 32;
        protected LayoutNode Root;

        /// <summary>
        /// 松散系数
        /// </summary>
        public float Loose { get; protected set; } = 1.5f;
        /// <summary>
        /// 最大深度
        /// </summary>
        public uint MaxDepth { get; protected set; }
        /// <summary>
        /// 中心坐标
        /// </summary>
        public Vector2 Center { get; protected set; }
        /// <summary>
        /// 四叉树左下角外围坐标
        /// </summary>
        public Vector2 LeftDown { get; protected set; }
        /// <summary>
        /// 四叉树的最大宽度
        /// </summary>
        public float HalfWidth { get; protected set; }
        /// <summary>
        /// 最小单元格宽度
        /// </summary>
        public float GridHalfWidth { get; protected set; }
        /// <summary>
        /// 控件数量
        /// </summary>
        public int Count => Nodes.Count;
        /// <summary>
        /// 莫顿码最大值
        /// </summary>
        public ulong MaxCode
        {
            get => (ulong)Math.Pow(2, (MaxDepth * 2));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="width"></param>
        /// <param name="center"></param>
        /// <param name="maxDepth"></param>
        /// <param name="loose"></param>
        public LayoutTree(float width, Vector2 center, uint maxDepth, float loose)
        {
            InitTree(width, center, maxDepth, loose);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool TryInsert(LayoutObject obj)
        {
            if (obj == null) return false;
            if (TryGetGridIndex(obj, out Vector2 index, out Vector2 center, out Bound2D box))
            {
                float halfWidth = GridHalfWidth;
                ulong code = Morton2(index); bool insert = false; ulong local;
                while (halfWidth <= HalfWidth && code > 0)
                {
                    if (box.FullContainsIn(center, new Vector2(halfWidth * Loose)))
                    {
                        insert = true;
                        break;
                    }
                    else
                    {
                        local = code & 3;
                        code >>= 2;
                        center = GetParentCenter(center, halfWidth, local);
                        halfWidth *= 2;
                    }
                }
                if (insert)
                {
                    if (Nodes.TryGetValue(code, out LayoutNode node))
                    {
                        node.ChildObjects.AddLast(obj);
                        Indexes.AddOrUpdate(obj, node, (key, value) => node);
                    }
                    else
                    {
                        LayoutNode newNode = NewLayoutNode(index, center, box);
                        newNode.ChildObjects.AddLast(obj);
                        Indexes.AddOrUpdate(obj, newNode, (key, value) => newNode);
                    }
                }
                return insert;
            }
            else return false;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="gridIndex"></param>
        /// <returns></returns>
        public ulong Morton2(Vector2 gridIndex)
        {
            ulong r = (Part1By1((ulong)gridIndex.Y) << 1) +
                Part1By1((ulong)gridIndex.X);
            return r;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="outsideObject"></param>
        public void Maintenance(out List<LayoutObject> outsideObject)
        {
            LayoutObject[] locals = ArrayPool<LayoutObject>.Shared.Rent(Count); Span<LayoutObject> s = locals;
            int end = 0; int length = 0;
            foreach (var item in Nodes.Values)
            {
                Span<LayoutObject> local = s.Slice(end, item.ChildObjects.Count);
                item.Maintenance(in local, out int l);
                end += l;
            }
            for (int i = 0; i < end; i++)
            {
                LayoutObject obj = s[i];
                Indexes.TryRemove(obj, out _);
                if (!TryInsert(obj))
                    s[length++] = obj;
            }
            s = s[..length];
            if (s.Length > 0) outsideObject = new List<LayoutObject>(s.ToArray());
            else outsideObject = new List<LayoutObject>();
            ArrayPool<LayoutObject>.Shared.Return(locals);
        }
        /// <summary>
        /// 碰撞检测
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="collidingobjs"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public bool Collision(LayoutObject obj, out List<LayoutObject> collidingobjs)
        {
            collidingobjs = null;
            if (obj == null) return false;
            bool r = TryGetNode(obj, out LayoutNode node);
            if (!r) return r;
            collidingobjs = new List<LayoutObject>();
            var related = GetChildNodes(node);
            related.Add(node);
            foreach(var child in related)
            {
                foreach(var cObj in child.ChildObjects)
                {
                    if(cObj != null && cObj.CollisionCheck(obj))
                    {
                        collidingobjs.Add(cObj);
                    }
                }
            }
            return collidingobjs != null && collidingobjs.Count > 0;
        }

        /// <summary>
        /// 获取对象所在树节点
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="node"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public bool TryGetNode(LayoutObject obj, out LayoutNode node)
        {
            ArgumentNullException.ThrowIfNull(obj, nameof(obj));
            if (Indexes.TryGetValue(obj, out node)) return true;
            else
            {
                if(TryGetGridIndex(obj, out Vector2 index, out Vector2 center, out Bound2D bound))
                {
                    ulong code = Morton2(index);
                    if (Nodes.TryGetValue(code, out node)) return true;
                    else
                    {
                        node = NewLayoutNode(index, center, bound);
                        return true;
                    }
                }
                else return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public LayoutNode GetParentNode(LayoutNode node)
        {
            ArgumentNullException.ThrowIfNull((object)node, nameof(node));
            var code = node.Code;
            var center = node.Center;
            var halfWidth = node.HalfWidth;
            ulong local = code & 3;
            code >>= 2;
            center = GetParentCenter(center, halfWidth, local);
            halfWidth *= 2;
            if(Nodes.TryGetValue(code, out LayoutNode pNode)) { return pNode; }
            pNode = new LayoutNode(code, center, halfWidth);
            Nodes.AddOrUpdate(code, pNode, (key, value) => pNode);
            return pNode;
        }

        /// <summary>
        /// 初始化树
        /// </summary>
        /// <param name="width"></param>
        /// <param name="center"></param>
        /// <param name="maxDepth"></param>
        /// <param name="loose"></param>
        protected virtual void InitTree(float width, Vector2 center, uint maxDepth, float loose)
        {
            maxDepth = Math.Min(MAXDEPTH, maxDepth);
            MaxDepth = maxDepth;
            Center = center;
            Loose = loose;
            float halfWidth = width * 0.5f;
            HalfWidth = halfWidth;
            LeftDown = new Vector2(center.X - halfWidth, center.Y - halfWidth);
            GridHalfWidth = halfWidth / MathF.Pow(2, maxDepth);
            LayoutNode root = new LayoutNode(1, Center, HalfWidth);
            Nodes.AddOrUpdate(1, root, (key, value) => value);
            Root = root;
        }
        /// <summary>
        /// 获取对象所在节点网格
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="gridIndex"></param>
        /// <param name="center"></param>
        /// <param name="bound"></param>
        /// <returns></returns>
        protected virtual bool TryGetGridIndex(LayoutObject obj, out Vector2 gridIndex, out Vector2 center, out Bound2D bound)
        {
            gridIndex = Vector2.Zero;
            center = Vector2.Zero;
            Vector2 rCenter = Center; float bWidth = HalfWidth;
            bound = obj.Bound;
            Vector2 extents = bound.Extents;
            Vector2 p = bound.Center;
            Vector2 boundary = new(rCenter.X - bWidth, rCenter.Y - bWidth);
            float ll = (boundary - rCenter).LengthSquared();
            float tl = (p - extents - rCenter).LengthSquared();
            float tr = (p + extents - rCenter).LengthSquared();
            if (tl > ll || tr > ll) return false;
            float w = GridHalfWidth * 2;
            Vector2 d = (p - LeftDown) / w;
            gridIndex = new Vector2((int)Math.Abs(d.X), 
                (int)Math.Abs(d.Y));
            center = new Vector2(LeftDown.X + gridIndex.X * w + GridHalfWidth, 
                LeftDown.Y + gridIndex.Y * w + GridHalfWidth);
            return true;
        }
        /// <summary>
        /// 新的节点
        /// </summary>
        /// <param name="index"></param>
        /// <param name="center"></param>
        /// <param name="box"></param>
        /// <returns></returns>
        protected virtual LayoutNode NewLayoutNode(Vector2 index, Vector2 center, Bound2D box)
        {
            float halfWidth = GridHalfWidth;
            ulong code = Morton2(index); ulong local;
            while (halfWidth <= HalfWidth && code > 0)
            {
                if (box.FullContainsIn(center, new Vector2(halfWidth * Loose)))
                {
                    if (Nodes.TryGetValue(code, out LayoutNode oldNode)) return oldNode;
                    LayoutNode newNode = new(code, center, halfWidth);
                    local = code & 3;
                    ulong pCode = code >> 2;
                    if (Nodes.TryGetValue(pCode, out LayoutNode n))
                    {
                        n.ChildMask |= (byte)(1 << (byte)local);
                    }
                    else
                    {
                        center = GetParentCenter(center, halfWidth, local);
                        halfWidth *= 2;
                        LayoutNode pNode = new LayoutNode(pCode, center, halfWidth);
                        pNode.ChildMask |= (byte)(1 << (byte)local);
                        Nodes.AddOrUpdate(pCode, pNode, (key, value) => pNode);
                    }
                    Nodes.AddOrUpdate(code, newNode, (key, value) => newNode);
                    return newNode;
                }
                else
                {
                    local = code & 3;
                    code >>= 2;
                    center = GetParentCenter(center, halfWidth, local);
                    halfWidth *= 2;
                }
            }
            return Root;
        }

        protected virtual List<LayoutNode> GetChildNodes(LayoutNode parent)
        {
            List<LayoutNode> r = new List<LayoutNode>();
            if (parent != null) 
            {
                var code = parent.Code;
                code <<= 2;
                while (code <= MaxCode)
                {
                    for (ulong i = 0; i <= 3; i++)
                    {
                        ulong cCode = code | i;
                        if (Nodes.TryGetValue(cCode, out LayoutNode cNode)) r.Add(cNode);
                    }
                    code <<= 2;
                }
            }
            return r;
        }

        /// <summary>
        /// 获取子网格所属父节点中心
        /// </summary>
        /// <param name="childCenter"></param>
        /// <param name="childHalfWidth"></param>
        /// <param name="localCode"></param>
        /// <returns></returns>
        internal static Vector2 GetParentCenter(Vector2 childCenter, float childHalfWidth, ulong localCode)
        {
            return localCode switch
            {
                1 => childCenter + new Vector2(-childHalfWidth, -childHalfWidth),
                2 => childCenter + new Vector2(childHalfWidth, childHalfWidth),
                3 => childCenter + new Vector2(-childHalfWidth, childHalfWidth),
                _ => childCenter + new Vector2(childHalfWidth, -childHalfWidth),
            };
        }

        internal static ulong Part1By1(ulong n)
        {
            n &= 0x00000000ffffffff;
            n = (n | (n << 16)) & 0x0000FFFF0000FFFF;
            n = (n | (n << 8)) & 0x00FF00FF00FF00FF;
            n = (n | (n << 4)) & 0x0F0F0F0F0F0F0F0F;
            n = (n | (n << 2)) & 0x3333333333333333;
            n = (n | (n << 1)) & 0x5555555555555555;
            return n;
        }
    }
}
