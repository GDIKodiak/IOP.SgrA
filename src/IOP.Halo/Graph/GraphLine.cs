﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.Halo.Graph
{
    public class GraphLine : IBoundaryGraph
    {

        private Bound2D _Bound;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Bound2D GetBound() => _Bound;

        public Vector2 Start { get; private set; }

        public Vector2 End { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="start">起始坐标</param>
        /// <param name="end">终点坐标</param>
        /// <param name="boxWidth">包围盒宽度</param>
        public GraphLine(Vector2 start, Vector2 end, float boxWidth)
        {
            Start = start;
            End = end;
            Vector2 normal = Vector2.Normalize(start - end);
            Vector2 center = (start + end) * 0.5f;
            float length = (start - end).Length();
            if (normal.X <= float.Epsilon && normal.X >= -float.Epsilon)
                _Bound = new Bound2D(center, new Vector2(boxWidth, length * 0.5f));
            else if (normal.Y <= float.Epsilon && normal.Y >= -float.Epsilon)
                _Bound = new Bound2D(center, new Vector2(length * 0.5f, boxWidth));
            else
            {
                var theta = end - center;
                _Bound = new Bound2D(center, new Vector2(MathF.Abs(theta.X), MathF.Abs(theta.Y)));
            }
        }
        /// <summary>
        /// 获取线段法向量
        /// </summary>
        /// <returns></returns>
        public Vector2 GetNormal() => Vector2.Normalize(Start - End);
    }
}
