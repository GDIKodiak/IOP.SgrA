﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.Halo.Graph
{
    public struct Bound2D
    {
        /// <summary>
        /// 
        /// </summary>
        public Vector2 Center;
        /// <summary>
        /// 
        /// </summary>
        public Vector2 Extents;
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Vector2 MinVector() => Center - Extents;
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Vector2 MaxVector() => Center + Extents;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="center"></param>
        /// <param name="extents"></param>
        public Bound2D(Vector2 center, Vector2 extents)
        {
            Center = center;
            Extents = extents;
        }
        /// <summary>
        /// 某特定区域是否完全在此包围盒内
        /// </summary>
        /// <param name="center"></param>
        /// <param name="extents"></param>
        /// <returns></returns>
        public bool FullContains(Vector2 center, Vector2 extents)
        {
            Vector2 pMin = center - extents;
            Vector2 pMax = center + extents;
            Vector2 min = MinVector();
            Vector2 max = MaxVector();
            if (pMin.X < min.X || pMin.Y < min.Y || pMax.X > max.X || pMax.Y > max.Y)
                return false;
            else return true;
        }
        /// <summary>
        /// 此包围盒是否完全存在于某一区域内
        /// </summary>
        /// <param name="center"></param>
        /// <param name="extents"></param>
        /// <returns></returns>
        public bool FullContainsIn(Vector2 center, Vector2 extents)
        {
            Vector2 pMin = center - extents;
            Vector2 pMax = center + extents;
            Vector2 min = MinVector();
            Vector2 max = MaxVector();
            return min.X >= pMin.X && min.Y >= pMin.Y &&
                max.X <= pMax.X && max.Y <= pMax.Y;
        }
    }
}
