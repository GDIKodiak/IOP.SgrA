﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace IOP.Halo.Graph
{
    /// <summary>
    /// 布局节点
    /// </summary>
    public class LayoutNode
    {
        /// <summary>
        /// 莫顿码
        /// </summary>
        public ulong Code {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public float HalfWidth {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Vector2 Center { get; set; }
        /// <summary>
        /// 子节点掩码
        /// </summary>
        public byte ChildMask {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public readonly LinkedList<LayoutObject> ChildObjects = new();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="center"></param>
        /// <param name="width"></param>
        public LayoutNode(ulong code, Vector2 center, float width) 
        {
            Code = code;
            Center = center;
            HalfWidth = width;
        }
        /// <summary>
        /// 维护节点
        /// </summary>
        /// <param name="outside"></param>
        /// <param name="length"></param>
        /// <exception cref="IndexOutOfRangeException"></exception>
        public void Maintenance(in Span<LayoutObject> outside, out int length)
        {
            if (outside.Length < ChildObjects.Count) throw new IndexOutOfRangeException();
            var current = ChildObjects.First; length = 0;
            var center = Center; var halfWidth = HalfWidth;
            while (current != null)
            {
                LayoutObject obj = current.Value;
                var bounds = obj.Bound;
                if (!bounds.FullContainsIn(center, new Vector2(halfWidth)))
                {
                    outside[length++] = obj;
                    var remove = current;
                    current = current.Next;
                    ChildObjects.Remove(remove);
                }
                else current = current.Next;
            }
        }
        /// <summary>
        /// 碰撞检测
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="collidingobjs"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public bool CollisionCheck(LayoutObject obj, List<LayoutObject> collidingobjs)
        {
            if (obj == null) return false;
            if(collidingobjs == null) throw new ArgumentNullException(nameof(collidingobjs));
            bool result = false;
            foreach(var item in ChildObjects)
            {
                bool r = obj.CollisionCheck(item);
                if(r)
                {
                    collidingobjs.Add(item);
                    result = true;
                }
            }
            return result;
        }
    }
}
