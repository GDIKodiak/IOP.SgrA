﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Metadata;
using Avalonia.Controls.Primitives;
using Avalonia.Controls.Shapes;
using Avalonia.Controls.Templates;
using Avalonia.Input;
using Avalonia.Media;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.Halo.Graph
{
    /// <summary>
    /// 图编辑器根容器
    /// </summary>
    [TemplatePart(Name = "PART_ViewBox", Type = typeof(Viewbox))]
    public class GraphEditor : ItemsControl
    {
        private static readonly FuncTemplate<Panel> DefaultPanel = 
            new(() => new Canvas());
        private Viewbox _PTBox;
        private Vector2 CenterOffset = Vector2.Zero;
        private Matrix3x2 Trans = Matrix3x2.Identity;

        private readonly LayoutTree LayoutTree;

        protected override Type StyleKeyOverride => typeof(GraphEditor);

        public const float BOXWIDTH = 65536;

        public GraphEditor()
        {
            Height = BOXWIDTH;
            Width = BOXWIDTH;
            LayoutTree = new LayoutTree(BOXWIDTH, Vector2.Zero, 10, 1.5f);
        }

        static GraphEditor()
        {
            ClipToBoundsProperty.OverrideDefaultValue<GraphEditor>(true);
            ItemsPanelProperty.OverrideDefaultValue<GraphEditor>(DefaultPanel);
            FocusableProperty.OverrideDefaultValue<GraphEditor>(true);
        }

        protected override void OnApplyTemplate(TemplateAppliedEventArgs e)
        {
            base.OnApplyTemplate(e);
            var view = e.NameScope.Find<Viewbox>("PART_ViewBox");
            if(view != null)
            {
                _PTBox = view;
            }
        }

        private bool test = false;
        protected override Size ArrangeOverride(Size finalSize)
        {
            if (_PTBox != null)
            {
                _PTBox.Width = DesiredSize.Width;
                _PTBox.Height = DesiredSize.Height;
                if (Height != BOXWIDTH) Height = BOXWIDTH;
                if (Width != BOXWIDTH) Width = BOXWIDTH;
                CenterOffset = new Vector2((float)DesiredSize.Width * 0.5f, (float)DesiredSize.Height * 0.5f);
                Matrix3x2 trans = Matrix3x2.CreateScale(new Vector2(1, -1));
                trans = trans * Matrix3x2.CreateTranslation(-BOXWIDTH * 0.5f, -BOXWIDTH * 0.5f);
                Trans = trans;
                trans = trans * Matrix3x2.CreateTranslation(CenterOffset);
                Vector2 transV = Vector2.Transform(Vector2.Zero, trans);
                ItemsPanelRoot.RenderTransform = new TranslateTransform(transV.X, transV.Y);

                if (!test)
                {
                    test = true;
                    Vector2 testStart1 = new Vector2(-50, 50);
                    Vector2 testEnd1 = new Vector2(100, -50);
                    var linker = GeneralLinker(testStart1, testEnd1);
                    Items.Add(linker);
                    Vector2 testStart2 = new Vector2(-50, 40);
                    Vector2 testEnd2 = new Vector2(100, -40);
                    var linker2 = GeneralLinker(testStart2, testEnd2);
                    Items.Add(linker2);
                }
            }
            var size = base.ArrangeOverride(finalSize);
            return size;
        }

        protected override void OnPointerMoved(PointerEventArgs e)
        {
            if(ItemsPanelRoot != null)
            {
                var p = e.GetCurrentPoint(ItemsPanelRoot);
            }
            base.OnPointerMoved(e);
        }

        protected virtual GraphLinker GeneralLinker(Vector2 start, Vector2 end)
        {
            GraphLinker linker = new GraphLinker();
            linker.Transform = Trans;
            linker.GeneratorPath(start, end, LayoutTree);
            return linker;
        }

    }
}
