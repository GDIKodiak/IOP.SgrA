﻿using Avalonia.Controls;
using CodeWF.EventBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.Halo.EventBus
{
    public class DialogWindowArgs : Command
    {
        /// <summary>
        /// 
        /// </summary>
        public int Height {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Width { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ContentControl Content { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="height"></param>
        /// <param name="width"></param>
        /// <param name="content"></param>
        public DialogWindowArgs(int height, int width, ContentControl content)
        {
            Height = height;
            Width = width;
            Content = content;
        }
    }
}
