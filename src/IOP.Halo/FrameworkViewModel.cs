﻿using Avalonia.Controls;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.Halo
{
    /// <summary>
    /// 框架视图模型基类
    /// </summary>
    public class FrameworkViewModel : ReactiveObject
    {
#nullable disable
#nullable enable
        /// <summary>
        /// 初始化
        /// </summary>
        public virtual void Initialition()
        {
        }
        /// <summary>
        /// 当被关闭时
        /// </summary>
        /// <returns></returns>
        public virtual Task OnClosed()
        {
            return Task.CompletedTask;
        }
    }
}
