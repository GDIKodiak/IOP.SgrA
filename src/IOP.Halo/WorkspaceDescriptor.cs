﻿using Avalonia.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace IOP.Halo
{
    /// <summary>
    /// 工作空间描述器
    /// </summary>
    public class WorkspaceDescriptor
    {
#nullable disable
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 选项卡头
        /// </summary>
        public string Header { get; set; }
        /// <summary>
        /// 是否会被关闭
        /// </summary>
        public bool CouldClose { get; set; } = true;
        /// <summary>
        /// 关闭命令
        /// </summary>
        public ICommand CloseCommand { get; set; }
        /// <summary>
        /// 工作空间
        /// </summary>
        public Workspace Workspace { get; set; }
#nullable enable

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public WorkspaceDescriptor(string name)
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));
            Name = Header = name;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual ValueTask CloseWorkspace()
        {
            if(Workspace != null) return Workspace.Close();
            else return ValueTask.CompletedTask;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TView"></typeparam>
    /// <typeparam name="TVM"></typeparam>
    public class WorkspaceDescriptor<TView, TVM> : WorkspaceDescriptor
        where TView : ContentControl
        where TVM : FrameworkViewModel
    {
        /// <summary>
        /// 
        /// </summary>
        public TVM VM { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public TView View { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public Func<TView, TVM, ValueTask> OnClosed { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public WorkspaceDescriptor(string name, TView view, TVM vm, Func<TView, TVM, ValueTask> func)
            :base(name)
        {
            VM = vm ?? throw new ArgumentNullException(nameof(vm)); 
            View= view ?? throw new ArgumentNullException(nameof(view));
            if (func != null) OnClosed = func;
            else OnClosed = (v, vm) => ValueTask.CompletedTask;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override async ValueTask CloseWorkspace()
        {
            await OnClosed(View, VM);
            await base.CloseWorkspace();
        }
    }
}
