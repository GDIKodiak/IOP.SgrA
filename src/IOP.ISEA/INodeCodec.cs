﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA
{
    /// <summary>
    /// 节点编解码器
    /// </summary>
    public interface INodeCodec
    {
        /// <summary>
        /// 获取编解码器节点类型
        /// </summary>
        /// <returns></returns>
        ushort GetNodeType();
        /// <summary>
        /// 编码
        /// </summary>
        /// <param name="node"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        Memory<byte> Encode(Node node, IISEADocument document, byte endian = 0);
        /// <summary>
        /// 解码
        /// </summary>
        /// <param name="data"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        Node Decode(Memory<byte> data, IISEADocument document, byte endian = 0);
    }

    /// <summary>
    /// 节点编码器
    /// </summary>
    /// <typeparam name="TNode"></typeparam>
    public interface INodeEncoder<TNode>
        where TNode : Node
    {
        /// <summary>
        /// 编码
        /// </summary>
        /// <param name="node"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        Memory<byte> Encode(TNode node, IISEADocument document, byte endian = 0);
    }
    /// <summary>
    /// 节点解码器
    /// </summary>
    /// <typeparam name="TNode"></typeparam>
    public interface INodeDecoder<TNode>
        where TNode : Node
    {
        /// <summary>
        /// 解码
        /// </summary>
        /// <param name="data"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        TNode Decode(Memory<byte> data, IISEADocument document, byte endian = 0);
    }
}
