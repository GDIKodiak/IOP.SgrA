﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace IOP.ISEA
{
    /// <summary>
    /// 配置节点编解码器
    /// </summary>
    public class ConfigNodeCodec : INodeCodec
    {
        private readonly ConfigNodeEncoder Encoder = new ConfigNodeEncoder();
        private readonly ConfigNodeDecoder Decoder = new ConfigNodeDecoder();

        /// <summary>
        /// 获取节点类型
        /// </summary>
        /// <returns></returns>
        public ushort GetNodeType() => (ushort)StandardNodeType.CONFIG;
        /// <summary>
        /// 解码
        /// </summary>
        /// <param name="data"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        public Node Decode(Memory<byte> data, IISEADocument document, byte endian = 0) => Decoder.Decode(data, document, endian);
        /// <summary>
        /// 编码
        /// </summary>
        /// <param name="node"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        public Memory<byte> Encode(Node node, IISEADocument document, byte endian = 0)
        {
            if (node is ConfigNode configNode) return Encoder.Encode(configNode, document, endian);
            throw new InvalidCastException($"target node is not {nameof(ConfigNode)}");
        }
    }

    /// <summary>
    /// 配置节点编码器
    /// </summary>
    public class ConfigNodeEncoder : NodeCodec, INodeEncoder<ConfigNode>
    {
        /// <summary>
        /// 编码
        /// </summary>
        /// <param name="node"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        public Memory<byte> Encode(ConfigNode node, IISEADocument document, byte endian = 0)
        {
            document.AddNode(node, out _);
            var hSize = Constant.NODEHEADERLENGTH;
            var eLength = node.Engine.GetBytesLength();
            var tLength = node.Type.GetBytesLength();
            var nLength = node.Name.GetBytesLength();
            var dLength = node.Config.GetBytesLength();
            var total = eLength + tLength + nLength + dLength;
            Memory<byte> r = new byte[total + hSize + Constant.SIGNLENGTH]; Span<byte> local = r.Span; int index = 0;
            NodeHeader nodeHeader = new NodeHeader() { Id = node.Header.Id, Length = (uint)total, NodeType = (ushort)StandardNodeType.CONFIG, Version = node.Header.Version };
            node.Header = nodeHeader;
            WriteNodeHeader(local, nodeHeader, ref index);
            node.Engine.WriteTo(local[index..]); index += eLength;
            node.Type.WriteTo(local[index..]);index += tLength;
            node.Name.WriteTo(local[index..]); index += nLength;
            node.Config.WriteTo(local[index..]);index += dLength;
            document.AddNode(node, out _);
            return r;
        }
    }

    /// <summary>
    /// 配置节点解码器
    /// </summary>
    public class ConfigNodeDecoder : NodeCodec, INodeDecoder<ConfigNode>
    {
        /// <summary>
        /// 解码
        /// </summary>
        /// <param name="data"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        public ConfigNode Decode(Memory<byte> data, IISEADocument document, byte endian = 0)
        {
            ConfigNode node = new ConfigNode();
            ReadOnlySpan<byte> local = data.Span; int index = 0;
            NodeHeader header = ReadNodeHeader(local, ref index);
            node.Header = header;
            (AsciiString e, short eLength) = AsciiString.Create(local[index..]); index += eLength;
            node.Engine = e;
            (AsciiString t, short tLength) = AsciiString.Create(local[index..]); index += tLength;
            node.Type = t;
            (Utf8String n, int nLength) = Utf8String.Create(local[index..]); index += nLength;
            node.Name = n;
            (Utf8String d, int dLength) = Utf8String.Create(local[index..]); index += dLength;
            node.Config = d;
            document.AddNode(node, out _);
            return node;
        }
    }
}
