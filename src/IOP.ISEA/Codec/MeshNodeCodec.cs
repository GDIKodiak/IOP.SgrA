﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;

namespace IOP.ISEA
{
    /// <summary>
    /// 网格节点编解码器
    /// </summary>
    public class MeshNodeCodec : INodeCodec
    {
        private readonly MeshNodeEncoder Encoder = new MeshNodeEncoder();
        private readonly MeshNodeDecoder Decoder = new MeshNodeDecoder();

        /// <summary>
        /// 获取节点类型
        /// </summary>
        /// <returns></returns>
        public ushort GetNodeType() => (ushort)StandardNodeType.MESH;
        /// <summary>
        /// 解码
        /// </summary>
        /// <param name="data"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        public Node Decode(Memory<byte> data, IISEADocument document, byte endian = 0) => Decoder.Decode(data, document);
        /// <summary>
        /// 编码
        /// </summary>
        /// <param name="node"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        public Memory<byte> Encode(Node node, IISEADocument document, byte endian = 0)
        {
            if (node is MeshNode meshNode) return Encoder.Encode(meshNode, document, endian);
            throw new InvalidCastException($"target node is not {nameof(MeshNode)}");
        }
    }

    /// <summary>
    /// 网格节点编码器
    /// </summary>
    public class MeshNodeEncoder : NodeCodec, INodeEncoder<MeshNode>
    {
        /// <summary>
        /// 编码
        /// </summary>
        /// <param name="node"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        public Memory<byte> Encode(MeshNode node, IISEADocument document,  byte endian = 0)
        {
            document.AddNode(node, out _);
            var hSize = Constant.NODEHEADERLENGTH;
            var nLength = node.Name.GetBytesLength();
            var vData = EncodeVecticesData(node, endian);
            var iData = node.Indexes.ToSpan(endian);
            var dData = EncodeIndirectData(node, endian);
            var total = nLength  + Constant.USHORTLENGTH + 
                Constant.INTLENGTH * 5 + node.VerticesComponentSequence.Count * Constant.INTLENGTH + 
                vData.Length + iData.Length + dData.Length + Constant.BOUNDINGLENGTH;
            Memory<byte> r = new byte[total + hSize +  Constant.SIGNLENGTH]; Span<byte> local = r.Span; int index = 0;
            NodeHeader nodeHeader = new NodeHeader() { Id = node.Header.Id, Length = (uint)total, NodeType = (ushort)StandardNodeType.MESH, Version = node.Header.Version };
            node.Header = nodeHeader;
            WriteNodeHeader(local, nodeHeader, ref index);
            node.Name.WriteTo(local[index..], endian); index += nLength;
            ushort m = (ushort)node.MeshMode; MemoryMarshal.Write(local[index..], ref m); index += Constant.USHORTLENGTH;
            int vc = node.VerticesCount; MemoryMarshal.Write(local[index..], ref vc); index += Constant.INTLENGTH;
            int s = node.VerticesStride; MemoryMarshal.Write(local[index..], ref s); index += Constant.INTLENGTH;
            int vcl = node.VerticesComponentLength; MemoryMarshal.Write(local[index..], ref vcl); index += Constant.INTLENGTH;
            int csl = node.ComponentSequenceLength; MemoryMarshal.Write(local[index..], ref csl); index += Constant.INTLENGTH;
            int dic = node.IndirectCommandsLength; MemoryMarshal.Write(local[index..], ref dic); index += Constant.INTLENGTH;
            Span<byte> seq = MemoryMarshal.AsBytes(new Span<int>(node.VerticesComponentSequence.ToArray()));
            seq.CopyTo(local[index..]); index += seq.Length;
            vData.CopyTo(local[index..]); index += vData.Length;
            iData.CopyTo(local[index..]); index += iData.Length;
            dData.CopyTo(local[index..]); index += dData.Length;
            Vector3 min = node.MinVector; var max = node.MaxVector;
            MemoryMarshal.Write(local[index..], ref min); index += Constant.VECTOR3LENGTH;
            MemoryMarshal.Write(local[index..], ref max); index += Constant.VECTOR3LENGTH;
            return r;
        }

        /// <summary>
        /// 编码顶点坐标数据
        /// </summary>
        /// <param name="node"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        protected Span<byte> EncodeVecticesData(MeshNode node, byte endian = 0)
        {
            if (node.VerticesComponents == null) node.VerticesComponents = new List<MeshData>();
            List<byte> r = new List<byte>();
            foreach(var item in node.VerticesComponents)
            {
                Span<byte> p = item.ToSpan(endian);
                r.AddRange(p.ToArray());
            }
            return r.ToArray();
        }
        /// <summary>
        /// 编码间件绘制数据
        /// </summary>
        /// <param name="node"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        protected Span<byte> EncodeIndirectData(MeshNode node, byte endian = 0)
        {
            if (node.DrawIndirectCommands == null) node.DrawIndirectCommands = new List<MeshData>();
            List<byte> r = new List<byte>();
            foreach(var item in node.DrawIndirectCommands)
            {
                Span<byte> p = item.ToSpan(endian);
                r.AddRange(p.ToArray());
            }
            return r.ToArray();
        }
    }
    /// <summary>
    /// 网格解码器
    /// </summary>
    public class MeshNodeDecoder : NodeCodec, INodeDecoder<MeshNode>
    {
        /// <summary>
        /// 解码
        /// </summary>
        /// <param name="data"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        public MeshNode Decode(Memory<byte> data, IISEADocument document, byte endian = 0)
        {
            MeshNode node = new MeshNode();
            ReadOnlySpan<byte> local = data.Span; int index = 0;
            NodeHeader header = ReadNodeHeader(local, ref index);
            node.Header = header;
            (Utf8String n, int nLength) = Utf8String.Create(local[index..]); index += nLength;
            node.Name = n;
            ushort m = MemoryMarshal.Read<ushort>(local.Slice(index, Constant.USHORTLENGTH)); index += Constant.USHORTLENGTH;
            node.MeshMode = (MeshMode)m;
            int vc = MemoryMarshal.Read<int>(local.Slice(index, Constant.INTLENGTH)); index += Constant.INTLENGTH;
            node.VerticesCount = vc;
            int s = MemoryMarshal.Read<int>(local.Slice(index, Constant.INTLENGTH)); index += Constant.INTLENGTH;
            node.VerticesStride = s;
            int vcl = MemoryMarshal.Read<int>(local.Slice(index, Constant.INTLENGTH)); index += Constant.INTLENGTH;
            int scl = MemoryMarshal.Read<int>(local.Slice(index, Constant.INTLENGTH)); index += Constant.INTLENGTH;
            int dic = MemoryMarshal.Read<int>(local.Slice(index, Constant.INTLENGTH)); index += Constant.INTLENGTH;
            if (scl > 0)
            {
                ReadOnlySpan<byte> seqSource = local.Slice(index, Constant.INTLENGTH * scl);
                ReadOnlySpan<int> seq = MemoryMarshal.Cast<byte, int>(seqSource); index += seqSource.Length;
                node.VerticesComponentSequence = new List<int>(seq.ToArray());
            }
            else node.VerticesComponentSequence = new List<int>();
            if (vcl > 0)
            {
                node.VerticesComponents = new List<MeshData>(vcl);
                for (int i = 0; i < vcl; i++)
                {
                    (MeshData md, int l) = MeshData.Create(local[index..], endian); index += l;
                    node.VerticesComponents.Add(md);
                }
            }
            else node.VerticesComponents = new List<MeshData>();
            (MeshData indexes, int c) = MeshData.Create(local[index..], endian); index += c;
            node.Indexes = indexes;
            if (dic > 0)
            {
                node.DrawIndirectCommands = new List<MeshData>(dic);
                for (int i = 0; i < dic; i++)
                {
                    (MeshData md, int l) = MeshData.Create(local[index..], endian); index += l;
                    node.DrawIndirectCommands.Add(md);
                }
            }
            Vector3 min = MemoryMarshal.Read<Vector3>(local.Slice(index, Constant.VECTOR3LENGTH)); index += Constant.VECTOR3LENGTH;
            Vector3 max = MemoryMarshal.Read<Vector3>(local.Slice(index, Constant.VECTOR3LENGTH)); index += Constant.VECTOR3LENGTH;
            node.MinVector = min; node.MaxVector = max;
            document.AddNode(node, out _);
            return node;
        }
    }
}
