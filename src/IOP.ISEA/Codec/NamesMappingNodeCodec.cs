﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace IOP.ISEA
{
    /// <summary>
    /// 名称映射节点编解码器
    /// </summary>
    public class NamesMappingNodeCodec : INodeCodec
    {
        private readonly NamesMappingNodeEncoder Encoder = new NamesMappingNodeEncoder();
        private readonly NamesMappingNodeDecoder Decoder = new NamesMappingNodeDecoder();

        /// <summary>
        /// 节点类型
        /// </summary>
        /// <returns></returns>
        public ushort GetNodeType() => (ushort)StandardNodeType.NAMESMAPPING;

        /// <summary>
        /// 解码
        /// </summary>
        /// <param name="data"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Node Decode(Memory<byte> data, IISEADocument document, byte endian = 0) => Decoder.Decode(data, document, endian);
        /// <summary>
        /// 编码
        /// </summary>
        /// <param name="node"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Memory<byte> Encode(Node node, IISEADocument document, byte endian = 0)
        {
            if (node is NamesMappingNode namesMappingNode) return Encoder.Encode(namesMappingNode, document, endian);
            throw new InvalidCastException($"target node is not {nameof(NamesMappingNode)}");
        }
    }

    /// <summary>
    /// 名称映射节点编码器
    /// </summary>
    public class NamesMappingNodeEncoder : NodeCodec, INodeEncoder<NamesMappingNode>
    {
        /// <summary>
        /// 编码
        /// </summary>
        /// <param name="node"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        public Memory<byte> Encode(NamesMappingNode node, IISEADocument document, byte endian = 0)
        {
            var hSize = Constant.NODEHEADERLENGTH;
            var names = node.NameMappings;
            var count = names.Count;
            uint total = 0;
            byte[][] mapping = new byte[count][];
            for(int i = 0; i < count; i++)
            {
                int l = names[i].NodeName.GetBytesLength() + Constant.INTLENGTH;
                byte[] m = new byte[l];
                names[i].NodeName.WriteTo(m);
                int id = names[i].NodeId;
                MemoryMarshal.Write(m.AsSpan()[(l - Constant.INTLENGTH)..], ref id);
                mapping[i] = m;
                total += (uint)l;
            }
            NodeHeader nodeHeader = new NodeHeader() { Id = 0, Length = total, NodeType = (ushort)StandardNodeType.NAMESMAPPING, Version = 1 };
            Memory<byte> r = new byte[total + hSize + Constant.SIGNLENGTH];
            Span<byte> local = r.Span; int index = 0;
            node.Header = nodeHeader;
            WriteNodeHeader(local, nodeHeader, ref index);
            for(int i = 0; i < count; i++)
            {
                mapping[i].AsSpan().CopyTo(local[index..]);
                index += mapping[i].Length;
            }
            return r;
        }
    }
    /// <summary>
    /// 名称映射节点解码
    /// </summary>
    public class NamesMappingNodeDecoder : NodeCodec, INodeDecoder<NamesMappingNode>
    {
        /// <summary>
        /// 解码
        /// </summary>
        /// <param name="data"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public NamesMappingNode Decode(Memory<byte> data, IISEADocument document, byte endian = 0)
        {
            NamesMappingNode node = new NamesMappingNode();
            var names = node.NameMappings;
            ReadOnlySpan<byte> local = data.Span; int index = 0;
            NodeHeader header = ReadNodeHeader(local, ref index);
            node.Header = header;
            while(index < local.Length)
            {
                (Utf8String n, int nLength) = Utf8String.Create(local[index..]); index += nLength;
                int id = MemoryMarshal.Read<int>(local[index..]); index += Constant.INTLENGTH;
                names.Add(new NamesMapping() { NodeName = n, NodeId = id });
            }
            return node;
        }
    }
}
