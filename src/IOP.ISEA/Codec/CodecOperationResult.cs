﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA
{
    /// <summary>
    /// 解码器操作结果
    /// </summary>
    public struct CodecOperationResult
    {
        /// <summary>
        /// 是否成功
        /// </summary>
        public bool IsSuccess;
        /// <summary>
        /// 消息
        /// </summary>
        public string Message;
        /// <summary>
        /// 节点头
        /// </summary>
        public NodeHeader NodeHeader;
        /// <summary>
        /// 节点
        /// </summary>
        public Node Node;
    }
}
