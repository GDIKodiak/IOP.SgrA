﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Text;

namespace IOP.ISEA
{
    /// <summary>
    /// 节点编解码器
    /// </summary>
    public class NodeCodec
    {
        /// <summary>
        /// 写入节点头
        /// </summary>
        /// <param name="span"></param>
        /// <param name="header"></param>
        /// <param name="index"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void WriteNodeHeader(Span<byte> span, NodeHeader header, ref int index)
        {
            var hSize = Constant.NODEHEADERLENGTH;
            Span<byte> sign = Constant.NODE;
            sign.CopyTo(span.Slice(index, Constant.SIGNLENGTH)); index += Constant.SIGNLENGTH;
            MemoryMarshal.Write(span.Slice(index, hSize), ref header); index += hSize;
        }
        /// <summary>
        /// 读取节点头
        /// </summary>
        /// <param name="span"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected NodeHeader ReadNodeHeader(ReadOnlySpan<byte> span, ref int index)
        {
            var hSize = Constant.NODEHEADERLENGTH;
            var length = span.Length - Constant.SIGNLENGTH - hSize; index = Constant.SIGNLENGTH;
            NodeHeader header = MemoryMarshal.Read<NodeHeader>(span.Slice(index, hSize)); index += hSize;
            if (header.Length != length) throw new InvalidOperationException("node data has be lost");
            return header;
        }
        /// <summary>
        /// 获取节点标识符
        /// </summary>
        /// <param name="span"></param>
        /// <param name="endian"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected NodeSign GetSign(ReadOnlySpan<byte> span, byte endian, ref int index)
        {
            int sign = MemoryMarshal.Read<int>(span.Slice(index, Constant.SIGNLENGTH)); index += Constant.SIGNLENGTH;
            return sign switch
            {
                Constant.LITTLENODESIGN => NodeSign.NODE,
                Constant.LITTLEREFNSIGN => NodeSign.REFN,
                Constant.LITTLENULLSIGN => NodeSign.NULL,
                _ => throw new NotSupportedException("Unknow node sign"),
            };
        }
        /// <summary>
        /// 写入引用节点
        /// </summary>
        /// <param name="span"></param>
        /// <param name="node"></param>
        /// <param name="endian"></param>
        /// <param name="document"></param>
        /// <param name="index"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void WriteReferenceNode(Span<byte> span, Node node, byte endian, IISEADocument document, ref int index)
        {
            if (node == null)
            {
                int length = Constant.SIGNLENGTH;
                int data = Constant.LITTLENULLSIGN;
                if (length > span.Length) throw new IndexOutOfRangeException("Target span is too small");
                MemoryMarshal.Write(span[index..], ref data); index += length;
            }
            else
            {
                document.AddNode(node, out _);
                int length = Constant.REFNLENGTH;
                int data = Constant.LITTLEREFNSIGN; int id = (int)node.Header.Id;
                if (length > span.Length) throw new IndexOutOfRangeException("Target span is too small");
                if (id <= 0) throw new InvalidOperationException("Invalid id");
                MemoryMarshal.Write(span[index..], ref data); index += Constant.SIGNLENGTH;
                MemoryMarshal.Write(span[index..], ref id);  index += Constant.INTLENGTH;
            }
        }
        /// <summary>
        /// 写入引用节点数组
        /// </summary>
        /// <param name="span"></param>
        /// <param name="nodes"></param>
        /// <param name="length"></param>
        /// <param name="endian"></param>
        /// <param name="document"></param>
        /// <param name="index"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void WriteReferenceNodeArray(Span<byte> span, Node[] nodes, int length, byte endian, IISEADocument document, ref int index)
        {
            if (nodes == null) nodes = new Node[0];
            if (nodes.Length != length) throw new ArgumentOutOfRangeException("Nodes's length is not same as paramter length");
            MemoryMarshal.Write(span.Slice(index, Constant.INTLENGTH), ref length); index += Constant.INTLENGTH;
            for(int i = 0; i < length; i++)
            {
                Node child = nodes[i];
                if (child == null) throw new NullReferenceException("Existence null reference in this node array");
                WriteReferenceNode(span, child, endian, document, ref index);
            }

        }
        /// <summary>
        /// 写入引用节点列表
        /// </summary>
        /// <param name="span"></param>
        /// <param name="nodes"></param>
        /// <param name="length"></param>
        /// <param name="endian"></param>
        /// <param name="document"></param>
        /// <param name="index"></param>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        /// <exception cref="NullReferenceException"></exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void WriteReferenceNodeList<TNode>(Span<byte> span, List<TNode> nodes, int length, byte endian, IISEADocument document, ref int index)
            where TNode : Node
        {
            if (nodes == null) nodes = new List<TNode>();
            if (nodes.Count != length) throw new ArgumentOutOfRangeException("Nodes's length is not same as paramter length");
            MemoryMarshal.Write(span.Slice(index, Constant.INTLENGTH), ref length); index += Constant.INTLENGTH;
            for (int i = 0; i < length; i++)
            {
                Node child = nodes[i];
                if (child == null) throw new NullReferenceException("Existence null reference in this node array");
                WriteReferenceNode(span, child, endian, document, ref index);
            }
        }
        /// <summary>
        /// 读取引用节点数组
        /// </summary>
        /// <typeparam name="TNode"></typeparam>
        /// <param name="span"></param>
        /// <param name="endian"></param>
        /// <param name="document"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected TNode[] ReadReferenceNodeArray<TNode>(ReadOnlySpan<byte> span, byte endian, IISEADocument document, ref int index)
            where TNode : Node
        {
            int length = MemoryMarshal.Read<int>(span.Slice(index, Constant.INTLENGTH)); index += Constant.INTLENGTH;
            TNode[] nodes = new TNode[length];
            for(int i = 0; i < length; i++)
            {
                TNode local = ReadReferenceNode<TNode>(span, endian, document, ref index);
                nodes[i] = local ?? throw new NullReferenceException("Existence null reference in this node array");
            }
            return nodes;
        }
        /// <summary>
        /// 读取引用节点列表
        /// </summary>
        /// <typeparam name="TNode"></typeparam>
        /// <param name="span"></param>
        /// <param name="endian"></param>
        /// <param name="document"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        /// <exception cref="NullReferenceException"></exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected List<TNode> ReadReferenceNodeList<TNode>(ReadOnlySpan<byte> span, byte endian, IISEADocument document, ref int index)
            where TNode : Node
        {
            int length = MemoryMarshal.Read<int>(span.Slice(index, Constant.INTLENGTH)); index += Constant.INTLENGTH;
            List<TNode> nodes = new List<TNode>(length);
            for (int i = 0; i < length; i++)
            {
                TNode local = ReadReferenceNode<TNode>(span, endian, document, ref index);
                nodes.Add(local ?? throw new NullReferenceException("Existence null reference in this node array"));
            }
            return nodes;
        }
        /// <summary>
        /// 读取引用节点
        /// </summary>
        /// <param name="span"></param>
        /// <param name="endian"></param>
        /// <param name="document"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected TNode ReadReferenceNode<TNode>(ReadOnlySpan<byte> span, byte endian, IISEADocument document, ref int index)
            where TNode : Node
        {
            NodeSign sign = GetSign(span, endian, ref index);
            if (sign == NodeSign.NULL || sign == NodeSign.NODE) return null;
            else
            {
                int id = MemoryMarshal.Read<int>(span[index..]); index += Constant.INTLENGTH;
                return document.GetNodeFromId<TNode>(id);
            }
        }
    }
}
