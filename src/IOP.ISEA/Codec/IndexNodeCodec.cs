﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace IOP.ISEA
{
    /// <summary>
    /// 索引节点编解码器
    /// </summary>
    public class IndexNodeCodec : INodeCodec
    {
        private readonly IndexNodeEncoder Encoder = new IndexNodeEncoder();
        private readonly IndexNodeDecoder Decoder = new IndexNodeDecoder();

        /// <summary>
        /// 获取节点类型
        /// </summary>
        /// <returns></returns>
        public ushort GetNodeType() => (ushort)StandardNodeType.INDEX;
        /// <summary>
        /// 解码
        /// </summary>
        /// <param name="data"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        public Node Decode(Memory<byte> data, IISEADocument document, byte endian = 0) => Decoder.Decode(data, document, endian);
        /// <summary>
        /// 编码
        /// </summary>
        /// <param name="node"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        public Memory<byte> Encode(Node node, IISEADocument document, byte endian = 0)
        {
            if (node is IndexNode indexNode) return Encoder.Encode(indexNode, document, endian);
            throw new InvalidCastException($"target node is not {nameof(IndexNode)}");
        }
    }

    /// <summary>
    /// 索引节点编码器
    /// </summary>
    public class IndexNodeEncoder : NodeCodec, INodeEncoder<IndexNode>
    {
        /// <summary>
        /// 编码
        /// </summary>
        /// <param name="node"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        public Memory<byte> Encode(IndexNode node, IISEADocument document,  byte endian = 0)
        {
            var size = Constant.NODEINDEXLENGTH;
            var hSize = Constant.NODEHEADERLENGTH;
            var count = node.Indexes.Count;
            var length = size * count;
            NodeHeader nodeHeader = new NodeHeader() { Id = 0, Length = (uint)length, NodeType = (ushort)StandardNodeType.INDEX, Version = 1 };
            node.Header = nodeHeader;
            Memory<byte> r = new byte[length + hSize + Constant.SIGNLENGTH]; Span<byte> local = r.Span; int index = 0;
            WriteNodeHeader(local, nodeHeader, ref index);
            foreach (var item in node.Indexes)
            {
                NodeIndex l = item.Value;
                MemoryMarshal.Write(local.Slice(index, size), ref l);
                index += size;
            }
            return r;
        }
    }
    /// <summary>
    /// 索引节点解码器
    /// </summary>
    public class IndexNodeDecoder : NodeCodec, INodeDecoder<IndexNode>
    {
        /// <summary>
        /// 解码
        /// </summary>
        /// <param name="data"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        public IndexNode Decode(Memory<byte> data, IISEADocument document, byte endian = 0)
        {
            IndexNode node = new IndexNode();
            node.Name = "INDEX";
            ReadOnlySpan<byte> local = data.Span;
            var size = Constant.NODEINDEXLENGTH;
            var length = data.Length - Constant.NODEINDEXLENGTH; var index = 0;
            NodeHeader header = ReadNodeHeader(local, ref index);
            if ((length % size) != 0) throw new InvalidOperationException("node data has be damaged");
            if (header.Length != length) throw new InvalidOperationException("node data has be lost");
            node.Header = header;
            while (index <= length)
            {
                NodeIndex nodeIndex = MemoryMarshal.Read<NodeIndex>(local.Slice(index, size));
                index += size;
                node.Indexes.Add(nodeIndex.Id, nodeIndex);
            }
            return node;
        }
    }
}
