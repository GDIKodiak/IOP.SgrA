﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace IOP.ISEA
{
    /// <summary>
    /// 图片节点编解码器
    /// </summary>
    public class ImageNodeCodec : INodeCodec
    {
        private readonly ImageNodeEncoder Encoder = new ImageNodeEncoder();
        private readonly ImageNodeDecoder Decoder = new ImageNodeDecoder();

        /// <summary>
        /// 获取节点类型
        /// </summary>
        /// <returns></returns>
        public ushort GetNodeType() => (ushort)StandardNodeType.IMAGE;
        /// <summary>
        /// 解码
        /// </summary>
        /// <param name="data"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        public Node Decode(Memory<byte> data, IISEADocument document, byte endian = 0) => Decoder.Decode(data, document, endian);
        /// <summary>
        /// 编码
        /// </summary>
        /// <param name="node"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        public Memory<byte> Encode(Node node, IISEADocument document, byte endian = 0)
        {
            if (node is ImageNode imageNode) return Encoder.Encode(imageNode, document, endian);
            throw new InvalidCastException($"target node is not {nameof(ImageNode)}");
        }
    }

    /// <summary>
    /// 图片节点编码器
    /// </summary>
    public class ImageNodeEncoder : NodeCodec, INodeEncoder<ImageNode>
    {
        /// <summary>
        /// 编码
        /// </summary>
        /// <param name="node"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        public Memory<byte> Encode(ImageNode node, IISEADocument document, byte endian = 0)
        {
            document.AddNode(node, out _);
            var hSize = Constant.NODEHEADERLENGTH;
            var eLength = node.Extension.GetBytesLength();
            var nLength = node.Name.GetBytesLength();
            Span<byte> arrayData = node.ImageData.ToSpan(endian);
            var total = nLength + eLength + arrayData.Length;
            Memory<byte> r = new byte[total + hSize + Constant.SIGNLENGTH]; Span<byte> local = r.Span; int index = 0;
            NodeHeader nodeHeader = new NodeHeader() { Id = node.Header.Id, Length = (uint)total, NodeType = (ushort)StandardNodeType.IMAGE, Version = node.Header.Version };
            node.Header = nodeHeader;
            WriteNodeHeader(local, nodeHeader, ref index);
            node.Name.WriteTo(local[index..], endian); index += nLength;
            node.Extension.WriteTo(local[index..], endian); index += eLength;
            arrayData.CopyTo(local[index..]);
            return r;
        }
    }

    /// <summary>
    /// 图像节点编解码器
    /// </summary>
    public class ImageNodeDecoder : NodeCodec, INodeDecoder<ImageNode>
    {
        /// <summary>
        /// 解码
        /// </summary>
        /// <param name="data"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        public ImageNode Decode(Memory<byte> data, IISEADocument document, byte endian = 0)
        {
            ImageNode node = new ImageNode();
            ReadOnlySpan<byte> local = data.Span; int index = 0;
            NodeHeader header = ReadNodeHeader(local, ref index);
            node.Header = header;
            (Utf8String name, int nLength) = Utf8String.Create(local[index..]); index += nLength;
            (AsciiString e, short eLength) = AsciiString.Create(local[index..]); index += eLength;
            node.Name = name;
            node.Extension = e;
            (DataArray<byte> array, _) = DataArray<byte>.Create(local[index..]);
            node.ImageData = array;
            document.AddNode(node, out _);
            return node;
        }
    }
}
