﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;

namespace IOP.ISEA
{
    /// <summary>
    /// 渲染对象节点编解码器
    /// </summary>
    public class RenderObjectNodeCodec : INodeCodec
    {
        private readonly RenderObjectNodeEncoder Encoder = new RenderObjectNodeEncoder();
        private readonly RenderObjectNodeDncoder Decoder = new RenderObjectNodeDncoder();

        /// <summary>
        /// 获取节点类型
        /// </summary>
        /// <returns></returns>
        public ushort GetNodeType() => (ushort)StandardNodeType.RENDEROBJECT;
        /// <summary>
        /// 解码
        /// </summary>
        /// <param name="data"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        public Node Decode(Memory<byte> data, IISEADocument document, byte endian = 0) => Decoder.Decode(data, document, endian);
        /// <summary>
        /// 编码
        /// </summary>
        /// <param name="node"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        public Memory<byte> Encode(Node node, IISEADocument document, byte endian = 0)
        {
            if (node is RenderObjectNode renderNode) return Encoder.Encode(renderNode, document, endian);
            throw new InvalidCastException($"target node is not {nameof(TextureNode)}");
        }
    }

    /// <summary>
    /// 渲染对象节点编码器
    /// </summary>
    public class RenderObjectNodeEncoder : NodeCodec, INodeEncoder<RenderObjectNode>
    {
        /// <summary>
        /// 编码
        /// </summary>
        /// <param name="node"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        public Memory<byte> Encode(RenderObjectNode node, IISEADocument document, byte endian = 0)
        {
            document.AddNode(node, out _);
            var hSize = Constant.NODEHEADERLENGTH;
            int nLength = node.Name.GetBytesLength();
            int pLength = Constant.VECTOR3LENGTH; int rLength = Constant.QUATERNIONLENGTH; int scaleLength = Constant.VECTOR3LENGTH;
            Vector3 p = node.Position; Quaternion rotation = node.Rotation; Vector3 s = node.Scale;
            int comNumber = node.ComponentArrayLength; int cnLength = Constant.INTLENGTH; int cLength = node.ComponentArrayLength * Constant.REFNLENGTH;
            int parLength = node.Parent == null ? Constant.SIGNLENGTH : Constant.REFNLENGTH;
            int childNumber = node.ChildArrayLength; int childnLength = Constant.INTLENGTH; int childLength = node.ChildArrayLength * Constant.REFNLENGTH;
            var total = nLength + pLength + rLength + scaleLength + cnLength + cLength + parLength + childnLength + childLength;
            Memory<byte> r = new byte[total + hSize + Constant.SIGNLENGTH]; Span<byte> local = r.Span; int index = 0;
            NodeHeader nodeHeader = new NodeHeader() { Id = node.Header.Id, Length = (uint)total, NodeType = (ushort)StandardNodeType.RENDEROBJECT, Version = node.Header.Version };
            node.Header = nodeHeader;
            WriteNodeHeader(local, nodeHeader, ref index);
            node.Name.WriteTo(local[index..], endian); index += nLength;
            MemoryMarshal.Write(local.Slice(index, pLength), ref p); index += pLength;
            MemoryMarshal.Write(local.Slice(index, rLength), ref rotation); index += rLength;
            MemoryMarshal.Write(local.Slice(index, scaleLength), ref s); index += scaleLength;
            if (node.Comonents == null) node.Comonents = new List<Node>();
            WriteReferenceNodeList(local, node.Comonents, comNumber, endian, document, ref index);
            if (node.Childrens == null) node.Childrens = new List<RenderObjectNode>();
            WriteReferenceNodeList(local, node.Childrens, childNumber, endian, document, ref index);
            return r;
        }
    }
    /// <summary>
    /// 渲染对象节点解码器
    /// </summary>
    public class RenderObjectNodeDncoder : NodeCodec, INodeDecoder<RenderObjectNode>
    {
        /// <summary>
        /// 解码
        /// </summary>
        /// <param name="data"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        public RenderObjectNode Decode(Memory<byte> data, IISEADocument document, byte endian = 0)
        {
            RenderObjectNode node = new RenderObjectNode();
            ReadOnlySpan<byte> local = data.Span; int index = 0;
            NodeHeader header = ReadNodeHeader(local, ref index);
            node.Header = header;
            (Utf8String n, int nLength) = Utf8String.Create(local[index..]); index += nLength;
            node.Name = n;
            Vector3 position = MemoryMarshal.Read<Vector3>(local.Slice(index, Constant.VECTOR3LENGTH)); index += Constant.VECTOR3LENGTH;
            node.Position = position;
            Quaternion rotation = MemoryMarshal.Read<Quaternion>(local.Slice(index, Constant.QUATERNIONLENGTH)); index += Constant.QUATERNIONLENGTH;
            if (rotation == new Quaternion(0f, 0f, 0f, 0f)) rotation = Quaternion.Identity;
            node.Rotation = rotation;
            Vector3 scale = MemoryMarshal.Read<Vector3>(local.Slice(index, Constant.VECTOR3LENGTH)); index += Constant.VECTOR3LENGTH;
            if(scale == Vector3.Zero) scale = Vector3.One;
            node.Scale = scale;
            var components = ReadReferenceNodeList<Node>(local, endian, document, ref index); node.Comonents = components;
            var childrens = ReadReferenceNodeList<RenderObjectNode>(local, endian, document, ref index); node.Childrens = childrens;
            foreach(var item in childrens)
            {
                item.Parent = node;
            }
            document.AddNode(node, out _);
            return node;
        }
    }
}
