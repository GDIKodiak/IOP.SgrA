﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace IOP.ISEA
{
    /// <summary>
    /// 纹理节点编解码器
    /// </summary>
    public class TextureNodeCodec : INodeCodec
    {
        private readonly TextureNodeEncoder Encoder = new TextureNodeEncoder();
        private readonly TextureNodeDecoder Decoder = new TextureNodeDecoder();

        /// <summary>
        /// 获取节点类型
        /// </summary>
        /// <returns></returns>
        public ushort GetNodeType() => (ushort)StandardNodeType.TEXTURE;
        /// <summary>
        /// 解码
        /// </summary>
        /// <param name="data"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        public Node Decode(Memory<byte> data, IISEADocument document, byte endian = 0) => Decoder.Decode(data, document, endian);
        /// <summary>
        /// 编码
        /// </summary>
        /// <param name="node"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        public Memory<byte> Encode(Node node, IISEADocument document, byte endian = 0)
        {
            if (node is TextureNode texNode) return Encoder.Encode(texNode, document, endian);
            throw new InvalidCastException($"target node is not {nameof(TextureNode)}");
        }
    }

    /// <summary>
    /// 纹理节点编码器
    /// </summary>
    public class TextureNodeEncoder : NodeCodec, INodeEncoder<TextureNode>
    {
        /// <summary>
        /// 编码
        /// </summary>
        /// <param name="node"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        public Memory<byte> Encode(TextureNode node, IISEADocument document, byte endian = 0)
        {
            document.AddNode(node, out _);
            var hSize = Constant.NODEHEADERLENGTH;
            int nLength = node.Name.GetBytesLength();
            int cLength = 0; int sLength = 0; int iLength = 0;
            if (node.TexConfig == null) cLength = Constant.SIGNLENGTH;
            else cLength = Constant.REFNLENGTH;
            if (node.SamplerConfig == null) sLength = Constant.SIGNLENGTH;
            else sLength = Constant.REFNLENGTH;
            if (node.Image == null) iLength = Constant.SIGNLENGTH;
            else iLength = Constant.REFNLENGTH;
            var total = nLength + Constant.USHORTLENGTH + cLength + sLength + iLength + Constant.UINTLENGTH * 4;
            Memory<byte> r = new byte[total + hSize + Constant.SIGNLENGTH]; Span<byte> local = r.Span; int index = 0;
            NodeHeader nodeHeader = new NodeHeader() { Id = node.Header.Id, Length = (uint)total, NodeType = (ushort)StandardNodeType.TEXTURE, Version = node.Header.Version };
            node.Header = nodeHeader;
            WriteNodeHeader(local, nodeHeader, ref index);
            node.Name.WriteTo(local[index..], endian); index += nLength;
            ushort u = (ushort)node.Usage; MemoryMarshal.Write(local[index..], ref u); index += Constant.USHORTLENGTH;
            uint s = node.SetIndex; MemoryMarshal.Write(local[index..], ref s); index += Constant.UINTLENGTH;
            uint b = node.Binding; MemoryMarshal.Write(local[index..], ref b); index += Constant.UINTLENGTH;
            uint a = node.ArrayElement; MemoryMarshal.Write(local[index..], ref a); index += Constant.UINTLENGTH;
            uint d = node.DescriptorCount; MemoryMarshal.Write(local[index..], ref d); index += Constant.UINTLENGTH;
            WriteReferenceNode(local, node.TexConfig, endian, document, ref index);
            WriteReferenceNode(local, node.SamplerConfig, endian, document, ref index);
            WriteReferenceNode(local, node.Image, endian, document, ref index);
            return r;
        }
    }
    /// <summary>
    /// 纹理节点解码器
    /// </summary>
    public class TextureNodeDecoder : NodeCodec, INodeDecoder<TextureNode>
    {
        /// <summary>
        /// 纹理节点解码器
        /// </summary>
        /// <param name="data"></param>
        /// <param name="document"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        public TextureNode Decode(Memory<byte> data, IISEADocument document, byte endian = 0)
        {
            TextureNode node = new TextureNode();
            ReadOnlySpan<byte> local = data.Span; int index = 0;
            NodeHeader header = ReadNodeHeader(local, ref index);
            node.Header = header;
            (Utf8String n, int nLength) = Utf8String.Create(local[index..]); index += nLength;
            node.Name = n;
            ushort u = MemoryMarshal.Read<ushort>(local[index..]); index += Constant.USHORTLENGTH; node.Usage = (TextureUsage)u;
            uint s = MemoryMarshal.Read<uint>(local[index..]); index += Constant.UINTLENGTH; node.SetIndex = s;
            uint b = MemoryMarshal.Read<uint>(local[index..]); index += Constant.UINTLENGTH; node.Binding = b;
            uint a = MemoryMarshal.Read<uint>(local[index..]); index += Constant.UINTLENGTH; node.ArrayElement = a;
            uint d = MemoryMarshal.Read<uint>(local[index..]); index += Constant.UINTLENGTH; node.DescriptorCount = d;
            var cNode = ReadReferenceNode<ConfigNode>(local, endian, document, ref index); node.TexConfig = cNode;
            var sNode = ReadReferenceNode<ConfigNode>(local, endian, document, ref index); node.SamplerConfig = sNode;
            var iNode = ReadReferenceNode<ImageNode>(local, endian, document, ref index); node.Image = iNode;
            document.AddNode(node, out _);
            return node;
        }
    }
}
