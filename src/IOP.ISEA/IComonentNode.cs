﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA
{
    /// <summary>
    /// 组件节点接口
    /// </summary>
    public interface IComonentNode
    {
        /// <summary>
        /// 名称
        /// </summary>
        Utf8String Name { get; set; }
    }
}
