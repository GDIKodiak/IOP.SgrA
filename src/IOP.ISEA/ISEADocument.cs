﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IOP.ISEA
{
    /// <summary>
    /// ISEA文档
    /// </summary>
    public class ISEADocument : IISEADocument
    {
        /// <summary>
        /// 字节序
        /// </summary>
        public byte Endian { get; protected set; }
        /// <summary>
        /// 是否压缩
        /// </summary>
        public byte Compress { get; protected set; }
        /// <summary>
        /// ID
        /// </summary>
        protected int ID = 0;
        /// <summary>
        /// 
        /// </summary>
        protected readonly object SyncRoot = new object();
        /// <summary>
        /// 索引节点
        /// </summary>
        protected internal IndexNode Indexes { get; set; } = new IndexNode();
        /// <summary>
        /// 名称映射节点
        /// </summary>
        protected internal NamesMappingNode NamesMapping { get; set; } = new NamesMappingNode();
        /// <summary>
        /// 节点字典
        /// </summary>
        protected internal readonly ConcurrentDictionary<int, Node> Nodes = new ConcurrentDictionary<int, Node>();
        /// <summary>
        /// 文字索引节点字典
        /// </summary>
        protected internal readonly ConcurrentDictionary<string, Node> NodesWithNames = new ConcurrentDictionary<string, Node>();
        /// <summary>
        /// 索引节点编码器
        /// </summary>
        protected internal readonly IndexNodeCodec IndexNodeCodec = new IndexNodeCodec();
        /// <summary>
        /// 名称映射节点编解码器
        /// </summary>
        protected internal readonly NamesMappingNodeCodec NamesMappingNodeCodec = new NamesMappingNodeCodec();
        /// <summary>
        /// 编解码器
        /// </summary>
        protected internal readonly ConcurrentDictionary<ushort, INodeCodec> Codecs = new ConcurrentDictionary<ushort, INodeCodec>();
        /// <summary>
        /// 未使用的ID
        /// </summary>
        protected internal readonly ConcurrentQueue<int> UnusedIds = new ConcurrentQueue<int>();
        /// <summary>
        /// 文件路径
        /// </summary>
        protected internal string FilePath { get; set; } = string.Empty;
        /// <summary>
        /// 文件锁
        /// </summary>
        protected bool FileLock = false;
        /// <summary>
        /// 构造函数
        /// </summary>
        public ISEADocument()
        {
            Codecs.TryAdd((ushort)StandardNodeType.CONFIG, new ConfigNodeCodec());
            Codecs.TryAdd((ushort)StandardNodeType.IMAGE, new ImageNodeCodec());
            Codecs.TryAdd((ushort)StandardNodeType.MESH, new MeshNodeCodec());
            Codecs.TryAdd((ushort)StandardNodeType.RENDEROBJECT, new RenderObjectNodeCodec());
            Codecs.TryAdd((ushort)StandardNodeType.TEXTURE, new TextureNodeCodec());
        }

        /// <summary>
        /// 通过Id获取节点
        /// </summary>
        /// <typeparam name="TNode"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual TNode GetNodeFromId<TNode>(int id) where TNode : Node
        {
            if (Nodes.TryGetValue(id, out Node node))
            {
                if (node is TNode r) return r;
                else throw new InvalidCastException($"Target node with id {id} is not {nameof(TNode)}");
            }
            else
            {
                node = ReadNodeFromId(id);
                if (node is TNode r) return r;
                else throw new InvalidCastException($"Target node with id {id} is not {nameof(TNode)}");
            }
        }
        /// <summary>
        /// 通过名称获取节点
        /// </summary>
        /// <typeparam name="TNode"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public virtual TNode GetNodeFromName<TNode>(string name)
            where TNode : Node
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));
            if (NodesWithNames.TryGetValue(name, out Node node))
            {
                if (node is TNode r) return r;
                else throw new InvalidCastException($"Target node with name {name} is not {nameof(TNode)}");
            }
            else
            {
                if (NamesMapping.TryGetIdWithName(name, out int id))
                {
                    node = ReadNodeFromId(id);
                    if (node is TNode r) return r;
                    else throw new InvalidCastException($"Target node with id {name} is not {nameof(TNode)}");
                }
                else
                    throw new NullReferenceException($"Cannot found node with name {name}, target node is non-existent");
            }
        }
        /// <summary>
        /// 获取所有同一类型的所有节点
        /// </summary>
        /// <typeparam name="TNode"></typeparam>
        /// <returns></returns>
        public virtual TNode[] GetNodes<TNode>() where TNode : Node, new()
        {
            TNode node = new TNode();
            ushort type = node.GetNodeType();
            List<TNode> nodes = new List<TNode>();
            foreach(var item in Indexes.Indexes)
            {
                if(item.Value.NodeType == type)
                {
                    TNode n = GetNodeFromId<TNode>((int)item.Value.Id);
                    nodes.Add(n);
                }
            }
            return nodes.ToArray();
        }
        /// <summary>
        /// 尝试根据Id获取节点
        /// </summary>
        /// <typeparam name="TNode"></typeparam>
        /// <param name="id"></param>
        /// <param name="node"></param>
        /// <returns></returns>
        public virtual bool TryGetNodeFromId<TNode>(int id, out TNode node) where TNode : Node
        {
            if (Nodes.TryGetValue(id, out Node old))
            {
                if (old is TNode r)
                {
                    node = r;
                    return true;
                }
            }
            node = null;
            return false;
        }
        /// <summary>
        /// 尝试根据名称获取节点
        /// </summary>
        /// <typeparam name="TNode"></typeparam>
        /// <param name="name"></param>
        /// <param name="node"></param>
        /// <returns></returns>
        public virtual bool TryGetNodeFromName<TNode>(string name, out TNode node)
            where TNode : Node
        {
            node = default;
            if (string.IsNullOrEmpty(name)) return false;
            if (NodesWithNames.TryGetValue(name, out Node n))
            {
                if (n is TNode r)
                {
                    node = r;
                    return true;
                }
                else return false;
            }
            else
            {
                if (NamesMapping.TryGetIdWithName(name, out int id))
                {
                    var rN = ReadNodeFromId(id);
                    if (rN is TNode r)
                    {
                        node = r;
                        return true;
                    }
                    return false;
                }
                else return false;
            }
        }

        /// <summary>
        /// 添加新的节点
        /// </summary>
        /// <param name="node"></param>
        /// <param name="id"></param>
        public virtual void AddNode(Node node, out int id)
        {
            void NewNode(NodeHeader header, string name, out int id)
            {
                id = GetNewId();
                header.Id = (uint)id;
                header.NodeType = node.GetNodeType();
                node.Header = header;
                lock (SyncRoot)
                {
                    Nodes.TryAdd(id, node);
                    NodesWithNames.TryAdd(name, node);
                    Indexes.NewNodeIndex(header);
                }
            }
            var header = node.Header;
            id = (int)header.Id;
            if (header.Id <= 0)
            {
                var name = GetNewName(node.Name);
                node.Name = name;
                NewNode(header, node.Name, out id);
            }
            else
            {
                if (Nodes.TryGetValue((int)header.Id, out Node oldNode) && NodesWithNames.ContainsKey(node.Name))
                {
                    if (oldNode.GetType() != node.GetType()) throw new InvalidOperationException($"Already has node with Id {id}");
                    else
                    {
                        Nodes.TryUpdate(id, node, oldNode);
                        NodesWithNames.TryUpdate(node.Name, node, oldNode);
                    }
                }
                else
                {
                    if (Indexes.Indexes.TryGetValue((uint)id, out var v))
                    {
                        Nodes.AddOrUpdate(id, node, (key, value) => node);
                        NodesWithNames.AddOrUpdate(node.Name, node, (key, value) => node);
                    }
                    else
                    {
                        var name = GetNewName(node.Name);
                        node.Name = name;
                        NewNode(header, node.Name, out id);
                    }
                }
            }
        }

        /// <summary>
        /// 从文件中读取指定Id的节点
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual Node ReadNodeFromId(int id)
        {
            if (!File.Exists(FilePath)) throw new FileNotFoundException("Cannot found document file", FilePath);
            if (!Indexes.GetNodeIndexFromId(id, out NodeIndex nodeIndex)) throw new NullReferenceException($"connot found node with id {id}");
            if (!Codecs.TryGetValue(nodeIndex.NodeType, out INodeCodec codec)) throw new NotSupportedException($"No NodeCodec Cound Decode this node with the type {nodeIndex.NodeType}");
            uint start = nodeIndex.Offset;
            uint length = nodeIndex.Length;
            FileInfo file = new FileInfo(FilePath);
            using FileStream stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.Read);
            try
            {
                Memory<byte> memory = new Memory<byte>(new byte[length]);
                Span<byte> local = memory.Span;
                stream.Seek(start, SeekOrigin.Begin);
                stream.Read(local);
                Node node = codec.Decode(memory, this);
                if (node != null) return node;
                else throw new InvalidOperationException($"Decode node with id {id} failed");
            }
            catch
            {
                throw;
            }
            finally { stream.Close(); }
        }

        /// <summary>
        /// 保存至指定路径
        /// </summary>
        /// <param name="basePath"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public virtual Task Save(string basePath, string name)
        {
            if (!Directory.Exists(basePath)) throw new DirectoryNotFoundException($"Cannot found directory with path {basePath}");
            string fileName = Path.Combine(basePath, name + Constant.EXTENSION);
            return WriteTo(fileName);
        }

        /// <summary>
        /// 加载
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public virtual Task Load(string path)
        {
            if (!File.Exists(path)) throw new FileNotFoundException($"Cannot found file with path {path}");
            FileInfo file = new FileInfo(path);
            using FileStream stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.Read);
            try
            {
                ReadFileHeader(stream);
                ReadIndexesNode(stream);
                ReadNameMapping(stream);
                FilePath = path;
            }
            finally { stream.Close(); }
            return Task.CompletedTask;
        }
        /// <summary>
        /// 加载并读取所有节点至缓存
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public virtual async Task LoadAllNodes(string path)
        {
            await Load(path);
            foreach(var item in Indexes.Indexes)
            {
                ReadNodeFromId((int)item.Key);
            }
        }

        /// <summary>
        /// 将文档写入文件
        /// </summary>
        /// <param name="path"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        protected virtual async Task WriteTo(string path, byte endian = 0)
        {
            while (Volatile.Read(ref FileLock)) { SpinWait.SpinUntil(() => !FileLock); }
            Volatile.Write(ref FileLock, true);
            int count = Nodes.Count;
            Exception exception = null;
            await Task.Run(() =>
            {
                FileInfo file = new FileInfo(path);
                try
                {
                    if (count > 0)
                    {
                        using FileStream stream = file.Open(FileMode.Create, FileAccess.Write, FileShare.None);
                        try
                        {
                            using CountdownEvent countdown = new CountdownEvent(count);
                            Memory<byte>[] memories = new Memory<byte>[count]; int index = 0;
                            CodecOperationResult[] Results = new CodecOperationResult[count];
                            Parallel.ForEach(Nodes, (value) =>
                            {
                                CodecOperationResult result = new CodecOperationResult();
                                var i = Interlocked.Increment(ref index);
                                try
                                {
                                    Node node = value.Value;
                                    if (Codecs.TryGetValue(node.Header.NodeType, out INodeCodec nodeCodec))
                                    {
                                        Memory<byte> memory = nodeCodec.Encode(node, this);
                                        memories[i - 1] = memory;
                                        result.IsSuccess = true;
                                        result.Message = "Success";
                                        result.NodeHeader = node.Header;
                                        result.Node = node;
                                    }
                                    else throw new NotSupportedException($"Cannot found codec with node {node.GetType().FullName}");
                                }
                                catch (Exception e)
                                {
                                    result.IsSuccess = false;
                                    result.Message = e.Message;
                                }
                                finally
                                {
                                    Results[i - 1] = result;
                                    countdown.Signal();
                                }
                            });
                            countdown.Wait();

                            var names = NamesMapping.NameMappings;
                            names.Clear();
                            Indexes.Indexes.Clear();
                            int indexCount = 0;
                            for (int i = 0; i < count; i++)
                            {
                                CodecOperationResult local = Results[i];
                                if (local.IsSuccess)
                                {
                                    var header = local.NodeHeader;
                                    names.Add(new NamesMapping() { NodeName = local.Node.Name, NodeId = (int)header.Id });
                                    indexCount++;
                                }

                            }
                            Memory<byte> mappingMemory = NamesMappingNodeCodec.Encode(NamesMapping, this);

                            uint indexesLength = (uint)(Constant.SIGNLENGTH + Constant.NODEHEADERLENGTH + Constant.NODEINDEXLENGTH * indexCount);
                            uint offset = indexesLength + (uint)mappingMemory.Length + Constant.FILEHEADERLENGTH + Constant.SIGNLENGTH;
                            for (int i = 0; i < count; i++)
                            {
                                Memory<byte> data = memories[i];
                                CodecOperationResult local = Results[i];
                                if (local.IsSuccess)
                                {
                                    var header = local.NodeHeader;
                                    NodeIndex nodeIndex = new NodeIndex { Id = header.Id, Length = (uint)data.Length, NodeType = header.NodeType, Offset = offset };
                                    Indexes.Indexes.Add(header.Id, nodeIndex);
                                    offset += (uint)data.Length;
                                }
                            }
                            Memory<byte> indexesMemory = IndexNodeCodec.Encode(Indexes, this);
                            Memory<byte> fileHeader = new byte[Constant.FILEHEADERLENGTH + Constant.SIGNLENGTH];
                            WriteFileHeader(fileHeader, 0, endian);
                            WriteMemory(fileHeader, stream);
                            WriteMemory(indexesMemory, stream);
                            WriteMemory(mappingMemory, stream);
                            for (int i = 0; i < count; i++)
                            {
                                Memory<byte> data = memories[i];
                                CodecOperationResult local = Results[i];
                                if (local.IsSuccess) WriteMemory(data, stream);
                            }
                        }
                        catch
                        {
                            throw;
                        }
                        finally { stream.Close(); }
                    }
                }
                catch(Exception e)
                {
                    exception = e;
                }
                finally
                {
                    Volatile.Write(ref FileLock, false);
                }
            });
            if (exception != null) throw exception;
        }

        /// <summary>
        /// 获取新Id
        /// </summary>
        /// <returns></returns>
        protected virtual int GetNewId()
        {
            if (UnusedIds.TryDequeue(out int id)) return id;
            else return Interlocked.Increment(ref ID);
        }
        /// <summary>
        /// 获取新的名称
        /// </summary>
        /// <returns></returns>
        protected virtual string GetNewName(string name)
        {
            if(string.IsNullOrEmpty(name) || string.IsNullOrWhiteSpace(name))
            {
                var r = string.IsNullOrEmpty(name) || string.IsNullOrWhiteSpace(name) ? Guid.NewGuid().ToString("N") : name;
                while (NodesWithNames.ContainsKey(r))
                {
                    r = Guid.NewGuid().ToString("N");
                }
                return r;
            }
            else
            {
                while (NodesWithNames.ContainsKey(name))
                {
                    name = Guid.NewGuid().ToString("N");
                }
                return name;
            }
        }

        /// <summary>
        /// 写入文件头
        /// </summary>
        /// <param name="memory"></param>
        /// <param name="compress"></param>
        /// <param name="endian"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void WriteFileHeader(Memory<byte> memory, byte compress, byte endian)
        {
            int index = 0; int sign = Constant.LITTLEFILESIGN;
            FileHeader header = new FileHeader()
            {
                Compress = compress,
                Endian = endian,
                MaxId = ID,
            };
            Span<byte> local = memory.Span;
            MemoryMarshal.Write(local.Slice(index, Constant.SIGNLENGTH), ref sign); index += Constant.SIGNLENGTH;
            MemoryMarshal.Write(local[index..], ref header);
        }
        /// <summary>
        /// 写入内存
        /// </summary>
        /// <param name="memory"></param>
        /// <param name="stream"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void WriteMemory(Memory<byte> memory, Stream stream)
        {
            ReadOnlyMemory<byte> local = memory;
            stream.Write(local.Span);
        }
        /// <summary>
        /// 读取文件头
        /// </summary>
        /// <param name="stream"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void ReadFileHeader(Stream stream)
        {
            Span<byte> data = stackalloc byte[Constant.FILEHEADERLENGTH + Constant.SIGNLENGTH];
            stream.Read(data);
            int sign = MemoryMarshal.Read<int>(data.Slice(0, Constant.INTLENGTH));
            if (sign != Constant.LITTLEFILESIGN) throw new InvalidOperationException("Target file is not a ISEA file");
            int index = Constant.INTLENGTH; ReadOnlySpan<byte> l = data;
            FileHeader header = MemoryMarshal.Read<FileHeader>(l.Slice(index, Constant.FILEHEADERLENGTH));
            Compress = header.Compress; Endian = header.Endian;
            ID = header.MaxId;
        }
        /// <summary>
        /// 读取索引节点
        /// </summary>
        /// <param name="stream"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void ReadIndexesNode(Stream stream)
        {
            Span<byte> local = stackalloc byte[Constant.NODEHEADERLENGTH + Constant.SIGNLENGTH];
            stream.Read(local);
            NodeHeader header = MemoryMarshal.Read<NodeHeader>(local[Constant.SIGNLENGTH..]);
            var length = header.Length + local.Length;
            Memory<byte> data = new byte[length];
            local.CopyTo(data.Span);
            stream.Read(data.Span[local.Length..]);
            IndexNode indexes = IndexNodeCodec.Decode(data, this, Endian) as IndexNode;
            Indexes = indexes;
        }
        /// <summary>
        /// 读取名称映射
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void ReadNameMapping(Stream stream)
        {
            Span<byte> local = stackalloc byte[Constant.NODEHEADERLENGTH + Constant.SIGNLENGTH];
            stream.Read(local);
            NodeHeader header = MemoryMarshal.Read<NodeHeader>(local[Constant.SIGNLENGTH..]);
            var length = header.Length + local.Length;
            Memory<byte> data = new byte[length];
            local.CopyTo(data.Span);
            stream.Read(data.Span[local.Length..]);
            NamesMappingNode names = NamesMappingNodeCodec.Decode(data, this, Endian) as NamesMappingNode;
            NamesMapping = names;
        }
    }
}
