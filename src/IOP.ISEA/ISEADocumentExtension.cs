﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;

namespace IOP.ISEA
{
    /// <summary>
    /// 文档扩展类
    /// </summary>
    public static class ISEADocumentExtension
    {
        /// <summary>
        /// 创建网格节点
        /// </summary>
        /// <param name="document"></param>
        /// <param name="name"></param>
        /// <param name="vCount"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public static MeshNode CreateEmptyMeshNode(this ISEADocument document, string name, int vCount, MeshMode mode = MeshMode.Vertex)
        {
            if (string.IsNullOrEmpty(name)) name = Guid.NewGuid().ToString("N");
            MeshNode mesh = new MeshNode();
            mesh.Name = name;
            mesh.VerticesCount = vCount;
            mesh.MeshMode = mode;
            document.AddNode(mesh, out _);
            return mesh;
        }
        /// <summary>
        /// 添加网格数据
        /// </summary>
        /// <typeparam name="TStruct"></typeparam>
        /// <param name="stride"></param>
        /// <param name="mesh"></param>
        /// <param name="struct"></param>
        /// <param name="seqIndex"></param>
        /// <param name="purpose"></param>
        /// <param name="compressed"></param>
        /// <returns></returns>
        public static MeshNode AddMeshData<TStruct>(this MeshNode mesh, TStruct[] @struct, ushort stride, int seqIndex, 
            MeshDataPurpose purpose, bool compressed)
            where TStruct : struct
        {
            if (mesh.VerticesComponents == null) mesh.VerticesComponents = new List<MeshData>();
            if (mesh.VerticesComponentSequence == null) mesh.VerticesComponentSequence = new List<int>();
            MeshDataType type; Type t = typeof(TStruct);
            if (t == typeof(byte)) type = MeshDataType.BYTE;
            else if (t == typeof(short)) type = MeshDataType.SHORT;
            else if (t == typeof(ushort)) type = MeshDataType.USHORT;
            else if (t == typeof(int)) type = MeshDataType.INT;
            else if (t == typeof(uint)) type = MeshDataType.UINT;
            else if (t == typeof(long)) type = MeshDataType.LONG;
            else if (t == typeof(ulong)) type = MeshDataType.ULONG;
            else if (t == typeof(float)) type = MeshDataType.FLOAT;
            else if (t == typeof(double)) type = MeshDataType.DOUBLE;
            else if (t == typeof(Vector2)) type = MeshDataType.VECTOR2;
            else if (t == typeof(Vector3)) type = MeshDataType.VECTOR3;
            else if (t == typeof(Vector4)) type = MeshDataType.VECTOR4;
            else type = MeshDataType.COMPLEX; Span<TStruct> span = @struct;
            Span<byte> binary = MemoryMarshal.AsBytes(span);
            MeshData c = new MeshData()
            {
                Data = DataArray<byte>.Create(binary.ToArray()),
                DataType = type,
                IsCompressed = (byte)(compressed ? 1 : 0),
                Purpose = purpose,
                Stride = stride
            };
            mesh.VerticesComponents.Add(c);
            mesh.VerticesComponentSequence.Add(seqIndex);
            mesh.VerticesStride += stride;
            return mesh;
        }
        /// <summary>
        /// 添加索引数据
        /// </summary>
        /// <param name="mesh"></param>
        /// <param name="indexes"></param>
        /// <param name="compressed"></param>
        /// <returns></returns>
        public static MeshNode AddIndexesData(this MeshNode mesh, uint[] indexes, bool compressed)
        {
            Span<uint> span = indexes;
            Span<byte> binary = MemoryMarshal.AsBytes(span);
            MeshData data = new MeshData
            {
                Data = DataArray<byte>.Create(binary.ToArray()),
                DataType = MeshDataType.UINT,
                IsCompressed = (byte)(compressed ? 1 : 0),
                Purpose = MeshDataPurpose.INDEXES,
                Stride = sizeof(uint)
            };
            mesh.Indexes = data;
            return mesh;
        }
        /// <summary>
        /// 添加间接绘制数据
        /// </summary>
        /// <param name="mesh"></param>
        /// <param name="datas"></param>
        /// <returns></returns>
        public static MeshNode AddDrawIndirectData(this MeshNode mesh, DrawIndirectData[] datas)
        {
            if (mesh.DrawIndirectCommands == null) mesh.DrawIndirectCommands = new List<MeshData>();
            Span<DrawIndirectData> span = datas;
            Span<byte> binary = MemoryMarshal.AsBytes(span);
            MeshData c = new MeshData
            {
                Data = DataArray<byte>.Create(binary.ToArray()),
                DataType = MeshDataType.COMPLEX,
                IsCompressed = 0,
                Purpose = MeshDataPurpose.DRAWINDIRECT,
                Stride = (ushort)(Marshal.SizeOf<DrawIndirectData>())
            };
            mesh.DrawIndirectCommands.Add(c);
            return mesh;
        }
        /// <summary>
        /// 添加间接索引绘制数据
        /// </summary>
        /// <param name="mesh"></param>
        /// <param name="datas"></param>
        /// <returns></returns>
        public static MeshNode AddDrawIndexedIndirectData(this MeshNode mesh, DrawIndexedIndirectData[] datas)
        {
            if (mesh.DrawIndirectCommands == null) mesh.DrawIndirectCommands = new List<MeshData>();
            Span<DrawIndexedIndirectData> span = datas;
            Span<byte> binary = MemoryMarshal.AsBytes(span);
            MeshData c = new MeshData
            {
                Data = DataArray<byte>.Create(binary.ToArray()),
                DataType = MeshDataType.COMPLEX,
                IsCompressed = 0,
                Purpose = MeshDataPurpose.DRAWINDEXEDINDIRECT,
                Stride = (ushort)(Marshal.SizeOf<DrawIndexedIndirectData>())
            };
            mesh.DrawIndirectCommands.Add(c);
            return mesh;
        }

        /// <summary>
        /// 创建配置节点
        /// </summary>
        /// <param name="document"></param>
        /// <param name="config"></param>
        /// <param name="engine"></param>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static ConfigNode CreateConfigNode(this ISEADocument document, string name, string config, 
            ConfigEngine engine, ConfigType type = ConfigType.Json)
        {
            if (string.IsNullOrEmpty(name)) name = Guid.NewGuid().ToString("N");
            if (string.IsNullOrEmpty(config)) config = string.Empty;
            ConfigNode configNode = new ConfigNode()
            {
                Config = config,
                Engine = engine.ToString(),
                Type = type.ToString(),
                Name = name
            };
            document.AddNode(configNode, out _);
            return configNode;
        }

        /// <summary>
        /// 创建图像节点
        /// </summary>
        /// <param name="document"></param>
        /// <param name="path"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static ImageNode CreateImageNode(this ISEADocument document, string name, string path)
        {
            if (!File.Exists(path)) throw new FileNotFoundException(path);
            FileInfo info = new FileInfo(path);
            byte[] data = File.ReadAllBytes(path);
            return CreateImageNode(document, name, info.Extension, data);
        }
        /// <summary>
        /// 创建图像节点
        /// </summary>
        /// <param name="document"></param>
        /// <param name="extension"></param>
        /// <param name="data"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static ImageNode CreateImageNode(this ISEADocument document, string name, string extension, byte[] data)
        {
            if (string.IsNullOrEmpty(name)) name = Guid.NewGuid().ToString("N");
            ImageNode imageNode = new ImageNode
            {
                Extension = extension,
                ImageData = DataArray<byte>.Create(data),
                Name = name,
            };
            imageNode.ImageData.IsCompressed = 1;
            document.AddNode(imageNode, out _);
            return imageNode;
        }

        /// <summary>
        /// 创建纹理节点
        /// </summary>
        /// <param name="document"></param>
        /// <param name="name"></param>
        /// <param name="usage"></param>
        /// <param name="image"></param>
        /// <param name="texConfig"></param>
        /// <param name="sampler"></param>
        /// <param name="setIndex"></param>
        /// <param name="binding"></param>
        /// <param name="arrayElement"></param>
        /// <param name="descriptorCount"></param>
        /// <returns></returns>
        public static TextureNode CreateTextureNode(this ISEADocument document, string name, TextureUsage usage, ImageNode image,
            ConfigNode texConfig, ConfigNode sampler, uint setIndex, uint binding = 0, uint arrayElement = 0, uint descriptorCount = 1)
        {
            if (string.IsNullOrEmpty(name)) name = Guid.NewGuid().ToString("N");
            TextureNode node = new TextureNode
            {
                ArrayElement = arrayElement,
                Binding = binding,
                DescriptorCount = descriptorCount,
                Image = image,
                Name = name,
                SamplerConfig = sampler,
                TexConfig = texConfig,
                SetIndex = setIndex,
                Usage = usage
            };
            document.AddNode(node, out _);
            return node;
        }

        /// <summary>
        /// 创建渲染对象节点
        /// </summary>
        /// <param name="document"></param>
        /// <param name="name"></param>
        /// <param name="components"></param>
        /// <returns></returns>
        public static RenderObjectNode CreateRenderObjectNode(this ISEADocument document, string name, params Node[] components)
        {
            if (string.IsNullOrEmpty(name)) name = Guid.NewGuid().ToString("N");
            RenderObjectNode renderObject = new RenderObjectNode()
            {
                Name = name,
                Comonents = new List<Node>(components)
            };
            document.AddNode(renderObject, out _);
            return renderObject;
        }
    }

    /// <summary>
    /// 间接绘制数据
    /// </summary>
    public struct DrawIndirectData
    {
        /// <summary>
        /// 
        /// </summary>
        public uint VertexCount;
        /// <summary>
        /// 
        /// </summary>
        public uint InstanceCount;
        /// <summary>
        /// 
        /// </summary>
        public uint FirstVertex;
        /// <summary>
        /// 
        /// </summary>
        public uint FirstInstance;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="vertexCount"></param>
        /// <param name="instanceCount"></param>
        /// <param name="firstVertex"></param>
        /// <param name="firstInstance"></param>
        public DrawIndirectData(uint vertexCount, uint instanceCount, uint firstVertex, uint firstInstance)
        {
            VertexCount = vertexCount;
            InstanceCount = instanceCount;
            FirstVertex = firstVertex;
            FirstInstance = firstInstance;
        }
    }
    /// <summary>
    /// 间接索引绘制
    /// </summary>
    public struct DrawIndexedIndirectData
    {
        /// <summary>
        /// 
        /// </summary>
        public uint IndexCount;
        /// <summary>
        /// 
        /// </summary>
        public uint InstanceCount;
        /// <summary>
        /// 
        /// </summary>
        public uint FirstIndex;
        /// <summary>
        /// 
        /// </summary>
        public int VertexOffset;
        /// <summary>
        /// 
        /// </summary>
        public uint FirstInstance;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="indexCount"></param>
        /// <param name="instanceCount"></param>
        /// <param name="firstIndex"></param>
        /// <param name="vertexOffset"></param>
        /// <param name="firstInstance"></param>
        public DrawIndexedIndirectData(uint indexCount, uint instanceCount, uint firstIndex, int vertexOffset, uint firstInstance)
        {
            IndexCount = indexCount;
            InstanceCount = instanceCount;
            FirstIndex = firstIndex;
            VertexOffset = vertexOffset;
            FirstInstance = firstInstance;
        }
    }
    /// <summary>
    /// 配置所属引擎
    /// </summary>
    public enum ConfigEngine
    {
        /// <summary>
        /// 
        /// </summary>
        Vulkan,
        /// <summary>
        /// 
        /// </summary>
        OpenGL
    }
    /// <summary>
    /// 配置载体
    /// </summary>
    public enum ConfigType
    {
        /// <summary>
        /// 
        /// </summary>
        Json
    }
}
