﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA
{
    /// <summary>
    /// 普通节点基类
    /// </summary>
    public abstract class Node
    {
        /// <summary>
        /// 标识符
        /// </summary>
        public readonly byte[] Sign = Constant.NODE;
        /// <summary>
        /// 节点头
        /// </summary>
        public NodeHeader Header { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public Utf8String Name { get; set; } = Guid.NewGuid().ToString("N");
        /// <summary>
        /// 获取节点类型
        /// </summary>
        /// <returns></returns>
        public virtual ushort GetNodeType() => ushort.MaxValue;
    }

    /// <summary>
    /// 简单节点
    /// </summary>
    public class SimpleNode : Node { }
    /// <summary>
    /// 复杂节点
    /// </summary>
    public class ComplexNode : Node { }
}
