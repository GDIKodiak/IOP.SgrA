﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA
{
    /// <summary>
    /// ISEA文档接口
    /// </summary>
    public interface IISEADocument
    {
        /// <summary>
        /// 以ID为依据获取节点
        /// </summary>
        /// <typeparam name="TNode"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        TNode GetNodeFromId<TNode>(int id) where TNode : Node;
        /// <summary>
        /// 以名称为依据获取节点
        /// </summary>
        /// <typeparam name="TNode"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        TNode GetNodeFromName<TNode>(string name) where TNode : Node;
        /// <summary>
        /// 尝试根据Id获取节点
        /// </summary>
        /// <typeparam name="TNode"></typeparam>
        /// <param name="id"></param>
        /// <param name="node"></param>
        /// <returns></returns>
        bool TryGetNodeFromId<TNode>(int id, out TNode node) where TNode : Node;
        /// <summary>
        /// 尝试根据名称获取节点
        /// </summary>
        /// <typeparam name="TNode"></typeparam>
        /// <param name="name"></param>
        /// <param name="node"></param>
        /// <returns></returns>
        bool TryGetNodeFromName<TNode>(string name, out TNode node) where TNode : Node;
        /// <summary>
        /// 获取所有同一类型的节点
        /// </summary>
        /// <typeparam name="TNode"></typeparam>
        /// <returns></returns>
        TNode[] GetNodes<TNode>() where TNode : Node, new();
        /// <summary>
        /// 添加新的节点
        /// </summary>
        /// <param name="node"></param>
        /// <param name="Id"></param>
        void AddNode(Node node, out int Id);
        /// <summary>
        /// 保存至特定路径
        /// </summary>
        /// <param name="basePath"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        Task Save(string basePath, string name);
        /// <summary>
        /// 加载文档
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        Task Load(string path);
        /// <summary>
        /// 加载所有节点
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        Task LoadAllNodes(string path);
    }
}
