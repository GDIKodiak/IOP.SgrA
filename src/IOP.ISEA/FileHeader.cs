﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace IOP.ISEA
{
    /// <summary>
    /// 文件头
    /// </summary>
    [StructLayout(LayoutKind.Explicit)]
    public struct FileHeader
    {
        /// <summary>
        /// 字节序
        /// </summary>
        [FieldOffset(0)]
        public byte Endian;
        /// <summary>
        /// 文件是否进行了压缩
        /// </summary>
        [FieldOffset(1)]
        public byte Compress;
        /// <summary>
        /// 最大ID
        /// </summary>
        [FieldOffset(2)]
        public int MaxId;
        /// <summary>
        /// 保留
        /// </summary>
        [FieldOffset(6)]
        public ushort Reserved1;
        /// <summary>
        /// 保留
        /// </summary>
        [FieldOffset(8)]
        public uint Reserved2;
    }
}
