﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace IOP.ISEA
{
    /// <summary>
    /// 索引数据
    /// </summary>
    public class IndexesData
    {
        /// <summary>
        /// 
        /// </summary>
        public uint[] Indexes { get; private set; } 
        /// <summary>
        /// 顶点数据
        /// 包含坐标，法向，UV等
        /// </summary>
        public Vector3[] Vertexes { get; private set; }
        /// <summary>
        /// 顶点数量
        /// </summary>
        public uint VertexCount {  get; private set; }
        /// <summary>
        /// 顶点最大值
        /// </summary>
        public Vector3 MaxVertor { get; private set; }
        /// <summary>
        /// 顶点最小值
        /// </summary>
        public Vector3 MinVertor { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="indexes"></param>
        /// <param name="vertexes"></param>
        /// <param name="maxVertor"></param>
        /// <param name="minVertor"></param>
        public IndexesData(uint[] indexes, Vector3[] vertexes, uint vCount, Vector3 maxVertor, Vector3 minVertor)
        {
            Indexes = indexes;
            Vertexes = vertexes;
            MaxVertor = maxVertor;
            MinVertor = minVertor;
            VertexCount = vCount;
        }
    }
}
