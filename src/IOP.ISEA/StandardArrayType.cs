﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA
{
    /// <summary>
    /// 获取节点类型
    /// </summary>
    public enum StandardArrayType : byte
    {
        /// <summary>
        /// byte
        /// </summary>
        b = 0,
        /// <summary>
        /// ushort
        /// </summary>
        us = 1,
        /// <summary>
        /// short
        /// </summary>
        s = 2,
        /// <summary>
        /// uint
        /// </summary>
        ui = 3,
        /// <summary>
        /// int
        /// </summary>
        i = 4,
        /// <summary>
        /// ulong
        /// </summary>
        ul = 5,
        /// <summary>
        /// long
        /// </summary>
        l = 6,
        /// <summary>
        /// float
        /// </summary>
        f = 7,
        /// <summary>
        /// double
        /// </summary>
        d = 8,
        /// <summary>
        /// vector2
        /// </summary>
        v2 = 9,
        /// <summary>
        /// vector3
        /// </summary>
        v3 = 10,
        /// <summary>
        /// vector4
        /// </summary>
        v4 = 11
    }
}
