﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace IOP.ISEA
{
    /// <summary>
    /// 节点头
    /// </summary>
    [StructLayout(LayoutKind.Explicit)]
    public struct NodeHeader
    {
        /// <summary>
        /// 节点ID
        /// </summary>
        [FieldOffset(0)]
        public uint Id;
        /// <summary>
        /// 节点类型
        /// </summary>
        [FieldOffset(4)]
        public ushort NodeType;
        /// <summary>
        /// 版本
        /// </summary>
        [FieldOffset(6)]
        public ushort Version;
        /// <summary>
        /// 长度
        /// </summary>
        [FieldOffset(8)]
        public uint Length;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="header1"></param>
        /// <param name="header2"></param>
        /// <returns></returns>
        public static bool operator == (NodeHeader header1, NodeHeader header2)
        {
            return header1.Id == header2.Id && 
                header1.Length == header2.Length && 
                header1.NodeType == header2.NodeType && 
                header1.Version == header2.Version;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="header1"></param>
        /// <param name="header2"></param>
        /// <returns></returns>
        public static bool operator != (NodeHeader header1, NodeHeader header2)
        {
            return header1.Id != header2.Id ||
                header1.Length != header2.Length ||
                header1.NodeType != header2.NodeType ||
                header1.Version != header2.Version;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            NodeHeader header = (NodeHeader)obj;
            return this == header;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode() => base.GetHashCode();
    }
}
