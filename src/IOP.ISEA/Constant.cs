﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace IOP.ISEA
{
    /// <summary>
    /// 常量
    /// </summary>
    public static class Constant
    {
        /// <summary>
        /// 扩展名
        /// </summary>
        public const string EXTENSION = ".isea";
        /// <summary>
        /// 文件头
        /// </summary>
        public static byte[] FILEHEADER = new byte[] { 0x49, 0x53, 0x45, 0x41 };
        /// <summary>
        /// 节点标识
        /// </summary>
        public static byte[] NODE = new byte[] { 0x4E, 0x4F, 0x44, 0x45 };
        /// <summary>
        /// 引用节点标识
        /// </summary>
        public static byte[] REFN = new byte[] { 0x52, 0x45, 0x46, 0x4e };
        /// <summary>
        /// 空引用
        /// </summary>
        public static byte[] NULL = new byte[] { 0x4e, 0x55, 0x4c, 0x4c };
        /// <summary>
        /// 小字节序ISEA标识
        /// </summary>
        public const int LITTLEFILESIGN = 0x41_45_53_49;
        /// <summary>
        /// 小字节序NODE节点标识
        /// </summary>
        public const int LITTLENODESIGN = 0x45_44_4F_4E;
        /// <summary>
        /// 小字节序REFN节点标识
        /// </summary>
        public const int LITTLEREFNSIGN = 0x4E_46_45_52;
        /// <summary>
        /// 小字节序NULL节点标识
        /// </summary>
        public const int LITTLENULLSIGN = 0x4C_4C_55_4E;
        /// <summary>
        /// 节点头长度
        /// </summary>
        public const int NODEHEADERLENGTH = 12;
        /// <summary>
        /// 标识符长度
        /// </summary>
        public const int SIGNLENGTH = 4;
        /// <summary>
        /// 引用节点长度
        /// </summary>
        public const int REFNLENGTH = 8;
        /// <summary>
        /// byte类型占用长度
        /// </summary>
        public const int BYTELENGTH = 1;
        /// <summary>
        /// int类型占用长度
        /// </summary>
        public const int INTLENGTH = 4;
        /// <summary>
        /// uint类型占用长度
        /// </summary>
        public const int UINTLENGTH = 4;
        /// <summary>
        /// ushort类型占用长度
        /// </summary>
        public const int USHORTLENGTH = 2;
        /// <summary>
        /// short类型占用长度
        /// </summary>
        public const int SHORTLENGTH = 2;
        /// <summary>
        /// ulong类型占用长度
        /// </summary>
        public const int ULONGLENGTH = 8;
        /// <summary>
        /// long类型占用长度
        /// </summary>
        public const int LONGLENGTH = 8;
        /// <summary>
        /// 单精度浮点数长度
        /// </summary>
        public const int FLOATLENGTH = 4;
        /// <summary>
        /// 双精度浮点数长度
        /// </summary>
        public const int DOUBLELENGTH = 8;
        /// <summary>
        /// 二维向量长度
        /// </summary>
        public const int VECTOR2LENGTH = 8;
        /// <summary>
        /// 三维向量长度
        /// </summary>
        public const int VECTOR3LENGTH = 12;
        /// <summary>
        /// 四维向量长度
        /// </summary>
        public const int VECTOR4LENGTH = 16;
        /// <summary>
        /// 四元数长度
        /// </summary>
        public const int QUATERNIONLENGTH = 16;
        /// <summary>
        /// 节点索引长度
        /// </summary>
        public const int NODEINDEXLENGTH = 16;
        /// <summary>
        /// 文件头长度
        /// </summary>
        public const int FILEHEADERLENGTH = 12;
        /// <summary>
        /// Brotli压缩质量
        /// </summary>
        public const int BROTLIQUALITY = 4;
        /// <summary>
        /// Brotli压缩窗口大小
        /// </summary>
        public const int BROTLIWINDOW = 16;
        /// <summary>
        /// AABB长度
        /// </summary>
        public const int BOUNDINGLENGTH = 24;
    }
}
