﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.IO.Compression;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;

namespace IOP.ISEA
{
    /// <summary>
    /// 数据集合
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    public class DataArray<TData>
        where TData : struct
    {
        /// <summary>
        /// 数组长度
        /// </summary>
        public int ArrayLength { get; private set; }
        /// <summary>
        /// 数组类型
        /// </summary>
        public byte ArrayType { get; private set; }
        /// <summary>
        /// 是否压缩
        /// </summary>
        public byte IsCompressed { get; set; }
        /// <summary>
        /// 单个数据占用大小
        /// </summary>
        public ushort DataSize { get; private set; }
        /// <summary>
        /// 数组
        /// </summary>
        public TData[] Arrays 
        { 
            get => _Arrays; 
            set 
            {
                if (value == null) _Arrays = Array.Empty<TData>();
                else _Arrays = value;
                ArrayLength = _Arrays.Length;
            } 
        }

        private TData[] _Arrays;

        /// <summary>
        /// 写入数据
        /// </summary>
        /// <param name="endian"></param>
        public Span<byte> ToSpan(byte endian = 0)
        {
            if (Arrays == null) Arrays = Array.Empty<TData>();
            ArrayLength = Arrays.Length; int length = ArrayLength;
            if (length == 0) IsCompressed = 0;
            (byte type, int tdataSize) = SwitchDataType();
            ArrayType = type; ushort size = (ushort)tdataSize; DataSize = size;
            var arrayByteSize = tdataSize * ArrayLength;
            Span<byte> r = new byte[arrayByteSize + Constant.ULONGLENGTH];
            ToSpan(r, out int used, endian);
            r = r.Slice(0, used);
            return r;
        }

        /// <summary>
        /// 写入数据
        /// </summary>
        /// <param name="data"></param>
        /// <param name="used"></param>
        /// <param name="endian"></param>
        public void ToSpan(Span<byte> data, out int used, byte endian = 0)
        {
            if (Arrays == null) Arrays = Array.Empty<TData>();
            ArrayLength = Arrays.Length; int length = ArrayLength;
            if (length == 0) IsCompressed = 0;
            (byte type, int tdataSize) = SwitchDataType();
            ArrayType = type; ushort size = (ushort)tdataSize; DataSize = size;
            var arrayByteSize = tdataSize * ArrayLength;
            if (data.Length < Constant.ULONGLENGTH + arrayByteSize) throw new ArgumentOutOfRangeException("Source data space is not enough");
            int index = 0;
            MemoryMarshal.Write(data.Slice(0, Constant.INTLENGTH), ref length); index += Constant.INTLENGTH;
            if (Arrays.Length <= 1024) IsCompressed = 0;
            data[index++] = ArrayType; data[index++] = IsCompressed;
            MemoryMarshal.Write(data.Slice(index, Constant.USHORTLENGTH), ref size); index += Constant.USHORTLENGTH;
            Span<TData> l = Arrays; Span<byte> convert = MemoryMarshal.AsBytes(l);
            if (IsCompressed == 1)
            {
                using BrotliEncoder encoder = new BrotliEncoder(Constant.BROTLIQUALITY, Constant.BROTLIWINDOW);
                ReadOnlySpan<byte> inD = convert; Span<byte> outD = new byte[arrayByteSize + Constant.ULONGLENGTH];
                OperationStatus status = encoder.Compress(inD, outD, out int u, out int written, true);
                outD = outD.Slice(0, written);
                outD.CopyTo(data.Slice(index, outD.Length));
                index += outD.Length;
                used = index;
            }
            else
            {
                if (arrayByteSize != 0) convert.CopyTo(data.Slice(index, arrayByteSize));
                index += arrayByteSize;
                used = index;
            }
        }

        /// <summary>
        /// 空数据集合
        /// </summary>
        public static DataArray<TData> Empty
        {
            get 
            {
                (byte b, int t) = SwitchDataType();
                return new DataArray<TData>() { ArrayLength = 0, Arrays = new TData[0], ArrayType = b, DataSize = (ushort)t, IsCompressed = 0 };
            }
        }

        /// <summary>
        /// 创建
        /// </summary>
        /// <param name="span"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        public static (DataArray<TData>, int) Create(ReadOnlySpan<byte> span, byte endian = 0)
        {
            if (span.Length < Constant.LONGLENGTH) throw new ArgumentOutOfRangeException("Source span is too small");
            (byte t, int size) = SwitchDataType(); int index = 0;
            var arrayLength = MemoryMarshal.Read<int>(span.Slice(0, Constant.INTLENGTH)); index += Constant.INTLENGTH;
            byte type = span[index++]; if (type != t) throw new InvalidOperationException($"Source span is not a {(StandardArrayType)t} data array");
            byte isCompressed = span[index++];
            ushort dataSize = MemoryMarshal.Read<ushort>(span.Slice(index, Constant.USHORTLENGTH)); index += Constant.USHORTLENGTH;
            if (size != dataSize) throw new InvalidOperationException($"Source span is not a {(StandardArrayType)t} data array");
            DataArray<TData> dataArray = new DataArray<TData>(); 
            dataArray.ArrayLength = arrayLength; dataArray.ArrayType = t; dataArray.IsCompressed = isCompressed; dataArray.DataSize = dataSize;
            var arrayByteSize = size * arrayLength;
            var usedSize = 0;
            if (arrayByteSize == 0)
            {
                dataArray.ArrayLength = 0;
                dataArray.Arrays = new TData[0];
                return (dataArray, Constant.LONGLENGTH);
            }
            if (isCompressed == 1)
            {
                Span<byte> r = new byte[arrayByteSize];
                using BrotliDecoder decoder = new BrotliDecoder();
                span = span[index..];
                var state = decoder.Decompress(span, r, out int used, out int wirtten);
                if(state != OperationStatus.Done)
                {
                    if (state == OperationStatus.DestinationTooSmall) throw new IndexOutOfRangeException("Decompress failed, destination span was too small");
                    else if (state == OperationStatus.InvalidData) throw new InvalidCastException("Decompress failed, invalid data");
                    else throw new IndexOutOfRangeException("Decompress failed, source data losted");
                }
                if (r.Length > wirtten) throw new InvalidCastException("source data lost");
                ReadOnlySpan<byte> read = r;
                ReadOnlySpan<TData> datas = MemoryMarshal.Cast<byte, TData>(read);
                dataArray.Arrays = datas.ToArray();
                usedSize = used;
            }
            else
            {
                span = span.Slice(index, arrayByteSize);
                if (span.Length != arrayByteSize) throw new InvalidCastException("source data lost");
                ReadOnlySpan<TData> datas = MemoryMarshal.Cast<byte, TData>(span);
                dataArray.Arrays = datas.ToArray();
                usedSize = arrayByteSize;
            }
            return (dataArray, usedSize + Constant.LONGLENGTH);
        }

        /// <summary>
        /// 创建
        /// </summary>
        /// <param name="datas"></param>
        /// <returns></returns>
        public static DataArray<TData> Create(TData[] datas)
        {
            (byte t, int size) = SwitchDataType();
            DataArray<TData> dataArray = new DataArray<TData>();
            if(datas == null) datas = Array.Empty<TData>();
            dataArray.ArrayLength = datas.Length;
            dataArray.Arrays = datas;
            dataArray.ArrayType = t;
            dataArray.DataSize = (ushort)size;
            return dataArray;
        }

        /// <summary>
        /// 获取数据类型
        /// </summary>
        /// <returns></returns>
        public static (byte, int) SwitchDataType()
        {
            var type = typeof(TData);
            if (type == typeof(byte)) return ((byte)StandardArrayType.b, Constant.BYTELENGTH);
            else if (type == typeof(ushort)) return ((byte)StandardArrayType.us, Constant.USHORTLENGTH);
            else if (type == typeof(short)) return ((byte)StandardArrayType.s, Constant.USHORTLENGTH);
            else if (type == typeof(int)) return ((byte)StandardArrayType.i, Constant.INTLENGTH);
            else if (type == typeof(uint)) return ((byte)StandardArrayType.ui, Constant.INTLENGTH);
            else if (type == typeof(long)) return ((byte)StandardArrayType.l, Constant.LONGLENGTH);
            else if (type == typeof(ulong)) return ((byte)StandardArrayType.ul, Constant.LONGLENGTH);
            else if (type == typeof(float)) return ((byte)StandardArrayType.f, Constant.FLOATLENGTH);
            else if (type == typeof(double)) return ((byte)StandardArrayType.d, Constant.DOUBLELENGTH);
            else if (type == typeof(Vector2)) return ((byte)StandardArrayType.v2, Constant.VECTOR2LENGTH);
            else if (type == typeof(Vector3)) return ((byte)StandardArrayType.v3, Constant.VECTOR3LENGTH);
            else if (type == typeof(Vector4)) return ((byte)StandardArrayType.v4, Constant.VECTOR4LENGTH);
            else throw new NotSupportedException("Unkown data type");
        }
    }
}
