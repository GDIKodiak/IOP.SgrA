﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA
{
    /// <summary>
    /// 标准节点类型
    /// </summary>
    public enum StandardNodeType : ushort
    {
        /// <summary>
        /// 头
        /// </summary>
        HEADER = 0,
        /// <summary>
        /// 索引
        /// </summary>
        INDEX = 1,
        /// <summary>
        /// 配置
        /// </summary>
        CONFIG = 2,
        /// <summary>
        /// 图片
        /// </summary>
        IMAGE = 3,
        /// <summary>
        /// 网格
        /// </summary>
        MESH = 4,
        /// <summary>
        /// 纹理
        /// </summary>
        TEXTURE = 5,
        /// <summary>
        /// 渲染对象
        /// </summary>
        RENDEROBJECT = 6,
        /// <summary>
        /// 名称映射
        /// </summary>
        NAMESMAPPING = 7
    }

    /// <summary>
    /// 节点标识
    /// </summary>
    public enum NodeSign
    {
        /// <summary>
        /// 普通节点
        /// </summary>
        NODE,
        /// <summary>
        /// 引用节点
        /// </summary>
        REFN,
        /// <summary>
        /// 空节点
        /// </summary>
        NULL
    }
}
