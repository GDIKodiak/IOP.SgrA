﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace IOP.ISEA
{
    /// <summary>
    /// Utf8字符串
    /// </summary>
    public struct Utf8String
    {
        /// <summary>
        /// 长度
        /// </summary>
        public int Length;
        /// <summary>
        /// 数据
        /// </summary>
        public string Data;
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString() => Data ?? string.Empty;
        /// <summary>
        /// 获取占用字节总长度
        /// </summary>
        /// <returns></returns>
        public int GetBytesLength()
        {
            if (string.IsNullOrEmpty(Data)) Data = string.Empty;
            int l = Encoding.UTF8.GetByteCount(Data);
            if (l > int.MaxValue - Constant.INTLENGTH) throw new IndexOutOfRangeException();
            return l + Constant.INTLENGTH;
        }
        /// <summary>
        /// 字符串隐式转换
        /// </summary>
        /// <param name="s"></param>
        public static implicit operator Utf8String(string s)
        {
            (Utf8String r, _) = Create(s);
            return r;
        }
        /// <summary>
        /// 字符串隐式转换
        /// </summary>
        /// <param name="utf8"></param>
        public static implicit operator string(Utf8String utf8) => utf8.Data;
        /// <summary>
        /// 空Utf8字符串
        /// </summary>
        public static Utf8String Empty => new Utf8String() { Data = string.Empty, Length = 0 };
        /// <summary>
        /// 创建Uft8字符串
        /// </summary>
        /// <param name="span"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        public static (Utf8String, int) Create(ReadOnlySpan<byte> span, byte endian = 0)
        {
            if (span.Length < Constant.INTLENGTH) throw new ArgumentOutOfRangeException();
            int l = MemoryMarshal.Read<int>(span.Slice(0, Constant.INTLENGTH));
            if (l <= 0) return (new Utf8String() { Data = string.Empty, Length = 0 }, Constant.INTLENGTH);
            ReadOnlySpan<byte> d = span[Constant.INTLENGTH..];
            if (d.Length < l) throw new ArgumentOutOfRangeException();
            d = d.Slice(0, l);
            string data = Encoding.UTF8.GetString(d);
            return (new Utf8String() { Length = l, Data = data }, l + Constant.INTLENGTH);
        }
        /// <summary>
        /// 创建
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static (Utf8String, int) Create(string s)
        {
            int l = Encoding.UTF8.GetByteCount(s);
            if (l > int.MaxValue) throw new IndexOutOfRangeException();
            return (new Utf8String { Data = s, Length = l }, l);
        }
        /// <summary>
        /// 写入到指定内存中
        /// </summary>
        /// <param name="data"></param>
        /// <param name="endian"></param>
        public void WriteTo(Span<byte> data, byte endian = 0)
        {
            int l = 0;
            if(!string.IsNullOrEmpty(Data))
            {
                Span<byte> s = Encoding.UTF8.GetBytes(Data);
                l = s.Length;
                if (data.Length < Constant.INTLENGTH + l) throw new ArgumentOutOfRangeException();
                MemoryMarshal.Write(data, ref l);
                s.CopyTo(data.Slice(Constant.INTLENGTH, s.Length));
            }
            else
            {
                if (data.Length < Constant.INTLENGTH) throw new ArgumentOutOfRangeException();
                MemoryMarshal.Write(data, ref l);
            }
        }
    }
}
