﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA
{
    /// <summary>
    /// ascii字符串
    /// </summary>
    public struct AsciiString
    {
        /// <summary>
        /// 长度
        /// </summary>
        public byte Length;
        /// <summary>
        /// 数据
        /// </summary>
        public string Data;
        /// <summary>
        /// 获取占用字节总长度
        /// </summary>
        /// <returns></returns>
        public short GetBytesLength()
        {
            if (string.IsNullOrEmpty(Data)) Data = string.Empty;
            int l = Encoding.ASCII.GetByteCount(Data);
            if (l > byte.MaxValue) throw new IndexOutOfRangeException();
            return (short)(l + Constant.BYTELENGTH);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString() => Data ?? string.Empty;
        /// <summary>
        /// 隐式转换
        /// </summary>
        /// <param name="s"></param>
        public static implicit operator AsciiString(string s)
        {
            (AsciiString r, _) = Create(s);
            return r;
        }
        /// <summary>
        /// 隐式转换
        /// </summary>
        /// <param name="ascii"></param>
        public static implicit operator string(AsciiString ascii) => ascii.Data;
        /// <summary>
        /// 空Ascii字符串
        /// </summary>
        public static AsciiString Empty => new AsciiString() { Data = string.Empty, Length = 0 };
        /// <summary>
        /// 创建
        /// </summary>
        /// <param name="span"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        public static (AsciiString, short) Create(ReadOnlySpan<byte> span, byte endian = 0)
        {
            if (span.IsEmpty) throw new ArgumentNullException();
            byte l = span[0];
            if (l <= 0) return (new AsciiString() { Data = string.Empty, Length = 0 }, Constant.BYTELENGTH);
            ReadOnlySpan<byte> d = span[Constant.BYTELENGTH..];
            if (d.Length < l) throw new ArgumentOutOfRangeException();
            d = d.Slice(0, l);
            string data = Encoding.ASCII.GetString(d);
            return (new AsciiString() { Length = l, Data = data }, (short)(l + Constant.BYTELENGTH));
        }
        /// <summary>
        /// 创建
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static (AsciiString, short) Create(string s)
        {
            int l = Encoding.ASCII.GetByteCount(s);
            if (l > byte.MaxValue) throw new IndexOutOfRangeException();
            return (new AsciiString { Data = s, Length = (byte)l }, (short)l);
        }

        /// <summary>
        /// 写入到指定内存中
        /// </summary>
        /// <param name="data"></param>
        /// <param name="endian"></param>
        public void WriteTo(Span<byte> data, byte endian = 0)
        {
            byte l = 0;
            if (!string.IsNullOrEmpty(Data))
            {
                Span<byte> s = Encoding.ASCII.GetBytes(Data);
                l = (byte)s.Length;
                if (data.Length < Constant.BYTELENGTH + l) throw new ArgumentOutOfRangeException();
                data[0] = l;
                s.CopyTo(data.Slice(Constant.BYTELENGTH, s.Length));
            }
            else
            {
                if (data.IsEmpty) throw new ArgumentOutOfRangeException();
                data[0] = l;
            }
        }
    }
}
