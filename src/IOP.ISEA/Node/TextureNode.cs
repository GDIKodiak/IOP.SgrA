﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA
{
    /// <summary>
    /// 纹理节点
    /// </summary>
    public class TextureNode : ComplexNode, IComonentNode
    {
        /// <summary>
        /// 获取节点类型
        /// </summary>
        /// <returns></returns>
        public override ushort GetNodeType() => (ushort)StandardNodeType.TEXTURE;
        /// <summary>
        /// 纹理用途
        /// </summary>
        public TextureUsage Usage { get; set; }
        /// <summary>
        /// 描述符集下标
        /// </summary>
        public uint SetIndex { get; set; } = 0;
        /// <summary>
        /// 绑定位置
        /// </summary>
        public uint Binding { get; set; } = 0;
        /// <summary>
        /// 数组数量
        /// </summary>
        public uint ArrayElement { get; set; } = 0;
        /// <summary>
        /// 描述符数量
        /// </summary>
        public uint DescriptorCount { get; set; } = 1;
        /// <summary>
        /// 纹理配置
        /// </summary>
        public ConfigNode TexConfig { get; set; }
        /// <summary>
        /// 采样器配置
        /// </summary>
        public ConfigNode SamplerConfig { get; set; }
        /// <summary>
        /// 图像
        /// </summary>
        public ImageNode Image { get; set; }
    }

    /// <summary>
    /// 纹理用途
    /// </summary>
    public enum TextureUsage : ushort
    {
        /// <summary>
        /// 合并采样器的纹理
        /// </summary>
        CombinedImageSampler,
        /// <summary>
        /// 可被采样的纹理
        /// </summary>
        SampledImage,
        /// <summary>
        /// 可被存储的纹理
        /// </summary>
        StorageImage,
        /// <summary>
        /// 自适应结构化缓冲
        /// </summary>
        UniformTexelBuffer,
        /// <summary>
        /// 可被存储的结构化缓冲
        /// </summary>
        StorageTexelBuffer,
        /// <summary>
        /// 自适应缓冲
        /// </summary>
        UniformBuffer,
        /// <summary>
        /// 可被存储的缓冲
        /// </summary>
        StorageBuffer
    }
}
