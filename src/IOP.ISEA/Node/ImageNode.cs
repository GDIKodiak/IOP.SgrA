﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA
{
    /// <summary>
    /// 图片节点
    /// </summary>
    public class ImageNode : SimpleNode
    {
        /// <summary>
        /// 获取节点类型
        /// </summary>
        /// <returns></returns>
        public override ushort GetNodeType() => (ushort)StandardNodeType.IMAGE;
        /// <summary>
        /// 扩展名
        /// </summary>
        public AsciiString Extension { get; set; }
        /// <summary>
        /// 图片数据
        /// </summary>
        public DataArray<byte> ImageData { get; set; }
    }
}
