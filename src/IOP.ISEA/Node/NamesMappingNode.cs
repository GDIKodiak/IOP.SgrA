﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IOP.ISEA
{
    /// <summary>
    /// 名称映射节点
    /// </summary>
    public class NamesMappingNode : SimpleNode
    {
        /// <summary>
        /// 获取节点类型
        /// </summary>
        /// <returns></returns>
        public override ushort GetNodeType() => (ushort)StandardNodeType.NAMESMAPPING;
        /// <summary>
        /// 构造函数
        /// </summary>
        public NamesMappingNode()
        {
            Name = nameof(NamesMapping);
        }
        /// <summary>
        /// 名称映射
        /// </summary>
        public List<NamesMapping> NameMappings { get; } = new List<NamesMapping>();
        /// <summary>
        /// 通过名称获取Id
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="NullReferenceException"></exception>
        public int GetIdWithName(string name)
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));
            var r = NameMappings.Where(x => x.NodeName == name).FirstOrDefault();
            if (r.NodeId <= 0) throw new NullReferenceException($"Cannot found node with name {name}");
            else return r.NodeId;
        }
        /// <summary>
        /// 尝试通过名称获取Id
        /// </summary>
        /// <param name="name"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool TryGetIdWithName(string name, out int id)
        {
            id = 0;
            if (string.IsNullOrEmpty(name)) return false;
            var r = NameMappings.Where(x => x.NodeName == name).FirstOrDefault();
            if (r.NodeId <= 0) return false;
            else
            {
                id = r.NodeId;
                return true;
            }
        }
    }

    /// <summary>
    /// 名称映射
    /// </summary>
    public struct NamesMapping
    {
        /// <summary>
        /// 节点名
        /// </summary>
        public Utf8String NodeName;
        /// <summary>
        /// 节点Id
        /// </summary>
        public int NodeId;
    }
}
