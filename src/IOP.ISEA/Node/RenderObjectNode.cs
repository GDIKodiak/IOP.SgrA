﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace IOP.ISEA
{
    /// <summary>
    /// 渲染对象节点
    /// </summary>
    public class RenderObjectNode : ComplexNode
    {
        /// <summary>
        /// 获取节点类型
        /// </summary>
        /// <returns></returns>
        public override ushort GetNodeType() => (ushort)StandardNodeType.RENDEROBJECT;
        /// <summary>
        /// 局部坐标相对位移
        /// </summary>
        public Vector3 Position { get; set; } = Vector3.One;
        /// <summary>
        /// 局部坐标相对旋转
        /// </summary>
        public Quaternion Rotation { get; set; } = Quaternion.Identity;
        /// <summary>
        /// 局部坐标相对缩放
        /// </summary>
        public Vector3 Scale { get; set; } = Vector3.One;
        /// <summary>
        /// 组件数组长度
        /// </summary>
        public int ComponentArrayLength { get => Comonents != null ? Comonents.Count : 0; }
        /// <summary>
        /// 组件数组
        /// </summary>
        public List<Node> Comonents { get; set; }
        /// <summary>
        /// 父渲染组
        /// </summary>
        public RenderObjectNode Parent { get; set; }
        /// <summary>
        /// 子渲染数组长度
        /// </summary>
        public int ChildArrayLength { get => Childrens != null ? Childrens.Count : 0; }
        /// <summary>
        /// 子渲染对象
        /// </summary>
        public List<RenderObjectNode> Childrens { get; set; }
    }
}
