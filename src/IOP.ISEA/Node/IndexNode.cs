﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace IOP.ISEA
{
    /// <summary>
    /// 索引节点
    /// </summary>
    public class IndexNode : SimpleNode
    {
        /// <summary>
        /// 获取节点类型
        /// </summary>
        /// <returns></returns>
        public override ushort GetNodeType() => (ushort)StandardNodeType.INDEX;
        /// <summary>
        /// 节点字典
        /// </summary>
        public readonly Dictionary<uint, NodeIndex> Indexes = new Dictionary<uint, NodeIndex>();
        /// <summary>
        /// 通过Id获取节点索引信息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="nodeIndex"></param>
        /// <returns></returns>
        public bool GetNodeIndexFromId(int id, out NodeIndex nodeIndex)
        {
            if (Indexes.TryGetValue((uint)id, out nodeIndex)) return true;
            else return false;
        }
        /// <summary>
        /// 添加新的节点
        /// </summary>
        /// <param name="header"></param>
        public void NewNodeIndex(NodeHeader header)
        {
            if (header.Id <= 0) throw new InvalidOperationException($"Cannot add node index with id {header.Id}");
            NodeIndex index = new NodeIndex()
            {
                Id = header.Id,
                NodeType = header.NodeType,
            };
            Indexes.Add(index.Id, index);
        }
        /// <summary>
        /// 移除节点索引
        /// </summary>
        /// <param name="id"></param>
        public void RemoveNodeIndex(int id) => Indexes.Remove((uint)id);
    }

    /// <summary>
    /// 节点索引
    /// </summary>
    public struct NodeIndex
    {
        /// <summary>
        /// ID
        /// </summary>
        public uint Id;
        /// <summary>
        /// 偏移
        /// </summary>
        public uint Offset;
        /// <summary>
        /// 长度
        /// </summary>
        public uint Length;
        /// <summary>
        /// 节点类型
        /// </summary>
        public ushort NodeType;
        /// <summary>
        /// 保留
        /// </summary>
        public ushort Reserved;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="index1"></param>
        /// <param name="index2"></param>
        /// <returns></returns>
        public static bool operator == (NodeIndex index1, NodeIndex index2)
        {
            return index1.Id == index2.Id &&
                index1.Offset == index2.Offset &&
                index1.Length == index2.Length &&
                index1.NodeType == index2.NodeType &&
                index1.Reserved == index2.Reserved;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="index1"></param>
        /// <param name="index2"></param>
        /// <returns></returns>
        public static bool operator != (NodeIndex index1, NodeIndex index2)
        {
            return index1.Id != index2.Id ||
                index1.Offset != index2.Offset ||
                index1.Length != index2.Length ||
                index1.NodeType != index2.NodeType ||
                index1.Reserved != index2.Reserved;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            NodeIndex index = (NodeIndex)obj;
            return this == index;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode() => base.GetHashCode();
    }
}
