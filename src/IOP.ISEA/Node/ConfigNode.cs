﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA
{
    /// <summary>
    /// 配置节点
    /// </summary>
    public class ConfigNode : SimpleNode
    {
        /// <summary>
        /// 获取节点类型
        /// </summary>
        /// <returns></returns>
        public override ushort GetNodeType() => (ushort)StandardNodeType.CONFIG;
        /// <summary>
        /// 该配置所属引擎
        /// </summary>
        public AsciiString Engine { get; set; }
        /// <summary>
        /// 该配置的载体类型
        /// </summary>
        public AsciiString Type { get; set; }
        /// <summary>
        /// 配置数据
        /// </summary>
        public Utf8String Config { get; set; }
    }
}
