﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;

namespace IOP.ISEA
{
    /// <summary>
    /// 网格节点
    /// </summary>
    public class MeshNode : SimpleNode, IComonentNode
    {
        /// <summary>
        /// 获取节点类型
        /// </summary>
        /// <returns></returns>
        public override ushort GetNodeType() => (ushort)StandardNodeType.MESH;
        /// <summary>
        /// 网格模式
        /// </summary>
        public MeshMode MeshMode { get; set; } = MeshMode.Vertex;
        /// <summary>
        /// 顶点数量
        /// </summary>
        public int VerticesCount { get; set; }
        /// <summary>
        /// 顶点数据步进
        /// </summary>
        public int VerticesStride { get; set; }
        /// <summary>
        /// LOD级别
        /// </summary>
        public int LODLevel { get; set; } = 0;
        /// <summary>
        /// 顶点组件数
        /// </summary>
        public int VerticesComponentLength { get => VerticesComponents != null ? VerticesComponents.Count : 0; }
        /// <summary>
        /// 组件序列数组长度
        /// </summary>
        public int ComponentSequenceLength { get => VerticesComponentSequence != null ? VerticesComponentSequence.Count : 0; }
        /// <summary>
        /// 间接绘制命令长度
        /// </summary>
        public int IndirectCommandsLength { get => DrawIndirectCommands != null ? DrawIndirectCommands.Count : 0; }
        /// <summary>
        /// 顶点组件序列
        /// </summary>
        public List<int> VerticesComponentSequence { get; set; } = new List<int>();
        /// <summary>
        /// 顶点组件
        /// </summary>
        public List<MeshData> VerticesComponents { get; set; } = new List<MeshData>();
        /// <summary>
        /// 间接绘制命令
        /// </summary>
        public List<MeshData> DrawIndirectCommands { get; set; } = new List<MeshData>();
        /// <summary>
        /// 索引数据
        /// </summary>
        public MeshData Indexes { get; set; } = new MeshData() { DataType = MeshDataType.FLOAT, Purpose = MeshDataPurpose.INDEXES, Stride = 4 };
        /// <summary>
        /// 网格最小顶点向量
        /// </summary>
        public Vector3 MinVector { get; set; } = Vector3.Zero;
        /// <summary>
        /// 网格最大顶点向量
        /// </summary>
        public Vector3 MaxVector { get; set; } = Vector3.Zero;
    }

    /// <summary>
    /// 网格数据
    /// </summary>
    public class MeshData
    {
        /// <summary>
        /// 数据用途
        /// </summary>
        public MeshDataPurpose Purpose { get; set; }
        /// <summary>
        /// 数据类型
        /// </summary>
        public MeshDataType DataType { get; set; }
        /// <summary>
        /// 数据步进
        /// </summary>
        public ushort Stride { get; set; }
        /// <summary>
        /// 是否压缩
        /// </summary>
        public byte IsCompressed { get => Data.IsCompressed; set => Data.IsCompressed = value; }
        /// <summary>
        /// 数据
        /// </summary>
        public DataArray<byte> Data { get; set; } = DataArray<byte>.Empty;
        /// <summary>
        /// 写入数据
        /// </summary>
        /// <param name="endian"></param>
        /// <returns></returns>
        public Span<byte> ToSpan(byte endian = 0)
        {
            var dataLength = Data.ArrayLength + Constant.ULONGLENGTH;
            Span<byte> total = new byte[Constant.USHORTLENGTH * 3 + dataLength];
            int index = 0;
            ushort p = (ushort)Purpose; ushort t = (ushort)DataType; ushort s = Stride;
            MemoryMarshal.Write(total[index..], ref p); index += Constant.USHORTLENGTH;
            MemoryMarshal.Write(total[index..], ref t); index += Constant.USHORTLENGTH;
            MemoryMarshal.Write(total[index..], ref s); index += Constant.USHORTLENGTH;
            Data.ToSpan(total[index..], out int used, endian); index += used;
            total = total.Slice(0, index);
            return total;
        }

        /// <summary>
        /// 创建
        /// </summary>
        /// <param name="source"></param>
        /// <param name="endian"></param>
        /// <returns></returns>
        public static (MeshData, int) Create(ReadOnlySpan<byte> source, byte endian = 0)
        {
            if (source.Length < Constant.LONGLENGTH + Constant.USHORTLENGTH * 3) throw new ArgumentOutOfRangeException("Source span is too small");
            int index = 0;
            MeshData result = new MeshData();
            ushort p = MemoryMarshal.Read<ushort>(source[index..]); index += Constant.USHORTLENGTH;
            ushort t = MemoryMarshal.Read<ushort>(source[index..]); index += Constant.USHORTLENGTH;
            ushort s = MemoryMarshal.Read<ushort>(source[index..]); index += Constant.USHORTLENGTH;
            result.Purpose = (MeshDataPurpose)p; result.DataType = (MeshDataType)t;
            result.Stride = s;
            (DataArray<byte> array, int l) = DataArray<byte>.Create(source[index..], endian);
            int total = l + Constant.USHORTLENGTH * 3;
            result.Data = array;
            return (result, total);
        }
    }

    /// <summary>
    /// 网格数据类型
    /// </summary>
    public enum MeshDataType : ushort
    {
        /// <summary>
        /// 
        /// </summary>
        BYTE = 0,
        /// <summary>
        /// 
        /// </summary>
        SHORT = 1,
        /// <summary>
        /// 
        /// </summary>
        USHORT = 2,
        /// <summary>
        /// 
        /// </summary>
        INT = 3,
        /// <summary>
        /// 
        /// </summary>
        UINT = 4,
        /// <summary>
        /// 
        /// </summary>
        LONG = 5,
        /// <summary>
        /// 
        /// </summary>
        ULONG = 6,
        /// <summary>
        /// 
        /// </summary>
        FLOAT = 7,
        /// <summary>
        /// 
        /// </summary>
        DOUBLE = 8,
        /// <summary>
        /// 
        /// </summary>
        VECTOR2 = 9,
        /// <summary>
        /// 
        /// </summary>
        VECTOR3 = 10,
        /// <summary>
        /// 
        /// </summary>
        VECTOR4 = 11,
        /// <summary>
        /// 
        /// </summary>
        COMPLEX = 12
    }

    /// <summary>
    /// 网格数据用途
    /// </summary>
    public enum MeshDataPurpose : ushort
    {
        /// <summary>
        /// 顶点坐标
        /// </summary>
        POSITION = 0,
        /// <summary>
        /// 纹理坐标
        /// </summary>
        TEXCOORDS = 1,
        /// <summary>
        /// 法向量
        /// </summary>
        NORMAL = 2,
        /// <summary>
        /// 顶点索引
        /// </summary>
        INDEXES = 3,
        /// <summary>
        /// 复合数据
        /// </summary>
        COMPLEX = 4,
        /// <summary>
        /// 间接绘制
        /// </summary>
        DRAWINDIRECT = 5,
        /// <summary>
        /// 间接索引绘制
        /// </summary>
        DRAWINDEXEDINDIRECT = 6
    }
    /// <summary>
    /// 网格渲染模式
    /// </summary>
    public enum MeshMode : ushort
    {
        /// <summary>
        /// 顶点渲染
        /// </summary>
        Vertex,
        /// <summary>
        /// 索引渲染
        /// </summary>
        Indexes
    }
}
