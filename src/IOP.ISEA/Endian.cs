﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA
{
    /// <summary>
    /// 字节序
    /// </summary>
    public enum Endian
    {
        /// <summary>
        /// 小字节序
        /// </summary>
        LittleEndian = 0,
        /// <summary>
        /// 大字节序
        /// </summary>
        BigEndian = 1
    }
}
