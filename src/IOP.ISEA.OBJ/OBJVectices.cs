﻿using System.Collections.Generic;
using System.Numerics;

namespace IOP.ISEA.OBJ
{
    /// <summary>
    /// OBJ文件顶点
    /// </summary>
    public class OBJVectices : OBJDataNode<Vector3>
    {
        /// <summary>
        /// 获取所有顶点
        /// </summary>
        /// <returns></returns>
        public Vector3[] GetAllVectices() => GetAllData();
        /// <summary>
        /// 获取一个顶点
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Vector3 GetVectex(int index) => GetData(index);
        /// <summary>
        /// 创建面
        /// </summary>
        /// <param name="vectors"></param>
        public static OBJVectices CreateVectices(IList<Vector3> vectors) => Create<OBJVectices>(vectors);
    }
}
