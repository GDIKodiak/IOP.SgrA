﻿using IOP.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.OBJ
{
    /// <summary>
    /// OBJ文档
    /// </summary>
    public class OBJDocument : IDisposable
    {
        /// <summary>
        /// 顶点
        /// </summary>
        public OBJVectices Vectices { get; set; }
        /// <summary>
        /// 纹理坐标
        /// </summary>
        public OBJVecticesTex VecticesTex { get; set; }
        /// <summary>
        /// 法线
        /// </summary>
        public OBJNormals Normals { get; set; }
        /// <summary>
        /// 组
        /// </summary>
        public List<OBJGroup> Groups { get; private set; } = new List<OBJGroup>();

        private bool _IsDispose = false;

        /// <summary>
        /// 销毁资源
        /// </summary>
        public void Dispose()
        {
            if (_IsDispose) return;
            _IsDispose = true;
            foreach (var item in Groups) item.Dispose();
            if (Normals != null) Normals.Dispose();
            if (VecticesTex != null) VecticesTex.Dispose();
            if (Vectices != null) Vectices.Dispose();
        }

        /// <summary>
        /// 加载文档
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static async Task<OBJDocument> LoadAsync(string path)
        {
            FileInfo info = new FileInfo(path);
            if (!info.Exists) throw new FileNotFoundException(path);
            if (info.Extension != ".obj") throw new ArgumentException($"Target file {path} is not a obj file");
            using FileStream stream = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.Read);
            OBJDocument document = await OBJCodec.Decode(stream);
            return document;
        }
    }
}
