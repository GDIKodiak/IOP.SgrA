﻿using System;

namespace IOP.ISEA.OBJ
{
    /// <summary>
    /// OBJ组
    /// </summary>
    public class OBJGroup : IDisposable
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; internal set; }
        /// <summary>
        /// 面数组
        /// </summary>
        public OBJFace Face { get; internal set; }

        /// <summary>
        /// 销毁资源
        /// </summary>
        public void Dispose()
        {
            if (Face != null) Face.Dispose();
        }

        /// <summary>
        /// 创建组
        /// </summary>
        /// <param name="name"></param>
        /// <param name="face"></param>
        public static OBJGroup CreateGroup(string name, OBJFace face)
        {
            OBJGroup group = new OBJGroup();
            group.Name = string.IsNullOrEmpty(name) ? Guid.NewGuid().ToString("N") : name;
            group.Face = face ?? throw new ArgumentNullException(nameof(face));
            return group;
        }
        /// <summary>
        /// 创建组
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static OBJGroup CreateGroup(string name)
        {
            OBJGroup group = new OBJGroup();
            group.Name = string.IsNullOrEmpty(name) ? Guid.NewGuid().ToString("N") : name;
            return group;
        }
        /// <summary>
        /// 设置面
        /// </summary>
        /// <param name="group"></param>
        /// <param name="face"></param>
        /// <returns></returns>
        public static OBJGroup SetFaces(OBJGroup group, OBJFace face)
        {
            group.Face = face ?? throw new ArgumentNullException(nameof(face));
            return group;
        }
    }
}
