﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace IOP.ISEA.OBJ
{
    /// <summary>
    /// OBJ法线
    /// </summary>
    public class OBJNormals : OBJDataNode<Vector3>
    {
        /// <summary>
        /// 获取所有法向量
        /// </summary>
        /// <returns></returns>
        public Vector3[] GetAllNormals() => GetAllData();
        /// <summary>
        /// 获取法向量
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Vector3 GetNormal(int index) => GetData(index);
        /// <summary>
        /// 创建面
        /// </summary>
        /// <param name="vectors"></param>
        public static OBJNormals CreateVectices(IList<Vector3> vectors) => Create<OBJNormals>(vectors);
    }
}
