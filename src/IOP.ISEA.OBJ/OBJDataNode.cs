﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace IOP.ISEA.OBJ
{
    /// <summary>
    /// OBJ节点
    /// </summary>
    public abstract class OBJDataNode<TVector> : IDisposable
        where TVector : struct
    {
        /// <summary>
        /// 向量所占字节
        /// </summary>
        protected static readonly int VectorSize = Marshal.SizeOf<TVector>();
        /// <summary>
        /// 数据大小
        /// </summary>
        public int Count { get; protected set; }
        /// <summary>
        /// 数据
        /// </summary>
        protected List<TVector> Data { get; set; } = new List<TVector>();
        /// <summary>
        /// 获取所有数据
        /// </summary>
        /// <returns></returns>
        protected virtual TVector[] GetAllData() => Data.ToArray();
        /// <summary>
        /// 获取单个数据
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        protected virtual TVector GetData(int index)
        {
            if(index < 0) return new TVector();
            else return Data[index];
        }
        /// <summary>
        /// 清理
        /// </summary>
        public virtual void Clear() => Data.Clear();
        /// <summary>
        /// 销毁资源
        /// </summary>
        public void Dispose() => Clear();

        /// <summary>
        /// 创建基础数据节点
        /// </summary>
        /// <param name="vectors"></param>
        /// <returns></returns>
        protected static TNode Create<TNode>(IList<TVector> vectors)
            where TNode : OBJDataNode<TVector>, new()
        {
            if (vectors == null) throw new ArgumentNullException(nameof(vectors));
            TNode node = new TNode();
            int length = vectors.Count;
            node.Count = length;
            node.Data.AddRange(vectors);
            return node;
        }
    }
}
