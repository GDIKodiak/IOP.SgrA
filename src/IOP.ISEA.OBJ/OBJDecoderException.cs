﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.OBJ
{
    /// <summary>
    /// OBJ文件解码错误
    /// </summary>
    public class OBJDecoderException : Exception
    {
        /// <summary>
        /// 行
        /// </summary>
        public int Line { get; private set; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="message"></param>
        /// <param name="line"></param>
        public OBJDecoderException(string message, int line) : base(message) { Line = line; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        /// <param name="line"></param>
        public OBJDecoderException(string message, int line, Exception innerException) : base(message, innerException) { Line = line; }
    }
}
