﻿using IOP.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.ISEA.OBJ
{
    /// <summary>
    /// 面
    /// </summary>
    public class OBJFace : OBJDataNode<IndexVector3>
    {
        /// <summary>
        /// 获取所有面
        /// </summary>
        /// <returns></returns>
        public IndexVector3[] GetAllFaces() => GetAllData();
        /// <summary>
        /// 获取一个面
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public IndexVector3 GetFace(int index) => GetData(index);
        /// <summary>
        /// 创建面
        /// </summary>
        /// <param name="indices"></param>
        public static OBJFace CreateFaces(IList<IndexVector3> indices) => Create<OBJFace>(indices);
    }
}
