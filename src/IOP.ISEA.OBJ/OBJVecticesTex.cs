﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace IOP.ISEA.OBJ
{
    /// <summary>
    /// 纹理坐标
    /// </summary>
    public class OBJVecticesTex : OBJDataNode<Vector3>
    {
        /// <summary>
        /// 获取所有法向量
        /// </summary>
        /// <returns></returns>
        public Vector3[] GetAllVecticesTexs() => GetAllData();
        /// <summary>
        /// 获取法向量
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Vector3 GetVecticesTex(int index) => GetData(index);
        /// <summary>
        /// 创建面
        /// </summary>
        /// <param name="vectors"></param>
        public static OBJVecticesTex CreateVecticesTexs(IList<Vector3> vectors) => Create<OBJVecticesTex>(vectors);
    }
}
