﻿using IOP.Models;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipelines;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.OBJ
{
    /// <summary>
    /// OBJ文件编解码器
    /// </summary>
    public class OBJCodec
    {
        private const int Step = 2048;

        /// <summary>
        /// 解码OBJ文件
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static async Task<OBJDocument> Decode(Stream stream)
        {
            OBJDocument document = new OBJDocument();
            Pipe pipe = new Pipe();
            var write = FillPipeline(stream, pipe.Writer);
            var read = ReadPipeline(pipe.Reader, document);
            await Task.WhenAll(write, read);
            return document;
        }

        /// <summary>
        /// 填充管线
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="writer"></param>
        /// <returns></returns>
        private static async Task FillPipeline(Stream stream, PipeWriter writer)
        {
            try
            {
                while (true)
                {
                    Memory<byte> memory = writer.GetMemory(Step);
                    int bytesRead = await stream.ReadAsync(memory);
                    writer.Advance(bytesRead);
                    if (bytesRead == 0) break;
                    FlushResult result = await writer.FlushAsync();
                    if (result.IsCompleted) break;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                writer.Complete();
            }
        }
        /// <summary>
        /// 读取管线
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        private static async Task ReadPipeline(PipeReader reader, OBJDocument document)
        {
            try
            {
                List<Vector3> v = new List<Vector3>();
                List<Vector3> vt = new List<Vector3>();
                List<Vector3> vn = new List<Vector3>();
                List<IndexVector3> f = new List<IndexVector3>();
                OBJGroup lGroup = null;
                SequencePosition? position = null;
                ReadOnlySequence<byte> local;
                int lineIndex = 0;
                string line;
                string[] split;
                while (true)
                {
                    ReadResult result = await reader.ReadAsync();
                    ReadOnlySequence<byte> buffer = result.Buffer;
                    do
                    {
                        position = buffer.PositionOf((byte)'\n');
                        if (position != null)
                        {
                            lineIndex++;
                            local = buffer.Slice(0, position.Value);
                            line = GetUTF8String(in local);
                            position = buffer.GetPosition(1, position.Value);
                            buffer = buffer.Slice(position.Value);
                            split = line.Split(' ');
                            split = split.Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
                            if (!split.Any())
                                continue;
                            switch (split[0])
                            {
                                case "#":
                                    break;
                                case "v":
                                    ProcessVectorLine(split, v);
                                    break;
                                case "vt":
                                    ProcessVectorLine(split, vt);
                                    break;
                                case "vn":
                                    ProcessVectorLine(split, vn);
                                    break;
                                case "g":
                                    lGroup = ProcessGroup(document, split, lGroup, f, lineIndex);
                                    break;
                                case "f":
                                    ProcessIndexVectorLine(split, f);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    while (!buffer.IsEmpty && position != null);
                    reader.AdvanceTo(buffer.Start);
                    if (result.IsCompleted) break;
                }
                if (v.Count == 0) throw new OBJDecoderException("OBJ file must be have vectices data", 0);
                else document.Vectices = OBJVectices.CreateVectices(v);
                if (vt.Any()) document.VecticesTex = OBJVecticesTex.CreateVecticesTexs(vt);
                if (vn.Any()) document.Normals = OBJNormals.CreateVectices(vn);
                if (f.Count != 0 && lGroup != null) ProcessGroup(document, new string[] { "g" }, lGroup, f, lineIndex);
                v.Clear();
                vt.Clear();
                vn.Clear();
                f.Clear();
            }
            finally
            {
                reader.Complete();
            }
        }
        /// <summary>
        /// 生成UTF8字符串
        /// </summary>
        /// <param name="buffers"></param>
        /// <returns></returns>
        private static string GetUTF8String(in ReadOnlySequence<byte> buffers)
        {
            if (buffers.IsSingleSegment) return Encoding.UTF8.GetString(buffers.First.ToArray());
            else
            {
                return string.Create((int)buffers.Length, buffers, (span, sequence) =>
                {
                    foreach (var segment in sequence)
                    {
                        Encoding.UTF8.GetChars(segment.Span, span);
                        span = span.Slice(segment.Length);
                    }
                });
            }
        }
        /// <summary>
        /// 处理一行
        /// </summary>
        /// <param name="data"></param>
        /// <param name="list"></param>
        private static void ProcessVectorLine(string[] data, List<Vector3> list)
        {
            Vector3 r = new Vector3();
            r.X = float.Parse(data[1]);
            r.Y = float.Parse(data[2]);
            r.Z = data.Length == 4 ? float.Parse(data[3]) : 0;
            list.Add(r);
        }
        /// <summary>
        /// 处理一行
        /// </summary>
        /// <param name="data"></param>
        /// <param name="list"></param>
        private static void ProcessIndexVectorLine(string[] data, List<IndexVector3> list)
        {
            IndexVector3 vector3;
            string[] s;
            for (int i = 1; i < data.Length; i++)
            {
                s = data[i].Split('/');
                if (s.Length == 1)
                {
                    vector3 = new IndexVector3();
                    vector3.X = int.Parse(s[0]);
                    vector3.Y = -1;
                    vector3.Z = -1;
                }
                else if (s.Length == 2)
                {
                    vector3 = new IndexVector3();
                    vector3.X = int.Parse(s[0]);
                    vector3.Y = string.IsNullOrEmpty(s[1]) ? -1 : int.Parse(s[1]);
                }
                else
                {
                    vector3 = new IndexVector3();
                    vector3.X = int.Parse(s[0]);
                    vector3.Y = string.IsNullOrEmpty(s[1]) ? -1 : int.Parse(s[1]);
                    vector3.Z = string.IsNullOrEmpty(s[2]) ? -1 : int.Parse(s[2]);
                }
                list.Add(vector3);
            }
        }
        /// <summary>
        /// 处理组
        /// </summary>
        /// <param name="data"></param>
        /// <param name="group"></param>
        /// <param name="faces"></param>
        /// <param name="document"></param>
        /// <param name="line"></param>
        private static OBJGroup ProcessGroup(OBJDocument document, string[] data, OBJGroup group, List<IndexVector3> faces, int line)
        {
            if (data.Length == 1)
            {
                if (group == null) throw new OBJDecoderException("Invalid group", line);
                OBJFace face = OBJFace.CreateFaces(faces);
                OBJGroup.SetFaces(group, face);
                faces.Clear();
                return group;
            }
            string name = string.Empty;
            if (data.Length < 1 || string.IsNullOrEmpty(data[1]))
            {
                name = Guid.NewGuid().ToString("N");
            }
            else
            {
                name = data[1].TrimEnd('\r').TrimEnd('\n');
                if (name == "(null)" || name == "null" || string.IsNullOrWhiteSpace(name)) name = Guid.NewGuid().ToString("N");
            }
            if (group != null)
            {
                OBJFace face = OBJFace.CreateFaces(faces);
                OBJGroup.SetFaces(group, face);
                faces.Clear();
                group = OBJGroup.CreateGroup(name);
                document.Groups.Add(group);
            }
            else
            {
                group = OBJGroup.CreateGroup(name);
                document.Groups.Add(group);
            }
            return group;
        }
    }
}
