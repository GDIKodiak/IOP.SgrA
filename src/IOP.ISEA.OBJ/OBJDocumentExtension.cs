﻿using IOP.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.OBJ
{
    /// <summary>
    /// OBJ文档扩展
    /// </summary>
    public static class OBJDocumentExtension
    {
        /// <summary>
        /// 合并数据(以顶点坐标，纹理坐标，法向量为顺序)
        /// </summary>
        /// <param name="document"></param>
        /// <param name="ptr"></param>
        /// <param name="totalSize"></param>
        /// <param name="vCount"></param>
        /// <param name="groupStart"></param>
        /// <param name="groupEnd"></param>
        /// <param name="useTex"></param>
        /// <param name="useNormals"></param>
        public static void CombineData(this OBJDocument document, out IntPtr ptr, out int totalSize, out int vCount, int groupStart, int groupEnd, bool useTex = false, bool useNormals = false)
        {
            if (groupStart > groupEnd) throw new IndexOutOfRangeException("groupEnd must be greater than groupStart");
            if (groupEnd >= document.Groups.Count) throw new IndexOutOfRangeException("groupEnd is out of Groups length");
            bool enableTex = false;
            bool enableNormal = false;
            int vs = Marshal.SizeOf<Vector3>();
            int[] counts = document.Groups.Select(x => x.Face.Count).ToArray();
            int total = 0;
            int vcount = 0;
            for (int l = groupStart; l <= groupEnd; l++) vcount += counts[l];
            total += vcount * vs;
            if (useTex && document.VecticesTex != null)
            {
                total += vcount * vs;
                enableTex = true;
            }
            if (useNormals && document.Normals != null)
            {
                total += vcount * vs;
                enableNormal = true;
            }
            totalSize = total;
            vCount = vcount;
            ptr = Marshal.AllocHGlobal(total);
            IntPtr p = ptr; IndexVector3 iv;
            Vector3 v, vt, vn;
            for (int g = groupStart; g <= groupEnd; g++)
            {
                OBJFace face = document.Groups[g].Face;
                if (face == null) continue;
                for (int i = 0; i < face.Count; i++)
                {
                    iv = face.GetFace(i);
                    v = document.Vectices.GetVectex(iv.X - 1);
                    Marshal.StructureToPtr(v, p, true);
                    p = IntPtr.Add(p, vs);
                    if (enableTex)
                    {
                        vt = document.VecticesTex.GetVecticesTex(iv.Y - 1);
                        Marshal.StructureToPtr(vt, p, true);
                        p = IntPtr.Add(p, vs);
                    }
                    if (enableNormal)
                    {
                        vn = document.Normals.GetNormal(iv.Z - 1);
                        Marshal.StructureToPtr(vn, p, true);
                        p = IntPtr.Add(p, vs);
                    }
                }
            }
        }
        /// <summary>
        /// 合并数据（以顶点坐标，纹理坐标，法向量为顺序）
        /// </summary>
        /// <param name="document"></param>
        /// <param name="data"></param>
        /// <param name="vCount"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="groupStart"></param>
        /// <param name="groupEnd"></param>
        /// <param name="useTex"></param>
        /// <param name="useNormals"></param>
        /// <exception cref="IndexOutOfRangeException"></exception>
        public static void CombineData(this OBJDocument document, out Vector3[] data, out int vCount, out Vector3 min, out Vector3 max, int groupStart, int groupEnd, bool useTex = false, bool useNormals = false)
        {
            if (groupStart > groupEnd) throw new IndexOutOfRangeException("groupEnd must be greater than groupStart");
            if (groupEnd >= document.Groups.Count) throw new IndexOutOfRangeException("groupEnd is out of Groups length");
            bool enableTex = false; bool enableNormal = false;
            int[] counts = document.Groups.Select(x => x.Face.Count).ToArray();
            int total = 0; vCount = 0; min = Vector3.Zero; max = Vector3.Zero;
            for (int l = groupStart; l <= groupEnd; l++) vCount += counts[l];
            total += vCount;
            if (useTex && document.VecticesTex != null)
            {
                total += vCount;
                enableTex = true;
            }
            if (useNormals && document.Normals != null)
            {
                total += vCount;
                enableNormal = true;
            }
            data = new Vector3[total];
            IndexVector3 iv;
            Vector3 v, vt, vn;
            int index = 0;
            var vertex = document.Vectices;
            var tex = document.VecticesTex;
            var normal = document.Normals;
            for (int g = groupStart; g <= groupEnd; g++)
            {
                OBJFace face = document.Groups[g].Face;
                if (face == null) continue;
                for (int i = 0; i < face.Count; i++)
                {
                    iv = face.GetFace(i);
                    v = vertex == null ? Vector3.Zero : vertex.GetVectex(iv.X - 1);
                    data[index++] = v;

                    if (v.X < min.X) min.X = v.X; if (v.X > max.X) max.X = v.X;
                    if (v.Y < min.Y) min.Y = v.Y; if (v.Y > max.Y) max.Y = v.Y;
                    if (v.Z < min.Z) min.Z = v.Z; if (v.Z > max.Z) max.Z = v.Z;

                    if (enableTex)
                    {
                        vt = tex == null ? Vector3.Zero : tex.GetVecticesTex(iv.Y - 1);
                        data[index++] = vt;
                    }
                    if (enableNormal)
                    {
                        vn = normal == null ? Vector3.Zero : normal.GetNormal(iv.Z - 1);
                        data[index++] = vn;
                    }
                }
            }
        }
        /// <summary>
        /// 合并数据(以顶点坐标，纹理坐标，法向量为顺序)
        /// </summary>
        /// <param name="document"></param>
        /// <param name="data"></param>
        /// <param name="groupStart"></param>
        /// <param name="groupEnd"></param>
        /// <param name="useTex"></param>
        /// <param name="useNormals"></param>
        public static void CombineData(this OBJDocument document, out Vector3[] data, int groupStart, int groupEnd, bool useTex = false, bool useNormals = false)
        {
            if (groupStart > groupEnd) throw new IndexOutOfRangeException("groupEnd must be greater than groupStart");
            if (groupEnd >= document.Groups.Count) throw new IndexOutOfRangeException("groupEnd is out of Groups length");
            bool enableTex = false;
            bool enableNormal = false;
            int[] counts = document.Groups.Select(x => x.Face.Count).ToArray();
            int total = 0;
            int vcount = 0;
            for (int l = groupStart; l <= groupEnd; l++) vcount += counts[l];
            total += vcount;
            if (useTex && document.VecticesTex != null)
            {
                total += vcount;
                enableTex = true;
            }
            if (useNormals && document.Normals != null)
            {
                total += vcount;
                enableNormal = true;
            }
            data = new Vector3[total];
            IndexVector3 iv;
            Vector3 v, vt, vn;
            int index = 0;
            var vertex = document.Vectices;
            var tex = document.VecticesTex;
            var normal = document.Normals;
            for (int g = groupStart; g <= groupEnd; g++)
            {
                OBJFace face = document.Groups[g].Face;
                if (face == null) continue;
                for (int i = 0; i < face.Count; i++)
                {
                    iv = face.GetFace(i);
                    v = vertex == null ? Vector3.Zero : vertex.GetVectex(iv.X - 1);
                    data[index++] = v;
                    if (enableTex)
                    {
                        vt = tex == null ? Vector3.Zero : tex.GetVecticesTex(iv.Y - 1);
                        data[index++] = vt;
                    }
                    if (enableNormal)
                    {
                        vn = normal == null ? Vector3.Zero : normal.GetNormal(iv.Z - 1);
                        data[index++] = vn;
                    }
                }
            }
        }
        /// <summary>
        /// 获取顶点最大与最小值向量
        /// </summary>
        /// <param name="document"></param>
        /// <param name="groupIndex"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        public static void GetMinAndMaxVector(this OBJDocument document, int groupIndex, out Vector3 min, out Vector3 max)
        {
            if (groupIndex <= 0 || groupIndex >= document.Groups.Count) throw new IndexOutOfRangeException("groupIndex was out of range");
            OBJFace face = document.Groups[groupIndex].Face;
            min = Vector3.Zero; max = Vector3.Zero;
            var vertex = document.Vectices;
            if (face == null) return;
            for (int i = 0; i < face.Count; i++)
            {
                var iv = face.GetFace(i);
                var v = vertex == null ? Vector3.Zero : vertex.GetVectex(iv.X - 1);
                if (v.X < min.X) min.X = v.X; if(v.X > max.X) max.X = v.X;
                if (v.Y < min.Y) min.Y = v.Y; if(v.Y > max.Y) max.Y = v.Y;
                if (v.Z < min.Z) min.Z = v.Z; if(v.Z > max.Z) max.Z = v.Z;
            }
        }
        /// <summary>
        /// 从OBJ文件创建网格节点
        /// </summary>
        /// <param name="document"></param>
        /// <param name="groupStart"></param>
        /// <param name="groupEnd"></param>
        /// <param name="iSEADocument"></param>
        /// <param name="seq">网格编排序列</param>
        /// <returns></returns>
        public static MeshNode[] CreateMeshNodeFromOBJ(this ISEADocument iSEADocument, OBJDocument document, int groupStart, int groupEnd, MeshDataPurpose[] seq)
        {
            if (seq == null) throw new ArgumentNullException(nameof(seq));
            if (groupStart > groupEnd) throw new IndexOutOfRangeException("groupEnd must be greater than groupStart");
            if (groupEnd >= document.Groups.Count) throw new IndexOutOfRangeException("groupEnd is out of Groups length");
            IndexVector3 iv; Vector3 v = Vector3.Zero, vt = Vector3.Zero, vn = Vector3.Zero; ushort stride = 12;
            int index = 0;
            OBJVectices objv = document.Vectices;
            OBJNormals objN = document.Normals;
            OBJVecticesTex objTex = document.VecticesTex;
            List<MeshNode> meshes = new List<MeshNode>();
            for (int g = groupStart; g <= groupEnd; g++)
            {
                OBJGroup group = document.Groups[g];
                OBJFace face = group.Face;
                if (face == null) continue;
                var name = string.IsNullOrEmpty(group.Name) ? Guid.NewGuid().ToString() : group.Name;
                if (name.Contains("null")) name = null;
                Vector3 minV = Vector3.Zero; Vector3 maxV = Vector3.Zero;
                Vector3[] vertices = new Vector3[face.Count];
                Vector3[] normals = objN != null ? new Vector3[face.Count] : null;
                Vector3[] texs = objTex != null ? new Vector3[face.Count] : null;
                for (int i = 0; i < face.Count; i++)
                {
                    iv = face.GetFace(i);
                    v = objv.GetVectex(iv.X - 1);
                    vertices[i] = v;
                    if(v.X < minV.X) minV.X = v.X; if(v.X > maxV.X) maxV.X = v.X;
                    if(v.Y < minV.Y) minV.Y = v.Y; if(v.Y > maxV.Y) maxV.Y = v.Y;
                    if(v.Z < minV.Z) minV.Z = v.Z; if(v.Z > maxV.Z) maxV.Z = v.Z;
                    if (normals != null)
                    {
                        vn = objN.GetNormal(iv.Z - 1);
                        normals[i] = vn;
                    }
                    if (texs != null)
                    {
                        vt = objTex.GetVecticesTex(iv.Y - 1);
                        texs[i] = vt;
                    }
                }
                var mesh = iSEADocument.CreateEmptyMeshNode(name, face.Count);
                mesh.MinVector = minV; mesh.MaxVector = maxV;
                for(int m = 0; m < seq.Length; m++)
                {
                    var item = seq[m];
                    if (item == MeshDataPurpose.POSITION) mesh.AddMeshData(vertices, stride, m, item, true);
                    else if (item == MeshDataPurpose.NORMAL && normals != null) mesh.AddMeshData(normals, stride, m, item, true);
                    else if (item == MeshDataPurpose.TEXCOORDS && texs != null) mesh.AddMeshData(texs, stride, m, item, true);
                }
                meshes.Add(mesh);
                index++;
            }
            return meshes.ToArray();
        }
    }
}
