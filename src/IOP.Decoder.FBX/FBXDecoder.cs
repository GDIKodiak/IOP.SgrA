﻿using System;
using System.Text;
using IOP.Models.FBX;
using System.Buffers;
using System.Runtime.CompilerServices;
using System.IO;
using System.Threading.Tasks;
using System.IO.Pipelines;
using ICSharpCode.SharpZipLib.Zip.Compression;

namespace IOP.Decoder.FBX
{
    /// <summary>
    /// FBX解码器
    /// </summary>
    public static class FBXDecoder
    {

        /// <summary>
        /// 执行解码
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="documentName"></param>
        /// <param name="errorLevel"></param>
        /// <returns></returns>
        public static FBXDocument Decode(Stream stream, string documentName, ErrorLevel errorLevel = ErrorLevel.Checked)
        {
            Pipe pipe = new Pipe();
            var write = FillPipeline(stream, pipe.Writer);
            var read = ReadPipeline(pipe.Reader, errorLevel);
            Task.WaitAll(write, read);
            var document = read.Result;
            document.Name = documentName;
            return document;
        }
        /// <summary>
        /// 执行解码
        /// </summary>
        /// <param name="pipeReader"></param>
        /// <param name="readResult"></param>
        /// <param name="reader"></param>
        /// <param name="errorLevel"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static FBXDocument Decode(PipeReader pipeReader, ReadResult readResult, SequenceReader<byte> reader,
            ErrorLevel errorLevel = ErrorLevel.Checked)
        {
            FBXDocument document = new FBXDocument();
            ulong position = 0;
            document.Header = GetHeaderString(ref reader, ref position, errorLevel);
            reader.TryReadLittleEndian(out int version);
            position += sizeof(int);
            FBXVersion fBXVersion = (FBXVersion)version;
            document.Version = fBXVersion;
            while (!readResult.IsCompleted)
            {
                FBXNode node;
                do
                {
                    node = GetFBXNode(pipeReader, ref reader, ref readResult, ref position, fBXVersion, errorLevel);
                    if (node != null) document.Nodes.Add(node);
                } while (node != null);
                RefreshSequence(pipeReader, ref reader, ref readResult);
            }
            return document;
        }

        /// <summary>
        /// 获取节点
        /// </summary>
        /// <param name="data"></param>
        /// <param name="errorLevel"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static FBXNode GetFBXNode(PipeReader pipeReader, 
            ref SequenceReader<byte> reader, ref ReadResult readResult, ref ulong position,
            FBXVersion version, ErrorLevel errorLevel = ErrorLevel.Checked)
        {
            long endOffset, numProperties, propertyListLen;
            if (version >= FBXVersion.V75)
            {
                endOffset = ReadLong(pipeReader, ref reader, ref readResult, ref position);
                numProperties = ReadLong(pipeReader, ref reader, ref readResult, ref position);
                propertyListLen = ReadLong(pipeReader, ref reader, ref readResult, ref position);
            }
            else
            {
                int endOffset32, numProperties32, propertyListLen32;
                endOffset32 = ReadInt(pipeReader, ref reader, ref readResult, ref position);
                numProperties32 = ReadInt(pipeReader, ref reader, ref readResult, ref position);
                propertyListLen32 = ReadInt(pipeReader, ref reader, ref readResult, ref position);
                endOffset = endOffset32;
                numProperties = numProperties32;
                propertyListLen = propertyListLen32;
            }
            var name = GetNameString(pipeReader, ref reader, ref readResult, ref position);
            if (endOffset <= 0)
            {
                if (errorLevel >= ErrorLevel.Checked && (numProperties != 0 || propertyListLen != 0 || !string.IsNullOrEmpty(name)))
                    throw new FBXDecoderException("Invalid node; expected NULL record");
                return null;
            }

            var node = new FBXNode() { Name = name };

            var propertyEnd = position + (ulong)propertyListLen;
            for (int i = 0; i < numProperties; i++)
                node.Properties.Add(GetFBXProperty(pipeReader, ref reader, ref readResult, ref position));

            var listLne = (ulong)endOffset - position;
            if(listLne > 0)
            {
                FBXNode nested;
                do
                {
                    nested = GetFBXNode(pipeReader, ref reader, ref readResult,
                        ref position, version, errorLevel);
                    node.Nodes.Add(nested);
                } while (nested != null);
            }
            return node;
        }
        /// <summary>
        /// 读取单个属性
        /// </summary>
        /// <param name="pipeReader"></param>
        /// <param name="reader"></param>
        /// <param name="readResult"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static object GetFBXProperty(PipeReader pipeReader, 
            ref SequenceReader<byte> reader, 
            ref ReadResult readResult, ref ulong position)
        {
            byte dataType = ReadByte(pipeReader, ref reader, ref readResult, ref position);
            char type = (char)dataType;
            switch (type)
            {
                case 'Y':
                    var shortValue = ReadShort(pipeReader, ref reader, ref readResult, ref position);
                    return shortValue;
                case 'C':
                    var byteValue = ReadByte(pipeReader, ref reader, ref readResult, ref position);
                    return (char)byteValue;
                case 'I':
                    var intValue = ReadInt(pipeReader, ref reader, ref readResult, ref position);
                    return intValue;
                case 'F':
                    var fintValue = ReadInt(pipeReader, ref reader, ref readResult, ref position);
                    return BitConverter.Int32BitsToSingle(fintValue);
                case 'D':
                    var dlongValue = ReadLong(pipeReader, ref reader, ref readResult, ref position);
                    return BitConverter.Int64BitsToDouble(dlongValue);
                case 'L':
                    var longValue = ReadLong(pipeReader, ref reader, ref readResult, ref position);
                    return longValue;
                case 'f':
                case 'd':
                case 'l':
                case 'i':
                case 'b':
                    return ReadArray(type, pipeReader, ref reader, ref readResult, ref position);
                case 'R':
                    var rlen = ReadInt(pipeReader, ref reader, ref readResult, ref position);
                    position += (ulong)rlen;
                    return GetBytesArray(pipeReader, ref reader, ref readResult, rlen);
                case 'S':
                    return GetPropertyString(pipeReader, ref reader, ref readResult, ref position);
                default:
                    throw new FBXDecoderException("Invalid property data type `" + dataType + "'");
            }
        }
        /// <summary>
        /// 获取数组
        /// </summary>
        /// <param name="pipeReader"></param>
        /// <param name="reader"></param>
        /// <param name="readResult"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static Array ReadArray(char type, PipeReader pipeReader, 
            ref SequenceReader<byte> reader, 
            ref ReadResult readResult, ref ulong position)
        {
            var len = ReadInt(pipeReader, ref reader, ref readResult, ref position);
            var encoding = ReadInt(pipeReader, ref reader, ref readResult, ref position);
            var compressedLen = ReadInt(pipeReader, ref reader, ref readResult, ref position);
            if (encoding != 0)
            {
                position += (ulong)compressedLen;
                return GetDeflateArray(type, pipeReader, ref reader, ref readResult, len, compressedLen);
            }
            else
            {
                return GetNormalArray(type, pipeReader, ref reader, ref readResult, ref position, len);
            }
        }


        /// <summary>
        /// 获取头部字符串
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static string GetHeaderString(ref SequenceReader<byte> reader, ref ulong position, ErrorLevel errorLevel = ErrorLevel.Checked)
        {
            var current = reader.Sequence;
            var str = current.Slice(reader.Position, FBXConstant.HeaderString.Length);
            if (errorLevel >= ErrorLevel.Checked && !CheckEqual(str, new Span<byte>(FBXConstant.HeaderString)))
                throw new FBXDecoderException("Invalid header string");
            reader.Advance(FBXConstant.HeaderString.Length);
            position += (ulong)FBXConstant.HeaderString.Length;
            if (str.IsSingleSegment) return Encoding.UTF8.GetString(str.First.Span);
            else
            {
                return string.Create(FBXConstant.HeaderString.Length, str, (span, sequence) =>
                {
                    foreach (var seq in sequence)
                    {
                        Encoding.UTF8.GetChars(seq.Span, span);
                        span = span.Slice(seq.Length);
                    }
                });
            }
        }
        /// <summary>
        /// 获取字符串
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static string GetNameString(PipeReader pipeReader, ref SequenceReader<byte> reader, 
            ref ReadResult readResult, ref ulong position)
        {
            var rlen = ReadByte(pipeReader, ref reader, ref readResult, ref position);
            position += rlen;
            return GetFBXString(pipeReader, ref reader, ref readResult, rlen);
        }
        /// <summary>
        /// 获取字符串
        /// </summary>
        /// <param name="pipeReader"></param>
        /// <param name="reader"></param>
        /// <param name="readResult"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static string GetPropertyString(PipeReader pipeReader, 
            ref SequenceReader<byte> reader, 
            ref ReadResult readResult, ref ulong position)
        {
            var rlen = ReadInt(pipeReader, ref reader, ref readResult, ref position);
            string result = GetFBXString(pipeReader, ref reader, ref readResult, rlen);
            if (result.Contains(FBXConstant.BinaryNamespaceSeparator))
                result = result.Replace(FBXConstant.BinaryNamespaceSeparator, FBXConstant.ASCIISeparator);
            position += (ulong)rlen;
            return result;
        }
        /// <summary>
        /// 获取FBX字符串
        /// </summary>
        /// <param name="pipeReader"></param>
        /// <param name="reader"></param>
        /// <param name="readResult"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static string GetFBXString(PipeReader pipeReader, 
            ref SequenceReader<byte> reader, 
            ref ReadResult readResult, int length)
        {
            Span<byte> bytes = GetBytesArray(pipeReader, ref reader, ref readResult, length);
            return Encoding.UTF8.GetString(bytes);
        }


        /// <summary>
        /// 检查字节是否相等
        /// </summary>
        /// <param name="data"></param>
        /// <param name="original"></param>
        /// <returns></returns>
        private static bool CheckEqual(Span<byte> data, Span<byte> original)
        {
            for (int i = 0; i < original.Length; i++)
                if (data[i] != original[i]) return false;
            return true;
        }
        /// <summary>
        /// 检查字节是否相等
        /// </summary>
        /// <param name="data"></param>
        /// <param name="original"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static bool CheckEqual(ReadOnlySequence<byte> data, Span<byte> original)
        {
            int index = 0;
            foreach(var item in data)
                foreach (var b in item.Span)
                    if (b != original[index++]) return false;
            return true;
        }


        /// <summary>
        /// 填充管线
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="writer"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static async Task FillPipeline(Stream stream, PipeWriter writer)
        {
            try
            {
                while (true)
                {
                    Memory<byte> memory = writer.GetMemory(2048);
                    int bytesRead = await stream.ReadAsync(memory);
                    writer.Advance(bytesRead);
                    if (bytesRead == 0) break;
                    FlushResult result = await writer.FlushAsync();
                    if (result.IsCompleted) break;
                }
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                writer.Complete();
            }
        }
        /// <summary>
        /// 读取管线数据
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static async Task<FBXDocument> ReadPipeline(PipeReader reader, ErrorLevel errorLevel)
        {
            try
            {
                ReadResult result = await reader.ReadAsync();
                ReadOnlySequence<byte> buffer = result.Buffer;
                var document = Decode(reader, result, new SequenceReader<byte>(buffer), errorLevel);
                return document;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                reader.Complete();
            }
        }


        /// <summary>
        /// 获取新的数据片段
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static async Task<(ReadOnlySequence<byte>, ReadResult)> GetNewSequence(PipeReader reader, ReadOnlySequence<byte> data)
        {
            reader.AdvanceTo(data.Start, data.End);
            ReadResult result = await reader.ReadAsync();
            return (result.Buffer, result);
        }
        /// <summary>
        /// 刷新数据片段
        /// </summary>
        /// <param name="pipeReader"></param>
        /// <param name="reader"></param>
        /// <param name="result"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static void RefreshSequence(PipeReader pipeReader, ref SequenceReader<byte> reader, ref ReadResult result)
        {
            var seqTask = GetNewSequence(pipeReader, reader.Sequence.Slice(reader.Position));
            Task.WaitAll(seqTask);
            ReadOnlySequence<byte> data;
            (data, result) = seqTask.Result;
            reader = new SequenceReader<byte>(data);
        }


        /// <summary>
        /// 获取被压缩的数组
        /// </summary>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static Array GetDeflateArray(char type, 
            PipeReader pipeReader, ref SequenceReader<byte> reader, 
            ref ReadResult readResult,int arrayLength, int compressedLength)
        {
            using MemoryStream stream = new MemoryStream();
            Inflater inflater = new Inflater();
            switch (type)
            {
                case 'f':
                    float[] farrays = new float[arrayLength];
                    LoadDataToDeflateStream(stream, pipeReader, ref reader, ref readResult, compressedLength);
                    inflater.SetInput(stream.ToArray());
                    byte[] flocal = new byte[sizeof(float)];
                    for (int i = 0; i < arrayLength; i++)
                    {
                        inflater.Inflate(flocal);
                        farrays[i] = BitConverter.ToSingle(flocal);
                    }
                    return farrays;
                case 'd':
                    double[] darrays = new double[arrayLength];
                    LoadDataToDeflateStream(stream, pipeReader, ref reader, ref readResult, compressedLength);
                    byte[] dlocal = new byte[sizeof(double)];
                    inflater.SetInput(stream.ToArray());
                    for (int i = 0; i < arrayLength; i++)
                    {
                        inflater.Inflate(dlocal);
                        darrays[i] = BitConverter.ToDouble(dlocal);
                    }
                    return darrays;
                case 'l':
                    long[] larrays = new long[arrayLength];
                    LoadDataToDeflateStream(stream, pipeReader, ref reader, ref readResult, compressedLength);
                    byte[] llocal = new byte[sizeof(long)];
                    inflater.SetInput(stream.ToArray());
                    for (int i = 0; i < arrayLength; i++)
                    {
                        inflater.Inflate(llocal);
                        larrays[i] = BitConverter.ToInt64(llocal);
                    }
                    return larrays;
                case 'i':
                    int[] iarrays = new int[arrayLength];
                    LoadDataToDeflateStream(stream, pipeReader, ref reader, ref readResult, compressedLength);
                    byte[] ilocal = new byte[sizeof(int)];
                    inflater.SetInput(stream.ToArray());
                    for (int i = 0; i < arrayLength; i++)
                    {
                        inflater.Inflate(ilocal);
                        iarrays[i] = BitConverter.ToInt32(ilocal);
                    }
                    return iarrays;
                case 'b':
                    bool[] barrays = new bool[arrayLength];
                    LoadDataToDeflateStream(stream, pipeReader, ref reader, ref readResult, compressedLength);
                    byte[] blocal = new byte[1];
                    inflater.SetInput(stream.ToArray());
                    for (int i = 0; i < arrayLength; i++)
                    {
                        inflater.Inflate(blocal);
                        barrays[i] = BitConverter.ToBoolean(blocal);
                    }
                    return barrays;
                default:
                    throw new FBXDecoderException("Invalid property data type `" + type + "'");
            }
        }
        /// <summary>
        /// 获取正常的数组
        /// </summary>
        /// <param name="type"></param>
        /// <param name="pipeReader"></param>
        /// <param name="reader"></param>
        /// <param name="readResult"></param>
        /// <param name="arrayLength"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static Array GetNormalArray(char type, PipeReader pipeReader, ref SequenceReader<byte> reader,
            ref ReadResult readResult, ref ulong position, int arrayLength)
        {
            switch (type)
            {
                case 'f':
                    float[] farrays = new float[arrayLength];
                    int fvalue;
                    for (int i = 0; i < farrays.Length; i++)
                    {
                        fvalue = ReadInt(pipeReader, ref reader, ref readResult, ref position);
                        farrays[i] = BitConverter.Int32BitsToSingle(fvalue);
                    }
                    return farrays;
                case 'd':
                    double[] darrays = new double[arrayLength];
                    long dvalue;
                    for (int i = 0; i < darrays.Length; i++)
                    {
                        dvalue = ReadLong(pipeReader, ref reader, ref readResult, ref position);
                        darrays[i] = BitConverter.Int64BitsToDouble(dvalue);
                    }
                    return darrays;
                case 'l':
                    long[] larrays = new long[arrayLength];
                    long lvalue;
                    for (int i = 0; i < larrays.Length; i++)
                    {
                        lvalue = ReadLong(pipeReader, ref reader, ref readResult, ref position);
                        larrays[i] = lvalue;
                    }
                    return larrays;
                case 'i':
                    int[] iarrays = new int[arrayLength];
                    int ivalue;
                    for (int i = 0; i < iarrays.Length; i++)
                    {
                        ivalue = ReadInt(pipeReader, ref reader, ref readResult, ref position);
                        iarrays[i] = ivalue;
                    }
                    return iarrays;
                case 'b':
                    bool[] barrays = new bool[arrayLength];
                    byte bvalue;
                    for (int i = 0; i < barrays.Length; i++)
                    {
                        bvalue = ReadByte(pipeReader, ref reader, ref readResult, ref position);
                        barrays[i] = bvalue != 0 ? true : false;
                    }
                    return barrays;
                default:
                    throw new FBXDecoderException("Invalid property data type `" + type + "'");
            }
        }
        /// <summary>
        /// 获取原始二进制数据
        /// </summary>
        /// <param name="pipeReader"></param>
        /// <param name="reader"></param>
        /// <param name="readResult"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static byte[] GetBytesArray(PipeReader pipeReader, ref SequenceReader<byte> reader,
            ref ReadResult readResult, long length)
        {
            Span<byte> bytes = new byte[length];
            var current = reader.Sequence;
            long seqLength = reader.Remaining;
            if (seqLength >= length)
            {
                var str = current.Slice(reader.Position, length);
                str.CopyTo(bytes);
                reader.Advance(length);
            }
            else
            {
                long isWrite = 0;
                while (!readResult.IsCompleted)
                {
                    var diff = length - seqLength - isWrite;
                    var diffSeq = current.Slice(reader.Position);
                    var remained = (int)diffSeq.Length;
                    reader.Advance(remained);
                    diffSeq.CopyTo(bytes.Slice((int)isWrite, remained));
                    isWrite += remained;
                    RefreshSequence(pipeReader, ref reader, ref readResult);
                    if (readResult.IsCanceled)
                        throw new FBXDecoderException("Docode failed,The FBX file is not complete");
                    current = reader.Sequence;
                    seqLength = current.Length;
                    if (seqLength < diff) continue;
                    var str = current.Slice(0, diff);
                    str.CopyTo(bytes.Slice((int)isWrite, (int)diff));
                    reader.Advance(diff);
                    break;
                }
            }
            return bytes.ToArray();
        }


        /// <summary>
        /// 加载数据至解压缩流中
        /// </summary>
        /// <param name="pipeReader"></param>
        /// <param name="reader"></param>
        /// <param name="readResult"></param>
        /// <param name="compressedLength"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static void LoadDataToDeflateStream(MemoryStream stream, PipeReader pipeReader, 
            ref SequenceReader<byte> reader, ref ReadResult readResult, int compressedLength)
        {
            long isWrite = 0;
            while (!readResult.IsCompleted || !reader.End)
            {
                var current = reader.Sequence;
                long seqLength = reader.Remaining;
                long end = seqLength + isWrite;
                if (end <= compressedLength)
                {
                    var str = current.Slice(reader.Position);
                    foreach (var item in str) stream.Write(item.Span);
                    isWrite += seqLength;
                    reader.Advance(seqLength);
                    RefreshSequence(pipeReader, ref reader, ref readResult);
                }
                else
                {
                    long diff = compressedLength - isWrite;
                    var diffSeq = current.Slice(reader.Position, diff);
                    reader.Advance(diff);
                    foreach (var item in diffSeq) stream.Write(item.Span);
                    return;
                }
            }
            throw new FBXDecoderException("Docode failed,The FBX file is not complete");
        }


        /// <summary>
        /// 读取一个字节
        /// </summary>
        /// <param name="pipeReader"></param>
        /// <param name="reader"></param>
        /// <param name="readResult"></param>
        /// <param name="position"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static byte ReadByte(PipeReader pipeReader, ref SequenceReader<byte> reader, ref ReadResult readResult, ref ulong position)
        {
            if (!reader.TryRead(out byte value))
            {
                RefreshSequence(pipeReader, ref reader, ref readResult);
                if (readResult.IsCanceled)
                    throw new FBXDecoderException("Docode failed,The FBX file is not complete");
                reader.TryRead(out value);
            }
            position++;
            return value;
        }
        /// <summary>
        /// 读取16bit数据
        /// </summary>
        /// <param name="pipeReader"></param>
        /// <param name="reader"></param>
        /// <param name="readResult"></param>
        /// <param name="position"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static short ReadShort(PipeReader pipeReader, ref SequenceReader<byte> reader, ref ReadResult readResult, ref ulong position)
        {
            if (!reader.TryReadLittleEndian(out short value))
            {
                RefreshSequence(pipeReader, ref reader, ref readResult);
                if (readResult.IsCompleted && reader.Length < sizeof(short))
                    throw new FBXDecoderException("Docode failed,The FBX file is not complete");
                reader.TryReadLittleEndian(out value);
            }
            position += sizeof(short);
            return value;
        }
        /// <summary>
        /// 读取32bit数据
        /// </summary>
        /// <param name="pipeReader"></param>
        /// <param name="reader"></param>
        /// <param name="readResult"></param>
        /// <param name="position"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static int ReadInt(PipeReader pipeReader, ref SequenceReader<byte> reader, ref ReadResult readResult, ref ulong position)
        {
            if (!reader.TryReadLittleEndian(out int value))
            {
                RefreshSequence(pipeReader, ref reader, ref readResult);
                if (readResult.IsCompleted && reader.Length < sizeof(int))
                    throw new FBXDecoderException("Docode failed,The FBX file is not complete");
                reader.TryReadLittleEndian(out value);
            }
            position += sizeof(int);
            return value;
        }
        /// <summary>
        /// 读取64bit数据
        /// </summary>
        /// <param name="pipeReader"></param>
        /// <param name="reader"></param>
        /// <param name="readResult"></param>
        /// <param name="position"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static long ReadLong(PipeReader pipeReader, ref SequenceReader<byte> reader, ref ReadResult readResult, ref ulong position)
        {
            if (!reader.TryReadLittleEndian(out long value))
            {
                RefreshSequence(pipeReader, ref reader, ref readResult);
                if (readResult.IsCompleted && reader.Length < sizeof(long))
                    throw new FBXDecoderException("Docode failed,The FBX file is not complete");
                reader.TryReadLittleEndian(out value);
            }
            position += sizeof(long);
            return value;
        }
    }
}
