﻿using System;

namespace IOP.Decoder.FBX
{
    /// <summary>
    /// FBX解码器异常
    /// </summary>
    public class FBXDecoderException : Exception
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="message"></param>
        public FBXDecoderException(string message):base(message) { }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public FBXDecoderException(string message, Exception innerException) : base(message, innerException) { }
    }
}
