﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.STL
{
    /// <summary>
    /// STL格式
    /// </summary>
    public enum STLFormat
    {
        /// <summary>
        /// 
        /// </summary>
        Unknown,
        /// <summary>
        /// 二进制
        /// </summary>
        Binary,
        /// <summary>
        /// 文本
        /// </summary>
        Ascii
    }
}
