﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.STL
{
    /// <summary>
    /// 
    /// </summary>
    public static class STLToken
    {
        /// <summary>
        /// 
        /// </summary>
        public const string SOLID = "solid";
        /// <summary>
        /// 
        /// </summary>
        public const string FACET = "facet";
        /// <summary>
        /// 
        /// </summary>
        public const string NORMAL = "normal";
        /// <summary>
        /// 
        /// </summary>
        public const string OUTER = "outer";
        /// <summary>
        /// 
        /// </summary>
        public const string LOOP = "loop";
        /// <summary>
        /// 
        /// </summary>
        public const string ENDLOOP = "endloop";
        /// <summary>
        /// 
        /// </summary>
        public const string ENDFACET = "endfacet";
        /// <summary>
        /// 
        /// </summary>
        public const string ENDSOLID = "endsolid";
        /// <summary>
        /// 
        /// </summary>
        public const string VERTEX = "vertex";
    }
    /// <summary>
    /// 令牌状态
    /// </summary>
    public enum STLTokenState
    {
        /// <summary>
        /// 
        /// </summary>
        SolidStart,
        /// <summary>
        /// 
        /// </summary>
        FacetStart,
        /// <summary>
        /// 
        /// </summary>
        Normal,
        /// <summary>
        /// 
        /// </summary>
        LoopStart,
        /// <summary>
        /// 
        /// </summary>
        Vertex,
        /// <summary>
        /// 
        /// </summary>
        LoopEnd,
        /// <summary>
        /// 
        /// </summary>
        FacetEnd,
        /// <summary>
        /// 
        /// </summary>
        SolidEnd
    }

    /// <summary>
    /// 
    /// </summary>
    public struct STLTokenData
    {
        /// <summary>
        /// 
        /// </summary>
        public STLTokenState State;
        /// <summary>
        /// 
        /// </summary>
        public Vector3 Data;

        public STLTokenData(STLTokenState state, Vector3 data)
        {
            State = state;
            Data = data;
        }
        public STLTokenData(STLTokenState state)
        {
            State = state;
            Data = Vector3.Zero;
        }
    }
}
