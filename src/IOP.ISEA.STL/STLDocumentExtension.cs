﻿using IOP.SgrA.Modeling.BRep;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace IOP.ISEA.STL
{
    /// <summary>
    /// STL文档扩展
    /// </summary>
    public static class STLDocumentExtension
    {
        /// <summary>
        /// 从STL文件创建网格节点
        /// </summary>
        /// <param name="iSEADocument"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public static MeshNode CreateMeshNodeFromSTL(this ISEADocument iSEADocument, STLDocument document)
        {
            var list = document.Facets;
            var name = string.IsNullOrEmpty(document.Name) ? Guid.NewGuid().ToString() : document.Name;
            var mesh = iSEADocument.CreateEmptyMeshNode(name, (int)(document.FacetCount * 3));
            Vector3[] datas = new Vector3[document.Facets.Count * 6]; int index = 0;
            Vector3 minV = Vector3.Zero; Vector3 maxV = Vector3.Zero;
            for (int i = 0; i < list.Count; i++)
            {
                var facet = list[i];
                var n = facet.Normal;
                var v1 = facet.Vertex1;
                var v2 = facet.Vertex2;
                var v3 = facet.Vertex3;
                datas[index++] = facet.Vertex1;
                datas[index++] = n;
                datas[index++] = facet.Vertex2;
                datas[index++] = n;
                datas[index++] = facet.Vertex3;
                datas[index++] = n;
                if (v1.X < minV.X) minV.X = v1.X;
                if (v1.Y < minV.Y) minV.Y = v1.Y;
                if (v1.Z < minV.Z) minV.Z = v1.Z;
                if (v1.X > maxV.X) maxV.X = v1.X;
                if (v1.Y > maxV.Y) maxV.Y = v1.Y;
                if (v1.Z > maxV.Z) maxV.Z = v1.Z;

                if (v2.X < minV.X) minV.X = v2.X;
                if (v2.Y < minV.Y) minV.Y = v2.Y;
                if (v2.Z < minV.Z) minV.Z = v2.Z;
                if (v2.X > maxV.X) maxV.X = v2.X;
                if (v2.Y > maxV.Y) maxV.Y = v2.Y;
                if (v2.Z > maxV.Z) maxV.Z = v2.Z;

                if (v3.X < minV.X) minV.X = v3.X;
                if (v3.Y < minV.Y) minV.Y = v3.Y;
                if (v3.Z < minV.Z) minV.Z = v3.Z;
                if (v3.X > maxV.X) maxV.X = v3.X;
                if (v3.Y > maxV.Y) maxV.Y = v3.Y;
                if (v3.Z > maxV.Z) maxV.Z = v3.Z;
            }
            mesh.AddMeshData(datas, 24, 0, MeshDataPurpose.COMPLEX, true);
            return mesh;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        /// <param name="data"></param>
        /// <param name="useNormals"></param>
        /// <param name="vertexCount"></param>
        public static void CombineData(this STLDocument document, out Vector3[] data, out uint vertexCount, 
            bool useNormals = true)
        {
            var l = document.Facets;
            int c = useNormals ? l.Count * 6 : l.Count * 3;
            vertexCount = (uint)(l.Count * 3);
            data = new Vector3[c]; int index = 0;
            for(int i = 0; i < l.Count; i++)
            {
                var f = l[i];
                data[index++] = f.Vertex1;
                if (useNormals) data[index++] = f.Normal;
                data[index++] = f.Vertex2;
                if (useNormals) data[index++] = f.Normal;
                data[index++] = f.Vertex3;
                if (useNormals) data[index++] = f.Normal;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        /// <param name="data"></param>
        /// <param name="vertexCount"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="useNormals"></param>
        public static void CombineData(this STLDocument document, out Vector3[] data, out uint vertexCount,
            out Vector3 min, out Vector3 max,bool useNormals = true)
        {
            min = Vector3.Zero; max = Vector3.Zero;
            var l = document.Facets;
            int c = useNormals ? l.Count * 6 : l.Count * 3;
            vertexCount = (uint)(l.Count * 3);
            data = new Vector3[c]; int index = 0;
            for (int i = 0; i < l.Count; i++)
            {
                var f = l[i];
                var v1 = f.Vertex1; var v2 = f.Vertex2;
                var v3 = f.Vertex3;
                var s1 = v1 - v2; var s2 = v1 - v3;
                var n = Vector3.Normalize(Vector3.Cross(s1, s2));
                data[index++] = f.Vertex1;
                if (useNormals) data[index++] = n;
                data[index++] = f.Vertex2;
                if (useNormals) data[index++] = n;
                data[index++] = f.Vertex3;
                if (useNormals) data[index++] = n;

                if (v1.X < min.X) min.X = v1.X;
                if (v1.Y < min.Y) min.Y = v1.Y;
                if (v1.Z < min.Z) min.Z = v1.Z;
                if (v1.X > max.X) max.X = v1.X;
                if (v1.Y > max.Y) max.Y = v1.Y;
                if (v1.Z > max.Z) max.Z = v1.Z;

                if (v2.X < min.X) min.X = v2.X;
                if (v2.Y < min.Y) min.Y = v2.Y;
                if (v2.Z < min.Z) min.Z = v2.Z;
                if (v2.X > max.X) max.X = v2.X;
                if (v2.Y > max.Y) max.Y = v2.Y;
                if (v2.Z > max.Z) max.Z = v2.Z;

                if (v3.X < min.X) min.X = v3.X;
                if (v3.Y < min.Y) min.Y = v3.Y;
                if (v3.Z < min.Z) min.Z = v3.Z;
                if (v3.X > max.X) max.X = v3.X;
                if (v3.Y > max.Y) max.Y = v3.Y;
                if (v3.Z > max.Z) max.Z = v3.Z;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public static void GetMinAndMaxVector(this STLDocument document, out Vector3 min, out Vector3 max)
        {
            min = Vector3.Zero; max = Vector3.Zero;
            var list = document.Facets;
            for (int i = 0; i < list.Count; i++)
            {
                var facet = list[i]; var n = facet.Normal;
                var v1 = facet.Vertex1; var v2 = facet.Vertex2;
                var v3 = facet.Vertex3;

                if (v1.X < min.X) min.X = v1.X;
                if (v1.Y < min.Y) min.Y = v1.Y;
                if (v1.Z < min.Z) min.Z = v1.Z;
                if (v1.X > max.X) max.X = v1.X;
                if (v1.Y > max.Y) max.Y = v1.Y;
                if (v1.Z > max.Z) max.Z = v1.Z;

                if (v2.X < min.X) min.X = v2.X;
                if (v2.Y < min.Y) min.Y = v2.Y;
                if (v2.Z < min.Z) min.Z = v2.Z;
                if (v2.X > max.X) max.X = v2.X;
                if (v2.Y > max.Y) max.Y = v2.Y;
                if (v2.Z > max.Z) max.Z = v2.Z;

                if (v3.X < min.X) min.X = v3.X;
                if (v3.Y < min.Y) min.Y = v3.Y;
                if (v3.Z < min.Z) min.Z = v3.Z;
                if (v3.X > max.X) max.X = v3.X;
                if (v3.Y > max.Y) max.Y = v3.Y;
                if (v3.Z > max.Z) max.Z = v3.Z;
            }
        }

        #region AdjTriangle
        /// <summary>
        /// 创建邻接三角形序列
        /// </summary>
        /// <param name="document"></param>
        /// <param name="useNormals"></param>
        /// <returns></returns>
        public static IndexesData CreateAdjTriangleSequence(this STLDocument document, bool useNormals = true)
        {
            Dictionary<int, List<BVertex>> books = new Dictionary<int, List<BVertex>>();
            Dictionary<int, List<BEdge>> edgeDic = new Dictionary<int, List<BEdge>>();
            List<BVertex> vertices = new List<BVertex>();
            List<BEdge> edges = new List<BEdge>();
            List<BTriangle> triangles = new List<BTriangle>();
            Vector3 min = Vector3.Zero; Vector3 max = Vector3.Zero;
            var stlFaces = document.Facets;
            int vIndex = 0; int eIndex = 0; int tIndex = 0;
            foreach(var face in stlFaces)
            {
                BVertex bv1, bv2, bv3;
                Vector3 v1 = face.Vertex1;
                Vector3 v2 = face.Vertex2;
                Vector3 v3 = face.Vertex3;
                CalculateMinMax(v1, v2, v3, ref min, ref max);
                bv1 = AddOrGetBVertex(books, vertices, v1, face.Normal, ref vIndex);
                bv2 = AddOrGetBVertex(books, vertices, v2, face.Normal, ref vIndex);
                bv3 = AddOrGetBVertex(books, vertices, v3, face.Normal, ref vIndex);
                BEdge edge1 = AddOrGetBEdge(edgeDic, edges, bv1, bv2, ref eIndex);
                BEdge edge2 = AddOrGetBEdge(edgeDic, edges, bv2, bv3, ref eIndex);
                BEdge edge3 = AddOrGetBEdge(edgeDic, edges, bv3, bv1, ref eIndex);
                NewTriangle(triangles, bv1, bv2, bv3, face.Normal, edge1, edge2, edge3, ref tIndex);
            }
            List<uint> indexes = new List<uint>();
            List<Vector3> outputs = new List<Vector3>();
            foreach(var t in triangles)
            {
                BEdge edge1 = edges[t.Edge1];
                if(edge1.FaceLeft == t.Index)
                {
                    if(edge1.FaceRight < 0)
                    {
                        indexes.Add((uint)edge1.Vertex1);
                        indexes.Add((uint)edge1.Vertex2);
                        indexes.Add((uint)edge1.Vertex2);
                    }
                    else
                    {
                        BTriangle right = triangles[edge1.FaceRight];
                        indexes.Add((uint)edge1.Vertex1);
                        int v = right.GetAdjVertexFromEdge(edge1);
                        if (v < 0)
                            throw new Exception();
                        indexes.Add((uint)v);
                        indexes.Add((uint)edge1.Vertex2);
                    }
                }
                else
                {
                    BTriangle left = triangles[edge1.FaceLeft];
                    indexes.Add((uint)edge1.Vertex2); //反向
                    int v = left.GetAdjVertexFromEdge(edge1);
                    if (v < 0)throw new Exception();
                    indexes.Add((uint)v);
                    indexes.Add((uint)edge1.Vertex1);
                }
                BEdge edge2 = edges[t.Edge2];
                if (edge2.FaceLeft == t.Index)
                {
                    if (edge2.FaceRight < 0)
                    {
                        indexes.Add((uint)edge2.Vertex2);
                        indexes.Add((uint)edge2.Vertex2);
                    }
                    else
                    {
                        BTriangle right = triangles[edge2.FaceRight];
                        int v = right.GetAdjVertexFromEdge(edge2);
                        if (v < 0) throw new Exception();
                        indexes.Add((uint)v);
                        indexes.Add((uint)edge2.Vertex2);
                    }
                }
                else
                {
                    BTriangle left = triangles[edge2.FaceLeft];
                    int v = left.GetAdjVertexFromEdge(edge2);
                    if (v < 0)throw new Exception();
                    indexes.Add((uint)v);
                    indexes.Add((uint)edge2.Vertex1); //反向
                }
                BEdge edge3 = edges[t.Edge3];
                if(edge3.FaceLeft == t.Index)
                {
                    if (edge3.FaceRight < 0)
                    {
                        indexes.Add((uint)edge3.Vertex2);
                    }
                    else
                    {
                        BTriangle right = triangles[edge3.FaceRight];
                        int v = right.GetAdjVertexFromEdge(edge3);
                        if (v < 0) throw new Exception();
                        indexes.Add((uint)v);
                    }
                }
                else
                {
                    BTriangle left = triangles[edge3.FaceLeft];
                    int v = left.GetAdjVertexFromEdge(edge3);
                    if (v < 0) throw new Exception();
                    indexes.Add((uint)v);
                }
            }
            InsertVertices(outputs, vertices);
            IndexesData data = new IndexesData(indexes.ToArray(), outputs.ToArray(), (uint)vertices.Count, max, min);
            return data;
        }

        private static BVertex AddOrGetBVertex(Dictionary<int, List<BVertex>> dic, List<BVertex> orderList, Vector3 v, Vector3 n, 
            ref int index)
        {
            BVertex bv = new BVertex(index, v, n, Vector3.Zero);
            float code = v.Length();
            if (code < 1) code *= 1000;
            int l = (int)code;
            if (dic.TryGetValue(l, out List<BVertex> list))
            {
                bool found = false;
                foreach (var local in list)
                {
                    if (bv.IsSame(local))
                    {
                        found = true;
                        bv = local;
                        break;
                    }
                }
                if (!found)
                {
                    list.Add(bv);
                    index++;
                    orderList.Add(bv);
                }
            }
            else
            {
                list = new List<BVertex>() { bv };
                dic.Add(l, list);
                index++;
                orderList.Add(bv);
            }
            return bv;
        }
        private static BEdge AddOrGetBEdge(Dictionary<int, List<BEdge>> edgeDic, List<BEdge> edges, BVertex v1, BVertex v2, ref int index)
        {
            BEdge edge = new BEdge(index, v1.Index, v2.Index);
            int code = Math.Min(v1.Index, v2.Index);
            if (edgeDic.TryGetValue(code, out List<BEdge> list)) 
            {
                bool found = false;
                foreach (var local in list) 
                {
                    if (local.IsSame(edge))
                    {
                        found = true;
                        edge = local; 
                        break;
                    }
                }
                if (!found)
                {
                    list.Add(edge);
                    index++;
                    edges.Add(edge);
                }
            }
            else
            {
                list = new List<BEdge>() { edge };
                edgeDic.Add(code, list);
                index++;
                edges.Add(edge);
            }
            return edge;
        }
        /// <summary>
        /// 判断法向量b是在法向量a的左边还是右边
        /// </summary>
        /// <param name="normala"></param>
        /// <param name="normalb"></param>
        /// <param name="normalc">翼边的法向量</param>
        /// <returns></returns>
        private static bool IsLeft(Vector3 normala, Vector3 normalb, Vector3 normalc) 
        { 
            Vector3 cross = Vector3.Cross(normala, normalb);
            float dot = Vector3.Dot(cross, normalc);
            return dot > 0.0f;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <param name="v3"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        private static void CalculateMinMax(Vector3 v1, Vector3 v2, Vector3 v3, ref Vector3 min, ref Vector3 max) 
        {
            min = Vector3.Min(min, v1);
            min = Vector3.Min(min, v2);
            min = Vector3.Min(min, v3);
            max = Vector3.Max(max, v1);
            max = Vector3.Max(max, v2);
            max = Vector3.Max(max, v3);
        }
        private static BTriangle NewTriangle(List<BTriangle> triangles, BVertex v1, BVertex v2, BVertex v3, Vector3 normal,
            BEdge edge1, BEdge edge2, BEdge edge3, ref int index)
        {
            BTriangle triangle = new BTriangle(index, edge1.Index, edge2.Index, edge3.Index,
                v1.Index, v2.Index, v3.Index, normal);
            if(edge1.FaceLeft < 0)
            {
                edge1.FaceLeft = triangle.Index;
                edge1.LeftFacePredEdge = edge3.Index;
                edge1.LeftFaceSuccEdge = edge2.Index;
            }
            else
            {
                edge1.FaceRight = triangle.Index;
                edge1.RightFacePredEdge = edge3.Index;
                edge1.RightFaceSuccEdge = edge2.Index;
            }
            if(edge2.FaceLeft < 0)
            {
                edge2.FaceLeft = triangle.Index;
                edge2.LeftFacePredEdge = edge1.Index;
                edge2.LeftFaceSuccEdge = edge3.Index;
            }
            else
            {
                edge2.FaceRight = triangle.Index;
                edge2.RightFacePredEdge = edge1.Index;
                edge2.RightFaceSuccEdge = edge3.Index;
            }
            if(edge3.FaceLeft < 0)
            {
                edge3.FaceLeft = triangle.Index;
                edge3.LeftFacePredEdge = edge2.Index;
                edge3.LeftFaceSuccEdge = edge1.Index;
            }
            else
            {
                edge3.FaceRight = triangle.Index;
                edge3.RightFacePredEdge = edge2.Index;
                edge3.RightFaceSuccEdge = edge1.Index;
            }
            triangles.Add(triangle);
            index++;
            return triangle;
        }
        private static void InsertVertices(List<Vector3> output, List<BVertex> input)
        {
            foreach (var vertex in input) 
            {
                output.Add(vertex.Position);
                output.Add(vertex.Normal);
                output.Add(vertex.UV);
            }
        }
        #endregion
    }
}
