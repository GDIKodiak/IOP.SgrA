﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.STL
{
    /// <summary>
    /// STL文档
    /// </summary>
    public class STLDocument : IDisposable
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; protected internal set; }
        /// <summary>
        /// 面数量
        /// </summary>
        public uint FacetCount { get => (uint)Facets.Count; }
        /// <summary>
        /// 格式
        /// </summary>
        public STLFormat Format { get; protected internal set; }
        /// <summary>
        /// 面列表
        /// </summary>
        public List<STLFacetData> Facets { get; private set; } = new List<STLFacetData>();


        /// <summary>
        /// 加载文档
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static async Task<STLDocument> LoadAsync(string path)
        {
            FileInfo info = new FileInfo(path);
            if (!info.Exists) throw new FileNotFoundException(path);
            if (info.Extension.ToLower() != ".stl") throw new ArgumentException($"Target file {path} is not a STL file");
            using FileStream stream = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.Read);
            STLDocument document = await STLCodec.Decode(stream);
            return document;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
        }
    }
}
