﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.STL
{
    /// <summary>
    /// STL面数据
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 50)]
    public struct STLFacetData
    {
        /// <summary>
        /// 法向量
        /// </summary>
        [FieldOffset(0)]
        public Vector3 Normal;
        /// <summary>
        /// 顶点1
        /// </summary>
        [FieldOffset(12)]
        public Vector3 Vertex1;
        /// <summary>
        /// 顶点2
        /// </summary>
        [FieldOffset(24)]
        public Vector3 Vertex2;
        /// <summary>
        /// 顶点3
        /// </summary>
        [FieldOffset(36)]
        public Vector3 Vertex3;
        [FieldOffset(48)]
        public ushort Padding;
    }
}
