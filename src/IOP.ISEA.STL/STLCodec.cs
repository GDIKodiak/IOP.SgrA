﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Pipelines;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IOP.ISEA.STL
{
    /// <summary>
    /// 
    /// </summary>
    public class STLCodec
    {
        private const int Step = 2048;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static async Task<STLDocument> Decode(Stream stream)
        {
            STLDocument document = new(); Pipe pipe = new();
            var write = FillPipeline(stream, pipe.Writer);
            var read = ReadPipeline(stream, pipe.Reader, document);
            await Task.WhenAll(write, read);
            return document;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="writer"></param>
        /// <returns></returns>
        private static async Task FillPipeline(Stream stream, PipeWriter writer)
        {
            try
            {
                while (true)
                {
                    Memory<byte> memory = writer.GetMemory(Step);
                    int bytesRead = await stream.ReadAsync(memory);
                    writer.Advance(bytesRead);
                    if (bytesRead == 0) break;
                    FlushResult result = await writer.FlushAsync();
                    if (result.IsCompleted) break;
                }
            }
            finally
            {
                writer.Complete();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="reader"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        private static async Task ReadPipeline(Stream stream, PipeReader reader, STLDocument document)
        {
            try
            {
                SequencePosition? position = null;
                long test = (stream.Length - 84) % 50;
                ReadResult result = await reader.ReadAsync();
                ReadOnlySequence<byte> buffer = result.Buffer;
                ReadOnlySequence<byte> cache = buffer;
                Stack<STLTokenData> asciiStack = new(); int line = 0;
                if (test == 0)
                {
                    cache = buffer.Slice(0, 5);
                    if(FindToken(STLToken.SOLID, in cache))
                    {
                        document.Format = STLFormat.Ascii;
                    }
                    else
                    {
                        document.Format = STLFormat.Binary;
                        cache = buffer.Slice(0, 80);
                        position = buffer.PositionOf((byte)'\0');
                        if(position != null)
                        {
                            cache = buffer.Slice(0, position.Value);
                            string name = GetUTF8String(in cache);
                            document.Name = name;
                        }
                        cache = buffer.Slice(80, 4);
                        uint count = MemoryMarshal.Read<uint>(cache.FirstSpan);
                        buffer = buffer.Slice(84);
                    }
                }
                else
                {
                    cache = buffer.Slice(0, 5);
                    if (FindToken(STLToken.SOLID, in cache))
                    {
                        document.Format = STLFormat.Ascii;
                    }
                    else throw new FormatException("Unknown STL format");
                }
                while (true)
                {
                    if (document.Format == STLFormat.Binary)
                    {
                        DecodeBinary(ref buffer, document);
                    }
                    else if (document.Format == STLFormat.Ascii)
                    {
                        DecodeAscii(ref buffer, ref line, document, asciiStack);
                    }
                    else throw new NotSupportedException("Unknown STL fotmat");
                    reader.AdvanceTo(buffer.Start);
                    if (result.IsCompleted) break;
                    result = await reader.ReadAsync();
                    buffer = result.Buffer;
                }
            }
            catch(Exception e)
            {
                Debug.WriteLine(e);
            }
            finally
            {
                reader.Complete();
            }
        }

        /// <summary>
        /// 解码文本
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="document"></param>
        private static void DecodeAscii(ref ReadOnlySequence<byte> buffer, ref int lineCount, STLDocument document, Stack<STLTokenData> stack)
        {
            SequencePosition? position = buffer.PositionOf((byte)'\n');
            while (position.HasValue)
            {
                var local = buffer.Slice(0, position.Value);
                string line = GetUTF8String(local);
                line = line.TrimEnd('\r');
                line = line.TrimStart();
                line = line.TrimEnd();
                string[] s = line.Split(' ');
                if(s.Length > 0)
                {
                    string start = s[0];
                    switch (start)
                    {
                        case STLToken.SOLID:
                            if (stack.Count > 0) throw new InvalidDataException($"Invalid token {STLToken.SOLID} in line {lineCount}");
                            stack.Push(new STLTokenData(STLTokenState.SolidStart));
                            line = line.Remove(0, 6);
                            document.Name = line;
                            break;
                        case STLToken.FACET:
                            float x = 0, y = 0, z = 0;
                            if (s.Length > 1 && s.Length == 5)
                            {
                                string n = s[1];
                                if (n != STLToken.NORMAL) throw new InvalidDataException($"Invalid token {n} in line {lineCount}");
                                x = float.Parse(s[2]); y = float.Parse(s[3]); z = float.Parse(s[4]);
                            }
                            STLTokenData data = new STLTokenData(STLTokenState.FacetStart, new Vector3(x, y, z));
                            stack.Push(data);
                            break;
                        case STLToken.OUTER:
                            if (s.Length != 2) throw new InvalidDataException($"Lost token {STLToken.LOOP}");
                            stack.Push(new STLTokenData(STLTokenState.LoopStart));
                            break;
                        case STLToken.VERTEX:
                            if (s.Length == 4)
                            {
                                x = float.Parse(s[1]); y = float.Parse(s[2]); z = float.Parse(s[3]);
                                STLTokenData vertex = new STLTokenData(STLTokenState.Vertex, new Vector3(x, y, z));
                                stack.Push(vertex);
                            }
                            else throw new InvalidDataException("Lost vertex data");
                            break;
                        case STLToken.ENDLOOP:
                            stack.Push(new STLTokenData(STLTokenState.LoopEnd));
                            break;
                        case STLToken.ENDFACET:
                            STLFacetData facetData = new STLFacetData(); 
                            int i = 0; bool f = false;
                            while(stack.TryPeek(out STLTokenData cache))
                            {
                                if (f)
                                {
                                    document.Facets.Add(facetData);
                                    break;
                                }
                                if (cache.State == STLTokenState.SolidStart)
                                    throw new InvalidDataException($"Incomplete data, lost token {STLToken.FACET}");
                                switch (cache.State)
                                {
                                    case STLTokenState.Vertex:
                                        if (i == 0) facetData.Vertex3 = cache.Data;
                                        else if (i == 1) facetData.Vertex2 = cache.Data;
                                        else facetData.Vertex1 = cache.Data;
                                        i++;
                                        break;
                                    case STLTokenState.FacetStart:
                                        facetData.Normal = cache.Data;
                                        f = true;
                                        break;
                                    default:
                                        break;
                                }
                                stack.Pop();
                            }
                            break;
                        default:
                            break;
                    }
                }
                position = buffer.GetPosition(1, position.Value);
                buffer = buffer.Slice(position.Value);
                lineCount++;
                position = buffer.PositionOf((byte)'\n');
            }
        }
        /// <summary>
        /// 解码二进制
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="document"></param>
        protected static void DecodeBinary(ref ReadOnlySequence<byte> buffer, STLDocument document)
        {
            byte[] c = ArrayPool<byte>.Shared.Rent(50);
            Span<byte> cache = c; cache = cache[..50];
            while(buffer.Length >= 50)
            {
                var local = buffer.Slice(0, 50);
                local.CopyTo(cache);
                STLFacetData data = MemoryMarshal.Read<STLFacetData>(cache);
                document.Facets.Add(data);
                buffer = buffer.Slice(50);
            }
            ArrayPool<byte>.Shared.Return(c);
        }
        /// <summary>
        /// 生成UTF8字符串
        /// </summary>
        /// <param name="buffers"></param>
        /// <returns></returns>
        private static string GetUTF8String(in ReadOnlySequence<byte> buffers)
        {
            if (buffers.IsSingleSegment) return Encoding.UTF8.GetString(buffers.First.ToArray());
            else
            {
                return string.Create((int)buffers.Length, buffers, (span, sequence) =>
                {
                    foreach (var segment in sequence)
                    {
                        Encoding.UTF8.GetChars(segment.Span, span);
                        span = span[segment.Length..];
                    }
                });
            }
        }

        private static bool FindToken(string token, in ReadOnlySequence<byte> buffer)
        {
            ReadOnlySpan<byte> t = Encoding.ASCII.GetBytes(token);
            int i = 0; int end = t.Length;
            foreach(var m in buffer)
            {
                foreach(var b in m.Span)
                {
                    if (i >= end) { return true; }
                    if (b == token[i]) { i++; }
                    else { return false; }
                }
            }
            if (i >= end) return true;
            else return false;
        }
    }
}
