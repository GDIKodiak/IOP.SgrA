﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Plugin
{
    /// <summary>
    /// 插件加载服务
    /// </summary>
    public interface IPluginService
    {
        /// <summary>
        /// 当插件加载失败时
        /// </summary>
        event Action<AggregateException> OnLoadFailed;
        /// <summary>
        /// 设置插件加载文件夹路径
        /// </summary>
        /// <param name="path"></param>
        void SetLoadingDirectoryPath(string path);
#nullable disable
        /// <summary>
        /// 从默认插件放置地址或者特定地址遍历所有插件
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        PluginInfo[] ForeachPlugins(string path = null);
#nullable enable
        /// <summary>
        /// 从指定程序集搜索插件
        /// </summary>
        /// <param name="assembly"></param>
        /// <returns></returns>
        PluginInfo[] ForeachPlugins(Assembly assembly);
        /// <summary>
        /// 枚举插件信息
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        IEnumerable<PluginInfo> EnumerationPluginInfo(PluginState state);
    }
}
