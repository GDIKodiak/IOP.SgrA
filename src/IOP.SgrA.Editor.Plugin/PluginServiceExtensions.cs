﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Plugin
{
    public static class PluginServiceExtensions
    {
        /// <summary>
        /// 添加插件服务
        /// </summary>
        /// <param name="service"></param>
        public static void AddPluginService(this IServiceCollection service)
        {
            var pro = service.BuildServiceProvider();
            PluginService pluginService = new(pro);
            service.AddSingleton<IPluginService>(pluginService);
        }
    }
}
