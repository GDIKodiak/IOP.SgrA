﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Plugin
{
    /// <summary>
    /// 插件状态
    /// </summary>
    public enum PluginState
    {
        /// <summary>
        /// 插件不可用
        /// </summary>
        NotAvailable,
        /// <summary>
        /// 插件可用
        /// </summary>
        Available,
        /// <summary>
        /// 加载成功
        /// </summary>
        LoadedSuccess,
        /// <summary>
        /// 加载失败
        /// </summary>
        LoadedFailed,
        /// <summary>
        /// 初始化成功
        /// </summary>
        InitializeSuccess,
        /// <summary>
        /// 初始化失败
        /// </summary>
        InitializeFailed,
        /// <summary>
        /// 附加至主机成功
        /// </summary>
        AttachSuccess,
        /// <summary>
        /// 附加至主机失败
        /// </summary>
        AttachFailed,
        /// <summary>
        /// 禁用
        /// </summary>
        Disabled
    }
}
