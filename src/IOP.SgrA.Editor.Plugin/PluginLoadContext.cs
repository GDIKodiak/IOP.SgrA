﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Plugin
{
    /// <summary>
    /// 插件加载上下文
    /// </summary>
    public class PluginLoadContext : AssemblyLoadContext
    {
        /// <summary>
        /// 
        /// </summary>
        private AssemblyDependencyResolver _resolver;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pluginPath"></param>
        public PluginLoadContext(string pluginPath)
        {
            if (string.IsNullOrEmpty(pluginPath)) throw new ArgumentNullException(nameof(pluginPath));
            _resolver = new AssemblyDependencyResolver(pluginPath);
        }

        /// <summary>
        /// 加载
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <returns></returns>
        /// <exception cref="DllNotFoundException"></exception>
        protected override Assembly Load(AssemblyName assemblyName)
        {
            string? assemblyPath = _resolver.ResolveAssemblyToPath(assemblyName);
            if (assemblyPath != null)
            {
                return LoadFromAssemblyPath(assemblyPath);
            }
            else
            {
                return Default.LoadFromAssemblyName(assemblyName);
            }
        }
        /// <summary>
        /// 加载非托管库
        /// </summary>
        /// <param name="unmanagedDllName"></param>
        /// <returns></returns>
        protected override IntPtr LoadUnmanagedDll(string unmanagedDllName)
        {
            string? libraryPath = _resolver.ResolveUnmanagedDllToPath(unmanagedDllName);
            if (libraryPath != null)
            {
                return LoadUnmanagedDllFromPath(libraryPath);
            }
            else throw new DllNotFoundException($"Cannot load {unmanagedDllName}, path lost");
        }
    }
}
