﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using IOP.Extension.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Plugin
{
    /// <summary>
    /// 插件加载服务
    /// </summary>
    public class PluginService : IPluginService
    {
        /// <summary>
        /// 默认加载路径
        /// </summary>
        public static string DefaultLoadingPath = Path.Combine(AppContext.BaseDirectory, "Plugins");
        /// <summary>
        /// 默认依赖项存放位置
        /// </summary>
        public static string DefaultDependencyPath = AppContext.BaseDirectory;
        /// <summary>
        /// 加载路径
        /// </summary>
        public string LoadingPath { get; private set; } = DefaultLoadingPath;
        /// <summary>
        /// 依赖加载路径
        /// </summary>
        public string DependencyPath { get; private set; } = DefaultDependencyPath;
#nullable disable
        /// <summary>
        /// 当加载插件失败时
        /// </summary>
        public event Action<AggregateException> OnLoadFailed;
        /// <summary>
        /// 插件Assembly
        /// </summary>
        public List<PluginInfo> PluginAssemblies { get; protected set; } = new List<PluginInfo>();
#nullable enable

        protected ILogger<PluginService> Logger;
        protected IServiceProvider ServiceProvider;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="serviceProvider"></param>
        public PluginService(IServiceProvider services)
        {
            ServiceProvider = services ?? throw new ArgumentNullException(nameof(services));
            var fac = ServiceProvider.GetRequiredService<ILoggerFactory>();
            Logger = fac.CreateLogger<PluginService>();
        }

        /// <summary>
        /// 设置插件加载路径
        /// </summary>
        /// <param name="path"></param>
        public void SetLoadingDirectoryPath(string path)
        {
            if(string.IsNullOrEmpty(path)) return;
            if(Directory.Exists(path))
            {
                LoadingPath = path;
            }
        }

#nullable disable
        /// <summary>
        /// 从默认插件放置地址或者特定地址遍历所有插件
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public PluginInfo[] ForeachPlugins(string path = null)
        {
            List<PluginInfo> plugins = new();
            if (string.IsNullOrEmpty(path)) path = LoadingPath;
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
                return plugins.ToArray();
            }
            else
            {
                DirectoryInfo root = new(path);
                var infos = FoundLoadedDllPath(root);
                foreach (var item in infos)
                {
                    var info = LoadPluginFromAssemblyFile(item, ServiceProvider);
                    if (info != null) plugins.AddRange(info);
                }
                return plugins.ToArray();
            }
        }
#nullable enable
        /// <summary>
        /// 从指定程序集加载搜索插件
        /// </summary>
        /// <param name="assembly"></param>
        /// <returns></returns>
        public PluginInfo[] ForeachPlugins(Assembly assembly)
        {
            List<PluginInfo> plugins = new();
            var infos = LoadPluginFromAssembly(plugins, assembly, ServiceProvider);
            return infos.ToArray();
        }
        /// <summary>
        /// 枚举插件信息
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public IEnumerable<PluginInfo> EnumerationPluginInfo(PluginState state)
        {
            foreach(var item in PluginAssemblies)
            {
                if (item.State == state) yield return item;
            }
        }

        /// <summary>
        /// 查找所有符合要求的Dll路径
        /// </summary>
        /// <param name="directory"></param>
        /// <returns></returns>
        private List<FileInfo> FoundLoadedDllPath(DirectoryInfo root)
        {
            List<FileInfo> plugins = new List<FileInfo>();
            foreach(var child in root.EnumerateDirectories())
            {
                foreach(var item in child.EnumerateFiles())
                {
                    if(item.Extension == ".dll" && item.Name.Contains(".Plugin"))
                    {
                        plugins.Add(item);
                    }
                }
            }
            return plugins;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="serviceProvider"></param>
        /// <returns></returns>
        private List<PluginInfo> LoadPluginFromAssemblyFile(FileInfo file, IServiceProvider serviceProvider)
        {
            List<PluginInfo> plugins = new();
            if (file == null || !file.Exists) return plugins;
            else
            {
                try
                {
                    PluginLoadContext context = new(file.FullName);
                    Assembly pluginAssembly = context.LoadFromAssemblyName(new AssemblyName(Path.GetFileNameWithoutExtension(file.FullName)));
                    plugins = LoadPluginFromAssembly(plugins, pluginAssembly, serviceProvider);
                    return plugins;
                }
                catch (Exception e)
                {
                    Logger.LogError(e, $"Load {file.Name} failed with excetion {e.Message}");
                    return plugins;
                }
            }
        }
        /// <summary>
        /// 加载单个插件
        /// </summary>
        /// <param name="assembly"></param>
        /// <param name="serviceProvider"></param>
        /// <returns></returns>
        private List<PluginInfo> LoadPluginFromAssembly(List<PluginInfo> container, Assembly assembly, IServiceProvider serviceProvider)
        {
            container ??= new List<PluginInfo>();
            try
            {
                if (assembly == null || assembly.IsDynamic) return container;
                var types = assembly.GetTypes();
                foreach (var item in types)
                {
                    if (item.GetInterfaces().Contains(typeof(IEditorPlugin)))
                    {
                        var info = new PluginInfo(assembly.Location, assembly);
                        try
                        {
                            if (serviceProvider.CreateAutowiredInstance(item) is IEditorPlugin plugin)
                            {
                                info.Plugin = plugin;
                                info.State = PluginState.LoadedSuccess;
                            }
                            PluginAssemblies.Add(info);
                            container.Add(info);
                        }
                        catch (Exception e)
                        {
                            Logger.LogError(e, $"create plugin {item.FullName} failed with excetion {e.Message}");
                            info.Exception = e;
                            info.State = PluginState.LoadedFailed;
                        }
                    }
                }
                return container;
            }
            catch (Exception e)
            {
                Logger.LogError(e, $"Load plugins assembly failed with excetion {e.Message}");
                return container;
            }
        }
    }
}
