﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Plugin
{
    /// <summary>
    /// 插件信息
    /// </summary>
    public class PluginInfo
    {
        private IEditorPlugin _Plugin;

        /// <summary>
        /// 插件全名
        /// </summary>
        public string FullName { get; internal set; }
        /// <summary>
        /// 插件地址
        /// </summary>
        public string Path { get; internal set; }
        /// <summary>
        /// 插件程序集
        /// </summary>
        public Assembly Assembly { get; internal set; }
#nullable disable
        /// <summary>
        /// 插件
        /// </summary>
        public IEditorPlugin Plugin 
        {
            get  => _Plugin;
            internal set 
            { 
                if (value != null) 
                { 
                    _Plugin = value; 
                    FullName = value.GetType().FullName;  
                } 
            }
        }
#nullable enable
        /// <summary>
        /// 插件状态
        /// </summary>
        public PluginState State { get; internal set; } = PluginState.NotAvailable;
        /// <summary>
        /// 当插件加载失败时异常信息
        /// </summary>
        public Exception? Exception { get; internal set; }
        /// <summary>
        /// 插件主机启动时是否自动启用
        /// </summary>
        public bool EnableWhenHostStarted { get; set; } = true;
        /// <summary>
        /// 将插件置为失败
        /// </summary>
        /// <param name="reason"></param>
        /// <param name="e"></param>
        public void Failed(PluginState reason, Exception e)
        {
            if (reason == PluginState.Available ||
                reason == PluginState.LoadedSuccess ||
                reason == PluginState.AttachSuccess ||
                reason == PluginState.InitializeSuccess) 
                reason = PluginState.NotAvailable;
            State = reason;
            Exception = e;
        }
        /// <summary>
        /// 变更状态
        /// </summary>
        /// <param name="state"></param>
        public void ChangeState(PluginState state) => State = state;

#nullable disable
        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="assembly"></param>
        /// <param name="plugin"></param>
        public PluginInfo(string path, Assembly assembly, IEditorPlugin plugin)
        {
            Path = path;
            Assembly = assembly;
            Plugin = plugin ?? throw new ArgumentNullException(nameof(plugin));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="assembly"></param>
        public PluginInfo(string path, Assembly assembly)
        {
            Path = path;
            Assembly = assembly;
        }
    }
#nullable enable
}
