﻿using IOP.Halo;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Plugin
{
    /// <summary>
    /// 编辑器插件接口
    /// </summary>
    public interface IEditorPlugin : IDisposable
    {
        /// <summary>
        /// 初始化
        /// </summary>
        /// <returns></returns>
        ValueTask Initialization();
        /// <summary>
        /// 当加载到上下文
        /// </summary>
        /// <returns></returns>
        ValueTask OnAttachedToContext(IFrameworkContainer container);
        /// <summary>
        /// 注册服务
        /// </summary>
        /// <param name="serviceDescriptors"></param>
        void RegistService(IServiceCollection serviceDescriptors);
    }
}
