﻿using System;

namespace IOP.Models.FBX
{
    public class FBXArray<TArray>
    {
        /// <summary>
        /// 编码
        /// </summary>
        public int Encoding { get; set; } = 0;

        /// <summary>
        /// 数据类型
        /// </summary>
        public Type DataType { get; }

        /// <summary>
        /// 数据
        /// </summary>
        public TArray Data { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="source"></param>
        public FBXArray(TArray source)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            DataType = typeof(TArray);
            Data = source;
        }
    }
}
