﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.Models.FBX
{
    /// <summary>
    /// FBX常量
    /// </summary>
    public static class FBXConstant
    {
        /// <summary>
        /// 头部字符串
        /// </summary>
        public static readonly byte[] HeaderString = Encoding.UTF8.GetBytes("Kaydara FBX Binary  \0\x1a\0");
        /// <summary>
        /// 文件尾码与版本的空字节大小
        /// </summary>
        public const int FooterCodeZeroes = 20;
        /// <summary>
        /// 文件
        /// </summary>
        public const int FooterExtensionZeroes = 120;
        /// <summary>
        /// 尾码大小
        /// </summary>
        public const int FooterCodeSize = 16;
        /// <summary>
        /// 命名空间分隔符
        /// </summary>
        public const string BinaryNamespaceSeparator = "\0\x1";
        /// <summary>
        /// 字符串分隔符
        /// </summary>
        public const string ASCIISeparator = "::";
    }
}
