﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.Models.FBX
{
    /// <summary>
    /// FBX对象
    /// </summary>
    public class FBXObject
    {
        /// <summary>
        /// 对象名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 几何信息
        /// </summary>
        public FBXGeometry Geometry { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="document"></param>
        public FBXObject(FBXDocument document)
        {
            LoadFBXDocument(document);
        }

        /// <summary>
        /// 加载FBX文档
        /// </summary>
        /// <param name="document"></param>
        public void LoadFBXDocument(FBXDocument document)
        {
            Name = document.Name;
            foreach(var item in document.Nodes)
            {
                if (item == null) continue;
                switch (item.Name)
                {
                    case "Objects":
                        LoadObjects(item);
                        continue;
                    default: continue;
                }
            }
        }

        /// <summary>
        /// 加载主节点Objects
        /// </summary>
        /// <param name="node"></param>
        private void LoadObjects(FBXNode node)
        {
            foreach(var item in node.Nodes)
            {
                if (item == null) continue;
                switch (item.Name)
                {
                    case "Geometry":
                        LoadGeometry(item);
                        continue;
                    default: continue;
                }
            }
        }

        /// <summary>
        /// 加载几何数据
        /// </summary>
        private void LoadGeometry(FBXNode geometryNode)
        {
            FBXGeometry geometry = new FBXGeometry();
            var vertices = geometryNode["Vertices"];
            if(vertices != null)
            {
                object verticesArray = vertices.Value;
                if(verticesArray != null)
                {
                    switch (verticesArray)
                    {
                        case float[] floats:
                            double[] data = new double[floats.Length];
                            for (int i = 0; i < floats.Length; i++)
                            {
                                data[i] = floats[i];
                            }
                            geometry.Vertices = new FBXArray<double[]>(data);
                            break;
                        case double[] doubles:
                            geometry.Vertices = new FBXArray<double[]>(doubles);
                            break;
                        default: throw new Exception($"Vertices Data Type Error,The Data Tpye is {verticesArray.GetType()}");
                    }
                }
            }
            var polygonVertexIndex = geometryNode["PolygonVertexIndex"];
            if(polygonVertexIndex != null)
            {
                object vertexIndexArray = polygonVertexIndex.Value;
                if(vertexIndexArray != null)
                {
                    List<ulong[]> result = new List<ulong[]>();
                    List<ulong> local = new List<ulong>();
                    switch (vertexIndexArray)
                    {
                        case int[] ints:
                            int localValue;
                            for (int i = 0; i < ints.Length; i++)
                            {
                                localValue = ints[i];
                                if (localValue < 0)
                                {
                                    local.Add((ulong)(-localValue - 1));
                                    result.Add(local.ToArray());
                                    local.Clear();
                                }
                                else local.Add((ulong)localValue);
                            }
                            break;
                        case long[] longs:
                            long llocalValue;
                            for(int i = 0; i < longs.Length; i++)
                            {
                                llocalValue = longs[i];
                                for (int l = 0; l < longs.Length; l++)
                                {
                                    if (llocalValue < 0)
                                    {
                                        local.Add((ulong)(-llocalValue - 1));
                                        result.Add(local.ToArray());
                                        local.Clear();
                                    }
                                    else local.Add((ulong)llocalValue);
                                }
                            }
                            break;
                        default:
                            throw new Exception($"PolygonVertexIndex Data Type Error,The Data Tpye is {vertexIndexArray.GetType()}");
                    }
                    geometry.PolygonVertexIndex = new FBXArray<List<ulong[]>>(result);
                }
            }
            var edge = geometryNode["Edges"];
            if(edge != null)
            {
                object edgeArray = edge.Value;
                if (!(edgeArray is int[])) throw new Exception($"PolygonVertexIndex Data Type Error,The Data Tpye is {edgeArray.GetType()}");
                var vertexIndex = edgeArray as int[];
                geometry.Edges = new FBXArray<int[]>(vertexIndex);
            }
            Geometry = geometry;
        }
    }
}
