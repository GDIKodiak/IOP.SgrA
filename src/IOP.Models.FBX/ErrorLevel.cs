﻿namespace IOP.Models.FBX
{
    /// <summary>
    /// 指示解码器应该在何时抛出错误
    /// </summary>
    public enum ErrorLevel
    {
        /// <summary>
        /// 忽略不一致，除非解码器无法继续执行
        /// </summary>
        Permissive = 0,

        /// <summary>
        /// 检查数据完整性
        /// </summary>
        Checked = 1,

        /// <summary>
        /// 对所有进行检查
        /// </summary>
        Strict = 2,
    }
}
