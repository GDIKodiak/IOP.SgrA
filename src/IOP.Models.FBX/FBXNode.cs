﻿using System.Collections.Generic;
using System.Linq;

namespace IOP.Models.FBX
{
    /// <summary>
    /// FBX节点
    /// </summary>
    public class FBXNode
    {
        /// <summary>
        /// 节点名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 子节点
        /// </summary>
        public List<FBXNode> Nodes { get; } = new List<FBXNode>();

        /// <summary>
        /// 属性列表
        /// </summary>
        public List<object> Properties { get; } = new List<object>();

        /// <summary>
        /// 当前节点值
        /// </summary>
        public object Value
        {
            get => Properties.Count < 1 ? default : Properties[0];
            set
            {
                if (Properties.Count < 1) Properties.Add(value);
                else Properties[0] = value;
            }
        }

        /// <summary>
        /// 节点是否为空
        /// </summary>
        public bool IsEmpty => string.IsNullOrEmpty(Name) && Properties.Count == 0 && Nodes.Count == 0;

        /// <summary>
        /// 索引器
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public FBXNode this[string name] => Nodes.FirstOrDefault(x=>x != null && x.Name == name);
    }
}
