﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.Models.FBX
{
    /// <summary>
    /// FBX物理对象
    /// </summary>
    public class FBXGeometry
    {
        /// <summary>
        /// 顶点数组
        /// </summary>
        public FBXArray<double[]> Vertices { get; set; }

        /// <summary>
        /// 多边形顶点序列
        /// </summary>
        public FBXArray<List<ulong[]>> PolygonVertexIndex { get; set; }

        /// <summary>
        /// 边
        /// </summary>
        public FBXArray<int[]> Edges { get; set; }
    }
}
