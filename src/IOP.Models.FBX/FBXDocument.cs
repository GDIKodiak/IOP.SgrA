﻿using System.Collections.Generic;

namespace IOP.Models.FBX
{
    /// <summary>
    /// FBX根节点
    /// </summary>
    public class FBXDocument
    {
        /// <summary>
        /// FBX版本
        /// </summary>
        public FBXVersion Version { get; set; } = FBXVersion.V74;

        /// <summary>
        /// 文档名字
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 头部字符串
        /// </summary>
        public string Header { get; set; } = "";

        /// <summary>
        /// 节点
        /// </summary>
        public List<FBXNode> Nodes { get; set; } = new List<FBXNode>();
    }
}
