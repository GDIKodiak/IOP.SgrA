﻿namespace IOP.Models.FBX
{
    /// <summary>
    /// FBX版本
    /// </summary>
    public enum FBXVersion
    {
        /// <summary>
        /// 6.0版本
        /// </summary>
        V60 = 6000,
        /// <summary>
        /// 6.1版本
        /// </summary>
        V61 = 6100,
        /// <summary>
        /// 7.0版本
        /// </summary>
        V70 = 7000,
        /// <summary>
        /// FBX 2011版本
        /// </summary>
        V71 = 7100,
        /// <summary>
        /// FBX 2012版本
        /// </summary>
        V72 = 7200,
        /// <summary>
        /// FBX 2013版本
        /// </summary>
        V73 = 7300,
        /// <summary>
        /// FBX 2014版本
        /// </summary>
        V74 = 7400,
        /// <summary>
        /// FBX 2016版本，添加大文件支持，不兼容老版本
        /// </summary>
        V75 = 7500
    }
}
