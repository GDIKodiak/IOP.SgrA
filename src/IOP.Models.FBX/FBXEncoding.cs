﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.Models.FBX
{
    /// <summary>
    /// FBX编码类型
    /// </summary>
    public enum FBXEncoding
    {
        /// <summary>
        /// 二进制
        /// </summary>
        Binary,
        /// <summary>
        /// ascii
        /// </summary>
        ASCII
    }
}
