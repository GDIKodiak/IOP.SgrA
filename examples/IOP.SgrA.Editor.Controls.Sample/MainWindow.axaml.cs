using Avalonia;
using Avalonia.Controls;
using Avalonia.Logging;
using Avalonia.Threading;
using IOP.Halo;
using ReactiveUI;
using System.Diagnostics;
using System.Reactive;
using System.Threading.Tasks;

namespace IOP.SgrA.Editor.Controls.Sample
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var vm = new MainWindowViewModel(this);
            DataContext = vm;
        }
    }

    public class MainWindowViewModel
    {
        private Window Owner;

        public MainWindowViewModel(Window owner)
        {
            Owner = owner;
        }
        public async void Test()
        {
            FileExplorer dialog = new FileExplorer() { Height = 475, Width = 750 };
            dialog.ShowInTaskbar = false;
            await dialog.ShowDialog<bool>(Owner);
        }
    }
}
