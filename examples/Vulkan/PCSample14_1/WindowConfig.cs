﻿using IOP.SgrA;
using IOP.SgrA.Vulkan;
using SharpVk;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace PCSample14_1
{
    public class WindowConfig : IWindowConfig<Window, VulkanManager>
    {
        public Task Config(Window window, VulkanManager graphics)
        {
            var windowOption = window.Option;
            var lDevice = graphics.LogicDevice;
            var basePath = AppContext.BaseDirectory;
            var document = JsonDocument.Parse(File.OpenRead(Path.Combine(basePath, "RenderPass.json")));
            var passJson = document.RootElement;
            lDevice.CreateDefaultSwapChain(window).CreateDefaultSwapChainImageViews();
            var imageAvailableSemaphore = lDevice.CreateSemaphore();
            var renderFinishedSemaphore = lDevice.CreateSemaphore();
            var dImage = graphics.CreateDepthBufferFromJson(lDevice, passJson.GetProperty("DepthBufferCreateOption").GetRawText(), new Extent3D(window.Option.Width, window.Option.Height, 1));
            var pass = graphics.CreateSwapchainRenderPassFromJson("Present", lDevice, window, new Format[] { dImage.ImageFormat },
                passJson.GetProperty("PresentPassCreateOption").GetRawText());
            pass.CreateSwapchainFrameBuffer(window, new ImageNode[] { dImage }, windowOption.Height, windowOption.Width)
                .ConfigBeginInfo((option) =>
                {
                    option.RenderArea = new Rect2D { Extent = new Extent2D(windowOption.Width, windowOption.Height), Offset = new Offset2D { X = 0, Y = 0 } };
                    option.ClearValues = new ClearValue[] { (0.0f, 0.0f, 0.0f, 0.0f), new ClearDepthStencilValue { Depth = 1.0f, Stencil = 0 } };
                });
            window.CreateSceneController(lDevice.WorkQueuesNode.WorkQueues[0], imageAvailableSemaphore.Semaphore, renderFinishedSemaphore.Semaphore);
            window.AddWindowModules(typeof(PipelineModule), typeof(AssetsModule));
            return Task.CompletedTask;
        }

        public void Initialization()
        {
        }
    }
}
