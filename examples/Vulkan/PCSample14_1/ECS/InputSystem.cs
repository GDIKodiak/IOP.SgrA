﻿using IOP.Extension.DependencyInjection;
using IOP.SgrA;
using IOP.SgrA.ECS;
using IOP.SgrA.Input;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace PCSample14_1
{
    public class InputSystem : ComponentSystem
    {
        [Autowired]
        private IInputStateController Input { get; set; }

        [Autowired]
        private ICameraManager CameraManager { get; set; }
        private Camera Camera;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="contextManager"></param>
        public InputSystem()
        {
        }

        public override void Initialization()
        {
            CameraManager.TryGetCamera("main", out Camera camera);
            Camera = camera;
            var viewPort = Camera.Viewport;
            Camera.SetScissor(0, 0, viewPort.Width, viewPort.Height);
        }

        public override void Update()
        {

            if (Input.GetMouseButtonState(MouseButton.ButtonLeft).Action == InputAction.Press)
            {
                var delta = Input.GetMousePositionDelta();
                if (delta.X != 0 || delta.Y != 0)
                {
                    Camera.EncircleFromMouse(Vector3.Zero, delta.X, delta.Y, (float)(0.2 * Math.PI / 180.0));
                }
            }
        }
    }
}
