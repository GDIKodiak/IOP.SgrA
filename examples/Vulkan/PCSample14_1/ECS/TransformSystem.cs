﻿using IOP.SgrA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace PCSample14_1
{
    public class TransformSystem : ComponentSystem
    {
        private readonly IContextManager ContextManager;
        public TransformSystem(IContextManager contextManager)
        {
            ContextManager = contextManager;
        }
        public override void Initialization()
        {
            ContextManager.Foreach<TransGroup>((group, index) =>
            {
                Context context = ContextManager.GetContext(group.Entity);
                ref MVPMatrix mvp = ref context.GetMVPMatrix();
                mvp.ModelMatrix = Matrix4x4.CreateTranslation(group.Transform.Position) * Matrix4x4.CreateScale(group.Transform.Scale);
            });
        }

        public override void Update()
        {

        }
    }
}
