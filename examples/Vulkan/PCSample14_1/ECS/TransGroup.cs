﻿using IOP.SgrA.ECS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCSample14_1
{
    public struct TransGroup
    {
        public Entity Entity;
        public TransformComponent Transform;
    }
}
