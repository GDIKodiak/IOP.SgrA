﻿using IOP.SgrA;
using IOP.SgrA.Vulkan;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SharpVk;
using SharpVk.Khronos;
using SharpVk.Multivendor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCSample14_1
{
    public class Startup : ISgrAStartup
    {
        public void Configure(IGraphicsBuilder server, IHostEnvironment env)
        {
            server.BuildVulkan((manager) =>
            {
                manager.CreateVkInstanceFromConfiguration();//.CreateDebugReportCallback(DebugReport);
                manager.Logger.LogInformation("创建Vulkan实例成功");
                manager.Logger.LogInformation($"Vulkan硬件设备数量为{manager.PhysicalDevices.Length}个");

                var pDevice = manager.PhysicalDevices[0];
                var lDevice = manager.CreateVkLogicDevice(pDevice, (device) =>
                {
                    device.Name = "main";
                    device.EnabledExtensionNames = new string[] { KhrExtensions.Swapchain };
                    device.EnabledLayerNames = ArrayProxy<string>.Null;
                }, (phyDevice) =>
                {
                    QueueFamilyProperties[] properties = phyDevice.GetQueueFamilyProperties();
                    manager.Logger.LogInformation($"Vulkan硬件设备支持的队列家族数量为{properties.Length}");
                    var result = new List<QueueFamilyIndices>();
                    QueueFamilyIndices index = new QueueFamilyIndices();
                    for (uint i = 0; i < properties.Length; i++)
                    {
                        if (properties[i].QueueFlags.HasFlag(QueueFlags.Graphics))
                        {
                            index.FamilyType |= QueueType.Graphics;
                            if (manager.GetPhysicalDevicePresentationSupport(manager.VkInstance, phyDevice, i))
                            {
                                index.FamilyType |= QueueType.Present;
                                index.FamilyIndex = i;
                                result.Add(index);
                                index = new QueueFamilyIndices();
                                continue;
                            }
                        }
                    }
                    return result.ToArray();
                });
                lDevice.InitWorkQueue((device, indeics) =>
                {
                    List<WorkQueue> list = new List<WorkQueue>();
                    foreach (var item in indeics)
                    {
                        if (item.FamilyType == (QueueType.Graphics | QueueType.Present))
                        {
                            Queue queue = device.GetQueue(item.FamilyIndex, 0);
                            var type = QueueType.Graphics | QueueType.Present;
                            WorkQueue work = new WorkQueue(type, item.FamilyIndex, 0, queue);
                            list.Add(work);
                            return list;
                        }
                    }
                    return list;
                });
                lDevice.CreateResourcesCommandPool(lDevice.EnableDeviceQueueIndices[0].FamilyIndex);
                manager.CreatePipelineCache(lDevice);

                var windowOption = new WindowOption() { Height = 900, Width = 1440, Title = "PCSample14_1", UpdateRate = 120 };
                var window = manager.CreateWindow<Window>("Main", windowOption).ConfigWindow<Window, WindowConfig>();
                window.RegistResizeCallbackHandle(ResizeHandle);
                manager.RunWindow(window);
            });
        }

        public void ConfigureServices(IServiceCollection services)
        {
        }

        private static Bool32 DebugReport(DebugReportFlags flags, DebugReportObjectType objectType, ulong @object, HostSize location, int messageCode, string layerPrefix, string message, IntPtr userData)
        {
            Console.WriteLine(message);

            return false;
        }

        private void ResizeHandle(VulkanManager manager, VulkanWindow window, uint width, uint height)
        {
            LogicDeviceNode lDevice = manager.LogicDevice;
            lDevice.RecreateSwapChain(window, width, height).
                CreateDefaultSwapChainImageViews();
            window.SwapchainRenderPass.RecreateSwapchainFrameBuffer(window, width, height);
            float aspect = (float)width / height;
            foreach (var item in manager.CameraManager.GetCameras())
            {
                item.SetViewport(0, 0, width, height, 0, 1);
                item.SetProjectMatrix(-aspect, 1.0f, item.Near, item.Far, item.ProjectMode);
            }
        }
    }
}
