﻿using IOP.Decoder.OBJ;
using IOP.Extension.DependencyInjection;
using IOP.Models.OBJ;
using IOP.SgrA;
using IOP.SgrA.Vulkan;
using Microsoft.Extensions.Logging;
using SharpVk;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace PCSample14_1
{
    [ModulePriority(ModulePriority.Assets)]
    public class AssetsModule : VulkanModule, IVulkanWindowModule
    {
        public VulkanWindow Window { get; set; }

        [Autowired]
        private IContextManager ContextManager { get; set; }
        [Autowired]
        private ILogger<AssetsModule> Logger { get; set; }

        protected override async Task Load(VulkanManager manager)
        {
            try
            {
                var basePath = AppContext.BaseDirectory;
                var modelsPath = Path.Combine(basePath, "Assets", "Models");
                var texPath = Path.Combine(basePath, "Assets", "Textures");
                var lDevice = manager.LogicDevice;
                JsonDocument dJson = await JsonDocument.ParseAsync(File.OpenRead(Path.Combine(texPath, "image.json")));
                var assetsJson = dJson.RootElement;
                var imageJson = assetsJson.GetProperty("Image");

                manager.TryGetDescriptorGroup("Main", out VulkanDescriptorGroup dGroup);
                manager.TryGetRenderGroup("MRT", out IRenderGroup mrtGroup);
                manager.TryGetRenderGroup("Present", out IRenderGroup presentGroup);
                manager.TryGetDescriptorPool("Main", out DescriptorPool dPool);
                lDevice.TryGetRenderPass("MRT", out RenderPassNode mrtPass);
                var sampler = manager.CreateSamplerFromJson("Sampler", lDevice, assetsJson.GetProperty("Sampler").GetRawText());
                var images = mrtPass.Attachments;
                VulkanImageTextureData[] attach = new VulkanImageTextureData[images.Length - 1];
                VulkanTexture[] mrtTexs = new VulkanTexture[images.Length - 1];
                for(int i = 0; i < attach.Length; i++)
                {
                    var sets = manager.CreateDescriptorSets(lDevice, dPool, new DescriptorSetLayout[] { dGroup.DescriptorSetLayouts[3] });
                    attach[i] = manager.CreateTextureDataFromImage($"MRTTexData{i}", lDevice, images[i], ImageLayout.General, DescriptorType.CombinedImageSampler);
                    mrtTexs[i] = manager.CreateTexture($"MRTTex{i}", attach[i], sampler).BindDescriptorSet(sets[0]);
                }
                manager.UpdateTextureDescriptorSets(lDevice, mrtTexs);
                float factorTemp = (float)(Window.Option.Width * 0.5 / Window.Option.Height);
                float[] vdataIn = new float[30]
                {
                    -factorTemp,0.5f,0,0,0, factorTemp,-0.5f,0,1,1, factorTemp,0.5f,0,1,0,
                    -factorTemp,0.5f,0,0,0, -factorTemp,-0.5f,0,0,1, factorTemp,-0.5f,0,1,1
                };
                var box = manager.CreateVRO("Box").CreateVecticesBuffer(vdataIn, 6, lDevice, SharingMode.Exclusive);
                RenderObject[] boxRenders = new RenderObject[attach.Length];
                for (int i = 0; i < attach.Length; i++)
                {
                    boxRenders[i] = manager.CreateRenderObject<RenderObject>($"Box{i}", box, mrtTexs[i]);
                }
                
                var texSets = manager.CreateDescriptorSets(lDevice, dPool, new DescriptorSetLayout[] { dGroup.DescriptorSetLayouts[1], dGroup.DescriptorSetLayouts[1], dGroup.DescriptorSetLayouts[1] });
                var carTexData = await manager.CreateEmptyTextureData<BntexVulkanTextureData>("CarData")
                    .LoadTextureData(lDevice, Path.Combine(texPath, "car.bntex"), imageJson.GetRawText());
                var ghxpTexData = await manager.CreateEmptyTextureData<BntexVulkanTextureData>("ChData")
                    .LoadTextureData(lDevice, Path.Combine(texPath, "ghxp.bntex"), imageJson.GetRawText());
                var treeTexData = await manager.CreateEmptyTextureData<BntexVulkanTextureData>("TreeData")
                    .LoadTextureData(lDevice, Path.Combine(texPath, "tree.bntex"), imageJson.GetRawText());
                var carTex = manager.CreateTexture("CarTex", carTexData, sampler).BindDescriptorSet(texSets[0]);
                var ghxpTex = manager.CreateTexture("ChTex", ghxpTexData, sampler).BindDescriptorSet(texSets[1]);
                var treeTex = manager.CreateTexture("TreeTex", treeTexData, sampler).BindDescriptorSet(texSets[2]);
                manager.UpdateTextureDescriptorSets(lDevice, new VulkanTexture[] { carTex, ghxpTex, treeTex });

                OBJDocument carObj = await OBJDecoder.Decode(File.OpenRead(Path.Combine(modelsPath, "car.obj")));
                carObj.CombineData(out IntPtr ptr, out int total1, out int vCount1, 0, 0, true, true);
                var car = manager.CreateVRO("Car").CreateVecticesBuffer(in ptr, total1, (uint)vCount1, lDevice, SharingMode.Exclusive);
                var carRender = manager.CreateRenderObject<RenderObject>("Car", car, carTex);
                Marshal.FreeHGlobal(ptr);
                OBJDocument chObj = await OBJDecoder.Decode(File.OpenRead(Path.Combine(modelsPath, "ch_t.obj")));
                chObj.CombineData(out IntPtr ptr1, out int total2, out int vCount2, 0, 0, true, true);
                var ch = manager.CreateVRO("Ch").CreateVecticesBuffer(in ptr1, total2, (uint)vCount2, lDevice, SharingMode.Exclusive);
                var chRender = manager.CreateRenderObject<RenderObject>("Ch", ch, ghxpTex);
                Marshal.FreeHGlobal(ptr1);
                OBJDocument treeObj = await OBJDecoder.Decode(File.OpenRead(Path.Combine(modelsPath, "tree.obj")));
                treeObj.CombineData(out IntPtr ptr2, out int total3, out int vCount3, 0, 0, true, true);
                var tree = manager.CreateVRO("Tree").CreateVecticesBuffer(in ptr2, total3, (uint)vCount3, lDevice, SharingMode.Exclusive);
                var treeRender = manager.CreateRenderObject<RenderObject>("Tree", tree, treeTex);
                Marshal.FreeHGlobal(ptr2);

                var ratio = (float)Window.Option.Width / Window.Option.Height;
                var objArchetype = ContextManager.CreateArchetype("OBJ", new TransformComponent(), new OBJComponent());
                var mrtArchetype = ContextManager.CreateArchetype("MRT", new TransformComponent(), new MRTComponent());
                ContextManager.CreateContexts(1, objArchetype, mrtGroup, chRender, new TransGroup { Transform = new TransformComponent { Position = new Vector3(0, 0, -100), Scale = new Vector3(1.6f) } });
                ContextManager.CreateContexts(1, objArchetype, mrtGroup, carRender, new TransGroup { Transform = new TransformComponent { Position = new Vector3(87, 0, 50), Scale = Vector3.One } });
                ContextManager.CreateContexts(1, objArchetype, mrtGroup, treeRender, new TransGroup { Transform = new TransformComponent { Position = new Vector3(-87, 0, 50), Scale = Vector3.One } });
                Vector3[] vectors = new Vector3[]
                { 
                    new Vector3(-ratio / 2,0.5f, 0), 
                    new Vector3(ratio / 2, 0.5f, 0),
                    new Vector3(-ratio / 2.0f, -0.5f, 0),
                    new Vector3(ratio / 2.0f, -0.5f, 0)
                };
                for(int i = 0; i < attach.Length; i++)
                {
                    ContextManager.CreateContexts(1, mrtArchetype, presentGroup, boxRenders[i], new TransGroup { Transform = new TransformComponent { Position = vectors[i], Scale = Vector3.One } });
                }
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message + "\r\n" + e.StackTrace);
            }
        }

        protected override Task Unload(VulkanManager manager)
        {
            return Task.CompletedTask;
        }
    }
}
