﻿using IOP.Extension.DependencyInjection;
using IOP.SgrA;
using IOP.SgrA.Vulkan;
using Microsoft.Extensions.Logging;
using SharpVk;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace PCSample14_1
{
    [ModulePriority(ModulePriority.RenderGroup)]
    public class PipelineModule : VulkanModule, IVulkanWindowModule
    {
        public VulkanWindow Window { get; set; }
        [Autowired]
        private ILogger<PipelineModule> Logger { get; set; }
        [Autowired]
        private ICameraManager CameraManager { get; set; }

        protected override async Task Load(VulkanManager manager)
        {
            try
            {
                var option = Window.Option;
                float aspcet = (float)option.Width / option.Height;
                var camera = CameraManager.CreateCamera("main", new Vector3(0, 0f, 300.0f), new Vector3(0, 0, 0), new Vector3(0, 1, 0));
                var hudCamera = CameraManager.CreateCamera("hud", new Vector3(0, 0f, 5.0f), new Vector3(0, 0, 0), new Vector3(0, 1, 0));
                camera.SetProjectMatrix(-aspcet, 1.0f, 1.5f, 1000);
                camera.SetViewport(0, 0, option.Width, option.Height, 0.0f, 1.0f);
                hudCamera.SetViewport(0, 0, option.Width, option.Height, 0.0f, 1.0f);
                hudCamera.SetProjectMatrix(-aspcet, 1.0f, 1.5f, 1000, ProjectMode.Orthogonal);

                var lDevice = manager.LogicDevice;
                var basePath = AppContext.BaseDirectory;
                var shaders = Path.Combine(basePath, "Assets", "Shaders");
                var passDocument = await JsonDocument.ParseAsync(File.OpenRead(Path.Combine(basePath, "RenderPass.json")));
                var pipeDocument = await JsonDocument.ParseAsync(File.OpenRead(Path.Combine(shaders, "Pipeline.json")));
                var pipelineCache = manager.GetPipelineCache();

                var dImage = manager.CreateDepthBufferFromJson(lDevice, passDocument.RootElement.GetProperty("DepthBufferCreateOption").GetRawText(), new Extent3D(Window.Option.Width, Window.Option.Height, 1));
                var mrtImages = new ImageNode[5];
                for(int i = 0; i < 4; i++)
                {
                    mrtImages[i] = lDevice.CreateImageFromJson(passDocument.RootElement.GetProperty("MRTImagesCreateOption").GetRawText(),
                        new Extent3D(option.Width, option.Height, 1), MemoryPropertyFlags.DeviceLocal);
                }
                mrtImages[4] = dImage;
                var mrtPass = manager.CreateRenderPassFromJson("MRT", lDevice, new Format[] { Format.R8G8B8A8UNorm, Format.R8G8B8A8UNorm, Format.R8G8B8A8UNorm, Format.R8G8B8A8UNorm, dImage.ImageFormat },
                    passDocument.RootElement.GetProperty("MRTPassCreateOption").GetRawText());
                mrtPass.CreateFrameBuffer(mrtImages, 1440, 900);
                mrtPass.ConfigBeginInfo((option) =>
                {
                    option.RenderArea = new Rect2D { Extent = new Extent2D(1440, 900), Offset = new Offset2D { X = 0, Y = 0 } };
                    option.ClearValues = new ClearValue[] { (0.2f, 0.2f, 0.2f, 0.2f), (0.2f, 0.2f, 0.2f, 0.2f), (0.2f, 0.2f, 0.2f, 0.2f), (0.2f, 0.2f, 0.2f, 0.2f), new ClearDepthStencilValue { Depth = 1.0f, Stencil = 0 } };
                });

                var dPool = manager.CreateDescriptorPoolFromJson("Main", lDevice, pipeDocument.RootElement.GetProperty("DescriptorPoolCreateOption").GetRawText());
                var setLayouts = manager.CreateDescriptorSetLayoutsFromJson(lDevice, pipeDocument.RootElement.GetProperty("LayoutSetCreateOptions").GetRawText());
                manager.CreateDescriptorGroup("Main", setLayouts);
                VulkanShaderInfo[] infos = new VulkanSPVShaderInfo[]
                {
                    new VulkanSPVShaderInfo(ShaderTypes.VertexShader, "MRTVert", new FileInfo(Path.Combine(shaders,"MRTVert.spv"))),
                    new VulkanSPVShaderInfo(ShaderTypes.FragmentShader, "MRTFrag", new FileInfo(Path.Combine(shaders, "MRTFrag.spv"))),
                    new VulkanSPVShaderInfo(ShaderTypes.VertexShader, "PresentVert", new FileInfo(Path.Combine(shaders,"PresentVert.spv"))),
                    new VulkanSPVShaderInfo(ShaderTypes.FragmentShader, "PresentFrag", new FileInfo(Path.Combine(shaders, "PresentFrag.spv")))
                };
                await Task.WhenAll(infos[0].LoadShader(), infos[1].LoadShader(), infos[2].LoadShader(), infos[3].LoadShader());
                var mrtlayout = manager.CreateDescriptorSets(lDevice, dPool, new DescriptorSetLayout[] { setLayouts[0] });
                var presentlayout = manager.CreateDescriptorSets(lDevice, dPool, new DescriptorSetLayout[] { setLayouts[2] });
                var mrtPipeline = manager.CreateVulkanPipeline(lDevice, "MRTPipeline")
                    .BuildUniformBuffer(0, 80, SharingMode.Exclusive, mrtlayout[0])
                    .BuildPipelineLayoutFromJson(new DescriptorSetLayout[] { setLayouts[0], setLayouts[1] }, pipeDocument.RootElement.GetProperty("MRTLatouyCreateOption").GetRawText())
                    .BuildGraphicsPipelineFromJson(mrtPass, pipelineCache, new VulkanShaderInfo[] { infos[0], infos[1] }, pipeDocument.RootElement.GetProperty("MRTPipeline").GetRawText());
                var presentPipeline = manager.CreateVulkanPipeline(lDevice, "PresentPipeline")
                    .BuildUniformBuffer(0, 4, SharingMode.Exclusive, presentlayout[0])
                    .BuildPipelineLayoutFromJson(new DescriptorSetLayout[] { setLayouts[2], setLayouts[3] }, pipeDocument.RootElement.GetProperty("PresentLayoutCreateOption").GetRawText())
                    .BuildGraphicsPipelineFromJson(Window.SwapchainRenderPass, pipelineCache, new VulkanShaderInfo[] { infos[2], infos[3] }, pipeDocument.RootElement.GetProperty("PresentPipeline").GetRawText());

                var pCommand = lDevice.CreateCommandPool((device, option) =>
                {
                    option.Flags = CommandPoolCreateFlags.ResetCommandBuffer;
                    option.QueueFamilyIndex = device.EnableDeviceQueueIndices[0].FamilyIndex;
                }).CreatePrimaryCommandBuffers(2);
                var fence = lDevice.CreateFence();
                var mrtSemaphore = lDevice.CreateSemaphore();
                var present = manager.CreatePrimaryRenderGroup("Present", presentPipeline)
                    .Binding(pCommand[0].CommandBuffer, lDevice, Window.SwapchainRenderPass, new Semaphore[] { Window.SceneController.ImageAvailableSemaphore, Window.SceneController.RenderFinishSemaphore }, new Fence[] { fence.Fence });
                present.CreateGroupRenderingAction((builder) => builder.UseMiddleware<PresentMiddleware>());
                present.CreateItemRenderingAction((builder) => builder.UseMiddleware<PresentItemMiddleware>());
                present.CreateInitializationAction((group) =>
                {
                    var uBuffer = group.Pipeline.Textures[0];
                    float data = 0.9f;
                    Span<byte> uData = stackalloc byte[sizeof(float)];
                    MemoryMarshal.Write(uData, ref data);
                    uBuffer.UpdateTextureMemoryData(uData);
                });
                present.SetCamera(hudCamera);

                var mrt = manager.CreatePrimaryRenderGroup("MRT", mrtPipeline)
                    .Binding(pCommand[1].CommandBuffer, lDevice, mrtPass, new Semaphore[] { mrtSemaphore.Semaphore }, Array.Empty<Fence>())
                    .SetSignalSemaphores(new SharpVk.Semaphore[] { mrtSemaphore.Semaphore });
                mrt.CreateGroupRenderingAction((builder) => builder.UseMiddleware<MRTMiddleware>());
                mrt.CreateItemRenderingAction((builder) => builder.UseMiddleware<MRTItemMiddleware>());
                mrt.SetCamera(camera);
                mrt.CreatePointLight("main", new Vector3(0, 100, 100), new LightStrength(0.6f, 0.6f, 0.6f, 0.6f));
                mrt.Light.Ambient = new Ambient(0.1f, 0.1f, 0.1f, 0.1f);
                present.AddChildren(mrt);

                manager.CreateAndCutScene<Scene>(Window, "Main", present, typeof(InputSystem), typeof(TransformSystem));
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message + "\r\n" + e.StackTrace);
            }
        }

        protected override Task Unload(VulkanManager manager)
        {
            return Task.CompletedTask;
        }
    }
}
