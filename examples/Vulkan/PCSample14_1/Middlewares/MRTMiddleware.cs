﻿using IOP.SgrA;
using IOP.SgrA.Vulkan;
using Microsoft.Extensions.Logging;
using SharpVk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PCSample14_1
{
    public class MRTMiddleware : IProductionLineMiddleware<PrimaryVulkanRenderGroup>
    {
        public void Invoke(PrimaryVulkanRenderGroup group, RenderingProductionLineDelegate<PrimaryVulkanRenderGroup> next)
        {
            try
            {
                var queue = group.WorkQueues.WorkQueues[0].Queue;
                var cmdBuffer = group.PrimaryCommandBuffer;
                var semaphores = group.Semaphores;
                var pass = group.RenderPass;
                var camera = group.Camera;
                var view = camera.ViewMatrix;
                var project = camera.ProjectionMatrix;

                var uBuffer = group.Pipeline.Textures[0];
                group.Light.TryGetPointLight("main", out PointLight light);
                Vector4 ambient = group.Light.Ambient;
                var cameraP = new Vector4(camera.EyePosition, 1.0f);
                var lightP = new Vector4(light.Position, 1.0f);
                Vector4 strength = light.Strength;
                Span<byte> uData = stackalloc byte[sizeof(float) * 20];
                MemoryMarshal.Write(uData.Slice(0, 16), ref cameraP);
                MemoryMarshal.Write(uData.Slice(16, 16), ref lightP);
                MemoryMarshal.Write(uData.Slice(32, 16), ref ambient);
                MemoryMarshal.Write(uData.Slice(48, 16), ref strength);
                MemoryMarshal.Write(uData.Slice(64, 16), ref strength);
                uBuffer.UpdateTextureMemoryData(uData);

                Framebuffer framebuffer = group.RenderPass.GetFramebuffer(group.CurrentFrame);
                cmdBuffer.Reset();
                cmdBuffer.Begin();
                cmdBuffer.BeginRenderPass(pass.RenderPass, framebuffer, pass.BeginOption.RenderArea, pass.BeginOption.ClearValues, SubpassContents.Inline);
                cmdBuffer.BindPipeline(PipelineBindPoint.Graphics, group.Pipeline.VkPipeline);
                foreach (var item in group.GetContexts())
                {
                    item.SetViewMatrix(in view);
                    item.SetProjectionMatrix(in project);
                    group.ItemProductionLine(item);
                }
                cmdBuffer.EndRenderPass();
                cmdBuffer.End();

                var submitInfo = new SubmitInfo
                {
                    WaitDestinationStageMask = new PipelineStageFlags[] { PipelineStageFlags.ColorAttachmentOutput },
                    CommandBuffers = new CommandBuffer[] { cmdBuffer },
                    SignalSemaphores = new Semaphore[] { semaphores[0] }
                };
                queue.Submit(submitInfo, null);
            }
            catch (Exception e)
            {
                group.Logger.LogError(e.Message + "\r\n" + e.StackTrace);
                group.Disable();
            }
        }
    }
}
