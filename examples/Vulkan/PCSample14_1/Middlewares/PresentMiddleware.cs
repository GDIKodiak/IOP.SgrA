﻿using IOP.SgrA;
using IOP.SgrA.Vulkan;
using Microsoft.Extensions.Logging;
using SharpVk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCSample14_1
{
    public class PresentMiddleware : IProductionLineMiddleware<PrimaryVulkanRenderGroup>
    {
        private readonly List<SharpVk.Semaphore> WaitSemaphores = new List<SharpVk.Semaphore>();
        public void Invoke(PrimaryVulkanRenderGroup group, RenderingProductionLineDelegate<PrimaryVulkanRenderGroup> next)
        {
            try
            {
                var camera = group.Camera;
                var view = camera.ViewMatrix;
                var project = camera.ProjectionMatrix;
                var queue = group.WorkQueues.WorkQueues[0].Queue;
                var cmdBuffer = group.PrimaryCommandBuffer;
                var semaphores = group.Semaphores;
                var pass = group.RenderPass;
                var face = group.Fences[0];

                WaitSemaphores.Clear();
                WaitSemaphores.Add(semaphores[0]);
                foreach (var child in group.GetChildrens())
                {
                    child.GroupRendering();
                    WaitSemaphores.AddRange(child.SignalSemaphores);
                }

                Framebuffer framebuffer = group.RenderPass.GetFramebuffer(group.CurrentFrame);
                cmdBuffer.Reset();
                cmdBuffer.Begin();
                cmdBuffer.BeginRenderPass(pass.RenderPass, framebuffer, pass.BeginOption.RenderArea, pass.BeginOption.ClearValues, SubpassContents.Inline);
                var viewPort = camera.Viewport;
                cmdBuffer.SetViewport(0, new Viewport(viewPort.X, viewPort.Y, viewPort.Width, viewPort.Height, viewPort.MinDepth, viewPort.MaxDepth));
                cmdBuffer.BindPipeline(PipelineBindPoint.Graphics, group.Pipeline.VkPipeline);
                foreach (var item in group.GetContexts())
                {
                    item.SetViewMatrix(in view);
                    item.SetProjectionMatrix(in project);
                    group.ItemProductionLine(item);
                }
                cmdBuffer.EndRenderPass();
                cmdBuffer.End();

                var local = WaitSemaphores.ToArray();
                PipelineStageFlags[] ps = new PipelineStageFlags[local.Length];
                for (int i = 0; i < local.Length; i++) ps[i] = PipelineStageFlags.ColorAttachmentOutput;
                var submitInfo = new SubmitInfo
                {
                    WaitDestinationStageMask = ps,
                    WaitSemaphores = local,
                    CommandBuffers = new CommandBuffer[] { cmdBuffer },
                    SignalSemaphores = new Semaphore[] { semaphores[1] }
                };
                queue.Submit(submitInfo, face);
                Result r = Result.Timeout;
                do
                {
                    r = group.LogicDevice.WaitForFences(face, true, 100000000);
                } while (r == Result.Timeout);
                group.LogicDevice.ResetFences(face);
            }
            catch (Exception e)
            {
                group.Logger.LogError(e.Message + "\r\n" + e.StackTrace);
                group.Disable();
            }
        }
    }
}
