﻿using IOP.SgrA;
using IOP.SgrA.Vulkan;
using SharpVk;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PCSample14_1
{
    public class PresentItemMiddleware : IProductionLineMiddleware<VulkanContext>
    {
        public void Invoke(VulkanContext context, RenderingProductionLineDelegate<VulkanContext> next)
        {
            var lDevice = context.LogicDevice;
            var render = context.GetContextRenderObject();
            var pipeline = context.VulkanPipeline;
            int size = Marshal.SizeOf<Matrix4x4>();
            ref MVPMatrix local = ref context.GetMVPMatrix();
            Matrix4x4 mat = local.GetFinalMatrix() * context.ClipMatrix;
            byte[] data = ArrayPool<byte>.Shared.Rent(size);
            Span<byte> span = data;
            span = span.Slice(0, size);
            mat.ToBytes(ref span, 0);

            var vro = render.GetComponents<VRO>().First();
            var tex = render.GetComponents<VulkanTexture>().First();
            var cmd = context.CommandBuffer;
            lDevice.UpdateDescriptorSets(tex.WriteDescriptorSet, null);

            DescriptorSet[] sets = new DescriptorSet[2];
            sets[0] = pipeline.Textures[0].DescriptorSet;
            sets[1] = tex.DescriptorSet;
            cmd.BindDescriptorSets(PipelineBindPoint.Graphics, pipeline.PipelineLayout, 0, sets, null);
            cmd.BindVertexBuffers(0, vro.VerticesBufferInfo.Buffer, new DeviceSize[] { 0 });
            cmd.PushConstants(pipeline.PipelineLayout, ShaderStageFlags.Vertex, 0, span.ToArray());
            cmd.Draw(vro.VecticesCount, 1, 0, 0);
            ArrayPool<byte>.Shared.Return(data);
        }
    }
}
