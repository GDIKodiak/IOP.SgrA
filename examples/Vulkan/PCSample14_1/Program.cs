﻿using IOP.SgrA;
using IOP.SgrA.Vulkan;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace PCSample14_1
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var h = Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((config) =>
                {
                    config.SetBasePath(Directory.GetCurrentDirectory());
                    if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) config.AddVulkanJsonFile("Vulkan.json");
                    else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux)) config.AddVulkanJsonFile("Vulkan-Linux.json");
                    else throw new NotSupportedException("Not supported Platform");
                })
                .ConfigureSgrAHost((sgra) =>
                {
                    sgra.AddVulkanEnvironmental();
                    sgra.UseStartup<Startup>();
                }).ConfigureLogging((logger) => logger.AddConsole()); ;
            await h.RunConsoleAsync();
        }
    }
}
