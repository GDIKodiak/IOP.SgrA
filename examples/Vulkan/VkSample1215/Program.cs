﻿using IOP.ISEA.Generator;
using IOP.ISEA;
using IOP.SgrA;
using IOP.SgrA.SilkNet.Vulkan;
using Microsoft.Extensions.Hosting;
using System.Runtime.InteropServices;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace VkSample1215
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            await CreateISEA(true);
            var h = Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((config) =>
                {
                    config.SetBasePath(Directory.GetCurrentDirectory());
                    if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) config.AddVulkanJsonFile("Vulkan.json");
                    else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux)) config.AddVulkanJsonFile("Vulkan-Linux.json");
                    else throw new NotSupportedException("Not supported Platform");
                })
                .ConfigureSgrAHost((sgra) =>
                {
                    sgra.AddVulkanEnvironmental();
                    sgra.UseStartup<Startup>();
                }).ConfigureLogging((logger) => logger.AddConsole()); ;
            await h.RunConsoleAsync();
        }

        static async Task CreateISEA(bool isNeed)
        {
            if (!isNeed) return;
            var basePath = AppContext.BaseDirectory;
            var assetsPath = Path.Combine(basePath, "Assets", "generator.json");
            using var stream = File.OpenRead(assetsPath);
            ISEADocument isea = await ISEAGenerator.GenerateDocument(stream);
            await isea.Save(basePath, "test1215");
        }
    }
}
