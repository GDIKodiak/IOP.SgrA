﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VkSample1215
{
    public class Flat
    {
        public float[] Data { get; set; }

        public uint VCount {  get; set; }

        public void GenVertexData()
        {
            int cols = 3;
            int rows = cols;
            List<float> alVertix = new List<float>();
            List<float> alTexCoor = new List<float>();
            float widht = 50.0f;
            float UNIT_SIZE = widht / cols;
            for (int i = 0; i < cols; i++)
            {
                for (int j = 0; j < rows; j++)
                {
                    float zsx = -UNIT_SIZE * cols / 2 + i * UNIT_SIZE;
                    float zsy = UNIT_SIZE * rows / 2 - j * UNIT_SIZE;
                    float zsz = 0;
                    alVertix.Add(zsx);
                    alVertix.Add(zsy);
                    alVertix.Add(zsz);
                    alVertix.Add(zsx);
                    alVertix.Add(zsy - UNIT_SIZE);
                    alVertix.Add(zsz);
                    alVertix.Add(zsx + UNIT_SIZE);
                    alVertix.Add(zsy);
                    alVertix.Add(zsz);
                    alVertix.Add(zsx + UNIT_SIZE);
                    alVertix.Add(zsy);
                    alVertix.Add(zsz);
                    alVertix.Add(zsx);
                    alVertix.Add(zsy - UNIT_SIZE);
                    alVertix.Add(zsz);
                    alVertix.Add(zsx + UNIT_SIZE);
                    alVertix.Add(zsy - UNIT_SIZE);
                    alVertix.Add(zsz);
                }
            }
            for (int i = 0; i < cols; i++)
            {
                for (int j = 0; j < rows; j++)
                {
                    alTexCoor.Add(0);
                    alTexCoor.Add(0);
                    alTexCoor.Add(0);
                    alTexCoor.Add(1);
                    alTexCoor.Add(1);
                    alTexCoor.Add(0);
                    alTexCoor.Add(1);
                    alTexCoor.Add(0);
                    alTexCoor.Add(0);
                    alTexCoor.Add(1);
                    alTexCoor.Add(1);
                    alTexCoor.Add(1);
                }
            }
            VCount = (uint)(cols * rows * 6);
            Data = new float[VCount * 6];
            int index = 0;
            for (int i = 0; i < VCount; i++)
            {
                Data[index++] = alVertix[i * 3 + 0];
                Data[index++] = alVertix[i * 3 + 1];
                Data[index++] = alVertix[i * 3 + 2];
                Data[index++] = alTexCoor[i * 2 + 0];
                Data[index++] = alTexCoor[i * 2 + 1];
                Data[index++] = 0.0f;
            }
        }
    }
}
