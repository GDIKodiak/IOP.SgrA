﻿using IOP.ISEA;
using IOP.SgrA;
using IOP.SgrA.SilkNet.Vulkan;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace VkSample1215
{
    [ModulePriority(ModulePriority.Assets)]
    [ParentModule(typeof(PipelineModule))]
    public class AssetsModule : VulkanModule, IVulkanRenderWapperModule
    {
        public VulkanRenderWapper RenderWapper { get; set; }

        private readonly ILogger<AssetsModule> _Logger;
        public AssetsModule(ILogger<AssetsModule> logger)
        {
            _Logger = logger;
        }

        protected override async Task Load(VulkanGraphicsManager graphicsManager)
        {
            try
            {
                var basePath = AppContext.BaseDirectory;
                var texPath = Path.Combine(basePath, "Assets");
                var lDevice = graphicsManager.VulkanDevice;
                var pModule = Parent as PipelineModule ?? throw new ArgumentNullException(nameof(Parent));

                ISEADocument document = new ISEADocument();
                await document.Load(Path.Combine(basePath, "test1215.isea"));
                var renders = await graphicsManager.CreateRenderObjectsFromISEA<RenderObject>(document);
                var brazier = renders.First(x => x.Name == "Brazier");
                var wall = renders.First(x => x.Name == "Wall");
                var fire = renders.First(x => x.Name == "Fire");
                Flat flat = new Flat();
                flat.GenVertexData();
                var mesh = graphicsManager.CreateMesh(graphicsManager.NewStringToken(), flat.Data, flat.VCount, Vector3.Zero, Vector3.Zero);
                wall.AddComponent(mesh);
                wall.GetTransform().Rotation = Quaternion.CreateFromAxisAngle(Vector3.UnitX, 1.5708f);

                var brazer1 = brazier.DeepCopy(graphicsManager);
                brazer1.GetTransform().Position = new Vector3(7.0f, 0.0f, 7.0f);
                var part1 = new ParticleSystem(ParticleSystem.COUNT[0], ParticleSystem.ParticlePositon[0][0], ParticleSystem.ParticlePositon[0][1], 0);
                var pTex1 = pModule.ParticleGroupOne.Pipeline.GetPipelineTexture(0, 0) as VulkanDynamicBuffer;
                var block1 = pTex1.MallocBlock(104);
                var firevro = graphicsManager.CreateMesh(graphicsManager.NewStringToken(), part1.Points, part1.VCount, Vector3.One, Vector3.One);
                var fire1 = fire.DeepCopy(graphicsManager);
                fire1.AddComponent(part1);
                fire1.AddComponent(block1);
                fire1.AddComponent(firevro);
                fire1.AddComponent(pModule.ParticleGroupOne);
                brazer1.AddChildren(fire1);

                var brazer2 = brazier.DeepCopy(graphicsManager);
                brazer2.GetTransform().Position = new Vector3(-7.0f, 0.0f, -7.0f);
                var part2 = new ParticleSystem(ParticleSystem.COUNT[1], ParticleSystem.ParticlePositon[1][0], ParticleSystem.ParticlePositon[1][1], 1);
                var pTex2 = pModule.ParticleGroupTwo.Pipeline.GetPipelineTexture(0, 0) as VulkanDynamicBuffer;
                var block2 = pTex2.MallocBlock(104);
                var firevro2 = graphicsManager.CreateMesh(graphicsManager.NewStringToken(), part2.Points, part2.VCount, Vector3.One, Vector3.One);
                var fire2 = fire.DeepCopy(graphicsManager);
                fire2.AddComponent(part2);
                fire2.AddComponent(block2);
                fire2.AddComponent(firevro2);
                fire2.AddComponent(pModule.ParticleGroupTwo);
                brazer2.AddChildren(fire2);

                var brazer3 = brazier.DeepCopy(graphicsManager);
                brazer3.GetTransform().Position = new Vector3(-7.0f, 0.0f, 7.0f);
                var part3 = new ParticleSystem(ParticleSystem.COUNT[2], ParticleSystem.ParticlePositon[2][0], ParticleSystem.ParticlePositon[2][1], 2);
                var pTex3 = pModule.ParticleGroupThree.Pipeline.GetPipelineTexture(0, 0) as VulkanDynamicBuffer;
                var block3 = pTex3.MallocBlock(104);
                var firevro3 = graphicsManager.CreateMesh(graphicsManager.NewStringToken(), part3.Points, part3.VCount, Vector3.One, Vector3.One);
                var fire3 = fire.DeepCopy(graphicsManager);
                fire3.AddComponent(part3);
                fire3.AddComponent(block3);
                fire3.AddComponent(firevro3);
                fire3.AddComponent(pModule.ParticleGroupThree);
                brazer3.AddChildren(fire3);

                var brazer4 = brazier.DeepCopy(graphicsManager);
                brazer4.GetTransform().Position = new Vector3(7.0f, 0.0f, -7.0f);
                var part4 = new ParticleSystem(ParticleSystem.COUNT[3], ParticleSystem.ParticlePositon[3][0], ParticleSystem.ParticlePositon[3][1], 3);
                var pTex4 = pModule.ParticleGroupFour.Pipeline.GetPipelineTexture(0, 0) as VulkanDynamicBuffer;
                var block4 = pTex4.MallocBlock(104);
                var firevro4 = graphicsManager.CreateMesh(graphicsManager.NewStringToken(), part4.Points, part4.VCount, Vector3.One, Vector3.One);
                var fire4 = fire.DeepCopy(graphicsManager);
                fire4.AddComponent(part4);
                fire4.AddComponent(block4);
                fire4.AddComponent(firevro4);
                fire4.AddComponent(pModule.ParticleGroupFour);
                brazer4.AddChildren(fire4);

                var arch = ContextManager.CreateArchetype(new TransformComponent());
                var arch2 = ContextManager.CreateArchetype(new ParticleComponent());
                ContextManager.CreateContexts(1, arch, pModule.CommonGroup, wall);
                ContextManager.CreateContexts(1, arch, pModule.CommonGroup, brazer1);
                ContextManager.CreateContexts(1, arch, pModule.CommonGroup, brazer2);
                ContextManager.CreateContexts(1, arch, pModule.CommonGroup, brazer3);
                ContextManager.CreateContexts(1, arch, pModule.CommonGroup, brazer4);
            }
            catch (Exception e)
            {
                _Logger.LogError(e, "");
            }
        }

        protected override Task Unload(VulkanGraphicsManager manager)
        {
            return Task.CompletedTask;
        }
    }
}
