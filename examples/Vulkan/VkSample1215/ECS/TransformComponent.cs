﻿using IOP.SgrA.ECS;
using System.Numerics;

namespace VkSample1215;

public struct TransformComponent : IComponentData
{
    public Vector3 Position;
    public Quaternion Angle;
    public Vector3 Scale;
}
