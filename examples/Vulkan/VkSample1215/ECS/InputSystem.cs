﻿using IOP.SgrA;
using IOP.SgrA.ECS;
using IOP.SgrA.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace VkSample1215
{
    public class InputSystem : ComponentSystem
    {
        private Camera Camera;
        private bool Fource = false;

        private IGraphicsManager GraphicsManager;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="contextManager"></param>
        public InputSystem(IGraphicsManager manager)
        {
            GraphicsManager = manager;
        }

        public override void Initialization()
        {
            GraphicsManager.TryGetCamera("main", out Camera camera);
            Camera = camera;
        }

        public override void Update(TimeSpan span)
        {
            var state = Input.GetMouseButtonState(MouseButton.ButtonLeft);
            if(state.Action == InputAction.Press)
            {
                var delta = Input.GetMousePositionDelta();
                if (delta.X != 0 || delta.Y != 0)
                {
                    Camera.EncircleFromMouse(Vector3.Zero, delta.X, delta.Y, (float)(0.02 * Math.PI / 180.0));
                }
            }
            ContextManager.Foreach<CommonGroup>((group) =>
            {
                var context = ContextManager.GetContext(group.Entity);
                var render = context.GetContextRenderObject();
                var renderGroup = context.GetContextRenderGroup();
                ref MVPMatrix mvp = ref context.GetMVPMatrix();
                mvp.ModelMatrix = Matrix4x4.CreateFromQuaternion(render.GetTransform().Rotation) *
                    Matrix4x4.CreateTranslation(render.GetTransform().Position);
                context.PushToRenderGroup();
                foreach(var child in render.GetChildrens())
                {
                    if(child.TryGetRenderGroup(out IRenderGroup g))
                    {
                        g.PushContext(child);
                    }
                }
            });
        }
    }

    public struct CommonGroup
    {
        public Entity Entity;
        public TransformComponent Transform;
    }

    public struct ParticleGroup
    {
        public Entity Entity;
        public ParticleComponent Particle;
    }
}
