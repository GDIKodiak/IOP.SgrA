﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;

namespace VkSample1215
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class ParticleThreeFrag : GLSLFrag
    {
        [DynamicUniformBuffer(0, 0, DescriptorSetTarget.Pipeline)]
        public ParticleVals Vals { get; set; }
        [SampledImage(1, 0, DescriptorSetTarget.RenderObject)]
        public sampler2D Tex { get; set; }

        [ShaderInput(0)]
        public float SjFactor { get; set; }
        [ShaderInput(1)]
        public vec4 VPosition { get; set; }

        [FragOutput(0, true, AlphaBlendOp = BlendOp.Add, ColorBlendOp = BlendOp.Add,
            SrcColorBlendFactor = BlendFactor.SrcAlpha, DstColorBlendFactor = BlendFactor.OneMinusSrcAlpha,
            SrcAlphaBlendFactor = BlendFactor.SrcAlpha, DstAlphaBlendFactor = BlendFactor.OneMinusSrcAlpha)]
        public vec4 OutColor { get; set; }

        public override void main()
        {
            if (VPosition.w == 10.0f) discard();
            vec4 colorTL = textureLod(Tex, gl_PointCoord, 0.0f);
            vec4 colorT;
            float disT = distance(VPosition.xyz, new vec3(0.0f, 0.0f, 0.0f));
            float tampFactor = (1.0f - disT / Vals.BJ) * SjFactor;
            vec4 factor4 = new vec4(tampFactor, tampFactor, tampFactor, tampFactor);
            colorT = clamp(factor4, Vals.EndColor, Vals.StartColor);
            colorT = colorT * colorTL.a;
            OutColor = colorT;
        }
    }
}
