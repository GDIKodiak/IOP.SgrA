﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VkSample1215
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class ParticleVert : GLSLVert
    {
        [DynamicUniformBuffer(0, 0, DescriptorSetTarget.Pipeline)]
        public ParticleVals Vals { get; set; }

        [VertInput(0, Format.R32G32Sfloat)]
        public vec2 Pos {  get; set; }
        [VertInput(1, Format.R32G32Sfloat)]
        public vec2 Param { get; set; }

        [ShaderOutput(0)]
        public float SjFactor {  get; set; }
        [ShaderOutput(1)]
        public vec4 VPosition {  get; set; }

        public override void main()
        {
            vec4 currPosition = Vals.MM * new vec4(Pos.xy, 0.0f, 1);
            float d = distance(currPosition.xyz, Vals.UCamera.xyz);
            float s = 1.0f / sqrt(0.05f + 0.05f * d + 0.001f * d * d); //求出距离缩放因子S的平方分之1
            gl_PointSize = Vals.BJ * s;//设置点精灵对应点的尺寸
            gl_Position = Vals.MVP * new vec4(Pos.x, Pos.y, 0.0f, 1.0f);
            VPosition = new vec4(Pos.x, Pos.y, 0.0f, Param.g);
            SjFactor = (Vals.MaxLifeSpan - Param.g) / Vals.MaxLifeSpan;
        }
    }

    public struct ParticleVals
    {
        public mat4 MVP;
        public mat4 MM;
        public vec4 UCamera;
        public vec4 StartColor;
        public vec4 EndColor;
        public float MaxLifeSpan;
        public float BJ;
    }
}
