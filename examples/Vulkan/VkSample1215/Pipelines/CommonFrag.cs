﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace VkSample1215
{
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class CommonFrag : GLSLFrag
    {
        [SampledImage(0, 0, DescriptorSetTarget.RenderObject)]
        public sampler2D Tex {  get; set; }
        [ShaderInput(0)]
        public vec2 InTexCoor {  get; set; }
        [FragOutput(0, false)]
        public vec4 OutColor {  get; set; }

        public override void main()
        {
            OutColor = textureLod(Tex, InTexCoor, 0.0f);
        }
    }
}
