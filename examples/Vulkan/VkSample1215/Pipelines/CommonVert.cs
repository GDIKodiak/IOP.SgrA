﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VkSample1215
{
    /// <summary>
    /// 
    /// </summary>
    [ShaderVersion("460")]
    [GLSLExtension("GL_ARB_separate_shader_objects : enable")]
    [GLSLExtension("GL_ARB_shading_language_420pack : enable")]
    public partial class CommonVert : GLSLVert
    {
        [PushConstant(64, 0)]
        public ConstantVals Vals { get; set; }
        [VertInput(0, Format.R32G32B32Sfloat)]
        public vec3 Pos { get; set; }
        [VertInput(1, Format.R32G32B32Sfloat)]
        public vec3 InTexCoor {  get; set; }
        [ShaderOutput(0)]
        public vec2 OutTexCoor { get; set; }

        public override void main()
        {
            OutTexCoor = InTexCoor.xy;
            gl_Position = Vals.MVP * new vec4(Pos, 1.0f);
        }
    }

    public struct ConstantVals
    {
        public mat4 MVP;
    }
}
