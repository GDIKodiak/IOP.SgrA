﻿using IOP.SgrA;
using IOP.SgrA.SilkNet.Vulkan;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace VkSample1215;

public class WindowConfig : IWindowConfig<Window, VulkanGraphicsManager>
{
    public Task Config(Window window, VulkanGraphicsManager graphics)
    {
        var windowOption = window.Option;
        var lDevice = graphics.VulkanDevice;
        var basePath = AppContext.BaseDirectory;
        var swapchain = lDevice.CreateDefaultSwapChain(window).CreateDefaultSwapChainImageViews();
        var area = new Area(0, 0, windowOption.Width, windowOption.Height);
        var pass = lDevice.CreateSwapchainScriptedRenderPass<SwapChainRenderPass>(swapchain, area);
        var i = lDevice.CreateSemaphore(ArrangeConstant.ImageAvailableSemaphore);
        var f = lDevice.CreateSemaphore(ArrangeConstant.RenderFinishSemaphore);
        window.CreateVulkanRenderDispatcher(lDevice.WorkQueues[0], swapchain, pass, i, f);
        window.ConfigResizeCallbackHandle(RasizeHandle);
        return Task.CompletedTask;
    }

    public void Initialization()
    {
    }

    private void RasizeHandle(VulkanGlfwWindow window, uint width, uint height)
    {
        window.RenderDispatcher.RecreateRenderDispatcher(width, height);
        var manager = window.GraphicsManager;
        float aspect = (float)width / height;
        foreach (var item in manager.GetCameras())
        {
            item.SetViewport(0, 0, width, height, 0, 1);
            item.SetProjectMatrix(-aspect, 1.0f, item.Near, item.Far, item.ProjectMode);
        }
    }
}
