﻿using IOP.Extension.DependencyInjection;
using IOP.SgrA;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.OpenType;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace TextSample.Modules
{
    /// <summary>
    /// 
    /// </summary>
    [ModulePriority(ModulePriority.Assets)]
    public class FontModule : VulkanModule, ISceneModule
    {
        public Scene Scene { get; set; }

        [Autowired]
        private ILogger<FontModule> _Logger { get; set; }

        protected override async Task Load(VulkanGraphicsManager manager)
        {
            try
            {
                var basePath = AppContext.BaseDirectory;
                var texPath = Path.Combine(basePath, "Assets", "Textures");
                var lDevice = manager.VulkanDevice;
                JsonDocument dJson = await JsonDocument.ParseAsync(File.OpenRead(Path.Combine(texPath, "image.json")));
                var assetsJson = dJson.RootElement;
                var fontTexData = await manager.CreateEmptyTextureData<DdsVulkanTextureData>("simfang")
                    .LoadTextureData(lDevice, Path.Combine(texPath, "sdfArray.dds"), assetsJson.GetProperty("SDFImage").GetRawText());

                var sampler = lDevice.CreateSamplerFromJson("Sampler", assetsJson.GetProperty("Sampler").GetRawText());
                var textTex = manager.CreateTexture("simfangTex", fontTexData, sampler);

                var mapping = await JsonSerializer.DeserializeAsync<List<GlyphMapping>>(File.OpenRead(Path.Combine(texPath, "mapping.json")));
                var metrics = await JsonSerializer.DeserializeAsync<FontMetrics>(File.OpenRead(Path.Combine(texPath, "metrics.json")));
                var fontTexInfo = new FontTextureInfo(7, 4096, 4096, 64);

                var creater = new StaticGlyphIdCharacterCreater(metrics, mapping, fontTexInfo);
                var font = manager.CreateSDFFont("simfang", creater, textTex);
                fontTexData.Clear();

                manager.TryGetRenderGroup("Text", out IRenderGroup group);
                manager.TryGetRenderGroup("Main", out IRenderGroup mainGroup);

                var archetype = ContextManager.CreateArchetype(new TransformComponent(), new TextComponent());
                var line1 = manager.GetOrCreateTextRender("字体名称：仿宋", font, 50, 1);
                var line2 = manager.GetOrCreateTextRender("版本：Version 5.01", font, 50, 1);
                var line3 = manager.GetOrCreateTextRender("OpenType Layout，已数字签名，TrueType Outlines", font, 50, 1);
                var line4 = manager.GetOrCreateTextRender("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ", font, 100, 1);
                var line5 = manager.GetOrCreateTextRender("1234567890.:,;‘“ (!?) +-*/=", font, 50, 1);
                var size12 = manager.GetOrCreateTextRender("12", font, 5, 1);
                var size18 = manager.GetOrCreateTextRender("18", font, 5, 1);
                var size24 = manager.GetOrCreateTextRender("24", font, 5, 1);
                var size36 = manager.GetOrCreateTextRender("36", font, 5, 1);
                var size48 = manager.GetOrCreateTextRender("48", font, 5, 1);
                var size60 = manager.GetOrCreateTextRender("60", font, 5, 1);
                var size72 = manager.GetOrCreateTextRender("72", font, 5, 1);
                var render = manager.GetOrCreateTextRender("Innovation in China 中国智造，慧及全球 0123456789", font, 50, 1);

                ContextManager.CreateContexts(1, archetype, group, line1, new TextGroup { Text = new TextComponent() { Height = 16 / 0.75f, FontSize = 16, Row = 0 } });
                ContextManager.CreateContexts(1, archetype, group, line2, new TextGroup { Text = new TextComponent() { Height = 16 / 0.75f, FontSize = 16, Row = 1 } });
                ContextManager.CreateContexts(1, archetype, group, line3, new TextGroup { Text = new TextComponent() { Height = 16 / 0.75f, FontSize = 16, Row = 2 } });
                ContextManager.CreateContexts(1, archetype, group, line4, new TextGroup { Text = new TextComponent() { Height = 16 / 0.75f, FontSize = 16, Row = 3 } });
                ContextManager.CreateContexts(1, archetype, group, line5, new TextGroup { Text = new TextComponent() { Height = 16 / 0.75f, FontSize = 16, Row = 4 } });
                ContextManager.CreateContexts(1, archetype, group, size12, new TextGroup { Text = new TextComponent() { Height = 16 / 0.75f, FontSize = 14, Row = 5 } });
                ContextManager.CreateContexts(1, archetype, group, size18, new TextGroup { Text = new TextComponent() { Height = 18 / 0.75f, FontSize = 14, Row = 6, YOffset = -2 } });
                ContextManager.CreateContexts(1, archetype, group, size24, new TextGroup { Text = new TextComponent() { Height = 24 / 0.75f, FontSize = 14, Row = 7, YOffset = -6 } });
                ContextManager.CreateContexts(1, archetype, group, size36, new TextGroup { Text = new TextComponent() { Height = 36 / 0.75f, FontSize = 14, Row = 8, YOffset = -10 } });
                ContextManager.CreateContexts(1, archetype, group, size48, new TextGroup { Text = new TextComponent() { Height = 48 / 0.75f, FontSize = 14, Row = 9, YOffset = -16 } });
                ContextManager.CreateContexts(1, archetype, group, size60, new TextGroup { Text = new TextComponent() { Height = 60 / 0.75f, FontSize = 14, Row = 10, YOffset = -22 } });
                ContextManager.CreateContexts(1, archetype, group, size72, new TextGroup { Text = new TextComponent() { Height = 72 / 0.75f, FontSize = 14, Row = 11, YOffset = -22 } });
                ContextManager.CreateContexts(1, archetype, group, render, new TextGroup { Text = new TextComponent() { Height = 16 / 0.75f, FontSize = 12, Row = 5, XOffset = 45 } });
                ContextManager.CreateContexts(1, archetype, group, render, new TextGroup { Text = new TextComponent() { Height = 18 / 0.75f, FontSize = 18, Row = 6, XOffset = 45 } });
                ContextManager.CreateContexts(1, archetype, group, render, new TextGroup { Text = new TextComponent() { Height = 24 / 0.75f, FontSize = 24, Row = 7, XOffset = 45 } });
                ContextManager.CreateContexts(1, archetype, group, render, new TextGroup { Text = new TextComponent() { Height = 36 / 0.75f, FontSize = 36, Row = 8, XOffset = 45 } });
                ContextManager.CreateContexts(1, archetype, group, render, new TextGroup { Text = new TextComponent() { Height = 48 / 0.75f, FontSize = 48, Row = 9, XOffset = 45 } });
                ContextManager.CreateContexts(1, archetype, group, render, new TextGroup { Text = new TextComponent() { Height = 60 / 0.75f, FontSize = 60, Row = 10, XOffset = 45 } });
                ContextManager.CreateContexts(1, archetype, group, render, new TextGroup { Text = new TextComponent() { Height = 72 / 0.75f, FontSize = 72, Row = 11, XOffset = 45 } });
            }
            catch (Exception e)
            {
                _Logger.LogError(e.Message + "\r\n" + e.StackTrace);
            }
        }

        protected override Task Unload(VulkanGraphicsManager manager)
        {
            return Task.CompletedTask;
        }
    }
}
