﻿using IOP.Extension.DependencyInjection;
using IOP.SgrA;
using IOP.SgrA.SilkNet.Vulkan;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using TextSample.Modules;

namespace TextSample
{
    [ModulePriority(ModulePriority.RenderGroup)]
    public class PipelineModule : VulkanModule, IVulkanRenderWapperModule
    {
        public VulkanRenderWapper RenderWapper { get; set; }

        [Autowired]
        private ILogger<PipelineModule> _Logger { get; set; }

        protected override async Task Load(VulkanGraphicsManager manager)
        {
            try
            {
                var lDevice = manager.VulkanDevice;
                var pipeCache = lDevice.PipelineCache;
                var disapatcher = RenderWapper.GetRenderDispatcher<VulkanSwapchainRenderDispatcher>();
                var pass = disapatcher.GetSwapchainRenderPass();
                var area = RenderWapper.RenderArea;
                var basePath = AppContext.BaseDirectory;
                var shaders = Path.Combine(basePath, "Assets", "Shaders");
                var document = await JsonDocument.ParseAsync(File.OpenRead(Path.Combine(shaders, "Pipeline.json")));
                var pipelineJson = document.RootElement;
                var pipelineCache = lDevice.PipelineCache;

                var camera = new Camera(new Vector3(0, 0f, 5.0f), new Vector3(0, 0, 0), new Vector3(0, 1, 0));
                float aspcet = (float)area.Width / area.Height;
                camera.SetProjectMatrix(-aspcet, 1.0f, 1.5f, 1000, ProjectMode.Orthogonal);
                camera.SetViewport(0, 0, area.Width, area.Height, 0.0f, 1.0f);

                manager.TryGetRenderGroup("Main", out IRenderGroup group);
                var mainGroup = group as PrimaryVulkanRenderGroup;

                VulkanShaderInfo[] infos = new VulkanSPVShaderInfo[]
                {
                    new VulkanSPVShaderInfo(ShaderTypes.VertexShader, "Vert", new FileInfo(Path.Combine(shaders,"vert.spv"))),
                    new VulkanSPVShaderInfo(ShaderTypes.FragmentShader, "Frag", new FileInfo(Path.Combine(shaders, "frag.spv")))
                };
                await Task.WhenAll(infos[0].LoadShader(), infos[1].LoadShader());

                var layoutJson = pipelineJson.GetProperty("DescriptorSetLayoutCreateOptions").GetRawText();
                var pipeLayoutJson = pipelineJson.GetProperty("PipelineLayoutCreateOption").GetRawText();
                var pipeline = manager.CreateEmptyPipeline("Text")
                    .BuildPipelineLayoutFromJson(layoutJson, pipeLayoutJson)
                    .BuildGraphicsPipelineFromJson(pass, pipelineCache, infos, pipelineJson.GetProperty("Pipeline").GetRawText());
                var sCommand = lDevice.CreateCommandPool((device, option) =>
                {
                    option.Flags = CommandPoolCreateFlags.ResetCommandBufferBit;
                    option.QueueFamilyIndex = device.WorkQueues[0].FamilyIndex;
                }).CreateSecondaryCommandBuffer();
                var sGroup = manager.CreateSecondaryVulkanRenderGroup("Text", pipeline)
                    .Binding(mainGroup, sCommand, new VulkanSemaphore[0], new VulkanFence[0]);
                sGroup.SetCamera(camera);
                sGroup.CreateGroupRenderingAction((builder) => builder.UseMiddleware<Middleware>());
                sGroup.CreateItemRenderingAction((builder) => builder.UseMiddleware<ItemMiddleware>());
                var scene = RenderWapper.CreateAndCutScene<Scene>("Main", mainGroup, typeof(TransformSystem));
                scene.AddSceneModules(typeof(FontModule));
                await scene.LoadSceneModules();
                scene.Initialization();
            }
            catch (Exception e)
            {
                _Logger.LogError(e.Message + "\r\n" + e.StackTrace);
            }
        }

        protected override Task Unload(VulkanGraphicsManager manager)
        {
            return Task.CompletedTask;
        }
    }
}
