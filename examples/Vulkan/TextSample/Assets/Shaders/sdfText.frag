#version 430
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (set = 0, binding = 0) uniform sampler2DArray text;

layout (location = 0) in vec3 inTexCoor;
layout (location = 0) out vec4 outColor;

const float distanceMark = 0.6;
const float smoothDelta = 1.0 / 4.0;

void main() {
    vec4 d = textureLod(text, inTexCoor, 0.0);
    float distance = d.r;
    vec4 final = vec4(0.0,0.0,0.0,0.0);
    float alpha = smoothstep(distanceMark - smoothDelta, distanceMark + smoothDelta * 2, distance);
    final.a = alpha;
    outColor = final;
}