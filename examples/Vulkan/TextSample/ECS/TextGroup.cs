﻿using IOP.SgrA.ECS;
using System;
using System.Collections.Generic;
using System.Text;

namespace TextSample
{
    public struct TextGroup
    {
        public Entity Entity;
        public TransformComponent Transform;
        public TextComponent Text;
    }
}
