﻿using IOP.SgrA.ECS;
using System;
using System.Collections.Generic;
using System.Text;

namespace TextSample
{
    /// <summary>
    /// 
    /// </summary>
    public struct TextComponent : IComponentData
    {
        public float Height;
        public float FontSize;
        public int Row;
        public float XOffset;
        public float YOffset;
    }
}
