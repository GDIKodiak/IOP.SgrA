﻿using IOP.Extension.DependencyInjection;
using IOP.SgrA;
using IOP.SgrA.ECS;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace TextSample
{
    public class TransformSystem : ComponentSystem
    {

        private float aspect;
        private Camera Camera;
        private readonly float[] heightBox = new float[30];

        public override void Initialization()
        {
            float height = 0;
            ContextManager.Foreach<TextGroup>((group) =>
            {
                var text = group.Text;
                if (heightBox[text.Row] != 0) return;
                height += text.Height;
                heightBox[text.Row] = height;
                if(Camera == null)
                {
                    var context = ContextManager.GetContext(group.Entity.Index);
                    var rGourp = context.GetContextRenderGroup();
                    Camera = rGourp.Camera;
                }
            });
        }

        public override void Update(TimeSpan timeSpan)
        {
            float width = Camera.Viewport.Width;
            float height = Camera.Viewport.Height;
            aspect = width / height;
            float xOffset = aspect / Camera.Viewport.Width;
            float top = 1.0f;
            ContextManager.Foreach<TextGroup>((group) =>
            {
                var text = group.Text;
                float size = text.FontSize / 0.75f / (height / 2);
                var scale = Matrix4x4.CreateScale(size, size, 1.0f);
                float line = heightBox[text.Row] / (height / 2);
                float x = text.XOffset / (height / 2);
                float y = text.YOffset / (height / 2);
                Context context = ContextManager.GetContext(group.Entity);
                ref MVPMatrix mvp = ref context.GetMVPMatrix();
                mvp.ModelMatrix = scale * Matrix4x4.CreateTranslation(new Vector3(-aspect + x, top - line - y, 0.0f));
                context.PushToRenderGroup();
            });
        }
    }
}
