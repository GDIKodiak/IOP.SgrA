﻿using IOP.SgrA.ECS;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace TextSample
{
    public struct TransformComponent : IComponentData
    {
        public Vector3 Position;
        public Quaternion Angle;
        public Vector3 Scale;
    }
}
