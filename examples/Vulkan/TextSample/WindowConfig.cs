﻿using IOP.SgrA;
using IOP.SgrA.SilkNet.Vulkan;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using TextSample.Middlewares;

namespace TextSample
{
    public class WindowConfig : IWindowConfig<Window, VulkanGraphicsManager>
    {
        public Task Config(Window window, VulkanGraphicsManager graphics)
        {
            var windowOption = window.Option;
            var lDevice = graphics.VulkanDevice;
            var basePath = AppContext.BaseDirectory;
            var document = JsonDocument.Parse(File.OpenRead(Path.Combine(basePath, "RenderPass.json")));
            var passJson = document.RootElement;
            var swapchain = lDevice.CreateDefaultSwapChain(window)
                .CreateDefaultSwapChainImageViews();
            var imageAvailableSemaphore = lDevice.CreateSemaphore(ArrangeConstant.ImageAvailableSemaphore);
            var renderFinishedSemaphore = lDevice.CreateSemaphore(ArrangeConstant.RenderFinishSemaphore);
            var fence = lDevice.CreateFence();
            var dImage = lDevice.CreateDepthBufferFromJson(passJson.GetProperty("DepthBufferCreateOption").GetRawText());
            var pass = lDevice.CreateSwapchainRenderPassFromJson("Main", swapchain, new Format[] { dImage.Format },
                passJson.GetProperty("RenderPassCreateOption").GetRawText());
            pass.CreateSwapchainFrameBuffer(swapchain, new VulkanImageView[] { dImage }, windowOption.Height, windowOption.Width);
            pass.ConfigBeginInfo((option) =>
            {
                option.RenderArea = new Rect2D { Extent = new Extent2D(windowOption.Width, windowOption.Height), Offset = new Offset2D { X = 0, Y = 0 } };
                option.ClearValues = new ClearValue[] 
                {
                    new ClearValue() { Color = new ClearColorValue(1.0f, 1.0f, 1.0f, 1.0f) },
                    new ClearValue() { DepthStencil = new ClearDepthStencilValue(1.0f, 0) }
                };
            });
            var pCommand = lDevice.CreateCommandPool((device, option) =>
            {
                option.Flags = CommandPoolCreateFlags.ResetCommandBufferBit;
                option.QueueFamilyIndex = device.WorkQueues[0].FamilyIndex;
            }).CreatePrimaryCommandBuffer();
            var mainGroup = graphics.CreatePrimaryRenderGroup("Main", null)
                .Binding(pCommand, lDevice, pass, new Semaphore[] { imageAvailableSemaphore.RawHandle, renderFinishedSemaphore.RawHandle }, new Fence[] { fence.RawHandle });
            mainGroup.CreateGroupRenderingAction((builder) => builder.UseMiddleware<MainGroupRenderingMiddleware>());
            window.CreateVulkanRenderDispatcher(lDevice.WorkQueues[0], swapchain, pass, imageAvailableSemaphore, renderFinishedSemaphore);
            window.ConfigResizeCallbackHandle(RasizeHandle);
            return Task.CompletedTask;
        }

        public void Initialization()
        {
        }

        private void RasizeHandle(VulkanGlfwWindow window, uint width, uint height)
        {
            window.RenderDispatcher.RecreateRenderDispatcher(width, height);
        }
    }
}
