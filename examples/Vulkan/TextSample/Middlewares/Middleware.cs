﻿using IOP.Extension.DependencyInjection;
using IOP.SgrA;
using IOP.SgrA.SilkNet.Vulkan;
using Microsoft.Extensions.Logging;
using Silk.NET.Vulkan;
using System;

namespace TextSample
{
    public class Middleware : IProductionLineMiddleware<SecondaryVulkanRenderGroup>
    {
        [Autowired]
        private ILogger<Middleware> _Logger { get; set; }

        public void Invoke(SecondaryVulkanRenderGroup group, RenderingProductionLineDelegate<SecondaryVulkanRenderGroup> next)
        {
            try
            {
                var camera = group.Camera;
                var view = camera.ViewMatrix;
                var project = camera.ProjectionMatrix;
                var cmd = group.SecondaryCommandBuffer;
                CommandBufferInheritanceInfo info = new CommandBufferInheritanceInfo
                {
                    Framebuffer = group.Framebuffer.RawHandle,
                    OcclusionQueryEnable = false,
                    RenderPass = group.RenderPass.RawHandle,
                    Subpass = 0
                };
                cmd.Reset();
                cmd.Begin(CommandBufferUsageFlags.OneTimeSubmitBit | 
                    CommandBufferUsageFlags.RenderPassContinueBit, info);
                cmd.BindPipeline(PipelineBindPoint.Graphics, group.Pipeline);

                var viewPort = camera.Viewport;
                cmd.SetViewport(new Viewport(viewPort.X, viewPort.Y, viewPort.Width, viewPort.Height, viewPort.MinDepth, viewPort.MaxDepth));
                cmd.SetScissor(new Rect2D(null, new Extent2D((uint)viewPort.Width, (uint)viewPort.Height)));

                foreach (var item in group.GetContexts())
                {
                    item.SetViewMatrix(in view);
                    item.SetProjectionMatrix(in project);
                    item.SetCamera(camera);
                    group.ItemProductionLine(item);
                }
                cmd.End();
            }
            catch (Exception e)
            {
                _Logger.LogError(e.Message + "\r\n" + e.StackTrace);
                group.Disable();
            }
        }
    }
}
