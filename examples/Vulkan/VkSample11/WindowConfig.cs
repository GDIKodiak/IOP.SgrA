﻿using IOP.SgrA;
using IOP.SgrA.SilkNet.Vulkan;
using Microsoft.Extensions.Logging;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VkSample11
{
    public class WindowConfig : IWindowConfig<Window, VulkanGraphicsManager>
    {

        public Task Config(Window window, VulkanGraphicsManager graphics)
        {
            try
            {
                var windowOption = window.Option;
                var lDevice = graphics.VulkanDevice; 
                VulkanSwapchain swapchain = lDevice.CreateDefaultSwapChain(window).CreateDefaultSwapChainImageViews();
                var area = new Area(0, 0, windowOption.Width, windowOption.Height);
                var pass = lDevice.CreateSwapchainScriptedRenderPass<SwapChainRenderPass>(swapchain, area);
                var i = lDevice.CreateSemaphore(ArrangeConstant.ImageAvailableSemaphore);
                var f = lDevice.CreateSemaphore(ArrangeConstant.RenderFinishSemaphore);
                window.CreateVulkanRenderDispatcher(lDevice.WorkQueues[0], swapchain, pass, i, f);
                window.ConfigResizeCallbackHandle(RasizeHandle);
            }
            catch (Exception e)
            {
                graphics.Logger.LogError(e.Message + "\r\n" + e.StackTrace);
            }
            return Task.CompletedTask;
        }

        public void Initialization()
        {
        }

        private void RasizeHandle(VulkanGlfwWindow window, uint width, uint height)
        {
            window.RenderDispatcher.RecreateRenderDispatcher(width, height);
        }
    }
}
