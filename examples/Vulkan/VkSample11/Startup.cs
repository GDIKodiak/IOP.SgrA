﻿using IOP.SgrA;
using IOP.SgrA.SilkNet.Vulkan;
using Silk.NET.Vulkan.Extensions.KHR;
using Silk.NET.Vulkan.Extensions.EXT;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Silk.NET.Vulkan;
using Microsoft.Extensions.Logging;

namespace VkSample11
{
    public class Startup : ISgrAStartup
    {
        public void Configure(IGraphicsBuilder server, IHostEnvironment env)
        {
            server.BuildVulkan((manager) =>
            {
                manager.CreateVulkanInstance((appInfo) =>
                {
                    appInfo.ApiVersion = new Versions(1, 0, 0);
                    appInfo.ApplicationName = "HelloVulkan";
                    appInfo.ApplicationVersion = new Versions(1, 0, 0);
                    appInfo.EngineName = "HelloVulkan";
                    appInfo.EngineVersion = new Versions(1, 0, 0);
                    appInfo.ApiVersion = new Versions(1, 2, 0);
                }, (instanceInfo) =>
                {
                    if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                        instanceInfo.EnabledExtensionNames = new string[] { KhrSurface.ExtensionName, KhrWin32Surface.ExtensionName, ExtDebugReport.ExtensionName };
                    else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                        instanceInfo.EnabledExtensionNames = new string[] { KhrSurface.ExtensionName, KhrXcbSurface.ExtensionName, ExtDebugReport.ExtensionName };
                    instanceInfo.EnabledLayerNames = new string[] { "VK_LAYER_KHRONOS_validation" };
                }).CreateDebugReportCallback(DebugReport);
                var pDevice = manager.PhysicalDevices[0];
                var lDevice = manager.CreateVulkanDevice(pDevice, (device) =>
                {
                    device.Name = "main";
                    device.EnabledExtensionNames = new string[] { KhrSwapchain.ExtensionName };
                    device.EnabledLayerNames = null;
                }, (phyDevice) =>
                {
                    QueueFamilyProperties[] properties = phyDevice.GetQueueFamilyProperties();
                    manager.Logger.LogInformation($"Vulkan硬件设备支持的队列家族数量为{properties.Length}");
                    var result = new List<QueueFamilyIndices>();
                    QueueFamilyIndices index = new QueueFamilyIndices();
                    for (uint i = 0; i < properties.Length; i++)
                    {
                        if (properties[i].QueueFlags.HasFlag(QueueFlags.GraphicsBit))
                        {
                            index.FamilyType |= QueueType.Graphics;
                            if (phyDevice.GetPhysicalDevicePresentationSupport((int)i))
                            {
                                index.FamilyType |= QueueType.Present;
                                index.FamilyIndex = i;
                                result.Add(index);
                                index = new QueueFamilyIndices();
                                continue;
                            }
                        }
                    }
                    return result.ToArray();
                });
                lDevice.InitWorkQueue((device, indeics) =>
                {
                    List<WorkQueue> list = new List<WorkQueue>();
                    foreach (var item in indeics)
                    {
                        if (item.FamilyType == (QueueType.Graphics | QueueType.Present))
                        {
                            Queue queue = device.GetQueue(item.FamilyIndex, 0);
                            var type = QueueType.Graphics | QueueType.Present;
                            WorkQueue work = new WorkQueue(type, item.FamilyIndex, 0, queue);
                            list.Add(work);
                            return list;
                        }
                    }
                    return list;
                });
                lDevice.CreateResourcesCommandPool(0);
                lDevice.CreatePipelineCache();
                var window = manager.CreateWindow<Window>("Sample11", new WindowOption() { Title = "Sample11", Height = 768, Width = 1366 }).ConfigWindow<Window, WindowConfig>();
                window.AddWapperModules(typeof(WindowModule));
                manager.RunWindow(window);
            });
        }

        public void ConfigureServices(IServiceCollection services)
        {
        }

        private static uint DebugReport(DebugReportFlagsEXT flags, DebugReportObjectTypeEXT objectType, ulong @object, UIntPtr location, int messageCode, string layerPrefix, string message, IntPtr userData)
        {
            Console.WriteLine(message);
            return 0;
        }
    }
}
