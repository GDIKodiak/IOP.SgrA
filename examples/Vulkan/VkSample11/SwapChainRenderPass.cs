﻿using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VkSample11
{
    [RenderPass("SwapChainRenderPass", RenderPassMode.Swapchain)]
    [Subpass(0, PipelineBindPoint.Graphics)]
    public partial class SwapChainRenderPass : ScriptedVulkanRenderPass
    {
        [Attachment(ImageLayout.PresentSrcKhr, Format.R8G8B8A8Unorm, StoreOp = AttachmentStoreOp.Store)]
        [SubpassAttachment(0, SubpassAttachmentType.ColorAttachment, ImageLayout.ColorAttachmentOptimal)]
        [ImageConfig(Usage = ImageUsageFlags.ColorAttachmentBit, ImageType = ImageType.Type2D, ViewType = ImageViewType.Type2D)]
        [AttachmentClearValue(0.2f, 0.2f, 0.2f, 0.2f)]
        public RenderPassAttachment SwapChain { get; set; }

        [Attachment(ImageLayout.DepthStencilAttachmentOptimal, Format.D16Unorm, StoreOp = AttachmentStoreOp.DontCare)]
        [SubpassAttachment(0, SubpassAttachmentType.DepthStencilAttachment, ImageLayout.DepthStencilAttachmentOptimal)]
        [ImageConfig(Usage = ImageUsageFlags.DepthStencilAttachmentBit, SubresourceRangeAspect = ImageAspectFlags.DepthBit,
            ImageType = ImageType.Type2D, ViewType = ImageViewType.Type2D)]
        [AttachmentClearValue(0.0f, 0)]
        public RenderPassAttachment Depth { get; set; }

        [SubpassDependency(SrcSubpass = 0, DstSubpass = uint.MaxValue, 
            SrcStageMask = PipelineStageFlags.ColorAttachmentOutputBit, DstStageMask = PipelineStageFlags.BottomOfPipeBit,
            SrcAccessMask = AccessFlags.ColorAttachmentWriteBit, DstAccessMask = 0, DependencyFlags = DependencyFlags.ByRegionBit)]
        public PassDependency PassDependency { get; set; }
    }
}
