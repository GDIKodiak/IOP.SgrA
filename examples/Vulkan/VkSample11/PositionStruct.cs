﻿using IOP.SgrA.ECS;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace VkSample11
{
    public struct PositionStruct : IComponentData
    {
        public Vector3 Position;
    }
}
