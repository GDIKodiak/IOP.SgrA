﻿using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VkSample11
{
    public partial class CommonPipeline : ScriptedVulkanPipeline
    {
        public Vert Vert { get; set; }

        public Frag Frag { get; set; }

        public override PipelineInputAssemblyStateCreateOption CreateInputAssemblyState()
        {
            return new PipelineInputAssemblyStateCreateOption()
            {
                PrimitiveRestartEnable = false,
                Topology = PrimitiveTopology.TriangleList
            };
        }
        public override PipelineDepthStencilStateCreateOption CreateDepthStencilState()
        {
            return new PipelineDepthStencilStateCreateOption()
            {
                Back = new StencilOpState()
                {
                    FailOp = StencilOp.Keep,
                    PassOp = StencilOp.Keep,
                    CompareOp = CompareOp.Always,
                    CompareMask = 0,
                    Reference = 0,
                    DepthFailOp = StencilOp.Keep,
                    WriteMask = 0
                },
                Front = new StencilOpState()
                {
                    FailOp = StencilOp.Keep,
                    PassOp = StencilOp.Keep,
                    CompareOp = CompareOp.Always,
                    CompareMask = 0,
                    Reference = 0,
                    DepthFailOp = StencilOp.Keep,
                    WriteMask = 0
                },
                DepthWriteEnable = true,
                DepthTestEnable = true,
                DepthCompareOp = CompareOp.Greater,
                DepthBoundsTestEnable = false,
                MinDepthBounds = 0,
                MaxDepthBounds = 0,
                StencilTestEnable = false
            };
        }
        public override PipelineDynamicStateCreateOption CreateDynamicState()
        {
            return new PipelineDynamicStateCreateOption()
            {
                DynamicStates = Array.Empty<DynamicState>()
            };
        }
    }
}
