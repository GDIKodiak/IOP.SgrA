﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VkSample11
{
    public class Vert : GLSLVert
    {
        [UniformBuffer(0, 0, DescriptorSetTarget.Pipeline)]
        public BufferVals Vals {  get; set; }

        [VertInput(0, Format.R32G32B32Sfloat)]
        public vec3 Pos { get; set; }
        [VertInput(1, Format.R32G32B32Sfloat)]
        public vec3 Color { get; set; }

        public override void main()
        {
        }
    }

    public struct BufferVals
    {
        public mat4 mvp;
    }
}
