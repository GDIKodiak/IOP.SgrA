﻿using IOP.SgrA.GLSL;
using IOP.SgrA.SilkNet.Vulkan.Scripted;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VkSample11
{
    public class Frag : GLSLFrag
    {
        [FragOutput(0, false)]
        public vec4 OutColor {  get; set; }

        public override void main()
        {
        }
    }
}
