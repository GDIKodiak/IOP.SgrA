﻿using IOP.SgrA;
using IOP.SgrA.ECS;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace VkSample11
{
    public class RotateSystem : ComponentSystem
    {

        private float angle;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="contextManager"></param>
        public RotateSystem()
        {
        }

        public override void Initialization()
        {
        }

        /// <summary>
        /// 更新
        /// </summary>
        public override void Update(TimeSpan lastStamp)
        {
            angle++;
            if (angle >= 360.0) angle = 0;
            ContextManager.Foreach<Group>((group) =>
            {
                Context local = ContextManager.GetContext(group.Entity);
                ref MVPMatrix mvp = ref local.GetMVPMatrix();
                mvp.ModelMatrix = Matrix4x4.CreateRotationX(angle / 180.0f * MathF.PI);
                local.PushToRenderGroup();
            });
        }
    }

    public struct Group
    {
        public Entity Entity;
        public PositionStruct Position;
    }
}
