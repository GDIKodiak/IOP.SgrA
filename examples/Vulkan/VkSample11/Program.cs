﻿using IOP.ISEA;
using IOP.SgrA;
using IOP.SgrA.SilkNet.Vulkan;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace VkSample11
{
    class Program
    {
        static async Task Main(string[] args)
        {
            CreateISEA(true);
            var h = Host.CreateDefaultBuilder(args).ConfigureSgrAHost((sgra) =>
            {
                sgra.AddVulkanEnvironmental();
                sgra.UseStartup<Startup>();
            }).ConfigureLogging((logger) => logger.AddConsole());
            await h.RunConsoleAsync();
        }

        static void CreateISEA(bool isNeed)
        {
            if (!isNeed) return;
            float[] data = new float[]
            {
                0.0f,75f,0,1,0,0,
                -45f,0,0,0,1,0,
                45f,0,0,0,0,1
            };
            ISEADocument document = new ISEADocument();
            MeshNode mesh = document.CreateEmptyMeshNode("Test1MESH", 3)
                .AddMeshData(data, 24, 0, MeshDataPurpose.COMPLEX, false);
            RenderObjectNode renderObject = new RenderObjectNode()
            {
                Comonents = new List<Node> { mesh },
                Name = "Test1"
            };
            document.AddNode(renderObject, out _);
            document.Save(AppContext.BaseDirectory, "Test11");
        }
    }
}
