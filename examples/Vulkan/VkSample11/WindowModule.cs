﻿using IOP.ISEA;
using IOP.SgrA;
using IOP.SgrA.SilkNet.Vulkan;
using Microsoft.Extensions.Logging;
using Silk.NET.Vulkan;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using SubmitOption = IOP.SgrA.SilkNet.Vulkan.SubmitOption;

namespace VkSample11
{
    public class WindowModule : VulkanModule, IVulkanRenderWapperModule
    {
        public VulkanRenderWapper RenderWapper { get; set; }

        protected override async Task Load(VulkanGraphicsManager manager)
        {
            var area = RenderWapper.RenderArea;
            var lDevice = manager.VulkanDevice;
            var pool = lDevice.CreateCommandPool((device, option) =>
            {
                option.Flags = CommandPoolCreateFlags.ResetCommandBufferBit;
                option.QueueFamilyIndex = device.WorkQueues[0].FamilyIndex;
            });
            var command = pool.CreatePrimaryCommandBuffer();
            VulkanShaderInfo[] infos = new VulkanSPVShaderInfo[]
            {
                new VulkanSPVShaderInfo(ShaderTypes.VertexShader, "main_vert", new FileInfo(Path.Combine("shaders","vert.spv"))),
                new VulkanSPVShaderInfo(ShaderTypes.FragmentShader, "main_frag", new FileInfo(Path.Combine("shaders","frag.spv")))
            };
            await manager.LoadShadersAsync(infos);
            var renderPass = RenderWapper.RenderDispatcher.GetSwapchainRenderPass();
            var pipeCache = lDevice.PipelineCache;
            var setLayout = new DescriptorSetLayoutCreateOption[]
            {
                new DescriptorSetLayoutCreateOption
                {
                    Bindings = new DescriptorSetLayoutBindingOption[]
                    {
                        new DescriptorSetLayoutBindingOption
                        {
                            Binding = 0,
                            DescriptorCount = 1,
                            DescriptorType = DescriptorType.UniformBuffer,
                            StageFlags = ShaderStageFlags.VertexBit
                        }
                    },
                    DescriptorSetTarget = DescriptorSetTarget.Pipeline,
                    MaxSets = 1,
                    SetIndex = 0
                }
            };
            var pipeline = manager.BuildScriptedGraphicsPipeline<CommonPipeline>("Main", renderPass, pipeCache, infos, area)
                .BuildUniformBuffer(0, 0, sizeof(float) * 16, SharingMode.Exclusive);
            manager.DeleteShaders(infos);

            var fence = lDevice.CreateFence();
            var disapatcher = RenderWapper.GetRenderDispatcher<VulkanSwapchainRenderDispatcher>();
            var group = manager.CreatePrimaryRenderGroup("MainRenderGroup", pipeline)
                .Binding(command, lDevice, disapatcher.SwapchainRenderPass,
                new Semaphore[] { disapatcher.ImageAvailableSemaphore, disapatcher.RenderFinishSemaphore },
                new Fence[] { fence.RawHandle });
            group.CreateGroupRenderingAction((builder) =>
            {
                builder.Run((group) =>
                {
                    try
                    {
                        var queue = group.WorkQueues[0].Queue;
                        var cmdBuffer = group.PrimaryCommandBuffer;
                        var semaphores = group.Semaphores;
                        var face = group.Fences;
                        var camera = group.Camera;
                        var pass = group.RenderPass;
                        var device = group.LogicDevice;
                        VulkanFrameBuffer framebuffer = group.RenderPass.GetFramebuffer(group.CurrentFrame);
                        cmdBuffer.Reset();
                        cmdBuffer.Begin();
                        cmdBuffer.BeginRenderPass(pass, framebuffer, pass.BeginOption, SubpassContents.Inline);
                        var view = camera.ViewMatrix;
                        var pro = camera.ProjectionMatrix;
                        foreach (var item in group.GetContexts())
                        {
                            item.SetViewMatrix(in view);
                            item.SetProjectionMatrix(in pro);
                            group.ItemProductionLine?.Invoke(item);
                        }
                        cmdBuffer.EndRenderPass();
                        cmdBuffer.End();

                        var submitInfo = new SubmitOption
                        {
                            WaitDstStageMask = new PipelineStageFlags[] { PipelineStageFlags.ColorAttachmentOutputBit },
                            WaitSemaphores = new Semaphore[] { semaphores[0] },
                            Buffers = new CommandBuffer[] { cmdBuffer.RawHandle },
                            SignalSemaphores = new Semaphore[] { semaphores[1] }
                        };
                        lDevice.Submit(queue, face[0], submitInfo);
                        Result r = Result.Timeout;
                        do
                        {
                            r = lDevice.WaitForFences(face, true, 100000000);
                        } while (r == Result.Timeout);
                        group.LogicDevice.ResetFences(face);
                    }
                    catch (Exception e)
                    {
                        group.Logger.LogError(e.Message);
                        group.Disable();
                    }
                });
            });
            group.CreateItemRenderingAction((builder) =>
            {
                builder.Run((context) =>
                {
                    var cmd = context.CommandBuffer;
                    var render = context.GetContextRenderObject();
                    var pipeline = context.VulkanPipeline;
                    var uBuffer = pipeline.GetPipelineTexture(0, 0);
                    ref MVPMatrix local = ref context.GetMVPMatrix();
                    var mat = local.ModelMatrix * local.ViewMatrix * local.ProjectionMatrix;
                    byte[] data = mat.ToBytes();
                    uBuffer.UpdateTextureMemoryData(data);
                    DescriptorSet[] sets = new DescriptorSet[] { uBuffer.DescriptorSet };
                    VRO vro = render.GetComponents<VRO>().First();

                    cmd.BindPipeline(PipelineBindPoint.Graphics, pipeline);
                    cmd.BindDescriptorSets(PipelineBindPoint.Graphics, pipeline.PipelineLayout, 0, null, sets);
                    cmd.BindVertexBuffers(0, new ulong[] { 0 }, vro.VerticesBufferInfo.Buffer);
                    cmd.Draw(3, 1, 0, 0);
                });
            });
            var aspect = (float)area.Width / area.Height;
            var camera = new Camera(new Vector3(0, 0, 200), new Vector3(0, 0, 0), new Vector3(0, 1, 0));
            camera.SetProjectMatrix(-aspect, 1.0f, 1.5f, 1000, reversedZ: true);
            group.SetCamera(camera);

            RenderWapper.CreateAndCutScene<Scene>("Main", group, typeof(RotateSystem));

            ISEADocument document = new ISEADocument();
            await document.Load(Path.Combine(AppContext.BaseDirectory, "Test11.isea"));
            var renders = await manager.CreateRenderObjectsFromISEA<RenderObject>(document);
            var archetype = ContextManager.CreateArchetype(new PositionStruct());
            ContextManager.CreateContexts(1, archetype, group, renders[0]);
        }

        protected override Task Unload(VulkanGraphicsManager manager)
        {
            return Task.CompletedTask;
        }
    }
}
